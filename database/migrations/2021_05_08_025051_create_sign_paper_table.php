<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSignPaperTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sign_paper', function(Blueprint $table)
		{
			$table->integer('id', true)->comment('giấy tờ phê duyệt');
			$table->string('name_paper', 400)->nullable()->comment('tên tờ trình');
			$table->date('date_sign')->nullable()->comment('Ngày ký');
			$table->string('content', 10000)->nullable()->comment('Có thể có có thể không');
			$table->integer('contract_purchase_id')->nullable()->comment('map với id trong contrat_purchase');
			$table->string('num_paper', 400)->nullable()->comment('Số tờ trình');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sign_paper');
	}

}
