<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmBackupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('film_backups', function(Blueprint $table)
		{
			$table->bigInteger('id',true);
			$table->string('driver', 100)->nullable()->comment('Mã ổ');
			$table->string('backup_driver', 100)->nullable()->comment('ổ backup');
			$table->string('file_name', 1000)->comment('tên file');
			$table->string('film_name', 1000)->nullable()->comment('Tên phim tiếng Anh');
			$table->string('vn_name', 1000)->nullable()->comment('Tên phim tiếng Việt');
			$table->string('country', 100)->nullable()->comment('Quốc gia');
			$table->string('partner', 100)->nullable()->comment('đối tác');
			$table->string('series_film', 100)->nullable()->comment('Bộ hay lẻ');
			$table->string('count_film', 100)->nullable()->comment('Số tập');
			$table->string('file_type', 10)->nullable()->comment('Loại file');
			$table->string('time_number', 100)->nullable()->comment('Thời lượng');
			$table->string('volumn', 100)->nullable()->comment('Dung lượng(Gb)');
			$table->string('file_format', 100)->nullable()->comment('Đuôi file hình');
			$table->date('end_time')->nullable()->comment('Thời hạn bản quyền');
			$table->string('file_delivery_person',1000)->nullable()->comment('Người giao file');
			$table->bigInteger('admin_id')->nullable();
			$table->integer('status')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('film_backups');
	}

}
