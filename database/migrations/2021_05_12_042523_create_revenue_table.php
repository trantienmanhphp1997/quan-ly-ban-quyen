<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRevenueTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('revenue', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('contract_id')->nullable()->comment('map với contract');
			$table->smallInteger('type_contract')->nullable()->default(1)->comment('0: viettel ban;  1: Viettel Mua');
			$table->bigInteger('media_id')->nullable()->comment('map với phim, nhạc, trong trường hợp phat sinh doanh thu theo media');
			$table->bigInteger('category_id')->nullable()->comment('map với category');
			$table->bigInteger('admin_id')->nullable()->comment('người tạo');
			$table->smallInteger('type_id')->nullable()->comment('Loại doanh thu: QPV; số liên kết với type_revenue');
			$table->date('date_sign')->nullable()->comment('Thời điểm ghi nhận doanh thu');
			$table->bigInteger('money')->nullable()->comment('Số tiền nhận được');
			$table->smallInteger('type_revenue')->nullable()->comment('0: ký hợp đồng, 1: gia hạn');
            $table->bigInteger('sale_money_id')->nullable()->comment('map với bảng contract_media_sale_role');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('revenue');
	}

}
