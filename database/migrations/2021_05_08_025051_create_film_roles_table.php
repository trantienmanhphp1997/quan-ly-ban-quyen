<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('film_roles', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->bigInteger('film_id')->nullable();
			$table->integer('role_id')->nullable()->comment('map voi role');
			$table->boolean('type')->nullable()->comment('1: truyen hinh; 2: VOD, 3 : kinh doanh nhac');
			$table->string('country_code', 100)->nullable()->comment('1: VN; 2: other');
			$table->date('start_date')->nullable()->comment('Ngay bat dau');
			$table->date('end_date')->nullable()->comment('Ngay ket thuc');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('film_roles');
	}

}
