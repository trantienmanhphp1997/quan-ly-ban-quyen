<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMusicUploadTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('music_upload', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->text('STT', 1000)->nullable();
			$table->text('ten_bai_hat', 1000);
			$table->string('ca_si', 500)->nullable();
			$table->string('nhac_si', 500)->nullable();
			$table->string('album', 500)->nullable();
			$table->string('doc_quyen', 500)->nullable();
			$table->string('tac_quyen', 500)->nullable();
			$table->string('doc_quyen_so', 100)->nullable();
			$table->string('tac_quyen_so', 100)->nullable();
			$table->string('ban_doi_tac_3', 100)->nullable();
			$table->string('loai_hd', 1000)->nullable();
			$table->text('quyen_ghi_am', 1000)->nullable();
			$table->text('quyen_tac_gia', 1000)->nullable();
			$table->text('phu_luc_hd', 1000)->nullable();
			$table->text('gia_tien', 1000)->nullable();
			$table->text('so_hd_lq', 1000)->nullable();
			$table->text('pl_hd', 1000)->nullable();
			$table->string('stt_phu_luc', 100)->nullable();
			$table->string('hd_tac_gia', 1000)->nullable();
			$table->string('pl_quyen_tac_gia', 1000)->nullable();
			$table->string('time_release', 1000)->nullable();
			$table->string('time_end', 1000)->nullable();
			$table->string('mv', 1000)->nullable();
			$table->text('audio')->nullable();
			$table->text('note')->nullable();
			$table->text('id_ms')->nullable();
			$table->integer('status')->default(0);
			$table->timestamps();
			$table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
			$table->string('stt_pl_quyen_tac_gia', 100)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('music_upload');
	}

}
