<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActionLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('action_logs', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->comment('Luu log he thong');
			$table->integer('record_id')->nullable()->comment('map voi ban ghi tac dong');
			$table->string('model')->nullable()->comment('map voi table tac dong');
			$table->string('module', 100)->nullable()->comment('module tac dong');
			$table->boolean('admin_id');
			$table->string('action_log', 200)->nullable()->comment('log tac dong');
			$table->timestamps();
			$table->text('old_value')->nullable();
			$table->text('new_value')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('action_logs');
	}

}
