<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColoumnNote3000ToFilmUploads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('film_uploads', function (Blueprint $table) {
            $table->dropColumn('note');
            $table->dropColumn('addition_note');
        });
        Schema::table('film_uploads', function (Blueprint $table) {
            $table->text('note', 3000)->nullable();  
            $table->text('addition_note', 3000)->nullable();  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('film_uploads', function (Blueprint $table) {
            //
        });
    }
}
