<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractorSelectionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contractor_selections', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('contract_number')->nullable();
			$table->string('date_sign')->nullable();
			$table->string('plan')->nullable();
			$table->integer('result')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contractor_selections');
	}

}
