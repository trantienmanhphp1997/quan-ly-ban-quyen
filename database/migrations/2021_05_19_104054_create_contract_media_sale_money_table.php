<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractMediaSaleMoneyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contract_media_sale_money', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('contract_id')->nullable();
			$table->bigInteger('media_id')->nullable()->comment('trong trường hợp nhiều bài hát thì để trống');
			$table->string('ma_nhac_cho')->nullable();
			$table->bigInteger('money')->nullable()->comment('Giá tiền');
			$table->bigInteger('category_id')->nullable();
			$table->boolean('cho_doc_quyen')->nullable()->default(0)->comment('cho nhạc; cho doc quyen');
			$table->boolean('cho_tac_quyen')->nullable()->default(0)->comment('cho nhạc; cho tac quyen');
			$table->boolean('so_doc_quyen')->nullable()->default(0)->comment('cho nhạc; so doc quyen');
			$table->boolean('so_tac_quyen')->nullable()->default(0)->comment('cho nhạc; So tac quyen');
			$table->boolean('sale_3rd')->nullable()->default(0)->comment('cho nhạc; ban doi tac thu 3');
			$table->date('start_time')->nullable()->comment('cho nhạc thoi gian bat dau');
			$table->date('end_time')->nullable()->comment('cho nhạc, thoi gian ket thuc');
			$table->timestamps();
			$table->bigInteger('admin_id')->nullable();
            $table->string('nuoc', 200)->nullable()->comment('nước phát sóng');
            $table->string('note', 500)->nullable()->comment('Ghi chú nước phát sóng');
            // $table->string('note_film', 500)->nullable()->comment('Ghi chú phim');
            $table->string('count_live', 200)->nullable()->comment('Số lần phát sóng');

        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contract_media_sale_money');
	}

}
