<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnToFilmUpload extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('film_uploads', function (Blueprint $table) {
            $table->dropColumn('addition_note');
            $table->dropColumn('note');
        });
        Schema::table('film_uploads', function (Blueprint $table) {
            $table->string('addition_note', 1000)->nullable();  
            $table->string('note', 1000)->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('film_uploads', function (Blueprint $table) {
            //
        });
    }
}
