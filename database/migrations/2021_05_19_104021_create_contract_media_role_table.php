<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractMediaRoleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contract_media_role', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('contract_id')->nullable();
			$table->bigInteger('role_id')->nullable();
			$table->bigInteger('media_id')->nullable();
			$table->integer('category_id')->nullable();
			$table->bigInteger('admin_id')->nullable();
			$table->date('start_time')->nullable();
			$table->date('end_time')->nullable();
			$table->boolean('cho_doc_quyen')->nullable()->comment('Cho nhạc');
			$table->boolean('cho_tac_quyen')->nullable()->comment('Cho nhạc');
			$table->boolean('so_doc_quyen')->nullable()->comment('Cho nhạc');
			$table->boolean('so_tac_quyen')->nullable()->comment('Cho nhạc');
			$table->boolean('sale_3rd')->nullable()->comment('bán đối tác thứ 3');
			$table->bigInteger('sale_money_id')->nullable();

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contract_media_role');
	}

}
