<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractMediaSaleRoleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contract_media_sale_role', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->comment('Hiện tại chỉ dành cho phim (nhiều quyền)');
			$table->bigInteger('contract_id')->nullable()->comment('Map với contract sale');
			$table->bigInteger('media_id')->nullable()->comment('map với music/film');
			$table->bigInteger('role_id')->nullable();
			$table->date('start_time')->nullable();
			$table->date('end_time')->nullable();
			$table->bigInteger('admin_id')->nullable();
			$table->bigInteger('category_id')->nullable()->comment('map với category');
            $table->bigInteger('sale_money_id')->nullable()->comment('map với contract_media_sale_money');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contract_media_sale_role');
	}

}
