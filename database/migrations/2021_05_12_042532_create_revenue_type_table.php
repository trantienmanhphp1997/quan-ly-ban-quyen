<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRevenueTypeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('revenue_type', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->string('name')->nullable()->comment('Tên doanh thu:');
			$table->bigInteger('parent_id')->nullable()->comment('id cha');
			$table->string('note', 1000)->nullable()->comment('mô tả doanh thu');
			$table->bigInteger('admin_id')->nullable()->comment('Người tạo');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('revenue_type');
	}

}
