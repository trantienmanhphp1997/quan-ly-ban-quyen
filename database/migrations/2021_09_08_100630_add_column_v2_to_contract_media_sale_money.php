<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnV2ToContractMediaSaleMoney extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contract_media_sale_money', function (Blueprint $table) {
			$table->string('name_khong_dau', 1000)->nullable()->comment('TÊN BÀI HÁT không dấu');
			$table->string('singer_khong_dau', 1000)->nullable()->comment('CA SĨ THỂ HIỆN không dấu');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contract_media_sale_money', function (Blueprint $table) {
            $table->dropColumn('name_khong_dau');
            $table->dropColumn('singer_khong_dau');
        });
    }
}
