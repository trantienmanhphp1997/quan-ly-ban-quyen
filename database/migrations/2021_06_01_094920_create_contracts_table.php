<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contracts', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('contract_number', 200)->nullable();
			$table->string('title', 200)->nullable();
			$table->date('date_sign')->nullable();
			$table->bigInteger('total_money')->nullable()->comment('Tổng chi phí');
			$table->string('content', 1000)->nullable();
			$table->string('user_name', 1000)->nullable();
			$table->string('user_msisdn', 1000)->nullable();
			$table->string('user_email', 1000)->nullable();
			$table->integer('admin_id')->nullable()->comment('Nguoi tao');
			$table->timestamps();
			$table->integer('status')->nullable();
			$table->string('action', 10000)->nullable();
			$table->integer('type')->nullable()->comment('1: Viettel mua, 2: Vietetl ban, 3 : Partner');
			$table->integer('admin_update_id')->nullable()->comment('Nguoi cap nhat cuoi cung');
			$table->integer('partner_id')->nullable();
			$table->integer('fee1')->nullable()->comment('Chi phí bản quyền (VNĐ)');
			$table->integer('fee2')->nullable()->comment('chi phí hậu kỳ (VNĐ)');
			$table->integer('fee3')->nullable()->comment('chi phí kiểm duyệt (VNĐ)');
			$table->smallInteger('category_id')->nullable()->comment('Loại hợp đồng(phim, nhạc,quảng cáo, mua domain)');
			$table->integer('type_fee_id')->nullable();
			$table->date('start_time')->nullable()->comment('Bắt đầu hợp đồng');
			$table->date('end_time')->nullable()->comment('Kết thúc hợp đồng');
			$table->boolean('warning_status')->nullable()->default(0)->comment('=1 thì tắt cảnh báo');
			$table->text('note')->nullable()->comment('Tình trạng nội dung');
			$table->bigInteger('media_id')->nullable();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contracts');
	}

}
