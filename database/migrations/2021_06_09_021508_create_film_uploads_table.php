<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('film_uploads', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->text('STT', 1000)->nullable();
            $table->text('country', 1000)->nullable();
            $table->string('vn_name', 500);
            $table->string('year_create', 500)->nullable();
            $table->string('category', 500)->nullable();
            $table->string('count_name', 500)->nullable();
            $table->string('count_tap', 500)->nullable();
            $table->string('count_hour', 500)->nullable();
            $table->string('partner_name', 100)->nullable();
            $table->string('DQ', 100)->nullable();
            $table->string('KDQ', 100)->nullable();
            $table->string('start_time1', 1000)->nullable();
            $table->string('end_time1', 1000)->nullable();
            $table->string('contract_number', 100)->nullable();
            $table->string('fee1', 100)->nullable();
            $table->string('fee2', 100)->nullable();
            $table->string('fee3', 100)->nullable();
            $table->string('note', 500)->nullable();
            $table->integer('status')->default(0);
            $table->string('admin_id', 500)->nullable();
            $table->string('actor_name', 500)->nullable();
            $table->string('director', 500)->nullable();
            $table->string('contract_number_root', 500)->nullable();           
            $table->string('broadcast_country', 500)->nullable();           
            $table->string('nuoc', 500)->nullable();           
            $table->string('count_live', 500)->nullable();                    
            $table->string('Broadcast_infrastructure', 500)->nullable();  
            $table->string('addition_note', 500)->nullable();  

            $table->string('youtube', 500)->nullable();           
            $table->string('VOD_nuoc_ngoai', 500)->nullable();           
            $table->string('VOD_noi_bo', 500)->nullable();           
            $table->string('VOD_ngoai', 500)->nullable();           
            $table->string('TH_noi_bo', 500)->nullable();           
            $table->string('TH_ngoai', 500)->nullable();           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('film_uploads');
    }
}
