<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('username', 50)->unique();
			$table->string('password')->nullable();
			$table->string('uuid')->nullable();
			$table->boolean('is_manager')->default(0);
			$table->string('full_name')->nullable();
			$table->string('phone_number', 50)->nullable()->unique();
			$table->string('other_phone_number')->nullable();
			$table->string('department_code')->nullable();
			$table->text('address')->nullable();
			$table->string('email')->nullable();
			$table->string('language')->default('vi');
			$table->boolean('status')->default(1);
			$table->text('note')->nullable();
			$table->text('description')->nullable();
			$table->timestamps();
			$table->bigInteger('created_by')->nullable();
			$table->bigInteger('updated_by')->nullable();
			$table->string('remember_token')->nullable();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
