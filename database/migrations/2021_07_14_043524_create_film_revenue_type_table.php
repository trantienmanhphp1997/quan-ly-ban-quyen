<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmRevenueTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('film_revenue_type', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('media_id');
            $table->bigInteger('revenue_type_id');
            $table->bigInteger('money');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('film_revenue_type');
    }
}
