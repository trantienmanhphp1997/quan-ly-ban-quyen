<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractMusicSaleUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contract_music_sale_uploads', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('admin_id')->nullable();
            $table->string('STT', 1000)->nullable()->comment('STT');
            $table->string('ma_nhac_cho', 200)->nullable()->comment('Mã nhạc chờ');
            $table->string('name_khong_dau', 200)->nullable()->comment('Tên bài hát không dấu');
            $table->string('singer_khong_dau', 200)->nullable()->comment('Ca sĩ thể hiện không dấu');
            $table->string('name', 200)->nullable()->comment('Tên bài hát');
            $table->string('singer', 200)->nullable()->comment('Ca sĩ thể hiện');
            $table->string('musician', 200)->nullable()->comment('Nhạc sĩ');
            $table->boolean('cho_docquyen')->nullable();
            $table->boolean('cho_tacquyen')->nullable();
            $table->boolean('so_docquyen')->nullable();
            $table->boolean('so_tacquyen')->nullable();
            // $table->boolean('sale_permission')->nullable();
            $table->string('ghi_am', 1000)->nullable();
            $table->string('tac_gia', 1000)->nullable();
            $table->string('contract_cps', 1000)->nullable()->comment('Số Phụ lục HĐ (Số HĐ của Viettel và CPs)');
            $table->string('contract_number1', 1000)->nullable()->comment('Số HĐ Quyền liên quan (biểu diễn, ghi âm)');
            $table->string('contract_number2', 1000)->nullable()->comment('Số Phụ lục HĐ Quyền liên quan (biểu diễn, ghi âm)');
            $table->string('contract_number3', 1000)->nullable()->comment('Số thứ tự trong PL Quyền liên quan');
            $table->string('contract_number4', 1000)->nullable()->comment('Số HĐ Quyền tác giả');
            $table->string('contract_number5', 1000)->nullable()->comment('Số Phụ lục HĐ Quyền tác giả');
            $table->string('contract_number6', 1000)->nullable()->comment('Số thứ tự trong PL Quyền Tác giả');
            $table->date('start_time')->nullable()->comment('Thời hạn bản quyền');
            $table->date('end_time')->nullable()->comment('Thời hạn bản quyền');
            $table->string('music_id', 1000)->nullable();
            $table->integer('status')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contract_music_sale_uploads');
    }
}
