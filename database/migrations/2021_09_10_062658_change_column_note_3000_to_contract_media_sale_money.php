<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnNote3000ToContractMediaSaleMoney extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contract_media_sale_money', function (Blueprint $table) {
            $table->dropColumn('note_film');
            $table->dropColumn('note');
        });
        Schema::table('contract_media_sale_money', function (Blueprint $table) {
            $table->string('note_film', 3000)->nullable();  
            $table->string('note', 3000)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contract_media_sale_money', function (Blueprint $table) {
            //
        });
    }
}
