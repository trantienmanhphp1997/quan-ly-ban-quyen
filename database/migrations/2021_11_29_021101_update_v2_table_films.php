<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateV2TableFilms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('films', function (Blueprint $table) {
            $table->longtext('license_fee')->change(); 
            $table->longtext('partner_fee')->change(); 
            $table->longtext('other_fee')->change(); 
            $table->longtext('total_fee')->change(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('films', function (Blueprint $table) {
            $table->integer('license_fee')->change(); 
            $table->integer('partner_fee')->change(); 
            $table->integer('other_fee')->change(); 
            $table->integer('total_fee')->change(); 
        });
    }
}
