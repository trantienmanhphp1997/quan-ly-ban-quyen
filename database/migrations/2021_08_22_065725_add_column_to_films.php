<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToFilms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('film_uploads', function (Blueprint $table) {
            $table->string('product_name', 300)->nullable()->comment('Tên phim tiếng anh');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('film_uploads', function (Blueprint $table) {
            $table->dropColumn('product_name');
        });
    }
}
