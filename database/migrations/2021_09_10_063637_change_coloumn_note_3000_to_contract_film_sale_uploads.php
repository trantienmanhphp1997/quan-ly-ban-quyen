<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColoumnNote3000ToContractFilmSaleUploads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contract_film_sale_uploads', function (Blueprint $table) {
            $table->dropColumn('country_note_ps');
            $table->dropColumn('note');
        });
        Schema::table('contract_film_sale_uploads', function (Blueprint $table) {
            $table->text('country_note_ps', 3000)->nullable();  
            $table->text('note', 3000)->nullable();  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contract_film_sale_uploads', function (Blueprint $table) {
            //
        });
    }
}
