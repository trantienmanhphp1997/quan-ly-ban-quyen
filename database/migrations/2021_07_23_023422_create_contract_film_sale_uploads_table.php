<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractFilmSaleUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contract_film_sale_uploads', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('admin_id')->nullable();
            $table->string('STT', 300)->nullable()->comment('Số thứ tự');
            $table->string('name', 300)->nullable()->comment('Tên tiếng anh');
            $table->string('vn_name', 300)->nullable()->comment('Tên tiếng Việt');
            $table->string('contract_number', 1000)->nullable()->comment('Số hợp đồng');
            $table->string('employee_name', 300)->nullable()->comment('Họ và tên nhân viên');
            $table->string('telephone_number', 100)->nullable()->comment('SĐT');
            $table->string('email', 300)->nullable()->comment('mail nhân viên');
            $table->string('partner_name', 500)->nullable()->comment('Tên đối tác');;
            $table->string('country', 1000)->nullable()->comment('Quốc gia được phép phát sóng');
            $table->string('count', 100)->nullable()->comment('Số lần phát sóng');
            $table->string('ĐQ', 1000)->nullable()->comment('Quyền độc quyền');
            $table->string('KĐQ', 1000)->nullable()->comment('Quyền không độc quyền');
            $table->string('HT', 1000)->nullable()->comment('Hạ tầng phát sóng');
            $table->string('country_ps', 1000)->nullable()->comment('Nước phát sóng');
            $table->string('country_note_ps', 1000)->nullable()->comment('Ghi chú nước phát sóng');
            $table->string('revenu_type', 1000)->nullable()->comment('Loại doanh thu');
            $table->string('money', 1000)->nullable()->comment('Giá tiền đã VAT');
            $table->string('moneyVAT', 1000)->nullable()->comment('Giá tiền chưa VAT');
            $table->date('start_time')->nullable()->comment('Thời gian bắt đầu');
            $table->date('end_time')->nullable()->comment('Thời hạn bản quyền');
            $table->date('date_sign')->nullable()->comment('Ngày tháng ký hợp đồng');
            $table->String('note', 1000)->nullable()->comment('Ghi chú');
            $table->integer('status')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contract_film_sale_uploads');
    }
}
