<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmListUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('film_list_uploads', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('admin_id')->nullable()->comment('Người tạo');
            
            $table->text('STT', 1000)->nullable();
            $table->string('name',1000)->nullable()->comment('Tên gốc');
            $table->string('vn_name',1000)->nullable()->comment('Tên tiếng Việt');
            $table->string('year_create',1000)->nullable()->comment('Năm sản xuất');
            $table->bigInteger('box_office_revenue')->nullable()->comment('Doanh thu phòng vé');
            $table->string('permission',1000)->nullable()->comment('Quyền');
            $table->string('category_film',1000)->nullable()->comment('Thể loại');
            $table->string('actor_name',1000)->nullable()->comment('Diễn viên');
            $table->string('director',1000)->nullable()->comment('Đạo diễn');
            $table->string('count_minute',1000)->nullable()->comment('Thời lượng');     
            $table->string('note',1000)->nullable()->comment('ghi chú');
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('film_list_uploads');
    }
}
