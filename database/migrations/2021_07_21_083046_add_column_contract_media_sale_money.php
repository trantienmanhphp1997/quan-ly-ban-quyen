<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnContractMediaSaleMoney extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contract_media_sale_money', function(Blueprint $table)
        {
            $table->string('note_film', 500)->nullable()->comment('Ghi chú phim');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contract_media_sale_money', function($table)
        {
            $table->dropColumn('note_film');
        });
    }
}
