<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmCountryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('film_country', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->comment('Lưu quan hệ quốc gia được phép kinh doanh');
			$table->bigInteger('country_id')->nullable()->comment('id country');
			$table->bigInteger('media_id')->nullable()->comment('lưu film id');
			$table->bigInteger('type')->nullable()->default(1)->comment('1: ban, 2: mua');
			$table->bigInteger('contract_id')->nullable()->comment('map voi hop dong');
			$table->bigInteger('sale_money_id')->nullable()->comment('map voi contract_media_sale_money');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('film_country');
	}

}
