<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MediaSellBuy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medias_sell_buy', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('media_id')->comment('id của phim hoặc nhạc');
            $table->string('name')->nullable()->comment('tên phim, nhạc');
            $table->bigInteger('user_id')->comment('id của người dùng');
            $table->tinyInteger('type')->comment('1: mua, 2: bán');
            $table->tinyInteger('category_id')->comment('1: nhạc, 2: phim');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medias_sell_buy');
    }
}
