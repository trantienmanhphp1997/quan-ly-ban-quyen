<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColoumnNote3000ToMusicUpload extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('music_upload', function (Blueprint $table) {
            $table->dropColumn('note');
        });
        Schema::table('music_upload', function (Blueprint $table) {
            $table->text('note', 3000)->nullable();  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('music_upload', function (Blueprint $table) {
            //
        });
    }
}
