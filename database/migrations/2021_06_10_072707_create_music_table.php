<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMusicTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('music', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->string('name', 1000)->nullable()->comment('TÊN BÀI HÁT (có dấu)');
			$table->string('singer', 1000)->nullable()->comment('CA SĨ THỂ HIỆN');
			$table->string('musician', 1000)->nullable()->comment('NHẠC SĨ');
			$table->boolean('cho_docquyen')->nullable();
			$table->boolean('cho_tacquyen')->nullable();
			$table->boolean('so_docquyen')->nullable();
			$table->boolean('so_tacquyen')->nullable();
			$table->boolean('sale_permission')->nullable();
			$table->string('ghi_am', 1000)->nullable();
			$table->string('tac_gia', 1000)->nullable();
			$table->string('contract_cps', 1000)->nullable()->comment('Số Phụ lục HĐ (Số HĐ của Viettel và CPs)');
			$table->string('contract_number1', 1000)->nullable()->comment('Số HĐ Quyền liên quan (biểu diễn, ghi âm)');
			$table->string('contract_number2', 1000)->nullable()->comment('Số Phụ lục HĐ Quyền liên quan (biểu diễn, ghi âm)');
			$table->string('contract_number3', 1000)->nullable()->comment('Số thứ tự trong PL Quyền liên quan');
			$table->string('contract_number4', 1000)->nullable()->comment('Số HĐ Quyền tác giả');
			$table->string('contract_number5', 1000)->nullable()->comment('Số Phụ lục HĐ Quyền tác giả');
			$table->string('contract_number6', 1000)->nullable()->comment('Số thứ tự trong PL Quyền Tác giả');
			$table->date('date_live')->nullable()->comment('Thời gian phát hành');
			$table->date('date_end')->nullable()->comment('Thời hạn bản quyền');
			$table->boolean('type_audio')->nullable();
			$table->boolean('type_MV')->nullable();
			$table->bigInteger('money')->nullable()->comment('Giá tiền (Đối với HĐ Mua bản quyền và sản xuất)');
			$table->string('note', 2000)->nullable();
			$table->string('music_id', 1000)->nullable();
			$table->bigInteger('contract_id')->nullable()->comment('map voi contract music');
			$table->bigInteger('admin_id')->nullable();
			$table->timestamps(6);
			$table->string('album', 1000)->nullable();
			$table->integer('type_fee_id')->default(1)->comment('map với type_fee( có thẻ dùng contact)');
			$table->date('start_time')->nullable()->comment('Thời hạn bản quyền');
			$table->date('end_time')->nullable()->comment('Thời hạn bản quyền');
			$table->smallInteger('warning_status')->nullable()->default(0)->comment('=1 turn off cảnh báo');
			// $table->index(['name','singer','musician'], 'search_name');
			// $table->unique(['name','contract_id'], 'index_name');
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('music');
	}

}
