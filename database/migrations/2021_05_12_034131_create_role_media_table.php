<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoleMediaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('role_media', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 50)->nullable();
			$table->string('code', 50)->nullable();
			$table->bigInteger('parent_id')->nullable()->comment('quyền cha trong bảng role_media');
			$table->bigInteger('categori_id')->nullable()->comment('map với loại quyền phim hay nhạc trong bảng cateogory');
			$table->integer('type')->nullable()->default(0)->comment('1: độc quyền; 2: không độc quyền; default 0');
			$table->bigInteger('admin_id')->nullable();
			$table->bigInteger('level')->nullable('=1 => hạ tầng');
		    $table->bigInteger('country_id')->nullable('nước có quyền này; = 0 => tất cả các nước');
            $table->string('nuoc', 200)->nullable()->comment('nước phát sóng');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('role_media');
	}

}
