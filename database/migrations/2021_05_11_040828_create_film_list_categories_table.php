<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmListCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('film_list_categories', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('id_film_list')->nullable()->comment('ID film_lists');
            $table->bigInteger('id_category_film')->nullable()->comment('ID category_films');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('film_list_categories');
    }
}
