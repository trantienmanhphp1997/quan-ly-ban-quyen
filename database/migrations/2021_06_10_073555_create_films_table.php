<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('films', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->string('vn_name', 200)->nullable()->comment('Ten tieng viet');
			$table->string('product_name', 200)->nullable();
			$table->string('actor_name', 200)->nullable()->comment('Tên diễn viên chính');
			$table->string('country', 50)->nullable()->comment('Quốc gia sản xuất');
			$table->integer('year_create')->nullable()->comment('Năm sản xuất');
			$table->dateTime('date_license_start')->nullable()->comment('Bản quyền từ ngày');
			$table->dateTime('date_license_end')->nullable()->comment('Bản quyền đến ngày');
			$table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
			$table->text('action_log')->nullable();
			$table->boolean('status')->nullable()->default(1)->comment('trang thai');
			$table->string('director', 200)->nullable()->comment('dao dien phim');
			$table->text('note')->nullable()->comment('Mo ta chung');
			$table->bigInteger('contract_id')->nullable()->comment('Hop dong');
			$table->date('year_live')->nullable()->comment('nam phat song');
			$table->string('rating', 10)->nullable()->comment('rating tai nuoc san xuat');
			$table->integer('license_fee')->nullable()->comment('Chi phi bản quyền');
			$table->integer('partner_fee')->nullable()->comment('Chi phi hau ki');
			$table->integer('other_fee')->nullable()->comment('Chi Phi kiem duyet');
			$table->integer('total_fee')->nullable()->comment('Tong chi phi');
			$table->timestamps();
			$table->string('count_live')->nullable()->comment('So nuoc phat song');
			$table->string('count_country')->nullable();
			$table->date('date_tv')->nullable()->comment('Ngay dau tien phat song tren truyen hinh');
			$table->date('date_vod')->nullable()->comment('Ngay dau tien phat song tren VOD');
			$table->string('category_id', 1000)->nullable()->comment('Thể Loại');
			$table->string('count_name', 1000)->nullable()->comment('Số đầu phim');
			$table->string('count_tap', 1000)->nullable()->comment('Số tập phim');
			$table->string('count_hour', 1000)->nullable()->comment('Số giờ phim');
			$table->string('partner_name', 1000)->nullable()->comment('Đối tác cung cấp');
			$table->string('permissin_contract', 1000)->nullable()->comment('Quyền hạn sử dụng');
			$table->string('category_name', 1000)->nullable()->comment('Tên phim');
			$table->date('start_time')->nullable()->comment('Thời hạn bản quyền');
			$table->date('end_time')->nullable()->comment('Thời hạn bản quyền');
			$table->string('nuoc', 1000)->nullable()->comment('Nước phát sóng');
			$table->string('addition_note', 1000)->nullable()->comment('Ghi chú');

			$table->smallInteger('warning_status')->nullable()->default(0)->comment('=1 turn off cảnh báo');
			//$table->index(['vn_name','product_name','actor_name'], 'search');
			$table->unique(['vn_name','contract_id'], 'index_name');
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('films');
	}

}
