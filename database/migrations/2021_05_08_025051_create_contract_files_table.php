<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractFilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contract_files', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->comment('danh sach file dinh kem voi hop dong, phim. media');
			$table->string('name', 1000)->nullable()->comment('Ten hop dong, to trinh');
			$table->date('date_sign')->nullable()->comment('Ngay ki');
			$table->integer('contract_id')->nullable()->comment('map voi hop dong');
			$table->string('note', 1000)->nullable()->comment('Ghi chu noi dung');
			$table->timestamps();
			$table->bigInteger('admin_id')->nullable();
			$table->string('link', 1000)->nullable()->comment('link lưu trữ file');
			$table->integer('categori_id')->nullable()->default(0)->comment('0: hợp đồng, music, nhạc, ... thì map với categori');
			$table->integer('media_id')->nullable()->default(0)->comment('map với media id tương ứng(phim, nhac, ..)');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contract_files');
	}

}
