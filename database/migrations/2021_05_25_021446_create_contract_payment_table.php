<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractPaymentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contract_payment', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->comment('Lưu thông tin tiến độ thanh toán hợp đồng');
			$table->bigInteger('contract_id')->nullable();
			$table->bigInteger('money');
			$table->date('date_pay')->nullable();
			$table->string('note', 1000)->nullable();
			$table->bigInteger('admin_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contract_payment');
	}

}
