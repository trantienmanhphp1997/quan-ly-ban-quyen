<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediaFilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('media_files', function(Blueprint $table)
		{
			$table->integer('id', true)->comment('luu tru dia chi file');
			$table->string('url', 200)->nullable();
			$table->bigInteger('media_id')->nullable();
			$table->boolean('type')->nullable()->comment('1: film, 2: nhac');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('media_files');
	}

}
