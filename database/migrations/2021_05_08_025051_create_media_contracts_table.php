<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediaContractsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('media_contracts', function(Blueprint $table)
		{
			$table->integer('id', true)->comment('luu nhung file media gan voi hop dong');
			$table->integer('media_id')->nullable();
			$table->integer('contract_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('media_contracts');
	}

}
