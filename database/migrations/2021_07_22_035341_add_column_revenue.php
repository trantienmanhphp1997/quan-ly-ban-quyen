<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnRevenue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('revenue', function(Blueprint $table)
        {
            $table->bigInteger('moneyVAT')->nullable()->comment('Giá tiền chưa VAT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('revenue', function($table)
        {
            $table->dropColumn('moneyVAT');
        });
    }
}
