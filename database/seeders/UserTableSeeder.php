<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
class UserTableSeeder extends Seeder
{

public function run()
	{
        $user = User::updateOrCreate([
            'username' => 'admin_qlbq',
            'email' => '',
            'password' => Hash::make('qlbqa@A1234'), // password
        ]);
        $user = User::updateOrCreate([
            'username' => 'admin',
            'email' => 'admin@admin.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'department_code' => config("common.departments.production"),
            "is_manager" => 1
        ]);
        $role = Role::updateOrCreate(['name' => 'administrator', 'note'=>'Quyền quản trị' ]);

        $permissions = Permission::pluck('id','id')->all();

        $role->syncPermissions($permissions);

        $user->assignRole([$role->id]);
	}
}
