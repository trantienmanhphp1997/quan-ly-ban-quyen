<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class TypeFeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_fee')->insert([
            [
                'id' =>'1',
                'name' => "Hợp đồng chia sẻ",
                'name_unique' => "share",
            ],
            [
                'id' =>'2',
                'name' => "Hợp đồng trọn gói",
                'name_unique' => 'doc_quyen',
            ],
        ]);
       
    }
}
