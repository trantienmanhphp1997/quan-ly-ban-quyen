<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use DB;

class MediaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $faker = Faker::create();

        // $limit = 100;

        // for ($i = 0; $i < $limit; $i++) {
        //     DB::table('music')->insert([
        //         'name' => $faker->unique()->name,
        //         'singer' => $faker->unique()->name,
        //         'contract_number1' => $faker->unique()->phoneNumber,
        //     ]);
        // }
        $faker = Faker::create();
        $limit = 1000;
        for ($i = 0; $i < $limit; $i++) {
            DB::table('music')->insert([
                'name' => $faker->unique()->name,
                'singer' => $faker->unique()->name,
                'contract_number1' => $faker->unique()->phoneNumber,
            ]);
        }
        DB::table('categories')->insert([
            [
                'id' => 1,
                'name' => "Nhạc",
            ],
            [
                'id' => 2,
                'name' => "Phim",
            ]
        ]);


    }
}
