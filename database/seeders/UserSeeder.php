<?php

namespace Database\Seeders;

use App\Models\Department;
use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $numberOfUsers = 50;
        $usersIds = array();
        $faker = Faker::create();
        $departments = Department::with(["roles"])->get();
        $departmentIds = $departments->map(function ($department) {
            return $department->id;
        });
        $departmentRoles = $departments->map(function ($department) {
            return $department->roles[0];
        });
        $departmentRoles = $departmentIds->combine($departmentRoles);
        $departmentCodes = array_values(config("common.departments"));
        $user = User::create([
            'username' => 'admin',
            'email' => 'admin@admin.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'department_code' => config("common.departments.production"),
            "is_manager" => 1
        ])->assignRole($departmentRoles[1], "super admin");
        Role::findByName("super admin")->givePermissionTo(Permission::all()->pluck('name')->toArray());
    }
}
