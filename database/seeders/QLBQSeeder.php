<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class QLBQSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('films')->insert([
    		[
    			'vn_name'=>'Trò chơi tình yêu',
    			'actor_name'=>'Nguyễn Minh Tuấn',
    			'year_create'=>'2015',
    			'date_license_start'=>'2017-07-01',
    			'date_license_end'=>'2021-02-01',
    		],
    		[
    			'vn_name'=>'Tây du ký',
    			'actor_name'=>'Lục tiểu linh đồng, Ngô Thừa Ân',
    			'year_create'=>'2015',
    			'date_license_start'=>'2017-07-01',
    			'date_license_end'=>'2021-02-01',
    		],
    		[
    			'vn_name'=>'Trò chơi vương quyền',
    			'actor_name'=>'Lưu Khải An, Triệu Tư Dĩnh',
    			'year_create'=>'2015',
    			'date_license_start'=>'2017-07-01',
    			'date_license_end'=>'2021-02-01',
    		],
    		[
    			'vn_name'=>'Pokemon',
    			'actor_name'=>'Pikachu',
    			'year_create'=>'2015',
    			'date_license_start'=>'2017-07-01',
    			'date_license_end'=>'2021-02-01',
    		],
    	]);
    }
}
