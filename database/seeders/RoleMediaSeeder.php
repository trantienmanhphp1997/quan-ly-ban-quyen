<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class RoleMediaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role_media')->insert([
            [
                'code' =>'TV',
                'categori_id' => 2,
                'type' => 1,
                'level' => 0,
            ],
            [
                'code' =>'VOD',
                'categori_id' => 2,
                'type' => 1,
                'level' => 0,
            ],
            [
                'code' =>'TV+VOD',
                'categori_id' => 2,
                'type' => 1,
                'level' => 0,
            ],
            [
                'code' =>'TV',
                'categori_id' => 2,
                'type' => 2,
                'level' => 0,
            ],
            [
                'code' =>'VOD',
                'categori_id' => 2,
                'type' => 2,
                'level' => 0,
            ],

            [
                'code' =>'TV+VOD',
                'categori_id' => 2,
                'type' => 2,
                'level' => 0,
            ],
        ]);

        DB::table('role_media')->insert([
            [
                'code' =>'SVOD',
                'parent_id' => DB::table('role_media')->where('code', '=', 'VOD')->where('type', '=', 1)->value('id'),
                'categori_id' => 2,
                'type' => 1,
                'level' => 0,
            ],
            [
                'code' =>'TVOD',
                'parent_id' => DB::table('role_media')->where('code', '=', 'VOD')->where('type', '=', 1)->value('id'),
                'categori_id' => 2,
                'type' => 1,
                'level' => 0,
            ],
            [
                'code' =>'AVOD',
                'parent_id' => DB::table('role_media')->where('code', '=', 'VOD')->where('type', '=', 1)->value('id'),
                'categori_id' => 2,
                'type' => 1,
                'level' => 0,
            ],
            [
                'code' =>'SVOD',
                'parent_id' => DB::table('role_media')->where('code', '=', 'VOD')->where('type', '=', 2)->value('id'),
                'categori_id' => 2,
                'type' => 2,
                'level' => 0,
            ],
            [
                'code' =>'TVOD',
                'parent_id' => DB::table('role_media')->where('code', '=', 'VOD')->where('type', '=', 2)->value('id'),
                'categori_id' => 2,
                'type' => 2,
                'level' => 0,
            ],
            [
                'code' =>'AVOD',
                'parent_id' => DB::table('role_media')->where('code', '=', 'VOD')->where('type', '=', 2)->value('id'),
                'categori_id' => 2,
                'type' => 2,
                'level' => 0,
            ],
        ]);

        DB::table('revenue_type')->insert([
            [
                'id'=> 1,
                'name' =>'VOD',
                'parent_id' =>0,
            ],
            [
                'id'=> 2,
                'name' =>'TH',
                'parent_id' =>0,
            ],
        ]);

        DB::table('revenue_type')->insert([
            [
                'id'=> 3,
                'name' =>'Youtube',
                'parent_id' => DB::table('revenue_type')->where('name', '=', 'VOD')->value('id'),
            ],
            [
                'id'=> 4,
                'name' =>'VOD nước ngoài',
                'parent_id' => DB::table('revenue_type')->where('name', '=', 'VOD')->value('id'),
            ],
            [
                'id'=> 5,
                'name' =>'VOD nội bộ',
                'parent_id' => DB::table('revenue_type')->where('name', '=', 'VOD')->value('id'),
            ],
            [
                'id'=> 6,
                'name' =>'VOD ngoài',
                'parent_id' => DB::table('revenue_type')->where('name', '=', 'VOD')->value('id'),
            ],
            [
                'id'=> 7,
                'name' =>'TH nội bộ',
                'parent_id' => DB::table('revenue_type')->where('name', '=', 'TH')->value('id'),
            ],
            [
                'id'=> 8,
                'name' =>'TH ngoài',
                'parent_id' => DB::table('revenue_type')->where('name', '=', 'TH')->value('id'),
            ],

        ]);

    }
}
