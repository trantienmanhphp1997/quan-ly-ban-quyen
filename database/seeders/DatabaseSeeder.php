<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            PermissionTableSeeder::class,
            UserTableSeeder::class,
            QLBQSeeder::class,
            MediaSeeder::class,
            RoleMediaSeeder::class,
            TypeFeeSeeder::class,
            RoleSeeder::class,
            CountrySeeder::class,
        ]);
    }
}

