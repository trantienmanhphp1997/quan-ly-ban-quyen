<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Country;
use DB;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $data = require_once(database_path('raw/CountryData.php'));
        foreach($data as $value){
            $country = Country::where('name',$value['name'])->first();
            if($country){
                $country->code = $value['code'];
                $country->save();
            }
            else {
                Country::create($value);
            }
        }
    }
}
