<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([

            [
                'name' =>'contract-viettel-buy-list',
                'guard_name' => 'web',
                'note' => 'Hợp đồng mua',
            ],
            [
                'name' =>'contract-viettel-buy-create-edit',
                'guard_name' => 'web',
                'note' => 'Hợp đồng mua - thêm mới - sửa',
            ],
            [
                'name' =>'contract-viettel-buy-delete',
                'guard_name' => 'web',
                'note' => 'Hợp đồng mua - xóa',
            ],
            [
                'name' =>'contract-viettel-sale-list',
                'guard_name' => 'web',
                'note' => 'Hợp đồng bán',
            ],
            [
                'name' =>'contract-viettel-sale-create-edit',
                'guard_name' => 'web',
                'note' => 'Hợp đồng bán - thêm mới - sửa',
            ],
            [
                'name' =>'contract-viettel-sale-delete',
                'guard_name' => 'web',
                'note' => 'Hợp đồng bán - xóa',
            ],
            [
                'name' =>'contract-partner-list',
                'guard_name' => 'web',
                'note' => 'Hợp đồng đối tác',
            ],
            [
                'name' =>'contract-partner-create-edit',
                'guard_name' => 'web',
                'note' => 'Hợp đồng đối tác - thêm mới - sửa',
            ],      
            [
                'name' =>'contract-partner-delete',
                'guard_name' => 'web',
                'note' => 'Hợp đồng đối tác - xóa',
            ],
            [
                'name' =>'contract-category-list',
                'guard_name' => 'web',
                'note' => 'Loại hợp đồng',
            ],
            [
                'name' =>'contract-category-create-edit',
                'guard_name' => 'web',
                'note' => 'Loại hợp đồng - thêm mới - sửa',
            ],
            [
                'name' =>'contract-category-delete',
                'guard_name' => 'web',
                'note' => 'Loại hợp đồng - xóa',
            ],
            [
                'name' =>'contract-file-list',
                'guard_name' => 'web',
                'note' => 'Danh sách giấy tờ',
            ],

            [
                'name' =>'partner-list',
                'guard_name' => 'web',
                'note' => 'Danh sách đối tác',
            ],
            [
                'name' =>'partner-create-edit',
                'guard_name' => 'web',
                'note' => 'Danh sách đối tác - thêm mới - sửa',
            ],
            [
                'name' =>'partner-delete',
                'guard_name' => 'web',
                'note' => 'Danh sách đối tác - xóa',
            ],
            [
                'name' =>'list-employee-list',
                'guard_name' => 'web',
                'note' => 'Nhân viên phụ trách hợp đồng',
            ],
            [
                'name' =>'list-employee-create-edit',
                'guard_name' => 'web',
                'note' => 'Nhân viên phụ trách hợp đồng - thêm mới - sửa',
            ],
            [
                'name' =>'list-employee-delete',
                'guard_name' => 'web',
                'note' => 'Nhân viên phụ trách hợp đồng - xóa',
            ],

            [
                'name' =>'music-list',
                'guard_name' => 'web',
                'note' => 'Danh sách nhạc',
            ],
            [
                'name' =>'music-create-edit',
                'guard_name' => 'web',
                'note' => 'Danh sách nhạc - thêm mới - sửa',
            ],
            [
                'name' =>'music-delete',
                'guard_name' => 'web',
                'note' => 'Danh sách nhạc - xóa',
            ],
            [
                'name' =>'film-list',
                'guard_name' => 'web',
                'note' => 'Danh sách phim',
            ],
            [
                'name' =>'film-create-edit',
                'guard_name' => 'web',
                'note' => 'Danh sách phim - thêm mới - sửa',
            ],

            [
                'name' =>'film-delete',
                'guard_name' => 'web',
                'note' => 'Danh sách phim - xóa',
            ],
            [
                'name' =>'phim-le-list',
                'guard_name' => 'web',
                'note' => 'Danh sách phim lẻ',
            ],
            [
                'name' =>'phim-le-create-edit',
                'guard_name' => 'web',
                'note' => 'Danh sách phim lẻ - thêm mới - sửa',
            ],
 
            [
                'name' =>'phim-le-delete',
                'guard_name' => 'web',
                'note' => 'Danh sách phim lẻ - xóa',
            ],
            [
                'name' =>'contract-media-sale-money-list',
                'guard_name' => 'web',
                'note' => 'Danh sách sản phẩm bán',
            ],

            [
                'name' =>'manage-list',
                'guard_name' => 'web',
                'note' => 'Quản trị tư liệu',
            ],
            [
                'name' =>'manage-create-edit',
                'guard_name' => 'web',
                'note' => 'Quản trị tư liệu - thêm mới - sửa',
            ],
      
            [
                'name' =>'manage-delete',
                'guard_name' => 'web',
                'note' => 'Quản trị tư liệu - xóa',
            ],
            [
                'name' =>'media-role-list',
                'guard_name' => 'web',
                'note' => 'Danh sách quyền hạn',
            ],
            [
                'name' =>'media-role-create-edit',
                'guard_name' => 'web',
                'note' => 'Danh sách quyền hạn - thêm mới - sửa',
            ],
        
            [
                'name' =>'media-role-delete',
                'guard_name' => 'web',
                'note' => 'Danh sách quyền hạn - xóa',
            ],
            [
                'name' =>'countries-list',
                'guard_name' => 'web',
                'note' => 'Danh sách quốc gia',
            ],
            [
                'name' =>'countries-create-edit',
                'guard_name' => 'web',
                'note' => 'Danh sách quốc gia - thêm mới - sửa',
            ],
 
            [
                'name' =>'countries-delete',
                'guard_name' => 'web',
                'note' => 'Danh sách quốc gia - xóa',
            ],
         
            [
                'name' =>'revenue-film-list',
                'guard_name' => 'web',
                'note' => 'Doanh thu phim',
            ],
            [
                'name' =>'revenue-music-list',
                'guard_name' => 'web',
                'note' => 'Doanh thu nhạc',
            ],

            [
                'name' =>'revenue-fee-list',
                'guard_name' => 'web',
                'note' => 'Danh sách chi phí nhạc',
            ],
            [
                'name' =>'revenue-fee-create-edit',
                'guard_name' => 'web',
                'note' => 'Danh sách chi phí nhạc - thêm mới - sửa',
            ],
            [
                'name' =>'revenue-fee-delete',
                'guard_name' => 'web',
                'note' => 'Danh sách chi phí nhạc - xóa',
            ],
            [
                'name' =>'revenue-type-list',
                'guard_name' => 'web',
                'note' => 'Loại doanh thu',
            ],
            [
                'name' =>'revenue-type-create-edit',
                'guard_name' => 'web',
                'note' => 'Loại doanh thu - thêm mới - sửa',
            ],
            [
                'name' =>'revenue-type-delete',
                'guard_name' => 'web',
                'note' => 'Loại doanh thu - xóa',
            ],
        ]);
    }
}
