<?php

namespace Database\Factories;

use App\Models\Music;
use Illuminate\Database\Eloquent\Factories\Factory;

class MusicFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Music::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'singer' => $this->faker->name,
            'musician' => $this->faker->name,
            'cho_docquyen' => $this->faker->randomElement(array('0','1')),
            'cho_tacquyen' =>  $this->faker->randomElement(array('0','1')),
            'so_docquyen' =>  $this->faker->randomElement(array('0','1')),
            'so_tacquyen' =>  $this->faker->randomElement(array('0','1')),
            'sale_permission' => $this->faker->name,
            'ghi_am' =>$this->faker->name,
            'tac_gia' => $this->faker->name,
            'contract_cps' => $this->faker->name,
            'contract_number1' => $this->faker->name,
            'contract_number2' =>$this->faker->name,
            'contract_number3'=>$this->faker->name,
            'contract_number4' =>$this->faker->name,
            'contract_number5' => $this->faker->name,
            'contract_number6' => $this->faker->name,
            'type_audio' => $this->faker->randomElement(array('0','1')),
            'type_MV' => $this->faker->randomElement(array('0','1')),
            'money' => $this->faker->randomElement(array('20000')),
            'note' => $this->faker->name,
            'music_id' =>$this->faker->randomElement(array(5,6,7)),
            'contract_id' => $this->faker->randomElement(array(5,6,7)),
            'admin_id' => $this->faker->randomElement(array('1')),
            // 'album' => $this->faker->name,
            // 'type_fee_id' => $this->faker->name,
            // 'start_time' => $this->faker->name,
            // 'end_time' => $this->faker->name,
        ];
    }
}
