<?php

namespace Database\Factories;

use App\Models\Film;
use Illuminate\Database\Eloquent\Factories\Factory;

class FilmFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Film::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'vn_name' => $this->faker->name,
            'actor_name' => $this->faker->name,
            'country' => $this->faker->name,
            'year_create' => $this->faker->randomElement(array('2015')),
            'date_license_start' => $this->faker->dateTimeThisMonth(),
            'date_license_end' => $this->faker->dateTimeThisMonth(),
            'admin_id' =>$this->faker->randomElement(array('1')),
            'director' => $this->faker->name,
            'note' =>$this->faker->randomElement(array('Chưa có note')),
            'contract_id' => $this->faker->randomElement(array('5','6')),
            'category_name' => $this->faker->name,
            'count_name' => $this->faker->name,
            'count_tap' =>$this->faker->name,
            'count_hour'=>$this->faker->name,
            'partner_name' =>$this->faker->name,
            'permissin_contract' => $this->faker->name,
            'time_end' => $this->faker->randomElement(array('1 năm','2 năm')),
        ];
    }
}
