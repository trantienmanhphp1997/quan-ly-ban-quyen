<?php
return [
    [
        'name' => 'Ấn Độ',
        'code' => 'IN',
    ],
    [
        'name' => 'Anh',
        'code' => 'EN',
    ],
    [
        'name' => 'Mỹ',
        'code' => 'US',
    ],
    [
        'name' => 'Canada',
        'code' => 'CA',
    ],
    [
        'name' => 'Hàn Quốc',
        'code' => 'KR',
    ],
    [
        'name' => 'Trung Quốc',
        'code' => 'CN',
    ],
    [
        'name' => 'Việt Nam',
        'code' => 'VN',
    ],
    [
        'name' => 'Nga',
        'code' => 'RU',
    ],
    [
        'name' => 'Nhật Bản',
        'code' => 'JP',
    ],
    [
        'name' => 'Thái Lan',
        'code' => 'TH',
    ],
    [
        'name' => 'Ai Cập',
        'code' => 'EG',
    ],
    [
        'name' => 'Nam Phi',
        'code' => 'ZA',
    ],
    [
        'name' => 'Argentina',
        'code' => 'AR',
    ],
    [
        'name' => 'Brazil',
        'code' => 'BR',
    ],
    [
        'name' => 'Chile',
        'code' => 'CL',
    ],
    [
        'name' => 'Cuba',
        'code' => 'CU',
    ],
    [
        'name' => 'Mexico',
        'code' => 'MX',
    ],
    [
        'name' => 'Uruguay',
        'code' => 'UY',
    ],
    [
        'name' => 'Afghanistan',
        'code' => 'AF',
    ],
    [
        'name' => 'Nepal',
        'code' => 'NP',
    ],
    [
        'name' => 'Pakistan',
        'code' => 'PK',
    ],
    [
        'name' => 'Campuchia',
        'code' => 'KH',
    ],
    [
        'name' => 'Indonesia',
        'code' => 'ID',
    ],
    [
        'name' => 'Malaysia',
        'code' => 'MY',
    ],
    [
        'name' => 'Myanma',
        'code' => 'MM',
    ],
    [
        'name' => 'Philippines',
        'code' => 'PH',
    ],
    [
        'name' => 'Singapore',
        'code' => 'SG',
    ],
    [
        'name' => 'Lào',
        'code' => 'LA',
    ],
    [
        'name' => 'Brunei',
        'code' => 'BN',
    ],
    [
        'name' => 'Đông Timor',
        'code' => 'TL',
    ],
    [
        'name' => 'Armenia',
        'code' => 'AM',
    ],
    [
        'name' => 'Iran',
        'code' => 'IR',
    ],
    [
        'name' => 'Irag',
        'code' => 'IQ',
    ],
    [
        'name' => 'Israel',
        'code' => 'IL',
    ],
    [
        'name' => 'Jordan',
        'code' => 'JO',
    ],
    [
        'name' => 'Ả Rập Xê Út',
        'code' => 'SA',
    ],
    [
        'name' => 'Syria',
        'code' => 'SY',
    ],
    [
        'name' => 'Áo',
        'code' => 'AT',
    ],
    [
        'name' => 'Bỉ',
        'code' => 'BE',
    ],
    [
        'name' => 'Croatia',
        'code' => 'HR',
    ],
    [
        'name' => 'Đan Mạch',
        'code' => 'DK',
    ],
    [
        'name' => 'Phần Lan',
        'code' => 'FI',
    ],
    [
        'name' => 'Pháp',
        'code' => 'FR',
    ],
    [
        'name' => 'Đức',
        'code' => 'DE',
    ],
    [
        'name' => 'Hy Lạp',
        'code' => 'GR',
    ],
    [
        'name' => 'Hungary',
        'code' => 'HU',
    ],
    [
        'name' => 'Ý',
        'code' => 'IT',
    ],
    [
        'name' => 'Hà Lan',
        'code' => 'NL',
    ],
    [
        'name' => 'Na uy',
        'code' => 'NO',
    ],
    [
        'name' => 'Bồ Đào Nha',
        'code' => 'PT',
    ],
    [
        'name' => 'Tây Ban Nha',
        'code' => 'ES',
    ],
    [
        'name' => 'Thụy Điển',
        'code' => 'SE',
    ],
    [
        'name' => 'Thổ Nhĩ Kỳ',
        'code' => 'TR',
    ],
    [
        'name' => 'Úc',
        'code' => 'AU',
    ],
    [
        'name' => 'New ZeaLand',
        'code' => 'NZ',
    ],
];
