<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\StatementController;
use App\Http\Controllers\Admin\Media\MusicController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', function () {
    return view('auth.login');
});

Auth::routes([
  'register' => false, // Registration Routes...
  'reset' => false, // Password Reset Routes...
  'verify' => false, // Email Verification Routes...
]);
Route::get('/home', 'HomeController@index');

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => ['HtmlMinifier']], function() {

    Route::get('tool/roles',['as'=>'tool.roles','uses'=>'Admin\Tool\CreateRoleController@index']);
    Route::get('tool/roles/create',['as'=>'tool.roles.create','uses'=>'Admin\Tool\CreateRoleController@create']);
    Route::post('tool/roles/create',['as'=>'tool.roles.store','uses'=>'Admin\Tool\CreateRoleController@store']);
    Route::get('tool/roles/{id}/edit',['as'=>'tool.roles.edit','uses'=>'Admin\Tool\CreateRoleController@edit']);
    Route::patch('tool/roles/{id}',['as'=>'tool.roles.update','uses'=>'Admin\Tool\CreateRoleController@update']);
    Route::delete('tool/roles/{id}',['as'=>'tool.roles.destroy','uses'=>'Admin\Tool\CreateRoleController@destroy']);
    Route::get('tool/createAdmin', ['as' => 'createAdmin.index', 'uses' => 'Admin\Tool\CreateAdminController@index']);
    Route::get('tool/createAdmin/{id}/show', ['as' => 'createAdmin.show', 'uses' => 'Admin\Tool\CreateAdminController@show']);
    Route::get('tool/createAdmin/{id}/mail', ['as' => 'createAdmin.mail', 'uses' => 'Admin\Tool\CreateAdminController@sendMail']);
    Route::get('tool/createAdmin/create', ['as' => 'createAdmin.create', 'uses' => 'Admin\Tool\CreateAdminController@create']);
    Route::post('tool/createAdmin/create',['as'=>'createAdmin.store','uses'=>'Admin\Tool\CreateAdminController@store']);
    Route::get('tool/createAdmin/{id}/edit',['as'=>'createAdmin.edit','uses'=>'Admin\Tool\CreateAdminController@edit']);
    Route::patch('tool/createAdmin/{id}',['as'=>'createAdmin.update','uses'=>'Admin\Tool\CreateAdminController@update']);
    Route::delete('tool/createAdmin/{id}',['as'=>'createAdmin.destroy','uses'=>'Admin\Tool\CreateAdminController@destroy']);
    Route::get('tool/createAdmin/update_role',['as'=>'createAdmin.update_role','uses'=>'Admin\Tool\CreateAdminController@updateRole']);

    Route::get('tool/action_log',['as'=>'action_log','uses'=>'Admin\Tool\ActionLogController@index']);
    Route::get('tool/action_log/download/{id}',['as'=>'action_log.download','uses'=>'Admin\Tool\ActionLogController@download']);
    //
    Route::get('management/revenue/fee/',['as'=>'type_fee.index', 'uses'=>'Admin\Tool\TypeFeeController@index']);
    Route::get('management/revenue/fee/create',['as'=>'type_fee.create', 'uses'=>'Admin\Tool\TypeFeeController@create']);
    Route::post('management/revenue/fee/store',['as'=>'type_fee.store', 'uses'=>'Admin\Tool\TypeFeeController@store']);
    Route::get('management/revenue/fee/{id}/edit',['as'=>'type_fee.edit', 'uses'=>'Admin\Tool\TypeFeeController@edit']);
    Route::post('management/revenue/fee/update/{id}',['as'=>'type_fee.update', 'uses'=>'Admin\Tool\TypeFeeController@update']);
    Route::post('management/revenue/fee/delete/{id}',['as'=>'type_fee.delete', 'uses'=>'Admin\Tool\TypeFeeController@delete']);
    });

Route::group(['middleware' => ['auth']], function() {

    Route::get('home', ['as' => 'home', 'uses' => 'Admin\HomeController@index']);

    Route::get('profile', ['as' => 'profile', 'uses' => 'Admin\AuthController@profile']);

    Route::get('password.edit', ['as' => 'password.edit', 'uses' => 'Admin\AuthController@editPassword']);

    Route::post('password.update', ['as' => 'password.update', 'uses' => 'Admin\AuthController@updatePassword']);



    Route::get('/contract-purchase', ['as' => 'contract-purchas', 'uses' => 'Admin\ContractPurchaseController@index']);

    // Contract purchase buy
    Route::get('contract/viettel-buy', ['as' => 'contract.viettel-buy', 'uses' => 'Admin\Contract\PurchaseContractController@index']);
    Route::get('contract/viettel-buy/create',['as'=>'contract.viettel-buy.create','uses'=>'Admin\Contract\PurchaseContractController@create']);
    Route::get('contract/viettel-buy/{id}/copy',['as'=>'contract.viettel-buy.copy','uses'=>'Admin\Contract\PurchaseContractController@copy']);
    Route::post('contract/viettel-buy/store',['as'=>'contract.viettel-buy.store','uses'=>'Admin\Contract\PurchaseContractController@store']);
    Route::get('contract/viettel-buy/{id}/edit',['as'=>'contract.viettel-buy.edit','uses'=>'Admin\Contract\PurchaseContractController@edit']);
    Route::patch('contract/viettel-buy/{id}',['as'=>'contract.viettel-buy.update','uses'=>'Admin\Contract\PurchaseContractController@update']);
    Route::delete('contract/viettel-buy/{id}',['as'=>'contract.viettel-buy.destroy','uses'=>'Admin\Contract\PurchaseContractController@destroy']);
    Route::post('contract/viettel-buy/{id}/storePaymentProcess',['as'=>'contract.viettel-buy.storePaymentProcess','uses'=>'Admin\Contract\PurchaseContractController@storePaymentProcess']);
    Route::delete('contract/viettel-buy/{id}/deletePaymentProcess',['as'=>'contract.viettel-buy.deletePaymentProcess','uses'=>'Admin\Contract\PurchaseContractController@deletePaymentProcess']);

    // Contract purchase sale
    Route::get('contract/viettel-sell', ['as' => 'contract.viettel-sell', 'uses' => 'Admin\Contract\PurchaseContractSaleController@index']);
    Route::get('contract/viettel-sell/create',['as'=>'contract.viettel-sell.create','uses'=>'Admin\Contract\PurchaseContractSaleController@create']);
    Route::post('contract/viettel-sell/create',['as'=>'contract.viettel-sell.store','uses'=>'Admin\Contract\PurchaseContractSaleController@store']);
    Route::get('contract/viettel-sell/{id}/edit',['as'=>'contract.viettel-sell.edit','uses'=>'Admin\Contract\PurchaseContractSaleController@edit']);
    Route::get('contract/viettel-sell/{id}/copy',['as'=>'contract.viettel-sell.copy','uses'=>'Admin\Contract\PurchaseContractSaleController@copy']);
    Route::patch('contract/viettel-sell/{id}',['as'=>'contract.viettel-sell.update','uses'=>'Admin\Contract\PurchaseContractSaleController@update']);
    //Route::post('contract/viettel-sell/delete/{id}',['as'=>'contract.viettel-sell.delete','uses'=>'Admin\Contract\PurchaseContractSaleController@delete']);
    Route::delete('contract/viettel-sell/{id}',['as'=>'contract.viettel-sell.destroy','uses'=>'Admin\Contract\PurchaseContractSaleController@destroy']);
    Route::post('contract/viettel-sell/{id}/storePaymentProcess',['as'=>'contract.viettel-sell.storePaymentProcess','uses'=> 'Admin\Contract\PurchaseContractSaleController@storePaymentProcess']);
    Route::post('contract/viettel-sell/{id}/save',['as'=>'contract.viettel-sell.save','uses'=>'Admin\Contract\PurchaseContractSaleController@save']);
    Route::post('contract/viettel-sell/saveFilmSale',['as'=>'contract.viettel-sell.saveFilmSale','uses'=>'Admin\Contract\PurchaseContractSaleController@saveFilmSale']);
    Route::delete('contract/viettel-sell/{id}/deletePaymentProcess',['as'=>'contract.viettel-sell.deletePaymentProcess','uses'=>'Admin\Contract\PurchaseContractSaleController@deletePaymentProcess']);
//
    Route::get('management/contract/viettel-partner', ['as' => 'contract.viettel-partner', 'uses' => 'Admin\Contract\PurchaseContractPartnerController@index']);
    Route::get('management/contract/viettel-partner/{id}/edit',['as'=>'contract.viettel-partner.edit','uses'=>'Admin\Contract\PurchaseContractPartnerController@edit']);
    Route::patch('management/contract/viettel-partner/{id}/update',['as'=>'contract.viettel-partner.update','uses'=>'Admin\Contract\PurchaseContractPartnerController@update']);



    Route::get('management/partner/list', ['as' => 'partner.index', 'uses' => 'Admin\Partner\PartnerController@index']);
    Route::get('management/partner/list/create', ['as' => 'partner.create', 'uses' => 'Admin\Partner\PartnerController@create']);
    Route::post('management/partner/store', ['as' => 'partner.store', 'uses' => 'Admin\Partner\PartnerController@store']);
    Route::get('management/partner/list/{id}/edit',['as'=>'partner.edit','uses'=>'Admin\Partner\PartnerController@edit']);
    Route::post('management/partner/update/{id}',['as'=>'partner.update','uses'=>'Admin\Partner\PartnerController@update']);
    Route::post('management/partner/delete/{id}',['as'=>'partner.delete','uses'=>'Admin\Partner\PartnerController@delete']);


    Route::get('upload/', ['as' => 'uploadFile', 'uses' => 'Admin\UploadController@upload']);
    Route::get('listEmployee/', ['as' => 'listEmployee', 'uses' => 'Admin\UploadController@listEmployee']);
    Route::post('upload/store', ['as' => 'storeFile', 'uses' => 'Admin\UploadController@store']);
    Route::get('listEmployee/export', ['as' => 'exportFile', 'uses' => 'Admin\UploadController@export']);


    Route::group([
        'prefix' => 'media',
        'as' => 'media.',
    ], function(){
        Route::resource('music', 'Admin\Media\MusicController')->parameters([
            'music' => 'music'
        ]);
        Route::post('/music/save', ['as' => 'music.upload', 'uses' => 'Admin\Media\MusicController@upload']);
        Route::post('/music/export', ['as' => 'music.export', 'uses' => 'Admin\Media\MusicController@export']);
        Route::get('/music/example', ['as' => 'music.example','uses' => 'Admin\Media\MusicController@example']);
        Route::get('/music/{id}/contractExtension', ['as' => 'music.contractExtension', 'uses' => 'Admin\Media\MusicController@contractExtension']);
        Route::put('/music/{id}/updateContract', ['as' => 'music.updateContract', 'uses' => 'Admin\Media\MusicController@updateContract']);
        Route::post('/music/buy', ['as' => 'music.sale', 'uses' => 'Admin\Media\MusicController@buy']);
        Route::post('/music/sale', ['as' => 'music.sale', 'uses' => 'Admin\Media\MusicController@sale']);
        Route::post('/music/getError', ['as' => 'music.getError', 'uses' => 'Admin\Media\MusicController@getError']);
    });
// film2
    Route::post('media/films/{id}/extention', ['as' => 'media.films.extension', 'uses' => 'Admin\Media\Film2Controller@extension']);
    Route::get('media/films/{id}/extention', ['as' => 'media.films.contractExtension', 'uses' => 'Admin\Media\Film2Controller@contractExtension']);
    Route::get('media/films', ['as' => 'media.films', 'uses' => 'Admin\Media\Film2Controller@index']);
    Route::post('media/films/save', ['as' => 'media.films.upload', 'uses' => 'Admin\Media\Film2Controller@upload3']);
    Route::post('media/films/{id}/saveListFilm', ['as' => 'media.films.uploadListFilm', 'uses' => 'Admin\Media\Film2Controller@uploadListFilm']);
    Route::get('media/films/export', ['as' => 'media.films.export', 'uses' => 'Admin\Media\Film2Controller@export']);
    Route::get('media/films/exportExampleFile', ['as' => 'media.films.exportExampleFile', 'uses' => 'Admin\Media\Film2Controller@exportExampleFile']);
    Route::get('media/films/exportExampleFileFilmList', ['as' => 'media.films.exportExampleFileFilmList', 'uses' => 'Admin\Media\Film2Controller@exportExampleFileFilmList']);
    Route::get('media/films/create', ['as' => 'media.films.create', 'uses' => 'Admin\Media\Film2Controller@create']);
    Route::post('media/films', ['as' => 'media.films.store', 'uses' => 'Admin\Media\Film2Controller@store']);
    Route::delete('media/films/delete/{id}', ['as' => 'media.films.delete', 'uses' => 'Admin\Media\Film2Controller@delete']);
    Route::delete('media/films/delete-all', ['as' => 'media.films.deleteAll', 'uses' => 'Admin\Media\Film2Controller@deleteAll']);
    Route::get('media/films/{id}/edit',['as'=>'media.films.edit','uses'=>'Admin\Media\Film2Controller@edit']);
    Route::get('media/films/{id}/show',['as'=>'media.films.show','uses'=>'Admin\Media\Film2Controller@show']);
    Route::post('media/films/{id}',['as'=>'media.films.update','uses'=>'Admin\Media\Film2Controller@update']);

    Route::get('media/phim-le',['as'=>'media.phim-le','uses'=>'Admin\Media\PhimLeController@index']);
    // Route::get('media/phim-le/create',['as'=>'media.phim-le.create','uses'=>'Admin\Media\PhimLeController@create']);
    // Route::post('media/phim-le', ['as' => 'media.phim-le.store', 'uses' => 'Admin\Media\PhimLeController@store']);
    Route::get('media/phim-le/{id}/edit',['as'=>'media.phim-le.edit','uses'=>'Admin\Media\PhimLeController@edit']);
    Route::post('media/phim-le/{id}',['as'=>'media.phim-le.update','uses'=>'Admin\Media\PhimLeController@update']);




    //Media Manage
    Route::get('media/manage', ['as' => 'media.manage', 'uses' => 'Admin\Media\ManageController@index']);
    Route::post('media/manage/save', ['as' => 'media.manage.upload', 'uses' => 'Admin\Media\ManageController@upload']);
    Route::get('media/manage/export', ['as' => 'media.manage.export', 'uses' => 'Admin\Media\ManageController@export']);
    Route::get('media/manage/exportExampleFile', ['as' => 'media.manage.exportExampleFile', 'uses' => 'Admin\Media\ManageController@exportExampleFile']);
    Route::get('media/manage/create',['as'=>'media.manage.create','uses'=>'Admin\Media\ManageController@create']);
    Route::post('media/manage/store', ['as' => 'media.manage.store', 'uses' => 'Admin\Media\ManageController@store']);
    Route::get('media/manage/{id}/edit',['as'=>'media.manage.edit','uses'=>'Admin\Media\ManageController@edit']);
    Route::post('media/manage/update/{id}',['as'=>'media.manage.update','uses'=>'Admin\Media\ManageController@update']);
    Route::post('media/manage/{id}/delete/',['as'=>'media.manage.delete', 'uses'=>'Admin\Media\ManageController@delete']);

    // Media Role
    Route::get('management/media/role',['as'=>'media.role.index', 'uses'=>'Admin\Media\RoleController@index']);
    Route::get('management/media/role/create',['as'=>'media.role.create', 'uses'=>'Admin\Media\RoleController@create']);
    Route::post('management/media/role/store',['as'=>'media.role.store', 'uses'=>'Admin\Media\RoleController@store']);
    Route::get('management/media/role/{id}/edit',['as'=>'media.role.edit', 'uses'=>'Admin\Media\RoleController@edit']);
    Route::post('management/media/role/update/{id}',['as'=>'media.role.update', 'uses'=>'Admin\Media\RoleController@update']);
    Route::post('management/media/role/delete/{id}',['as'=>'media.role.delete', 'uses'=>'Admin\Media\RoleController@delete']);

    // Route::get('media/music', ['as' => 'media.music', 'uses' => 'Admin\Media\MusicController@index']);
    // Route::get('media/music/create', ['as' => 'media.music.create', 'uses' => 'Admin\Media\MusicController@create']);
    // Route::post('media/music/store', ['as' => 'media.music.store', 'uses' => 'Admin\Media\MusicController@store']);
    // Route::get('media/music/{id}/edit',['as'=>'media.music.edit','uses'=>'Admin\Media\MusicController@edit']);
    // Route::delete('media/music/{id}/destroy',['as'=>'media.music.destroy','uses'=>'Admin\Media\MusicController@destroy']);
    // Route::post('media/music/upload', ['as' => 'media.music.upload', 'uses' => 'Admin\Media\MusicController@upload']);
    // Route::post('media/music/export', ['as' => 'media.music.export', 'uses' => 'Admin\Media\MusicController@export']);
    // Route::delete('media/music/{id}',['as'=>'media.music.destroy','uses'=>'Admin\Media\MusicController@destroy','middleware' => ['role:administrator']]);
    Route::post('media/music/{id}',['as'=>'media.music.update','uses'=>'Admin\Media\MusicController@update']);


// Category Contract
    Route::get('management/contract/category', ['as' => 'contract.category', 'uses' => 'Admin\Contract\CategoryController@index']);
    Route::get('management/contract/category/create', ['as' => 'contract.category.create', 'uses' => 'Admin\Contract\CategoryController@create']);
    Route::post('management/contract/category/store', ['as' => 'contract.category.store', 'uses' => 'Admin\Contract\CategoryController@store']);
    Route::get('management/contract/category/{id}/edit', ['as' => 'contract.category.edit', 'uses' => 'Admin\Contract\CategoryController@edit']);
    Route::patch('management/contract/category/{id}', ['as' => 'contract.category.update', 'uses' => 'Admin\Contract\CategoryController@update']);
    Route::delete('management/contract/category/{id}', ['as' => 'contract.category.destroy', 'uses' => 'Admin\Contract\CategoryController@destroy']);

// Contract File
    Route::get('management/contract/file', ['as' => 'contract.file', 'uses' => 'Admin\Contract\ContractFileController@index']);
    //Route::post('management/contract/file/{id}', ['as' => 'contract.contractFile.store', 'uses' => 'Admin\Contract\ContractFileController@store']);
    Route::delete('management/contract/file', ['as' => 'contract.contractFile.destroy', 'uses' => 'Admin\Contract\ContractFileController@destroy']);
    Route::get('management/contract/file/create',['as'=>'contract.file.create','uses'=>'Admin\Contract\ContractFileController@create']);
    Route::post('management/contract/file/', ['as' => 'contract.file.store', 'uses' => 'Admin\Contract\ContractFileController@store']);
    Route::get('management/contract/file/{id}/edit',['as'=>'contract.file.edit','uses'=>'Admin\Contract\ContractFileController@edit']);
    Route::post('management/contract/file/update/{id}',['as'=>'contract.file.update', 'uses'=>'Admin\Contract\ContractFileController@update']);
    Route::post('management/contract/file/delete/{id}',['as'=>'contract.file.delete', 'uses'=>'Admin\Contract\ContractFileController@delete']);
    //Route::get('businessPartner/listEmployee', ['as' => 'admin.businessPartner.listEmployee', 'uses' => 'Admin\BusinessPartnerController@listEmployee']);
    Route::get('management/partner/contractOfficer', ['as' => 'admin.businessPartner.listEmployee', 'uses' => 'Admin\BusinessPartnerController@listEmployee']);
    Route::get('management/contract/file/download/{id}',['as'=>'contract.file.download','uses'=>'Admin\Contract\ContractFileController@forDownload']);
    //revenue

    Route::get('revenue/music', ['as' => 'revenue.music', 'uses' => 'Admin\Revenue\RevenueMusicController@index']);
    Route::get('revenue/film', ['as' => 'revenue.film', 'uses' => 'Admin\Revenue\RevenueFilmController@index']);
    Route::post('revenue/film/delete/{id}', ['as' => 'revenue.film.delete', 'uses' => 'Admin\Revenue\RevenueFilmController@delete']);
    Route::post('revenue/music/delete/{id}', ['as' => 'revenue.music.delete', 'uses' => 'Admin\Revenue\RevenueMusicController@delete']);

    Route::get('management/revenue/type', ['as' => 'revenue_type.index', 'uses' => 'Admin\Revenue\RevenueTypeController@index']);
    Route::get('management/revenue/type/create', ['as' => 'revenue_type.create', 'uses' => 'Admin\Revenue\RevenueTypeController@create']);
    Route::post('management/revenue/type/store', ['as' => 'revenue_type.store', 'uses' => 'Admin\Revenue\RevenueTypeController@store']);
    Route::get('management/revenue/type/{id}/edit', ['as' => 'revenue_type.edit', 'uses' => 'Admin\Revenue\RevenueTypeController@edit']);
    Route::post('management/revenue/type/update/{id}', ['as' => 'revenue_type.update', 'uses' => 'Admin\Revenue\RevenueTypeController@update']);
    Route::post('management/revenue/type/delete/{id}', ['as' => 'revenue_type.delete', 'uses' => 'Admin\Revenue\RevenueTypeController@delete']);
     Route::get('/search-box', function () {
     return view('admin.media.music.searchbox');
     });

    Route::get('media/contract-media-sale-money', ['as' => 'media.contract-media-sale-money', 'uses' => 'Admin\Media\ContractMediaSaleMoneyController@index']);

    //Countries
    Route::get('management/media/countries',['as'=>'media.countries','uses'=>'Admin\Media\CountriesController@index']);
    Route::get('management/media/countries/create',['as'=>'media.countries.create','uses'=>'Admin\Media\CountriesController@create']);
    Route::post('management/media/countries', ['as' => 'media.countries.store', 'uses' => 'Admin\Media\CountriesController@store']);
    Route::get('management/media/countries/{id}/edit',['as'=>'media.countries.edit','uses'=>'Admin\Media\CountriesController@edit']);
    Route::post('management/media/countries/{id}',['as'=>'media.countries.update','uses'=>'Admin\Media\CountriesController@update']);
    Route::post('management/media/countries/delete/{id}',['as'=>'media.countries.delete', 'uses'=>'Admin\Media\CountriesController@delete']);
    Route::get('management/media/countries/export',['as'=>'media.countries.export', 'uses'=>'Admin\Media\CountriesController@export']);


    // theo dõi bản quyền export
    Route::get('copyright',['as'=>'copyright.index', 'uses'=>'Admin\Copyright\CopyrightController@index']);
    Route::get('copyright/sheet3',['as'=>'copyright.sheet3', 'uses'=>'Admin\Copyright\CopyrightController@sheet3']);
});
