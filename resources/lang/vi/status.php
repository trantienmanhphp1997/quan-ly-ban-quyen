<?php
return [
    "record_plan" => [
        "inactive" => "Nháp",
        "pending" => "Chờ xác nhận",
        "approved" => "Đã chốt",
        "in_progress" => "Đang diễn ra",
        "wrap" => "Đã đóng máy", //Đã đóng máy, chưa chốt xong giấy tờ
        "done" => "Đã hoàn thành",
        "cancel" => "Đã hủy"
    ],
    "record_plan_change_status" => [
        "inactive" => "Nháp",
        "pending" => "Chờ xác nhận",
        "approved" => "Chốt lịch",
        "in_progress" => "Bắt đầu sản xuất",
        "wrap" => "Đóng máy", //Đã đóng máy, chưa chốt xong giấy tờ
        "done" => "Hoàn thành",
        "cancel" => "Hủy"
    ],
    "propose" => [
        "pending" => "Chờ duyệt",
        "approved" => "Đã phân công",
        "rejected" => "Từ chối phân người",
        "cancel" => "Hủy"
    ],
    "propose_device" => [
        "pending" => "Chờ phê duyệt",
        "approved" => "Đã phân bổ",
        "delivered" => "Đã giao thiết bị",
        "returned" => "Đã trả thiết bị",
        "rejected" => "Từ chối",
        "duplicated" => "Đề xuất trùng lịch",
    ],
    "member" => [
        "inactive" => "Không hoạt động",
        "active" => "Hoạt động",
    ],
    "car" => [
        "all" => "Tất cả",
        "inactive" => "Không hoạt động",
        "active" => "Hoạt động",
        "borrowed" => "Đã cho mượn",
        "instock" => "Trong kho",
        "broken" => "Hỏng",
    ],
    "transportation" => [
        "ready" => "Đang sẵn sàng",
        "busy" => "Đang có chuyến",
        "broken" => "Bận",
    ],
    "device" => [
        "all" => "Tất cả",
        "inactive" => "Không hoạt động",
        "active" => "Hoạt động",
        "borrowed" => "Đã cho mượn",
        "instock" => "Trong kho",
        "broken" => "Hỏng",
        "request" => "Yêu cầu trả thiết bị"
    ],
    "device_category" => [
        "ready" => "Sẵn sàng",
    ],
    "assignments" => [
        "devices" => [
            "proposal" => "Đang đề xuất",
            "accept" => "Đồng ý",
            "cancel" => "Hủy"
        ],
        "members" => [
            "proposal" => "Đang đề xuất",
            "accept" => "Đồng ý",
            "cancel" => "Hủy"
        ]
    ],
    "stage" => [
        "active" => "Hoạt động",
        "inactive"=> "Không hoạt động"
    ]
];
