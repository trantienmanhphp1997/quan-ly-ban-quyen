<?php
return [
    'button' => [
        'view' => 'Xem',
        'show' => 'Chi tiết',
        'create' => 'Tạo mới',
        'delete' => 'Xóa',
        'bulk_delete' => 'Xóa',
        'bulk_restore' => 'Khôi phục',
        'block' => 'Khóa',
        'edit' => 'Chỉnh sửa',
        'update' => 'Cập nhật',
        'back' => 'Quay lại',
        'search' => 'Tìm kiếm',
        'upload' => 'Tải lên',
        'download' => 'Tải về',
        'download_template' => 'Tải File mẫu',
        'export' => 'Export',
        'import' => 'Import',
        'export_excel' => 'Export excel',
        'restore' => 'Phục hồi',
        'used' => 'Sử dụng',
        'send' => 'Gửi',
        'send_all' => 'Gửi tất cả',
        'resend' => 'Gửi lại tin nhắn',
        'save' => 'Lưu',
        'remove' => 'Xóa',
        'cancel' => 'Hủy',
        'ok' => 'OK',
        'submit' => 'Xác nhận',
        'confirm' => 'Xác nhận',
        'extend' => 'Mở rộng',
        'view_tasks' => 'Danh sách công việc',
        "assign" => "Phân công",
        "assign-device" => "Phân bổ thiết bị",
        "proposal" => "Đề xuất",
        "remove-assign" => "Bỏ phân công",
        "remove-assign-device" => "Bỏ thiết bị",
        "remove-proposal" => "Bỏ đề xuất",
        "device-proposal" => "Đề xuất thiết bị",
        "lending-device" => "Giao thiết bị",
        "retrieve-device" => "Nhận lại thiết bị",
        "assignment" => [
            "camera" => "Phân quay phim",
            "technical_camera" => "Phân kỹ thuật phòng quay",
            "reporter" => "Phân phóng viên",
            "car-transportation" => "Phân xe/lái xe",
            "device" => "Phân thiết bị"
        ],
        "lending" => "Cấp thiết bị",
        "report_broken" => "Báo hỏng",
        "choosefile" => "Chọn file ",
        "drag" => "hoặc kéo thả file vào đây",
        "upload" => "Tải lên",
        "login" => "Đăng nhập",
        "logout" => "Đăng xuất",
        "expiredLendingDayDevices" => "DS mượn quá hạn",
        "source" => "Nguồn lực"

    ],
    "error" => "Đã có lỗi xảy ra",
    'place_holder' => [
        'search' => 'Tìm kiếm',
        'enter_key_word' => 'Nhập từ khóa',
        'password' => 'Nhập thông tin Mật khẩu',
        'reason_report_broken' => 'Lý do báo hỏng'
    ],
    'message' => [
        'showing_entries' => 'Hiển thị danh sách từ :from Đến :to trong số :all bản ghi',
        'delete_success' => 'Xóa thành công :modelName.',
        'retore_success' => 'Khôi phục thành công :modelName.',
        'no_data' => 'Không có bản ghi nào',
        'uploading' => 'Đang tải lên',
        'no_plan' => 'Không có kế hoạch phù hợp',
        'no_assign' => 'Không được phân công'
    ],
    'confirm_message' => [
        'confirm_title'=>'Xác nhận',
        'are_you_sure_delete' => 'Bạn có chắc chắn muốn xóa ',
        'are_you_sure_unassign' => 'Bạn có chắc chắn muốn bỏ phân công ',
        'confirm_to_delete' => 'Đồng ý',
        'confirm_to_restore' => 'Đồng ý',
        'confirm_to_report_broken' => 'Đồng ý',
        "are_you_sure_report_broken" => 'Bạn có chắc chắn muốn báo hỏng',
        "are_you_sure_restore" => 'Bạn có chắc chắn muốn khôi phục',
        "are_you_sure_retrieve_device" => 'Bạn có chắc chắn muốn nhận lại thiết bị',
    ],
    "warning" => [
        "no_data" => "Không có bản ghi nào"
    ],
    'toast_message' => [
        'bulk_delete_nothing' => 'Bạn chưa chọn bản ghi nào để xóa',
        'bulk_restore_nothing' => 'Bạn chưa chọn bản ghi nào để khôi phục'
    ],
    "excel" => [
        "row" => "dòng",
        "column" => "cột"
    ],
    "tooltip"=>[
        "menu" =>[
            "access" => "Phân quyền truy cập",
            "function" => "Phân quyền chức năng",
            "plus" => "Mở rộng",
            "edit" => "Chỉnh sửa",
            "delete" => "Xóa"
        ]
    ],
    "transportation_involve_type" =>[
        "go_only" => "Chỉ dưa đi",
        "return_only" => "Chỉ đón về",
        "round_trip" => "Chỉ đưa đi và đón về",
        "along_with_trip" => "Đi cùng đoàn"
    ],
    "address_type" => [
        "urban" => "Nội thành",
        "interMunicipal" => "Liên tỉnh"
    ]
];
