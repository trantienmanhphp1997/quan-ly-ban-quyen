<?php
return [

    "car" => [
        "name" => [
            "required" => "Tên xe là thông tin bắt buộc",
            "maxlength" => "Tên xe quá dài"
        ],
        "model" => [
            "required" => "Tên model là thông tin bắt buộc",
            "maxlength" => "Tên model quá dài"
        ],
         "brand" => [
             "required" => "Tên model là thông tin bắt buộc",
             "maxlength" => "Tên model quá dài"
         ],
         "color" => [
                       "required" => "Tên model là thông tin bắt buộc",
                           "maxlength" => "Tên model quá dài"
                       ],
        "number_of_seats" => [
            "required" => "Số chỗ ngồi là thông tin bắt buộc",
            "maxlength" => "Số chỗ ngồi quá dài",
            "numberic" => "Số chỗ ngồi không hợp lệ"
        ],
        "license_plate" => [
            "required" => "Biển số xe là thông tin bắt buộc",
            "maxlength" => "Biển số xe quá dài",
            "unique" => "Biển số xe đã tồn tại",
            "not_exist" => "Biển số xe không tồn tại",
            "has_car" => "Lái xe đã có xe",
            "has_driver" => "Xe đã có tài xế phụ trách"
        ],
    ],
    "stage" => [
        "name" => [
            "required" => "Tên trường quay là thông tin bắt buộc",
            "max" => "Tên trường quay quá dài"
        ],
        "address" => [
            "required" => "Địa điểm trường quay là thông tin bắt buộc",
            "max" => "Địa điểm trường quay quá dài"
        ],
        "code" => [
            "required" => "Mã trường quay là thông tin bắt buộc",
            "max" => "Mã trường quay quá dài",
            "unique" => "Mã trường quay đã tồn tại"
        ],
    ],

    "contract" => [
        "title" => [
            "required" => "Tiêu đề hợp đồng là thông tin bắt buộc",
            "maxlength" => "Tiêu đề hợp đồng quá dài"
        ],
        "code" => [
            "required" => "Mã hợp đồng là thông tin bắt buộc",
            "maxlength" => "Mã hợp dồng quá dài",
            "unique" => "Mã đã tồn tại"
        ],
        "department_id" => [
            "required" => "Phòng ban là thông tin bắt buộc",
        ],
        "statement_id" => [
            "required" => "Tờ trình là thông tin bắt buộc",
        ],
    ],
    "department" => [
        "name" => [
            "required" => "Tên phòng ban là thông tin bắt buộc",
            "maxlength" => "Tên  phòng ban quá dài"
        ],
    ],
    "device_category" => [
        "name" => [
            "required" => "Tên danh mục thiết bị là thông tin bắt buộc",
            "maxlength" => "Tên danh mục thiết bị quá dài",
            "unique" => "Tên danh mục đã tồn tại"
        ],
        "display_code" => [
            "required" => "Mã danh mục thiết bị là thông tin bắt buộc",
            "maxlength" => "Mã danh mục thiết bị quá dài",
            "unique" => "Mã danh mục đã tồn tại"
        ],
    ],
    "device" => [
        "name" => [
            "required" => "Tên thiết bị là thông tin bắt buộc",
            "maxlength" => "Tên thiết bị quá dài"
        ],
        "device_category_id" => [
            "required" => "Danh mục thiết bị là thông tin bắt buộc",
        ],
        "category" => [
            "required" => "Loại thiết bị là bắt buộc"
        ],
        "serial" => [
            "required" => "Mã thiết bị là bắt buộc",
            "unique" => "Mã thiết bị đã tồn tại"
        ]
    ],
    "manager" => [
        "username" => [
            "required" => "Username là thông tin bắt buộc",
            "unique" => "Username đã tồn tại"
        ],
        "phone_number" => [
            "required" => "Số điện thoại là thông tin bắt buộc",
            "unique" => "Số điện thoại đã tồn tại"
        ],
        "full_name" => [
            "required" => "Tên quản lý là thông tin bắt buộc",
        ],

    ],
    "member" => [
        "full_name" => [
            "required" => "Tên nhân sự là thông tin bắt buộc",
            "maxlength" => "Tên nhân sự quá dài"
        ],
        "phone_number" => [
            "required" => "Số điện thoại là thông tin bắt buộc",
            "unique" => "Số điện thoại đã được sử dụng trong phòng :department."
        ],

    ],
    "outsource" => [
        "full_name" => [
            "required" => "Tên danh mục thiết bị là thông tin bắt buộc",
            "maxlength" => "Tên danh mục thiết bị quá dài"
        ],
        "phone_number" => [
            "required" => "Số điện thoại là thông tin bắt buộc",
            "unique" => "Số điện thoại đã tồn tại"
        ],

    ],
    "outsource_type" => [
        "name" => [
            "required" => "Tên loại thuê ngoài là thông tin bắt buộc",
            "maxlength" => "Tên loại thuê ngoài quá dài"
        ],
        "code" => [
            "unique" => "Mã đã tồn tại",
            "required" => "Mã là thông tin bắt buộc",
        ]

    ],
    "plan_info" => [
        "title" => [
            "required" => "Tiêu đề kế hoạch là thông tin bắt buộc",
            "maxlength" => "Tiêu đề kế hoạch quá dài"
        ],
        "code" => [
            "required" => "Mã kế hoạch là thông tin bắt buộc",
            "maxlength" => "Mã kế hoạch quá dài",
            "unique" => "Mã đã tồn tại"
        ],
        "department_id" => [
            "required" => "Phòng ban là thông tin bắt buộc",
        ],
        "year" => [
            "required" => "Năm là thông tin bắt buộc",
        ],
    ],
    "statement" => [
        "title" => [
            "required" => "Tiêu đề tờ trình là thông tin bắt buộc",
            "maxlength" => "Tiêu đề tờ trình quá dài"
        ],
        "code" => [
            "required" => "Mã tờ trình là thông tin bắt buộc",
            "maxlength" => "Mã tờ trình quá dài",
            "unique" => "Mã đã tồn tại"
        ],
        "department_id" => [
            "required" => "Phòng ban là thông tin bắt buộc",
        ],
    ],
    "record_plan" => [
        "name" => [
            "required" => "Tên lịch sản xuất là thông tin bắt buộc",
            "max" => "Tên kế hoạch quá dài",
            "unique" => "Tên lịch sản xuất đã được sử dụng"
        ],
        "address" => [
            "required" => "Địa chỉ sản xuất là thông tin bắt buộc",
        ],
        "start_time" => [
            "required" => "Thời gian bắt đầu lịch sản xuất là thông tin bắt buộc",
        ],
        "end_time" => [
            "required" => "Thời gian kết thúc lịch sản xuất là thông tin bắt buộc",
        ],
        "start_date" => [
             "required" => "Ngày bắt đầu lịch sản xuất là thông tin bắt buộc",
        ],
        "end_date" => [
              "required" => "Ngày kết thúc lịch sản xuất là thông tin bắt buộc",
        ],
        "department_code" => [
            "required" => "Phòng ban của lịch sản xuất là thông tin bắt buộc"
        ],
        "plan_id" => [
            "required" => "Kế hoạch của lịch sản xuất là thông tin bắt buộc"
        ],
        "recurring" => [
            "required" => "Bạn chưa chọn ngày lịch lặp lại"
        ]

    ],
    "email" => [
        "format" => "Email không đúng định dạng"
    ],
    "file" => [
        "required" => "Bạn chưa chọn file nào",
        "mimes" => "File không đúng định dạng"
    ]
];
