<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form method="POST" id="upload-form" enctype="multipart/form-data" action="{{route('revenue.sale.store')}}">
       @csrf
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Thêm Doanh thu</h5>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin: 10px;">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <lable class="col-md-3 col-form-label">Mã hợp đồng:</lable>
                                {!! Form::text('contract_id', null, array('placeholder' => 'mã hợp đồng','class' => 'col-md-8')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <lable class="col-md-3 col-form-label">Loại hợp đồng:</lable>
                                {!! Form::select('type_contract', [0=>'Vietel bán',1=>'Vietel mua'], null, array('class' => 'col-md-8')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <lable class="col-md-3 col-form-label">Mã sản phẩm media:</lable>
                                {!! Form::text('media_id', null, array('placeholder' => 'mã sản phẩm media','class' => 'col-md-8')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <lable class="col-md-3 col-form-label">Danh mục:</lable>
                                {!! Form::select('category_id', $categories,null, array('class' => 'col-md-8')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <lable class="col-md-3 col-form-label">Loại doanh thu:</lable>
                                {!! Form::select('type_id', $revenue_type,null, array('class' => 'col-md-8')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <lable class="col-md-3 col-form-label">Ngày ghi nhận:</lable>
                                {!! Form::text('date_sign',null, array('class' => 'col-md-8')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <lable class="col-md-3 col-form-label">Số tiền:</lable>
                                {!! Form::number('money', null, array('class' => 'col-md-8')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <lable class="col-md-3 col-form-label">Doanh thu từ:</lable>
                                {!! Form::select('type_revenue', [0=>'Ký hợp đồng', 1=>'Gia hạn'],null, array('class' => 'col-md-8')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="submit" class="btn btn-primary">Tạo</button>
                            <a class="btn btn-default" href="{{ route('revenue.sale') }}"> Bỏ qua</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

