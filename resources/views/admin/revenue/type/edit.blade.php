@extends('layouts.master')

@section('title')
    Sửa loại doanh thu
@endsection
@section('css')
    <style>
        .col-xs-12 ,.col-sm-12 ,.col-md-12{
            margin-bottom: 20px;
        }
        .table-input{
            margin-top:30px;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Sửa loại doanh thu</h2>
            </div>

        </div>
    </div>
    {!! Form::open(array('route' => ['revenue_type.update',$data->id],'method'=>'POST')) !!}
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong class="col-md-2">Tên loại doanh thu(<span class="text-danger">*</span>)</strong> 
                {!! Form::text('name', $data->name, array('placeholder' => 'Tên loại doanh thu','class' => 'col-md-4')) !!}
                @error('name')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
        @if($htmlOption)
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong class="col-md-2">Loại doanh thu cha</strong>
                {!! Form::select('parent_id', ['---Loại doanh thu cha---']+$htmlOption,$data->parent_id, ['class' => 'col-md-4',]) !!}
            </div>
        </div>
        @endif
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong class="col-md-2">Ghi chú</strong>
                {!! Form::text('note', $data->note, array('placeholder' => 'Ghi chú...','class' => 'col-md-4')) !!}
            </div>
        </div>
    </div>
    <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
            @include('layouts.partials.button._save')
            &nbsp; &nbsp; &nbsp;
            @include('layouts.partials.button._back')
        </div>
    </div>
    {!! Form::close() !!}
@endsection
