@extends('layouts.master')
@section('title')
    Danh sách loại doanh thu
@endsection
@section('content')
    <div class="page-header">
        <div class="row">
            <div class="col-sm-12">
                <div class="widget-box">
                    <div class="widget-header">
                        <h4 class="widget-title">Tìm kiếm</h4>

                        <span class="widget-toolbar">
                            <a href="#" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up"></i>
                            </a>
                        </span>
                    </div>

                    <div class="widget-body">
                        <div class="widget-main">
                            {!! Form::open(['method'=>'GET','route'=>'revenue_type.index','role'=>'search'])  !!}
                            <div class="row">
                                <div class="col-xs-4 col-sm-4">
                                    <label for="form-field-select-1">Nhập từ khóa:</label>
                                    <input class="form-control" name="keyword" placeholder="Nhập tên loại doanh thu..." type="text" value="{{request()->keyword}}" />
                                </div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-xs-6 col-sm-6">
                                    <button type="submit" class="btn btn-info btn-sm">
                                        <span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
                                        Tìm kiếm
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-sm-8">
            </div>
            <div class="col-sm-12">
                <div id="main-container"></div>
            </div>
        </div>
    </div><!-- /.page-header -->
    <div class="row">
        <div class="col-xs-12 form-group">
            <div class="pull-left">
                <h2>Danh sách loại doanh thu</h2>
            </div>
            <div class="pull-right">
                @include('layouts.partials.button._new')
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <div class="row">
                <div class="col-xs-12">
                    <table id="simple-table" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="hidden-480" style="text-align: center; color: black;">STT</th>
                            <th style="text-align: center; color: black;">Tên loại doanh thu</th>
                            <th style="text-align: center; color: black;">Loại doanh thu cha</th>
                            <th style="text-align: center; color: black;">Người tạo</th>
                            <th style="text-align: center; color: black;">Ghi chú</th>
                            <th style="text-align: center; color: black;" width="280px">Hành động</th>
                        </tr>
                        </thead>

                        <tbody>
                        @forelse ($data as $key => $rs)
                            <tr>
                                <td style="text-align: center;">{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</td>
                                <td>{!! boldTextSearchV2($rs->name,request('keyword')) !!}</td>
                                <td>{{ $rs->parent_name }}</td>
                                <td>{{ $rs->user_name }}</td>
                                <td>{{ $rs->note }}</td>
                                <td style="text-align: center;">

                                    @php $id = $rs->id; @endphp
                                    @include('layouts.partials.button._edit')
                                    @if($rs->id > 8)
                                        {!! Form::open(array('route'=>['revenue_type.delete',$rs->id],'method'=>'POST', 'style'=>'display:inline')) !!}
                                        @include('layouts.partials.button._deleteForm')
                                        {!! Form::close() !!}
                                    @endif
                                    
                                    <div class="modal fade" id="modal-form-delete-category" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Xóa quyền</h5>
                                                </div>
                                                <div class="modal-body">
                                                    Bạn có muốn xóa không? Thao tác này không thể phục hồi!
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Không</button>
                                                    <button type="button" class="btn btn-danger" onclick="document.getElementById('delete').click()">Xóa bỏ</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @empty
                                <tr>
                                    <td colspan="6" class="text-center"> Không có kết quả phù hợp</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                    @include('layouts.partials._paginate1')
                </div><!-- /.span -->
            </div><!-- /.row -->
        </div><!-- /.col -->
    </div><!-- /.row -->
@endsection
