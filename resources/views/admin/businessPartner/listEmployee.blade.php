@extends('layouts.master')
@section('title')
    Danh sách nhân viên
@endsection
@section('content')
    <div class="page-header">
        <div class="row">
            <div class="col-sm-12">
                <div class="widget-box">
                    <div class="widget-header">
                        <h4 class="widget-title">Tìm kiếm</h4>

                        <span class="widget-toolbar">
                            <a href="#" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up"></i>
                            </a>
                        </span>
                    </div>

                    <div class="widget-body">
                        <div class="widget-main">
                            {!! Form::open(array('method'=>'GET','role'=>'search','id'=>'form-search-QLBQ',))  !!}
                            <div class="row">
                                <div class="col-xs-4 col-sm-4">
                                    <label for="form-field-select-1">Tên nhân viên</label>
                                    <input class="form-control" name="searchName" type="text"
                                        value="{{request('searchName')}}" id='input_vn_name' autocomplete="off" placeholder='Tên nhân viên' />
                                </div>
                                <div class="col-xs-4 col-sm-4">
                                    <label for="form-field-select-1">Số hợp đồng</label>
                                    <input class="form-control" name="searchContractNumber" type="text"
                                        value="{{request('searchContractNumber')}}" id='input_contract_number' autocomplete="off" placeholder='Số hợp đồng'/>
                                </div>
                                <div class="col-xs-4 col-sm-4">
                                    <label for="form-field-select-1">Số điện thoại</label>
                                    <input class="form-control" name="searchPhone" type="text"
                                        value="{{request('searchPhone')}}" id='input_actor_name' autocomplete="off" placeholder='Số điện thoại' />
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                            <div class="col-xs-6 col-sm-6">
                                                <button type="submit" class="btn btn-info btn-sm">
                                                    <span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
                                                Tìm kiếm
                                                </button>
                                            </div>
                                    </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div><!-- /.page-header -->
    {{-- QLBQ --}}
    <table class="table mt-4 table-bordered table-striped table-hover">
        <h2>Danh sách nhân viên phụ trách hợp đồng</h2>
        <hr>
        <thead>
            <tr>
                <th scope="col">
                	<center style='color:black;'>STT </center>
                </th>
                <th scope="col">
                   <center style='color:black;'>Họ và tên</center>
                </th>
                <th scope="col">
                    <center style='color:black;'>Số điện thoại</center>
                </th>
                <th scope="col">
                   <center style='color:black;'>Email</center>
                </th>
                <th scope="col">
                   <center style='color:black;'>Số hợp đồng</center>
                </th>
                <th scope="col">
                   <center style='color:black;'>Loại hợp đồng</center>
                </th>
                <th scope="col">
                   <center style='color:black;'>Đối tác</center>
                </th>
                <th scope="col">
                   <center style='color:black;'>Thời gian bắt đầu</center>
                </th>
                <th scope="col">
                   <center style='color:black;'>Thời gian kết thúc</center>
                </th>
            </tr>
        </thead>
            @forelse($data as $key => $contract)
                <tr>
                    <th scope="row">
                    	<center>{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</center>
                    </th>
                    <td>{!! boldTextSearchV2($contract->user_name,request('searchName')) !!}</td>
                    <td>{!! boldTextSearchV2($contract->user_msisdn,request('searchPhone')) !!}</td>
                    <td>{{$contract->user_email}}</td>
                    <td>{!! boldTextSearchV2($contract->contract_number,request('searchContractNumber')) !!}</td>
                    <td>{{ ($contract->category)?$contract->category->name:'' }}</td>
                    <td>{{ ($contract->partner)?$contract->partner->name:'' }}</td>
                    <td><center>{{ reFormatDate($contract-> start_time,'d-m-Y') }}</center></td>
                    <td><center>{{ reFormatDate($contract-> end_time,'d-m-Y') }}</center></td>
                </tr>
            @empty
                <tr>
                    <td colspan="9" class="text-center"> Không có kết quả phù hợp</td>
                </tr>
            @endforelse
        </tbody>
    </table>
    @include('layouts.partials._paginate1')


@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/QLBQ.css')}}">

@endsection

@section('js')
    <script src="{{asset('js/QLBQ.js')}}"></script>
@endsection
