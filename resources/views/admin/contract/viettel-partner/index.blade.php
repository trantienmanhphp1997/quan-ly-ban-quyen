@extends('layouts.master')
@section('title')
    Quản lý hợp đồng đối tác
@endsection
@section('content')

    @livewire('contract.viettel-partner')

@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('css/QLBQ.css')}}">
@endsection
@section('js')
    <script src="{{asset('js/QLBQ.js')}}"></script>
@endsection
