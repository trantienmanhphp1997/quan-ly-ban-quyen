@extends('layouts.master')

@section('title')
    Sửa hợp đồng đối tác
@endsection

@section('content')

    <div class="main-container">
        <h2>Sửa hợp đồng đối tác</h2>
        {!! Form::model($data, ['method' => 'POST', 'id'=>'form_update_contract', 'class' => 'form-horizontal',  'autocomplete' => "off",'route' => ['contract.viettel-partner.update', $data->id]]) !!}
        <ul class="nav nav-tabs">
            <li class="@if(!Session::has('tab')||Session::get('tab')=='home') active  @endif"><a data-toggle="tab" href="#home">Thông tin hợp đồng</a></li>
            <li class="@if(Session::has('tab')&&Session::get('tab')=='menu1') active  @endif"><a data-toggle="tab" href="#menu1">Giấy tờ phê duyệt</a></li>
            <li style="float:right;">
                @include('layouts.partials.button._save')
                @include('layouts.partials.button._back')
            </li>
        </ul>
        @csrf
        @method('PATCH')
        <div class="tab-content">
            @include('admin.contract.viettel-partner._formContract')
            @include('admin.contract.viettel-partner._menu1')
        </div>
        {!! Form::close() !!}
    </div>
@endsection
