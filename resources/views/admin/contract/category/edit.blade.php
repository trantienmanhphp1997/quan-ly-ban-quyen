@extends('layouts.master')

@section('title')
    Sửa Loại hợp đồng
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Sửa Loại hợp đồng</h2>
            </div>
        </div>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::model($category, ['method' => 'PATCH','route' => ['contract.category.update', $category->id]]) !!}
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Tên(<span class="text-danger">*</span>)</strong>
                {!! Form::text('name', null, array('placeholder' => 'Tên','class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Ghi chú</strong>
                {!! Form::text('note', null, array('placeholder' => 'Ghi chú','class' => 'form-control')) !!}
            </div>
        </div>
    </div>
    <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
            @include('layouts.partials.button._save')
            &nbsp; &nbsp; &nbsp;
            @include('layouts.partials.button._back')
        </div>
    </div>
    {!! Form::close() !!}
@endsection
