@extends('layouts.master')
@section('title')
Danh sách Loại hợp đồng
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12 margin-tb">
            <div class="pull-left">
                <h2>Danh sách Loại hợp đồng</h2>
            </div>
            <div class="pull-right">
                @include('layouts.partials.button._new')
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <div class="row">
                <div class="col-xs-12">
                    <table id="simple-table" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="hidden-480" style="text-align: center; color: black;">STT</th>
                            <th style="text-align: center; color: black;">Tên</th>
                            <th style="text-align: center; color: black;">Ghi chú</th>
                            <th style="text-align: center; color: black;" width="280px">Hành động</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $key => $rs)
                            <tr>
                                <td style="text-align: center;">{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</td>
                                <td>{{ $rs->name }}</td>
                                <td>{{ $rs->note }}</td>
                                <td style="text-align: center;">
                                @if($rs->id != 1 && $rs->id !=2)
                                    @php $id = $rs->id; @endphp
                                    @include('layouts.partials.button._edit')
                                    {!! Form::open(['method' => 'DELETE','route' => ['contract.category.destroy', $rs->id],'style'=>'display:inline']) !!}
                                        @include('layouts.partials.button._deleteForm')
                                    {!! Form::close() !!}
                                @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- /.span -->
            </div><!-- /.row -->
        </div><!-- /.col -->
    </div><!-- /.row -->

@endsection
