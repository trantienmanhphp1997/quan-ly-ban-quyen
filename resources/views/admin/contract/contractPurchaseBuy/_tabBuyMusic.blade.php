<div id="menu4" class="tab-pane fade @if(Session::has('tab')&&Session::get('tab')=='menu4') in active @endif">
    @livewire('contract-music-buy-table', ['contract_id' => $data->id])
</div>
