
            <div id="menu3" class="tab-pane fade @if(Session::has('tab')&&Session::get('tab')=='menu3') in active @endif">
                <div class="row">
                    <div class="col-xs-12">
                        <table id="simple-table" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Số tờ trình</th>
                                    <th>Ngày ký</th>
                                    <th>Kế hoạch</th>
                                    <th>Kết quả</th>
                                    <th width="280px">Hành động</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        {{--                                       {!! Form::open(['method' => 'DELETE','route' => ['tool.roles.destroy', $val->id],'style'=>'display:inline']) !!}--}}
                                        {{-- <button class="btn btn-xs btn-danger" type="submit">
                                            <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                        </button> --}}
                                        {{--                                       {!! Form::close() !!}--}}

                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal1">
                            + Thêm
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel"> + Thêm</h5>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group row">
                                            <label for="inputEmail3" class="col-sm-2 col-form-label">Số tờ trình</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="name" id="inputEmail3" placeholder="Đối tác">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputEmail3" class="col-sm-2 col-form-label">Ngày ký</label>
                                            <div class="col-sm-10">
                                                <input type="date" class="form-control"  id="inputEmail3" placeholder="Ngày ký">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputEmail3" class="col-sm-2 col-form-label">Kế hoạch</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="plan" id="inputEmail3" placeholder="Kế hoạch">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputEmail3" class="col-sm-2 col-form-label">Kết quả</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="result" id="inputEmail3" placeholder="Kết quả">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">x</button>
                                        <button type="button" class="btn btn-primary add">OK</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.span -->
                </div>
            </div>
