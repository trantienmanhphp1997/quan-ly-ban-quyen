<div id="menu6" class="tab-pane fade @if(Session::has('tab')&&Session::get('tab')=='menu6') in active @endif">
    @livewire('contract-music-sale-table', ['contract_id' => $data->id])
</div>
