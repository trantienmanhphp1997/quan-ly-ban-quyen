@extends('layouts.master')

@section('title')
Thêm mới hợp đồng
@endsection
@section('css')
    <link rel="stylesheet" href="{{asset('assets/select2/select2.min.css')}}" >
    <style>
        .select2-search--inline::after{
            margin-left:150px; !important;
        }
    </style>
@endsection
@section('content')

<div class="main-container">
    <h2>Thêm mới hợp đồng mua</h2>
    {!! Form::open(array('route' => 'contract.viettel-buy.store','method'=>'POST', 'class' => 'form-horizontal',  'autocomplete' => "off")) !!}
    <form action="{{ route('contract.viettel-buy.store')}}" method="POST" class ='form-horizontal' autocomplete="off">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home">Thông tin hợp đồng</a></li>
            <li><a data-toggle="tab" href="#menu2">Người phụ trách</a></li>
            <li style="float:right;">
                @include('layouts.partials.button._save')
                @include('layouts.partials.button._back')
            </li>
        </ul>
            <div class="tab-content">
                @include('admin.contract.contractPurchaseBuy._formContact')
                @include('admin.contract.contractPurchaseBuy._menu2')
            </div>
    {!! Form::close() !!}
</div>
    @include('layouts.partials.lib._date_picker')
@endsection

