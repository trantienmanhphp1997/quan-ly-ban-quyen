<div id="menu1" class="tab-pane fade @if(Session::has('tab')&&Session::get('tab')=='menu1') in active @endif">
    @livewire('contract-file-table', ['data' => $data])
</div>
