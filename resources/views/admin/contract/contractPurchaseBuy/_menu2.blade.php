
<div id="menu2" class="tab-pane fade @if(Session::has('tab')&&Session::get('tab')=='menu2') in active @endif">
    <div class="form-group row @error('user_name') has-error @enderror">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Họ và tên</label>
        <div class="col-sm-10">
            {!! Form::text('user_name', null, array('placeholder' => 'Họ và tên','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="form-group row @error('user_msisdn') has-error @enderror">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Số điện thoại</label>
        <div class="col-sm-10">
        {!! Form::text('user_msisdn', null, array('placeholder' => 'Số điện thoại','class' => 'form-control')) !!}
            @error('user_msisdn')
                @include('layouts.partials.text._error')
            @enderror
        </div>
    </div>
    <div class="form-group row @error('user_email') has-error @enderror"">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
        <div class="col-sm-10">
            {!! Form::text('user_email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
            @error('user_email')
                @include('layouts.partials.text._error')
            @enderror
        </div>
    </div>
</div>
