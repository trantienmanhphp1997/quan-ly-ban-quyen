@extends('layouts.master')
@section('title')
QUẢN LÝ HỢP ĐỒNG MUA BẢN QUYỀN
@endsection
@section('content')

    <div class="page-header">
        <div class="row">
            <div class="col-sm-12">
                <div class="widget-box">
                    <div class="widget-header">
                        <h4 class="widget-title">Tìm kiếm</h4>
                        <span class="widget-toolbar">
                            <a href="#" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up"></i>
                            </a>
                        </span>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            {!! Form::open(['method'=>'GET','route'=>'contract.viettel-buy','role'=>'search'])  !!}
                            <div class="row">
                                <div class="col-xs-4 col-sm-4">
                                    <label for="form-field-select-1">Số hợp đồng</label>
                                    <input class="form-control" name="contract_number" type="text" value="{{request()->contract_number}}"/>
                                </div>
                                <div class="col-xs-4 col-sm-4">
                                    <label for="form-field-select-1">Người phụ trách</label>
                                    <input class="form-control" name="user_name" type="text" value="{{request()->user_name}}"/>
                                </div>
                                <div class="col-xs-4 col-sm-4">
                                    <!-- #section:plugins/date-time.datepicker -->
                                    <label for="id-date-picker-1">Thời gian</label>
                                    <div class="input-group">
                                        <input class="form-control date-picker" id="id-date-picker-1" type="date" data-date-format="dd-mm-yyyy" name="date_sign" value="{{request()->date_sign}}"/>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-xs-6 col-sm-6">
                                    <button type="submit" class="btn btn-info btn-sm">
                                        <span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
                                        Tìm kiếm
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div><!-- /.page-header -->

    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>QUẢN LÝ HỢP ĐỒNG MUA BẢN QUYỀN</h2>
                        <h2>qeqweqw</h2>
                    </div>
                    <div class="pull-right">
                    @include('layouts.partials.button._new')

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <table id="simple-table" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="hidden-480" style="color: black;">STT</th>
                                <th>Số hợp đồng</th>
                                <th>Ngày ký</th>
                                <th>Đối tác</th>
                                <th>Tổng giá trị</th>
                                <th>Người phụ trách</th>
                                <th>Số điện thoại</th>
                                <th>Hành động</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($data as $key => $val)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $val-> contract_number }}</td>
                                <td>{{ reFormatDate($val-> date_sign, 'd-m-Y') }}</td>
                                <td>{{ $val-> partner }}</td>
                                <td>{{ $val-> total_money }}</td>
                                <td>{{ $val-> user_name }}</td>
                                <td>{{ $val-> user_msisdn }}</td>
                                <td>
                                    <a class="btn btn-xs btn-info" href="{{ route('contract.viettel-buy.edit',$val->id) }}">
                                        <i class="ace-icon fa fa-pencil bigger-120"></i>
                                    </a>
                                    {!! Form::open(['method' => 'DELETE','route' => ['contract.viettel-buy.destroy', $val->id] ,'style'=>'display:inline']) !!}
                                    <button type='button'  class="btn btn-xs btn-danger "
                                    data-target="#modal-form-delete-contract" data-toggle="modal">
                                    <i class="menu-icon fa fa-trash-o" style="color: white;font-size:16px;"></i>
                                    </button>
                                    <button id='delete' type="submit" style="display: none;"></button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div><!-- /.span -->
                @include('layouts.partials._pagination')
            </div><!-- /.row -->
        </div><!-- /.col -->
    </div><!-- /.row -->
    {{-- modal xóa hợp đồng --}}
    <div class="modal fade" id="modal-form-delete-contract" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Xóa hợp đồng</h5>
                </div>
                <div class="modal-body">
                    Bạn có muốn xóa không? Thao tác này không thể phục hồi!
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Không</button>
                    <button type="button" class="btn btn-danger" onclick="document.getElementById('delete').click()">Xóa bỏ</button>
                </div>
            </div>
        </div>
    </div>
@endsection
