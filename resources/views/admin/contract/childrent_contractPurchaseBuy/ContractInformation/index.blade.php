@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form id="testform" action="" method="POST">
    @csrf
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Tiêu đề</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="title" id="inputEmail3" placeholder="Số hợp đồng">
        </div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Nội dung</label>
        <div class="col-sm-10">
            <textarea class="form-control" name="content" id="exampleFormControlTextarea1" rows="3"></textarea>
        </div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Số hợp đồng</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="contract_number" id="inputEmail3" placeholder="Số hợp đồng">
        </div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Loại hợp đồng</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="contract_number" id="inputEmail3" placeholder="Lọa hợp đồng">
        </div>
    </div>
    <div class="form-group row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Quốc gia được quyền kinh doanh</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="country_entitled_business" id="inputEmail3" placeholder="Quốc gia được quyền kinh doanh">
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Số lần phát sóng</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" name="number" id="inputEmail3" placeholder="Số lần phát sóng">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Ngày ký</label>
                    <div class="col-sm-8">
                        <input type="date" class="form-control" name="date_sign" id="inputEmail3" placeholder="Ngày ký">
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Thời hạn</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control" name="duration" id="inputEmail3" placeholder="Thời hạn">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Tổng giá trị hợp đồng</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="total_money" id="inputEmail3" placeholder="Tổng giá trị hợp đồng">
        </div>
    </div>
    <div class="form-group row">
        <label class="btn btn-light" for="flexCheckDefault">VOD VIT</label>
        <input type="checkbox" class="btn-check" name="vod_vit" id="flexCheckDefault">

        <label class="btn btn-light" for="flexCheckDefault1">VOD VTM</label>
        <input type="checkbox" class="btn-check" name="vod_vtm" id="flexCheckDefault1">

        <label class="btn btn-light" for="flexCheckDefault2">VOD Ngoài</label>
        <input type="checkbox" class="btn-check" name="vod_ngoai" id="flexCheckDefault2">

        <label class="btn btn-light" for="flexCheckDefault3">TH QPVN</label>
        <input type="checkbox" class="btn-check" name="th_qpvn" id="flexCheckDefault3">

        <label class="btn btn-light" for="flexCheckDefault4">TH Ngoài</label>
        <input type="checkbox" class="btn-check" name="th_ngoai" id="flexCheckDefault4">
    </div>
    <div class="form-group row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="col-xs-4 col-sm-4 col-md-4">
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-6 col-form-label">Tiến độ thanh toán</label>
                    <div class="col-sm-6">
                        <input type="date" class="form-control" name="payment_schedule" id="inputEmail3" placeholder="Tiến độ thanh toán">
                        <button style="border:none; color:#02790E;font-weight:bold;margin:10px 0 0 0;" id="addRow">+ Thêm mốc thanh toán</button>
                    </div>
                </div>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8">
                <div class="form-group row">
                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="payment_amount" id="inputEmail3" placeholder="Số tiền thanh toán">
                        <button style="border:none; color:#333333;font-weight:bold;margin:10px 0 0 0;" id="addRow">Tổng đã thanh toán: 0đ</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="col-xs-4 col-sm-4 col-md-4">
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-6 col-form-label">Tiến độ bàn giao</label>
                    <div class="col-sm-6">
                        <input type="date" class="form-control" name="handover_schedule" id="inputEmail3" placeholder="Tiến độ bàn giao">
                        <button style="border:none; color:#02790E;font-weight:bold;margin:10px 0 0 0;" id="addRow">+ Thêm mốc bàn giao</button>
                    </div>
                </div>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8">
                <div class="form-group row">
                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="content_handover" id="inputEmail3" placeholder="Nội dung bàn giao">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Tình trạng nhận nội dung</label>
        <div class="col-sm-10">
            <textarea class="form-control" name="content_status" id="exampleFormControlTextarea1" rows="3"></textarea>
        </div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Đối tác</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="name" id="inputEmail3" placeholder="Đối tác">
        </div>
    </div>
</form>
