@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form id="testform" action="" method="POST">
    @csrf
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Họ và tên</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="fullname" id="inputEmail3" placeholder="Họ và tên">
        </div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Số điện thoại</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="phone" id="inputEmail3" placeholder="Số điện thoại">
        </div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="email" id="inputEmail3" placeholder="Email">
        </div>
    </div>
</form>
