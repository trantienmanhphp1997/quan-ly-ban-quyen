<table  class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>STT</th>
            <th>Số tờ trình</th>
            <th>Ngày ký</th>
            <th>Link</th>
            <th width="280px">Hành động</th>
        </tr>
    </thead>

    <tbody>
        @foreach( $contract->contractFiles as $key => $contractFile)
        <tr>
            <td></td>
            <td>{{ $contractFile->name }}</td>
            <td>{{ $contractFile->date_sign }}</td>
            <td>{{ $contractFile->link }}</td>
            <td>
                <button type="button" class="btn btn-xs btn-danger delete-button" data-target="#modal-form-delete-contract_file" data-toggle="modal" data-id='{{$contractFile->id}}'>
                    <i class="ace-icon fa fa-trash-o bigger-120"></i>
                </button>
                <input type="hidden" class="contractFile_id" value="{{ $contractFile->id }}">

            </td>
        </tr>
        @endforeach
        <input type="hidden" id="contract_id" value="{{ $contract->id }}">
    </tbody>
</table>
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
    + Thêm tờ trình
</button>

<!-- Modal -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form method="POST" id="upload-form" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Thêm tờ trình</h5>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 col-form-label" >Số tờ trình</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" id="name" placeholder="Đối tác">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Ngày ký</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control" name="date_sign_contract_file"  placeholder="Ngày ký">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Ghi chú nội dung</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control-file" name='file_upload' id="file_upload" placeholder="Ghi chú nội dung">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">x</button>
                    <button type="submit" class="btn btn-primary add">OK</button>
                </div>
            </div>
        </div>
    </form>
</div>

<!-- Modal delete contract file-->
<div class="modal fade" id="modal-form-delete-contract_file" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Xóa tờ trình</h5>
            </div>
            <div class="modal-body">
                Bạn có muốn xóa không? Thao tác này không thể phục hồi!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Không</button>
                <button type="button" class="btn btn-danger delete_contract_file" >Xóa bỏ</button>
            </div>
        </div>
    </div>
</div>