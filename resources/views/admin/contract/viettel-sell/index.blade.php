@extends('layouts.master')
@section('title')
	Hợp đồng Viettel bán
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('css/QLBQ.css')}}">
    <link rel="stylesheet" href="{{asset('assets/select2/select2.min.css')}}" >
    <style>
        .select2-search--inline::after{
            margin-left:100px; !important;
        }
    </style>
@endsection
@section('content')

@livewire('contract.viettel-sell')

@endsection
@section('js')
    <script src="{{asset('js/QLBQ.js')}}"></script>
    <script src="{{asset('assets/select2/select2.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $(function () {
                $(".select_box").select2({
                    placeholder: "Chọn  loại doanh thu...",
                    allowClear: true
                });
            });
        });
    </script>
@endsection
