@extends('layouts.master')

@section('title')
Sửa hợp đồng Viettel bán
@endsection
@section('css')
    <link rel="stylesheet" href="{{asset('assets/select2/select2.min.css')}}" >
    <style>
        .select2-search--inline::after{
            margin-left:150px; !important;
        }
    </style>
@endsection
@section('content')

<div class="main-container">
    <h2>Sửa hợp đồng Viettel bán</h2>
<!--     <form action="{{ route('contract.viettel-sell.update', ['id' => $data->id])}}" method="POST"> -->
    {!! Form::model($data, ['method' => 'POST','id'=>'form_update_contract' ,'class' => 'form-horizontal','autocomplete' => "off", 'route' => ['contract.viettel-sell.update', $data->id]]) !!}
        @csrf
        @method('PATCH')
        <ul class="nav nav-tabs">
            <li class="@if(!Session::has('tab')||Session::get('tab')=='home') active @endif"><a data-toggle="tab" href="#home">Thông tin hợp đồng</a></li>
            <li class="@if(Session::has('tab')&&Session::get('tab')=='menu1') active @endif"><a data-toggle="tab" href="#menu1">Giấy tờ phê duyệt</a></li>
            <li class="@if(Session::has('tab')&&Session::get('tab')=='menu2') active @endif"><a data-toggle="tab" href="#menu2">Người phụ trách</a></li>
            @if($data->category_id == 1)
                <li class="@if(Session::has('tab')&&Session::get('tab')=='menu4') active @endif"><a data-toggle="tab" href="#menu4">Danh sách Nhạc bán</a></li>
            @endif
            @if($data->category_id == 2)
                <li class="@if(Session::has('tab')&&Session::get('tab')=='menu4') active @endif"><a data-toggle="tab" href="#menu4">Danh sách Phim bán</a></li>
            @endif
            <li style="float:right;">
                @include('layouts.partials.button._save')
                @include('layouts.partials.button._back')
            </li>
        </ul>
        <div class="tab-content">
            @include('admin.contract.contractPurchaseBuy._formContact')
            @include('admin.contract.contractPurchaseBuy._menu1')
            @include('admin.contract.contractPurchaseBuy._menu2') 
            @if($data->category_id == 1)              
                @include('admin.contract.contractPurchaseSale._tabSaleMusic')
            @endif
            @if($data->category_id == 2)              
                @include('admin.contract.contractPurchaseSale._tabSaleFilm')
            @endif
        </div>
    </form>
</div>
@endsection
@section('js')

<!-- <script>
    $('#form_update_contract').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });
</script> -->
@endsection
