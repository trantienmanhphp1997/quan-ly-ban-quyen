@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form action="" method="POST">
    @csrf
    <div class="modal-body">
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Số tờ trình</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="name" id="inputEmail3" placeholder="Đối tác">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Ngày ký</label>
            <div class="col-sm-10">
                <input type="date" class="form-control" name="date_sign" id="inputEmail3" placeholder="Ngày ký">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Kế hoạch</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="plan" id="inputEmail3" placeholder="Kế hoạch">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Kết quả</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="result" id="inputEmail3" placeholder="Kết quả">
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">x</button>
        <button type="submit" class="btn btn-primary">OK</button>
    </div>
</form>
