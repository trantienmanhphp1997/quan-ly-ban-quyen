@extends('layouts.master')

@section('title')
Sửa hợp đồng bán
@endsection

@section('content')

<div class="main-container">
    <h2>Sửa hợp đồng bán</h2>
    <form action="{{ route('contract.viettel-sale.store')}}" method="POST">
        @csrf
        @method('POST')
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home">Thông tin hợp đồng</a></li>
            <li><a data-toggle="tab" href="#menu2">Người phụ trách</a></li>
            <li style="float:right;">
                <button class="btn btn-light" type="submit">Lưu</button>
                <button type="button" class="btn btn-primary" onclick="document.getElementById('back').click()">Quay lại</button>
                <a id='back' class="btn btn-primary"  href="{{ route('contract.viettel-sale') }}" style="display: none;">Quay lại</a>
            </li>
        </ul>
        <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Tiêu đề</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="title" placeholder="Số hợp đồng">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Nội dung</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="content" >
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Số hợp đồng</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="contract_number" placeholder="Số hợp đồng">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Loại hợp đồng</label>
                    <div class="col-sm-4">
                        {!! Form::select('list_categoy', $list_categoy, request('list_categoy'), ['class' => 'form-control', 'id' => "form-field-select-1"]) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-4 col-form-label">Quốc gia được quyền kinh doanh</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="country_entitled_business" placeholder="Quốc gia được quyền kinh doanh">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Số lần phát sóng</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control" name="number" id="inputEmail3" placeholder="Số lần phát sóng">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Ngày ký</label>
                                <div class="col-sm-8">
                                    <input type="date" class="form-control" name="date_sign" placeholder="Ngày ký">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Thời hạn</label>
                                <div class="col-sm-10">
                                    <input type="date" class="form-control" name="duration" placeholder="Thời hạn">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Tổng giá trị hợp đồng</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="total_money" placeholder="Tổng giá trị hợp đồng">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="btn btn-light" for="flexCheckDefault">VOD VIT</label>
                    <input type="checkbox" class="btn-check" name="vod_vit" id="flexCheckDefault">

                    <label class="btn btn-light" for="flexCheckDefault1">VOD VTM</label>
                    <input type="checkbox" class="btn-check" name="vod_vtm" id="flexCheckDefault1">

                    <label class="btn btn-light" for="flexCheckDefault2">VOD Ngoài</label>
                    <input type="checkbox" class="btn-check" name="vod_ngoai" id="flexCheckDefault2">

                    <label class="btn btn-light" for="flexCheckDefault3">TH QPVN</label>
                    <input type="checkbox" class="btn-check" name="th_qpvn" id="flexCheckDefault3">

                    <label class="btn btn-light" for="flexCheckDefault4">TH Ngoài</label>
                    <input type="checkbox" class="btn-check" name="th_ngoai" id="flexCheckDefault4">
                </div>
                <div class="form-group row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-6 col-form-label">Tiến độ thanh toán</label>
                                <div class="col-sm-6">
                                    <input type="date" class="form-control" name="payment_schedule" id="inputEmail3" placeholder="Tiến độ thanh toán">
                                    <button style="border:none; color:#02790E;font-weight:bold;margin:10px 0 0 0;" id="addRow">+ Thêm mốc thanh toán</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8">
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" name="payment_amount" id="inputEmail3" placeholder="Số tiền thanh toán">
                                    <button style="border:none; color:#333333;font-weight:bold;margin:10px 0 0 0;" id="addRow">Tổng đã thanh toán: 0đ</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-6 col-form-label">Tiến độ bàn giao</label>
                                <div class="col-sm-6">
                                    <input type="date" class="form-control" name="handover_schedule" id="inputEmail3" placeholder="Tiến độ bàn giao">
                                    <button style="border:none; color:#02790E;font-weight:bold;margin:10px 0 0 0;" id="addRow">+ Thêm mốc bàn giao</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8">
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" name="content" id="inputEmail3" placeholder="Nội dung bàn giao">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Tình trạng nhận nội dung</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="content" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Đối tác</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="name" id="inputEmail3" placeholder="Đối tác">
                    </div>
                </div>
            </div>

            <div id="menu2" class="tab-pane fade">
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Họ và tên</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="user_name" placeholder="Họ và tên">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Số điện thoại</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="user_msisdn" placeholder="Số điện thoại">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="user_email" placeholder="Email">
                    </div>
                </div>
            </div>
        </div>
    </form>

</div>
@endsection
