
<div id="menu2" class="tab-pane fade">
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Họ và tên</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="user_name" value="{{ $data->user_name }}" placeholder="Họ và tên">
        </div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Số điện thoại</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="user_msisdn" value="{{ $data->user_msisdn }}"  placeholder="Số điện thoại">
            @error('user_msisdn')
                @include('layouts.partials.text._error')
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="user_email" value="{{ $data->user_email }}" placeholder="Email">
            @error('user_email')
                @include('layouts.partials.text._error')
            @enderror
        </div>
    </div>
</div>
