<div id="home" class="tab-pane fade @if(!Session::has('tab')) in active @endif">
                    <div class="form-group row @error('title') has-error @enderror">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Tiêu đề</label>
                        <div class="col-sm-10">
                            {!! Form::text('title', null, array('placeholder' => 'Tiêu đề','class' => 'form-control')) !!}
                            @error('title')
                                @include('layouts.partials.text._error')
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Nội dung</label>
                        <div class="col-sm-10">
                         {!! Form::text('content', null, array('placeholder' => 'Nội dung','class' => 'form-control')) !!}
                            @error('content')
                                @include('layouts.partials.text._error')
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row @error('contract_number') has-error @enderror">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Số hợp đồng <span class="text-danger">(*)</span></label>
                        <div class="col-sm-10" >
                            {!! Form::text('contract_number', null, array('placeholder' => 'Số hợp đồng','class' => 'form-control')) !!}
                            @error('contract_number')
                                @include('layouts.partials.text._error')
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row @error('category_id') has-error @enderror">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Loại hợp đồng <span class="text-danger">(*)</span></label>
                        <div class="col-sm-4">
                            {!! Form::select('category_id', $list_categoy, null, ['class' => 'form-control', 'id' => "form-field-select", 'disabled' => isset($data)]) !!}
                            @error('category_id')
                                @include('layouts.partials.text._error')
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row @error('date_sign') has-error @enderror">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Ngày ký</label>
<!--                         <div class="col-sm-4">
                            {!! Form::text('date_sign', date('d/m/Y', strtotime(old('date_sign'))), array('placeholder' => 'Ngày ký','class' => 'form-control date-picker', 'id' => 'id-date-picker-1')) !!}
                        </div> -->
                        <div class="col-sm-4">
                            <div class="input-group">
                                  {!! Form::text('date_sign', isset($data)? reFormatDate($data->date_sign, 'd-m-Y') : null, array('placeholder' => 'Ngày ký','class' => 'form-control date-picker', 'id' => "id-date-picker-1", "data-date-format"=>"dd-mm-yyyy")) !!}
                                @include('layouts.partials.input._calender')
                             </div>
                        </div>
                            @error('date_sign')
                                @include('layouts.partials.text._error')
                            @enderror
                    </div>
                    <div class="form-group row @error('country_entitled_business') has-error @enderror">
                        <label class="col-sm-2 control-label" for="form-field-8">Ngày bắt đầu bản quyền</label>
                        <div class="col-sm-4">
                                                        <div class="input-group">

                                 {!! Form::text('start_time', isset($data)? reFormatDate($data->start_time, 'd-m-Y') : null, array('placeholder' => 'Ngày bắt đầu','class' => 'form-control date-picker', 'id' => "id-date-picker-1", "data-date-format"=>"dd-mm-yyyy")) !!}
                                @include('layouts.partials.input._calender')
                                                             </div>

                            @error('start_time')
                                @include('layouts.partials.text._error')
                            @enderror
                        </div>
                        <label class="col-sm-2 control-label padding-right" for="form-field-8">Ngày hết hạn</label>
                        <div class="col-sm-4">
                            <div class="input-group">

                             {!! Form::text('end_time', isset($data)? reFormatDate($data->end_time, 'd-m-Y') : null, array('placeholder' => 'Ngày hết hạn','class' => 'form-control date-picker', 'id' => "id-date-picker-1", "data-date-format"=>"dd-mm-yyyy")) !!}
                            @include('layouts.partials.input._calender')
                            </div>

                            @error('end_time')
                                @include('layouts.partials.text._error')
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row  @error('total_money') has-error @enderror">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Tổng giá trị hợp đồng</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="total_money" value= "{{ old('total_money') }}" placeholder="Tổng giá trị hợp đồng">
                        </div>
                    </div>
                    @if(isset($data))
                        <input type="text" style="display: none;" id = 'idContract' value="{{$data->id}}">
                        <div id = 'div_payment'>
                        @foreach($contract_payment as $key => $value)
                        <div class="form-group row div-delete">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="form-group row @error('payment_schedule') has-error @enderror">
                                        <label for="inputEmail3" class="col-sm-6 col-form-label">
                                            @if($key==0)
                                                Tiến độ thanh toán
                                            @endif
                                        </label>
                                        <div class="col-sm-6">
                                            <div class="input-group">
                                                <input type="text" class="form-control date-picker" name="payment_schedule" id="inputEmail3" placeholder="Tiến độ thanh toán" value="{{isset($value)? reFormatDate($value->date_pay, 'd-m-Y') : null}}" data-date-format= "dd-mm-yyyy">
                                                @include('layouts.partials.input._calender')
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-2 col-sm-2 col-md-2">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control money" name="payment_amount" id="inputEmail3" placeholder="Số tiền thanh toán" value="{{$value->money}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control note" name="payment_amount" id="inputEmail3" placeholder="Ghi chú" value="{{$value->note}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-2 col-sm-2 col-md-2">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <button class="btn btn-danger btn-sm btn-delete-payment">Xóa</button>
                                            <input type="text" style="display: none;" value="{{$value->id}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        </div>
                        <div class="form-group row">
                            <div class="col-xs-12 col-sm-12 col-md-12 in div-delete">
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="form-group row @error('payment_schedule') has-error @enderror">
                                        <label for="inputEmail3" class="col-sm-6 col-form-label">
                                            @if(count($contract_payment)==0)
                                                Tiến độ thanh toán
                                            @endif
                                        </label>
                                        <div class="col-sm-6">
                                            <div class="input-group">
                                                <input type="text" class="form-control date-picker" name="payment_schedule" id="inputEmail3" placeholder="Tiến độ thanh toán" value="{{null}}"data-date-format= "dd-mm-yyyy">
                                                @include('layouts.partials.input._calender')
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-2 col-sm-2 col-md-2">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control money" name="payment_amount" id="inputEmail3" placeholder="Số tiền thanh toán">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control note" name="payment_amount" id="inputEmail3" placeholder="Ghi chú">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-2 col-sm-2 col-md-2">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <button class="btn btn-primary btn-sm" id='btn-save-payment'>Lưu</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="form-group row @error('payment_schedule') has-error @enderror">
                                        <label for="inputEmail3" class="col-sm-6 col-form-label"></label>
                                        <div class="col-sm-6">
                                            <p class="text-danger" id='text-danger'></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Tình trạng nhận nội dung</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="note" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="form-group row @error('partner_id') has-error @enderror">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Đối tác <span class="text-danger">(*)</span></label>
                        <div class="col-sm-4">
                            {!! Form::select('partner_id', $list_partner, null, ['class' => 'form-control select_box_partner', 'id' => "form-field-select-1"]) !!}
                            @error('partner_id')
                                @include('layouts.partials.text._error')
                            @enderror
                            </select>
                        </div>
                    </div>
            </div>
<script type="text/javascript">
    $(document).ready(function() {
        var date_pay
        var contract_id = $('#idContract').val()
        var money
        var note
        $('#btn-save-payment').click(function(e){
            e.preventDefault()
            $('#text-danger').text('')
            var parent = $(this).closest('div .in');
            input = $(parent).find('input');
            label = $(parent).find('label');
            var text = $(label[0]).html().trim();
            if(text==null|| text ==''){

            }
            else {
                text = 'Tiến độ thanh toán'
            }
            date_pay = input[0].value
            money =  input[1].value
            note =  input[2].value
            var _token = $("input[name='_token']").val();
            if(money&&note&&date_pay){
                $.ajax({
                    url: "/contract/viettel-sell/"+contract_id+"/storePaymentProcess",
                    type:'POST',
                    data: {
                        _token:_token,
                        date_pay:date_pay,
                        money:money,
                        note:note
                    },
                    success: function(data) {
                        var div = `<div class="form-group row div-delete">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <div class="form-group row @error('payment_schedule') has-error @enderror">
                                            <label for="inputEmail3" class="col-sm-6 col-form-label">
                                                ${text}
                                            </label>
                                            <div class="col-sm-6">
                                                <div class="input-group">
                                                    <input type="text" class="form-control date-picker inputdate" name="payment_schedule" id="inputEmail3" placeholder="Tiến độ thanh toán" value="${date_pay}" data-date-format= "dd-mm-yyyy">
                                                    @include('layouts.partials.input._calender')
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 col-sm-2 col-md-2">
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control money" name="payment_amount" id="inputEmail3" placeholder="Số tiền thanh toán" value="${money}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control note" name="payment_amount" id="inputEmail3" placeholder="Ghi chú" value="${note}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 col-sm-2 col-md-2">
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <button class="btn btn-danger btn-sm btn-delete-payment">Xóa</button>
                                                <input type="text" style="display: none;" value="${data}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>`
                        $('#div_payment').append(div)
                        $(input[0]).val('')
                        $(input[1]).val('')
                        $(input[2]).val('')
                        $(label[0]).html('')
                        $('.btn-delete-payment').click(function(e){
                            e.preventDefault();
                            var parent = $(this).parent()[0];
                            var input = $(parent).find('input');
                            var id = $(input[0]).val()
                            var btn_delete_payment = $(this);
                            var _token = $("input[name='_token']").val();
                            $.ajax({
                                url: "/contract/viettel-buy/"+id+"/deletePaymentProcess",
                                type:'DELETE',
                                data: {
                                    _token:_token,
                                },
                                success: function(data) {
                                    var div = $(btn_delete_payment).closest('div .div-delete')
                                    $(div[0]).remove()
                                    var div_delete = $('.div-delete')[0];
                                    var label = $(div_delete).find('label')[0]
                                    $(label).html('Tiến độ thanh toán')
                                }
                            });
                        })
                    }
                });
            }
            else {
                $('#text-danger').text('*Kiểm tra lại dữ liệu')
            }
        })

        $('.btn-delete-payment').click(function(e){
            e.preventDefault();
            var parent = $(this).parent()[0];
            var input = $(parent).find('input');
            var id = $(input[0]).val()
            var btn_delete_payment = $(this);
            var _token = $("input[name='_token']").val();
            $.ajax({
                url: "/contract/viettel-sell/"+id+"/deletePaymentProcess",
                type:'DELETE',
                data: {
                    _token:_token,
                },
                success: function(data) {
                    var div = $(btn_delete_payment).closest('div .div-delete')
                    $(div[0]).remove()
                    var div_delete = $('.div-delete')[0];
                    var label = $(div_delete).find('label')[0]
                    $(label).html('Tiến độ thanh toán')
                }
            });
        })
    })
</script>
