@extends('layouts.master')

@section('title')
Sửa hợp đồng mua
@endsection
@section('css')
    <link rel="stylesheet" href="{{asset('assets/select2/select2.min.css')}}" >
    <style>
        .select2-search--inline::after{
            margin-left:150px; !important;
        }
    </style>
@endsection
@section('content')

<div class="main-container">
    <h2>Sửa hợp đồng mua</h2>
    <form action="{{ route('contract.viettel-sell.update', ['id' => $data->id])}}" method="POST">
        @csrf
        @method('PATCH')
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home">Thông tin hợp đồng</a></li>
            <li><a data-toggle="tab" href="#menu1">Giấy tờ phê duyệt</a></li>
            <li><a data-toggle="tab" href="#menu2">Người phụ trách</a></li>

            @if($data->category_id == 1)
                <li><a data-toggle="tab" href="#menu4">
                    Danh sách phim bán
                </a></li>
            @endif
            @if($data->category_id == 2)
                <li><a data-toggle="tab" href="#menu4">
                    Danh sách nhạc bán
                </a></li>            
            @endif
            <li style="float:right;">
                <button class="btn btn-light" type="submit">Lưu</button>
                <button class="btn btn-primary"  href="{{ route('contract.viettel-buy') }}">Quay lại</button>
            </li>
        </ul>
        <div class="tab-content">
            
            @include('admin.contract.contractPurchaseBuy._formContact')
            @include('admin.contract.contractPurchaseBuy._menu1')
            @include('admin.contract.contractPurchaseBuy._menu2')
            @if($data->category_id == 1)
                @include('admin.contract.tab._tabMusicBuy')
            @endif
            @if($data->category_id == 2)
                @include('admin.contract.tab._tabFilmSell')
            @endif
        </div>
    </form>
</div>
@endsection

@section('js')
<script src="{{asset('assets/select2/select2.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $(function () {
            $(".select_box").select2({
                placeholder: "Chọn bài hát",
                allowClear: true,
                dropdownAutoWidth: true            
            });
        });
    });
</script>
@endsection

{{--<script>--}}
{{--    $(document).ready(function() {--}}
    {{--        var t = $('#payment_schedule').DataTable();--}}
    {{--        var counter = 1;--}}

    {{--        $('#addRow').on( 'click', function () {--}}
        {{--            t.row.add( [--}}
            {{--                counter +'.1',--}}
            {{--                counter +'.2',--}}
            {{--                counter +'.3',--}}
            {{--                counter +'.4',--}}
            {{--                counter +'.5'--}}
            {{--            ] ).draw( false );--}}

        {{--            counter++;--}}
        {{--        } );--}}

{{--        // Automatically add a first row of data--}}
{{--        $('#addRow').click();--}}
{{--    } );--}}
{{--</script>--}}
