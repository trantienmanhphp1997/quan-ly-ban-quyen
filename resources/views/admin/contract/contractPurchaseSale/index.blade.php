@extends('layouts.master')
@section('title')
    QUẢN LÝ HỢP ĐỒNG BÁN BẢN QUYỀN
@endsection
@section('content')
    <div class="page-header">
        <div class="row">
            <div class="col-sm-12">
                <div class="widget-box">
                    <div class="widget-header">
                        <h4 class="widget-title">Tìm kiếm</h4>

                        <span class="widget-toolbar">
                            <a href="#" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up"></i>
                            </a>
                        </span>
                    </div>

                    <div class="widget-body">
                        <div class="widget-main">
                            {!! Form::open(['method'=>'GET','url'=>'','role'=>'search'])  !!}
                            <div class="row">
                                <div class="col-xs-4 col-sm-4">
                                    <label for="form-field-select-1">Số hợp đồng</label>
                                    <input class="form-control" name="roomId" type="text" value="{{request('contract_number')}}"/>
                                </div>
                                <div class="col-xs-4 col-sm-4">
                                    <label for="form-field-select-1">Người phụ trách</label>
                                    <input class="form-control" name="userId" type="text" value="{{request('user_name')}}"/>
                                </div>
                                <div class="col-xs-4 col-sm-4">
                                    <!-- #section:plugins/date-time.datepicker -->
                                    <label for="id-date-picker-1">Thời gian</label>
                                    <div class="input-group">
                                        <input class="form-control date-picker" id="id-date-picker-1" type="text" data-date-format="dd-mm-yyyy" name="fromDate" value="{{request('fromDate')}}"/>
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar bigger-110"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-xs-6 col-sm-6">
                                    <button type="submit" class="btn btn-info btn-sm">
                                        <span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
                                        Tìm kiếm
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div><!-- /.page-header -->

    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>QUẢN LÝ HỢP ĐỒNG BÁN BẢN QUYỀN </h2>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-success" href="{{ route('contract.viettel-sale.create') }}">Thêm mới</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <table id="simple-table" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="hidden-480" style="color: black;">STT</th>
                            <th>Số hợp đồng </th>
                            <th>Ngày ký</th>
                            <th>Đối tác</th>
                            <th>Tổng giá trị</th>
                            <th>Người phụ trách</th>
                            <th>Số điện thoại</th>
                            <th>Hành động</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach ($data as $key => $val)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $val-> contract_number }}</td>
                            <td>{{ $val-> date_sign }}</td>
                            <td>{{ $val-> partner }}</td>
                            <td>{{ $val-> total_money }}</td>
                            <td>{{ $val-> user_name }}</td>
                            <td>{{ $val-> user_msisdn }}</td>
                            <td>
                                <a class="btn btn-xs btn-info" href="{{ route('contract.viettel-sale.edit',$val->id) }}">
                                    <i class="ace-icon fa fa-pencil bigger-120"></i>
                                </a>
{{--                                {!! Form::open(['method' => 'DELETE','route' => ['tool.roles.destroy'],'style'=>'display:inline']) !!}--}}
                                <button class="btn btn-xs btn-danger" type="submit" href="{{ route('contract.viettel-sale.destroy',$val->id) }}">
                                    <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                </button>
{{--                                {!! Form::close() !!}--}}
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- /.span -->
                @include('layouts.partials._pagination')
            </div><!-- /.row -->
        </div><!-- /.col -->
    </div><!-- /.row -->
@endsection
@section('js')
    <script>
        document.addEventListener('livewire:load', function () {
            $(function () {
                $("#btn-save-revenue").on('click', (e) => {
                    window.livewire.emit('storeRevenue',
                        document.getElementById("type_contract").value,
                        $('#media_id').val(),
                        document.getElementById("category_id").value,
                        document.getElementById("id-date-picker-1 start_time").value,
                        $('#money_revenue').val(),
                        $('#type_revenue').val(),
                        document.getElementById("type_id").value,
                        document.getElementById("id-date-picker-1 end_time").value,
                    );
                });
            });
        });
    </script>
@endsection
