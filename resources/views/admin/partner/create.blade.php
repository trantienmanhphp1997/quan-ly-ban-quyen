@extends('layouts.master')

@section('title')
    Thêm Đối tác
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Thêm mới đối tác</h2>
            </div>
        </div>
    </div>
    {!! Form::open(array('route' => 'partner.store','method'=>'POST', 'class' => 'form-horizontal',  'autocomplete' => "off", 'id'=>'form-create-film')) !!}

        <div class="row">
            @csrf
            <div class="col-xs-8 col-sm-8 col-md-8">
                <div class="form-group">
                    <strong> Tên trong hợp đồng(<span class="text-danger">*</span>):</strong><br>
                    {!! Form::text('name', null, array('placeholder' => 'Tên trong hợp đồng','class' => 'form-control')) !!}
                    @error('name')
                        @include('layouts.partials.text._error')
                    @enderror
                </div>
                <div class="form-group">
                    <strong> Tên thường dùng:</strong><br>
                    {!! Form::text('common_name', null, array('placeholder' => 'Tên thường dùng','class' => 'form-control')) !!}
                    @error('common_name')
                        @include('layouts.partials.text._error')
                    @enderror
                </div>
                <div class="form-group">
                    <strong>Email(<span class="text-danger">*</span>):</strong><br>
                    {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
                    @error('email')
                        @include('layouts.partials.text._error')
                    @enderror
                </div>
                <div class="form-group">
                    <strong>Quốc gia: </strong><br>
                    <select name="country" class="select_country_partner form-control">
                        <option value="">Chọn quốc gia...</option>
                        @foreach($countries as $key=> $item)
                            <option {{(old('country')==$item)?'selected':''}} value="{{$item}}">{{$item}}</option>
                        @endforeach
                    </select>
                    @error('country')
                        @include('layouts.partials.text._error')
                    @enderror
                </div>
                <div class="form-group">
                    <strong>Địa chỉ:</strong><br>
                    {!! Form::text('address', null, array('placeholder' => 'Nhập địa chỉ','class' => 'form-control')) !!}
                    @error('address')
                        @include('layouts.partials.text._error')
                    @enderror
                </div>
                <div class="form-group">
                    <strong>Số điện thoại:</strong><br>

                    {!! Form::text('phone', null, array('placeholder' => 'Nhập số điện thoại','class' => 'form-control')) !!}
                    @error('phone')
                        @include('layouts.partials.text._error')
                    @enderror
                </div>
                <div class="form-group">
                    <strong>Ghi chú:</strong><br>
                    {!! Form::textarea('note', null, array('placeholder' => 'Nhập ghi chú','class' => 'form-control')) !!}
                    @error('note')
                        @include('layouts.partials.text._error')
                    @enderror
                </div>
            </div>
        </div>
        <div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                @include('layouts.partials.button._save')
                &nbsp; &nbsp; &nbsp;
                @include('layouts.partials.button._back')
            </div>
        </div>
    {!! Form::close() !!}
@endsection
