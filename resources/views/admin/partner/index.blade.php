@extends('layouts.master')
@section('title')
Danh sách Đối tác
@endsection
@section('content')
    <div class="page-header">
        <div class="row">
            <div class="col-sm-12">
                <div class="widget-box">
                    <div class="widget-header">
                        <h4 class="widget-title">Tìm kiếm</h4>

                        <span class="widget-toolbar">
                            <a href="#" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up"></i>
                            </a>
                        </span>
                    </div>

                    <div class="widget-body">
                        <div class="widget-main">
                            {!! Form::open(['method'=>'GET','route'=>'partner.index','role'=>'search'])  !!}
                            <div class="row">
                                <div class="col-xs-4 col-sm-4">
                                    <label for="form-field-select-1">Tên đối tác</label>
                                    <input class="form-control" name="searchName" type="text"
                                        value="{{request('searchName')}}" id='input_vn_name' autocomplete="off" placeholder="Tên đối tác" />
                                </div>
                                <div class="col-xs-4 col-sm-4">
                                    <label for="form-field-select-1">Số điện thoại</label>
                                    <input class="form-control" name="searchPhone" type="text"
                                        value="{{request('searchPhone')}}" id='input_actor_name' autocomplete="off" placeholder="Số điện thoại" />
                                </div>
                                <div class="col-xs-4 col-sm-4">
                                    <label for="form-field-select-1">Email</label>
                                    <input class="form-control" name="searchEmail" type="text"
                                        value="{{request('searchEmail')}}" id='input_vn_name' autocomplete="off" placeholder="Email" />
                                </div>                  
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-xs-6 col-sm-6">
                                    <button type="submit" class="btn btn-info btn-sm">
                                        <span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
                                        Tìm kiếm
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-sm-8">
            </div>
            <div class="col-sm-12">
                <div id="main-container"></div>
            </div>
        </div>
    </div><!-- /.page-header -->
    <div class="row">
        <div class="col-xs-12 margin-tb">
            <div class="pull-left">
                <h2>Danh sách Đối tác</h2>
            </div>

                <div class="pull-right">
                    @include('layouts.partials.button._new')
                </div>

        </div>
    </div>
    {{-- Danh sách nhân viên --}}
    <table class="table mt-4 table-bordered table-striped table-hover">
        <thead>
        <tr>
            <th style="text-align: center; color: black;" scope="col">STT</th>
            <th style="text-align: center; color: black;" scope="col">
                Tên trong hợp đồng   &nbsp;
            </th>
            <th style="text-align: center; color: black;" scope="col">
                Tên thường dùng   &nbsp;
            </th>
            <th style="text-align: center; color: black;" scope="col">
                Email
            </th>
            <th style="text-align: center; color: black;" scope="col">
                Quốc gia
            </th>
            <th style="text-align: center; color: black;" scope="col">
                Số điện thoại
            </th>
            <th style="text-align: center; color: black;" scope="col">
                Địa chỉ
            </th>
            <th style="text-align: center; color: black;" scope="col">
                Hành động
            </th>
        </tr>
        </thead>
        @forelse($data as $key => $rs)
            <tr>
                <th style="text-align: center;" scope="row">{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</th>
                <td>{!! boldTextSearchV2($rs->name,request('searchName')) !!}</td>
                <td>{!! boldTextSearchV2($rs->common_name,request('searchName')) !!}</td>
                <td>{!! boldTextSearchV2($rs->email,request('searchEmail')) !!}</td>
                <td>{{$rs->country_name}}</td>
                <td>{!! boldTextSearchV2($rs->phone,request('searchPhone')) !!}</td>
                <td>{{$rs->address}}</td>
                <td style="text-align: center;">
                    @php $id = $rs->id; @endphp
                    @include('layouts.partials.button._edit')
                    <form action="{{route('partner.delete',$rs->id)}}" method="post" style="display: inline">
                        @csrf
                        @include('layouts.partials.button._deleteForm')
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="8" class="text-center">
                Không có kết quả phù hợp
                </td>
            </tr>
            @endforelse
            </tbody>
    </table>

    @include('layouts.partials._paginate1')
@endsection
