@extends('layouts.master')
@section('title')
    Thêm tập phim mới
@endsection
@section('content')
{!! Form::open(array('route' => 'media.phim-le.store','method'=>'POST', 'class' => 'form-horizontal',  'autocomplete' => "off", 'id'=>'form-create-film')) !!}
    @include('admin.media.phim-le._formFilmLe')
    {!! Form::close() !!}
@endsection