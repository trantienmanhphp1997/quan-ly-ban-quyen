<div class="row">
        <div class="form-group">
            <label for=""></label>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Tên gốc (<span class="text-danger">*</span>)</label>
            <div class="col-sm-3 @error('vn_name') has-error @enderror">
            {!! Form::text('name', null, array('placeholder' => 'Tên gốc','class' => 'form-control')) !!}
                @error('name')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Tên tiếng Việt (<span class="text-danger">*</span>)</label>
            <div class="col-sm-3 @error('vn_name') has-error @enderror">
                {!! Form::text('vn_name', null, array('placeholder' => 'Tên bộ phim (có dấu)','class' => 'form-control')) !!}
                @error('vn_name')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label for="form-field-8" class="col-sm-2 control-label no-padding-right">Doanh thu phòng vé</label>
            <div class="col-sm-3 @error('box_office_revenue') has-error @enderror">
            {!! Form::text('box_office_revenue', null, array('placeholder' => 'Doanh thu phòng vé','class' => 'form-control')) !!}
                @error('box_office_revenue')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
            <label for="form-field-8" class="col-sm-2 control-label no-padding-right">Quyền</label>
            <div class="col-sm-3 @error('box_office_revenue') has-error @enderror">
            {!! Form::text('permission', null, array('placeholder' => 'Quyền','class' => 'form-control')) !!}
                @error('permission')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Năm sản xuất</label>
            <div class="col-sm-3">
                {!! Form::text('year_create', null, array('placeholder' => 'Năm sản xuất','class' => 'form-control')) !!}
                @error('year_create')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Thể loại</label>
            <div class="col-sm-3">
            {!! Form::text('category_film', null, array('placeholder' => 'Thể loại','class' => 'form-control')) !!}
                @error('category_film')
                    @include('layouts.partials.text._error')
                @enderror
      
                </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Diễn viên (<span class="text-danger">*</span>)</label>
            <div class="col-sm-3 @error('actor_name') has-error @enderror">
                {!! Form::text('actor_name', null, array('placeholder' => 'Diễn viên','class' => 'form-control')) !!}
                @error('actor_name')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Đạo diễn</label>
            <div class="col-sm-3 @error('director') has-error @enderror">
                {!! Form::text('director', null, array('placeholder' => 'Đạo diễn','class' => 'form-control')) !!}
                @error('director')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label for="form-field-8" class="col-sm-2 control-label no-padding-right">Thời lượng phim</label>
            <div class="col-sm-3 @error('count_minute') has-error @enderror">
            {!! Form::text('count_minute', null, array('placeholder' => 'Thời lượng phim','class' => 'form-control')) !!}
                @error('count_minute')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
            <label for="form-field-8" class="col-sm-2 control-label no-padding-right">Ghi chú   
            </label>
            <div class="col-sm-3 @error('note') has-error @enderror">
            {!! Form::text('note', null, array('placeholder' => 'Ghi chú','class' => 'form-control')) !!}
                @error('note')
                    @include('layouts.partials.text._error')
                @enderror  
            </div>
        </div>
        <div class="form-group">
            <label for="form-field-8" class="col-sm-2 control-label no-padding-right">Tên phim cha</label>
            <div class="col-sm-3 @error('film_name') has-error @enderror">
            {!! Form::text('film_name', isset($data)?$data->film_name:null, array('placeholder' => 'Tên phim cha','class' => 'form-control','disabled'=>'true')) !!}
                @error('film_name')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
        <div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                @include('layouts.partials.button._save')
                &nbsp; &nbsp; &nbsp;
                @include('layouts.partials.button._back')
            </div>
        </div>
    @include('layouts.partials.lib._date_picker')
    <br><br><br>
</div>