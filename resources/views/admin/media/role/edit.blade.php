@extends('layouts.master')

@section('title')
    Sửa quyền hạn
@endsection
@section('css')
    <style>
        .col-xs-12 ,.col-sm-12 ,.col-md-12{
            margin-bottom: 20px;
        }
        .table-input{
            margin-top:30px;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Sửa quyền hạn</h2>
            </div>

        </div>
    </div>
    {!! Form::open(array('route' => ['media.role.update',$role->id],'method'=>'POST')) !!}
    <div class="row table-input">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group @error('name') has-error @enderror">
                <strong class="col-md-2">Tên quyền(<span class="text-danger">*</span>)</strong>
                {!! Form::text('name', $role->name, array('placeholder' => 'Tên quyền','class' => 'col-md-4')) !!}
                @error('name')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group @error('code') has-error @enderror">
                <strong class="col-md-2">Mã quyền(<span class="text-danger">*</span>)</strong>
                {!! Form::text('code', $role->code, array('placeholder' => 'Mã quyền hạn','class' => 'col-md-4')) !!}
                @error('code')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong class="col-md-2">Quyền cha:</strong>
                <div class="col-md-4" style="padding: 0px;">
                    {!! Form::select('parent_id', $list_role, $role->parent_id, ['class' => 'form-control', 'id' => "form-field-select-1"]) !!}
                </div>
                @error('type')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong class="col-md-2">Loại quyền:</strong>
                {!! Form::select('type', [0=>'Chọn loại quyền',1=>'Độc quyền',2=>'Không độc quyền'],$role->type, ['class' => 'col-md-4',]) !!}
                @error('type')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
      {{--  <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong class="col-md-2">Danh mục:</strong>
                {!! Form::select('categori_id', $categories,$role->categori_id, array('class' => 'col-md-4', 'disabled'=>'true')) !!}
            </div>
        </div> --}}
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong class="col-md-2">Nước phát sóng</strong>
                <input name="nuoc" class="col-md-4" type="text" value="{{$role->nuoc}}" placeholder="Nước phát sóng">
             </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong class="col-md-2">Hạ tầng</strong>
                <div class="col-md-4">
                    {{ Form::checkbox('level', 1, $role->level, ['class' => 'form-control']) }}
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                @include('layouts.partials.button._save')
                &nbsp; &nbsp; &nbsp;
                @include('layouts.partials.button._back')
            </div>
        </div>
    {!! Form::close() !!}
@endsection
