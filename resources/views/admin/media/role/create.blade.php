@extends('layouts.master')

@section('title')
    Thêm quyền hạn
@endsection
@section('css')
    <style>
        .col-xs-12 ,.col-sm-12 ,.col-md-12{
            margin-bottom: 20px;
        }
        .table-input{
            margin-top:30px;
        }
    </style>
    @endsection
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Thêm mới quyền hạn</h2>
            </div>

        </div>
    </div>

    {!! Form::open(array('route' => 'media.role.store','method'=>'POST')) !!}
    <div class="row table-input">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group @error('name') has-error @enderror">
                <strong class="col-md-2">Tên quyền(<span class="text-danger">*</span>)</strong>
                {!! Form::text('name', null, array('placeholder' => 'Tên quyền','class' => 'col-md-4')) !!}
                @error('name')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group @error('code') has-error @enderror">
                <strong class="col-md-2">Mã quyền(<span class="text-danger">*</span>)</strong>
                {!! Form::text('code', null, array('placeholder' => 'Mã quyền hạn','class' => 'col-md-4')) !!}
                @error('code')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong class="col-md-2">Quyền cha</strong>
                <select name="parent_id" class="col-md-4">
                    <option value="0" {{old('parent_id')==0?"selected":''}}>---Quyền cha---</option>
                    @foreach($data_role as $value)
                        <option value="{{$value['id']}}" {{old('parent_id')==$value['id']?"selected":''}}>
                            {!! $value['text'].$value['name'] !!}
                        </option>
                    @endforeach
                </select>
                @error('type')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong class="col-md-2">Loại quyền</strong>
            {!! Form::select('type', [0=>'Chọn loại quyền',1=>'Độc quyền',2=>'Không độc quyền'],null, ['class' => 'col-md-4',]) !!}
            @error('type')
                @include('layouts.partials.text._error')
            @enderror
        </div>
    </div>
        <div class="col-xs-12 col-sm-12 col-md-12" style="display: none;">
            <div class="form-group">
                <strong class="col-md-2">Danh mục</strong>
                {!! Form::select('categori_id', $categories,2, array('class' => 'col-md-4')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong class="col-md-2">Nước phát sóng</strong>
{{--                <select name="country" class="select_country col-md-4">--}}
{{--                    <option value="">Chọn quốc gia...</option>--}}
{{--                    @foreach($countries as $key=> $item)--}}
{{--                        <option {{isset($data)&&$data->country== $key ? 'selected':''}} value="{{$item}}" if>{{$item}}</option>--}}
{{--                    @endforeach--}}
{{--                </select>--}}
                <input name="nuoc" class="col-md-4" type="text" placeholder="Nước phát sóng">
             </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong class="col-md-2">Hạ tầng</strong>
                <div class="col-md-4">
                    {{ Form::checkbox('level', 1, null, ['class' => 'form-control']) }}
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
            @include('layouts.partials.button._save')
            &nbsp; &nbsp; &nbsp;
            @include('layouts.partials.button._back')
        </div>
    </div>
    {!! Form::close() !!}
@endsection
