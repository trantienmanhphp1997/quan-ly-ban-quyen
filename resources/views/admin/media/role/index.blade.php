@extends('layouts.master')
@section('title')
    Danh sách quyền hạn
@endsection
@section('content')
    <div class="page-header">
        <div class="row">
            <div class="col-sm-12">
                <div class="widget-box">
                    <div class="widget-header">
                        <h4 class="widget-title">Tìm kiếm</h4>

                        <span class="widget-toolbar">
                            <a href="#" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up"></i>
                            </a>
                        </span>
                    </div>

                    <div class="widget-body">
                        <div class="widget-main">
                            {!! Form::open(['method'=>'GET','route'=>'media.role.index','role'=>'search'])  !!}
                            <div class="row">
                                <div class="col-xs-3 col-sm-3">
                                    <label for="form-field-select-1">Nhập từ khóa:</label>
                                    <input class="form-control" name="keyword" placeholder="Nhập tên quyền hoặc mã quyền..." type="text" value="{{request()->keyword}}" />
                                </div>
                            {{--    <div class="col-xs-3 col-sm-3">
                                    <label for="form-field-select-1">Danh mục:</label>
                                    
                                    <select name="categori_id" class="form-control">
                                        <option value="0">Chọn tất cả</option>
                                        <option value="1"@if(request()->categori_id==1) selected @endif>Nhạc</option>
                                        <option value="2"@if(request()->categori_id==2) selected @endif>Phim</option>
                                    </select>
                                </div> --}}
                                <div class="col-xs-3 col-sm-3">
                                    <label for="form-field-select-1">Loại quyền:</label>
                                    <select name="type" class="form-control">
                                        <option value="0">Chọn loại quyền</option>
                                        <option value="1"@if(request()->type==1) selected @endif>Độc quyền</option>
                                        <option value="2"@if(request()->type==2) selected @endif>Không độc quyền</option>
                                    </select>
                                </div>
                                <div class="col-xs-3 col-sm-3">
                                    <label for="form-field-select-1">Hạ tầng:</label>
                                    <select name="level" class="form-control">
                                        <option value="0">Chọn tất cả</option>
                                        <option value="1"@if(request()->level==1) selected @endif>Hạ tầng phát sóng</option>
                                    </select>
                                </div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-xs-6 col-sm-6">
                                    <button type="submit" class="btn btn-info btn-sm">
                                        <span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
                                        Tìm kiếm
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-sm-8">
            </div>
        </div>
    </div><!-- /.page-header -->
    <div class="row">
        <div class="col-xs-12 margin-tb">
            <div class="pull-left">
                <h2>Danh sách Quyền hạn - Hạ tầng</h2>
            </div>

                <div class="pull-right">
                    @include('layouts.partials.button._new')
                </div>
            
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <div class="row">
                <div class="col-xs-12">
                    <table id="simple-table" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="hidden-480" style="text-align: center; color: black;">STT</th>
                            <th style="text-align: center; color: black;">Tên quyền</th>
                            <th style="text-align: center; color: black;">Mã quyền</th>
                            <th style="text-align: center; color: black;">Quyền cha</th>
                        {{--   <th style="text-align: center; color: black;">Danh mục</th> --}}
                            <th style="text-align: center; color: black;">Loại quyền</th>
                            <th style="text-align: center; color: black;">Hạ tầng phát sóng</th>
                            <th style="text-align: center; color: black;">Người tạo</th>
                            <th style="text-align: center; color: black;">Nước phát sóng</th>
                            <th style="text-align: center; color: black;" width="280px">Hành động</th>
                        </tr>
                        </thead>

                        <tbody>
                        @forelse ($data as $key => $rs)
                            <tr>
                                <td style="text-align: center;">{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</td>
                                <td>{!! boldTextSearchV2($rs->name,request('keyword')) !!}</td>
                                <td>{!! boldTextSearchV2($rs->code,request('keyword')) !!}</td>
                                <td>{{ $rs->parent_name }}</td>
                                {{--<td>{{ $rs->cate_name }}</td> --}}
                                <td>@if($rs->type==1) độc quyền @elseif($rs->type==2) không độc quyền @else   @endif</td>
                                </td>
                                @if($rs->level == 1)
                                <td style="text-align: center;"><input class="form-check-input" type="checkbox" checked onclick="return false;"></td>
                                @else
                                <td style="text-align: center;"><input class="form-check-input" type="checkbox" onclick="return false;"></td>
                                @endif

                                <td>{{ $rs->user_name }}</td>
                                <td>{{$rs->nuoc}}</td>
                                <td style="text-align: center;">
                                    @php $id = $rs->id; @endphp
                                    @include('layouts.partials.button._edit')
                                    {!! Form::open(array('route' => ['media.role.delete',$rs->id],'method'=>'POST', 'style'=>'display:inline')) !!}
                                    @include('layouts.partials.button._deleteForm')
                                    {!! Form::close() !!}

                                </td>
                            </tr>
                            @empty
                                <tr>
                                    <td colspan="9" class="text-center"> Không có kết quả phù hợp</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                    @include('layouts.partials._paginate1')
                </div><!-- /.span -->
            </div><!-- /.row -->
        </div><!-- /.col -->
    </div><!-- /.row -->
{{--    <div class="modal fade" id="modal-form-delete-category" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">--}}
{{--        <div class="modal-dialog">--}}
{{--            <div class="modal-content">--}}
{{--                <div class="modal-header">--}}
{{--                    <h5 class="modal-title" id="exampleModalLabel">Xóa quyền</h5>--}}
{{--                </div>--}}
{{--                <div class="modal-body">--}}
{{--                    Bạn có muốn xóa không? Thao tác này không thể phục hồi!--}}
{{--                </div>--}}
{{--                <div class="modal-footer">--}}
{{--                    <button type="button" class="btn btn-primary" data-dismiss="modal">Không</button>--}}
{{--                    <button type="button" class="btn btn-danger" onclick="document.getElementById('delete').click()">Xóa bỏ</button>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
@endsection

