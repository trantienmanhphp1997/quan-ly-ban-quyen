@extends('layouts.master')
@section('title')
    Quản lý bản quyền phim
@endsection
@section('content')
    
    <div>
        <div class="form-group row">
            <form>
            <div class="col-lg-3" style="text-align: center;">
                <button class="btn deadline-3m" name="deadline" value="3" style="background: #FBA420 !important; border: 4px solid #FBA420;
                    margin-top: 5px;">
                    Sắp hết hạn trong vòng 3 tháng<br>
                    <span style="font-size: 25px;">{{$threeMonth}}</span>
                </button>
            </div>
            <div class="col-lg-3" style="text-align: center;">
                <button class="btn deadline-2m" name="deadline" value="2" style="background: #FB8441 !important; border: 4px solid #FB8441;
                    margin-top: 5px;" >
                    Sắp hết hạn trong vòng 2 tháng<br>
                    <span style="font-size: 25px;">{{$twoMonth}}</span>
                </button>
            </div>
            <div class="col-lg-3" style="text-align: center;">
                <button class="btn deadline-1m" name="deadline" value="1" style="background: #EE5816 !important; border: 4px solid #EE5816;  margin-top: 5px;">
                    Sắp hết hạn trong vòng 1 tháng<br>
                    <span style="font-size: 25px;">{{$oneMonth}}</span>
                </button>
            </div>
            <div class="col-lg-3" style="text-align: center;">
                <button class="btn deadline-1w" name="deadline" value="4" style="background: #FB1F0E !important; border: 4px solid #FB1F0E;  margin-right: 10px; margin-top: 5px;">
                    Sắp hết hạn trong vòng 1 tuần<br>
                    <span style="font-size: 25px;">{{$oneWeek}}</span>
                </button>
            </div>
            </form>
        </div>
    </div>
    <div class="page-header">
        <div class="row">
            <div class="col-sm-12">
                <div class="widget-box">
                    <div class="widget-header">
                        <h4 class="widget-title">Tìm kiếm</h4>

                        <span class="widget-toolbar">
                            <a href="#" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up"></i>
                            </a>
                        </span>
                    </div>

                    <div class="widget-body">
                        <div class="widget-main">
                            {!! Form::open(array('method'=>'GET','route'=>'media.films','role'=>'search','id'=>'form-search-QLBQ',))  !!}
                            <div class="row">
                                <div class="col-xs-4 col-sm-4">
                                    <label for="form-field-select-1">Tên sản phẩm</label>
                                    <input class="form-control" name="vn_name" type="text"
                                        value="{{request('vn_name')}}" id='input_vn_name' autocomplete="off" />
                                </div>
                                <div class="col-xs-4 col-sm-4">
                                    <label for="form-field-select-1">Tên diễn viên</label>
                                    <input class="form-control" name="actor_name" type="text"
                                        value="{{request('actor_name')}}" id='input_actor_name' autocomplete="off" />
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 d-flex" style="text-align:center;">
                                    <div style="float:left;">
                                        <button type="button" class="btn btn-info btn-sm" id='btn-submit-search-QLBQ' style="margin-top: 2px; outline: none;">
                                            <span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
                                            Tìm kiếm
                                        </button>
                                        <p class="text-danger" style="display: inline;" id='text-error-form'></p>
                                    </div>
                                    <div style="display: inline-block; float:right;" class="ml-auto">
                                        <div class="dropdown" style="display: inline-block;">
                                            <button class="btn btn-info btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background:#8080FF !important; border: 4px solid #8080FF; margin-top: 2px; outline: none;">
                                                <span class="menu-icon fa fa-plus" style="font-size: 18px;"></span>
                                                Tạo mới
                                                <span class="menu-icon fa fa-caret-down" style="font-size: 18px; margin-left: 2px;"></span>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton" style="min-width: 108px;">
                                                <div class="col-sm-12 div-hover" style="margin: 0px; padding: 0px; cursor: pointer;"
                                                    onclick="document.getElementById('a-create').click();">
                                                    <a class="dropdown-item" href="{{route('media.films.create')}}" id = 'a-create'
                                                        style="color: black; margin-left: 5px; text-decoration: none;">
                                                        Tạo mới<br>
                                                    </a>
                                                </div>
                                                <div class="col-sm-12 div-hover" style="margin: 0px; padding: 0px; cursor: pointer;"
                                                    data-target="#modal-form-upload-file" data-toggle="modal" id='div-upload'>
                                                    <a class="dropdown-item " id='a_upload'
                                                        style="color: black; margin-left: 5px; text-decoration: none;">
                                                        Tải lên từ Excel
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-outline-dark btn-sm" data-target="#modal-form-export-film" data-toggle="modal"
                                            style="background:#FFFFFF !important;color: #7F7F7F !important;
                                            border: 1px black solid; padding: 7px; margin-top: 2px; outline: none;">
                                            <span class="menu-icon fa fa-download" style="font-size: 18px;"></span>
                                            Excel file
                                        </button>
                                        <button type="button" class="btn btn-info btn-sm" data-target="#modal-form-delete-all-film" data-toggle="modal"
                                            id = 'btn-delete-all-film_1'
                                            style="background: #F46A6A !important; border: 4px solid #F46A6A; margin-top: 2px; outline: none;">
                                            <span class="menu-icon fa fa-trash-o" style="font-size: 18px;"></span>
                                            Xóa
                                        </button>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div><!-- /.page-header -->
    {{-- QLBQ --}}
    @if ($message = Session::get('message_success'))
        <div class="alert alert-success">
          <p>{{ $message }}</p>
        </div>
    @endif
    @if ($message = Session::get('message_danger'))
        <div class="alert alert-danger">
          <p>{{ $message }}</p>
        </div>
    @endif
    <div>
        <table class="table mt-4 table-bordered">
            <thead>
                <tr>
                    <th scope="col"></th>
                    <th scope="col">
                    <center style='color:black;'>Tên phim</center>
                    </th>
                    <th scope="col">
                        <center style='color:black;'>Tên diễn chính</center>
                    </th>
                    <th scope="col">
                        <center style='color:black;'>Thể loại</center>
                    </th>
                <th scope="col">
                        <center style='color:black;'>Số tập phim</center>
                    </th>
                    <th scope="col">
                        <center style='color:black;'>Quốc gia sản xuất</center>
                    </th>
                    <th scope="col">
                    <center style='color:black;'>Tên đối tác</center>
                    </th>
                    <th scope="col">
                    <center style='color:black;'>Hợp đồng số</center>
                    </th>
                    <th scope="col">
                    <center style='color:black;'>Năm sản xuất</center>
                    </th>
                    <th scope="col">
                    <center style='color:black;'>Thời hạn quản quyền</center>
                    </th>
                    <th scope="col">
                    <center style='color:black;'> Hành động</center>
                    </th>
                </tr>
            </thead>
            <tbody>
                <form name='form-delete-all-film' action='{{route('media.films.deleteAll')}}' method="POST">
                    @csrf {{method_field('DELETE')}}
                    @forelse($data as $key => $film)
                        <tr>
                            <th scope="row">
                                <center>
                                    <input type="checkbox" class="checkboxClick" value="{{$film->id}}" name="filmIds[]">
                                </center>
                            </th>
                            <td><center>{{$film->vn_name}}</center></td>
                            <td><center>{{$film->actor_name}}</center></td>
                            <td><center>{{$film->category_name}}</center></td>
                            <td><center>{{$film->count_tap}}</center></td>
                            <td><center>{{$film->country}}</center></td>
                            <td><center>{{$film->partner_name}}</center></td>
                            <td><center>{{$film->partner_name}}</center></td>
                            <td><center>{{$film->year_create}}</center></td>
                            <td><center>{{$film->end_time}}</center></td>
                            <td><center>
                                <button style="background: #fff; border: 1px solid black;margin-top: 2px;" type="button"data-target="#modal-form-upload-list-film" data-toggle="modal" data-id='{{$film->id}}' class='btn-upload-list-film'>
                                    <i class="fa fa-upload" style="font-size:16px; color: #2199EB;"></i>
                                </button>
                                <button style="background: #fff; border: 1px solid black;margin-top: 2px;" type="button" class="btn-edit-film btn-info" filmID='{{$film->id}}'>
                                    <i class="ace-icon fa fa-pencil bigger-120"></i>
                                </button>
                                <button type='button' style="background: #f46a6a;border: none; padding: 2px 9px; margin-top: 2px;" class="btn-delete-film"
                                    data-target="#modal-form-delete-film" data-toggle="modal" data-id='{{$film->id}}'>
                                    <i class="menu-icon fa fa-trash-o" style="color: white;font-size:16px;"></i>
                                </button>

                            </center></td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="11" class="text-center">Không có kết quả phù hợp</td>
                        </tr>
                    @endforelse
                </form>
            </tbody>
        </table>
    </div>
    @include('layouts.partials._pagination')
    {{-- Modal --}}
    <div class="modal fade" id="modal-form-upload-file" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Thêm file excel</h5>
                </div>
                <div class="modal-body">
                    <form action="{{route('media.films.upload')}}" method="POST" enctype="multipart/form-data" id='form-upload-file'>
                        {{csrf_field()}}
                        <div class="form-group">
                            <input type="file" id="file-upload" name='file' accept=".xlsx,.xls">
                        </div>
                        <input type="radio" class="checkboxClick" name="purchase_contract" value='buy' required>
                        <label for="vehicle1"> Hợp đồng mua</label><br>
                        <input type="radio" class="checkboxClick" name="purchase_contract" value='sale' required>
                        <label for="vehicle1"> Hợp đồng bán</label><br>
                    </form>
                </div>
                <div class="modal-footer" style="text-align: center;">
                    <a href="{{route('media.films.exportExampleFile')}}" style="float: left;">File excel mẫu &nbsp;</a>
                    <div style="float: left;">
                        <p class="text-danger" id='text-error'></p>
                    </div>
                    <div style="float: right;">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                        <button type="button" class="btn btn-primary" id='btn-save-file'>Lưu</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Modal export phim --}}
    <div class="modal fade" id="modal-form-export-film" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tải file excel xuống</h5>
                </div>
                <div class="modal-body">
                    Bạn có chắc chắn muốn xuất file không?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Quay lại</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" id='btn-upload-film' download>Đồng ý</button>
                </div>
            </div>
        </div>
    </div>

    {{-- modal xóa phim --}}
    <div class="modal fade" id="modal-form-delete-film" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Xóa phim</h5>
                </div>
                <div class="modal-body">
                    Bạn có muốn xóa không? Thao tác này không thể phục hồi!
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Không</button>
                    <button type="button" class="btn btn-danger" id='btn-delete-film'>Xóa bỏ</button>
                </div>
            </div>
        </div>
    </div>
    {{-- modal xóa tất cả phim được check --}}
    <div class="modal fade" id="modal-form-delete-all-film" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Xóa phim</h5>
                </div>
                <div class="modal-body">
                    Bạn có muốn xóa tất cả phim được chọn không? Thao tác này không thể phục hồi!
                </div>
                <div class="modal-footer" style="text-align: center;">
                    <div style="float: left;">
                        <p class="text-danger" id='modal-p-delete-all-film' style="display: inline-block;"></p>
                    </div>
                    <div style="float: right;">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Không</button>
                        <button type="button" class="btn btn-danger" id='btn-delete-all-film'>Xóa bỏ</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
   <div class="modal fade" id="modal-form-upload-list-film" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Thêm danh sách phim</h5>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="file" id="file-upload-list-film" name='file' accept=".xlsx,.xls">
                    </div>
                </div>
                <div class="modal-footer" style="text-align: center;">
                    <a href="{{route('media.films.exportExampleFile')}}" style="float: left;">File excel mẫu &nbsp;</a>
                    <div style="float: left;">
                        <p class="text-danger" id='text-error-list-film'></p>
                    </div>
                    <div style="float: right;">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                        <button type="button" class="btn btn-primary" id='btn-save-list-film'>Lưu</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- form delete phim --}}
    <div id="div-form-delelte">
        @foreach($data as $film)
            <form name='{{$film->id}}' action="{{route('media.films.delete',['id'=>$film->id])}}" method="POST">
                @csrf {{method_field('DELETE')}}
            </form>
        @endforeach
    </div>
    {{-- form edit film --}}
    <div id="div-form-edit">
        @foreach($data as $film)
            <form name='{{$film->id}}' action="{{route('media.films.edit',['id'=>$film->id])}}" method="GET">
            </form>
        @endforeach
    </div>
    {{-- form export file  --}}
    <form name='form-export-film' action="{{route('media.films.export')}}" method="GET">
        @csrf
    </form>
    {{-- form upload list film  --}}
    <div id='div-form-upload-list-film'>
        @foreach($data as $film)
            <form action="{{route('media.films.uploadListFilm',['id'=>$film->id])}}" method="POST" enctype="multipart/form-data" name='{{$film->id}}'>
                @csrf
            </form>
        @endforeach
    </div>
    
@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/QLBQ.css')}}">

@endsection

@section('js')
    <script src="{{asset('js/QLBQ.js')}}"></script>
@endsection
