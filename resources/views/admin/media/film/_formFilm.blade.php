<div class="row">
    <div class="form-group">
        <div class="col-xs-6">
            <div class="form-group">
                <label class="col-sm-4 control-label no-padding-right" for="form-field-8">Quốc gia sản xuất (<span  style='color:red;'>*</span>)</label>
                <div class="col-sm-6">
                    <select name="country" class="select_country_partner form-control">
                        <option value="">Chọn quốc gia...</option>
                        @foreach($countries as $key=> $item)
                            <option {{(old('country')!='')?(old('country')==$item?'selected':''):((isset($data)&&$data->country== $key) ? 'selected':'')}} value="{{$item}}">{{$item}}</option>
                        @endforeach
                    </select>
                    @error('country')
                        @include('layouts.partials.text._error')
                    @enderror
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="form-group">
                <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Tên phim (<span  style='color:red;'>*</span>)</label>
                <div class="col-sm-6 @error('vn_name') has-error @enderror">
                    {!! Form::text('vn_name', null, array('placeholder' => 'Tên phim (có dấu)','class' => 'form-control')) !!}
                    @error('vn_name')
                        @include('layouts.partials.text._error')
                    @enderror
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Nước phát sóng</label>
        <div class="col-sm-3">
            {!! Form::text('nuoc', null, array('placeholder' => 'Nước phát sóng','class' => 'form-control')) !!}
        </div>
        <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Tên phim tiếng anh</label>
        <div class="col-sm-3">
            {!! Form::text('product_name', null, array('placeholder' => 'Tên phim tiếng anh','class' => 'form-control')) !!}
            @error('product_name')
                @include('layouts.partials.text._error')
            @enderror
        </div>
    </div>
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Năm sản xuất (<span  style='color:red;'>*</span>)</label>
            <div class="col-sm-3">
                {!! Form::text('year_create', null, array('placeholder' => 'Năm sản xuất','class' => 'form-control')) !!}
                @error('year_create')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Thể loại</label>
            <div class="col-sm-3">
            <select name="cate_film[]" class="select_cate form-control" multiple>
            @foreach($cate_film as $key=> $item)
                <option {{(old('cate_film')!=''||old('edited'))?(old('cate_film')!=''&&in_array($item,old('cate_film'))?'selected':''):((isset($data)&&$cate_filmId->contains( $key)) ? 'selected':'')}} value="{{$item}}">{{$item}}</option>
            @endforeach
            </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Số đầu phim</label>
            <div class="col-sm-3">
                {!! Form::text('count_name', null, array('placeholder' => 'Số đầu phim','class' => 'form-control')) !!}
                @error('count_name')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Số tập phim (<span  style='color:red;'>*</span>)</label>
            <div class="col-sm-3">
                {!! Form::text('count_tap', null, array('placeholder' => 'Số tập phim','class' => 'form-control number_input format_number','id'=>'count_tap','maxlength' => "20",'onkeypress'=>"return isNumberKey(event)")) !!}
                @error('count_tap')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Thời lượng/tập</label>
            <div class="col-sm-3">
                {!! Form::text('count_hour', null, array('placeholder' => 'Thời lượng/tập','class' => 'form-control')) !!}
                @error('count_hour')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Đối tác cung cấp (<span style='color:red;'>*</span>)</label>
            <div class="col-sm-3">
            <!-- <p>{{old('partner_name')}}a </p> -->
                <select name="partner_name" class="select_partner form-control">
                    <option value="">Chọn đối tác...</option>
                    @foreach($partnerName as $key=> $item)
                        <option {{ old('partner_name')==$key ? 'selected':((isset($data)&&$data->partner_name== $key) ? 'selected':'')}} value="{{$key}}">{{$item}}</option>
                    @endforeach
                </select>
                @error('partner_name')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
    <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Quốc gia được phép phát sóng</label>
        <div class="col-sm-3">
            <select name="country_films[]" class="select_country_film form-control" multiple>
                @foreach($countries as $key=> $item)
                    <option {{(old('country_films')!=''||old('edited'))?(old('country_films')!=''&&in_array($item,old('country_films'))?'selected':''):((isset($data)&&$countryId->contains($key)) ? 'selected':'')}} value="{{$item}}" >{{$item}}</option>
                @endforeach
            </select></div>
        <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Số lần phát sóng</label>
        <div class="col-sm-3">
            {!! Form::text('count_live', null, array('placeholder' => 'Số lần phát sóng','class' => 'form-control')) !!}
            @error('count_live')
                    @include('layouts.partials.text._error')
            @enderror
        </div>
    </div>

        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Diễn viên</label>
            <div class="col-sm-3 @error('actor_name') has-error @enderror">
                {!! Form::text('actor_name', null, array('placeholder' => 'Diễn viên','class' => 'form-control')) !!}
                @error('actor_name')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Đạo diễn</label>
            <div class="col-sm-3">
                {!! Form::text('director', null, array('placeholder' => 'Đạo diễn','class' => 'form-control')) !!}
                @error('director')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
        <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Chi phí bản quyền (đơn giá/tập)</label>
            <div class="col-sm-3">
                {!! Form::text('license_fee', (isset($data)&&!$data->license_fee)?0:null, array('placeholder' => 'Chi phí bản quyền','class' => 'form-control number_input','id'=>'license_fee','onkeypress'=>"return isNumberKey(event)"))!!}
                @error('license_fee')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Chi phí hậu kì (đơn giá/tập)</label>
            <div class="col-sm-3">
                {!! Form::text('partner_fee', (isset($data)&&!$data->partner_fee)?0:null, array('placeholder' => 'Chi phí hậu kỳ','class' => 'form-control number_input','id'=>'partner_fee','onkeypress'=>"return isNumberKey(event)"))!!}

            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Chi phí kiểm duyệt (đơn giá/tập)</label>
            <div class="col-sm-3">
                {!! Form::text('other_fee', (isset($data)&&!$data->other_fee)?0:null, array('placeholder' => 'Chi phí kiểm duyệt','class' => 'form-control number_input','id'=>'other_fee','onkeypress'=>"return isNumberKey(event)")) !!}
                @error('other_fee')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Chi phí/tập</label>
            <div class="col-sm-3">
                {!! Form::text('total_fee_1', isset($data)?countChiPhiTap($data):null, array('placeholder' => 'Chi phí/tập','class' => 'form-control','id'=>'total_fee_1', 'readonly')) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Tổng chi phí</label>
            <div class="col-sm-3">
                {!! Form::text('total_fee', isset($data)?countChiPhi($data):null, array('placeholder' => 'Tổng chi phí','class' => 'form-control','id'=>'total_fee','readonly')) !!}
                @error('total_fee')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Số hợp đồng (<span  style='color:red;'>*</span>)</label>
            <div class="col-sm-3">
                @if(!isset($data))
                    <select name="contract_id" class="select_contract_number form-control">
                        <option value="">Chọn số hợp đồng...</option>
                        @foreach($contractName as $key=> $item)
                            <option {{ old('contract_id')==$key ? 'selected':((isset($data)&&$data->contract_id== $key) ? 'selected':'')}} value="{{$key}}">{{$item}}</option>
                        @endforeach
                    </select>
                @else 
                    {!! Form::text('contract_number', (isset($data,$contract_num))?$contract_num->contract_number:null, array((isset($data))? "readonly":'','class' => 'form-control','id'=>'input', 'onkeyup'=>'display()')) !!}
                @endif
                @error('contract_id')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
        <div class="form-group">
            @php $i=0; @endphp
            @foreach($revenueType as $type)
                @php  $i++; @endphp
                <label class="col-sm-2 control-label no-padding-right" for="form-field-8">DTTT {{$type->name}}</label>
                <div class="col-sm-3">
                    {!! Form::number('id'.$type->id, isset($data)?$data->revenueByType($type->id):'', array('placeholder' => 'Doanh thu tối thiếu theo tờ trình (Đơn giá/tập)' ,'class' => 'form-control DTTT type'.$type->getParent->id,'oninput' => "this.value = Math.abs(this.value)",'maxlength' => "20",)) !!}
                </div>
                @if($i%2==0)
                    </div>
                    <div class="form-group">
                @endif
            @endforeach
        </div>
        <div class="form-group">
            @php $i=0; @endphp
            @foreach($revenueParentType as $type)
                @php  $i++; @endphp
                <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Doanh thu {{$type->name}} (đơn giá/tập)</label>
                <div class="col-sm-3">
                    {!! Form::text(null, null, array('class' => 'form-control','id'=>'sum'.$type->id, 'disabled')) !!}
                </div>
                @if($i%2==0)
                    </div>
                    <div class="form-group">
                @endif
            @endforeach


        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">DTTT theo tờ trình (đơn giá/tập) </label>
            <div class="col-sm-3">
                <input type="text" class="form-control" id='sumDTTT' disabled="">
            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Tổng DTTT theo tờ trình</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" id='sumDTTT2' disabled="">
            </div>
        </div>
    <div id="role" style="display:@if(isset($data))block else none @endif">
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Độc quyền (<span id = 'spanDocQuyen'></span>)</label>
            <div class="col-sm-3">
                <select id='selectDocQuyen' name="role1[]" class="select_box form-control" multiple>
                {(old('cate_film')!='')?(in_array($item,old('cate_film'))?'selected':''):((isset($data)&&$cate_filmId->contains( $key)) ? 'selected':'')}}
                    @foreach($data_type1 as $value)
                        <option value="{{$value['id']}}"
                            @if(old('role1')!=''||old('edited'))
                                @if(old('role1')!=''&&in_array($value['id'],old('role1')))
                                    selected="selected"
                                @endif
                            @else
                                @if(isset($data))
                                    @foreach($data->roles->where('type',1) as $role)
                                        @if($value['id'] == $role->id) selected="selected" @endif
                                    @endforeach
                                @endif
                            @endif
                        >
                            {!!$value['text'].$value['name']  !!}
                        </option>
                    @endforeach
                </select>
            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Không độc quyền (<span id = 'spanKoDocQuyen'></span>)</label>
            <div class="col-sm-3">
                <select id='selectKoDocQuyen' name="role2[]" class="select_box form-control" multiple>
                    @foreach($data_type2 as $value)
                        <option value="{{$value['id']}}"
                            @if(old('role2')!=''||old('edited'))
                                @if(old('role2')!=''&&in_array($value['id'],old('role2')))
                                    selected="selected"
                                @endif
                            @else
                                @if(isset($data))
                                    @foreach($data->roles->where('type',2) as $role)
                                        @if($value['id'] == $role->id) selected="selected" @endif
                                    @endforeach
                                @endif
                            @endif
                        >
                            {!!$value['text'].$value['name']  !!}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div id="role" style="display:@if(isset($data))block else none @endif">
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Hạ tầng phát sóng</label>
            <div class="col-sm-3">
                <select name="role3[]" class="select_box_HT form-control" multiple>
                    @foreach($data_type3 as $value)
                        <option value="{{$value['id']}}"
                            @if(old('role3')!=''||old('edited'))
                                @if(old('role3')!=''&&in_array($value['id'],old('role3')))
                                    selected="selected"
                                @endif
                            @else
                                @if(isset($data))
                                    @foreach($data->roles->where('type',2) as $role)
                                        @if($value['id'] == $role->id) selected="selected" @endif
                                    @endforeach
                                @endif
                            @endif
                        >
                            {!!$value['text'].$value['name']  !!}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Ngày bắt đầu bản quyền(<span  style='color:red;'>*</span>)</label>
        <div class="col-sm-3">
            <div class="input-group">
                {!! Form::text('start_time', isset($data)? reFormatDate($data->start_time, 'd-m-Y') : null, array('placeholder' => 'Ngày bắt đầu','class' => 'form-control date-picker', 'id' => "id-date-picker-1", "data-date-format"=>"dd-mm-yyyy")) !!}
                @include('layouts.partials.input._calender')
            </div>
            @error('start_time')
                @include('layouts.partials.text._error')
            @enderror
        </div>
        <div class="col-xs-6">
            <div class="form-group">
                <label class="col-sm-4 control-label no-padding-right" for="form-field-8">Ngày kết thúc(<span  style='color:red;'>*</span>)</label>
                <div class="col-sm-6 @error('vn_name') has-error @enderror">
                    <div class="input-group">
                        {!! Form::text('end_time', isset($data)? reFormatDate($data->end_time, 'd-m-Y') : null, array('placeholder' => 'Ngày kết thúc','class' => 'form-control date-picker', 'id' => "id-date-picker-2", "data-date-format"=>"dd-mm-yyyy")) !!}
                        @include('layouts.partials.input._calender')
                    </div>
                    @error('end_time')
                    @include('layouts.partials.text._error')
                    @enderror
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group">
                <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Nội dung phim</label>
                <div class="col-sm-8">
                    {!! Form::textarea('note', null, array('placeholder' => 'Nội dung phim','class' => 'form-control')) !!}
                    @error('note')
                        @include('layouts.partials.text._error')
                    @enderror
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group">
                <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Ghi chú phim</label>
                <div class="col-sm-8">
                    {!! Form::textarea('addition_note', null, array('placeholder' => 'Ghi chú phim','class' => 'form-control')) !!}
                </div>
            </div>
        </div>
        <input type='hidden' value='true' name='edited'>
    </div>

    <br><br>

        <div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                @include('layouts.partials.button._save')
                &nbsp; &nbsp; &nbsp;
                @include('layouts.partials.button._back')
            </div>
        </div>
    @include('layouts.partials.lib._date_picker')
    <br><br><br>
    <input type="hidden" id="parent_type" value="{{$revenueParentTypeJson}}" name="">
</div>
<script>
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57))
            return false;
    
        return true;
    }

    function calculateSum() {
        var parent_type = [];
        parent_type = jQuery.parseJSON($("#parent_type").val());

        for (var i = 0; i < parent_type.length; i++) {
            var sum = 0;
            $(".type"+parent_type[i]).each(function() {
                if (!isNaN(this.value) && this.value.length != 0) {
                    sum += parseFloat(this.value);
                }
            });
            $("input#sum"+parent_type[i]).val(sum);
        }
    }

    function calculateSumDTTT() {
        var sum = 0;
        //iterate through each textboxes and add the values
        $(".DTTT").each(function() {
            if (!isNaN(this.value) && this.value.length != 0) {
                sum += parseFloat(this.value);
            }
        });

        $("input#sumDTTT").val(sum);
    }
    function calculateSumDTTT2() {
        var sum = 0;
        //iterate through each textboxes and add the values
        $(".DTTT").each(function() {
            if (!isNaN(this.value) && this.value.length != 0) {
                count_tap = $('#count_tap').val().replaceAll(',','')
                if(!count_tap||count_tap=='.'){
                    count_tap = 0
                } 
                sum += parseFloat(this.value)*parseFloat(count_tap);
            }
        });

        $("input#sumDTTT2").val(sum);
    }
    function maxLengthCheck(object) {
        if (object.value.length > object.maxLength)
        object.value = object.value.slice(0, object.maxLength)
    }
    $("document").ready(function() {

        let countDocQuyen = $("#selectDocQuyen :selected").length;
        $('#spanDocQuyen').text(countDocQuyen);
        let countKoDocQuyen = $("#selectKoDocQuyen :selected").length;
        $('#spanKoDocQuyen').text(countKoDocQuyen);
        $('#selectDocQuyen').select2({closeOnSelect: false}).on("change", function(e){
            countDocQuyen = $("#selectDocQuyen :selected").length;
            $('#spanDocQuyen').text(countDocQuyen);
        });
        $('#selectKoDocQuyen').select2({closeOnSelect: false}).on("change", function(e){
            countKoDocQuyen = $("#selectKoDocQuyen :selected").length;
            $('#spanKoDocQuyen').text(countKoDocQuyen);
        });



        calculateSum();
        calculateSumDTTT();
        calculateSumDTTT2();
        $(".DTTT").on("keydown keyup", function() {
            calculateSum();
            calculateSumDTTT();
            calculateSumDTTT2();
        });
        $("#count_tap").on("keydown keyup", function() {
            calculateSumDTTT2();
        });
        initValue()
        $('.number_input').keyup(function(){
            formatCurrency($(this));
            if($(this).val()=='') {
                $(this).val(0)
            }
            var count_tap = $('#count_tap').val()
            var license_fee = $('#license_fee').val()
            var partner_fee = $('#partner_fee').val()
            var other_fee = $('#other_fee').val()
            count_tap = count_tap.replaceAll(',','')
            license_fee = license_fee.replaceAll(',','')
            partner_fee = partner_fee.replaceAll(',','')
            other_fee = other_fee.replaceAll(',','')
            if(!checkNull(count_tap)&&!checkNull(license_fee)&&!checkNull(partner_fee)&&!checkNull(other_fee)){
                var total_fee_1 = parseFloat(license_fee)+parseFloat(partner_fee)+parseFloat(other_fee)
                $('#total_fee_1').val(total_fee_1)
                if(!count_tap||count_tap=='.'){
                    count_tap = 0
                }
                var multi = total_fee_1*count_tap+""
                if (multi.indexOf(".") >= 0) {
                    var decimal_pos = multi.indexOf(".");
                    var left_side = multi.substring(0, decimal_pos);
                    var right_side = multi.substring(decimal_pos);
                    right_side = right_side.substring(1, 4);
                    if(right_side=='000'){
                        multi = left_side
                    }
                    else {
                        multi = left_side + "." + right_side;
                    }
                }
                $('#total_fee').val(multi);
                formatCurrency($('#total_fee'))
                formatCurrency($('#total_fee_1'))
            }
        })
    })
    function checkNull(str){
        if(str==''||str==null){
            return true
        }
        else {
            return false
        }
    }
    function initValue(){
        formatCurrency($('#count_tap'))
        formatCurrency($('#license_fee'))
        formatCurrency($('#partner_fee'))
        formatCurrency($('#other_fee'))
        formatCurrency($('#total_fee_1'))
        formatCurrency($('#total_fee'))
    }
</script>
