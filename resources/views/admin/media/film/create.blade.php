@extends('layouts.master')

@section('title')
    Thêm mới phim
@endsection
@section('css')
    <link rel="stylesheet" href="{{asset('assets/select2/select2.min.css')}}" >
    <style>
        .select2-search--inline::after{
            margin-left:150px; !important;
        }
    </style>
@endsection
@section('content')
    <h2>Thêm mới phim</h2>
    <hr>
    {!! Form::open(array('route' => 'media.films.store','method'=>'POST', 'class' => 'form-horizontal',  'autocomplete' => "off", 'id'=>'form-create-film')) !!}
    @include('admin.media.film._formFilm')
    {!! Form::close() !!}
    <!-- inline scripts related to this page -->
@endsection
@include('admin.media.film._js')
