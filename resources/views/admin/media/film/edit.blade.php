@extends('layouts.master')

@section('title')
    Cập nhật Phim
@endsection
@section('css')
    <link rel="stylesheet" href="{{asset('assets/select2/select2.min.css')}}" >

    <style>
        .select2-search--inline::after{
            margin-left:150px; !important;
        }
    </style>
@endsection
@section('content')
<div class="main-container">
    <h2>Cập nhật phim</h2>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">Thông tin phim</a></li>
        <li><a data-toggle="tab" href="#menu1">Giấy tờ phê duyệt</a></li>
    </ul>
    <div class="tab-content">
        <div id="home" class="tab-pane fade in active">
            {!! Form::model($data, ['method' => 'POST', 'id' => 'myForm', 'class' => 'form-horizontal',  'autocomplete' => "off",'route' => ['media.films.update', $data->id]]) !!}
            @include('admin.media.film._formFilm')
            {!! Form::close() !!}
        </div>
        @include('admin.media.film._menu1')
    </div>
</div>
@endsection
@include('admin.media.film._js')


    
