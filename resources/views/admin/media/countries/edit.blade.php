@extends('layouts.master')

@section('title')
    Sửa quốc gia
@endsection
@section('css')
    <link rel="stylesheet" href="{{asset('assets/select2/select2.min.css')}}" >
    <style>
        .select2-search--inline::after{
            margin-left:150px; !important;
        }
    </style>
@endsection
@section('content')
<div class="main-container">
    <h2>Sửa quốc gia</h2>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">Thông tin quốc gia</a></li>
    </ul>
    <div class="tab-content">
        <div id="home" class="tab-pane fade in active">
        {!! Form::model($data,array('route' => ['media.countries.update',$data->id],'method'=>'POST', 'class' => 'form-horizontal',  'autocomplete' => "off", 'id'=>'form-create-film')) !!}
    @include('admin.media.countries._formEditCountries')
    {!! Form::close() !!}
        </div>

    </div>
</div>
@endsection
