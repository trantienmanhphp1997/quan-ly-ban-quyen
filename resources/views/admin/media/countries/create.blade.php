@extends('layouts.master')
@section('title')
    Thêm quốc gia 
@endsection
@section('content')
<h1>Thêm quốc gia mới</h1>
{!! Form::open(array('route' => 'media.countries.store','method'=>'POST', 'class' => 'form-horizontal',  'autocomplete' => "off", 'id'=>'form-create-film')) !!}
    @include('admin.media.countries._formCountries')
    {!! Form::close() !!}
@endsection