@extends('layouts.master')
@section('title')
    Quản lý quốc gia
@endsection
@section('content')
    <div class="page-header">
        <div class="row">
            <div class="col-sm-12">
                <div class="widget-box">
                    <div class="widget-header">
                        <h4 class="widget-title">Tìm kiếm</h4>

                        <span class="widget-toolbar">
                            <a href="#" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up"></i>
                            </a>
                        </span>
                    </div>

                    <div class="widget-body">
                        <div class="widget-main">
                            {!! Form::open(array('method'=>'GET','route'=>'media.countries','role'=>'search','id'=>'form-search-QLBQ',))  !!}
                            <div class="row">
                                <div class="col-xs-4 col-sm-4">
                                    <label for="form-field-select-1">Mã quốc gia</label>
                                    <input class="form-control" name="searchCode" type="text" placeholder="Nhập mã quốc gia"
                                           value="{{request()->searchCode}}" id='input_vn_name' autocomplete="off" />
                                </div>
                                <div class="col-xs-4 col-sm-4">
                                    <label for="form-field-select-1">Tên quốc gia</label>
                                    <input class="form-control" name="searchName" type="text" placeholder="Nhập tên quốc gia"
                                        value="{{request()->searchName}}" id='input_vn_name' autocomplete="off" />
                                </div>
                            </div><br>
                            <div class="row">
                                            <div class="col-xs-6 col-sm-6">
                                                <button type="submit" class="btn btn-info btn-sm">
                                                    <span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
                                                Tìm kiếm
                                                </button>
                                            </div>
                                    </div>
                            <hr>
                        
                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div><!-- /.page-header -->
    {{-- QLBQ --}}
    @if ($message = Session::get('message_success'))
        <div class="alert alert-success">
          <p>{{ $message }}</p>
        </div>
    @endif
    @if ($message = Session::get('message_danger'))
        <div class="alert alert-danger">
          <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table mt-4 table-bordered table-striped table-hover">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Danh sách quốc gia</h2>
            </div>
            <div class="pull-right">
                @include('layouts.partials.button._new')
            </div>
        </div>
        <thead>
            <tr>
                <th scope="col">
                <center style ="color:black">STT</center>
                </th>
                <th scope="col">
                   <center style='color:black;'>Tên quốc gia</center>
                </th>
                <th scope="col">
                    <center style='color:black;'>Mã quốc gia</center>
                </th>
                <th scope="col">
                   <center style='color:black;'> Hành động</center>
                </th>
            </tr>
        </thead>
        <tbody>
            
                @csrf {{method_field('DELETE')}}

                @forelse($data as $key => $rs)
                    <tr>
                        <th scope="row">
                            <center>{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</center>
                        </th>
                        <td><center>{!! boldTextSearchV2($rs->name,request('searchName')) !!}</center></td>
                        <td><center>{!! boldTextSearchV2($rs->code,request('searchCode')) !!}</center></td>

                        <td><center>
                            @php $id = $rs->id; @endphp
                            @include('layouts.partials.button._edit')
                            {!! Form::open(array('route' => ['media.countries.delete',$rs->id],'method'=>'POST', 'style'=>'display:inline')) !!}
                                @include('layouts.partials.button._deleteForm')
                            {!! Form::close() !!}

                        </center></td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="17" class="text-center">Không có kết quả phù hợp</td>
                    </tr>
                @endforelse
        </tbody>
    </table>
    @include('layouts.partials._paginate1')

    {{-- Modal --}}
    <div class="modal fade" id="modal-form-upload-file" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Thêm file excel</h5>
                </div>
                <div class="modal-body">
                    <form action="{{route('media.manage.upload')}}" method="POST" enctype="multipart/form-data" id='form-upload-file'>
                        {{csrf_field()}}
                        <div class="form-group">
                            <input type="file" id="file-upload" name='file' accept=".xlsx,.xls">
                        </div>
                    </form>
                </div>
                <div class="modal-footer" style="text-align: center;">
                    <a href="{{route('media.manage.exportExampleFile')}}" style="float: left;">File excel mẫu &nbsp;</a>
                    <div style="float: left;">
                        <p class="text-danger" id='text-error'></p>
                    </div>
                    <div style="float: right;">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                        <button type="button" class="btn btn-primary" id='btn-save-file-backup'>Lưu</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Modal export phim --}}
    <div class="modal fade" id="modal-form-export-film" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tải file excel xuống</h5>
                </div>
                <div class="modal-body">
                    Bạn có chắc chắn muốn xuất file không?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Quay lại</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" id='btn-upload-film-backup' download>Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
    {{-- modal xóa phim --}}
    <!-- <div class="modal fade" id="modal-form-delete-film" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Xóa phim</h5>
                </div>
                <div class="modal-body">
                    Bạn có muốn xóa không? Thao tác này không thể phục hồi!
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Không</button>
                    <button type="button" class="btn btn-danger" id='btn-delete-film'>Xóa bỏ</button>
                </div>
            </div>
        </div>
    </div> -->
    {{-- modal xóa tất cả phim được check --}}
    <div class="modal fade" id="modal-form-delete-all-film" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Xóa phim</h5>
                </div>
                <div class="modal-body">
                    Bạn có muốn xóa tất cả phim được chọn không? Thao tác này không thể phục hồi!
                </div>
                <div class="modal-footer" style="text-align: center;">
                    <div style="float: left;">
                        <p class="text-danger" id='modal-p-delete-all-film' style="display: inline-block;"></p>
                    </div>
                    <div style="float: right;">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Không</button>
                        <button type="button" class="btn btn-danger" id='btn-delete-all-film'>Xóa bỏ</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
   <div class="modal fade" id="modal-form-upload-list-film" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Thêm danh sách phim</h5>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="file" id="file-upload-list-film" name='file' accept=".xlsx,.xls">
                    </div>
                </div>
                <div class="modal-footer" style="text-align: center;">
                    <a href="{{route('media.films.exportExampleFile')}}" style="float: left;">File excel mẫu &nbsp;</a>
                    <div style="float: left;">
                        <p class="text-danger" id='text-error-list-film'></p>
                    </div>
                    <div style="float: right;">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                        <button type="button" class="btn btn-primary" id='btn-save-list-film'>Lưu</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- form delete phim --}}
{{--     <div id="div-form-delelte">
        @foreach($data as $film)
            <form name='{{$film->id}}' action="{{route('media.films.delete',['id'=>$film->id])}}" method="POST">
                @csrf {{method_field('DELETE')}}
            </form>
        @endforeach
    </div> --}}
    {{-- form edit film --}}
{{--     <div id="div-form-edit">
        @foreach($data as $film)
            <form name='{{$film->id}}' action="{{route('media.films.edit',['id'=>$film->id])}}" method="GET">
            </form>
        @endforeach
    </div> --}}
    {{-- form export file  --}}
    <form name='form-export-film-backup' action="{{route('media.manage.export')}}" method="GET">
        @csrf
    </form>
    {{-- form upload list film  --}}
{{--     <div id='div-form-upload-list-film'>
        @foreach($data as $film)
            <form action="{{route('media.films.uploadListFilm',['id'=>$film->id])}}" method="POST" enctype="multipart/form-data" name='{{$film->id}}'>
                @csrf
            </form>
        @endforeach
    </div> --}}

@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/QLBQ.css')}}">

@endsection

@section('js')
    <script src="{{asset('js/QLBQ.js')}}"></script>
@endsection

