<div class="row">
        <div class="form-group">
            <label for=""></label>
            <label class="col-sm-2 control-label no-padding-left" for="form-field-8">Tên quốc gia(<span class="text-danger">*</span>)</label>
            <div class="col-sm-3 @error('name') has-error @enderror">
            {!! Form::text('name', null, array('placeholder' => 'Tên quốc gia','class' => 'form-control')) !!}
                @error('name')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
            <label class="col-sm-2 control-label no-padding-left" for="form-field-8">Mã quốc gia(<span class="text-danger">*</span>)</label>
            <div class="col-sm-3 @error('code') has-error @enderror">
                {!! Form::text('code', null, array('placeholder' => 'Mã quốc gia','class' => 'form-control')) !!}
                @error('code')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>

       

        <div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                @include('layouts.partials.button._save')
                &nbsp; &nbsp; &nbsp;
                @include('layouts.partials.button._back')
            </div>
        </div>
    @include('layouts.partials.lib._date_picker')
    <br><br><br>
</div>