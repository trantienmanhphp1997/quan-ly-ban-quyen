@extends('layouts.master')
@section('title')
    Thêm tư liệu mới
@endsection
@section('content')
<h1>Thêm mới tư liệu</h1>
{!! Form::open(array('route' => 'media.manage.store','method'=>'POST', 'class' => 'form-horizontal',  'autocomplete' => "off", 'id'=>'form-create-film')) !!}
    @include('admin.media.manage._formManage')
    {!! Form::close() !!}
@endsection