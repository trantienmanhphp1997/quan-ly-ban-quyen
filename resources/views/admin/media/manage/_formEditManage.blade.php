
<div class="row">
    <div class="form-group">
        <label for=""></label>
        <label class="col-sm-2 control-label no-padding-left" for="form-field-8">Mã ổ (<span class="text-danger">*</span>)</label>
        <div class="col-sm-3 @error('driver') has-error @enderror">
        {!! Form::text('driver', null, array('placeholder' => 'Mã ổ','class' => 'form-control')) !!}
            @error('driver')
                @include('layouts.partials.text._error')
            @enderror
        </div>
        <label class="col-sm-2 control-label no-padding-left" for="form-field-8">Ổ backup(<span class="text-danger">*</span>) </label>
        <div class="col-sm-3 @error('backup_driver') has-error @enderror">
            {!! Form::text('backup_driver', null, array('placeholder' => 'Ổ backup ','class' => 'form-control')) !!}
            @error('backup_driver')
                @include('layouts.partials.text._error')
            @enderror
        </div>
    </div>
    <div class="form-group">
        <label for="form-field-8" class="col-sm-2 control-label no-padding-right"> Tên phim Tiếng Việt(<span class="text-danger">*</span>)</label>
        <div class="col-sm-3 @error('vn_name') has-error @enderror">
        {!! Form::text('vn_name', null, array('placeholder' => 'Tên phim Tiếng Việt','class' => 'form-control')) !!}
            @error('vn_name')
                @include('layouts.partials.text._error')
            @enderror
        </div>
        <label for="form-field-8" class="col-sm-2 control-label no-padding-right"> Quốc gia(<span class="text-danger">*</span>)</label>
        <div class="col-sm-3 @error('country') has-error @enderror">
        {!! Form::text('country', null, array('placeholder' => 'Quốc gia','class' => 'form-control')) !!}
            @error('country')
                @include('layouts.partials.text._error')
            @enderror
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Đối tác</label>
        <div class="col-sm-3">
            {!! Form::text('partner', null, array('placeholder' => 'Đối tác','class' => 'form-control')) !!}
            @error('partner')
                @include('layouts.partials.text._error')
            @enderror
        </div>
        <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Thời hạn bản quyền(<span class="text-danger">*</span>)</label>
        <div class="col-sm-3 @error('end_time') has-error @enderror">
        <div class="input-group">
                {!! Form::text('end_time', isset($data)? reFormatDate($data->end_time, 'd-m-Y') : null, array('placeholder' => 'Ngày kết thúc','class' => 'form-control date-picker', 'id' => "id-date-picker-1", "data-date-format"=>"dd-mm-yyyy")) !!}
                @include('layouts.partials.input._calender')
            </div>
            @error('end_time')
                @include('layouts.partials.text._error')
            @enderror
        </div>    
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Dung lượng (<span class="text-danger">*</span>)</label>
        <div class="col-sm-3 @error('volumn') has-error @enderror">
            {!! Form::text('volumn', null, array('placeholder' => 'Dung lượng','class' => 'form-control')) !!}
            @error('volumn')
                @include('layouts.partials.text._error')
            @enderror
        </div>
        
        <label for="form-field-8" class="col-sm-2 control-label no-padding-right">Đuôi file</label>
        <div class="col-sm-3 @error('file_format') has-error @enderror">
        {!! Form::text('file_format', null, array('placeholder' => 'Đuôi file','class' => 'form-control')) !!}
            @error('file_format')
                @include('layouts.partials.text._error')
            @enderror
        </div>
    </div>
       
    <div class="form-group">
        <label for="form-field-8" class="col-sm-2 control-label no-padding-right">Thể loại
        </label>
        
        <div class="col-md-3">
            <select name="series_film" class="col-md-12" >
                <option value="0" {{($data->series_film==0)?'selected':''}}>--Chọn thể loại--</option>
                <option value="1" {{($data->series_film==1)?'selected':''}}>Phim bộ</option>
                <option value="2" {{($data->series_film==2)?'selected':''}}>Phim lẻ</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Tên phim tiếng anh</label>
            <div class="col-sm-3 @error('film_name') has-error @enderror">
            {!! Form::text('film_name', null, array('placeholder' => 'Tên phim tiếng anh','class' => 'form-control')) !!}
            @error('film_name')
                @include('layouts.partials.text._error')
            @enderror

            </div>
        <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Tên file(<span class="text-danger">*</span>)</label>
            <div class="col-sm-3 @error('file_name') has-error @enderror">
            {!! Form::text('file_name', null, array('placeholder' => 'Tên file','class' => 'form-control')) !!}
            @error('file_name')
                @include('layouts.partials.text._error')
            @enderror
            </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Số tập</label>
            <div class="col-sm-3 @error('count_film') has-error @enderror">
            {!! Form::text('count_film', null, array('placeholder' => 'Số tập','class' => 'form-control')) !!}
            @error('count_film')
                @include('layouts.partials.text._error')
            @enderror
            </div>
        <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Loại file</label>
            <div class="col-sm-3 @error('file_type') has-error @enderror">
            {!! Form::text('file_type', null, array('placeholder' => 'Loại file','class' => 'form-control')) !!}
            @error('file_type')
                @include('layouts.partials.text._error')
            @enderror
            </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Thời lượng(<span class="text-danger">*</span>)</label>
            <div class="col-sm-3 @error('time_number') has-error @enderror">
            {!! Form::text('time_number', null, array('placeholder' => 'Thời lượng','class' => 'form-control')) !!}
            @error('time_number')
                @include('layouts.partials.text._error')
            @enderror
            </div>
        <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Người giao</label>
                <div class="col-sm-3 @error('file_delivery_person') has-error @enderror">
                {!! Form::text('file_delivery_person', null, array('placeholder' => 'Người giao','class' => 'form-control')) !!}
                @error('file_delivery_person')
                    @include('layouts.partials.text._error')
                @enderror
                </div>
        </div>
    </div>

    <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
            @include('layouts.partials.button._save')
            &nbsp; &nbsp; &nbsp;
            @include('layouts.partials.button._back')
        </div>
    </div>
    @include('layouts.partials.lib._date_picker')
    <br><br><br>
</div>