
@extends('layouts.master')
@section('title')
    Quản trị tư liệu
@endsection
@section('content')
<livewire:media.manage.manage/>
@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/QLBQ.css')}}">

@endsection

@section('js')
    <script src="{{asset('js/QLBQ.js')}}"></script>
@endsection

