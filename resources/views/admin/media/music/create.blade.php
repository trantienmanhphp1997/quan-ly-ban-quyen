@extends('layouts.master')

@section('title')
    Thêm mới nhạc
@endsection
@section('css')
    <link rel="stylesheet" href="{{asset('assets/select2/select2.min.css')}}" >
    <style>
        .select2-search--inline::after{
            margin-left:150px; !important;
        }
        .select2-container{
            width: 267px !important;
        }
    </style>
@endsection
@section('content')

    <h2>Thêm mới nhạc</h2>
    <hr>
    @include('admin.media.music._formMusic')
    <!-- inline scripts related to this page -->
@endsection
