<div id="home" class="tab-pane fade in active">
    {!! Form::model($data, ['method' => 'POST', 'class' => 'form-horizontal','route' => ['media.music.updateContract', $data->id]]) !!}
        @method('put')
        @csrf
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Tên bài hát</label>
            <div class="col-sm-3">
                {!! Form::text('name', null, array('placeholder' => 'TÊN BÀI HÁT (có dấu)','class' => 'form-control', 'readonly' => 'true')) !!}
            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">CA SĨ THỂ HIỆN</label>
            <div class="col-sm-3">
                {!! Form::text('singer', null, array('placeholder' => 'CA SĨ THỂ HIỆN)','class' => 'form-control', 'readonly' => 'true')) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Số hợp đồng Viettel mua</label>
            <div class="col-sm-3">
                {!! Form::text('ghi_am', null, array('placeholder' => 'Số hợp đồng Viettel mua','class' => 'form-control', 'readonly' => 'true')) !!}
            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Quyền tác giả</label>
            <div class="col-sm-3">
                {!! Form::text('tac_gia', null, array('placeholder' => 'Quyền tác giả','class' => 'form-control' , 'readonly' => 'true')) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Số Phụ lục HĐ (Số HĐ của Viettel và CPs)</label>
            <div class="col-sm-3">
                {!! Form::text('contract_cps', null, array('placeholder' => 'Số Phụ lục HĐ (Số HĐ của Viettel và CPs))','class' => 'form-control', 'readonly' => 'true')) !!}
            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Số HĐ Quyền liên quan (biểu diễn, ghi âm)</label>
            <div class="col-sm-3">
                {!! Form::text('contract_number1', null, array('placeholder' => 'Số HĐ Quyền liên quan (biểu diễn, ghi âm)','class' => 'form-control', 'readonly' => 'true')) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Số Phụ lục HĐ Quyền liên quan (biểu diễn, ghi âm)</label>
            <div class="col-sm-3">
                {!! Form::text('contract_number2', null, array('placeholder' => 'Số Phụ lục HĐ Quyền liên quan (biểu diễn, ghi âm)','class' => 'form-control', 'readonly' => 'true')) !!}
            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Số thứ tự trong PL Quyền liên quan</label>
            <div class="col-sm-3">
                {!! Form::text('contract_number3', null, array('placeholder' => 'Số thứ tự trong PL Quyền liên quan','class' => 'form-control', 'readonly' => 'true')) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Số HĐ Quyền tác giả</label>
            <div class="col-sm-3">
                {!! Form::text('contract_number4', null, array('placeholder' => 'Số Phụ lục HĐ (Số HĐ của Viettel và CPs))','class' => 'form-control', 'readonly' => 'true')) !!}
            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Số Phụ lục HĐ Quyền tác giả</label>
            <div class="col-sm-3">
                {!! Form::text('contract_number5', null, array('placeholder' => 'Số Phụ lục HĐ Quyền tác giả','class' => 'form-control', 'readonly' => 'true')) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Số thứ tự trong PL Quyền Tác giả</label>
            <div class="col-sm-3">
                {!! Form::text('contract_number6', null, array('placeholder' => 'Số thứ tự trong PL Quyền Tác giả','class' => 'form-control', 'readonly' => 'true')) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Thời gian phát hành</label>
            <div class="col-sm-3">
                {!! Form::date('start_time', null, array('placeholder' => 'Thời gian phát hành','class' => 'form-control', 'readonly' => 'true')) !!}
            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Thời hạn bản quyền</label>
            <div class="col-sm-3">
                {!! Form::date('end_time', null, array('placeholder' => 'Thời hạn bản quyển','class' => 'form-control')) !!}
                @error('end_time')
                    <div class="text-danger">{{$message}}</div>
                @enderror
            </div>
        </div>
        <div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                @include('layouts.partials.button._save')
                &nbsp; &nbsp; &nbsp;
                <a href="{{route('media.music.index')}}">
                    @include('layouts.partials.button._back')
                </a>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
<!--         <div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                <button class="btn btn-info" type="submit">
                    <i class="ace-icon fa fa-check bigger-110"></i>
                    Submit
                </button>

                &nbsp; &nbsp; &nbsp;
                <button class="btn" type="reset">
                    <i class="ace-icon fa fa-undo bigger-110"></i>
                    Reset
                </button>
            </div>
        </div> -->