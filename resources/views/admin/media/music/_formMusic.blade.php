<div id="home" class="tab-pane fade in active">
    @if(isset($data))
    {!! Form::model($data, ['id'=>'myForm', 'method' => 'POST', 'class' => 'form-horizontal','route' => ['media.music.update', $data->id]]) !!}
            @csrf

    @else
    {!! Form::open(array('id'=>'myForm','route' => 'media.music.store','method'=>'POST', 'class' => 'form-horizontal', 'id'=>'form-create-music')) !!}
            @csrf

    @endif
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Tên bài hát<span class="text-danger">(*)</span></label>
            <div class="col-sm-3">
                {!! Form::text('name', null, array('placeholder' => 'Tên bài hát (có dấu)','class' => 'form-control')) !!}
                @error('name')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Ca sĩ thể hiện</label>
            <div class="col-sm-3">
                {!! Form::text('singer', null, array('placeholder' => 'Ca sĩ thể hiện','class' => 'form-control')) !!}
                @error('singer')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Nhạc sĩ</label>
            <div class="col-sm-3">
                {!! Form::text('musician', null, array('placeholder' => 'Nhạc sĩ','class' => 'form-control')) !!}
                @error('musician')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Album</label>
            <div class="col-sm-3">
                {!! Form::text('album', null, array('placeholder' => 'Album','class' => 'form-control')) !!}
                @error('album')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Loại hợp đồng<span class="text-danger">(*)</span></label>
            <div class="col-sm-3">
                {!! Form::select('type_fee_id', $list_type, null, ['class' => 'form-control', 'id' => "form-field-select-1"]) !!}
                @error('type_fee_id')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Quyền liên quan</label>
            <div class="col-sm-3">
                {!! Form::text('ghi_am', (isset($data->contract))?$data->contract->contract_number:null, array((isset($data))? "readonly":'','class' => 'form-control','id'=>'input', 'onkeyup'=>'display()','placeholder'=>'Quyền liên quan')) !!}
                @error('ghi_am')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Quyền tác giả</label>
            <div class="col-sm-3">
                {!! Form::text('tac_gia', null, array((isset($data))? "readonly":'', 'onkeyup'=>'display()','placeholder' => 'Quyền tác giả','class' => 'form-control')) !!}
                @error('tac_gia')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Số Phụ lục HĐ (Số HĐ của Viettel và CPs)</label>
            <div class="col-sm-3">
                {!! Form::text('contract_cps', null, array('placeholder' => 'Số Phụ lục HĐ (Số HĐ của Viettel và CPs)','class' => 'form-control')) !!}
                @error('contract_cps')
                    @include('layouts.partials.text._error')
                @enderror

            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Số HĐ Quyền liên quan (biểu diễn, ghi âm)</label>
            <div class="col-sm-3">
                {!! Form::text('contract_number1', null, array('placeholder' => 'Số HĐ Quyền liên quan (biểu diễn, ghi âm)','class' => 'form-control', (isset($data)&&$data->contract_number1)?'readonly':'')) !!}
                @error('contract_number1')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Số Phụ lục HĐ Quyền liên quan (biểu diễn, ghi âm)</label>
            <div class="col-sm-3">
                {!! Form::text('contract_number2', null, array('placeholder' => 'Số Phụ lục HĐ Quyền liên quan (biểu diễn, ghi âm)','class' => 'form-control')) !!}
                @error('contract_number2')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Số thứ tự trong PL Quyền liên quan</label>
            <div class="col-sm-3">
                {!! Form::text('contract_number3', null, array('placeholder' => 'Số thứ tự trong PL Quyền liên quan','class' => 'form-control')) !!}
                @error('contract_number3')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Số HĐ Quyền tác giả</label>
            <div class="col-sm-3">
                {!! Form::text('contract_number4', null, array('placeholder' => 'Số Phụ lục HĐ (Số HĐ của Viettel và CPs))','class' => 'form-control', (isset($data)&&$data->contract_number4)?'readonly':'')) !!}
                @error('contract_number4')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Số Phụ lục HĐ Quyền tác giả</label>
            <div class="col-sm-3">
                {!! Form::text('contract_number5', null, array('placeholder' => 'Số Phụ lục HĐ Quyền tác giả','class' => 'form-control')) !!}
                @error('contract_number5')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Số thứ tự trong PL Quyền Tác giả</label>
            <div class="col-sm-3">
                {!! Form::text('contract_number6', null, array('placeholder' => 'Số thứ tự trong PL Quyền Tác giả','class' => 'form-control')) !!}
                @error('contract_number6')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">ID Keeng</label>
            <div class="col-sm-3">
                {!! Form::text('music_id', null, array('placeholder' => 'ID Keeng','class' => 'form-control')) !!}
                @error('music_id')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
        <div>
            <div class="form-group">
                <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Ngày bắt đầu bản quyền(<span class="text-danger">*</span>)</label>
                <div class="col-sm-3">
                    <div class="input-group">
                        {!! Form::text('start_time', isset($data)? reFormatDate($data->start_time, 'd-m-Y') : null, array('placeholder' => 'Ngày bắt đầu','class' => 'form-control date-picker', 'id' => "id-date-picker-1", "data-date-format"=>"dd-mm-yyyy",'autocomplete'=>'off')) !!}
                        @include('layouts.partials.input._calender')
                    </div>
                        @error('start_time')
                        @include('layouts.partials.text._error')
                        @enderror
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Ngày kết thúc(<span class="text-danger">*</span>)</label>
                <div class="col-sm-3 @error('vn_name') has-error @enderror">
                    <div class="input-group">
                        {!! Form::text('end_time', isset($data)? reFormatDate($data->end_time, 'd-m-Y') : null, array('placeholder' => 'Ngày kết thúc','class' => 'form-control date-picker', 'id' => "id-date-picker-2", "data-date-format"=>"dd-mm-yyyy",'autocomplete'=>'off')) !!}
                        @include('layouts.partials.input._calender')
                    </div>
                    @error('end_time')
                    @include('layouts.partials.text._error')
                    @enderror    
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Giá tiền</label>
            <div class="col-sm-3">
                {!! Form::text('money', null, array('placeholder' => 'Giá tiền','class' => 'form-control')) !!}
                @error('money')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Ghi chú bài hát</label>
            <div class="col-sm-8">
                {!! Form::textarea('note', null, array('placeholder' => 'Ghi chú','class' => 'form-control')) !!}
                @error('note')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>

        <hr>
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Nhạc chờ độc quyền</label>
            <div class="col-sm-3">
                {{ Form::checkbox('cho_docquyen', 1, null, ['class' => 'form-control']) }}
            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Nhạc chờ tác quyền</label>
            <div class="col-sm-3">
                {{ Form::checkbox('cho_tacquyen', 1, null, ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Nhạc số độc quyền</label>
            <div class="col-sm-3">
                 {!! Form::hidden('so_docquyen',0) !!}
                {{ Form::checkbox('so_docquyen', 1, null, ['class' => 'form-control']) }}
            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Nhạc số tác quyền</label>
            <div class="col-sm-3">
                  {!! Form::hidden('so_tacquyen',0) !!}
                {{ Form::checkbox('so_tacquyen', 1, null, ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Bán đối tác thứ 3</label>
            <div class="col-sm-3">
         {{ Form::checkbox('sale_permission', 1, null, ['class' => 'form-control']) }}
            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Audio</label>
            <div class="col-sm-3">
            {{ Form::checkbox('type_audio', 1, null, ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">MV</label>
            <div class="col-sm-3">
               {{ Form::checkbox('type_MV', 1, null, ['class' => 'form-control']) }}
                @error('type_MV')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
        <div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                @include('layouts.partials.button._save')
                &nbsp; &nbsp; &nbsp;
                @include('layouts.partials.button._back')
            </div>
        </div>
    </div>
    {!! Form::close() !!}
        @include('layouts.partials.lib._date_picker')
</div>
