@extends('layouts.master')

@section('title')
    Gia hạn hợp đồng nhạc
@endsection

@section('content')
<div class="container">
    <h2>    Gia hạn hợp đồng nhạc</h2>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">Thông tin nhạc</a></li>
        <li><a data-toggle="tab" href="#menu1">Giấy tờ phê duyệt</a></li>
    </ul>
    <div class="tab-content">
        @include('admin.media.music._formExtensionMusic')
        @include('admin.media.music._menu1')
    </div>
</div>
@endsection
