
<!-- <input type="text" class="form-control" name="title" placeholder="Thời hạn"> -->
<br>
            <div class=" add-input">
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Thời gian bắt đầu" wire:model="start_time.{{ $value }}">
                            @error('start_time.'.$value) <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <input type="email" class="form-control" wire:model="end_time.{{ $value }}" placeholder="Thời gian kết thúc">
                            @error('end_time.'.$value) <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                </div>
            </div>
	<button class="btn btn-primary" wire:click="addMedia" id="addMusic">+Thêm vào hợp đồng</button>