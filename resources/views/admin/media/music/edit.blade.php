@extends('layouts.master')

@section('title')
    Cập nhật Nhạc
@endsection

@section('content')
<div class="container">
    <h2>Cập nhật nhạc</h2>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">Thông tin nhạc</a></li>
        <li><a data-toggle="tab" href="#menu1">Giấy tờ phê duyệt</a></li>
    </ul>
    <div class="tab-content">
        @include('admin.media.music._formMusic')
        @include('admin.media.music._menu1')
    </div>
</div>
@endsection
@include('admin.media.music._js')
