@section('js')
    <script src="{{asset('js/easy-number-separator.js')}}"></script>
    <script src="{{asset('assets/select2/select2.min.js')}}"></script>
    <script src="{{asset('js/formValidation.min.js')}}"></script>
    <script src="{{asset('js/validate/bootstrap.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $(function () {
                $(".select_box").select2({
                    placeholder: "Chọn  quyền...",
                    allowClear: true
                });
                $(".select_cate").select2({
                    placeholder: "Chọn  thể loại...",
                    tags: true,
                    tokenSeparators: [',']
                });
                $(".select_country").select2({
                    placeholder: "Chọn quốc gia...",
                    tags: true,
                    tokenSeparators: [',']
                });
            });
        });
        $('#input').bind('onchange',function(){
            if (this.value==='role')
            {
                this.input['role'].style.visibility='visible'
            }
            else {
                this.input['role'].style.visibility='hidden'};
        });
    </script>


<script>
        $(document).ready(function() {
        $('#id-date-picker-2')
        .datepicker({
            format: 'dd-mm-yyyy'
        })
        .on('changeDate', function(e) {
            // Revalidate the date field
            
            $('#myForm').formValidation('revalidateField', 'end_time');
            
        });
        $('#id-date-picker-1')
        .datepicker({
            format: 'dd-mm-yyyy'
        })
        .on('changeDate', function(e) {
            // Revalidate the date field
            
            $('#myForm').formValidation('revalidateField', 'start_time');
            
        });
       $('#myForm').formValidation({
            
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'vn_name': {
                    trigger: 'blur',
                    validators: {
                        notEmpty: {
                            message: '{{  __("Vui lòng nhập tên phim")}}'
                        },
                        stringLength: {
                            max: 256,
                            message: '{{  __("Độ dài không quá 256 kí tự")}}'
                        }
                    }
                },
                'start_time': {
                    trigger: 'blur',
                    validators: {
                        notEmpty: {
                            message: '{{ __("Thời gian bắt đầu bắt buộc")}}'
                        },
                        date:{
                            format: 'DD-MM-YYYY',
                            message: 'Thời gian không hợp lệ'
                        }
                    }
                },
                'end_time': {
                    trigger: 'blur',
                    validators: {
                        notEmpty: {
                            message: '{{ __("Thời gian kết thúc bắt buộc")}}'
                        },
                        date:{
                            format: 'DD-MM-YYYY',
                            message: 'Thời gian không hợp lệ'
                        }
                    }
                }
            }
        })
        .on('success.validator.fv', function(e, data) {
            
            if (data.field === 'eventDate' && data.validator === 'date' && data.result.date) {
                // The eventDate field passes the date validator
                // We can get the current date as a Javascript Date object
                var currentDate = data.result.date,
                    day         = currentDate.getDay();

                // If the selected date is Sunday
                if (day === 0) {
                    // Treat the field as invalid
                    data.fv
                        .updateStatus(data.field, data.fv.STATUS_INVALID, data.validator)
                        .updateMessage(data.field, data.validator, 'Please choose a day except Sunday');
                } else {
                    // Reset the message
                    data.fv.updateMessage(data.field, data.validator, 'The date is not valid');
                }
            }
        });
    });
    </script>


@endsection