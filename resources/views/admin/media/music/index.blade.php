@extends('layouts.master')
@section('title')
    Quản lý Nhạc
@endsection
@section('content')
    <div>
        <div class="row">
            <div class="col-sm-3" style="text-align: center;">
                <button class="btn deadline-3m" style="background: #FBA420 !important; border: 4px solid #FBA420;">
                    Sắp hết hạn trong vòng 3 tháng<br>
                    <span style="font-size: 25px;">30</span>
                </button>
            </div>
            <div class="col-sm-3" style="text-align: center;">
                <button class="btn deadline-2m" style="background: #FB8441 !important; border: 4px solid #FB8441;">
                    Sắp hết hạn trong vòng 2 tháng<br>
                    <span style="font-size: 25px;">20</span>
                </button>
            </div>
            <div class="col-sm-3" style="text-align: center;">
                <button class="btn deadline-1m" style="background: #EE5816 !important; border: 4px solid #EE5816;">
                    Sắp hết hạn trong vòng 1 tháng<br>
                    <span style="font-size: 25px;">10</span>
                </button>
            </div>
            <div class="col-sm-3" style="text-align: center;">
                <button class="btn deadline-1w" style="background: #FB1F0E !important; border: 4px solid #FB1F0E;">
                    Sắp hết hạn trong vòng 1 tuần<br>
                    <span style="font-size: 25px;">5</span>
                </button>
            </div>
        </div>
    </div>
    <div class="page-header">
        <div class="row">
            <div class="col-sm-12">
                <div class="widget-box">
                    <div class="widget-header">
                        <h4 class="widget-title">Tìm kiếm</h4>

                        <span class="widget-toolbar">
                            <a href="#" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up"></i>
                            </a>
                        </span>
                    </div>

                    <div class="widget-body">
                        <div class="widget-main">
                            {!! Form::open(array('method'=>'GET','route'=>'media.music.index','role'=>'search','id'=>'form-search-QLBQ',))  !!}
                            <div class="row">
                                <div class="col-xs-4 col-sm-4">
                                    <label for="form-field-select-1">Nhập từ khóa</label>
                                    <input class="form-control" name="keyword" type="text"
                                        value="{{request('keyword')}}" id='input_vn_name' autocomplete="off" />
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 d-flex" style="text-align:center;">
                                    <div style="float:left;">
                                        <button type="submit" class="btn btn-info btn-sm" id='btn-submit-search-QLBQ' style="margin-top: 2px; outline: none;">
                                            <span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
                                            Tìm kiếm
                                        </button>
                                        <p class="text-danger" style="display: inline;" id='text-error-form'></p>
                                    </div>
                                    <div style="display: inline-block; float:right;" class="ml-auto">
                                        <div class="dropdown" style="display: inline-block;">
                                            <button class="btn btn-info btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background:#8080FF !important; border: 4px solid #8080FF; margin-top: 2px; outline: none;">
                                                <span class="menu-icon fa fa-plus" style="font-size: 18px;"></span>
                                                Tạo mới
                                                <span class="menu-icon fa fa-caret-down" style="font-size: 18px; margin-left: 2px;"></span>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton" style="min-width: 108px;">
                                                <div class="col-sm-12 div-hover" style="margin: 0px; padding: 0px;">
                                                    <a class="dropdown-item" href="{{route('media.music.create')}}" style="color: black; margin-left: 5px; text-decoration: none;">Tạo mới<br></a>
                                                </div>
                                                <div class="col-sm-12 div-hover" style="margin: 0px; padding: 0px; cursor: pointer;"
                                                    data-target="#modal-form-upload-file" data-toggle="modal" id='div-upload'>
                                                    <a class="dropdown-item " id='a_upload'
                                                        style="color: black; margin-left: 5px; text-decoration: none;">
                                                        Tải lên từ Excel
                                                    </a>
                                                </div>                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-outline-dark btn-sm" data-target="#modal-form-export-music" data-toggle="modal"
                                            style="background:#FFFFFF !important;color: #7F7F7F !important;
                                            border: 1px black solid; padding: 7px; margin-top: 2px; outline: none;">
                                            <span class="menu-icon fa fa-download" style="font-size: 18px;"></span>
                                            Excel file
                                        </button>
                                        <button type="button" class="btn btn-info btn-sm" style="background: #F46A6A !important; border: 4px solid #F46A6A; margin-top: 2px; outline: none;">
                                            <span class="menu-icon fa fa-trash-o" style="font-size: 18px;"></span>
                                            Xóa
                                        </button>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div><!-- /.page-header -->
    {{-- Modal Upload Nhạc--}}
    <div class="modal fade" id="modal-form-upload-file" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Thêm file excel</h5>
                </div>
                <div class="modal-body">
                    <form action="{{route('media.music.upload')}}" method="POST" enctype="multipart/form-data" id='form-upload-file'>
                        {{csrf_field()}}
                        <div class="form-group">
                            <input type="file" id="file-upload" name='file' accept=".xlsx,.xls">
                        </div>
                        <input type="radio" class="checkboxClick" name="purchase_contract" value='buy' required> 
                        <label for="vehicle1"> Hợp đồng mua</label><br>
                        <input type="radio" class="checkboxClick" name="purchase_contract" value='sale' required> 
                        <label for="vehicle1"> Hợp đồng bán</label><br>
                    </form>
                </div>
                <div class="modal-footer" style="text-align: center;">
                    <a href="{{route('media.music.exportExampleFile')}}" style="float: left;">File excel mẫu &nbsp;</a>
                    <div style="float: left;">
                        <p class="text-danger" id='text-error'></p>
                    </div>
                    <div style="float: right;">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                        <button type="button" class="btn btn-primary" id='btn-save-file'>Lưu</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Modal export nhạc --}}
    <div class="modal fade" id="modal-form-export-music" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('media.music.export') }}" method="POST" enctype="multipart/form-data" id='form-upload-file'>
                    {{csrf_field()}}
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tải file excel xuống</h5>
                    </div>
                    <div class="modal-body">

                        Bạn có chắc chắn muốn xuất file không?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Quay lại</button>
                        <button type="submit" class="btn btn-danger" id='btn-upload-music'>Đồng ý</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- QLBQ --}}
     <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <div class="row">
                <div class="col-xs-12">
                    <table id="simple-table" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                        <th class="hidden-480" style="color: black;">STT</th>
                        <th>Tên bài hát</th>
                        <th>Ca sĩ</th>
                        <th>Nhạc sĩ</th>
                        <th>Quyền ghi âm</th>
                        <th>Quyền tác giả</th>
                        <th>Thời gian phát hành</th>
                        <th>Thời gian kết thúc</th>
                        <th>Hợp đồng Cps</th>
                        <th>Loại hợp đồng</th>
                        <th>Hành động</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($data as $key => $rs)
                        <tr>
                            <td class="hidden-480">{{ ++$i }}</td>
                            <td>{{ $rs->name }}</td>
                            <th>{{ $rs->singer }}</td>
                            <td>{{ $rs->musician }}</td>
                            <td>{{ $rs->ghi_am }}</td>
                            <td>{{ $rs->tac_gia }}</td>
                            <td>{{ $rs->start_time }}</td>
                            <td>
                                {{ $rs->end_time }}
                                <a  href="{{ route('media.music.contractExtension',$rs->id) }}" type="button" class="btn btn-sm btn-primary"> Gia hạn</a>
                            </td>
                            <td>{{ $rs->contract_number }}</td>
                            <td>{{ isset($list_type[$rs->type_fee_id]) ? $list_type[$rs->type_fee_id] : "" }}</td>
                            <td><center>
                                <button style="background: #fff; border: 1px solid black;margin-top: 2px;" type="button" class = 'btn-buy-music' data-target="#modal-form-buy-music" data-toggle="modal" data-id='{{$rs->id}}' data-name='{{$rs->name}}' data-singer='{{$rs->singer}}'>
                                    <i class="fa fa-shopping-cart" style="font-size:16px; color: #2199EB;"></i>
                                </button>

                                <button style="background: #fff; border: 1px solid black;margin-top: 2px;" type="button" class = 'btn-sale-music' data-target="#modal-form-sale-music" data-toggle="modal" data-id='{{$rs->id}}' data-name='{{$rs->name}}' data-singer='{{$rs->singer}}'>
                                    <i class="fa fa-dollar" style="font-size:16px; color: #2199EB;"></i>
                                </button>
                                <button style="background: #fff; border: 1px solid black;margin-top: 2px;" type="button" class="btn-edit-music btn-info" musicID='{{$rs->id}}'>
                                    <i class="ace-icon fa fa-pencil bigger-120"></i>
                                </button>
                                <button type='button' style="background: #f46a6a;border: none; padding: 2px 9px; margin-top: 2px;" class="btn-delete-music"
                                    data-target="#modal-form-delete-music" data-toggle="modal" data-id='{{$rs->id}}'>
                                    <i class="menu-icon fa fa-trash-o" style="color: white;font-size:16px;"></i>
                                </button>

                            </center></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- /.span -->
                @include('layouts.partials._pagination')
            </div><!-- /.row -->
        </div><!-- /.col -->
    </div><!-- /.row -->

    {{-- modal xóa nhạc --}}
    <div class="modal fade" id="modal-form-delete-music" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Xóa phim</h5>
                </div>
                <div class="modal-body">
                    Bạn có muốn xóa không? Thao tác này không thể phục hồi!                 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Không</button>
                    <button type="button" class="btn btn-danger" id='btn-delete-music'>Xóa bỏ</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-form-sale-music" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Bán nhạc</h5>
                </div>
                <div class="modal-body">
                    Xác nhận bán bài hát         
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Quay lại</button>
                    <button type="button" class="btn btn-danger" id='btn-sale-music' data-dismiss="modal">Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-form-buy-music" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Mua nhạc</h5>
                </div>
                <div class="modal-body">
                    Xác nhận mua bài hát         
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Quay lại</button>
                    <button type="button" class="btn btn-danger" id='btn-buy-music'data-dismiss="modal">Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
    {{-- form delete nhạc --}}
    <div id="div-form-delelte">
        @foreach($data as $rs)
            <form name='{{$rs->id}}' action="{{route('media.music.destroy',$rs->id)}}" method="POST">
                @csrf {{method_field('DELETE')}}
            </form>
        @endforeach
    </div>
    {{-- form edit nhạc --}}
    <div id="div-form-edit">
        @foreach($data as $rs)
            <form name='{{$rs->id}}' action="{{route('media.music.edit',$rs->id)}}" method="GET">
            </form>
        @endforeach
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            var musicID
            var name 
            var single

            // hợp đồng mua 
            $('.btn-buy-music').click(function(){
                musicID = $(this).attr('data-id')
                name = $(this).attr('data-name')
                singer = $(this).attr('data-singer')
            })
            $("#btn-buy-music").click(function(){
                var _token = $("input[name='_token']").val();
                $.ajax({
                    url: "/media/music/buy",
                    type:'POST',
                    data: {
                        _token:_token, 
                        musicID:musicID, 
                        name:name,
                        singer:singer
                    },
                    success: function(data) {
                        console.log(data)
                    }
                });
            }); 

            // hợp đồng bán
            $('.btn-sale-music').click(function(){
                musicID = $(this).attr('data-id')
                name = $(this).attr('data-name')
                singer = $(this).attr('data-singer')
            })
            $("#btn-sale-music").click(function(){
                var _token = $("input[name='_token']").val();
                $.ajax({
                    url: "/media/music/sale",
                    type:'POST',
                    data: {
                        _token:_token, 
                        musicID:musicID, 
                        name:name,
                        singer:singer
                    },
                    success: function(data) {
                        console.log(data)
                    }
                });
            }); 
        })
    </script>
@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/QLBQ.css')}}">

@endsection

@section('js')
    <script src="{{asset('js/QLBQ.js')}}"></script>
@endsection
