@extends('layouts.master')
@section('title')
    Quản lý bản quyền phim
@endsection
@section('content')

@livewire('media.film.film-list')
@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/QLBQ.css')}}">

@endsection

@section('js')
    <script src="{{asset('js/QLBQ.js')}}"></script>
@endsection
