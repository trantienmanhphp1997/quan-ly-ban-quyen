
@extends('layouts.master')

@section('content')
<div class="container">
    <h2> Gia hạn hợp đồng phim</h2>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">Thông tin phim</a></li>
        <li><a data-toggle="tab" href="#menu1">Giấy tờ phê duyệt</a></li>
    </ul>
    <div class="tab-content">
        <div id="home" class="tab-pane fade in active" >
            {!! Form::model($data, ['method' => 'POST', 'class' => 'form-horizontal','route' => ['media.films.extension', $data->id]]) !!}
                @method('POST')
                @csrf
            <div class="row">
                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Số hợp đồng</label>
                    <div class="col-sm-3">
                        {!! Form::text('contract_id', ($data->contract)?$data->contract->contract_number:'', array('placeholder' => 'Mã hợp đồng','class' => 'form-control', 'readonly' => 'true')) !!}
                    </div>
                    <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Loại hợp đồng</label>
                    <div class="col-sm-3">
                        {!! Form::select('type', [0 =>'Viettel mua',1=>'Viettel bán'],$data->type, array('class' => 'form-control','disabled'=>true)) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Sản phẩm media</label>
                    <div class="col-sm-3">
                        {!! Form::text('vn_name', $data->vn_name, array('class' => 'form-control', 'readonly' => 'true')) !!}
                    </div>
                    <label class="col-sm-2 control-label no-padding-right" for="form-field-8">
                </div>

                <div class="form-group">
                    {{--<label class="col-sm-2 control-label no-padding-right" for="form-field-8">Số tiền</label>
                    <div class="col-sm-3">
                        {!! Form::number('total_fee', null, array('class' => 'form-control','oninput'=>"this.value = Math.abs(this.value)")) !!}
                        @error('total_fee')
                            <div class="text-danger">{{$message}}</div>
                        @enderror
                    </div>--}}
                    {{-- <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Doanh thu từ</label>
                    <div class="col-sm-3">
                        {!! Form::select('type_revenue', [0=>'Ký hợp đồng', 1=>'Gia hạn'],null, array('class' => 'form-control')) !!}
                    </div> --}}
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Ngày bắt đầu</label>
                    <div class="col-sm-3">
                        {!! Form::date('start_time', $data->start_time, array('placeholder' => 'Ngày bắt đầu','class' => 'form-control', 'readonly' => 'true')) !!}
                    </div>
                    <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Thời hạn bản quyền</label>
                    <div class="col-sm-3">
                        {!! Form::date('end_time', null, array('placeholder' => 'Thời hạn bản quyển','class' => 'form-control')) !!}
                        @error('end_time')
                            <div class="text-danger">{{$message}}</div>
                        @enderror
                    </div>
                </div>

               
                 
                <div class="clearfix form-actions">
                    <div class="col-md-offset-3 col-md-9">
                        @include('layouts.partials.button._save')
                        &nbsp; &nbsp; &nbsp;
                        <a href="{{route('media.films')}}">
                            @include('layouts.partials.button._back')
                        </a>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        @include('admin.media.film._menu1')
    </div>
</div>


{{-- <div class="modal fade" id="exampleModal{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    {!! Form::model($data, ['method' => 'POST', 'class' => 'form-horizontal',  'autocomplete' => "off",'route' => ['media.films.extension', $data->id]]) !!}
    @csrf
    <div class="modal-dialog" role="document">
        <div class="modal-content">
                        <div class="form-group row">
                            <div class="col-md-6 col-xs-12 col-sm-12 @error('contract_number') has-error @enderror">
                                <lable class="col-md-4 col-xs-4 col-sm-4 col-form-label">Mã hợp đồng <span class="text-danger">(*)</span></lable>
{!! Form::text('contract_id', $data->con_num, array('placeholder' => 'Mã hợp đồng','class' => 'col-md-8','disabled'=>true)) !!}

                                @error('contract_number')
                                @include('layouts.partials.text._error')
                            </div>
                            <div class="col-md-6">
                                <lable class="col-md-4 col-xs-4 col-sm-4 col-form-label">Loại hợp đồng:</lable>


{!! Form::text('contract_id', $data->con_num, array('placeholder' => 'Mã hợp đồng','class' => 'col-md-8','disabled'=>true)) !!}

                                @error('contract_number')
                                @include('layouts.partials.text._error')
                            </div>
                            <div class="col-md-6">
                                <lable class="col-md-4 col-xs-4 col-sm-4 col-form-label">Loại hợp đồng:</lable>

 {!! Form::select('type', [0=>'Vietel bán',1=>'Vietel mua'], $data->type, array('class' => 'col-md-8','disabled'=>true)) !!}
                            </div>
                        </div>
                    </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <lable class="col-md-4 col-xs-4 col-sm-4 col-form-label">Sản phẩm media:</lable>

  {!! Form::text('media_id', $data->vn_name, array('placeholder' => 'Sản phẩm media','class' => 'col-md-8','disabled'=>true)) !!}
                            </div>
                            <div class="col-md-6">
                                <lable class="col-md-4 col-xs-4 col-sm-4 col-form-label">Danh mục:</lable>


{!! Form::text('category_id', $data->category_id, array('class' => 'col-md-8','disabled'=>true)) !!}
                            </div>
                        </div>
                    </div>

 <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group row">
                            <div class="col-md-6 @error('date_sign') has-error @enderror">
                                <label class="col-md-4 col-xs-4 col-sm-4 col-form-label" >Ngày ký<span class="text-danger">(*)</span></label>
                                <div class="input-group col-sm-8">


  {!! Form::text('date_sign', isset($data)? reFormatDate($data->date_sign, 'd-m-Y') : null, array('placeholder' => 'Ngày bắt đầu','class' => 'form-control date-picker', 'id' => "id-date-picker-1", "data-date-format"=>"dd-mm-yyyy")) !!}
                                    @include('layouts.partials.input._calender')
                                </div>
                                @error('date_sign')
        </div>
    </div>
    {!! Form::close() !!}


</div> --}}


@endsection
@section('js')
    <script src="{{asset('js/QLBQ.js')}}"></script>
@endsection


