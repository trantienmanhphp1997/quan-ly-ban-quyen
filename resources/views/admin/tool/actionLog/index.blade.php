@extends('layouts.master')

@section('title')
    Nhật kí hoạt động
@endsection
@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>NHẬT KÍ HOẠT ĐỘNG</h2>
            </div>

        </div>
    </div>


    @if ($message = Session::get('message'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="page-header">
        <div class="row">
            <div class="col-sm-12">
                <div class="widget-box">
                    <div class="widget-header">
                        <h4 class="widget-title">Tìm kiếm</h4>

                        <span class="widget-toolbar">
                            <a href="#" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up"></i>
                            </a>
                        </span>
                    </div>

                    <div class="widget-body">
                        <div class="widget-main">
                            {!! Form::open(['method' => 'GET', 'route' => 'action_log', 'role' => 'search', 'id' => 'form-search-action-log']) !!}
                            <input type="hidden" name="action" value="search">
                            <div class="row">
                                <div class="col-xs-4 col-sm-4">
                                    <label for="form-field-select-1">Loại</label>
                                    <div>
                                        {{ Form::select( 'search_type', [ 'music' => 'Bài hát', 'films' => 'Phim', 'contracts' => 'Hợp đồng'], request('search_type')) }}
                                    </div>
                                </div>

                                <div class="col-xs-4 col-sm-4">
                                    <label id="search-name-label" for="form-field-select-1">Tên phim/bài hát/hợp đồng</label>
                                    <input class="form-control" name="name" type="text" value="{{ request('name') }}"
                                        autocomplete="off" />
                                </div>
                                <div class="col-xs-4 col-sm-4">
                                    <label for="id-date-picker-1">Thời gian:</label>
                                    <input class="form-control" type="text" name="start_date" id="id-date-range-picker-1" value="{{request('start_date')}}" autocomplete="off" readonly style="background-color:white !important;"/>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 d-flex" style="text-align:center;">
                                    <div style="float:left;">
                                        <button type="submit" class="btn btn-info btn-sm" id='btn-submit-search-QLBQ'
                                            style="margin-top: 2px; outline: none;">
                                            <span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
                                            Tìm kiếm
                                        </button>
                                        <p class="text-danger" style="display: inline;" id='text-error-form'></p>
                                    </div>
                                    <!-- <div style="display: inline-block; float:right;" class="ml-auto">
                                        <button type="button" class="btn btn-outline-dark btn-sm"
                                            data-target="#modal-form-export-film" data-toggle="modal" style="background:#FFFFFF !important;color: #7F7F7F !important;
                                                border: 1px black solid; padding: 7px; margin-top: 2px; outline: none;">
                                            <span class="menu-icon fa fa-download" style="font-size: 18px;"></span>
                                            Excel file
                                        </button>
                                    </div> -->
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div><!-- /.page-header -->

    <table class="table table-bordered table-striped table-hover">
        <tr>
            <th style="color: black;"><center>STT</center></th>
            <th style="color: black;"><center>Bản ghi</center></th>
            <th style="color: black;"><center>Loại</center></th>
            <th style="color: black;"><center>Người thay đổi</center></th>
            <th style="color: black;"><center>Hành động</center></th>
            <th style="color: black;"><center>Giá trị cũ</center></th>
            <th style="color: black;"><center>Giá trị mới</center></th>
            <th style="color: black;"><center>File tải lên</center></th>
            <th style="color: black;"><center>Thời gian tác động</center></th>
        </tr>
        @forelse ($data as $key => $rs)
            <tr>
                <td><center>{{ ++$i }}</center></td>
                <td>
                @if($rs->action_log!='Xóa'&&$rs->action_log!='Tải lên')
                    @if(!$rs->delete_time)
                        <a href="{{ route($rs->edit_route, [$rs->record_id]) }}">{!! boldTextSearchV2($rs->record_name,request('name')) !!} </a>
                    @else 
                    {!! boldTextSearchV2($rs->record_name,request('name')) !!}
                    @endif
                @else 
                    {!! boldTextSearchV2($rs->record_name,request('name')) !!}
                @endif

                </td>
                <td>@switch($rs->model)
                        @case('contracts')
                            Hợp đồng
                            @break
                        @case('films')
                            Phim
                            @break
                        @default
                            Nhạc
                    @endswitch
                </td>
                <td>{{ $rs->admin_name }}</td>
                <td>{{ $rs->action_log }}</td>
                @if($rs->action_log!='Xóa'&&$rs->action_log!='Tải lên')
                <td class="old-value">
                    @if ($rs->old_value)
                        @foreach (json_decode($rs->old_value) as $field => $value)
                            @if ($value)
                                <div><b>{{ ToVNColumnName($rs->model, $field) }}: </b> {{ $value }}</div>
                            @endif
                        @endforeach
                    @endif
                </td>
                <td class="new-value">
                    @if ($rs->new_value)
                        @foreach (json_decode($rs->new_value) as $field => $value)
                            @if ($value)
                                <div><b>{{ ToVNColumnName($rs->model, $field) }}: </b> {{ $value }}</div>
                            @endif
                        @endforeach
                    @endif
                </td>
                @else
                <td></td>
                <td></td>
                @endif
                <td><a href="{{ route('action_log.download', ['id' => $rs->id])}}">{{ $rs->file }}</td>
                <td><center>{{ reFormatDate($rs->created_at, 'd-m-Y H:i:s') }}</center></td>
            </tr>
        @empty

            <tr>
                <td colspan="9" class="text-center">Chưa có hành động nào</td>
            </tr>

        @endforelse
    </table>
    @include('layouts.partials._paginate1')
    @include('layouts.partials.lib._date_picker')
@endsection
