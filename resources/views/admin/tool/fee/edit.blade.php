@extends('layouts.master')

@section('title')
    Sửa loại chi phí
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Sửa loại chi phí</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('type_fee.index') }}"> Trở lại</a>
            </div>
        </div>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form method="post" action="{{route('type_fee.update',$data->id)}}">
        <div class="row">
            @csrf
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Tên(<span class="text-danger">*</span>)</strong><br>
                    <input type="text" placeholder="Tên loại chi phí" name="name" class="form-control" value="{{$data->name}}">
                </div>
                <div class="form-group">
                    <strong>Mã(<span class="text-danger">*</span>)</strong><br>
                    <input type="text" placeholder="Mã" name="name_unique" class="form-control" value="{{$data->name_unique}}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary m-3">Lưu</button>
            </div>
        </div>
    </form>
@endsection
