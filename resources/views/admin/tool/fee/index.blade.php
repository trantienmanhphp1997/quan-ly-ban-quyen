@extends('layouts.master')
@section('title')
    Danh sách chi phí
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12 form-group">
            <div class="pull-left">
                <h2>Danh sách chi phí nhạc</h2>
            </div>
            <div class="pull-right">
                @include('layouts.partials.button._new')
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <div class="row">
                <div class="col-xs-12">
                    <table id="simple-table" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="hidden-480" style="text-align: center; color: black;">STT</th>
                            <th style="text-align: center; color: black;">Tên loại chi phí</th>
                            <th style="text-align: center; color: black;">Mã</th>
                            <th style="text-align: center; color: black;" width="280px">Hành động</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach ($data as $key => $rs)
                            <tr>
                                <td style="text-align: center;">{{ ++$key }}</td>
                                <td>{{ $rs->name }}</td>
                                <td>{{ $rs->name_unique }}</td>
                                <td style="text-align: center;">
                                    @php $id = $rs->id; @endphp
                                    @include('layouts.partials.button._edit')

                                        <form action="{{route('type_fee.delete',$rs->id)}}" method="post" style="display: inline">
                                        @csrf
                                            @include('layouts.partials.button._deleteForm')
                                        </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- /.span -->
            </div><!-- /.row -->
        </div><!-- /.col -->
    </div><!-- /.row -->
@endsection
