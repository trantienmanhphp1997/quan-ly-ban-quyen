@extends('layouts.master')
@section('title')
    Quản lý người dùng
@endsection

@section('content')
<div class="page-header">
    <div class="row">
        <div class="col-sm-12">
            <div class="widget-box">
                <div class="widget-header">
                    <h4 class="widget-title">Tìm kiếm</h4>

                    <span class="widget-toolbar">
                        <a href="#" data-action="collapse">
                            <i class="ace-icon fa fa-chevron-up"></i>
                        </a>
                    </span>
                </div>

                <div class="widget-body">
                    <div class="widget-main">
                        {!! Form::open(['method'=>'GET','route'=>'createAdmin.index','role'=>'search'])  !!}
                        <div class="row">
                            <div class="col-xs-4 col-sm-4">
                                <label for="form-field-select-1">Tên đăng nhập</label>
                                <input class="form-control" name="username" type="text" value="{{request('username')}}" placeholder='Tên đăng nhập' />
                            </div>
                            <br>
                            <div class="col-xs-6 col-sm-6">
                                <button type="submit" class="btn btn-info btn-sm" style="margin-top:8px">
                                    <span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
                                    Tìm kiếm
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="pull-left">
            <h2>QUẢN LÝ NGƯỜI DÙNG</h2>
        </div>
        <div class="pull-right">
            @include('layouts.partials.button._new')
        </div>
    </div>
</div>



<table class="table table-bordered table-striped table-hover">
    <tr>
        <th class="hidden-480" style="text-align: center; color: black;">STT</th>
        <th style="text-align: center; color: black;">Tên đăng nhập</th>
        <th style="text-align: center; color: black;">Họ và tên</th>
        <th style="text-align: center; color: black;">Email</th>
        <th style="text-align: center; color: black;">Số điện thoại</th>
        <th style="text-align: center; color: black;">Thể loại sản phẩm</th>
        <th style="text-align: center; color: black;">Trạng thái mua bán</th>
        <th style="text-align: center; color: black;">Vai trò</th>
        <th style="text-align: center; color: black;">Is manage</th>
        <th style="text-align: center; color: black;" width="280px">Hành động</th>
    </tr>
    @forelse ($users as $key => $rs)
    <tr>
        <td style="text-align: center;">{{($users->currentPage() - 1) * $users->perPage() + $loop->iteration}}</td>
        <td>{!! boldTextSearchV2($rs->username,request('username')) !!}</td>
        <td>{{ $rs->full_name }}</td>
        <td>{{ $rs->email }}</td>
        <td>{{ $rs->phone_number }}</td>
        <td>{{ $categoryList[$rs->category_id]??"" }}</td>
        <td>{{ $rs->status_media==1?"Mua":($rs->status_media==2?"Bán":"") }}</td>
        <td>
            @if(!empty($rs->roles))
            @foreach($rs->roles as $role)
            <label class="badge badge-success">{{ $role->note }}</label>
            @endforeach
            <button type="submit" class="fa fa-plus btn-show" data-toggle="modal" data-target="#edit_role" data-id='{{$rs->id}}'></button>
            <input type="hidden" value='{{ $rs->id }}' name="id">
            @foreach($rs->roleDontBelongto as $role)
            <input type="hidden" value="{{ $role->name }}" note="{{ $role->note }} ">
            @endforeach
            @foreach($rs->roles as $role)
            <input type="hidden" value="{{ $role->name }}" note="{{ $role->note }} ">
            @endforeach
            <input type="hidden" value='{{ $rs->roles()->count() }}'>
            <input type="hidden" value='{{ $rs->roleDontBelongto->count() }}'>
            @endif
        </td>
        @if($rs->is_manager == 1)
            <td style="text-align: center;"><input class="form-check-input" type="checkbox" value="" checked onclick="return false;"></td>
        @else
            <td style="text-align: center;"><input class="form-check-input" type="checkbox" value="" onclick="return false;"></td>
        @endif
        <td style="text-align: center;">
            <a title="Xem thông tin người dùng" class="btn btn-success fa fa-eye" style="background: #fff; border: 1px solid black;margin-top: -2px; padding: 4px 6px;" href="{{ route('createAdmin.show', ['id' => $rs->id]) }}"></a>
            @php $id = $rs->id; @endphp
            @include('layouts.partials.button._edit')
        </td>
    </tr>
    @empty

    <tr>
        <td colspan="10" class="text-center">Không tìm thấy người dùng</td>
    </tr>

    @endforelse
</table>

<div class="row">
    <div class="col-xs-12">
        <div class="col-xs-6">
            <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">Hiển thị {{$users->firstItem()}} đến {{$users->lastItem()}} trong tổng {{$users->total()}} bản ghi</div>
        </div>
        <div class="col-xs-6" style="padding-right: 0px">
            <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
                {{ $users->appends($_GET)->onEachSide(4)->links('vendor.pagination.default') }}
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="edit_role" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Thay đổi vai trò</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <!--           <span aria-hidden="true">&times;</span> -->
                </button>
            </div>
            <form  action="{{ route('createAdmin.update_role') }}" method="GET">
                <div class="modal-body">
                    <div id='div-submit'>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button"  class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    <button type="submit"  class="btn btn-primary">Lưu thay đổi</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="{{ asset('js/updateRoleUser.js') }}"></script>

@endsection
