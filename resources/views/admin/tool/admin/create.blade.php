@extends('layouts.master')
@section('title')
    Tạo mới người dùng
@endsection
@section('css')
    <link rel="stylesheet" href="{{asset('assets/select2/select2.min.css')}}" >
    <style>
        .select2-search--inline::after{
            margin-left:150px; !important;
        }
    </style>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12 margin-tb">
        <div class="pull-left">
            <h2>Tạo người dùng mới</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('createAdmin.index') }}"> Trở lại</a>
        </div>
    </div>
</div>

{!! Form::open(array('route' => 'createAdmin.store','method'=>'POST')) !!}
<div class="row" style="margin: 20px;" >
    <div class="col-xs-12 col-sm-12 col-md-12" style="margin: 10px;">
        <div class="form-group @error('username') has-error @enderror">
            <strong class="col-md-2">Tên đăng nhập: <span class="text-danger">(*)</span></strong>
            {!! Form::text('username', null, array('placeholder' => 'Tên đăng nhập:','class' => 'col-md-4')) !!}
        </div>
        @error('username')
            @include('layouts.partials.text._error')
        @enderror
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12" style="margin: 10px;">
        <div class="form-group @error('full_name') has-error @enderror">
            <strong class="col-md-2">Họ và tên:<span class="text-danger">(*)</span></strong>
            {!! Form::text('full_name', null, array('placeholder' => 'Họ và tên','class' => 'col-md-4')) !!}
        </div>
        @error('full_name')
            @include('layouts.partials.text._error')
        @enderror
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12" style="margin: 10px;">
        <div class="form-group @error('phone_number') has-error @enderror">
            <strong class="col-md-2">Số điện thoại:</strong>
            {!! Form::text('phone_number', null, array('placeholder' => 'Số điện thoại','class' => 'col-md-4')) !!}
        </div>
        @error('phone_number')
            @include('layouts.partials.text._error')
        @enderror
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12" style="margin: 10px;">
        <div class="form-group @error('email') has-error @enderror">
            <strong class="col-md-2">Email <span class="text-danger">(*)</span></strong>
            {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'col-md-4')) !!}
        </div>
        @error('email')
            @include('layouts.partials.text._error')
        @enderror
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12" style="margin: 10px;">
        <div class="form-group @error('password') has-error @enderror">
            <strong class="col-md-2">Mật khẩu: <span class="text-danger">(*)</span></strong>
            {!! Form::password('password', array('placeholder' => 'Mật khẩu:','class' => 'col-md-4')) !!}
        </div>
            @error('password')
                @include('layouts.partials.text._error')
            @enderror
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12" style="margin: 10px;">
        <div class="form-group @error('confirm-password') has-error @enderror">
            <strong class="col-md-2">Mật khẩu xác nhận:</strong>
            {!! Form::password('confirm-password', array('placeholder' => 'Mật khẩu xác nhận','class' => 'col-md-4')) !!}
        </div>
        @error('confirm-password')
            @include('layouts.partials.text._error')
        @enderror
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12" style="margin: 10px;">
        <div class="form-group @error('category_id') has-error @enderror">
            <strong class="col-md-2">Thể loại sản phẩm:</strong>
            {!! Form::select('category_id',['Chọn thể loại sản phẩm'] + $categoryList,null, array('class' => ' form_control col-md-4')) !!}
        </div>
        @error('category_id')
            @include('layouts.partials.text._error')
        @enderror
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12" style="margin: 10px;">
        <div class="form-group @error('status_media') has-error @enderror">
            <strong class="col-md-2">Trạng thái mua, bán:</strong>
            {!! Form::select('status_media',['Chọn trạng thái','Mua','Bán'],null, array('class' => ' form_control col-md-4')) !!}
        </div>
        @error('status_media')
            @include('layouts.partials.text._error')
        @enderror
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12" style="margin: 10px;">
        <div class="form-group">
            <div class="form-group">
                <strong for="password" class="col-md-2 control-label">Quyền</strong>
                    {!! Form::select('roles[]', $roles,[], array('class' => ' form_control select_box col-md-4','multiple','placeholder' => 'Chọn quyền')) !!}
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12" style="margin: 10px;">
        <div class="form-group">
            <strong class="col-md-2">Is manager</strong>
            {!! Form::checkbox('is_manager',1,null, array('class' => 'col-md-4')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Tạo</button>
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('js')
    <script src="{{asset('assets/select2/select2.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $(function () {
                $(".select_box").select2({
                    placeholder: "Chọn  quyền...",
                    allowClear: true
                });
            });
        });
    </script>
@endsection
