@extends('layouts.master')

@section('title')
    Thêm quyền mới
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12 margin-tb">
            <div class="pull-left">
                <h2>Tạo mới Role</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('tool.roles') }}"> Trở lại</a>
            </div>
        </div>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(array('route' => 'tool.roles.store','method'=>'POST')) !!}
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Tên quyền:</strong>
                {!! Form::text('name', null, array('placeholder' => 'Tên quyền','class' => 'form-control')) !!}
            </div>
            <div class="form-group">
                <strong>Mô tả:</strong>
                {!! Form::text('note', null, array('placeholder' => 'Mô tả' ,'class'=> 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Tạo</button>
        </div>
    </div>
    {!! Form::close() !!}
@endsection
