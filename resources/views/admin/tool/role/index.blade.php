@extends('layouts.master')
@section('title')
    Danh sách quyền
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12 margin-tb">
            <div class="pull-left">
                <h2>Danh sách vai trò</h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <div class="row">
                <div class="col-xs-12">
                    <table id="simple-table" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="hidden-480" style="text-align: center; color: black;">STT</th>
                            <th style="text-align: center; color: black;">Tên</th>
                            <th style="text-align: center; color: black;">Mô tả</th>

                        </tr>
                        </thead>

                        <tbody>
                        @foreach ($data as $key => $rs)
                            <tr>
                                <td style="text-align: center;">{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</td>
                                <td>{{ $rs->name }}</td>
                                <td>{{ $rs->note }}</td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @include('layouts.partials._paginate1')
                </div><!-- /.span -->
            </div><!-- /.row -->
        </div><!-- /.col -->
    </div><!-- /.row -->
@endsection
