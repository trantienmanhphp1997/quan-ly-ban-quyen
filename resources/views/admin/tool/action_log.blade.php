@extends('layouts.master')

@section('title')
    Nhật kí hoạt động
@endsection

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>NHẬT KÍ HOẠT ĐỘN</h2>
        </div>

    </div>
</div>


@if ($message = Session::get('message'))
<div class="alert alert-success">
  <p>{{ $message }}</p>
</div>
@endif


<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Record ID</th>
        <th>Table name</th>
        <th>Module</th>
        <th>Admin ID</th>
        <th>Action</th>
    </tr>
    @forelse ($actionLogs as $key => $actionLog)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $actionLog->record_id }}</td>
        <td>{{ $actionLog->model }}</td>
        <td>{{ $actionLog->module }}</td>
        <td>{{ $actionLog->admin_id }}</td>
        <td>{{ $actionLog->action_log }}</td>
    </tr>
    @empty

    <tr>
        <td colspan="9" class="text-center">Chưa có hành động nào</td>
    </tr>

    @endforelse
</table>
{!! $actionLogs->render() !!}
@endsection
