@extends('layouts.master')
@section('title')
    List Partners
@endsection
@section('content')
    <div class="page-header">
        <div class="row">
            <div class="col-sm-4">
                <div id="piechart_pub_type">

                </div>

            </div>
            <div class="col-sm-8">
            </div>
            <div class="col-sm-12">
                <div id="main-container"></div>
            </div>
        </div>
    </div><!-- /.page-header -->
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Danh sách Đối tác</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{route('partner.create')}}"> Tạo mới</a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    {{-- Danh sách nhân viên --}}
    <table class="table mt-4">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">
                Name &nbsp;
            </th>
            <th scope="col">
                Email
            </th>
            <th scope="col">
                Phone
            </th>
            <th scope="col">
                Address
            </th>
            <th scope="col">
                Action
            </th>
        </tr>
        </thead>
        @forelse($partners as $key => $partner)
            <tr>
                <th scope="row">{{$key+1}}</th>
                <td>{{$partner->name}}</td>
                <td>{{$partner->email}}</td>
                <td>{{$partner->phone}}</td>
                <td>{{$partner->address}}</td>
                <td>
                    <a class="btn btn-xs btn-info" href="{{route('partner.edit',$partner->id)}}">
                        <i class="ace-icon fa fa-pencil bigger-120"></i>
                    </a>
                    <form action="{{route('partner.delete',$partner->id)}}" method="post" style="display: inline">
                        @csrf
                        <button class="btn btn-xs btn-danger" type="submit">
                            <i class="ace-icon fa fa-trash-o bigger-120"></i>
                        </button>
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="6" class="text-center">
                    Chưa có đối tác nào
                </td>
            </tr>
            @endforelse
            </tbody>
    </table>
@endsection
