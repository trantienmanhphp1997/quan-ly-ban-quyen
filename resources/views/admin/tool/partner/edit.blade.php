@extends('layouts.master')

@section('title')
    Sửa Đối tác
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Partner</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('listEmployee') }}"> Trở lại</a>
            </div>
        </div>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form method="post" action="{{route('partner.update',$partner->id)}}">
        <div class="row">
            @csrf
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong> Name:</strong><br>
                    <input type="text" placeholder="Name" name="name" class="form-control" value="{{$partner->name}}">
                </div>
                <div class="form-group">
                    <strong>Email:</strong><br>
                    <input type="email" placeholder="Email" name="email" class="form-control" value="{{$partner->email}}">
                </div>
                <div class="form-group">
                    <strong>Address:</strong><br>
                    <input type="text" placeholder="Email" name="address" class="form-control" value="{{$partner->address}}">
                </div>
                <div class="form-group">
                    <strong>Phone:</strong><br>
                    <input type="number" placeholder="Phone" name="phone" class="form-control" value="{{$partner->phone}}">
                </div>
                <div class="form-group">
                    <strong>Note:</strong><br>
                    <textarea name="note" class="form-control">{{$partner->note}}</textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary m-3">Lưu</button>
            </div>
        </div>
    </form>
@endsection
