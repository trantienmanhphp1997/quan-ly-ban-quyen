@extends('layouts.master')
@section('title')
    Thêm giấy tờ 
@endsection
@section('content')
<h1>Thêm mới giấy tờ</h1>
{!! Form::open(array('route' => 'contract.file.store','method'=>'POST', 'class' => 'form-horizontal',  'autocomplete' => "off", 'id'=>'form-create-film')) !!}
    @include('admin.contractFile._formContractFile')
    {!! Form::close() !!}
@endsection