
<div class="row">
        <div class="form-group">
            <label for=""></label>
            <label class="col-sm-2 control-label no-padding-left" for="form-field-8">Tên hợp đồng(<span class="text-danger">*</span>)</label>
            <div class="col-sm-3 @error('name') has-error @enderror">
            {!! Form::text('name', null, array('placeholder' => 'Tên hợp đồng','class' => 'form-control')) !!}
                @error('name')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Ngày ký(<span class="text-danger">*</span>)</label>
            <div class="col-sm-3 @error('date_sign') has-error @enderror">
            <div class="input-group">
                    {!! Form::text('date_sign', isset($data)? reFormatDate($data->date_sign, 'd-m-Y') : null, array('placeholder' => 'Ngày ký','class' => 'form-control date-picker', 'id' => "id-date-picker-1", "data-date-format"=>"dd-mm-yyyy")) !!}
                    @include('layouts.partials.input._calender')
                </div>
                @error('date_sign')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-left" for="form-field-8">Số hợp đồng</label>
            <div class="col-sm-3 @error('contract_number') has-error @enderror">
            {!! Form::text('contract_number', null, array('placeholder' => 'Số hợp đồng','class' => 'form-control', 'readonly' => 'true')) !!}
                @error('contract_number')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
            <label class="col-sm-2 control-label no-padding-left" for="form-field-8">Loại hợp đồng</label>
            <div class="col-sm-3">
                <select name="categori_id1"  class="form-control" disabled>
                    <option value="1" {{($data->categori_id==1)?'selected':""}}>Hợp đồng nhạc</option>
                    <option value="2" {{($data->categori_id==2)?'selected':""}}>Hợp đồng phim</option>
                </select>
            </div>

        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-left" for="form-field-8">Người tạo(<span class="text-danger">*</span>)</label>
            <div class="col-sm-3">
            {!! Form::text('full_name', null, array('placeholder' => 'Link','class' => 'form-control','readonly' => 'true')) !!}
            </div>
            <label class="col-sm-2 control-label no-padding-left" for="form-field-8">Media_id</label>
            <div class="col-sm-3 @error('media_id') has-error @enderror">
                {!! Form::text('media_id', null, array('placeholder' => 'Media_id','class' => 'form-control', 'readonly' => 'true')) !!}
                @error('media_id')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-left" for="form-field-8">Link(<span class="text-danger">*</span>)</label>
            <div class="col-sm-3 @error('link') has-error @enderror">
            {!! Form::text('link', null, array('placeholder' => 'Link','class' => 'form-control')) !!}
                @error('link')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
        <div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                @include('layouts.partials.button._save')
                &nbsp; &nbsp; &nbsp;
                @include('layouts.partials.button._back')
            </div>
        </div>
    @include('layouts.partials.lib._date_picker')
    <br><br><br>
</div>