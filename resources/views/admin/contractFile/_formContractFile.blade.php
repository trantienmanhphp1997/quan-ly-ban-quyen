
<div class="row">
        <div class="form-group">
            <label for=""></label>
            <label class="col-sm-2 control-label no-padding-left" for="form-field-8">Tên hợp đồng(<span class="text-danger">*</span>)</label>
            <div class="col-sm-3 @error('name') has-error @enderror">
            {!! Form::text('name', null, array('placeholder' => 'Tên hợp đồng','class' => 'form-control')) !!}
                @error('name')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
            <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Ngày ký(<span class="text-danger">*</span>)</label>
            <div class="col-sm-3 @error('date_sign') has-error @enderror">
            <div class="input-group">
                    {!! Form::text('date_sign', isset($data)? reFormatDate($data->end_time, 'd-m-Y') : null, array('placeholder' => 'Ngày ký','class' => 'form-control date-picker', 'id' => "id-date-picker-1", "data-date-format"=>"dd-mm-yyyy")) !!}
                    @include('layouts.partials.input._calender')
                </div>
                @error('date_sign')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label for=""></label>
            <label class="col-sm-2 control-label no-padding-left" for="form-field-8">Số hợp đồng</label>
            <div class="col-sm-3 @error('contract_id') has-error @enderror">
            {!! Form::text('contract_id', null, array('placeholder' => 'Số hợp đồng','class' => 'form-control')) !!}
                @error('contract_id')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
            <label class="col-sm-2 control-label no-padding-left" for="form-field-8">Người tạo</label>
            <div class="col-sm-3 @error('note') has-error @enderror">
                {!! Form::text('note', null, array('placeholder' => 'Người tạo','class' => 'form-control')) !!}
                @error('note')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label for=""></label>
            <label class="col-sm-2 control-label no-padding-left" for="form-field-8">Link(<span class="text-danger">*</span>)</label>
            <div class="col-sm-3 @error('link') has-error @enderror">
            {!! Form::text('link', null, array('placeholder' => 'Link','class' => 'form-control')) !!}
                @error('link')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
            <label class="col-sm-2 control-label no-padding-left" for="form-field-8">Media_id</label>
            <div class="col-sm-3 @error('media_id') has-error @enderror">
                {!! Form::text('media_id', null, array('placeholder' => 'Media_id','class' => 'form-control')) !!}
                @error('media_id')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
        <div class="form-group">
        <label for="form-field-8" class="col-sm-2 control-label no-padding-right">Loại hợp đồng</label>
            
            <div class="col-md-6">
                <form action="" method="post">
            <select name="OptionCate" id="" class="col-md-6">
                <option value="0">--Chọn loại hợp đồng--</option>
                <option value="1">Hợp đồng nhạc</option>
                <option value="2">Hợp đồng phim</option>
            </select>
            </form>
            
            </div>
           
        </div>
       
        <div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                @include('layouts.partials.button._save')
                &nbsp; &nbsp; &nbsp;
                @include('layouts.partials.button._back')
            </div>
        </div>
    @include('layouts.partials.lib._date_picker')
    <br><br><br>
</div>