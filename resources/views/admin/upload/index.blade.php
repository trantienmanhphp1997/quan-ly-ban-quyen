@extends('layouts.master')
@section('title')
    Upload File
@endsection
@section('content')

    <div class="page-header">
        <div class="row">
            <div class="col-sm-4">
                <div id="piechart_pub_type">
                	
                </div>

            </div>
            <div class="col-sm-8">
            </div>
            <div class="col-sm-12">
                <div id="main-container"></div>
            </div>
        </div>
    </div><!-- /.page-header -->
	{{-- Upload File  --}}
{{--     <form action="{{route('storeFile')}}" method="POST" enctype="multipart/form-data">
		{{csrf_field()}}
		<h2 style="margin-bottom: 10px;">Chọn file Exel</h2>
        <div class="form-group">
            <label for="file">Thêm File</label><span id='label-file'></span><br>
            <input type="file" id="file" name='file' accept=".xlsx,.xls"">
        </div>
		<div style="text-align: center;">
			<button type="submit" class="btn btn-primary" id='btn-upload' style="margin: 0 auto;">Upload</button>
		</div>
	</form> --}}
	{{-- Danh sách nhân viên --}}
	


    <div id="modal-table" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header no-padding">
                    <div class="table-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            <span class="white">&times;</span>
                        </button>
                    </div>
                </div>
                <div id="table-wrapper">
                    <div class="modal-body no-padding" id = "table-scroll">
                        <div id="main-container1"></div>

                    </div>

                </div>


                <div class="modal-footer no-margin-top">
                    <button class="btn btn-sm btn-danger pull-left" data-dismiss="modal">
                        <i class="ace-icon fa fa-times"></i>
                        Close
                    </button>

                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

@endsection
