@extends('layouts.master')
@section('title')
    Upload File
@endsection
@section('content')

    <div class="page-header">
        <div class="row">
            <div class="col-sm-4">
                <div id="piechart_pub_type">

                </div>

            </div>
            <div class="col-sm-8">
            </div>
            <div class="col-sm-12">
                <div id="main-container"></div>
            </div>
        </div>
    </div><!-- /.page-header -->
    {{-- Danh sách nhân viên --}}
    <table class="table mt-4">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">
                FirstName &nbsp;
            </th>
            <th scope="col">
                LastName
            </th>
            <th scope="col">
                Email
            </th>
            <th scope="col">
                Salary
            </th>
            <th scope="col">
                Phone
            </th>
        </tr>
        </thead>
        @forelse($employees as $key => $employee)
            <tr>
                <th scope="row">{{$key+1}}</th>
                <td>{{$employee->firstname}}</td>
                <td>{{$employee->lastname}}</td>
                <td>{{$employee->email}}</td>
                <td>{{$employee->salary}}</td>
                <td>{{$employee->phone}}</td>
            </tr>
        @empty
            <tr>
                <td colspan="6" class="text-center">
                    Chưa có nhân viên nào
                </td>
            </tr>
            @endforelse
            </tbody>
    </table>
    <div class="d-flex">
        <button type="button" class="btn btn-primary" id='addEmployees' data-toggle="modal" data-target="#modal-form-upload-file" style="margin-top:5px;">
            Thêm nhân viên
        </button>
        <button type="button" class="btn btn-primary" id='exportEmployees' data-toggle="modal" data-target="#modal-form-export-file" style="margin-top:5px;">
            Xuất file
        </button>
    </div>



    <div id="modal-table" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header no-padding">
                    <div class="table-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            <span class="white">&times;</span>
                        </button>
                    </div>
                </div>
                <div id="table-wrapper">
                    <div class="modal-body no-padding" id = "table-scroll">
                        <div id="main-container1"></div>

                    </div>

                </div>


                <div class="modal-footer no-margin-top">
                    <button class="btn btn-sm btn-danger pull-left" data-dismiss="modal">
                        <i class="ace-icon fa fa-times"></i>
                        Close
                    </button>

                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    {{-- Modal form --}}
    <div class="modal fade" id="modal-form-upload-file" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Thêm file excel</h5>
                </div>
                <div class="modal-body">
                    <form action="{{route('storeFile')}}" method="POST" enctype="multipart/form-data" id='form-upload-file'>
                        {{csrf_field()}}
                        <div class="form-group">
                            <input type="file" id="file-upload" name='file' accept=".xlsx,.xls"">
                            <p style="color: red;font-size:16px;" id='text-error'></p>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    <button type="button" class="btn btn-primary" id='btn-save-file'>Lưu</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-form-export-file" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Xuất file excel</h5>
                    {{-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> --}}
                </div>
                <div class="modal-body">
                    <p>Bạn có chắc chắn muốn xuất file không?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Quay lại</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id='btn-export-file'>Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
@endsection
