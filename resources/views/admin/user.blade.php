<table id="myTable" style="border-collapse:collapse;width: 100%;">
    <thead>
    <tr>
        <th style="text-align: center;" colspan="25">HIỆU QUẢ DOANH THU CÁC HỢP ĐỒNG MUA PHIM QUỐC TẾ - Cập nhật đến hết tháng 07/2019</th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th>Đơn vị: triệu đồng</th>
    </tr>
    <tr>
        <th style="width: 5px;" rowspan="3" >STT</th>
        <th style="width: 15px;" rowspan="3">Quốc gia</th>
        <th style="width: 45px;" rowspan="3">Tên nội dung</th>
        <th rowspan="3">Số tập phim</th>
        <th rowspan="3">Số giờ phim</th>
        <th rowspan="3">Đối tác</th>
        <th rowspan="3">Thời hạn bản quyền</th>
        <th rowspan="3">Thời gian bắt đầu BQ</th>
        <th rowspan="3">Chi phí bản quyền</th>
        <th rowspan="3">Chi phí hậu kì</th>
        <th rowspan="3">Chi phí kiểm duyệt</th>
        <th rowspan="3">Tổng chi phí</th>
        <th colspan="7">DT DV VTM (Phim Keeng)</th>
        <th colspan="4">DT bán ngoài (VNĐ)</th>
        <th rowspan="3">Tổng DT</th>
        <th rowspan="3">Lợi nhuận</th>
        <th rowspan="3">Tỷ lệ LN/DT</th>
    </tr>
    <tr>
        <th colspan="2">2017</th>
        <th colspan="2">2018</th>
        <th colspan="2">2019</th>
        <th rowspan="2">Tổng DT VTM</th>
        <th colspan="3">2019</th>
        <th rowspan="2">Tổng DT bán ngoài</th>
    </tr>
    <tr>
        <th>Số</th>
        <th>QPVN</th>
        <th>Số</th>
        <th>QPVN</th>
        <th>Số</th>
        <th>QPVN</th>
        <th>VTT</th>
        <th>TH</th>
        <th>Số</th>
    </tr>

    </thead>
    <tbody>

        <tr>
            <td style="text-align: left;" colspan="3">Mua đứt</td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="3">Phim bộ</td>
        </tr>


    @php $stt=1; $i = 0;@endphp

    @foreach($phimBo as $row)
        <tr>
            <td>{{ $stt++ }}</td>
            @if($loop->index == 0 || in_array($loop->index, $indexMergePhimBo))
                <td style="text-align: center" rowspan="{{$countryNumberPhimBo[$i++]}}">{{$row->countryProduction->name??'' }}</td>
            @else
            @endif
            <td>{{ $row->vn_name }}</td>
            <td>{{ $row->count_tap }}</td>
            <td>{{ $row->count_hour }}</td>
            <td>{{ $row->contract->partner->name??'' }}</td>
            <td>{{ $row->end_time }}</td>
            <td>{{ $row->start_time }}</td>
            <td>{{ $row->license_fee }}</td>
            <td>{{ $row->partner_fee }}</td>
            <td>{{ $row->other_fee }}</td>
            <td>{{ $row->total_fee }}</td>
        </tr>
    @endforeach
    <tr>
        <th style="text-align: center;" colspan="3">Phim lẻ</th>
    </tr>
    @php $i = 0; @endphp
    @foreach($phimLe as $row)
        <tr>
            <td>{{ $stt++ }}</td>
            @if($loop->index == 0 || in_array($loop->index, $indexMergePhimLe))
                <td style="text-align: center" rowspan="{{$countryNumberPhimLe[$i++]}}">{{$row->countryProduction->name??'' }}</td>
            @else
            @endif
            <td>{{ $row->vn_name }}</td>
            <td>{{ $row->count_tap }}</td>
            <td>{{ $row->count_hour }}</td>
            <td>{{ $row->contract->partner->name??'' }}</td>
            <td>{{ $row->end_time }}</td>
            <td>{{ $row->start_time }}</td>
            <td>{{ $row->license_fee }}</td>
            <td>{{ $row->partner_fee }}</td>
            <td>{{ $row->other_fee }}</td>
            <td>{{ $row->total_fee }}</td>
        </tr>
    @endforeach
    </tbody>
</table>


