<div id="tab-second" class="tab-pane fade">
    <table class="table mt-4 table-bordered table-striped table-hover">
        <h2>Tổng phim có trong kho</h2>
        <thead>
            <tr>
                <th scope="col" rowspan='2'>
                <center style="color:black">Phim</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>Năm phát sóng</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>Số tập</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>Giá tập (USD/tập)</center>
                </th>
                <th scope="col" colspan='3'>
                    <center style='color:black;'>Đã bán</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>Phải bán còn lại (theo chi phí nhập)</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>Tổng giá trị cần thu hồi (theo chi phí nhập)</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>Tổng DT bán</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>Ghi chú</center>
                </th>
            </tr>
            <tr>
                <th scope="col">
                    <center style="color:black">Bán ngoài</center>
                </th>
                <th scope="col">
                    <center style="color:black">Nội bộ</center>
                </th>
                <th scope="col">
                    <center style="color:black">Tổng</center>
                </th>
            </tr>
            @foreach($data as $key => $rs)
                <tr>
                    <td>{{$rs->vn_name}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            @endforeach
        </thead>
    </table>
    <table class="table mt-4 table-bordered table-striped table-hover">
        <h2>Tính hiệu quả kinh doanh phim Hàn</h2>
        <thead>
            <tr>
                <th scope="col" rowspan='2'>
                <center style="color:black">Phim Hàn đã bán</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>Giá nhập vào</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>Đã bán ngoài</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>Nội bộ</center>
                </th>
                <th scope="col" rowspan='3'>
                    <center style='color:black;'>Tổng KD</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>CL/tập</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>SL tập</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>Tổng giá nhập</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>Tổng KD</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>Tổng CL (tính trên giá nhập)</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>LN tính trên phần chi phí tính phân bổ</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>% LN tính trên giá nhập</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>% LN tính theo CP phân bổ</center>
                </th>
            </tr>
        </thead>
    </table>
</div>