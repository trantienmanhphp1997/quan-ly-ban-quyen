
<div class="main-container">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>THEO DÕI BẢN QUYỀN</h2>
                </div>
            </div>
        </div>
    </div>
    <div wire:loading class="loader"></div>
    <div wire:ignore>
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tab-first">Tab 1</a></li>
            <li><a data-toggle="tab" href="#tab-second">Tab 2</a></li>
            <li><a data-toggle="tab" href="#tab-third">Tab 3</a></li>
            <li style="float:right;">
                <div wire:ignore>
                    <button type="button" class="btn btn-outline-dark btn-sm" data-target="#modal-form-export" data-toggle="modal"
                    style="background:#FFFFFF !important;color: #7F7F7F !important;
                                border: 1px black solid; padding: 7px;outline: none; cursor: pointer;">
                        <span class="menu-icon fa fa-download" style="font-size: 18px;"></span>
                        Excel file
                    </button>
                </div>
            </li>
        </ul>
        <div class="tab-content" wire:ignore>
            @livewire('copyright.tab-first')
            @livewire('copyright.tab-second')
            @livewire('copyright.tab-third')
        </div>
    </div>

    {{-- Modal --}}
    <div wire:ignore class="modal fade" id="modal-form-export" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title" id="exampleModalLabel">Tải file excel xuống</h2>
                </div>
                <div class="modal-body">
                    Bạn có chắc chắn muốn xuất file không?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Quay lại</button>
                    <button type="button" wire:click="export" class="btn btn-danger" data-dismiss="modal">Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
</div>
 {{-- @include('layouts.partials.lib._date_picker') --}}

