<div id="tab-first" class="tab-pane fade in active">
    <table class="table mt-4 table-bordered table-striped table-hover" 
        style='display: block;overflow: auto;white-space: nowrap;'>
        <h2>Tổng phim có trong kho</h2>
        <thead>
            <tr>
                <th scope="col" rowspan='2'>
                    <center style="color:black">STT</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style="color:black">NƯỚC SX</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>LOẠI (Phim/Nhạc)</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>LOẠI (Bộ/Lẻ)</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>TÊN PHIM/ NHẠC</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>HỢP ĐỒNG</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>LOẠI PHIM/ NHẠC</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>PHIM/ NHẠC</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>Năm phát sóng</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>Số tập</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>Giá nhập (USD/tập)</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>Tổng giá mua</center>
                </th>
                <th scope="col" colspan='2'>
                    <center style='color:black;'>THỜI GIAN BQ</center>
                </th>
                <th scope="col" colspan='6'>
                    <center style='color:black;'>HẠ TẦNG SD (VNĐ)- Phân bổ theo TTr</center>
                </th>
                <th scope="col" colspan='3'>
                    <center style='color:black;'>Đã bán</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>Phải bán còn lại (theo chi phí nhập)</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>Tổng giá trị cần thu hồi (theo chi phí nhập)</center>
                </th>
                <th scope="col" colspan='4'>
                    <center style='color:black;'>DOANH THU BÁN</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>LỢI NHUẬN DT-CP</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>TỈ SUẤT LỢI NHUẬN LN/DT</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>Phải bán còn lại (theo chi phí nhập)</center>
                </th>
                <th scope="col" rowspan='2'>
                    <center style='color:black;'>Ghi chú</center>
                </th>
            </tr>
            <tr>
                <th scope="col">
                    <center style='color:black;'>Từ</center>
                </th>    
                <th scope="col">
                    <center style='color:black;'>Đến</center>
                </th>      
                <th scope="col">
                    <center style='color:black;'>QPVN</center>
                </th> 
                <th scope="col">
                    <center style='color:black;'>HẠ TẦNG SỐ</center>
                </th> 
                <th scope="col">
                    <center style='color:black;'>TH</center>
                </th> 
                <th scope="col">
                    <center style='color:black;'>BQ</center>
                </th> 
                <th scope="col">
                    <center style='color:black;'>khác</center>
                </th>   
                <th scope="col">
                    <center style='color:black;'>Tổng</center>
                </th>     
                <th scope="col">
                    <center style='color:black;'>Bán ngoài</center>
                </th>  
                <th scope="col">
                    <center style='color:black;'>Nội bộ (gồm cả VTT)</center>
                </th>  
                <th scope="col">
                    <center style='color:black;'>Tổng</center>
                </th>    
                <th scope="col">
                    <center style='color:black;'>Tổng DT bán</center>
                </th>  
                <th scope="col">
                    <center style='color:black;'>N2019</center>
                </th>  
                <th scope="col">
                    <center style='color:black;'>N2020</center>
                </th>  
                <th scope="col">
                    <center style='color:black;'>TỔNG</center>
                </th>  
            </tr>
        </thead>
        <tbody>
            @foreach($data as $key => $row)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$row->country_name}}</td>
                    <td>Phim</td>
                    <td></td>
                    <td>{{$row->vn_name}}</td>
                    <td>{{$row->contract_number}}</td>
                    <td></td>
                    <td>{{$row->vn_name}}</td>
                    <td>{{$row->year_create}}</td>
                    <td>{{$row->count_tap}}</td>
                    <td>{{numberFormat($row->license_fee+$row->partner_fee+$row->other_fee)}}</td>
                    <td>{{numberFormat($row->count_tap*($row->license_fee+$row->partner_fee+$row->other_fee))}}</td>
                    <td>{{reFormatDate($row->start_time)}}</td>
                    <td>{{reFormatDate($row->end_time)}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>{{numberFormat($row->saleRevenue())}}</td>
                    <td>{{numberFormat($row->license_fee+$row->partner_fee+$row->other_fee-$row->saleRevenue())}}</td>
                    <td>{{numberFormat($row->count_tap*($row->license_fee+$row->partner_fee+$row->other_fee-$row->saleRevenue()))}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>{{$row->addition_note}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>