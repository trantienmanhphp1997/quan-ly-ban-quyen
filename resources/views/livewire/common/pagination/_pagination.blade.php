
<div class="row">
    <div class="col-xs-12">
        <div class="col-xs-6" style="float: left;">
            <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite" style="float:left;">Hiển thị {{$paginator->firstItem()}} đến {{$paginator->lastItem()}} trong tổng {{$paginator->total()}} bản ghi</div>
        </div>
        <div class="col-xs-6" style="padding-right: 0px">
            <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
                {{ $paginator->appends($_GET)->onEachSide(3)->links('vendor.livewire.bootstrap') }}
            </div>

        </div>
    </div>
</div>

