<form wire:submit.prevent="update">

@if ( Session::has('live_success'))
    <script>
        alert("{{ Session::get('live_success') }}");
        toastr.success("{{ Session::get('live_success') }}");
    </script>
    <?php
        session()->forget('live_success');
    ?>
@endif
<!-- Hiển thị thông báo lỗi  -->
@if ( Session::has('live_error'))
    <script>
        toastr.error("{{ Session::get('live_error') }}");
    </script>
    <?php
    session()->forget('live_error');
    ?>
@endif
</form>
