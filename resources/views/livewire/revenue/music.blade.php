<div>
    <div id="music-revenue-chart"></div>
    
    <div class="page-header">
        <div class="row">
            <div class="col-sm-12">
                <div class="widget-box">
                    <div class="widget-header">
                        <h4 class="widget-title">Tìm kiếm</h4>
                        <span class="widget-toolbar">
                            <a href="#" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up"></i>
                            </a>
                        </span>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row">
                                <div class="col-xs-4 col-sm-4">
                                    <label for="form-field-select-1">Số hợp đồng</label>
                                    <input class="form-control" name="country" type="text" wire:model.debounce.1000ms="searchNumber" placeholder = 'Số hợp đồng' />
                                </div>
                                <div class="col-xs-4 col-sm-4">
                                    <!-- #section:plugins/date-time.datepicker -->
                                    <label for="id-date-picker-1">Thời gian</label>
                                    @include('layouts.partials.input._inputDateRanger')
                                </div>
                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 d-flex" style="text-align:center;">
                                    @include('livewire.common._buttonReset')

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div><!-- /.page-header -->
    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Thống kê doanh thu nhạc</h2>
                    </div>

                    <div class="pull-right">
                        <button type="button" class="btn btn-outline-dark btn-sm" data-target="#modal-form-export-revenue" data-toggle="modal"
                                style="background:#FFFFFF !important;color: #7F7F7F !important;
                                            border: 1px black solid; padding: 7px; margin-top: 2px; outline: none;">
                            <span class="menu-icon fa fa-download" style="font-size: 18px;"></span>
                            Excel file
                        </button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <table id="simple-table" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th style="color: black; class="hidden-480"><center>STT</center></th>
                            <th style="color: black;"><center>Số hợp đồng</center></th>
                            <th style="color: black;"><center>Giá tiền</center></th>
                            <th style="color: black;"><center>Ngày ký</center></th>
                            <th style="color: black;"><center>Thời gian bắt đầu bản quyền</center></th>
                            <th style="color: black;"><center>Thời gian kết thúc bản quyền</center></th>
                        </tr>
                        </thead>
                        <div wire:loading class="loader"></div>
                        <tbody>
                            {{-- {{dd($data)}} --}}
                        @forelse ($data as $key => $rs)
                            <tr>
                                <td><center>{{ ($data->currentPage() - 1) * $data->perPage() + $loop->iteration }}</center></td>
                                <td><a href="{{route('contract.viettel-sell.edit', $rs->id)}}">{!! boldTextSearchV2($rs->contract_number,$searchNumber) !!}</a></td>
                                <td style="text-align:right; ">{{ numberFormat($rs->total_money) }}</td>
                                <td><center>{{reFormatDate($rs->date_sign) }}</center></td>
                                <td><center>{{ reFormatDate($rs->start_time)}}</center></td>
                                <td><center>{{ reFormatDate($rs->end_time)}}<center></td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="11" class="text-center">Không có kết quả phù hợp</td>
                            </tr>
                        @endforelse

                        </tbody>
                    </table>
                </div><!-- /.span -->
                @include('layouts.partials._pagination')

            </div><!-- /.row -->
        </div><!-- /.col -->
    </div><!-- /.row -->

    <!-- Export modal /-->
    <div class="modal fade" id="modal-form-export-revenue" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title" id="exampleModalLabel">Tải file excel xuống</h2>
                    </div>
                    <div class="modal-body">
                        Bạn có chắc chắn muốn xuất file không?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Quay lại</button>
                        <button type="button" wire:click="export" data-dismiss="modal" class="btn btn-danger" id='btn-upload-music'>Đồng ý</button>
                    </div>
            </div>
        </div>
    </div>

    @include('layouts.partials.lib._date_picker')
</div>

<script>

window.onload = function() {
    Livewire.emit('get-music-revenue-chart-data');
    Livewire.on('show-music-revenue-chart-data', (revenueData) => {
        Highcharts.chart('music-revenue-chart', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Biểu đồ doanh thu nhạc các ngày qua'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                title: {
                    text: 'Thời gian'
                },
                categories: revenueData.map(item => item.date_modified),
                type: "date",
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Doanh thu (triệu VND)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y} VND</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [
                {
                    name: 'Doanh thu ngày',
                    data: revenueData.map(item => Number(item.sum_money))
                }
            ]
        });
    });
}


/* (function() {

    let revenueData = JSON.parse(document.getElementById('music-revenue-chart-data').value);

})(); */

</script>
