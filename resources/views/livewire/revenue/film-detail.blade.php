<div>
    <figure class="highcharts-figure">
        {{-- <input type="hidden" id="revenue-data" value="{{json_encode($revenueDay)}}"> --}}
        <div id="revenue-chart"></div>
        {{-- <p class="highcharts-description">
            
        </p> --}}
    </figure>
    
    <div class="page-header">
        <div class="row">
            <div class="col-sm-12">
                <div class="widget-box">
                    <div class="widget-header">
                        <h4 class="widget-title">Tìm kiếm</h4>
                        <span class="widget-toolbar">
                            <a href="#" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up"></i>
                            </a>
                        </span>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row">
                                
                                <div class="col-xs-2 col-sm-2">
                                    <label>Tên phim:</label>
                                    <input class="form-control" type="text" wire:model.debounce.1000ms="searchName" placeholder='Tên phim' />
                                </div>
                                <div class="col-xs-2 col-sm-2" wire:ignore>
                                    <label>Thời gian:</label>
                                    <input class="form-control" type="text" name="start_date" value="{{request('start_date')}}" placeholder="Thời gian bắt đầu bản quyền phim" wire:model.debounce.1000ms="setDate" autocomplete="off" readonly style="background-color:white !important;"/>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <label>Quốc gia:</label>
                                    <select id='search_country' wire:model="searchCountry" style="min-width:90%;" class="form-control">
                                    <option value="">Tất cả quốc gia</option>
                                        @foreach($listCountry as $key => $val)
                                        <option value="{{$key}}">{{$val}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <label>Thể loại:</label>
                                    <select id='select_box' wire:model="searchCategory" style="min-width:90%;" class="form-control">
                                        <option value="" selected>---Chọn loại phim---</option>
                                        <option value="1">Phim lẻ</option>
                                        <option value="2">Phim bộ</option>
                                    </select>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <label>Số hợp đồng: </label>
                                    <input class="form-control" name="country" type="text" wire:model.debounce.1000ms="searchNumber" placeholder='Số hợp đồng'/>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <label>Loại báo cáo:</label>
                                    <select id='select_box' wire:model="viewType" style="min-width:90%;" class="form-control">
                                        <option value="2">Chi tiết</option>
                                        <option value="1">Tổng hợp</option>
                                    </select>
                                </div>
                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 d-flex" style="text-align:center;">
                                    @include('livewire.common._buttonReset')

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div><!-- /.page-header -->

    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Thống kê doanh thu phim</h2>
                    </div>
                    <div class="pull-right">
                        <button type="button"  id="export" class="btn btn-outline-dark btn-sm" data-target="#modal-form-export-revenue" data-toggle="modal"
                                style="background:#FFFFFF !important;color: #7F7F7F !important;
                                            border: 1px black solid; padding: 7px; margin-top: 2px; outline: none;">
                            <span class="menu-icon fa fa-download" style="font-size: 18px;"></span>
                            Excel file
                        </button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <table id="simple-table" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th style="text-align:center; color: black" class="hidden-480">STT</th>
                            <th style="text-align:center; color: black">Tên phim tiếng Việt</th>
                            <th style="text-align:center; color: black">Tên phim tiếng Anh</th>
                            <th style="text-align:center; color: black">Số hợp đồng</th>
                            <th style="text-align:center; color: black">Đối tác</th>
                            <th style="text-align:center; color: black">Tổng chi phí mua/tập</th>
                            <th style="text-align:center; color: black">Doanh thu đã bán</th>
                            <th style="text-align:center; color: black">Loại doanh thu</th>
                            <th style="text-align:center; color: black">Hạ tầng phát sóng mua</th>
                            <th style="text-align:center; color: black">Hạ tẩng phát sóng bán</th>
                            <th style="text-align:center; color: black">Thời gian bắt đầu</th>
                            <th style="text-align:center; color: black">Thời gian kết thúc</th>
                            <th style="text-align:center; color: black">Ngày ký hợp đồng</th>
                        </tr>
                        </thead>
                        <div wire:loading class="loader"></div>
                        <tbody>
                        @forelse ($data as $key => $rs)
                            @if($rs->media_id)
                                <tr>
                                    <td style="text-align:center;">{{ ($data->currentPage() - 1) * $data->perPage() + $loop->iteration }}</td>
                                    <td><a href="{{route('media.films.show', $rs->media_id)}}">{!! boldTextSearchV2($rs->vn_name,$searchName) !!}</a></td>
                                    <td>{{ $rs->product_name}}</td>
                                    <td>{!! boldTextSearchV2($rs->con_num,$searchNumber) !!}</td>
                                    <td>{{ $rs->contract->partner?$rs->contract->partner->name:'' }}</td>
                                    <td style="text-align:right; ">{{ numberFormat(countChiPhiTap($rs->film)) }}</td>
                                    <td style="text-align:right; ">{{ numberFormat($rs->moneyVAT) }}</td>
                                    <td style="text-align:right; ">{{$rs->revenueType->name }}</td>

                                    <td>
                                        @foreach($buyRoles as $role)
                                            @if($role->media_id==($rs->film->id??'') && $role->level==1)
                                                <span class="badge badge-grey">{{$role->code}}</span>
                                            @endif
                                        @endforeach
                                    </td>


                                    <td>
                                        @foreach($saleRoles as $role)
                                            @if($role->sale_money_id==$rs->sale_money_id && $role->level==1)
                                                <span class="badge badge-grey">{{$role->code}}</span>
                                            @endif
                                        @endforeach
                                    </td>

                                    <td><center>{{ reFormatDate($rs->media_sale->start_time) }}</center></td>
                                    <td><center>{{ reFormatDate($rs->media_sale->end_time) }}</center></td>
                                    <td><center>{{ reFormatDate($rs->contract->date_sign) }}</center></td>
                                </tr>
                            @endif
                        @empty
                            <tr>
                                <td colspan="13" class="text-center">Không có kết quả phù hợp</td>
                            </tr>
                        @endforelse

                        </tbody>
                    </table>
                </div><!-- /.span -->
                @include('layouts.partials._pagination')

            </div><!-- /.row -->
        </div><!-- /.col -->
    </div><!-- /.row -->
    
    <!-- Export modal /-->
    <div class="modal fade" id="modal-form-export-revenue" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tải file excel xuống</h5>
                    </div>
                    <div class="modal-body">
                        Bạn có chắc chắn muốn xuất file không?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Quay lại</button>
                        <button type="button" wire:click="exportChiTiet" data-dismiss="modal" class="btn btn-danger" id='btn-upload-music'>Đồng ý</button>
                    </div>
            </div>
        </div>
    </div>
    @include('layouts.partials.lib._date_picker')
</div>
<script src="{!! asset('assets/js/highcharts.js') !!}"></script>
<script type="text/javascript">
    document.getElementById('tonghop').onclick = function() {
        $('#tonghop_export').css('display','inline');
        $('#chitiet_export').css('display','none');
    }
    document.getElementById('chitiet').onclick = function() {
        $('#tonghop_export').css('display','none');
        $('#chitiet_export').css('display','inline');
    }
    document.getElementById('export').onclick = function() {
        $('#tonghop').removeAttr('checked');
        $('#chitiet').removeAttr('checked');
        $('#tonghop_export').css('display','none');
        $('#chitiet_export').css('display','none');
    }
</script>
