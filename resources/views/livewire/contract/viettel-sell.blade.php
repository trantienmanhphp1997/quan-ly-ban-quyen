<div>
    <div>
        <div>
            <div class="form-group row">
                <div class="col-lg-3" style="text-align: center;">
                    <button class="btn deadline-3m" type="button" wire:click="query1(3)" style="background: #FBA420 !important; border: 4px solid #FBA420; margin-top: 5px;">
                        Sắp hết hạn trong vòng 3 tháng<br>
                        <span style="font-size: 25px;">{{$threeMonth}}</span>
                    </button>
                </div>
                <div class="col-lg-3" style="text-align: center;">
                    <button class="btn deadline-2m" type="button" wire:click="query1(2)" style="background: #FB8441 !important; border: 4px solid #FB8441; margin-top: 5px;" >
                        Sắp hết hạn trong vòng 2 tháng<br>
                        <span style="font-size: 25px;">{{$twoMonth}}</span>
                    </button>
                </div>
                <div class="col-lg-3" style="text-align: center;">
                    <button class="btn deadline-1m" type="button" wire:click="query1(1)" style="background: #EE5816 !important; border: 4px solid #EE5816;  margin-top: 5px;">
                        Sắp hết hạn trong vòng 1 tháng<br>
                        <span style="font-size: 25px;">{{$oneMonth}}</span>
                    </button>
                </div>
                <div class="col-lg-3" style="text-align: center;">
                    <button class="btn deadline-1w" type="button" wire:click="query1(7)" style="background: #FB1F0E !important; border: 4px solid #FB1F0E;  margin-right: 10px; margin-top: 5px;">
                        Sắp hết hạn trong vòng 1 tuần<br>
                        <span style="font-size: 25px;">{{$oneWeek}}</span>
                    </button>
                </div>
            </div>
        </div>
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="widget-title">Tìm kiếm</h4>
                            <span class="widget-toolbar">
                                <a href="#" data-action="collapse">
                                    <i class="ace-icon fa fa-chevron-up"></i>
                                </a>
                            </span>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                {!! Form::open(['method'=>'GET','route'=>'contract.viettel-sell','role'=>'search'])  !!}
                                <div class="row">
                                    <div class="col-xs-3 col-sm-3">
                                        <label for="form-field-select-1">Số hợp đồng</label>
                                        <input class="form-control" type="text" placeholder="Số hợp đồng" wire:model.debounce.1000ms="searchTerm"
                                        id='input_vn_name' autocomplete="off" />
                                    </div>
                                    <div class="col-xs-3 col-sm-3">
                                        <label for="form-field-select-1">Số điện thoại</label>
                                        <input class="form-control" type="text" placeholder="Số điện thoại người phụ trách" wire:model.debounce.1000ms="searchPhone"
                                        id='input_vn_name' autocomplete="off" />
                                    </div>
                                    <div class="col-xs-2 col-sm-2" wire:ignore>
                                        <!-- #section:plugins/date-time.datepicker -->
                                        <label for="id-date-picker-1">Thời gian</label>
                                        @include('layouts.partials.input._inputDateRanger')
                                    </div>
                                    <div class="col-xs-2 col-sm-2" wire:ignore>
                                        <label for="form-field-select-1">Thể loại</label>
                                        <br>
                                         <select id='select_box' class="form-control" wire:model="searchCategory" style="min-width:90%;" {{$searchCategory?'disabled':''}}>
                                            @foreach($categories as $key=> $item)
                                                <option value="{{$key}}" {{$searchCategory == $key?'selected':''}}>{{$item}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <!-- #section:plugins/date-time.datepicker -->

                                    <div class="col-xs-2 col-sm-2">
                                        <label for="form-field-select-1">Trạng thái</label>
                                        <select id='select_box' wire:model="searchStatus" style="min-width:90%;" class="form-control">
                                            </option>
                                                <option value="0" selected="selected">Hiệu lực</option>
                                                <option value="1">Hết hạn</option>
                                                <option value="2">Đã xóa</option>
                                                <option value="3">Tất cả</option>
                                        </select>
                                    </div>
                                </div>

                                <hr />
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 d-flex" style="text-align:center;">
                                        @include('livewire.common._buttonReset')
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.page-header -->
    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>QUẢN LÝ HỢP ĐỒNG BÁN BẢN QUYỀN</h2>
                    </div>
                        <div class="pull-right">
                            @include('layouts.partials.button._new')
                            @hasanyrole('administrator|contract-viettel-sale-create-edit|contract-viettel-sale-delete')
                                <button type="button" class="btn btn-sm btn-outline-dark" data-target="#exampleModal3" data-toggle="modal" style="background:#FFFFFF !important;color: #7F7F7F !important;border: 1px black solid; padding: 7px;outline: none;" title="Thêm danh sách phim bán">
                                    <span class="menu-icon fa fa-upload" style="font-size: 18px;"></span>
                                    Tải lên
                                </button>
                            @endhasanyrole
                            <button type="button" id='show_update_modal' class="btn btn-danger btn-sm" style='outline: none;'data-target="#modal-form-delete-all-film" data-toggle="modal" {{(count($checkContracts))?'':'disabled'}} ><span class="menu-icon fa fa-trash" style="font-size: 18px;"></span> Xóa tất cả</button>
                            <!-- @hasanyrole('administrator|contract-viettel-sale-create-edit|contract-viettel-sale-delete')
                                <button type="button" class="btn btn-outline-dark" data-target="#exampleModalMusicSale" data-toggle="modal" style="background:#FFFFFF !important;color: #7F7F7F !important;border: 1px black solid; padding: 10px;outline: none;" title="Xuất excel file danh sách nhạc bán">
                                    <span class="menu-icon fa fa-download" style="font-size: 18px;"></span>
                                    Excel nhạc
                                </button>
                            @endhasanyrole -->
                        </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <table id="simple-table" class="table table-striped table-bordered table-hover">

                        <thead>
                            <tr>
                                <th></th>
                                <th class="hidden-480" style="color: black;"><center>STT</center></th>
                                <th style="color: black;"><center>Số hợp đồng</center></th>
                                <th style="color: black;"><center>Ngày ký</center></th>
                                <th style="color: black;"><center>Đối tác</center></th>
                                <th style="color: black;"><center>Thể loại hợp đồng</center></th>
                                <th style="color: black;"><center>Tổng giá trị</center></th>
                                <th style="color: black;"><center>Người phụ trách</center></th>
                                <th style="color: black;"><center>Số điện thoại</center></th>
                                <th style="color: black;"><center>Thời gian hết thúc</center></th>
                                <th style="color: black;"><center>Hành động</center></th>
                            </tr>
                        </thead>
                        <div wire:loading class="loader"></div>
                        <tbody>
                            @forelse ($data as $key => $rs)
                            <tr>
                                <td>
                                    @if(myMedia($rs->admin_id))
                                        @if($rs->deleted_at)

                                        @else
                                        <input type="checkbox" wire:model="checkContracts" value="{{$rs->id}}">
                                        @endif
                                    @endif
                                </td>
                                <td style="text-align: center;">{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</td>
                                <td>{!! boldTextSearchV2($rs->contract_number,$searchTerm) !!}</td>
                                <td><center>{{ reFormatDate($rs->date_sign, 'd-m-Y') }}</center></td>
                                <td>{{ $rs->partner }}</td>
                                <td>{{ $rs->category }}</td>
                                <td style="text-align:right;">{{ numberFormat($rs->total_money) }}</td>
                                <td>{{ $rs->user_name }}</td>
                                <td>{!! boldTextSearchV2($rs->user_msisdn,$searchPhone) !!}</td>
                                <td>
                                    <center>
                                        {{reFormatDate($rs->end_time, 'd-m-Y')}}
                                        @if($rs->deleted_at)
                                        @else
                                        <br>
                                        @if((strtotime(reFormatDate($rs->end_time, 'd-m-Y')) >= strtotime(date('d-m-Y'))||!$rs->end_time))
                                        <button  title="Click vào để gia hạn" type="button"  wire:click="extensionId({{ $rs->id }})" class="btn btn-xs btn-primary"  data-target="#exampleModal" data-toggle="modal" >Gia hạn</button>
                                        @else
                                        <button title="Click vào để gia hạn" type="button"  wire:click="extensionId({{ $rs->id }})" class="btn btn-xs btn-danger"  data-target="#exampleModal" data-toggle="modal">Đã hết hạn</button>
                                        @endif
                                        @endif
                                    </center>
                                </td>
                                <td>
                                    <center>
                                    @if($rs->deleted_at)
                                        <button data-toggle="tooltip" title="Khôi phục" style="background: #fff; border: 1px solid black;margin-top: 2px;" type="button" class ='btn-buy-music' wire:click="restore({{$rs->id}})" >
                                        <i class="fa fa-undo" style="font-size:16px; color: #2199EB;"></i>
                                        </button>
                                    @else
                                    @if($rs->warning_status==0)
                                        <button title="Tắt thông báo" type="button" wire:click="notify({{$rs->id}},1)" style="background: orange ; border: 1px solid black;margin-top: 2px; padding: 1px 8px;"  class="btn-edit-film btn-warning" >
                                            <i class="ace-icon fa fa-bell bigger-120"></i>
                                        </button>
                                    @else
                                        <button title="Bật thông báo" type="button" wire:click="notify({{$rs->id}},0)" style="background: orange ; border: 1px solid black;margin-top: 2px; padding: 1px 8px;"  class="btn-edit-film btn-warning" >
                                            <i class="ace-icon fa fa-bell-slash bigger-120"></i>
                                        </button>
                                    @endif
                                    @php $id = $rs->id; @endphp
                                    @include('layouts.partials.button._edit')
                                    @include('layouts.partials.button._delete')

                                    {{-- Button copy --}}
                                    {!! Form::open(['method' => 'GET','route' => ['contract.viettel-sell.copy', $rs->id] ,'style'=>'display:inline']) !!}
                                    <button title="sao chép hợp đồng" type="submit" style="background: orange ; border: 1px solid black;margin-top: 2px; padding: 1px 8px;"  class="btn-edit-film btn-success" >
                                        <i class="ace-icon fa fa-copy bigger-120"></i>
                                    </button>
                                    {!! Form::close() !!}
                                    @endif
                                    </center>
                                </td>
                            </tr>
                            @empty
                            <tr>
                             <td colspan="12" class="text-center">Không có kết quả phù hợp</td>
                         </tr>
                         @endforelse
                     </tbody>
                 </table>
             </div>
             <!-- /.span -->
             {{ $data->links() }}
                @include('livewire.contract._modalContractExtension')
                @include('livewire.common._message')
                @include('livewire.common._modalConfirmDelete')
               <div class="modal fade" id="exampleModal3" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" wire:ignore>
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h2 class="modal-title" id="exampleModalLabel">Tải lên danh sách hợp đồng/phim bán</h2>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <form action="{{ route('contract.viettel-sell.saveFilmSale') }}" method="POST" enctype="multipart/form-data" id='form-upload-file-contract-music'>
                                        {{csrf_field()}}
                                        <input type="file" id="file-upload-contract-music" name='file' accept=".xlsx,.xls">
                                    </form>
                                </div>
                            </div>
                            <div class="modal-footer" style="text-align: center;">
                                <form>
                                    {{csrf_field()}}
                                    <a href="#" style="float: left;" id='example_elementB'>File excel mẫu &nbsp;</a>
                                    <button style="display:none;" id='button_exampleB' type="button" wire:click="example" class="btn btn-danger" data-dismiss="modal">Đồng ý</button>
                                </form>
                                <div style="float: left;">
                                    <p class="text-danger" id='text-error-list-film'></p>
                                </div>
                                <div style="float: right;">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                                    <button type="button" class="btn btn-primary" id='btn-save-contract-music-sale'>Lưu</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- <div class="modal fade" id="exampleModalMusicSale" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form action="{{ route('media.music.export') }}" method="POST" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="modal-header">
                                    <h2 class="modal-title" id="exampleModalLabel">Tải danh sách bài hát bán</h2>
                                </div>
                                <div class="modal-body">
                                    Bạn có chắc chắn muốn xuất file không?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Quay lại</button>
                                    <button type="button" wire:click="export" data-dismiss="modal"class="btn btn-danger" id='btn-upload-music'>Đồng ý</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div> -->

                <div class="modal fade" id="modal-download-error" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h2 class="modal-title" id="exampleModalLabel">Tải xuống danh sách lỗi</h2>
                            </div>
                            <form method="POST" name='form-get-error-upload' action="{{route('media.music.getError')}}">
                                {{csrf_field()}}
                                <div class="modal-body">
                                    <div class="form-group">
                                        <input id='error-upload-music' type="text" name="error-upload" value="{{ Session::get('outputError') }}" style="display: none;" >
                                    </div>
                                </div>
                                <div class="modal-footer" style="text-align: center;">
                                    <div style="float: right;">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
                                        <button type="button" class="btn btn-primary" id='btn-submit-get-error' wire:click=''>Tải file lỗi</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
                {{-- modal xóa tất cả hợp đồng được check --}}
                <div class="modal fade" id="modal-form-delete-all-film" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h2 class="modal-title" id="exampleModalLabel">Xác nhận xóa tất cả hợp đồng</h2>
                            </div>
                            <div class="modal-body">
                                Bạn có muốn xóa tất cả hợp đồng được chọn không?
                            </div>
                            <div class="modal-footer" style="text-align: center;">
                                <div style="float: left;">
                                    <p class="text-danger" id='modal-p-delete-all-film' style="display: inline-block;"></p>
                                </div>
                                <div style="float: right;">
                                    <button type="button" class="btn" data-dismiss="modal">Đóng</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal" wire:click='deleteAll()'>Xóa bỏ</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- end modal--}}

         </div>
         <!-- /.row -->
     </div>
     <!-- /.col -->
 </div>
 <!-- /.row -->

    @include('layouts.partials.lib._date_picker')
</div>
@section('js')
    <script>
        document.addEventListener('livewire:load', function () {
            $(function () {
                $("#btn-save-revenue").on('click', (e) => {
                    if(!document.getElementById("id-date-picker-1 date_sign")) var x='';
                    else x=document.getElementById("id-date-picker-1 date_sign").value;
                    if(!document.getElementById("start-time")) var z='';
                    else z=document.getElementById("start-time").value;
                    if(!document.getElementById("type_id")) var y='';
                    else y=document.getElementById("type_id").value;
                    window.livewire.emit('storeRevenue',2,
                        $('#media_id').val(),
                        document.getElementById("category_id").value,
                        x,
                        $('#money_revenue').val(),
                        $('#type_revenue').val(),
                        y,
                        document.getElementById("id-date-picker-1 end_time").value,
                        z,
                    );
                });
                $('#btn-submit-get-error').on('click', (e) => {
                    var data = $('#error-upload-music').val()
                    window.livewire.emit('getError', data);
                })
            });
        });
        document.addEventListener('DOMContentLoaded', function(){
            $('#btn-save-contract-music-sale').click(function(){
                var fileListFilm = $('#file-upload-contract-music').val()
                if(fileListFilm==''||fileListFilm==null){
                    $('#text-error-list-film').text('*Chưa có file');
                }
                else {
                    $(this).attr('disabled', true)
                    var formCreateFilm = document.forms['form-upload-file-contract-music']
                    formCreateFilm.submit()
                }
            })
            $('#btn-upload').click(function(){
                $('#text-error-list-film').text('');
            })
            $('#example_elementB').click(function(e){
                e.preventDefault()
                document.getElementById('button_exampleB').click()
            })
            $('#btn-download-error').click()
        })
    </script>
    <script>

        document.addEventListener('livewire:load', function () {  
            $(function () {
                $('input[name="start_date"]').on('apply.daterangepicker', (e)=>{
                    window.livewire.emit('searchTime',
                        document.getElementById('id-date-range-picker-1').value,
                    );
                });
            });
        });

    </script>
    <script>
        function getEditURL(id) {
            var url = window.location.href;
            url = url.split('?')[0];
            window.location.href = url + "/" + id +  "/edit";
        }
    </script>
@endsection

