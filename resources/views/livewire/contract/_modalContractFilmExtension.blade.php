<form wire:submit.prevent="submit" autocomplete="off">
    <div wire:init="openModal" wire:ignore.self  class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        {!! csrf_field() !!}
        <div class="modal-backdrop fade in" style="height: 100%;"></div>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel" style="font-weight: bold;">
                        Gia hạn hợp đồng bán phim
                    </h3>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-top: 10px;">
                        @foreach($data_revenue as $item)
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <lable class="col-md-4 col-form-label">Số hợp đồng:</lable>
                                        <input type="text" name="contract_number" value="{{$item->contract_number}}" id="contract_number" placeholder="mã hợp đồng" class="col-md-8" disabled>
                                    </div>
                                    <div class="col-md-6">
                                        <lable class="col-md-4 col-form-label">Loại hợp đồng:</lable>
                                        <select name="type_contract" id="type_contract" class="col-md-8" disabled>
                                            <option value="0" selected>Vietel bán</option>
                                            <option value="1">Vietel mua</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <lable class="col-md-4 col-form-label">Tên phim:</lable>
                                        <input type="text" name="name" value="{{$item->vn_name}}" id="vn_name" placeholder="mã sản phẩm media" class="col-md-8" disabled>
                                        <input type="hidden" name="media_id" value="{{$item->media_id}}" id="media_id">

                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group row">
                                    <div class="col-md-6" >
                                        <label class="col-md-4 col-form-label" >Ngày ký<span class="text-danger">(*)</span></label>
                                        <div class="input-group col-sm-8" wire:ignore>
                                            <input type="text" class="form-control date-picker" id="id-date-picker-1 start_time2" name='start_time'
                                                   data-date-format="dd-mm-yyyy" id="date_sign" placeholder="Ngày ký" required>
                                            @include('layouts.partials.input._calender')
                                        </div>
                                        <div>
                                            @error('dataSign') 
                                                @include('layouts.partials.text._error')
                                            @enderror     
                                        </div>
                                    </div>
                                    <div class="col-sm-6" >
                                        <lable class="col-sm-4 col-form-label">Loại doanh thu:</lable>
                                        <input type="text" name="name" value="{{$revenue1?$revenue1->name:''}}" class="col-md-8" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group row">
                                    <div class="col-md-6" >
                                        <lable class="col-md-4 col-form-label">Số tiền:</lable>
                                        {!! Form::number('money_revenue', null, array('class' => 'col-md-8','id'=>'money_revenue', 'placeholder'=> 'Số tiền gia hạn','oninput'=>"this.value = Math.abs(this.value)")) !!}
                                        <div>
                                            @error('moneyRevenue') 
                                                @include('layouts.partials.text._error')
                                            @enderror     
                                        </div> 
                                    </div>
                                    <div class="col-md-6" >
                                        <lable class="col-md-4 col-form-label">Doanh thu từ:</lable>
                                        {!! Form::select('type_revenue', [0=>'Ký hợp đồng', 1=>'Gia hạn'], 1, array('class' => 'col-md-8','readonly'=>'readonly','id'=>'type_revenue')) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label class="col-md-4 col-form-label" >Ngày bắt đầu</label>
                                        <div class="input-group col-sm-8">
                                            <input type="text" class="form-control date-picker" id="id-date-picker-1 start_time1" name='end_date'
                                                   data-date-format="dd-mm-yyyy" id="end_date" value = "{{$item->start_time?reFormatDate($item->start_time):''}}" placeholder="Thời gian bắt đầu" disabled>
                                            @include('layouts.partials.input._calender')
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="col-md-4 col-form-label" >Ngày kết thúc</label>
                                        <div class="input-group col-sm-8">
                                            <input type="text" class="form-control date-picker" id="id-date-picker-1 end_time1" name='end_date'
                                                   data-date-format="dd-mm-yyyy" id="end_date" value = "{{$item->end_time?reFormatDate($item->end_time):''}}" placeholder="Thời gian kết thúc">
                                            @include('layouts.partials.input._calender')
                                        </div>
                                        <div>
                                            @error('endTime') 
                                                @include('layouts.partials.text._error')
                                            @enderror     
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="col-xs-12 col-sm-12 col-md-12 text-right " style="margin-top: 40px;">
                            <button wire:loading.attr="disabled" id="btn-save-revenue" class="btn btn-primary" type="button" >Lưu</button>
                            <button type="button" class="btn btn-secondary" id="close-media-film-modal" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
