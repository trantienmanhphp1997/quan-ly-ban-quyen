<div>
    <table  class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th class="hidden-480" style="text-align: center; color: black;">STT</th>
                <th style="text-align: center; color: black;">Tên giấy tờ</th>
                <th style="text-align: center; color: black;">Ngày ký</th>
                <th style="text-align: center; color: black;">Link</th>
                <th style="text-align: center; color: black;" width="280px">Hành động</th>
            </tr>
        </thead>
      <div wire:loading class="loader"></div>
        <tbody>
            @foreach ($contractFiles as $contractFile)
            <tr>
                <td style="text-align: center;">{{($contractFiles->currentPage() - 1) * $contractFiles->perPage() + $loop->iteration}}</td>
                <td>{{ $contractFile->name }}</td>
                <td style="text-align: center;">{{ reFormatDate($contractFile->date_sign) }}</td>
                <td><a id='onClickElementA' wire:click='forDownload({{$contractFile->id}})' href="#">{{ $contractFile->link }}</a></td>
                <td style="text-align: center;">
                    <button type="button" wire:click="deleteId({{ $contractFile->id }})" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#ModalDelete">
                        <i class="menu-icon fa fa-trash-o" style="color: white;font-size:16px;"></i>
                    </button>
                    <button type="button" wire:click="editId({{ $contractFile->id }})" class="btn btn-xs btn-primary btn-edit" data-toggle="modal" data-target="#ModalContractFileEdit">
                        <i class="menu-icon fa fa-pencil" style="color: white;font-size:16px;"></i>
                    </button>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @include('livewire.common._message')
    <!-- Modal -->
    @include('livewire.common._modalConfirmDelete')
    <div wire:init="openModal" wire:ignore.self class="modal fade" id="ModalContractFileEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-backdrop fade in" style="height: 1024px;"></div>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel" style="font-weight: bold;">Sửa giấy tờ</h3>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-4 col-form-label" >Tên giấy tờ<span class="text-danger">(*)</span></label></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="file_name_edit" id="file_name_edit" placeholder="Tên giấy tờ" value="{{$file?($this->kt?$this->file_name_edit:$file->name):''}}">
                            @error('file_name_edit') 
                                @include('layouts.partials.text._error')
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-4 col-form-label">Ngày ký</label>
                        <div class="col-sm-8">
                            <input type="date" class="form-control" name="date_sign_edit"  placeholder="Ngày ký" value="{{$file?$file->date_sign:''}}" id='date_sign_edit'>
                            @error('date_sign_edit')
                                @include('layouts.partials.text._error')
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-4 col-form-label">Giấy tờ liên quan</label>
                        <div class="col-sm-8">
                            <input type="file" class="form-control-file" name='file_upload_edit' id="file_upload_edit" placeholder="Ghi chú nội dung" wire:model.lazy="file_upload_edit" accept=".doc, .xlsx, .png,.docx,.jpg,.pdf">
                            <span class='red' wire:loading wire:target="file_upload_edit"><strong>Chờ tải file...</strong><br></span>
                            @error('file_upload_edit') 
                                @include('layouts.partials.text._error')
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="close-contract-file-modal-edit" class="btn btn-secondary close-btn" data-dismiss="modal" aria-label="Close" wire:click.prevent="resetInput()" >Đóng</button>
                    <button type="button" id='btn-save-edit' class="btn btn-primary" wire:target="file_upload_edit" wire:loading.class="disabled">Lưu</button>
                </div>
            </div>
        </div>
    </div>
    {{$contractFiles->links()}}
    <div class="row">
        <div class="col-xs-12">
            <!-- Button trigger modal -->
            <button type="button" id='button_add' class="btn btn-primary" data-toggle="modal" data-target="#ModalContractFile">
                + Thêm giấy tờ
            </button>
            <!-- Modal -->
            @include('livewire.contract._modalCreateContractFile')
        </div><!-- /.span -->
    </div>
</div>
<script>
    $("document").ready(function() {
        $('#onClickElementA').click(function(e){
            e.preventDefault()
        })
        $('#button_add').click(function(){
            $('.red').remove();
        })
        $('.btn-edit').click(function(){
            $('#file_upload_edit').val('');
            $('.red').remove();
        })
    })
    document.addEventListener('livewire:load', function () {  
        $(function () {
            $('#btn-save-edit').on('click', (e)=>{
                @this.emit('update',{
                    file_upload_edit: $('#file_upload_edit').val(),
                    date_sign_edit: $('#date_sign_edit').val(),
                    file_name_edit: $('#file_name_edit').val(),
                });
            });
        });
    });
</script>



