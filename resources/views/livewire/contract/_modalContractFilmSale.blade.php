<div>
<form wire:submit.prevent="submit" autocomplete="off">
    <div wire:init="openModal" wire:ignore.self class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        {!! csrf_field() !!}
        <div class="modal-backdrop fade in" style="height: 150%;"></div>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel" style="font-weight: bold;">
                        Thêm phim
                    </h3>
                    <button type="button" id="close-media-music-modal" class="close" data-dismiss="modal" aria-label="Close" multiple>
                   </button>
               </div>
               <div class="modal-body" id='modal-body-contract-music'>
                     <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-4 col-form-label">Tìm kiếm</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name='search' id="searchTerm" placeholder="Nhập tên phim: từ 2 ký tự" wire:model.lazy="searchTerm">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-4 col-form-label">Tên phim {{$country}} (<span class="text-danger">*</span>){{ count($arrFilm)}}</label>
                        <div class="col-sm-6">
                            <select id='select_box_fim' class="form-control select_box_fim input-xlarge">
                                @foreach($arrFilm as $key=> $name)
                                    <option {{$country==$key?'selected':''}} value="{{$key}}">{{ $name }}</option>
                                @endforeach
                            </select>
                            @error('film') 
                                @include('layouts.partials.text._error')
                            @enderror 
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-4 col-form-label">Loại doanh thu</label>
                        <div class="col-sm-6">
                            <select name="type_id" class="form-control" id="type_id1">
                                {!! $revenue !!}
                            </select>
                        </div>
                    </div>
                   <div class="form-group row">
                       <label for="inputEmail3" class="col-sm-4 col-form-label">Giá tiền chưa VAT</label>
                       <div class="col-sm-6">
                            <input type="number" min="0"  class="form-control" id="moneyVAT" name="moneyVAT"  placeholder="Giá tiền chưa VAT" oninput="this.value = Math.abs(this.value)">
                            @error('moneyVAT') 
                                @include('layouts.partials.text._error')
                            @enderror 
                       </div>
                   </div>
                   <div class="form-group row">
                       <label for="inputEmail3" class="col-sm-4 col-form-label">Giá tiền đã VAT</label>
                       <div class="col-sm-6">
                            <input type="number" min="0"  class="form-control" id="money" name="money" placeholder="Giá tiền đã VAT" oninput="this.value = Math.abs(this.value)">
                            @error('money') 
                                @include('layouts.partials.text._error')
                            @enderror 
                       </div>
                   </div>
                    <div class="form-group row"  wire:ignore>
                       <label for="inputEmail3" class="col-sm-4 col-form-label">Số lần phát sóng</label>
                       <div class="col-sm-6">
                            <input type="text" class="form-control" id="count_live" name="count_live" placeholder="Số lần phát sóng">
                       </div>
                   </div>
                   <div class="form-group row" wire:ignore>
                       <label for="inputEmail3" class="col-sm-4 col-form-label">ĐỘC QUYỀN</label>
                       <div class="col-sm-6">
                           <select name="role1[]" class="select_role input-xlarge" id="role1" multiple >
                               @foreach($data_type1 as $value)
                                   <option value="{{$value['id']}}">
                                       {!! $value['text'].$value['name'] !!}
                                   </option>
                               @endforeach
                           </select>
                       </div>
                   </div>
                   <div class="form-group row" wire:ignore>
                       <label for="inputEmail3" class="col-sm-4 col-form-label">KHÔNG ĐỘC QUYỀN</label>
                       <div class="col-sm-6">
                           <select name="role2[]" class="select_role input-xlarge" id="role2" multiple>
                               @foreach($data_type2 as $value)
                                   <option value="{{$value['id']}}">
                                       {!! $value['text'].$value['name'] !!}
                                   </option>
                               @endforeach
                           </select>
                       </div>
                   </div>
                   <div class="form-group row" wire:ignore>
                       <label for="inputEmail3" class="col-sm-4 col-form-label">Quốc gia được phép phát sóng</label>
                       <div class="col-sm-6">
                           <select name="role2[]" class="select_country input-xlarge" id="countryFilm" multiple>
                               @foreach($filmCountry as $value)
                                   <option value="{{$value->id}}">
                                       {{ $value->name }}
                                   </option>
                               @endforeach
                           </select>
                       </div>
                   </div>
                   <div class="form-group row" wire:ignore>
                       <label for="inputEmail3" class="col-sm-4 col-form-label">Nước phát sóng</label>
                       <div class="col-sm-6">
                           <input type="text" name="nuoc" id="country" placeholder="Nước phát sóng"  class="form-control">
                       </div>
                   </div>
                   <div class="form-group row" wire:ignore>
                       <label for="inputEmail3" class="col-sm-4 col-form-label">Ghi chú nước phát sóng</label>
                       <div class="col-sm-6">
                           <textarea style='padding: 5px 4px 6px' name="note" id="note_nuoc" rows="3" placeholder="Ghi chú nước phát sóng"  class="form-control"></textarea>
                       </div>
                   </div>
                   <div class="form-group row" wire:ignore>
                       <label for="note" class="col-sm-4 col-form-label">Ghi chú phim</label>
                       <div class="col-sm-6">
                           <textarea name="note" style='padding: 5px 4px 6px' id="note_film" rows="3" placeholder="Ghi chú phim"  class="form-control"></textarea>
                       </div>
                   </div>
                    <div class="form-group row" wire:ignore>
                       <label for="inputEmail3" class="col-sm-4 col-form-label">Hạ tầng phát sóng</label>
                       <div class="col-sm-6">
                           <select name="role3[]" class="select_role2 input-xlarge" id="role3" multiple>                        
                               @foreach($role_level as $key=> $item)
                                   <option value="{{$key}}">
                                       {{$item}}
                                   </option>
                               @endforeach
                           </select>
                       </div>
                   </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-4 col-form-label">Thời gian bắt đầu</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                            <input type="text" autocomplete="off" class="form-control date-picker" id="id-date-picker-1 start_time"
                                   data-date-format="dd-mm-yyyy" name="start_time" value = "{{$start_time}}"  placeholder="Thời gian bắt đầu">
                            @include('layouts.partials.input._calender')
                            </div>
                            <div>
                                @error('startTime') 
                                    @include('layouts.partials.text._error')
                                @enderror 
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-4 col-form-label">Thời gian kết thúc</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                            <input type="text" class="form-control date-picker" id="id-date-picker-1 end_time" name='end_time'
                                   data-date-format="dd-mm-yyyy" id="end_time" value = "{{$end_time}}" placeholder="Thời gian kết thúc">
                            @include('layouts.partials.input._calender')
                            </div>
                           <div>
                                @error('endTime1') 
                                    @include('layouts.partials.text._error')
                                @enderror 
                           </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button wire:loading.attr="disabled" id='btn-save-contract-music' class="btn btn-primary" type="button">Lưu</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id='close-modal'>Đóng</button>
                </div>
        </div>
    </div>
    </div>
</form>
</div>
