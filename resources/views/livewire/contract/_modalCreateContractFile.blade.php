<form wire:submit.prevent="submit">
    <div wire:init="openModal" wire:ignore.self class="modal fade" id="ModalContractFile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <!-- <form method="POST" id="upload-form" enctype="multipart/form-data"> -->
            {!! csrf_field() !!}
            <div class="modal-backdrop fade in" style="height: 1024px;"></div>
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title" id="exampleModalLabel" style="font-weight: bold;">Thêm giấy tờ</h3>

                        </div>
                        <div class="modal-body">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-4 col-form-label" >Tên giấy tờ<span class="text-danger">(*)</span></label></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="file_name" id="file_name" placeholder="Tên giấy tờ" wire:model.lazy="file_name">
                                    @error('file_name') 
                                        @include('layouts.partials.text._error')
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-4 col-form-label">Ngày ký</label>
                                <div class="col-sm-8">
                                    <input type="date" class="form-control" name="date_sign"  placeholder="Ngày ký" wire:model.lazy="date_sign">
                                    @error('date_sign')
                                        @include('layouts.partials.text._error')
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-4 col-form-label">Giấy tờ liên quan<span class="text-danger">(*)</span></label>
                                <div class="col-sm-8">
                                    <input type="file" class="form-control-file" name='file_upload' id="file_upload" placeholder="Ghi chú nội dung" wire:model.lazy="file_upload" accept=".doc, .xlsx, .png,.docx,.jpg, .pdf">
                                    <span class="red" wire:loading wire:target="file_upload"><strong>Chờ tải file...</strong><br></span>
                                    @error('file_upload') 
                                        @include('layouts.partials.text._error')
                                    @enderror
                                </div>
                            </div>
                          {{--  <div class="form-group row">
                                @if ($file_upload)
                                    <label for="inputEmail3" class="col-sm-4 col-form-label"></label>
                                    @if($file_upload->getClientOriginalExtension()=='png'||$file_upload->getClientOriginalExtension()=='jpeg'||$file_upload->getClientOriginalExtension()=='jpg')
                                        <div class="col-sm-8">
                                            <img src="{{ $file_upload->temporaryUrl() }}" style='width:150px'>
                                        </div>
                                
                                    @elseif($file_upload->getClientOriginalExtension()=='pdf')
                                        <div class="col-sm-8">
                                            <embed src="{{ $file_upload->temporaryUrl() }}" width="350px"/>
                                        </div>
                                    @elseif($file_upload->getClientOriginalExtension()=='doc'||$file_upload->getClientOriginalExtension()=='xlsx'||$file_upload->getClientOriginalExtension()=='docx'||$file_upload->getClientOriginalExtension()=='xml')
                                        <div class="col-sm-8">
                                            <iframe src="{{'https://docs.google.com/gview?url='.$file_upload->temporaryUrl().'&embedded=true'}}" frameborder="0">
                                            </iframe>
                                        </div>
                                    @endif

                                @endif
                            </div> --}}
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="close-contract-file-modal" class="btn btn-secondary close-btn" data-dismiss="modal" aria-label="Close" wire:click.prevent="resetInput()" >Đóng</button>
                            <button type="button" id='btn-save' class="btn btn-primary" wire:target="file_upload" wire:loading.class="disabled">Lưu</button>
                            <button type="button" wire:click.prevent="store()" style = "display: none;" id='btn-save2'></button>
                            <button type="button" wire:click.prevent="checkData()" style = "display: none;" id='btn-save3'></button>
                        </div>
                    </div>
                </div>
    </div>
</form>
