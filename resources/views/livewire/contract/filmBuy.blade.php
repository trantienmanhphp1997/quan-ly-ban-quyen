<div>
    <table  class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th class="hidden-480" style="text-align: center; color: black;">STT</th>
                <th style="text-align: center; color: black;">Tên phim</th>
                <th style="text-align: center; color: black;">Tên diễn viên</th>
                <th style="text-align: center; color: black;">Hợp đồng nguồn</th>
                <th style="text-align: center; color: black;">Giá tiền</th>
                <th style="text-align: center; color: black;">Ngày bắt đầu</th>
                <th style="text-align: center; color: black;">Ngày kết thúc</th>
                <th style="text-align: center; color: black;">Hành động</th>
            </tr>
        </thead>
        <div wire:loading class="loader"></div>
        <tbody>
            @foreach ($data as $rs)
                <tr>
                    <td style="text-align: center;">{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</td>
                    <td><a href="{{ route('media.films.edit', ['id'=>$rs->id]) }}" title="Chỉnh sửa phim">{{$rs->vn_name}}</a></td>
                    <td>{{ $rs->actor_name }}</td>
                    <td>{{ $rs->contract_number }}</td>
                    <td style="text-align: right;">{{$rs->total_fee}}</td>
                    <td style="text-align: center;">{{  reFormatDate($rs->start_time, 'd-m-Y') }}</td>
                    <td style="text-align: center;">{{  reFormatDate($rs->end_time, 'd-m-Y') }}</td>
                    <td style="text-align: center;">
                        <button title="Xóa" type='button' wire:click="deleteId({{ $rs->id }})" style="margin-left: 3px;background: #f46a6a;border: none; padding: 2px 9px; margin-top: 2px;" class="btn-delete-film" data-target="#ModalDeleteMedia" data-toggle="modal">
                            <i class="menu-icon fa fa-trash-o" style="color: white;font-size:16px;"></i>
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
        {{$data->links()}}
    <!-- Button trigger modal -->
    <button type="button" id='btn-add-film' class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2">
        + Thêm media
    </button>
    @include('livewire.contract._modalContractFilmBuy')

    <!-- Modal -->

</div>
<script>
    document.addEventListener('livewire:load', function () {
        $(function () {
            // $('#select_box_fim').on('change', (e) => {
            //     @this.emit('nameSelected', $('#select_box_fim').select2('val'));
            // });

            // $('#select_box_fim').val(@this.get('names')).trigger('change');

            $("#btn-save-contract-music").on('click', (e) => {
                @this.emit('storeFilmBuy', {
                    listFilm: $('#select_box_fim').select2('val'), //phim
                });
            });
        });
    });

    Livewire.on('fail-saving-film-buy', (errors) => {
        alert(Object.values(errors).join('\n'));
    });

</script>
