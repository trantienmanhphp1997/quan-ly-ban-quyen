    
<form wire:submit.prevent="submit" autocomplete="off">
    <div wire:init="openModal" wire:ignore.self class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        {!! csrf_field() !!}
        <div class="modal-backdrop fade in" style="height: 100%;"></div>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel" style="font-weight: bold;">
                        Thêm phim
                    </h3>
                    <button type="button" id="close-media-music-modal" class="close" data-dismiss="modal" aria-label="Close" multiple>
                   </button>
               </div>
               <div class="modal-body" id='modal-body-contract-music'>
                      <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-4 col-form-label">Tìm kiếm</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name='search' id="searchTerm" placeholder="Nhập tên phim: từ 2 ký tự" wire:model.lazy="searchTerm">
                        </div>
                    </div>
                    <div class="form-group row" >
                        <label for="inputEmail3" class="col-sm-4 col-form-label">Tên phim (<span class="text-danger">*</span>){{count($arrFilm)}}</label>
                        <div class="col-sm-6">
                            <select id='select_box_fim' class="form-control select_box_fim input-xlarge" multiple>
                                @foreach($arrFilm as $key=> $name)
                                    <option {{($films!=''&&in_array($key,$films))?'selected':''}} value="{{$key}}">{{ $name }}</option>
                                @endforeach
                            </select>
                            @error('listFilm') 
                                @include('layouts.partials.text._error')
                            @enderror 
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button wire:loading.attr="disabled" id='btn-save-contract-music' class="btn btn-primary" type="button" >Lưu</button>
                    <button type="button" id="close-contract-file-modal" class="btn btn-secondary close-btn" data-dismiss="modal" aria-label="Close">Đóng</button>
                </div>
            </div>
        </div>
    </div>
</form>
<div wire:ignore.self class="modal fade" id="ModalDeleteMedia" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-backdrop fade in" style="height: 100%;"></div>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title" id="exampleModalLabel">Xóa media</h2>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true close-btn">×</span>
                </button> -->
            </div>
           <div class="modal-body">
                <p>Bạn có muốn xóa không? Thao tác này không thể phục hồi!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary close-btn" data-dismiss="modal">Không</button>
                <button type="button" wire:click.prevent="delete()" class="btn btn-danger close-modal" data-dismiss="modal">Xóa bỏ</button>
            </div>
        </div>
    </div>
</div>

