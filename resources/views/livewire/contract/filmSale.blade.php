<div>
    <div class="page-header">
        <div class="row">
            <div class="col-sm-12">
                <div class="widget-box">
                    <div class="widget-header">
                        <h4 class="widget-title">Tìm kiếm</h4>
                        <span class="widget-toolbar">
                            <a href="#" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up"></i>
                            </a>
                        </span>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            {!! Form::open(['method'=>'GET','route'=>['contract.viettel-sell.edit',$contract_id],'role'=>'search'])  !!}
                            <div class="row">
                                <div class="col-xs-4 col-sm-4">
                                    <label for="form-field-select-1">Tên phim,tên diễn viên</label>
                                    <input class="form-control hitting-enter" name="vn_name" type="text" placeholder="Nhập tên phim hoặc tên diễn viên" wire:model.debounce.1000ms="searchTerm1"
                                    id='input_vn_name' autocomplete="off" />
                                </div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 d-flex" style="text-align:center;">
                                    @include('livewire.common._buttonReset')
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <table  class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th class="hidden-480" style="text-align: center; color: black;">STT</th>
                <th style="text-align: center; color: black;">Tên phim</th>
                <th style="text-align: center; color: black;">Tên diễn viên</th>
                <th style="text-align: center; color: black;"  width="150px;">Loại doanh thu</th>
                <th style="text-align: center; color: black;">Giá tiền chưa VAT</th>
                <th style="text-align: center; color: black;">Ngày bắt đầu</th>
                <th style="text-align: center; color: black;">Ngày kết thúc</th>
                <th style="text-align: center; color: black;" width="150px;">Độc quyền</th>
                <th style="text-align: center; color: black;" width="150px;">Không độc quyền</th>
                <th style="text-align: center; color: black;" width="150px;">Hạ tầng phát sóng</th>
                <th style="text-align: center; color: black;">Nước phát sóng</th>
                <th style="text-align: center; color: black;">Hành động</th>
            </tr>
        </thead>
        <div wire:loading class="loader"></div>
        <tbody>
            @forelse ($data as $key=> $rs)
                <tr>
                    <td>{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</td>
                    <td><a href="{{ route('media.films.show', ['id'=>$rs->media_id]) }}" title="Xem chi tiết phim">
                    {!! boldTextSearchV2($rs->vn_name,$searchTerm1) !!}</a></td>
                    <td>{!! boldTextSearchV2($rs->actor_name,$searchTerm1) !!}</td>
                    <td>                        
                        @if(!empty($revenueSale))
                        @foreach($revenueSale as $item)
                            @if($item->media_id==$rs->id && $item->sale_money_id == $rs->sale_money_id)
                                <span class="badge badge-secondary">{{$item->name}}</span>
                            @endif
                        @endforeach
                        @endif
                    </td>
                    <td style="text-align: right;">{{ number_format($rs->moneyVAT) }}</td>
                    <td style="text-align: center;">{{ reFormatDate($rs->start_time, 'd-m-Y') }}</td>
                    <td style="text-align: center;">{{ reFormatDate($rs->end_time, 'd-m-Y') }}</td>
                    <td>
                        @if(!empty($type1))
                        @foreach($type1 as $item)
                            @if($item->media_id==$rs->id && $item->sale_money_id == $rs->sale_money_id)<span class="badge badge-secondary">{{$item->code}}</span>@endif
                        @endforeach
                        @endif
                    </td>
                    <td>
                        @if(!empty($type2))
                        @foreach($type2 as $value)
                            @if($value->media_id==$rs->id && $value->sale_money_id == $rs->sale_money_id)<span class="badge badge-secondary">{{$value->code}}</span>@endif
                        @endforeach
                        @endif
                    </td>
                    <td>
                        @if(!empty($type3))
                        @foreach($type3 as $value)
                            @if($value->media_id==$rs->id && $value->sale_money_id == $rs->sale_money_id)<span class="badge badge-secondary">{{$value->code}}</span>@endif
                        @endforeach
                        @endif
                    </td>
                    <td>{{$rs->nuoc}}</td>
                    <td>
                        <center>
                            <button title="Chỉnh sửa" type="button" style="background: #fff; border: 1px solid black;margin-top: 2px; padding: 1px 8px;" class="btn-edit-film btn-info" data-target="#exampleModalEdit" data-toggle="modal" wire:click="editId({{ $rs->sale_money_id }})"> 
                             <i class="ace-icon fa fa-pencil bigger-120"></i>
                            </button>                            
                            <button title="Xóa" type='button' wire:click="deleteId({{ $rs->sale_money_id }})" style="margin-left: 3px;background: #f46a6a;border: none; padding: 2px 9px; margin-top: 2px;" class="btn-delete-film" data-target="#ModalDeleteMedia" data-toggle="modal">
                                <i class="menu-icon fa fa-trash-o" style="color: white;font-size:16px;"></i>
                            </button>
                            <button type="button" wire:click="extensionId({{ $rs->sale_money_id }})" class="btn btn-xs btn-yellow" style="margin-top: 5px;" data-target="#exampleModal" data-toggle="modal">Gia hạn</button>
                        </center>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="12" class="text-center">
                        Không có kết quả phù hợp
                    </td>
                </tr>
            @endforelse
        </tbody>
    </table>
    {{$data->links()}}
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2">
        + Thêm media
    </button>
    @include('livewire.contract._modalContractFilmEdit')
    @include('livewire.contract._modalContractFilmSale')
    @include('livewire.contract._modalContractFilmExtension')
    @include('livewire.common._message')
    <div wire:ignore.self class="modal fade" id="ModalDeleteMedia" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-backdrop fade in" style="height: 100%;"></div>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title" id="exampleModalLabel">Xóa media</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <!-- <span aria-hidden="true close-btn">×</span> -->
                    </button>
                </div>
            <div class="modal-body">
                <p>Bạn có muốn xóa không? Thao tác này không thể phục hồi!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary close-btn" data-dismiss="modal">Không</button>
                <button type="button" wire:click.prevent="delete()" class="btn btn-danger close-modal" data-dismiss="modal">Xóa bỏ</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    document.addEventListener('livewire:load', function () {
        $(function () {
            $('#country').val(@this.get('country')).trigger('change');
            $("#btn_edit_film").on('click', (e) => {
                // var moneyArr = [];
                // var moneyList = $('.money_edit')
                // for(var i = 0; i < moneyList.length; i++){
                //     moneyArr.push(moneyList[i].value)
                // }
                // var moneyVATArr = [];
                // var moneyList = $('.moneyVAT_edit')
                // for(var i = 0; i < moneyList.length; i++){
                //     moneyVATArr.push(moneyList[i].value)
                // }
                // var revenuTypeArr = [];
                // var revenuTypeList = $('.revenuType')
                // for(var i = 0; i < revenuTypeList.length; i++){
                //     revenuTypeArr.push(revenuTypeList[i].value)
                // }
                window.livewire.emit('editFilmSale', {
                    // film: $('#select_box_fim').select2('val'), //phim
                    countLiveEdit: $('#count_live_edit').val(), //Số lần phát sóng
                    role1: $('#role4').select2('val'), //doc quyen
                    role2: $('#role5').select2('val'), //khong doc quyen
                    country: $('#country_edit').val(), //quoc gia được phép kinh doanh
                    role3: $('#role6').select2('val'), //ha tang phat song
                    countryFilm: $('#countryFilmEdit').select2('val'), //Quốc gia được phép phát sóng
                    startTimeEdit: document.getElementById("id-date-picker-1 start_time_edit").value,
                    endTimeEdit: document.getElementById("id-date-picker-1 end_time_edit").value,
                    note: $("#note_nuoc_edit").val(),
                    noteFilm: $("#note_film_edit").val(),
                    moneyEdit: $('#money_edit').val(),
                    moneyVATEdit: $('#moneyVAT_edit').val(),
                    revenuTypeEdit: $('#revenueTypeEdit').val(),
                });
            });
            $("#btn-save-contract-music").on('click', (e) => {
                window.livewire.emit('storeFilmSale', {
                    film: $('#select_box_fim').select2('val'), //phim
                    money: $('#money').val(), //gia
                    moneyVAT: $('#moneyVAT').val(), //gia chưa VAT
                    countLive: $('#count_live').val(), //Số lần phát sóng
                    role1: $('#role1').select2('val'), //doc quyen
                    role2: $('#role2').select2('val'), //khong doc quyen
                    country: $('#country').val(), //quoc gia được phép kinh doanh
                    role3: $('#role3').select2('val'), //ha tang phat song
                    countryFilm: $('#countryFilm').select2('val'), //Quốc gia được phép phát sóng
                    startTime: document.getElementById("id-date-picker-1 start_time").value,
                    endTime: document.getElementById("id-date-picker-1 end_time").value,
                    revenuType: $("#type_id1").val(),
                    note: $("#note_nuoc").val(),
                    noteFilm: $("#note_film").val(),
                });
            });
            $("#btn-save-revenue").on('click', (e) => {
                window.livewire.emit('storeRevenue',
                    $('#media_id').val(),
                    document.getElementById("id-date-picker-1 start_time2").value,
                    $('#money_revenue').val(),
                    $('#type_revenue').val(),
                    document.getElementById("id-date-picker-1 end_time1").value,
                    document.getElementById("id-date-picker-1 start_time1").value,
                );
            });

        });
    });
    $(document).ready(function () {
        $(function () {
            $(".select_role").select2({
                placeholder: 'Chọn quyền...',
                allowClear:true
            });
            $(".select_country").select2({
                placeholder: 'Chọn quốc gia...',
                allowClear:true
            });            
            $(".select_box").select2({
                placeholder: 'Chọn...',
                allowClear:true
            });
            $(".select_role1").select2({
                placeholder: 'Chọn quyền...',
                allowClear:true
            });
            $(".select_box1").select2({
                placeholder: 'Chọn...',
                allowClear:true
            });
            $('.hitting-enter').keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
            // $('#example_elementA').click(function(e){
            //     e.preventDefault()
            //     document.getElementById('button_example').click()
            // })
            // $('#example_elementB').click(function(e){
            //     e.preventDefault()
            //     document.getElementById('button_exampleB').click()
            // })

        });
    });

</script>
