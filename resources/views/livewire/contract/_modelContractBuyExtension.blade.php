    <div>
        <form wire:submit.prevent="submit" autocomplete="off">
            <div wire:ignore.self class="modal fade" id="contract-buy-modal" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalLabel" aria-hidden="true">
                {!! csrf_field() !!}
                <div class="modal-backdrop fade in" style="height: 100%;"></div>
                <div class="modal-dialog" role="document" >
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title form-group" id="exampleModalLabel" style="font-weight: bold;">
                                Gia hạn hợp đồng mua
                            </h3>
                        </div>

                        <div class="modal-body">
                            <div class="row" style="margin-top: 10px">
                                @if(!empty($currentContract))
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label class="col-md-4 col-form-label">Số hợp đồng:</label>
                                            <input type="text" name="contract_number"
                                                value="{{ $currentContract->contract_number }}" id="contract_number"
                                                placeholder="Mã hợp đồng" class="col-md-8" disabled>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label class="col-md-4 col-form-label">Loại Hợp đồng:</label>
                                            {{ Form::select('category_id', ['1' => 'Nhạc', '2' => 'Phim'], $currentContract->category_id, ['class' => 'col-md-8', 'disabled' => true]) }}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label class="col-md-4 col-form-label">Ngày ký:</label>
                                            <div class="input-group col-sm-8">
                                                <input type="text" name="date_sign" class="form-control"
                                                    value="{{ reFormatDate($currentContract->date_sign, 'd-m-Y') }}"
                                                    disabled>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                @if ($currentContract->category_id == 1||$currentContract->category_id == 2)
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label class="col-md-4 col-form-label">Ngày bắt đầu</label>
                                            <div class="input-group col-sm-8">
                                                <input type="text" id="start-time" class="form-control"
                                                    value="{{ reFormatDate($currentContract->start_time, 'd-m-Y') }}"
                                                    disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="col-md-12">
                                                <label class="col-md-4 col-form-label">Ngày kết thúc<span
                                                        class="text-danger">(*)</span></label>
                                                <div class="input-group col-sm-8">
                                                    <input type="text" class="form-control date-picker"
                                                        data-date-format="dd-mm-yyyy" id="end-time"
                                                        placeholder="Thời gian hết hạn"
                                                        data-date = "{{ reFormatDate($currentContract->end_time)}}"
                                                        value="{{ reFormatDate($currentContract->end_time, 'd-m-Y') }}">
                                                    @include('layouts.partials.input._calender')
                                                </div>                                         
                                            </div>
                                            <div class="col-md-12" style="margin-left: 10px;">
                                                @error('endTime') 
                                                    @include('layouts.partials.text._error')
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                </div>

                                @if ($currentContract->category_id == 1)
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label class="col-md-4 col-form-label">Số tiền:</label>
                                                <div class="input-group col-sm-8">
                                                    <input type="number" id="addition-money" class="form-control"
                                                        min="0">
                                                    @error('additionMoney') 
                                                        @include('layouts.partials.text._error')
                                                    @enderror                                                     
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @endif
                            </div>
                        </div>

                        <div class="modal-footer">
                            <div class="col-xs-12 col-sm-12 col-md-12 text-right " style="margin-top: 40px;"
                                wire:ignore>
                                <button wire:loading.attr="disabled" id="btn-save-revenue" class="btn btn-primary" type="button" onclick="updateCurrentContract()">Lưu</button>
                                <button type="button" class="btn btn-secondary" id="close-contract-buy-modal"
                                    data-dismiss="modal">Đóng</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>
