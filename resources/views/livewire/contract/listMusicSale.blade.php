<div>
    <div class="page-header">
        <div class="row">
            <div class="col-sm-12">
                <div class="widget-box">
                    <div class="widget-header">
                        <h4 class="widget-title">Tìm kiếm</h4>
                        <span class="widget-toolbar">
                            <a href="#" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up"></i>
                            </a>
                        </span>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row">
                                <div class="col-xs-4 col-sm-4">
                                    <label for="form-field-select-1">Tên bài hát, tên ca sĩ</label>
                                    <input class="form-control hitting-enter" name="vn_name" type="text" placeholder="Nhập tên bài hát hoặc tên ca sĩ cần tìm kiếm" wire:model.debounce.1000ms="searchTerm1"
                                    id='input_vn_name' autocomplete="off" />
                                </div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 d-flex" style="text-align:center;">
                                    @include('livewire.common._buttonReset')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <table  class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th style="color: black;"><center>STT</center></th>
                <th style="color: black;"><center>Tên bài hát</center></th>
                <th style="color: black;"><center>Tên ca sĩ</center></th>
                <th style="color: black;"><center>Số hợp đồng bán</center></th>
                <th style="color: black;"><center>Mã nhạc chờ</center></th>
                <th style="color: black;"><center>Quyền tác giả</center></th>
                <th style="color: black;"><center>Ngày bắt đầu</center></th>
                <th style="color: black;"><center>Ngày kết thúc</center></th>
                <th style="color: black;"><center>Thời hạn bản quyền</center></th>
                <th style="color: black;"><center>Chờ độc quyền</center></th>
                <th style="color: black;"><center>Chờ tác quyền</center></th>
                <th style="color: black;"><center>Số độc quyền</center></th>
                <th style="color: black;"><center>Số tác quyền</center></th>

            </tr>
        </thead>
        <div wire:loading class="loader"></div>
        <tbody>
        <?php
            $i =0;
        ?>
            @forelse ($data as $rs)
                <tr>                 
                    <td><center>{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</center></td>
                    <td><a href="{{ route('media.music.show', ['music'=>$rs->media_id]) }}" title="Chỉnh sửa nhạc">{!! boldTextSearchV2($rs->name,$searchTerm1) !!}</a></td>
                    <td>{!! boldTextSearchV2($rs->singer,$searchTerm1) !!}</td>
                    <td>{{ $rs->contract_number }}</td>
                    <td>{{ $rs->ma_nhac_cho }}</td>
                    <td>{{ $rs->tac_gia }}</td>
                    <td><center>{{ reFormatDate($rs->start_time, 'd-m-Y') }}</center></td>
                    <td><center>{{ reFormatDate($rs->end_time, 'd-m-Y') }}</center></td>
                    <td><center>{{ reFormatDate($rs->contract_end_time, 'd-m-Y') }}</center></td>
                    <td>{{ ($rs->cho_doc_quyen==1)? "x" : ""}}</td>
                    <td>{{ ($rs->cho_tac_quyen==1)? "x" : ""}}</td>
                    <td>{{ ($rs->so_doc_quyen==1)? "x" : ""}}</td>
                    <td>{{ ($rs->so_tac_quyen==1)? "x" : ""}}</td>             
                </tr>
            @empty 
            <tr>
                <td colspan="13" class="text-center">
                    Không có kết quả phù hợp
                </td>
            </tr>
            @endforelse
        </tbody>
    </table>
    {{$data->links()}}
    @include('livewire.common._message')
</div>
