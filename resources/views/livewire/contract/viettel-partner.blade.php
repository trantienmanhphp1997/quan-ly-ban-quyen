<div>
    <div>
        <div>
            <div class="form-group row">
                <div class="col-lg-3" style="text-align: center;">
                    <button class="btn deadline-3m" type="button" wire:click="query1(3)" style="background: #FBA420 !important; border: 4px solid #FBA420; margin-top: 5px;">
                        Sắp hết hạn trong vòng 3 tháng<br>
                        <span style="font-size: 25px;">{{$threeMonth}}</span>
                    </button>
                </div>
                <div class="col-lg-3" style="text-align: center;">
                    <button class="btn deadline-2m" type="button" wire:click="query1(2)" style="background: #FB8441 !important; border: 4px solid #FB8441; margin-top: 5px;" >
                        Sắp hết hạn trong vòng 2 tháng<br>
                        <span style="font-size: 25px;">{{$twoMonth}}</span>
                    </button>
                </div>
                <div class="col-lg-3" style="text-align: center;">
                    <button class="btn deadline-1m" type="button" wire:click="query1(1)" style="background: #EE5816 !important; border: 4px solid #EE5816;  margin-top: 5px;">
                        Sắp hết hạn trong vòng 1 tháng<br>
                        <span style="font-size: 25px;">{{$oneMonth}}</span>
                    </button>
                </div>
                <div class="col-lg-3" style="text-align: center;">
                    <button class="btn deadline-1w" type="button" wire:click="query1(7)" style="background: #FB1F0E !important; border: 4px solid #FB1F0E;  margin-right: 10px; margin-top: 5px;">
                        Sắp hết hạn trong vòng 1 tuần<br>
                        <span style="font-size: 25px;">{{$oneWeek}}</span>
                    </button>
                </div>
            </div>
        </div>
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="widget-title">Tìm kiếm</h4>
                            <span class="widget-toolbar">
                                <a href="#" data-action="collapse">
                                    <i class="ace-icon fa fa-chevron-up"></i>
                                </a>
                            </span>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                {!! Form::open(['method'=>'GET','route'=>'contract.viettel-buy','role'=>'search'])  !!}
                                <div class="row">
                                    <div class="col-xs-3 col-sm-3">
                                        <label for="form-field-select-1">Số hợp đồng </label>
                                        <input class="form-control" name="vn_name" type="text" placeholder="Số hợp đồng" wire:model.debounce.1000ms="searchTerm"
                                               id='input_vn_name' autocomplete="off" />
                                    </div>
                                    <div class="col-xs-3 col-sm-3">
                                        <!-- #section:plugins/date-time.datepicker -->
                                        <label for="id-date-picker-1">Thời gian</label>
                                        @include('layouts.partials.input._inputDateRanger')
                                    </div>
                                    <!-- <div class="col-xs-3 col-sm-3" >
                                        <label for="form-field-select-1">Thể loại hợp đồng</label>
                                        <br>
                                        <select id='select_box' class="form-control" wire:model="searchCategory" style="min-width:90%;">
                                            <option value="none" selected hidden>
                                            </option>
                                            <option value="3">Đối tác</option>
                                            <option value="4">Tác giả</option>
                                        </select>
                                    </div> -->
                                    <div class="col-xs-3 col-sm-3">
                                        <label for="form-field-select-1">Trạng thái</label>
                                        <select id='select_box' wire:model="searchStatus" style="min-width:90%;" class="form-control">
                                            </option>
                                                <option value="0" selected="selected">Hiệu lực</option>
                                                <option value="1">Hết hạn</option>
                                                <option value="2">Đã xóa</option>
                                                <option value="3">Tất cả</option>
                                        </select>
                                    </div>
                                    <!-- #section:plugins/date-time.datepicker -->
                                   
                                </div>

                                <hr />
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 d-flex" style="text-align:center;">
                                        @include('livewire.common._buttonReset')
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.page-header -->
    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>QUẢN LÝ HỢP ĐỒNG ĐỐI TÁC</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <table id="simple-table" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                                <th class="hidden-480" style="color: black;"><center>STT</center></th>
                                <th style="color: black;"><center>Số hợp đồng</center></th>
                                <th style="color: black;"><center>Ngày ký</center></th>
                                <th style="color: black;"><center>Đối tác</center></th>
                                <!-- <th style="color: black;"><center>Thể loại hợp đồng</center></th> -->
                                <th style="color: black;"><center>Tổng giá trị</center></th>
                                <th style="color: black;"><center>Người phụ trách</center></th>
                                <th style="color: black;"><center>Số điện thoại</center></th>
                                <th style="color: black;"><center>Danh sách bài hát</center></th>
                                <th style="color: black;"><center>Thời gian hết thúc</center></th>
                                <th style="color: black;"><center>Hành động</center></th>
                            </tr>
                        </thead>
                        <div wire:loading class="loader"></div>
                        <tbody>

                        @forelse ($data as $key => $rs)
                            <tr>
                                <td>{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</td>
                                <td>{!! boldTextSearchV2($rs->contract_number,$searchTerm) !!}</td>
                                <td><center>{{ reFormatDate($rs-> date_sign, 'd-m-Y') }}</center></td>
                                <td>{{ $rs-> partners_name }}</td>
                                <!-- <td>{{ ($rs->type==3)?"Hợp đồng đối tác":(($rs->type==4)?"Hợp đồng tác giả":"") }}</td> -->
                                <td style="text-align:right;">{{numberFormat($rs-> total_money) }}</td>
                                <td>{{ $rs-> user_name }}</td>
                                <td>{{ $rs-> user_msisdn }}</td>
                                <td><center><a  href="{{ route('media.music.index') . '?contract_number=' . $rs->contract_number }}" type="button" class="btn btn-xs btn-primary">Chi tiết</a></center></td>
                                <td>{{ reFormatDate($rs-> end_time, 'd-m-Y') }}</td>

                                <td><center>
                                    @if($rs->deleted_at)
                                        <div>
                                        <button data-toggle="tooltip" title="Khôi phục" style="background: #fff; border: 1px solid black;margin-top: 2px;" type="button" class ='btn-buy-music' wire:click="restore({{$rs->id}})">
                                        <i class="fa fa-undo" style="font-size:16px; color: #2199EB;"></i>
                                        </button>
                                        </div>
                                    @else
                                        @php $id = $rs->id; @endphp
                                        @include('layouts.partials.button._edit')
                                        @include('layouts.partials.button._delete')
                                    @endif
                                </center></td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="11" class="text-center">Không có kết quả phù hợp</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
                <!-- /.span -->
                {{ $data->links() }}
                @include('livewire.common._message')

            </div>
            @include('livewire.common._modalConfirmDelete')
            @include('layouts.partials.lib._date_picker')
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</div>

<script>
$('input[name="start_date"]').on('apply.daterangepicker', function(ev, picker) {
            var setDate = (picker.startDate.format('YYYY/MM/DD') + ' - ' + picker.endDate.format('YYYY/MM/DD'));
            $(this).val(setDate);
        
            $('input[name="start_date"]').on('cancel.daterangepicker', function(ev, picker) {
                    $(this).val('');
                });          
        });
    document.addEventListener('livewire:load', function () {  
        $(function () {
            $('input[name="start_date"]').on('apply.daterangepicker', (e)=>{
                window.livewire.emit('searchTime',
                    document.getElementById('id-date-range-picker-1').value,
                );
            });
        });
    });

</script>
<script>
   function getEditURL(id) {
    var url = window.location.href;
    url = url.split('?')[0];
    window.location.href = url + "/" + id +  "/edit";
   }
</script>

