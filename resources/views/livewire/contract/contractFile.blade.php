<div> 
    <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="widget-title">Tìm kiếm</h4>

                            <span class="widget-toolbar">
                            <a href="#" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up"></i>
                            </a>
                        </span>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="row">
                                    <div class="col-xs-3 col-sm-3">
                                        <label for="form-field-select-1">Tên giấy tờ</label>
                                        <input class="form-control" wire:model.debounce.1000ms="searchName" placeholder="Tên giấy tờ" autocomplete="off" />
                                    </div>
                                    <div class="col-xs-3 col-sm-3">
                                        <label for="form-field-select-1">Số hợp đồng, tên sản phẩm</label>
                                        <input class="form-control" wire:model.debounce.1000ms="searchContractNumber" placeholder="Số hợp đồng, tên sản phẩm" autocomplete="off" />
                                    </div>
                                    <!-- <div class="col-xs-4 col-sm-4">
                                        <label for="form-field-select-1">Loại hợp đồng</label>
                                        <select wire:model.debounce.1000ms="searchCategory" class="form-control" id='select_box'  style="min-width:90%;" >
                                            <option value="none" selected hidden>
                                                <option value="0" selected>--Loại hợp đồng--</option>
                                                <option value="1">Hợp đồng nhạc</option>
                                                <option value="2">Hợp đồng phim</option>
                                        </select>
                                    </div> -->
                                    <div class="col-xs-3 col-sm-3">
                                        <label for="form-field-select-1">Loại giấy tờ</label>
                                        <select wire:model.debounce.1000ms="searchCategory" class="form-control" id='select_box'  style="min-width:90%;" >
                                            <!-- <option value="none"> -->
                                                <option value="0" selected>--Tất cả--</option>
                                                <option value="1">Hợp đồng</option>
                                                <option value="2">Nhạc</option>
                                                <option value="3">Phim</option>
                                        </select>
                                    </div>
                                </div><hr>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 d-flex" style="text-align:center;">
                                        @include('livewire.common._buttonReset')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div><!-- /.page-header -->
        <div class="row">
            <div class="col-sm-4">
                <div id="piechart_pub_type">

                </div>

            </div>
            <div class="col-sm-8">
            </div>
            <div class="col-sm-12">
                <div id="main-container"></div>
            </div>
        </div>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Danh sách Giấy tờ</h2>
            </div>
        </div>
    </div>
        <div wire:loading class="loader"></div>
    <table class="table mt-4 table-bordered table-striped table-hover">
    <thead>
            <tr>
                <th scope="col">
                    <center style="color:black;">STT</center>
                </th>
                <th scope="col">
                   <center style='color:black;'>Tên giấy tờ</center>
                </th>
                <th scope="col">
                    <center style='color:black;'>Ngày ký</center>
                </th>
                <th scope="col">
                   <center style='color:black;'>Số hợp đồng</center>
                </th>
                <th scope="col">
                    <center style='color:black;'>Tên sản phẩm</center>
                 </th>
                <th scope="col">
                   <center style='color:black;'>Người tạo</center>
                </th>
                <th scope="col">
                   <center style='color:black;'>Link</center>
                </th>
                <th scope="col">
                   <center style='color:black;'>Loại giấy tờ</center>
                </th>
                <th scope="col">
                    <center style='color:black;'>Ngày bắt đầu bản quyền</center>
                 </th>
                 <th scope="col">
                    <center style='color:black;'>Ngày hết hạn bản quyền</center>
                 </th>

            </tr>
        </thead>
        @forelse($data as $key => $rs)
        {{-- {{dd($data)}} --}}
            <tr>
                <th style="text-align: center;" scope="row">{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</th>
                <td>{!! boldTextSearchV2($rs->name,$searchName) !!}</td>
                <td style="text-align: center;">{{reFormatDate($rs->date_sign, 'd-m-Y')}}</td>
                <td><a href="{{ $rs->contract_id ? (($rs->type==1)?route('contract.viettel-buy.edit', $rs->contract_id):(($rs->type==2)?route('contract.viettel-sell.edit', $rs->contract_id):route('contract.viettel-partner.edit', $rs->contract_id))) : '#'}}"><center>{!! boldTextSearchV2($rs->contract_number,$searchContractNumber) !!}</center></a></td>
                <td>{!! boldTextSearchV2($rs->music_name,$searchContractNumber) !!}{!! boldTextSearchV2($rs->film_name,$searchContractNumber) !!}</td>
                <td>{{$rs->full_name}}</td>
                <td><a style="cursor: pointer;" wire:click='forDownload({{$rs->id}})'>{{$rs->link}}</a></td>
                @if($rs->contract_id)
                    <td>Hợp đồng</td>
                    <td style="text-align: center;">{{($rs->start_time)?reFormatDate($rs->start_time):""}}</td>
                    <td style="text-align: center;">{{($rs->end_time)?reFormatDate($rs->end_time):""}}</td>
                @elseif($rs->media_id!=0 && $rs->categori_id==2)
                    <td>Phim</td>
                    <td style="text-align: center;">{{($rs->film_start_time)?reFormatDate($rs->film_start_time):""}}</td>
                    <td style="text-align: center;">{{($rs->film_end_time)?reFormatDate($rs->film_end_time):""}}</td>
                @else
                    <td>Nhạc</td>
                    <td style="text-align: center;">{{($rs->music_start_time)?reFormatDate($rs->music_start_time):""}}</td>
                    <td style="text-align: center;">{{($rs->music_end_time)?reFormatDate($rs->music_end_time):""}}</td>
                @endif
            </tr>
        @empty
            <tr>
                <td colspan="10" class="text-center">
                    Không có kết quả phù hợp
                </td>
            </tr>
        @endforelse
            </tbody>
    </table>
    @include('livewire.common._message')
    @include('livewire.common._modalConfirmDelete')
    {{$data->links()}}
</div>
    <script type="text/javascript">
        $("document").ready(function() {
            $('#onClickElementA').click(function(e){
                e.preventDefault()
            })
        })
    </script>
