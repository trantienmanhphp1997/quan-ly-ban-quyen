<div>
    <table  class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th style="color: black;"><center>STT</center></th>
                <th style="color: black;"><center>Tên bài hát</center></th>
                <th style="color: black;"><center>Tên ca sĩ</center></th>
                <th style="color: black;"><center>Hợp đồng nguồn</center></th>
                <th style="color: black;"><center>Ngày bắt đầu</center></th>
                <th style="color: black;"><center>Ngày kết thúc</center></th>
                <th style="color: black;"><center>Chờ độc quyền</center></th>
                <th style="color: black;"><center>Chờ tác quyền</center></th>
                <th style="color: black;"><center>Số độc quyền</center></th>
                <th style="color: black;"><center>Số tác quyền</center></th>
                <th style="color: black;"><center>Bán đối tác thứ 3</center></th>
                <th style="color: black;"><center>Giá tiền</center></th>
                <th style="color: black;"><center>Hành động</center></th>
            </tr>
        </thead>
        <div wire:loading class="loader"></div>
        <tbody>
        <?php
            $i = 0;
        ?>
            @foreach ($data as $rs)
                <tr>
                    <td>{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</td>
                    <td><a href="{{ route('media.music.edit', ['music'=>$rs->id]) }}" title="Chỉnh sửa nhạc">{{$rs->name}}</a></td>
                    <td>{{ $rs->singer }}</td>
                    <td>{{ $rs->contract_number }}</td>
                    <td><center>{{reFormatDate($rs->start_time, 'd-m-Y') }}</center></td>
                    <td><center>{{reFormatDate($rs->end_time, 'd-m-Y') }}</center></td>
                    <td>{{ ($rs->cho_docquyen==1)? "x" : ""}}</td>
                    <td>{{ ($rs->cho_tacquyen==1)? "x" : ""}}</td>
                    <td>{{ ($rs->so_docquyen==1)? "x" : ""}}</td>
                    <td>{{ ($rs->so_tacquyen==1)? "x" : ""}}</td>
                    <td>{{ ($rs->sale_permission==1)? "x" : ""}}</td>
                    <td style="text-align:right;">{{ $rs->money }}</td>
                    <td><center>
                        <button title="Xóa" type='button' wire:click="deleteId({{ $rs->id }})" style="margin-left: 3px;background: #f46a6a;border: none; padding: 2px 9px; margin-top: 2px;" class="btn-delete-film" data-target="#ModalDeleteMediaQTG" data-toggle="modal">
                            <i class="menu-icon fa fa-trash-o" style="color: white;font-size:16px;"></i>
                        </button>
                        </center>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{$data->links()}}

    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2QTG">
        + Thêm media
    </button>

    <form wire:submit.prevent="submit" autocomplete="off">
        <div wire:init="openModal" wire:ignore.self class="modal fade" id="exampleModal2QTG" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            {!! csrf_field() !!}
            <div class="modal-backdrop fade in" style="height: 100%;"></div>
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel" style="font-weight: bold;">
                            Thêm bài hát
                        </h3>
                        <button type="button" id="close-media-music-modal2" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body" id='modal-body-contract-music'>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-4 col-form-label">Tìm kiếm bài hát</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name='search' id="searchTerm" placeholder="Nhập tên bài hát: từ 2 ký tự" wire:model.lazy="searchTerm">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-4 col-form-label">Tên bài hát (<span class="text-danger">*</span>){{ count($arrMusic)}}</label>
                            <div class="col-sm-6">
                                <select id='select_box2' class="form-control select_box input-xlarge" multiple>
                                    @foreach($arrMusic as $key=> $name)
                                        <option {{($music!=''&&in_array($key,$music))?'selected':''}} value="{{$key}}">{{ $name }}</option>
                                    @endforeach

                                </select>
                                @error('listMusic') 
                                    @include('layouts.partials.text._error')
                                @enderror 
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button wire:loading.attr="disabled" id='btn-save-contract-music2' class="btn btn-primary" type="button">Lưu</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div wire:ignore.self class="modal fade" id="ModalDeleteMediaQTG" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-backdrop fade in" style="height: 100%;"></div>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title" id="exampleModalLabel">Xóa media</h2>
                </div>
            <div class="modal-body">
                    <p>Bạn có muốn xóa không? Thao tác này không thể phục hồi!</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary close-btn" data-dismiss="modal">Không</button>
                    <button type="button" wire:click.prevent="delete()" class="btn btn-danger close-modal" data-dismiss="modal">Xóa bỏ</button>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    document.addEventListener('livewire:load', function () {
        $(function () {
            // $('#search').on('change', (e) => {
            //     alert(123);
            //     // @this.emit('searchMusic', $('#search').value);
            // });
            // $('#select_box').on('change', (e) => {
            //     @this.emit('nameSelected', $('#select_box').select2('val'));
            // });

            $('#select_box').val(@this.get('names')).trigger('change');
            $("#btn-save-contract-music2").on('click', (e) => {
                @this.emit('storeMusicBuyQTG', {
                    listMusic: $('#select_box2').select2('val'),
                    //money: $('#money').val(),
                    // choDocQuyen: $('#cho_doc_quyen').is(':checked'),
                    // choTacQuyen: $('#cho_tac_quyen').is(':checked'), //doc quyen
                    // soDocQuyen: $('#so_doc_quyen').is(':checked'), //Số lần phát sóng
                    // soTacQuyen: $('#so_tac_quyen').is(':checked'), //doc quyen
                    // sale3rd: $('#sale_3rd').is(':checked'), //Số lần phát sóng
                });
            });
        });
    });
    $(document).ready(function() {
        $('.hitting-enter').keydown(function(event){
            if(event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        });
    })
</script>
