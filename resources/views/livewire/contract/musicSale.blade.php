<div>

    <div class="page-header">
        <div class="row">
            <div class="col-sm-12">
                <div class="widget-box">
                    <div class="widget-header">
                        <h4 class="widget-title">Tìm kiếm</h4>
                        <span class="widget-toolbar">
                            <a href="#" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up"></i>
                            </a>
                        </span>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row">
                                <div class="col-xs-4 col-sm-4">
                                    <label for="form-field-select-1">Tên bài hát, tên ca sĩ</label>
                                    <input class="form-control hitting-enter" name="vn_name" type="text" placeholder="Nhập tên bài hát hoặc tên ca sĩ cần tìm kiếm" wire:model.debounce.1000ms="searchTerm1"
                                    id='input_vn_name' autocomplete="off" />
                                </div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 d-flex" style="text-align:center;">
                                    @include('livewire.common._buttonReset')
                                    <div style="float:right;">
                                        <button type="button" class="btn btn-outline-dark btn-sm" data-target="#exampleModal3" data-toggle="modal" style="background:#FFFFFF !important;color: #7F7F7F !important;border: 1px black solid; padding: 7px; margin-top: 2px; outline: none;">
                                            <span class="menu-icon fa fa-upload" style="font-size: 18px;"></span>
                                            Tải lên
                                        </button>
                                        <button type="button" class="btn btn-outline-dark btn-sm" data-target="#exampleModalMusicSale" data-toggle="modal" style="background:#FFFFFF !important;color: #7F7F7F !important;border: 1px black solid; padding: 7px; margin-top: 2px; outline: none;">
                                            <span class="menu-icon fa fa-download" style="font-size: 18px;"></span>
                                            Excel
                                        </button>
                                        <button type="button" class="btn btn-info btn-sm" data-target="#modal-form-delete-all-film"data-toggle="modal"
                                            id = 'btn-delete-all-film_1' 
                                            style="background: #F46A6A !important; border: 4px solid #F46A6A; margin-top: 2px; outline: none;" {{(count($this->checkMusicSell))?'':'disabled'}}>
                                            <span class="menu-icon fa fa-trash-o" style="font-size: 18px;"></span>
                                            Xóa
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    

    <table  class="table table-bordered table-hover">
        <thead>
            <tr>
                <th><input type="checkbox" wire:model="selectAll" ></th>
                <th class="hidden-480" style="text-align: center; color: black;">STT</th>
                <th style="text-align: center; color: black;">Mã nhạc chờ</th>
                <th style="text-align: center; color: black;">Tên bài hát</th>
                <th style="text-align: center; color: black;">Tên ca sĩ</th>
                <th style="text-align: center; color: black;">Quyền liên quan</th>
                <th style="text-align: center; color: black;">Quyền tác giả</th>
                <th style="text-align: center; color: black;">Thời hạn bản quyền của nhạc bán</th>
                <th style="text-align: center; color: black;">Hành động</th>
            </tr>
        </thead>
        <div wire:loading class="loader"></div>
        <tbody>
        <?php
            $i =0;
        ?>
            @forelse ($data as $rs)
                <tr>
                    <td><input type="checkbox" wire:model="checkMusicSell" value="{{$rs->id}}" wire:loading.attr="disabled"></td>
                    <td style="text-align: center;">{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</td>
                    <td>{{ $rs->ma_nhac_cho }}</td>
                    <td><a href="{{ route('media.music.show', ['music'=>$rs->media_id]) }}" title="Xem chi tiết bài hát">{!! boldTextSearchV2($rs->name,$searchTerm1) !!}</a></td>
                    <td>{!! boldTextSearchV2($rs->singer,$searchTerm1) !!}</td>
                    <td>{{ $rs->contract_number }}</td>
                    <td>{{ $rs->tac_gia }}</td>
                    <td style="text-align: center;">{{ reFormatDate($rs->end_time, 'd-m-Y') }}</td>
                    <td>
                        <center>
                            <button title="Xóa" type='button' wire:click="deleteId({{ $rs->id }})" style="margin-left: 3px;background: #f46a6a;border: none; padding: 2px 9px; margin-top: 2px;" class="btn-delete-film" data-target="#ModalDeleteMedia" data-toggle="modal">
                                <i class="menu-icon fa fa-trash-o" style="color: white;font-size:16px;"></i>
                            </button>
                            <button title="Chỉnh sửa" type="button" style="background: #fff; border: 1px solid black;margin-top: 2px; padding: 1px 8px;" class="btn-edit-film btn-info" data-target="#exampleModalEdit" data-toggle="modal" wire:click="editId({{ $rs->id }})"> 
                             <i class="ace-icon fa fa-pencil bigger-120"></i>
                            </button>  
                        </center>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="9" class="text-center">
                        Không có kết quả phù hợp
                    </td>
                </tr>
            @endforelse
        </tbody>
    </table>
    {{$data->links()}}
    @include('livewire.common._message')
    @include('livewire.contract._modalContractMusicEdit')
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2">
        + Thêm media
    </button>
    @include('livewire.contract._modalContractMusicSale')

    <!-- Modal -->
   <div class="modal fade" id="modal-form-delete-all-film" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" wire:ignore>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title" id="exampleModalLabel">Xóa hết các nhạc đã chọn</h2>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <p style="margin-left: 12px;">Bạn có muốn xóa hết các bài nhạc đã chọn không? Thao tác này không thể phục hồi</p>
                    </div>
                </div>
                <div class="modal-footer" style="text-align: center;">
                    <div style="float: right;">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Quay lại</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" wire:click="deleteAll()">Đồng ý</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-download-error" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" wire:ignore>
        <div class="modal-backdrop fade in" style="height: 100%;"></div>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title" id="exampleModalLabel">Tải xuống danh sách lỗi</h2>
                </div>
                <form method="POST" name='form-get-error-upload' action="{{route('media.music.getError')}}">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <input id='error-upload-music' type="text" name="error-upload" value="{{ Session::get('outputError') }}" style="display: none;" >
                        </div>
                    </div>
                    <div class="modal-footer" style="text-align: center;">
                        <div style="float: right;">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
                            <button type="button" class="btn btn-primary" id='btn-submit-get-error' data-dismiss="modal">Tải file lỗi</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

   <div class="modal fade" id="exampleModal3" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" wire:ignore>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title" id="exampleModalLabel">Thêm danh sách nhạc bán</h2>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <form action="{{ route('contract.viettel-sell.save',['id'=>$this->contract_id]) }}" method="POST" enctype="multipart/form-data" id='form-upload-file-contract-music'>
                            {{csrf_field()}}
                            <input type="file" id="file-upload-contract-music" name='file' accept=".xlsx,.xls" style="margin-left: 15px;">
                        </form>
                    </div>
                </div>
                <div class="modal-footer" style="text-align: center;">
                    <form>
                        {{csrf_field()}}
                        <a href="#" style="float: left;" id='example_elementB'>File excel mẫu &nbsp;</a>
                        <button style="display:none;" id='button_exampleB' type="button" wire:click="example" class="btn btn-danger" data-dismiss="modal">Đồng ý</button>
                    </form>
                    <div style="float: left;">
                        <p class="text-danger" id='text-error-list-film'></p>
                    </div>
                    <div style="float: right;">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                        <button type="button" class="btn btn-primary" id='btn-save-contract-music-sale'>Lưu</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div wire:ignore.self class="modal fade" id="exampleModalMusicSale" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Tải danh sách bài hát bán</h2>
                </div>
                <div class="modal-body">
                    Bạn có chắc chắn muốn xuất file không?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Quay lại</button>
                    <button type="button" wire:click="export" data-dismiss="modal" class="btn btn-danger" id='btn-upload-music'>Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    document.addEventListener('livewire:load', function () {
        $(function () {
            // $('#select_box').on('change', (e) => {
            //     @this.emit('nameSelected', $('#select_box').select2('val'));
            // });
            $('#btn-submit-get-error').on('click', (e) => {
                var data = $('#error-upload-music').val()
                window.livewire.emit('getError', data);
            })
            $('#select_box').val(@this.get('names')).trigger('change');
            $("#btn-save-contract-music").on('click', (e) => {
                @this.emit('storeMusicSale', {
                    listMusic: $('#select_box').select2('val'),
                    money: $('#money').val(),
                    choDocQuyen: $('#cho_doc_quyen').is(':checked'),
                    choTacQuyen: $('#cho_tac_quyen').is(':checked'), //doc quyen
                    soDocQuyen: $('#so_doc_quyen').is(':checked'), //Số lần phát sóng
                    soTacQuyen: $('#so_tac_quyen').is(':checked'), //doc quyen
                    sale3rd: $('#sale_3rd').is(':checked'), //Số lần phát sóng
                    startTime: document.getElementById("id-date-picker-1 start_time").value,
                    endTime: document.getElementById("id-date-picker-1 end_time").value,
                    maNhacCho: $('#ma_nhac_cho').val(),
                    nameKhongDau: $('#name_khong_dau').val(),
                    singerKhongDau: $('#singer_khong_dau').val(),
                });
            });
            $("#btn-edit-contract-music").on('click', (e) => {
                @this.emit('editMusicSale', {
                    choDocQuyen: $('#cho_doc_quyen_edit').is(':checked'),
                    choTacQuyen: $('#cho_tac_quyen_edit').is(':checked'), //doc quyen
                    soDocQuyen: $('#so_doc_quyen_edit').is(':checked'), //Số lần phát sóng
                    soTacQuyen: $('#so_tac_quyen_edit').is(':checked'), //doc quyen
                    startTime: document.getElementById("id-date-picker-1 start_time_edit").value,
                    endTime: document.getElementById("id-date-picker-1 end_time_edit").value,
                    maNhacCho: $('#ma_nhac_cho_edit').val(),
                    nameKhongDau: $('#name_khong_dau_edit').val(),
                    singerKhongDau: $('#singer_khong_dau_edit').val(),

                });
            });
        });
    });
    document.addEventListener('DOMContentLoaded', function(){
        $('#btn-save-contract-music-sale').click(function(){
            var fileListFilm = $('#file-upload-contract-music').val()
            if(fileListFilm==''||fileListFilm==null){
                $('#text-error-list-film').text('*Chưa có file');
            }
            else {
                $(this).attr('disabled', true)
                var formCreateFilm = document.forms['form-upload-file-contract-music']
                formCreateFilm.submit()
            }
        })
        $('#btn-upload').click(function(){
            $('#text-error-list-film').text('');
        })
        $('#example_elementB').click(function(e){
            e.preventDefault()
            document.getElementById('button_exampleB').click()
        })
        $('.hitting-enter').keydown(function(event){
            if(event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        });    
        $('#btn-download-error').click()   
    })
    Livewire.on('fail-saving-music-sell', (errors) => {
        alert(Object.values(errors).join('\n'));
    });

</script>
