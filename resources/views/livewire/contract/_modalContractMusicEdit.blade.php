<form wire:submit.prevent="submit" autocomplete="off">
    <div wire:init="openModal" wire:ignore.self class="modal fade" id="exampleModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        {!! csrf_field() !!}
        <div class="modal-backdrop fade in" style="height: 100%;"></div>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel" style="font-weight: bold;">
                        Chỉnh sửa bài hát
                    </h3>
                    <button type="button" id="close-media-music-modal-edit" class="close" data-dismiss="modal" aria-label="Close">
                   </button>
               </div>
               <div class="modal-body" id='modal-body-contract-music'>
                    @if($contractMediaSaleEdit)
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-4 col-form-label">Tên bài hát</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" placeholder="Tên bài hát" value='{{$contractMediaSaleEdit->name}}' readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-4 col-form-label">Tên bài hát không dấu</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name='name_khong_dau_edit' id="name_khong_dau_edit" placeholder="Tên bài hát không dấu" value='{{$contractMediaSaleEdit->name_khong_dau}}'>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-4 col-form-label">Ca sĩ thể hiện không dấu</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name='singer_khong_dau_edit' id="singer_khong_dau_edit" placeholder="Ca sĩ thể hiện không dấu" value='{{$contractMediaSaleEdit->singer_khong_dau}}'>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-4 col-form-label">Mã nhạc chờ</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name='ma_nhac_cho' id="ma_nhac_cho_edit" placeholder="Mã nhạc chờ" value='{{$contractMediaSaleEdit->ma_nhac_cho}}'>
                        </div>
                    </div>
                   <div class="form-group row">
                       <label for="inputEmail3" class="col-sm-4 col-form-label">Thời gian bắt đầu</label>
                       <div class="col-sm-6">
                           <div class="input-group">
                               <input type="text" class="form-control date-picker" id="id-date-picker-1 start_time_edit"
                                      data-date-format="dd-mm-yyyy" name="start_time"  placeholder="Thời gian bắt đầu" value="{{ReFormatDate($contractMediaSaleEdit->start_time)}}">
                               @include('layouts.partials.input._calender')
                           </div>
                           <div>
                                @error('startTime')
                                   @include('layouts.partials.text._error')
                                @enderror
                           </div>
                       </div>
                   </div>
                   <div class="form-group row">
                       <label for="inputEmail3" class="col-sm-4 col-form-label">Thời gian kết thúc</label>
                       <div class="col-sm-6">
                           <div class="input-group">
                               <input type="text" class="form-control date-picker" id="id-date-picker-1 end_time_edit" name='end_time' data-date-format="dd-mm-yyyy"  placeholder="Thời gian kết thúc" value="{{ReFormatDate($contractMediaSaleEdit->end_time)}}">
                               @include('layouts.partials.input._calender')
                           </div>
                           <div>
                                @error('endTime')
                                    @include('layouts.partials.text._error')
                                @enderror
                           </div>
                       </div>
                   </div>

                    <div class="form-group row">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Nhạc chờ độc quyền</label>
                        <div class="col-sm-4">
                              <input type="checkbox"  id="cho_doc_quyen_edit" {{$contractMediaSaleEdit->cho_doc_quyen?'checked':''}}>
                        </div>
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Nhạc chờ tác quyền</label>
                        <div class="col-sm-4">
                              <input type="checkbox"  id="cho_tac_quyen_edit" {{$contractMediaSaleEdit->cho_tac_quyen?'checked':''}}>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Nhạc số độc quyền</label>
                        <div class="col-sm-4">
                            <input type="checkbox"  id="so_doc_quyen_edit" {{$contractMediaSaleEdit->so_doc_quyen?'checked':''}}>
                        </div>
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Nhạc số tác quyền</label>
                        <div class="col-sm-4">
                            <input type="checkbox"  id="so_tac_quyen_edit" {{$contractMediaSaleEdit->so_tac_quyen?'checked':''}}>
                        </div>
                    </div>
                    <!-- <div class="form-group row" style="display: none;">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-8">Quyền bán đối tác thứ 3</label>
                        <div class="col-sm-4">
                            <input type="checkbox" id="sale_3rd_eidt" value="sale_3rd">
                        </div>
                    </div> -->
                    @endif
                </div>

                <div class="modal-footer">
                    <button wire:loading.attr="disabled" id='btn-edit-contract-music' class="btn btn-primary" type="button">Lưu</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>
</form>