<div>
    <table  class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th style="color: black;"><center>STT</center></th>
                <th style="color: black;"><center>Tên bài hát</center></th>
                <th style="color: black;"><center>Tên ca sĩ</center></th>
                <th style="color: black;"><center>Hợp đồng nguồn</center></th>
                <th style="color: black;"><center>Ngày bắt đầu</center></th>
                <th style="color: black;"><center>Ngày kết thúc</center></th>
                <th style="color: black;"><center>Chờ độc quyền</center></th>
                <th style="color: black;"><center>Chờ tác quyền</center></th>
                <th style="color: black;"><center>Số độc quyền</center></th>
                <th style="color: black;"><center>Số tác quyền</center></th>
                <th style="color: black;"><center>Bán đối tác thứ 3</center></th>
                <th style="color: black;"><center>Giá tiền</center></th>
                <th style="color: black;"><center>Hành động</center></th>
            </tr>
        </thead>
        <div wire:loading class="loader"></div>
        <tbody>
        <?php
            $i = 0;
        ?>
            @foreach ($data as $rs)
                <tr>
                    <td>{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</td>
                    <td><a href="{{ route('media.music.edit', ['music'=>$rs->id]) }}" title="Chỉnh sửa nhạc">{{$rs->name}}</a></td>
                    <td>{{ $rs->singer }}</td>
                    <td>{{ $rs->contract_number }}</td>
                    <td><center>{{reFormatDate($rs->start_time, 'd-m-Y') }}</center></td>
                    <td><center>{{reFormatDate($rs->end_time, 'd-m-Y') }}</center></td>
                    <td>{{ ($rs->cho_docquyen==1)? "x" : ""}}</td>
                    <td>{{ ($rs->cho_tacquyen==1)? "x" : ""}}</td>
                    <td>{{ ($rs->so_docquyen==1)? "x" : ""}}</td>
                    <td>{{ ($rs->so_tacquyen==1)? "x" : ""}}</td>
                    <td>{{ ($rs->sale_permission==1)? "x" : ""}}</td>
                    <td style="text-align:right;">{{ $rs->money }}</td>
                    <td><center>
                        <button title="Xóa" type='button' wire:click="deleteId({{ $rs->id }})" style="margin-left: 3px;background: #f46a6a;border: none; padding: 2px 9px; margin-top: 2px;" class="btn-delete-film" data-target="#ModalDeleteMedia" data-toggle="modal">
                            <i class="menu-icon fa fa-trash-o" style="color: white;font-size:16px;"></i>
                        </button>
                        </center>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
                {{$data->links()}}

    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2">
        + Thêm media
    </button>
    @include('livewire.contract._modalContractMusicBuy')

    <!-- Modal -->

</div>
<script>
    document.addEventListener('livewire:load', function () {
        $(function () {
            // $('#search').on('change', (e) => {
            //     alert(123);
            //     // @this.emit('searchMusic', $('#search').value);
            // });
            // $('#select_box').on('change', (e) => {
            //     @this.emit('nameSelected', $('#select_box').select2('val'));
            // });

            $('#select_box').val(@this.get('names')).trigger('change');
            $("#btn-save-contract-music").on('click', (e) => {
                @this.emit('storeMusicBuy', {
                    listMusic: $('#select_box').select2('val'),
                    //money: $('#money').val(),
                    // choDocQuyen: $('#cho_doc_quyen').is(':checked'),
                    // choTacQuyen: $('#cho_tac_quyen').is(':checked'), //doc quyen
                    // soDocQuyen: $('#so_doc_quyen').is(':checked'), //Số lần phát sóng
                    // soTacQuyen: $('#so_tac_quyen').is(':checked'), //doc quyen
                    // sale3rd: $('#sale_3rd').is(':checked'), //Số lần phát sóng
                });
            });
        });
    });
    $(document).ready(function() {
        $('.hitting-enter').keydown(function(event){
            if(event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        });
    })
    Livewire.on('fail-saving-music-buy', (errors) => {
        alert(Object.values(errors).join('\n'));
    });
</script>
