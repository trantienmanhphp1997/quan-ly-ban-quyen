<div>
<form wire:submit.prevent="submit" autocomplete="off">
    <div wire:init="openModal" wire:ignore.self class="modal fade" id="exampleModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        {!! csrf_field() !!}
        <div class="modal-backdrop fade in" style="height: 150%;"></div>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" style="font-weight: bold;">
                        Chỉnh sửa bán phim
                    </h3>
               </div>
               <div class="modal-body">
                    @if($revenue_edit)
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-4 col-form-label">Tên phim (<span class="text-danger">*</span>)</label>
                        <div class="col-sm-6">
                            <input type="text" class ='form-control' value="{{$revenue_edit->vn_name}}" disabled="true">
                        </div>
                    </div>
                    {{-- @foreach($RevenueRevenuType as $value) --}}
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-4 col-form-label">Loại doanh thu</label>
                        <div class="col-sm-6">
                            {!!Form::select('revenuType', $RevenuType,$revenue2?$revenue2->id:null,array('class' => 'form-control revenuType', 'id'=>'revenueTypeEdit') )!!}
                        </div>
                    </div>
                   <div class="form-group row">
                       <label for="inputEmail3" class="col-sm-4 col-form-label">Giá tiền chưa VAT</label>
                       <div class="col-sm-6">
                            <input type="number" min="0"  id='moneyVAT_edit' class="form-control moneyVAT_edit" name="money"  value = "{{$revenue_edit->moneyVAT}}" placeholder="Giá tiền chưa VAT" oninput="this.value = Math.abs(this.value)">
                       </div>
                   </div>
                   <div class="form-group row">
                       <label for="inputEmail3" class="col-sm-4 col-form-label">Giá tiền đã VAT</label>
                       <div class="col-sm-6">
                            <input type="number" min="0"  id='money_edit' class="form-control money_edit" name="money"  value = "{{$revenue_edit->money}}" placeholder="Giá tiền đã VAT" oninput="this.value = Math.abs(this.value)">
                       </div>
                   </div>
                    {{-- @endforeach --}}
                    @error('revenuTypeEdit') 
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-4 col-form-label"></label>
                            <div class="col-sm-6">
                                @include('layouts.partials.text._error')
                            </div>
                        </div>
                    @enderror 
                    <div class="form-group row">
                       <label for="inputEmail3" class="col-sm-4 col-form-label">Số lần phát sóng</label>
                       <div class="col-sm-6">
                            <input type="text" class="form-control" id="count_live_edit" name="count_live" placeholder="Số lần phát sóng" value = "{{$revenue_edit->count_live}}">
                            @error('countLiveEdit') 
                                @include('layouts.partials.text._error')
                            @enderror 
                       </div>
                   </div>
                   <div class="form-group row">
                       <label for="inputEmail3" class="col-sm-4 col-form-label">ĐỘC QUYỀN</label>
                       <div class="col-sm-6">

                           <select name="role1[]" class="select_role input-xlarge" id="role4" multiple >
                               @foreach($data_type1 as $value)
                                    <option value="{{$value['id']}}"
                                        @if($roleEditSave1!='')
                                            @if(in_array($value['id'],$roleEditSave1))
                                                selected="true"
                                            @endif
                                        @else
                                            @foreach($roleEdit1 as $valueType)
                                                @if($valueType->role_id==$value['id'])
                                                    selected="true"
                                                @endif
                                            @endforeach
                                        @endif
                                    >
                                        {!! $value['text'].$value['name'] !!}
                                    </option>
                               @endforeach
                           </select>
                       </div>
                   </div>
                   <div class="form-group row">
                       <label for="inputEmail3" class="col-sm-4 col-form-label">KHÔNG ĐỘC QUYỀN</label>
                       <div class="col-sm-6">
                           <select name="role2[]" class="select_role input-xlarge" id="role5" multiple>
                               @foreach($data_type2 as $value)
                                    <option value="{{$value['id']}}"
                                        @if($roleEditSave2!='')
                                            @if(in_array($value['id'],$roleEditSave2))
                                                selected="true"
                                            @endif
                                        @else
                                            @foreach($roleEdit2 as $valueType)
                                                @if($valueType->role_id==$value['id'])
                                                    selected="true"
                                                @endif
                                            @endforeach
                                        @endif
                                    >
                                        {!! $value['text'].$value['name'] !!}
                                    </option>
                               @endforeach
                           </select>
                       </div>
                   </div>
                   <div class="form-group row">
                       <label for="inputEmail3" class="col-sm-4 col-form-label">Quốc gia được phép phát sóng</label>
                       <div class="col-sm-6">
                           <select name="role2[]" class="select_country input-xlarge" id="countryFilmEdit" multiple>
                               @foreach($filmCountry as $value)
                                    <option value="{{$value->id}}"
                                        @if($countryFilmSave!='')
                                            @if(in_array($value->id,$countryFilmSave))
                                                selected="true"
                                            @endif
                                        @else
                                            @foreach($filmSaleCountry as $valueSale)
                                                @if($valueSale->country_id==$value->id)
                                                    selected="true"
                                                @endif
                                            @endforeach
                                        @endif
                                    >
                                       {{ $value->name }}
                                   </option>
                               @endforeach
                           </select>
                       </div>
                   </div>
                   <div class="form-group row">
                       <label for="inputEmail3" class="col-sm-4 col-form-label">Nước phát sóng</label>
                       <div class="col-sm-6">
                           <input type="text" name="nuoc" id="country_edit" placeholder="Nước phát sóng"  class="form-control" value="{{$revenue_edit->nuoc}}">
                       </div>
                   </div>
                   <div class="form-group row">
                       <label for="inputEmail3" class="col-sm-4 col-form-label">Ghi chú nước phát sóng</label>
                       <div class="col-sm-6">
                           <textarea style='padding: 5px 4px 6px' name="note" id="note_nuoc_edit" rows="3" placeholder="Ghi chú nước phát sóng"  class="form-control" >{{$revenue_edit->note}}</textarea>
                       </div>
                   </div>
                    <div class="form-group row">
                       <label for="inputEmail3" class="col-sm-4 col-form-label">Ghi chú phim</label>
                       <div class="col-sm-6">
                           <textarea style='padding: 5px 4px 6px' name="note" id="note_film_edit" rows="3" placeholder="Ghi chú phim"  class="form-control" >{{$revenue_edit->note_film}}</textarea>
                       </div>
                   </div>
                    <div class="form-group row">
                       <label for="inputEmail3" class="col-sm-4 col-form-label">Hạ tầng phát sóng</label>
                       <div class="col-sm-6">
                           <select name="role3[]" class="select_role2 input-xlarge" id="role6" multiple>                        
                                @foreach($role_level as $key=> $item)
                                    <option value="{{$key}}"
                                        @if($roleEditSave3!='')
                                            @if(in_array($key,$roleEditSave3))
                                                selected="true"
                                            @endif
                                        @else
                                            @foreach($roleEdit3 as $valueType)
                                                @if($valueType->role_id==$key)
                                                    selected="true"
                                                @endif
                                            @endforeach
                                        @endif
                                    >
                                        {{$item}}
                                    </option>
                                @endforeach
                           </select>
                       </div>
                   </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-4 col-form-label">Thời gian bắt đầu</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                            <input type="text" autocomplete="off" class="form-control date-picker" id="id-date-picker-1 start_time_edit"
                                   data-date-format="dd-mm-yyyy" name="start_time" value = "{{reFormatDate($revenue_edit->start_time)}}"  placeholder="Thời gian bắt đầu">
                            @include('layouts.partials.input._calender')
                            </div>
                            <div>
                                @error('startTimeEdit') 
                                    @include('layouts.partials.text._error')
                                @enderror 
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-4 col-form-label">Thời gian kết thúc</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                            <input type="text" class="form-control date-picker" id="id-date-picker-1 end_time_edit" name='end_time'
                                   data-date-format="dd-mm-yyyy" id="end_time" value = "{{reFormatDate($revenue_edit->end_time)}}" placeholder="Thời gian kết thúc">
                            @include('layouts.partials.input._calender')
                            </div>
                           <div>
                                @error('endTimeEdit1') 
                                    @include('layouts.partials.text._error')
                                @enderror 
                           </div>
                        </div>
                    </div>
                </div>
                @endif
                <div class="modal-footer">
                    <button wire:loading.attr="disabled" id='btn_edit_film' class="btn btn-primary" type="button">Lưu</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id='close-modal_edit'>Đóng</button>
                </div>
            </div>
        </div>
    </div>
</form>
</div>

