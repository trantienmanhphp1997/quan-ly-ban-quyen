<div>

<form wire:submit.prevent="submit"  autocomplete="off">
    <div  wire:ignore.self class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        {!! csrf_field() !!}
        <div class="modal-backdrop fade in" style="height: 100%;"></div>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel" style="font-weight: bold;">
                        @if (!empty($data_revenue) && $data_revenue->category_id == 1)
                            Gia hạn hợp đồng bán nhạc
                        @else
                            Gia hạn hợp đồng bán phim
                        @endif
                    </h3>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label class="col-md-4 col-form-label">Số hợp đồng:</label>
                                    <input type="text" name="contract_number" value="{{!empty($data_revenue)?$data_revenue->contract_number:''}}" id="contract_number" placeholder="mã hợp đồng" class="col-md-8" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group row">
                                @if(empty($data_revenue->type))
                                <div class="col-md-6">
                                    <label class="col-md-4 col-form-label">Tên phim:</label>
                                    <input type="text" name="name" value="{{!empty($data_revenue)?$data_revenue->media_id:''}}" id="vn_name" placeholder="mã sản phẩm media" class="col-md-8" disabled>
                                    <input type="hidden" name="media_id" value="{{!empty($data_revenue)?$data_revenue->media_id:''}}" id="media_id">
                                </div>
                                @endif
                                <div class="col-md-6">
                                    <label class="col-md-4 col-form-label">Loại hợp đồng:</label>
                                    <select name="category_id" id="category_id" class="col-md-8" disabled>
                                        @foreach($categories as $key=> $category)
                                        <option value="{{$key}}"{{($key==(!empty($data_revenue)?$data_revenue->category_id:''))?'selected':''}} >{{$category}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div>
                        @if(!empty($data_revenue)&&($data_revenue->category_id==1||$data_revenue->contract_id!=''))
                        <div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group row">
                                    <div class="col-md-6" >
                                        <div class="col-md-12">
                                            <label class="col-md-4 col-form-label" style="padding: 0px;">Ngày ký<span class="text-danger">(*)</span></label>
                                            <div class="input-group col-sm-8" style="padding: 0px;">
                                                <input type="text" class="form-control date-picker" id="id-date-picker-1 date_sign" name='date_sign'
                                                    data-date-format="dd-mm-yyyy" id="date_sign"  placeholder="Ngày ký" value="{{reFormatDate($data_revenue->date_sign)}}">
                                                @include('layouts.partials.input._calender')
                                            </div>                                        
                                        </div>
                                        <div class="col-md-12">
                                            @error('date_sign') 
                                                @include('layouts.partials.text._error')
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group row">
                                    <div class="col-md-6"  >
                                        <label class="col-md-4 col-form-label">Số tiền:</label>
                                        {!! Form::number('money_revenue', null, array('class' => 'col-md-8','id'=>'money_revenue','placeholder'=> "Nhập số tiền")) !!}
                                        <div class="col-md-12">
                                            @error('money_revenue') 
                                                @include('layouts.partials.text._error')
                                            @enderror 
                                        </div>
                                    </div>
                                    <div class="col-md-6" >
                                        <label class="col-md-4 col-form-label">Doanh thu từ:</label>
                                        {!! Form::select('type_revenue', [0=>'Ký hợp đồng', 1=>'Gia hạn'],1, array('class' => 'col-md-8','readonly'=>'readonly','id'=>'type_revenue')) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        </div>
                        @if(!empty($data_revenue))
                        <div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label class="col-md-4 col-form-label">Ngày bắt đầu</label>
                                        <div class="input-group col-sm-8">
                                            <input type="text" id="start-time" class="form-control"
                                                value="{{!empty($data_revenue)?reFormatDate($data_revenue->start_time,'d-m-Y'):''}}" placeholder="Ngày bắt đầu bản quyền" 
                                                disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="col-md-12">
                                            <label class="col-md-4 col-form-label" style="padding: 0px;" >Ngày hết hạn</label>
                                            <div class="input-group col-sm-8">
                                                <input type="text" class="form-control date-picker" id="id-date-picker-1 end_time" name='end_date1'
                                                    data-date-format="dd-mm-yyyy" id="end_date" value="{{!empty($data_revenue)?reFormatDate($data_revenue->end_time,'d-m-Y'):''}}"  placeholder="Thời gian hết hạn">

                                                @include('layouts.partials.input._calender')
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            @error('end_time') 
                                            @include('layouts.partials.text._error')
                                            @enderror                                       
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="col-xs-12 col-sm-12 col-md-12 text-right " style="margin-top: 40px;" wire:ignore>
                            <button wire:loading.attr="disabled" id="btn-save-revenue" class="btn btn-primary" type="button">Lưu</button>
                            <button type="button" class="btn btn-secondary" id="close-media-film-modal" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
</div>
