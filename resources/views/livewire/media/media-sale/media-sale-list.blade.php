<div>
    <div class="page-header">
        <div class="row">
            <div class="col-sm-12">
                <div class="widget-box">
                    <div class="widget-header">
                        <h4 class="widget-title">Tìm kiếm</h4>
                        <span class="widget-toolbar">
                            <a href="#" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up"></i>
                            </a>
                        </span>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row">
                                <div class="col-xs-3 col-sm-3">
                                    <label for="form-field-select-1">Tên hợp đồng</label>
                                    <input class="form-control" type="text" placeholder="Tên hợp đồng" wire:model.debounce.1000ms="searchTerm" autocomplete="off" />
                                </div>
                                <div class="col-xs-3 col-sm-3">
                                    <label for="form-field-select-1">Tên sản phẩm</label>
                                    <input class="form-control" type="text" placeholder="Tên sản phẩm" wire:model.debounce.1000ms="searchName" autocomplete="off" />
                                </div>
                                <div class="col-xs-3 col-sm-3" >
                                    <label for="form-field-select-1">Thể loại</label>
                                    <br>
                                    <select class="form-control"  id='select_box' wire:model="searchCategory" style="min-width:90%;" {{$disabledCategory?'disabled':''}}>
                                        <option value="none" {{$disabledCategory?"":"selected"}} hidden>
                                        </option>
                                        @foreach($category as $item)
                                            <option value="{{$item->id}}" name="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <hr />

                          <div class="row">
                                <div class="col-xs-12 col-sm-12 d-flex" style="text-align:center;">
                                    @include('livewire.common._buttonReset')
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div wire:loading class="loader"></div>
    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <div class="row">
                <div class="col-xs-12">
                    <table id="simple-table" class="table table-striped table-bordered table-hover">
                        <h2>Danh sách sản phẩm bán</h2>
                        <hr>
                        <thead>
                            <tr>
                                <th class="hidden-480" style="text-align: center; color: black;">STT</th>
                                <th style="text-align: center; color: black;">Tên hợp đồng</th>
                                <th style="text-align: center; color: black;">Tên sản phẩm</th>
                                <th style="text-align: center; color: black;">Loại sản phẩm</th>
                                <th style="text-align: center; color: black;">Giá tiền</th>
                                <th style="text-align: center; color: black;">Ngày bắt đầu</th>
                                <th style="text-align: center; color: black;">Ngày kết thúc</th>
                            </tr>
                        </thead>

                        <tbody>
                            @forelse ($data as $key => $rs)
                            <tr>
                                <td style="text-align: center;">{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</td>
                                <td><a href="{{route('contract.viettel-sell.edit', ['id' => $rs->data_contract_id])}}">{!! boldTextSearchV2($rs->contract_number,$searchTerm) !!}</a></td>
                                <td>{!! boldTextSearchV2($rs->film_name,$searchName) !!}{!! boldTextSearchV2($rs->music_name,$searchName) !!}</td>
                                <td>{{ $rs->category }}</td>
                                <td style="text-align: right;">{{ isset($rs->revenue->moneyVAT)?numberFormat($rs->revenue->moneyVAT):0 }}</td>
                                <td style="text-align: center;">{{ reFormatDate($rs->data_start_time) }}</td>
                                <td style="text-align: center;">{{ reFormatDate($rs->data_end_time) }}</td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="11" class="text-center">Không có kết quả phù hợp</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                    {{ $data->links() }}
                </div><!-- /.span -->
            </div><!-- /.row -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</div>
