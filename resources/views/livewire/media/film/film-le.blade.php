<div>
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="widget-title">Tìm kiếm</h4>
                            <span class="widget-toolbar">
                     <a href="#" data-action="collapse">
                     <i class="ace-icon fa fa-chevron-up"></i>
                     </a>
                     </span>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="row">
                                    <div class="col-xs-3 col-sm-3">
                                        <label for="form-field-select-1">Tên phim gốc</label>
                                        <input class="form-control" name="vn_name" type="text" placeholder="Nhập tên phim gốc" wire:model.debounce.1000ms="searchName"
                                               id='input_vn_name' autocomplete="off" />
                                    </div>
                                    <div class="col-xs-3 col-sm-3">
                                        <label for="form-field-select-1">Tên tiếng việt</label>
                                        <input class="form-control" name="vn_name" type="text" placeholder="Nhập tên tiếng việt" wire:model.debounce.1000ms="searchVNName"
                                               id='input_vn_name' autocomplete="off" />
                                    </div>
                                    <div class="col-xs-3 col-sm-3">
                                        <label for="form-field-select-1">Tên diễn viên</label>
                                        <input class="form-control" name="vn_name" type="text" placeholder="Nhập tên diễn viên" wire:model.debounce.1000ms="searchActor"
                                               id='input_vn_name' autocomplete="off" />
                                    </div>
                                    <div class="col-xs-3 col-sm-3">
                                        <label for="form-field-select-1">Tên phim cha</label>
                                        <input class="form-control" name="vn_name" type="text" placeholder="Nhập tên phim cha" wire:model.debounce.1000ms="searchParent"
                                               id='input_vn_name' autocomplete="off" />
                                    </div>
                                </div>
                                <hr>
                                {!! Form::open(array('method'=>'GET','route'=>'media.films','role'=>'search','id'=>'form-search-QLBQ',))  !!}
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 d-flex" style="text-align:center;">
                                        @include('livewire.common._buttonReset')
                                        <div style="display: inline-block; float:right;" class="ml-auto">

                                            <button type="button" class="btn btn-outline-dark btn-sm" data-target="#modal-form-export-film" data-toggle="modal"
                                                    style="background:#FFFFFF !important;color: #7F7F7F !important;
                                    border: 1px black solid; padding: 7px; margin-top: 2px; outline: none;">
                                                <span class="menu-icon fa fa-download" style="font-size: 18px;"></span>
                                                Excel file
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.page-header -->
        <div wire:loading class="loader"></div>
        <div>
            <table class="table mt-4 table-bordered table-striped table-hover">
            <h2>Danh sách tập phim</h2>
                <thead>
                <tr>
                    <th scope="col">
                    <center style="color:black">STT</center>
                    </th>
                    <th scope="col">
                        <center style='color:black;'>Tên gốc</center>
                    </th>
                    <th scope="col">
                        <center style='color:black;'>Tên tiếng Việt</center>
                    </th>
                    <th scope="col">
                        <center style='color:black;'>Năm sản xuất</center>
                    </th>
                    <th scope="col">
                        <center style='color:black;'>Doanh thu phòng vé</center>
                    </th>
                    <th scope="col">
                        <center style='color:black;'>Quyền</center>
                    </th>
                    <th scope="col">
                        <center style='color:black;'>Thể loại</center>
                    </th>
                    <th scope="col">
                        <center style='color:black;'>Diễn viên</center>
                    </th>
                    <th scope="col">
                        <center style='color:black;'>Đạo diễn</center>
                    </th>
                    <th scope="col">
                        <center style='color:black;'>Thời lượng</center>
                    </th>
                    <th scope="col">
                        <center style='color:black;'>Ghi chú</center>
                    </th>
                    <th scope="col">
                        <center style='color:black;'>Phim cha</center>
                    </th>
                    <th scope="col">
                        <center style='color:black;'> Hành động</center>
                    </th>
                </tr>
                </thead>
                <tbody>
                <form name='form-delete-all-film' action='' method="POST">
                    @csrf {{method_field('DELETE')}}
                    @forelse($data as $key => $rs)
                        <tr>
                            <th scope="row">
                                <center>
                                {{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}
                                </center>
                            </th>
                            <td><center>{!! boldTextSearchV2($rs->name,$searchName) !!}</center></td>
                            <td><center>{!! boldTextSearchV2($rs->vn_name,$searchVNName) !!}</center></td>
                            <td>{{$rs->year_create}}</td>
                            <td>{{numberFormat($rs->box_office_revenue)}}</td>
                            <td>{{$rs->permission}}</td>
                            <td>{{$rs->category_film}}</td>
                            <td>{!! boldTextSearchV2($rs->actor_name,$searchActor) !!}</td>
                            <td>{{$rs->director}}</td>
                            <td>{{$rs->count_minute}}</td>
                            <td>{{stringLimit($rs->note)}}</td>
                            <td>{!! boldTextSearchV2($rs->parent_name,$searchParent) !!}</td>
                            <td>
                                <center>
                                    @php $id = $rs->id; @endphp
                                    @include('layouts.partials.button._edit')
                                    @include('layouts.partials.button._delete')
                                </center>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="13" class="text-center">Không có kết quả phù hợp</td>
                        </tr>
                    @endforelse
                </form>
                </tbody>
            </table>
            @include('livewire.common._message')
        </div>
        {{$data->links()}}
        @include('livewire.common._modalConfirmDelete')
        {{-- Modal --}}
        <div class="modal fade" id="modal-form-upload-file" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Thêm file excel</h5>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('media.films.upload')}}" method="POST" enctype="multipart/form-data" id='form-upload-file'>
                            {{csrf_field()}}
                            <div class="form-group">
                                <input type="file" id="file-upload" name='file' accept=".xlsx,.xls">
                            </div>
                            <input type="radio" class="checkboxClick" name="purchase_contract" value='buy' required>
                            <label for="vehicle1"> Hợp đồng mua</label><br>
                            <input type="radio" class="checkboxClick" name="purchase_contract" value='sale' required>
                            <label for="vehicle1"> Hợp đồng bán</label><br>
                        </form>
                    </div>
                    <div class="modal-footer" style="text-align: center;">
                        <a href="{{route('media.films.exportExampleFileFilmList')}}" style="float: left;">File excel mẫu &nbsp;</a>
                        <div style="float: left;">
                            <p class="text-danger" id='text-error'></p>
                        </div>
                        <div style="float: right;">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                            <button type="button" class="btn btn-primary" id='btn-save-file'>Lưu</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal export phim --}}
        <div class="modal fade" id="modal-form-export-film" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title" id="exampleModalLabel">Tải file excel xuống</h2>
                    </div>
                    <div class="modal-body">
                        Bạn có chắc chắn muốn xuất file không?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Quay lại</button>
                        <button type="button" wire:click="export" class="btn btn-danger" data-dismiss="modal" id='btn-upload-film' download>Đồng ý</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- modal xóa tất cả phim được check --}}
        <div class="modal fade" id="modal-form-delete-all-film" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Xóa phim</h5>
                    </div>
                    <div class="modal-body">
                        Bạn có muốn xóa tất cả phim được chọn không? Thao tác này không thể phục hồi!
                    </div>
                    <div class="modal-footer" style="text-align: center;">
                        <div style="float: left;">
                            <p class="text-danger" id='modal-p-delete-all-film' style="display: inline-block;"></p>
                        </div>
                        <div style="float: right;">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Không</button>
                            <button type="button" class="btn btn-danger" id='btn-delete-all-film'>Xóa bỏ</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal-form-upload-list-film" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Thêm danh sách phim</h5>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="file" id="file-upload-list-film" name='file' accept=".xlsx,.xls">
                        </div>
                    </div>
                    <div class="modal-footer" style="text-align: center;">
                        <a href="{{route('media.films.exportExampleFile')}}" style="float: left;">File excel mẫu &nbsp;</a>
                        <div style="float: left;">
                            <p class="text-danger" id='text-error-list-film'></p>
                        </div>
                        <div style="float: right;">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                            <button type="button" class="btn btn-primary" id='btn-save-list-film'>Lưu</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <script>
   function getURL() {
   	var url = window.location.href;
   	url = url.split('?')[0]; //bỏ giá trị ? và 0
   	window.location.href = url + "/create";

   }
</script> -->
