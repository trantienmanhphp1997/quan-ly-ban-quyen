
<div>
    <div>
        <div>
            <div class="form-group row">
                <div class="col-lg-3" style="text-align: center;">
                    <button class="btn deadline-3m" type="button" wire:click="query1(3)" style="background: #FBA420 !important; border: 4px solid #FBA420;
                  margin-top: 5px;">
                        Sắp hết hạn trong vòng 3 tháng<br>
                        <span style="font-size: 25px;">{{$threeMonth}}</span>
                    </button>
                </div>
                <div class="col-lg-3" style="text-align: center;">
                    <button class="btn deadline-2m" type="button" wire:click="query1(2)" style="background: #FB8441 !important; border: 4px solid #FB8441;
                  margin-top: 5px;" >
                        Sắp hết hạn trong vòng 2 tháng<br>
                        <span style="font-size: 25px;">{{$twoMonth}}</span>
                    </button>
                </div>
                <div class="col-lg-3" style="text-align: center;">
                    <button class="btn deadline-1m" type="button" wire:click="query1(1)" style="background: #EE5816 !important; border: 4px solid #EE5816;  margin-top: 5px;">
                        Sắp hết hạn trong vòng 1 tháng<br>
                        <span style="font-size: 25px;">{{$oneMonth}}</span>
                    </button>
                </div>
                <div class="col-lg-3" style="text-align: center;">
                    <button class="btn deadline-1w" type="button" wire:click="query1(7)" style="background: #FB1F0E !important; border: 4px solid #FB1F0E;  margin-right: 10px; margin-top: 5px;">
                        Sắp hết hạn trong vòng 1 tuần<br>
                        <span style="font-size: 25px;">{{$oneWeek}}</span>
                    </button>
                </div>
            </div>
        </div>
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="widget-title">Tìm kiếm </h4>
                            <span class="widget-toolbar">
                     <a href="#" data-action="collapse">
                     <i class="ace-icon fa fa-chevron-up"></i>
                     </a>
                     </span>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                {!! Form::open(array('method'=>'GET','route'=>'media.films','role'=>'search','id'=>'form-search-QLBQ',))  !!}
                                <div class="row">
                                    <div class="col-xs-3 col-sm-3">
                                        <label for="form-field-select-1">Tên phim</label>
                                        <input class="form-control" name="vn_name" type="text" placeholder="Nhập tên phim" wire:model.debounce.1000ms="searchTerm"
                                               id='input_vn_name' autocomplete="off" />
                                    </div>
                                    <div class="col-xs-3 col-sm-3">
                                        <label for="form-field-select-1">Đối tác</label>
                                        <input class="form-control" name="partner_name" type="text"
                                               wire:model.debounce.1000ms="searchPartner" id='input_partner_name' autocomplete="off" placeholder="Tên đối tác" />
                                    </div>
                                    <div class="col-xs-2 col-sm-2" >
                                        <label for="form-field-select-1">Thể loại</label>
                                        <br>
                                        <select name="cate_film[]" id='select_box' wire:model="searchCategory" style="min-width:90%;" class="form-control">
                                            <option value="">
                                                Tất cả
                                            </option>
                                            @foreach($cate_film as $key=> $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-xs-2 col-sm-2" wire:ignore>
                                        <label for="form-field-select-1">Quốc gia sản xuất</label>
                                        <br>
                                        <select name="country[]" id='select_box' wire:model="searchCountry" style="min-width:90%;" class="select_country_list_film form-control">
                                            <option value="0">
                                                --- Chọn tất cả ---
                                            </option>
                                            @foreach($country as $key=> $item)
                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-xs-2 col-sm-2" >
                                        <label for="form-field-select-1">Trạng thái</label>
                                        <select id='select_box' wire:model="searchStatus" style="min-width:90%;" class="form-control">
                                            </option>
                                                <option value="0" selected="selected">Hiệu lực</option>
                                                <option value="1">Hết hạn</option>
                                                <option value="2">Đã xóa</option>
                                                <option value="3">Tất cả</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-2 col-sm-2">
                                        <label for="form-field-select-1">Năm sản xuất</label>
                                        <br>
                                        <select name="cate_film[]" id='select_box' wire:model="searchYear" style="min-width:90%;" class="form-control">
                                            <option value="">
                                                Tất cả
                                            </option>
                                            @foreach($listYear as $key=> $item)
                                                <option value="{{$item->year_create}}">{{$item->year_create}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 d-flex" style="text-align:center;">
                                        @include('livewire.common._buttonReset')
                                        <div style="display: inline-block; float:right;" class="ml-auto">                   
                                            @include('layouts.partials.button._newUpload')

                                            <button type="button" class="btn btn-outline-dark btn-sm" data-target="#modal-form-export-film" data-toggle="modal"
                                                    style="background:#FFFFFF !important;color: #7F7F7F !important;
                                    border: 1px black solid; padding: 7px; margin-top: 2px; outline: none;">
                                                <span class="menu-icon fa fa-download" style="font-size: 18px;"></span>
                                                Excel file
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.page-header -->
        <div wire:loading class="loader"></div>
        <div>
            <table class="table mt-4 table-bordered table-striped table-hover">
            <h2 style="float:left;">Danh sách phim</h2>
            <div style="display: inline-block; float:right;" class="ml-auto">
                <br>
                <button type="button" id='show_update_modal' class="btn btn-danger btn-sm" data-target="#modal-form-delete-all-film" data-toggle="modal" {{(count($checkFilms))?'':'disabled'}} >Xóa tất cả</button>
            </div>
                <thead>
                <tr>
                    <th></th>
                    <th scope="col">
                    <center style="color:black">STT</center>
                    </th>
                    <th scope="col">
                        <center style='color:black;'>Tên phim</center>
                    </th>
                    <th scope="col">
                        <center style='color:black;'>Tên phim tiếng anh</center>
                    </th>
                    <th scope="col">
                        <center style='color:black;'>Số tập phim</center>
                    </th>
                    <th scope="col">
                        <center style='color:black;'>Quốc gia sản xuất</center>
                    </th>
                    <th scope="col">
                        <center style='color:black;'>Tên đối tác</center>
                    </th>
                    <th scope="col">
                        <center style='color:black;'>Tổng chi phí mua/tập</center>
                    </th>
                    <th scope="col">
                        <center style='color:black;'>Hợp đồng số</center>
                    </th>
                    <th scope="col">
                        <center style='color:black;'>Năm sản xuất</center>
                    </th>
                    <th scope="col">
                        <center style='color:black;'>Ngày bắt đầu</center>
                    </th>
                    <th scope="col">
                        <center style='color:black;'>Thời hạn bản quyền</center>
                    </th>
                    <th scope="col">
                        <center style='color:black;'>Hành động</center>
                    </th>
                </tr>
                </thead>
                <tbody>
                            
                    @forelse($data as $key => $rs)
                        <tr>
                            <td>
                                @if(myMedia($rs->admin_id))
                                    @if($rs->deleted_at)
                                    @else
                                        <input type="checkbox" wire:model="checkFilms" value="{{$rs->id}}">
                                    @endif
                                @endif
                            </td>
                            <td scope="row">
                                <center>{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</center>
                            </td>
                            <td><a href="{{route('media.phim-le', ['id'=>$rs->id]) }}" title="Danh sách phim lẻ">{!! boldTextSearchV2($rs->vn_name,$searchTerm) !!}</a></td>
                            <td>{{$rs->product_name}}</td>
                            <td style="text-align:right">{{$rs->count_tap}}</td>
                            <td>{{$rs->country_name}}</td>
                            <td><a href="{{$rs->partner_name?route('partner.edit', ['id'=>$rs->partner_name]):'' }}">{!! boldTextSearchV2($rs->partnerName,$searchPartner) !!} </a></td>
                            <td style="text-align:right">{{numberFormat(countChiPhiTap($rs))}}</td>
                            <td><a href="{{ $rs->contract_id ? route('contract.viettel-buy.edit', $rs->contract_id) : '#'}}">{{$rs->contract?$rs->contract->contract_number:""}}</a></td>
                            <td><center>{{$rs->year_create}}</center></td>
                            <td><center>{{reFormatDate($rs->start_time, 'd-m-Y')}}</center></td>
                            <td> <center>
                                {{reFormatDate($rs->end_time, 'd-m-Y')}}
                                <br>
                                @if(myMedia($rs->admin_id))
                                @if($rs->deleted_at)
                                @else
                                    @if(strtotime(reFormatDate($rs->end_time, 'd-m-Y')) >= strtotime(date('d-m-Y'))||!$rs->end_time )
                                    @else
                                        <a data-toggle="tooltip" title="Click vào để gia hạn" href="{{ route('media.films.contractExtension',$rs->id) }}" type="button" class="btn btn-xs btn-danger" disabled>Đã hết hạn</a>
                                    @endif
                                @endif
                                @endif
                            </center>
                            <td>
                                <center>
                                    @if($rs->deleted_at)
                                    <button data-toggle="tooltip" title="Khôi phục" style="background: #fff; border: 1px solid black;margin-top: 2px;" type="button" class ='btn-buy-music' wire:click="restore({{$rs->id}})" >
                                        <i class="fa fa-undo" style="font-size:16px; color: #2199EB;"></i>
                                    </button>
                                    @else
                                        <button data-toggle="tooltip" title="{{in_array($rs->id,$dataBuySell['buy'])?'Xóa phim khỏi danh sách mua hàng':'Thêm phim vào danh sách mua hàng'}}" style="background: #fff; border: 1px solid black;margin-top: 2px;" type="button" class = 'btn-buy-music' wire:click="buyId({{$rs->id}},'{{$rs->vn_name}}')" >
                                            <i class="fa fa-shopping-cart" style="font-size:16px; {{in_array($rs->id,$dataBuySell['buy'])?'color: red;':'color: #2199EB;'}}"></i>
                                        </button>
                                        <button data-toggle="tooltip" title="{{in_array($rs->id,$dataBuySell['sell'])?'Xóa phim khỏi danh sách bán hàng':'Thêm phim vào danh sách bán hàng'}}" style="background: #fff; border: 1px solid black;margin-top: 2px;" type="button" class = 'btn-sale-music' wire:click="saleId({{$rs->id}},'{{$rs->vn_name}}')" >
                                            <i class="fa fa-dollar" style="font-size:16px; {{in_array($rs->id,$dataBuySell['sell'])?'color: red;':'color: #2199EB;'}}"></i>
                                        </button>
                                        @hasanyrole('administrator|phim-le-create-edit|phim-le-delete')
                                            <button title="Thêm danh sách phim lẻ" style="background: #fff; border: 1px solid black;margin-top: 2px;" type="button"data-target="#modal-form-upload-list-film" data-toggle="modal" data-id='{{$rs->id}}' class='btn-upload-list-film'>
                                                <i class="fa fa-upload" style="font-size:16px; color: #2199EB;"></i>
                                            </button>
                                        @endhasanyrole
                                        <br>
                                        @php $id = $rs->id; @endphp
                                        @if(myMedia($rs->admin_id))
                                            @include('layouts.partials.button._edit')
                                            @include('layouts.partials.button._delete')
                                        @endif
                                    @endif
                                </center>

                            </td>

                        </tr>
                    @empty
                        <tr>
                            <td colspan="12" class="text-center">Không có kết quả phù hợp</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
                {{$data->links()}}

            @include('livewire.common._message')
        </div>
        @include('livewire.common._modalConfirmDelete')
        {{-- Modal --}}
        @include('layouts.partials.lib._date_picker')
        <div class="modal fade" id="modal-form-upload-file" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title" id="exampleModalLabel">Thêm file excel</h2>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('media.films.upload')}}" method="POST" enctype="multipart/form-data" id='form-upload-file'>
                            {{csrf_field()}}
                            <div class="form-group">
                                <input type="file" id="file-upload" name='file' accept=".xlsx,.xls">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer" style="text-align: center;">
                        <form>
                            {{csrf_field()}}
                            <a href="#" style="float: left;" id='example_elementA'>File excel mẫu &nbsp;</a>
                            <button style="display:none;" id='button_example' type="button" wire:click="example" class="btn btn-danger" data-dismiss="modal">Đồng ý</button>
                        </form>
                        {{-- <a href="{{route('media.music.example')}}" style="float: left;">File excel mẫu &nbsp;</a> --}}
                        <div style="float: left;">
                            <p class="text-danger" id='text-error'></p>
                        </div>
                        <div style="float: right;">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                            <button type="button" class="btn btn-primary" id='btn-save-file'>Lưu</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal export phim --}}
        <div class="modal fade" id="modal-form-export-film" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title" id="exampleModalLabel">Tải file excel xuống</h2>
                    </div>
                    <div class="modal-body">
                        Bạn có chắc chắn muốn xuất file không?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Quay lại</button>
                        <button type="button" wire:click="export" class="btn btn-danger" data-dismiss="modal" id='btn-upload-film'>Đồng ý</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- modal xóa tất cả phim được check --}}
        <div class="modal fade" id="modal-form-delete-all-film" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title" id="exampleModalLabel">Xác nhận xóa tất cả phim</h2>
                    </div>
                    <div class="modal-body">
                        Bạn có muốn xóa tất cả phim được chọn không?
                    </div>
                    <div class="modal-footer" style="text-align: center;">
                        <div style="float: left;">
                            <p class="text-danger" id='modal-p-delete-all-film' style="display: inline-block;"></p>
                        </div>
                        <div style="float: right;">
                            <button type="button" class="btn" data-dismiss="modal">Đóng</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal" wire:click='deleteAll()'>Xóa bỏ</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal-form-upload-list-film" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title" id="exampleModalLabel">Tải lên danh sách phim lẻ</h2>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="file" id="file-upload-list-film" name='file' accept=".xlsx,.xls">
                        </div>
                    </div>
                    <div class="modal-footer" style="text-align: center;">
                        <form>
                            {{csrf_field()}}
                            <a href="#" style="float: left;" id='example_elementB'>File excel mẫu &nbsp;</a>
                            <button style="display:none;" id='button_exampleB' type="button" wire:click="exampleLe" class="btn btn-danger" data-dismiss="modal">Đồng ý</button>
                        </form>
                        <div style="float: left;">
                            <p class="text-danger" id='text-error-list-film'></p>
                        </div>
                        <div style="float: right;">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                            <button type="button" class="btn btn-primary" id='btn-save-list-film'>Lưu</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="modal fade" id="modal-download-error" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title" id="exampleModalLabel">Tải xuống danh sách lỗi</h2>
                    </div>
                    <form method="POST" name='form-get-error-upload' action="{{route('media.music.getError')}}">
                        {{csrf_field()}}
                        <div class="modal-body">
                            <div class="form-group">
                                <input id='error-upload-music' type="text" name="error-upload" value="{{ Session::get('outputError') }}" style="display: none;" >
                            </div>
                        </div>
                        <div class="modal-footer" style="text-align: center;">
                            <div style="float: right;">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
                                <button type="button" class="btn btn-primary" id='btn-submit-get-error' data-dismiss="modal">Tải file lỗi</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <div class="modal fade" id="modal-download-error-le" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title" id="exampleModalLabel">Tải xuống danh sách lỗi</h2>
                    </div>
                    <form method="POST" name='form-get-error-upload' action="{{route('media.music.getError')}}">
                        {{csrf_field()}}
                        <div class="modal-body">
                            <div class="form-group">
                                <input id='error-upload-music-le' type="text" name="error-upload" value="{{ Session::get('outputError') }}" style="display: none;" >
                            </div>
                        </div>
                        <div class="modal-footer" style="text-align: center;">
                            <div style="float: right;">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
                                <button type="button" class="btn btn-primary" id='btn-submit-get-error-le' data-dismiss="modal" >Tải file lỗi</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div id='div-form-upload-list-film' style="display: none;">
            @foreach($data as $film)
                <form action="{{route('media.films.uploadListFilm',['id'=>$film->id])}}" method="POST" enctype="multipart/form-data" name='{{$film->id}}'>
                    @csrf
                </form>
            @endforeach
        </div>


    </div>
</div>

    @section('js')
        <script type="text/javascript">
            document.addEventListener('DOMContentLoaded', function(){
                $('#example_elementA').click(function(e){
                    e.preventDefault()
                    document.getElementById('button_example').click()
                })
                $('#example_elementB').click(function(e){
                    e.preventDefault()
                    document.getElementById('button_exampleB').click()
                })
                $('#btn-download-error').click()
                $('#btn-download-error-le').click()

                $('.select_country_list_film').select2();
                $('.select_country_list_film').on('change', function (e) {
                    var data = $('.select_country_list_film').select2("val");
                    @this.set('searchCountry', data);
                });

            })
            document.addEventListener('livewire:load', function () {
                $(function () {
                    $('#btn-submit-get-error').on('click', (e) => {
                        var data = $('#error-upload-music').val()
                        window.livewire.emit('getError', data);
                    })
                    $('#btn-submit-get-error-le').on('click', (e) => {
                        var data = $('#error-upload-music-le').val()
                        window.livewire.emit('getErrorLe', data);
                    })
                })
            })

        </script>
        <script>
        function getEditURL(id) {
            var url = window.location.href;
            url = url.split('?')[0];
            window.location.href = url + "/" + id +  "/edit";
        }
        </script>
    @endsection
