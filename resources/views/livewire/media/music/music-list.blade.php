<div>
    <div>
        <div>
            <div class="row">
                <div class="col-sm-3" style="text-align: center;">
                    <button class="btn deadline-3m" wire:click="query1(3)" style="background: #FBA420 !important; border: 4px solid #FBA420;">
                        Sắp hết hạn trong vòng 3 tháng<br>
                        <span style="font-size: 25px;">{{$threeMonth}}</span>
                    </button>
                </div>
                <div class="col-sm-3" style="text-align: center;">
                    <button class="btn deadline-2m" wire:click="query1(2)" style="background: #FB8441 !important; border: 4px solid #FB8441;">
                        Sắp hết hạn trong vòng 2 tháng<br>
                        <span style="font-size: 25px;">{{$twoMonth}}</span>
                    </button>
                </div>
                <div class="col-sm-3" style="text-align: center;">
                    <button class="btn deadline-1m" wire:click="query1(1)" style="background: #EE5816 !important; border: 4px solid #EE5816;">
                        Sắp hết hạn trong vòng 1 tháng<br>
                        <span style="font-size: 25px;">{{$oneMonth}}</span>
                    </button>
                </div>
                <div class="col-sm-3" style="text-align: center;" >
                    <button class="btn deadline-1w" wire:click="query1(7)" style="background: #FB1F0E !important; border: 4px solid #FB1F0E;">
                        Sắp hết hạn trong vòng 1 tuần<br>
                        <span style="font-size: 25px;">{{$oneWeek}}</span>
                    </button>
                </div>
            </div>
        </div>
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="widget-title">Tìm kiếm</h4>

                            <span class="widget-toolbar">
                            <a href="#" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up"></i>
                            </a>
                        </span>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main">
                                {!! Form::open(array('method'=>'GET','route'=>'media.music.index','role'=>'search','id'=>'form-search-QLBQ',))  !!}
                                <div class="row">
                                    <div class="col-xs-3 col-sm-3">
                                        <label for="form-field-select-1">Nhập tên bài hát</label>
                                        <input type="text"  class="form-control" placeholder="Nhập tên bài hát" wire:model.debounce.1000ms="searchName" />
                                    </div>
                                    <div class="col-xs-3 col-sm-3">
                                        <label for="form-field-select-1">Nhập tên ca sĩ</label>
                                        <input type="text"  class="form-control" placeholder="Nhập tên ca sĩ" wire:model.debounce.1000ms="searchSinger" />
                                    </div>
                                    <div class="col-xs-3 col-sm-3">
                                        <label for="form-field-select-1">Nhập tên nhạc sĩ</label>
                                        <input type="text"  class="form-control" placeholder="Nhập tên nhạc sĩ" wire:model.debounce.1000ms="searchMusician" />
                                    </div>
                                    <div class="col-xs-3 col-sm-3" >
                                        <label for="form-field-select-1">Trạng thái</label>
                                        <select id='select_box' wire:model="searchStatus" style="min-width:90%;" class="form-control">
                                            </option>
                                                <option value="0" selected="selected">Hiệu lực</option>
                                                <option value="1">Hết hạn</option>
                                                <option value="2">Đã xóa</option>
                                                <option value="3">Tất cả</option>
                                        </select>
                                    </div>
                                </div>
                                <hr>
                                <div class="row" wire:ignore>
                                    <div class="col-xs-12 col-sm-12 d-flex" style="text-align:center;">
                                        @include('livewire.common._buttonReset')
                                        <div style="display: inline-block; float:right;" class="ml-auto">

                                            @include('layouts.partials.button._newUpload')
                                            
                                            <button type="button" class="btn btn-outline-dark btn-sm" data-target="#modal-form-export-music" data-toggle="modal"
                                                    style="background:#FFFFFF !important;color: #7F7F7F !important;
                                            border: 1px black solid; padding: 7px; margin-top: 2px; outline: none;">
                                                <span class="menu-icon fa fa-download" style="font-size: 18px;"></span>
                                                Excel file
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div><!-- /.page-header -->
        {{-- Modal Upload Nhạc--}}
        <div class="modal fade" id="modal-form-upload-file" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title" id="exampleModalLabel">Thêm file excel</h2>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('media.music.upload')}}" method="POST" enctype="multipart/form-data" id='form-upload-file'>
                            {{csrf_field()}}
                            <div class="form-group">
                                <input type="file" id="file-upload" name='file' accept=".xlsx,.xls">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer" style="text-align: center;">
                        <form>
                            {{csrf_field()}}
                            <a href="#" style="float: left;" id='example_elementA'>File excel mẫu &nbsp;</a>
                            <button style="display:none;" id='button_example' type="button" wire:click="example" class="btn btn-danger" data-dismiss="modal">Đồng ý</button>
                        </form>
                        {{-- <a href="{{route('media.music.example')}}" style="float: left;">File excel mẫu &nbsp;</a> --}}
                        <div style="float: left;">
                            <p class="text-danger" id='text-error'></p>
                        </div>
                        <div style="float: right;">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                            <button type="button" class="btn btn-primary" id='btn-save-file'>Lưu</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal export nhạc --}}
        <div class="modal fade" id="modal-form-export-music" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">

            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="{{ route('media.music.export') }}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="modal-header">
                            <h2 class="modal-title" id="exampleModalLabel">Tải file excel xuống</h2>
                        </div>
                        <div class="modal-body">
                            Bạn có chắc chắn muốn xuất file không?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Quay lại</button>
                            <button type="button" wire:click="export" data-dismiss="modal"class="btn btn-danger" id='btn-upload-music'>Đồng ý</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{-- QLBQ --}}
        <div class="row">
            <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                <div class="row">
                    <div class="col-xs-12">
                        <table id="simple-table" class="table table-striped table-bordered table-hover">
                        <div class="col-xs-12 col-sm-12 d-flex" style="text-align:center;">
                        <div style="float:left;"><h2>Danh sách nhạc</h2></div>
                        <div style="display: inline-block; float:right;" class="ml-auto">
                            <br>
                            <button type="button" id='show_update_modal' class="btn btn-info btn-sm" data-target="#modal-update-music" data-toggle="modal" {{(count($this->checkMusic))?'':'disabled'}} >Cập nhật quyền nhạc</button>
                        </div>
                        </div>
                            <thead>
                            <tr>
                                <th></th>
                                <th style="text-align: center; color: black;" class="hidden-480">STT</th>
                                <th style="text-align: center; color: black;">Tên bài hát</th>
                                <th style="text-align: center; color: black;">Ca sĩ</th>
                                <th style="text-align: center; color: black;">Nhạc sĩ</th>
                                <th style="text-align: center; color: black;">Quyền liên quan</th>
                                <th style="text-align: center; color: black;">Quyền tác giả</th>
                                <th style="text-align: center; color: black;">Thời gian phát hành</th>
                                <th style="text-align: center; color: black;">Thời gian kết thúc</th>
                                <!-- <th>Hợp đồng Cps</th> -->
                                <th style="text-align: center; color: black;">Loại hợp đồng</th>
                                <th style="text-align: center; color: black;">Hành động</th>
                            </tr>
                            </thead>

                            <tbody>
                            <div wire:loading class="loader"></div>

                            @forelse($data as $key => $rs)
                                <tr>

                                    <td>
                                        @if(myMedia($rs->admin_id))
                                        @if($rs->deleted_at)
                                        @else
                                        <input type="checkbox" wire:model="checkMusic" value="{{$rs->id}}">
                                        @endif
                                        @endif
                                    </td>
                                    <td scope="row">
                                    <center>{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</center>
                                    </td>
                                    <td>{!! boldTextSearchV2($rs->name,$searchName) !!}</td>
                                    <td>{!! boldTextSearchV2($rs->singer,$searchSinger) !!}</td>
                                    <td>{!! boldTextSearchV2($rs->musician,$searchMusician) !!}</td>
                                    <td> <a href="{{ $rs->contract_id ? route('contract.viettel-buy.edit', $rs->contract_id) : '#'}}">{{$rs->contract?$rs->contract->contract_number:''}}</a></td>
                                    <td><a href="{{ $rs->contract_id_tac_gia ? route('contract.viettel-buy.edit', $rs->contract_id_tac_gia) : '#'}}">{{$rs->tac_gia}}</a></td>
                                    <td><center>{{reFormatDate($rs->start_time, 'd-m-Y')}}</center></td>
                                    <td><center>
                                        {{reFormatDate($rs->end_time, 'd-m-Y')}}
                                        <br>
                                        @if(myMedia($rs->admin_id))
                                        @if($rs->deleted_at)
                                        @else
                                        @if(strtotime(reFormatDate($rs->end_time, 'd-m-Y')) >= strtotime(date('d-m-Y'))||!$rs->end_time)
                                        <a data-toggle="tooltip" title="Click vào để gia hạn" href="{{ route('media.music.contractExtension',$rs->id) }}" type="button" class="btn btn-xs btn-primary"> Gia hạn</a>
                                        @else
                                        <a data-toggle="tooltip" title="Click vào để gia hạn" href="{{ route('media.music.contractExtension',$rs->id) }}" type="button" class="btn btn-xs btn-danger"> Đã hết hạn</a>
                                        @endif
                                        @endif
                                        @endif
                                        </center>
                                    </td>
                                    <!-- <td>
                                        <a href="{{ route('contract.viettel-partner') . '?media_id=' . $rs->id }}" type="button" class="btn btn-sm btn-primary">Chi tiết</a>
                                    </td> -->
                                    <td>{{ isset($typeFee[$rs->type_fee_id])? $typeFee[$rs->type_fee_id] : ""}}</td>
                                    <td><center>
                                        @if($rs->deleted_at)
                                        <button  data-toggle="tooltip" title="Khôi phục" style="background: #fff; border: 1px solid black;margin-top: 2px;" type="button" class ='btn-buy-music' wire:click="restore({{$rs->id}})" >
                                        <i class="fa fa-undo" style="font-size:16px; color: #2199EB;"></i>
                                        </button>
                                        @else
                                            <button data-toggle="tooltip" title="{{in_array($rs->id,$dataBuySell['buy'])?'Xóa nhạc khỏi danh sách mua hàng':'Thêm nhạc vào danh sách mua hàng'}}" style="background: #fff; border: 1px solid black;margin-top: 2px;" type="button" wire:click="buyId({{$rs->id}},'{{$rs->name}}')">
                                                <i class="fa fa-shopping-cart" style="font-size:16px;{{in_array($rs->id,$dataBuySell['buy'])?'color: red;':'color: #2199EB;'}}"></i>
                                            </button>

                                            <button data-toggle="tooltip" title="{{in_array($rs->id,$dataBuySell['sell'])?'Xóa nhạc khỏi danh sách bán hàng':'Thêm nhạc vào danh sách bán hàng'}}" style="background: #fff; border: 1px solid black;margin-top: 2px;" type="button" wire:click="saleId({{$rs->id}},'{{$rs->name}}')">
                                                <i class="fa fa-dollar" style="font-size:16px; {{in_array($rs->id,$dataBuySell['sell'])?'color: red;':'color: #2199EB;'}}"></i>
                                            </button>
                                            @php $id = $rs->id; @endphp
                                            @if(myMedia($rs->admin_id))
                                                @include('layouts.partials.button._edit')
                                                @include('layouts.partials.button._delete')
                                            @endif
                                        @endif
                                        </center></td>
                                </tr>
                                @empty
                                    <tr>
                                        <td colspan="11" class="text-center"> Không có kết quả phù hợp</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                        {{ $data->links() }}
                    </div><!-- /.span -->
                </div><!-- /.row -->
            </div><!-- /.col -->
        </div><!-- /.row -->

        {{-- modal xóa nhạc --}}
        @include('livewire.common._message')
        <div class="modal fade" id="modal-download-error" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title" id="exampleModalLabel">Tải xuống danh sách lỗi</h2>
                    </div>
                    <form method="POST" name='form-get-error-upload' action="{{route('media.music.getError')}}">
                        {{csrf_field()}}
                        <div class="modal-body">
                            <div class="form-group">
                                <input id='error-upload-music' type="text" name="error-upload" value="{{ Session::get('outputError') }}" style="display: none;" >
                            </div>
                        </div>
                        <div class="modal-footer" style="text-align: center;">
                            <div style="float: right;">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
                                <button type="button" class="btn btn-primary" id='btn-submit-get-error' data-dismiss="modal">Tải file lỗi</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    @include('livewire.common._modalConfirmDelete')
        <div class="modal fade" id="modal-form-sale-music" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Bán nhạc</h5>
                    </div>
                    <div class="modal-body">
                        Xác nhận bán bài hát
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Quay lại</button>
                        <button type="button" class="btn btn-danger" id='btn-sale-music' data-dismiss="modal">Đồng ý</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal cập nhật quyền nhạc -->
        <div wire:ignore.self class="modal fade" id="modal-update-music" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
>           <div class="modal-backdrop fade in" style="height: 100%;"></div>
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title" id="exampleModalLabel">Cập nhật quyền nhạc</h2>
                    </div>
                    <div class="modal-body">
                        <label>Cập nhật tất cả các bài hát được chọn</label>
                        <br><br>
                        <div class="form-group row" >
                            <label for="inputEmail3" class="col-sm-5 col-form-label">Quyền liên quan: </label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" placeholder="Quyền liên quan" wire:model.debounce.1000ms="updateContractNumber">
                                @error('updateContractNumber')
                                    @include('layouts.partials.text._error')
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row" >
                            <label for="inputEmail3" class="col-sm-5 col-form-label">Quyền tác giả: </label>
                            <div class="col-sm-6">
                                <input type="text" name="" class="form-control" placeholder="Quyền tác giả" wire:model.debounce.1000ms="updateQuyenTacGia">
                                @error('updateQuyenTacGia')
                                    @include('layouts.partials.text._error')
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    <button type="button" id="close-update-music-modal" class="btn btn-primary" data-dismiss="modal" wire:click.prevent="resetInput()">Quay lại</button>
                    <button type="button" wire:click="update" class="btn btn-danger">Đồng ý</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- QLBQ --}}

        <div class="modal fade" id="modal-form-buy-music" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Mua nhạc</h5>
                    </div>
                    <div class="modal-body">
                        Xác nhận mua bài hát
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Quay lại</button>
                        <button type="button" class="btn btn-danger" id='btn-buy-music'data-dismiss="modal">Đồng ý</button>
                    </div>
                </div>
            </div>
        </div>

        {{-- form edit nhạc --}}
        <div id="div-form-edit">
            @foreach($data as $rs)
                <form name='{{$rs->id}}' action="{{route('media.music.edit',$rs->id)}}" method="GET">
                </form>
            @endforeach
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            var musicID
            var name
            var single

            // hợp đồng mua
            $('.btn-buy-music').click(function(){
                musicID = $(this).attr('data-id')
                name = $(this).attr('data-name')
                singer = $(this).attr('data-singer')
            })
            $("#btn-buy-music").click(function(){
                var _token = $("input[name='_token']").val();
                $.ajax({
                    url: "/media/music/buy",
                    type:'POST',
                    data: {
                        _token:_token,
                        musicID:musicID,
                        name:name,
                        singer:singer
                    },
                    success: function(data) {
                        console.log(data)
                    }
                });
            });

            // hợp đồng bán
            $('.btn-sale-music').click(function(){
                musicID = $(this).attr('data-id')
                name = $(this).attr('data-name')
                singer = $(this).attr('data-singer')
            })
            $("#btn-sale-music").click(function(){
                var _token = $("input[name='_token']").val();
                $.ajax({
                    url: "/media/music/sale",
                    type:'POST',
                    data: {
                        _token:_token,
                        musicID:musicID,
                        name:name,
                        singer:singer
                    },
                    success: function(data) {
                        console.log(data)
                    }
                });
            });
            $('#btn-download-error').click()
        })
    </script>
    @section('css')
        <link rel="stylesheet" type="text/css" href="{{asset('css/QLBQ.css')}}">

    @endsection

    @section('js')
        <script src="{{asset('js/QLBQ.js')}}"></script>
        <script type="text/javascript">
            document.addEventListener('DOMContentLoaded', function(){
                $('#example_elementA').click(function(e){
                    e.preventDefault()
                    document.getElementById('button_example').click()
                })
                $('#show_update_modal').click(function(e){
                    $('.red').remove();
                })
            })
            document.addEventListener('livewire:load', function () {
                $(function () {
                    $('#btn-submit-get-error').on('click', (e) => {
                        var data = $('#error-upload-music').val()
                        window.livewire.emit('getError', data);
                    })
                })
            })
        </script>
        <script>
            function getEditURL(id) {
                var url = window.location.href;
                url = url.split('?')[0];
                window.location.href = url + "/" + id +  "/edit";
            }
        </script>
    @endsection
</div>
