<div>
<div>    
    <div class="page-header">
        <div class="row">
            <div class="col-sm-12">
                <div class="widget-box">
                    <div class="widget-header">
                        <h4 class="widget-title">Tìm kiếm</h4>

                        <span class="widget-toolbar">
                            <a href="#" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up"></i>
                            </a>
                        </span>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row">
                                <div class="col-xs-4 col-sm-4">
                                    <label for="form-field-select-1">Mã ổ</label>
                                    <input class="form-control" wire:model.debounce.1000ms="searchDriver" placeholder="Nhập mã ổ" autocomplete="off" />
                                </div>
                                <div class="col-xs-4 col-sm-4">
                                    <label for="form-field-select-1">Tên phim tiếng Việt</label>
                                    <input class="form-control" wire:model.debounce.1000ms="searchTerm" placeholder="Nhập tên phim tiếng Việt" autocomplete="off" />
                                </div>
                            </div>                         
                            <hr>                         
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 d-flex" style="text-align:center;">
                                    @include('livewire.common._buttonReset')
                                    <div style="display: inline-block; float:right;" class="ml-auto">                   
                                        @include('layouts.partials.button._newUpload')
                                        <button type="button" class="btn btn-outline-dark btn-sm" data-target="#modal-form-export-film" data-toggle="modal"
                                                    style="background:#FFFFFF !important;color: #7F7F7F !important;
                                    border: 1px black solid; padding: 7px; margin-top: 2px; outline: none;">
                                                <span class="menu-icon fa fa-download" style="font-size: 18px;"></span>
                                                Excel file
                                            </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    <div class="modal fade" id="modal-form-upload-file" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title" id="exampleModalLabel">Thêm file excel</h2>
                </div>
                <div class="modal-body">
                    <form action="{{route('media.manage.upload')}}" method="POST" enctype="multipart/form-data" id='form-upload-file'>
                        {{csrf_field()}}
                        <div class="form-group">
                            <input type="file" id="file-upload" name='file' accept=".xlsx,.xls">
                        </div>
                    </form>
                </div>
                <div class="modal-footer" style="text-align: center;">
                    <form>
                        {{csrf_field()}}
                        <a href="#" style="float: left;" id='example_elementA'>File excel mẫu &nbsp;</a>
                        <button style="display:none;" id='button_exampleA' type="button" wire:click="example" class="btn btn-danger" data-dismiss="modal">Đồng ý</button>
                    </form>
                    {{-- <a href="{{route('media.music.example')}}" style="float: left;">File excel mẫu &nbsp;</a> --}}
                    <div style="float: left;">
                        <p class="text-danger" id='text-error'></p>
                    </div>
                    <div style="float: right;">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                        <button type="button" class="btn btn-primary" id='btn-save-file'>Lưu</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Modal export phim --}}
    <div class="modal fade" id="modal-form-export-film" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title" id="exampleModalLabel">Tải file excel xuống</h2>
                </div>
                <div class="modal-body">
                    Bạn có chắc chắn muốn xuất file không?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Quay lại</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" wire:click="export">Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
        <div class="modal fade" id="modal-download-error" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title" id="exampleModalLabel">Tải xuống danh sách lỗi</h2>
                    </div>
                    <form method="POST" name='form-get-error-upload' action="{{route('media.music.getError')}}">
                        {{csrf_field()}}
                        <div class="modal-body">
                            <div class="form-group">
                                <input id='error-upload-music' type="text" name="error-upload" value="{{ Session::get('outputError') }}" style="display: none;" >
                            </div>
                        </div>
                        <div class="modal-footer" style="text-align: center;">
                            <div style="float: right;">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
                                <button type="button" class="btn btn-primary" id='btn-submit-get-error' data-dismiss="modal" >Tải file lỗi</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div><!-- /.page-header -->
    {{-- QLBQ --}}
    @if ($message = Session::get('message_success'))
        <div class="alert alert-success">
          <p>{{ $message }}</p>
        </div>
    @endif
    @if ($message = Session::get('message_danger'))
        <div class="alert alert-danger">
          <p>{{ $message }}</p>
        </div>
    @endif
    <div wire:loading class="loader"></div>
    <table class="table mt-4 table-bordered table-striped table-hover">
        <h2>Quản trị tư liệu</h2>
        <thead>
            <tr>
                <th scope="col"> 
                    <center style='color:black;'>STT</center>
                </th>
                <th scope="col">
                   <center style='color:black;'>Mã ổ</center>
                </th>
                <th scope="col">
                    <center style='color:black;'>Ổ backup</center>
                </th>
                <th scope="col">
                    <center style='color:black;'>Tên file</center>
                </th>
                <th scope="col">
                    <center style='color:black;'>Tên phim tiếng Anh</center>
                </th>
                <th scope="col">
                    <center style='color:black;'>Tên phim tiếng Việt</center>
                </th>
               <th scope="col">
                    <center style='color:black;'>Quốc gia</center>
                </th>
                <th scope="col">
                    <center style='color:black;'>Đối tác</center>
                </th>
                <th scope="col">
                   <center style='color:black;'>Thể loại phim</center>
                </th>
                <th scope="col">
                    <center style='color:black;'>Số tập</center>
                </th>
                <th scope="col">
                    <center style='color:black;'>Loại file</center>
                </th>
                <th scope="col">
                    <center style='color:black;'>Thời lượng</center>
                </th>
                <th scope="col">
                   <center style='color:black;'>Dung lượng</center>
                </th>
                <th scope="col">
                    <center style='color:black;'>Đuôi file</center>
                </th>
                <th scope="col">
                   <center style='color:black;'>Thời hạn bản quyền</center>
                </th>
                <th scope="col">
                    <center style='color:black;'>Người giao</center>
                </th>
                <th scope="col">
                   <center style='color:black;'> Hành động</center>
                </th>
            </tr>
        </thead>
        <tbody>
            <!-- <form name='form-delete-all-film' action='{{route('media.manage')}}' method="POST"> -->
                @forelse($data as $key => $rs)
                    <tr>
                        <th scope="row">
                            <center>
                            {{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}
                            </center>
                        </th>
                        <td><center>{!! boldTextSearchV2($rs->driver,$searchDriver) !!}</center></td>
                        <td><center>{{$rs->backup_driver}}</center></td>
                        <td><center>{{$rs->file_name}}</center></td>
                        <td><center>{{$rs->film_name}}</center></td>
                        <td><center>{!! boldTextSearchV2($rs->vn_name,$searchTerm) !!}</center></td>
                        <td><center>{{$rs->country}}</center></td>
                        <td><center>{{$rs->partner}}</center></td>
                        <td><center>{{($rs->series_film == 1)? "Phim bộ" : (($rs->series_film == 2)?"Phim lẻ":"")}}</center></td>
                        <td><center>{{$rs->count_film}}</center></td>
                        <td><center>{{$rs->file_type}}</center></td>
                        <td><center>{{$rs->time_number}}</center></td>
                        <td><center>{{$rs->volumn}}</center></td>
                        <td><center>{{$rs->file_format}}</center></td>
                        <td><center>{{reFormatDate($rs->end_time, 'd-m-Y')}}</center></td>
                        <td><center>{{$rs->file_delivery_person}}</center></td>
                        <td><center>
                            @php $id = $rs->id; @endphp
                            @include('layouts.partials.button._edit')
                            @include('layouts.partials.button._delete')
                        </center></td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="17" class="text-center">Không có kết quả phù hợp</td>
                    </tr>
                @endforelse
            <!-- </form> -->
        </tbody>
    </table>
    @include('livewire.common._message')
    @include('livewire.common._modalConfirmDelete')
    {{-- @include('layouts.partials._pagination') --}}
    {{$data->links()}}
</div>
</div>
    {{-- Modal --}}
    @section('js')
        <script type="text/javascript">
            document.addEventListener('DOMContentLoaded', function(){
                $('#example_elementA').click(function(e){
                    e.preventDefault()
                    document.getElementById('button_exampleA').click()
                })
                $('#btn-download-error').click()
            })
            document.addEventListener('livewire:load', function () {
                $(function () {
                    $('#btn-submit-get-error').on('click', (e) => {
                        var data = $('#error-upload-music').val()
                        window.livewire.emit('getError', data);
                    })
                })
            })

        </script>
    @endsection
