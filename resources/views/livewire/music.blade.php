<div>
    <table class="table-auto" style="width: 100%;">
      <thead>
        <tr>
          <th class="px-4 py-2">ID</th>
          <th class="px-4 py-2">Name</th>
          <th class="px-4 py-2">Email</th>
        </tr>
      </thead>
      <div wire:loading class="loader"></div>
      <tbody>
        @foreach ($data as $rs)
            <tr>
            <td class="border px-4 py-2">{{ $data->id }}</td>
            <td class="border px-4 py-2">{{ $data->name }}</td>
            <td class="border px-4 py-2">{{ $data->singer }}</td>
            </tr>
        @endforeach
      </tbody>
    </table>
  
    {{ $data->links() }}
</div>