<div class="row" style=" background: #F6F6F7; border-radius: 16px; margin-top: 20px; padding: 32px 17px;">
    <div class="page-header">
        <div class="row">
            <div class="col-sm-12">
                <div class="widget-box">
                    <div class="widget-header">
                        <h4 class="widget-title">Tìm kiếm</h4>
                        <span class="widget-toolbar">
                            <a href="#" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up"></i>
                            </a>
                        </span>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6">
                                    <label>Chọn khoảng thời gian thống kê: Từ năm</label>
                                    <select class="form-group" wire:model='startYear'>
                                          @foreach($startYearList as $year)
                                                <option value="{{$year}}">{{$year}}</option>
                                          @endforeach
                                    </select>
                                    <label>đến năm</label>
                                    <select class="form-group" wire:model='endYear'>
                                          @foreach($endYearList as $year)
                                                <option value="{{$year}}">{{$year}}</option>
                                          @endforeach
                                    </select>
                                </div>
                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 d-flex" style="text-align:center;">
                                    @include('livewire.common._buttonReset')

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <div class="row">
            <div class="col-sm-6">
                <figure class="highcharts-figure">
                    <div id="film-year-chart"></div>
                </figure>
            </div>
            <div class="col-sm-6">
                <figure class="highcharts-figure">
                    <div id="film-year-create-chart"></div>
                </figure>
            </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
               <figure class="highcharts-figure">
                <div id="film-country-chart"></div>
            </figure>
        </div><div class="col-sm-6">
               <figure class="highcharts-figure">
                <div id="film-list-country-chart"></div>
            </figure>
        </div>
    </div>
    @include('layouts.partials.lib._date_picker')
</div>

<script>
    document.addEventListener('DOMContentLoaded', function(){
        window.onload=function(){

            Livewire.emit('getFilmYearData');
            Livewire.on('showFilmYearData',function(filmYear){
                Highcharts.chart('film-year-chart', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Biểu đồ về tổng lượng phim qua các năm'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        title: {
                            text: 'Năm bản quyền'
                        },
                        categories: filmYear.map(item => item.year),
                        type: "date",


                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Số lượng (phim)'
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                          '<td style="padding:0"><b>{point.y} phim</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                          pointPadding: 0.2,
                          borderWidth: 0
                        }
                    },
                    series: [{
                        name: 'Tổng lượng phim qua các năm',
                        data: filmYear.map(item => Number(item.count))
                    }]
                });
            })
            Livewire.emit('getFilmCountryData');
            Livewire.on('showFilmCountryData',function(filmCountry){
                Highcharts.chart('film-country-chart', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Biểu đồ về số lượng phim bộ giữa các quốc gia'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        title: {
                            text: 'Quốc gia sản xuất'
                        },
                        categories: filmCountry.map(item => item.country_name),
                        type: "date",


                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Số lượng (phim)'
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                          '<td style="padding:0"><b>{point.y} phim</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                          pointPadding: 0.2,
                          borderWidth: 0
                        }
                    },
                    series: [{
                        name: 'Số  lượng phim bộ giữa các quốc gia',
                        data: filmCountry.map(item => Number(item.count))
                    }]
                });
            })

            Livewire.emit('getFilmYearCreateData');
            Livewire.on('showFilmYearCreateData',function(filmYearCreate){
                Highcharts.chart('film-year-create-chart', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Biểu đồ về số lượng phim theo năm sản xuất'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        title: {
                            text: 'Năm sản xuất'
                        },
                        categories: filmYearCreate.map(item => item.year_create),
                        type: "date",


                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Số lượng (phim)'
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                          '<td style="padding:0"><b>{point.y} phim</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                          pointPadding: 0.2,
                          borderWidth: 0
                        }
                    },
                    series: [{
                        name: 'Số lượng phim theo năm sản xuất',
                        data: filmYearCreate.map(item => Number(item.count))
                    }]

                });
            })

            Livewire.emit('getFilmListCountryData');
            Livewire.on('showFilmListCountryData',function(filmListCountry){
                Highcharts.chart('film-list-country-chart', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Biểu đồ về số lượng phim lẻ giữa các quốc gia'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        title: {
                            text: 'Quốc gia sản xuất'
                        },
                        categories: filmListCountry.map(item => item.country_name),
                        type: "date",


                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Số lượng (phim)'
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                          '<td style="padding:0"><b>{point.y} phim</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                          pointPadding: 0.2,
                          borderWidth: 0
                        }
                    },
                    series: [{
                        name: 'Số lượng phim lẻ giữa các quốc gia',
                        data: filmListCountry.map(item => Number(item.count))
                    }]

                });
            })
        }
    })
</script>
