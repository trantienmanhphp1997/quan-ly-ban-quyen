<!DOCTYPE html>
<html lang="en">
@livewireScripts
@include('layouts.partials._head')
<body class="no-skin">
<!-- #section:basics/navbar.layout -->
@include('layouts.partials._header')

<!-- /section:basics/navbar.layout -->
<div class="main-container" id="main-container">
    <script type="text/javascript">
        try{ace.settings.check('main-container' , 'fixed')}catch(e){}
    </script>

    <!-- #section:basics/sidebar -->
    @include('layouts.partials._sidebar')

    <!-- /section:basics/sidebar -->
    <div class="main-content">
        <div class="main-content-inner">
            <!-- #section:basics/content.breadcrumbs -->
            <div class="breadcrumbs" id="breadcrumbs">
                <script type="text/javascript">
                    try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
                </script>

                <ul class="breadcrumb">
                    <li>
                        <!-- <i class="ace-icon fa fa-home home-icon"></i> -->
                        <!-- <a href="#">Home</a> -->
                    </li>
                    <!-- <li class="active">Dashboard</li> -->
                </ul><!-- /.breadcrumb -->

                <!-- #section:basics/content.searchbox -->

                <!-- /section:basics/content.searchbox -->
            </div>

            <!-- /section:basics/content.breadcrumbs -->
        <div class="content-wrapper">
            <div class="page-content">
                @include('layouts.partials._message')
                @yield('content')
            </div><!-- /.page-content -->
        </div>

        </div>
    </div><!-- /.main-content -->

    @include('layouts.partials._footer')
</div><!-- /.main-main-container -->
@include('layouts.partials._script')
@yield('css')
@yield('js')
</body>
</html>
