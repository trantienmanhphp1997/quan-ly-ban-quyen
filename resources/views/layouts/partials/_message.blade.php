
    <!-- Hiển thị thông báo thành công  -->
<!-- custom toastr -->
<link rel="stylesheet" href="{{asset('css/toastr.min.css')}}">

<script src="{{asset('js/toastr.min.js')}}"></script>
<script>
   toastr.options = {
    "closeButton": false,
    "debug": false,
    "progressBar": true,
    "positionClass": "toast-top-right",
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "3000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}
</script>
<!-- Hiển thị thông báo thành công  -->
@if ( Session::has('success'))
    <script>
        toastr.success("{{ Session::get('success') }}");
    </script>
    <?php
        session()->forget('success');
    ?>
@endif
<!-- Hiển thị thông báo lỗi  -->
@if ( Session::has('error'))
    <script>
        toastr.error("{{ Session::get('error') }}");
    </script>
    <?php
    session()->forget('error');
    ?>
@endif
@if ( Session::has('errorVersion2'))
        <?php
            $message = Session::get('errorVersion2'); 
            if(strlen($message)>200) {
                $message = substr($message, 0, 200);
                $message = $message.'...';
            }  
        ?>
    <script>
        toastr.error("{{ $message }}"); 
    </script>   
    @if(Session::has('phimLe'))
        <button type="button" style='display: none;' data-target="#modal-download-error-le" data-toggle="modal" id='btn-download-error-le'>download</button>
    @else 
        <button type="button" style='display: none;' data-target="#modal-download-error" data-toggle="modal" id='btn-download-error'>download</button>
    @endif    
    <?php
        session()->forget('errorVersion2');
    ?>
@endif

<!-- Hiển thị cảnh báo -->
@if ( Session::has('warning'))
    <div class="alert alert-warning alert-dismissible" role="alert" style="margin-bottom: 10px;">
        <strong>{{ Session::get('warning') }}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
        </button>
    </div>
    <?php session()->forget('warning');?>
@endif

