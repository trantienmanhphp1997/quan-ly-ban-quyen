@if(checkRole($model."-create-edit",$model."-delete"))
<div class="dropdown" style="display: inline-block;">
   <button class="btn btn-info btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background:#8080FF !important; border: 4px solid #8080FF; margin-top: 2px; outline: none;">
   <span class="menu-icon fa fa-plus" style="font-size: 18px;"></span>
   Tạo mới
   <span class="menu-icon fa fa-caret-down" style="font-size: 18px; margin-left: 2px;"></span>
   </button>
   <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton" style="min-width: 108px;">
      <div class="col-sm-12 div-hover" style="margin: 0px; padding: 0px; cursor: pointer;" onclick="getURL()">
         <a class="dropdown-item" id="a-create" style="color: black; margin-left: 5px; text-decoration: none;">
         Tạo mới<br>
         </a>
      </div>
      <div class="col-sm-12 div-hover" style="margin: 0px; padding: 0px; cursor: pointer;" data-target="#modal-form-upload-file" data-toggle="modal" id="div-upload">
         <a class="dropdown-item " id="a_upload" style="color: black; margin-left: 5px; text-decoration: none;">
         Tải lên từ Excel
         </a>
      </div>
   </div>
</div>
@endif
<script>
   function getURL() {
   	var url = window.location.href;
   	url = url.split('?')[0];
   	window.location.href = url + "/create";
   }
</script>
