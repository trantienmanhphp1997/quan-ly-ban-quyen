@if(checkRole($model."-create-edit",$model."-delete"))
<button type="button" class="btn-sm btn  btn-success" onclick="getCreateURL()">
<span class="menu-icon fa fa-plus" style="font-size: 18px;"></span> Thêm mới</button>
@endif
<script>
    function getCreateURL() {
    	var url = window.location.href;
    	url = url.split('?')[0];
		window.location.href = url + "/create";
    }
</script>
