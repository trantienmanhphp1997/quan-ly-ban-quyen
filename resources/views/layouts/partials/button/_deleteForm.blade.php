
@if(checkRole($model."-delete", null))
    <button title="Xóa" type='button' class="btn btn-xs btn-danger " style="margin-left: 0px;background: #f46a6a;border: none; padding: 2px 9px; margin-bottom: 3px;" data-target="#modal-form-delete-contract-{{$rs->id}}" data-toggle="modal">
    <i class="menu-icon fa fa-trash-o" style="color: white;font-size:16px;"></i>
        </button>
    <button id='delete-{{$rs->id}}' type="submit" style="display: none;"></button>

    <div class="modal fade" id="modal-form-delete-contract-{{$rs->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title" style="text-align: left;" id="exampleModalLabel">Xóa bản ghi</h2>
                </div>
                <div style="text-align: left;" class="modal-body">
                    Bạn có muốn xóa không? Thao tác này không thể phục hồi!
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Không</button>
                    <button type="button" class="btn btn-danger" onclick="document.getElementById('delete-{{$rs->id}}').click()">Xóa bỏ</button>
                </div>
            </div>
        </div>
    </div>
@endif
