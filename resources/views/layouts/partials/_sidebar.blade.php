<!-- #section:basics/sidebar -->
<div id="sidebar" class="sidebar responsive">
    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <button class="btn btn-success">
                <i class="ace-icon fa fa-signal"></i>
            </button>

            <button class="btn btn-info">
                <i class="ace-icon fa fa-pencil"></i>
            </button>

            <!-- #section:basics/sidebar.layout.shortcuts -->
            <button class="btn btn-warning">
                <i class="ace-icon fa fa-users"></i>
            </button>

            <button class="btn btn-danger">
                <i class="ace-icon fa fa-cogs"></i>
            </button>

            <!-- /section:basics/sidebar.layout.shortcuts -->
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>

            <span class="btn btn-info"></span>

            <span class="btn btn-warning"></span>

            <span class="btn btn-danger"></span>
        </div>
    </div><!-- /.sidebar-shortcuts -->

    <ul class="nav nav-list">
        <li {{ setActive('home') }}>
            <a href="{{url('home')}}">
                <i class="menu-icon fa fa-tachometer"></i>
                <span class="menu-text"> Dashboard </span>
            </a>

            <b class="arrow"></b>
        </li>
        @hasanyrole('administrator|contract-viettel-buy-list|contract-viettel-sale-list|contract-viettel-buy-create-edit|contract-viettel-sale-create-edit|contract-viettel-buy-delete|contract-viettel-sale-delete')
            <li {{ setOpen('contract') }}>
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-book"></i>
                    <span class="menu-text"> Quản lý hợp đồng</span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    @hasanyrole('administrator|contract-viettel-buy-list|contract-viettel-buy-create-edit|contract-viettel-buy-delete')
                        <li {{ setActive('contract/viettel-buy') }}>
                                <a href="{{url('contract/viettel-buy')}}">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Hợp đồng mua
                            </a>
                            <b class="arrow"></b>
                        </li>
                    @endhasanyrole
                    @hasanyrole('administrator|contract-viettel-sale-list|contract-viettel-sale-create-edit|contract-viettel-sale-delete')
                        <li {{ setActive('contract/viettel-sell') }}>
                                <a href="{{url('contract/viettel-sell')}}">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Hợp đồng bán
                            </a>
                            <b class="arrow"></b>
                        </li>
                    @endhasanyrole
                </ul>
            </li>
        @endhasanyrole
        @hasanyrole('administrator|film-list|music-list|phim-le-list|contract-media-sale-money-list|manage-list|film-create-edit|music-create-edit|phim-le-create-edit|contract-media-sale-money-create-edit|manage-create-edit|film-delete|music-delete|phim-le-delete|contract-media-sale-money-delete|manage-delete')
            <li {{ setOpen('media') }}>
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-desktop"></i>
                    <span class="menu-text"> Sản phẩm</span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>
                <ul class="submenu">
                    @hasanyrole('administrator|film-list|film-create-edit|film-delete')
                        <li {{ setActive('media/films') }}>
                            <a href="{{route('media.films')}}">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Danh sách phim
                            </a>
                            <b class="arrow"></b>
                        </li>
                    @endhasanyrole
                    @hasanyrole('administrator|phim-le-list|phim-le-create-edit|phim-le-delete')
                        <li {{ setActive('media/phim-le') }}>
                            <a href="{{route('media.phim-le')}}">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Danh sách tập phim
                            </a>
                            <b class="arrow"></b>
                        </li>
                    @endhasanyrole
                    @hasanyrole('administrator|music-list|music-create-edit|music-delete')
                        <li {{ setActive('media/music') }}>
                            <a href="{{route('media.music.index')}}">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Danh sách nhạc
                            </a>
                            <b class="arrow"></b>
                        </li>
                    @endhasanyrole
                    @hasanyrole('administrator|contract-media-sale-money-list|contract-media-sale-money-create-edit|contract-media-sale-money-delete')
                        <li {{ setActive('media/contract-media-sale-money') }}>
                            <a href="{{route('media.contract-media-sale-money')}}">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Sản phẩm bán
                            </a>
                            <b class="arrow"></b>
                        </li>
                    @endhasanyrole
                    @hasanyrole('administrator|manage-list|manage-create-edit|manage-delete')
                        <li {{ setActive('media/manage') }}>
                            <a href="{{route('media.manage')}}">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Quản trị tư liệu
                            </a>
                            <b class="arrow"></b>
                        </li>
                    @endhasanyrole
                </ul>
            </li>
        @endhasanyrole
        @hasanyrole('administrator|partner-list|list-employee-list|list-employee-create-edit|list-employee-delete|partner-create-edit|partner-delete|revenue-fee-list|revenue-fee-create-edit|revenue-fee-delete|revenue-type-list|revenue-type-create-edit|revenue-type-delete|media-role-list|media-role-create-edit|media-role-delete|countries-list|countries-create-edit|countries-delete|contract-file-list|contract-file-create-edit|contract-file-delete|contract-partner-list|contract-partner-create-edit|contract-partner-delete|contract-category-list|contract-category-create-edit|contract-category-delete')
            <li  {{ setOpen('management') }}>
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-users"></i>
                    <span class="menu-text"> Quản lý chung</span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>
                <ul class="submenu">
                    <!-- 1 -->
                    @hasanyrole('administrator|contract-file-list|contract-file-create-edit|contract-file-delete')
                        <li {{ setActive('management/contract/file') }}>
                            <a href="{{route('contract.file')}}">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Danh sách giấy tờ
                            </a>
                            <b class="arrow"></b>
                        </li>
                    @endhasanyrole
                    <!-- 2 -->
                    @hasanyrole('administrator|contract-partner-list|contract-partner-create-edit|contract-partner-delete')
                        <li {{ setActive('management/contract/viettel-partner') }}>
                                <a href="{{route('contract.viettel-partner')}}">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Hợp đồng đối tác
                            </a>
                            <b class="arrow"></b>
                        </li>
                    @endhasanyrole
                    @hasanyrole('administrator|partner-list|partner-create-edit|partner-delete')
                    <li {{ setActive('management/partner/list') }}>
                        <a href="{{route('partner.index')}}">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Danh sách đối tác
                        </a>
                        <b class="arrow"></b>
                    </li>
                    @endhasanyrole
                    @hasanyrole('administrator|list-employee-list|list-employee-create-edit|list-employee-delete')
                        <li {{ setActive('management/partner/contractOfficer') }}>
                            <a href="{{route('admin.businessPartner.listEmployee')}}">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Danh sách nhân viên phụ trách hợp đồng
                            </a>
                            <b class="arrow"></b>
                        </li>
                    @endhasanyrole
                    <!-- 3 -->
                    @hasanyrole('administrator|contract-category-list|contract-category-create-edit|contract-category-delete')
                        <li {{ setActive('management/contract/category') }}>
                                <a href="{{route('contract.category')}}">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Loại hợp đồng
                            </a>
                            <b class="arrow"></b>
                        </li>
                    @endhasanyrole
                    @hasanyrole('administrator|media-role-list|media-role-create-edit|media-role-delete')
                        <li {{setActive('management/media/role')}}>
                            <a href="{{route('media.role.index')}}">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Quyền hạn - Hạ tầng
                            </a>
                            <b class="arrow"></b>
                        </li>
                    @endhasanyrole
                    @hasanyrole('administrator|countries-list|countries-create-edit|countries-delete')
                        <li {{setActive('management/media/countries')}}>
                            <a href="{{route('media.countries')}}">
                            <i class="menu-icon fa fa-caret-right"></i>
                                Danh sách quốc gia
                            </a>
                            <b class="arrow"></b>
                        </li>
                    @endhasanyrole
                    @hasanyrole('administrator|revenue-fee-list|revenue-fee-create-edit|revenue-fee-delete')
                        <li {{ setActive('management/revenue/fee') }}>
                            <a href="{{route('type_fee.index')}}">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Loại chi phí nhạc
                            </a>
                            <b class="arrow"></b>
                        </li>
                    @endhasanyrole
                    @hasanyrole('administrator|revenue-type-list|revenue-type-create-edit|revenue-type-delete')
                        <li {{ setActive('management/revenue/type') }}>
                            <a href="{{route('revenue_type.index')}}">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Loại doanh thu phim
                            </a>
                            <b class="arrow"></b>
                        </li>
                    @endhasanyrole
                </ul>
            </li>
        @endhasanyrole
        @hasanyrole('administrator')
        <li {{ setOpen('tool') }}>
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-cogs"></i>
                <span class="menu-text">Quản trị hệ thống </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
    
                <li {{ setActive('tool/roles') }}>
                    <a href="{{route('tool.roles')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Danh sách vai trò
                    </a>

                    <b class="arrow"></b>
                </li>
                <li {{ setActive('tool/createAdmin') }}>
                    <a href="{{route('createAdmin.index')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Quản lý người dùng
                    </a>
                    <b class="arrow"></b>
                </li>

                <li {{ setActive('tool/action_log') }}>
                    <a href="{{route('action_log')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Lịch sử tác động
                    </a>

                    <b class="arrow"></b>
                </li>
   
            </ul>
        </li>
        @endhasanyrole
        @hasanyrole('administrator|revenue-film-list|revenue-music-list|revenue-film-create-edit|revenue-music-create-edit|revenue-film-delete|revenue-music-delete')
            <li {{ setOpen('revenue') }}>
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-pencil-square-o"></i>
                    <span class="menu-text"> Báo cáo thống kê </span>

                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    @hasanyrole('administrator|revenue-film-list|revenue-film-create-edit|revenue-film-delete')
                        <li {{ setActive('revenue/film') }}>
                            <a href="{{route('revenue.film')}}">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Thống kê doanh thu phim
                            </a>
                            <b class="arrow"></b>
                        </li>
                    @endhasanyrole
                    @hasanyrole('administrator|revenue-music-list|revenue-music-create-edit|revenue-music-delete')
                        <li {{ setActive('revenue/music') }}>
                            <a href="{{route('revenue.music')}}">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Thống kê doanh thu nhạc
                            </a>

                        </li>
                    @endhasanyrole
                </ul>
            </li>
        @endhasanyrole


        <li {{ setOpen('copyright') }}>
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-copyright"></i>
                <span class="menu-text"> Theo dõi bản quyền </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
            <ul class="submenu">
                    <li {{ setActive('copyright') }}>
                        <a href="{{route('copyright.index')}}">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Export file
                        </a>
                        <b class="arrow"></b>
                    </li>
            </ul>
        </li>
    </ul><!-- /.nav-list -->

    <!-- #section:basics/sidebar.layout.minimize -->
    <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
        <i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
    </div>

    <!-- /section:basics/sidebar.layout.minimize -->
{{--     <script type="text/javascript">
        $("document").ready(function() {
            var ul = $('.submenu')
            for(var i=0; i<ul.length; i++){
                var parent = $(ul[i]).parent()[0]
                $(parent).addClass('open')
            }

        })
    </script> --}}
</div>
<!-- /section:basics/sidebar -->
