
<!-- basic scripts -->

<!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='../assets/js/jquery.js'>"+"<"+"/script>");
</script>

<!-- <![endif]-->

<!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='../assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
<script type="text/javascript">
    if('ontouchstart' in document.documentElement) document.write("<script src='../assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
</script>
<script src="{!! asset('assets/js/bootstrap.js') !!}"></script>
<!-- page specific plugin scripts -->

<!--[if lte IE 8]>
<script src="{!! asset('assets/js/excanvas.js') !!}"></script>
<![endif]-->
<!-- <script src="{!! asset('assets/js/jquery-ui.custom.js') !!}"></script>
<script src="{!! asset('assets/js/jquery.ui.touch-punch.js') !!}"></script>
<script src="{!! asset('assets/js/jquery.easypiechart.js') !!}"></script>
<script src="{!! asset('assets/js/jquery.sparkline.js') !!}"></script>
<script src="{!! asset('assets/js/flot/jquery.flot.js') !!}"></script>
<script src="{!! asset('assets/js/flot/jquery.flot.pie.js') !!}"></script>
<script src="{!! asset('assets/js/flot/jquery.flot.resize.js') !!}"></script>
<script src="{!! asset('assets/js/chosen.jquery.js') !!}"></script>
<script src="{!! asset('assets/js/fuelux/fuelux.spinner.js') !!}"></script>
<script src="{!! asset('assets/js/date-time/bootstrap-datepicker.js') !!}"></script>
<script src="{!! asset('assets/js/date-time/bootstrap-timepicker.js') !!}"></script>
<script src="{!! asset('assets/js/date-time/moment.js') !!}"></script>
<script src="{!! asset('assets/js/date-time/daterangepicker.js') !!}"></script>
<script src="{!! asset('assets/js/bootstrap-colorpicker.js') !!}"></script>
<script src="{!! asset('assets/js/jquery.knob.js') !!}"></script>
<script src="{!! asset('assets/js/jquery.autosize.js') !!}"></script>
<script src="{!! asset('assets/js/jquery.inputlimiter.1.3.1.js') !!}"></script>
<script src="{!! asset('assets/js/jquery.maskedinput.js') !!}"></script>
<script src="{!! asset('assets/js/bootstrap-tag.js') !!}"></script> -->

<!-- ace scripts -->
<!-- <script src="{!! asset('assets/js/ace/elements.scroller.js') !!}"></script>
<script src="{!! asset('assets/js/ace/elements.colorpicker.js') !!}"></script>
<script src="{!! asset('assets/js/ace/elements.fileinput.js') !!}"></script>
<script src="{!! asset('assets/js/ace/elements.typeahead.js') !!}"></script>
<script src="{!! asset('assets/js/ace/elements.wysiwyg.js') !!}"></script>
<script src="{!! asset('assets/js/ace/elements.spinner.js') !!}"></script>
<script src="{!! asset('assets/js/ace/elements.treeview.js') !!}"></script>
<script src="{!! asset('assets/js/ace/elements.wizard.js') !!}"></script>
<script src="{!! asset('assets/js/ace/elements.aside.js') !!}"></script> -->
<script src="{!! asset('assets/js/ace/ace.js') !!}"></script>
<script src="{!! asset('assets/js/ace/ace.ajax-content.js') !!}"></script>
<script src="{!! asset('assets/js/ace/ace.touch-drag.js') !!}"></script>
<script src="{!! asset('assets/js/ace/ace.sidebar.js') !!}"></script>
<script src="{!! asset('assets/js/ace/ace.sidebar-scroll-1.js') !!}"></script>
<script src="{!! asset('assets/js/ace/ace.submenu-hover.js') !!}"></script>
<script src="{!! asset('assets/js/ace/ace.widget-box.js') !!}"></script>
<script src="{!! asset('assets/js/ace/ace.settings.js') !!}"></script>
<script src="{!! asset('assets/js/ace/ace.settings-rtl.js') !!}"></script>
<script src="{!! asset('assets/js/ace/ace.settings-skin.js') !!}"></script>
<script src="{!! asset('assets/js/ace/ace.widget-on-reload.js') !!}"></script>
<script src="{!! asset('assets/js/ace/ace.searchbox-autocomplete.js') !!}"></script>
<script src="{!! asset('assets/js/date-time/bootstrap-datepicker.js') !!}"></script>
<script src="{!! asset('assets/js/date-time/bootstrap-timepicker.js') !!}"></script>

<!-- <script src="{!! asset('assets/tinymce/js/tinymce/tinymce.min.js') !!}"></script> -->
<script src="{!! asset('assets/js/ace-extra.js') !!}"></script>
<!-- <script src="{{asset('js/uploadFile.js')}}"></script> -->
<!-- <script src="{{asset('js/jquery.min.js')}}"></script> -->
<script src="{{ asset('js/contract_file.js') }}"></script>
<script src="{{asset('assets/select2/select2.min.js')}}"></script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<!-- the following scripts are used in demo only for onpage help and you don't need them -->
<!-- <link rel="stylesheet" href="{!! asset('assets/css/ace.onpage-help.css') !!}" />
<link rel="stylesheet" href="{!! asset('docs/assets/js/themes/sunburst.css') !!}" />

<script type="text/javascript"> ace.vars['base'] = '..'; </script>
<script src="{!! asset('assets/js/ace/elements.onpage-help.js') !!}"></script>
<script src="{!! asset('assets/js/ace/ace.onpage-help.js') !!}"></script>
<script src="{!! asset('docs/assets/js/rainbow.js') !!}"></script>
<script src="{!! asset('docs/assets/js/language/generic.js') !!}"></script>
<script src="{!! asset('docs/assets/js/language/html.js') !!}"></script>
<script src="{!! asset('docs/assets/js/language/css.js') !!}"></script>
<script src="{!! asset('docs/assets/js/language/javascript.js') !!}"></script> -->