<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <link rel="icon" href="data:,">

    <meta name="description" content="overview &amp; stats" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="{!! asset('assets/css/bootstrap.css') !!}" />
    <link rel="stylesheet" href="{!! asset('assets/css/font-awesome.css') !!}" />

    <!-- page specific plugin styles -->

    <!-- text fonts -->
    <link rel="stylesheet" href="{!! asset('assets/css/ace-fonts.css') !!}" />

    <!-- ace styles -->
    <link rel="stylesheet" href="{!! asset('assets/css/ace.css') !!}" class="ace-main-stylesheet" id="main-ace-style" />
    @livewireStyles
    <!--[if lte IE 9]>
    <link rel="stylesheet" href="{!! asset('assets/css/ace-part2.css') !!}" class="ace-main-stylesheet" />
    <![endif]-->

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="{!! asset('assets/css/ace-ie.css') !!}" />
    <![endif]-->

    <link rel="stylesheet" href="{!! asset('css/style.css') !!}" />

    <!-- inline styles related to this page -->

    <!-- ace settings handler -->
    <script src="{!! asset('assets/js/ace-extra.js') !!}"></script>

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

    <link rel="stylesheet" href="{!! asset('assets/css/jquery-ui.custom.css') !!}" />
    <!-- <link rel="stylesheet" href="{!! asset('assets/css/chosen.css') !!}" /> -->
<!--     <link rel="stylesheet" href="{!! asset('assets/css/datepicker.css') !!}" />
    <link rel="stylesheet" href="{!! asset('assets/css/bootstrap-timepicker.css') !!}" />
    <link rel="stylesheet" href="{!! asset('assets/css/daterangepicker.css') !!}" />
    <link rel="stylesheet" href="{!! asset('assets/css/bootstrap-datetimepicker.css') !!}" />
    <link rel="stylesheet" href="{!! asset('assets/css/colorpicker.css') !!}" /> -->
    
    <script src="{{asset('js/quanlybanquyen.js')}}"></script>
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <link rel="stylesheet" href="{{asset('assets/select2/select2.min.css')}}" >

    <!--import thư viện highcharts-->
    <script src="{{asset('js/highchart/highchart.js')}}"></script>
    <script src="{{asset('js/highchart/modules/exporting.js')}}"></script>
    <script src="{{asset('js/highchart/modules/export-data.js')}}"></script>
    <script src="{{asset('js/highchart/modules/accessibility.js')}}"></script>
    <link rel="stylesheet" href="{!! asset('assets/css/highcharts.css') !!}" >
   
    <!--[if lte IE 8]>
    <script src="{!! asset('assets/js/html5shiv.js') !!}"></script>
    <script src="{!! asset('assets/js/respond.js') !!}"></script>
    <![endif]-->
</head>