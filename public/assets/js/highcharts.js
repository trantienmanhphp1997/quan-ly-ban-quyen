document.addEventListener('DOMContentLoaded', function(){ 
  window.onload=function(){

    Livewire.emit('getRevenueFilmData'),
    Livewire.on('showRevenueFilmData',function(revenueDay){
  //     let revenueData=JSON.parse(document.getElementById('revenue-data').value);
  // console.log(revenueData);
  Highcharts.chart('revenue-chart', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Biểu đồ doanh thu phim các ngày qua'
      },
      subtitle: {
        text: ''
      },
      xAxis: {
        title: {
          text: 'Thời gian bán phim'
        },
        categories: revenueDay.map(item => item.date_modified),
        type: "date",
       
        // labels: {
        //   formatter: function() {
        //     return Highcharts.dateFormat('%b/%e/%Y', this.value);
        //   }
        // },
        crosshair: true
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Doanh thu ( nghìn VND)'
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y} VND</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0
        }
      },
      series: [{
        name: 'Doanh thu ngày',
        data: revenueDay.map(item => Number(item.sum_money))
    
      // }, {
      //   name: 'New York',
      //   data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0]
    
      // }, {
      //   name: 'London',
      //   data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0]
    
      // }, {
      //   name: 'Berlin',
      //   data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4]
    
      }]
    });
    })

  }
})



