
document.addEventListener('DOMContentLoaded', function(){
	$('#btn-save-file').click(function(){
		var fileUpload = $('#file-upload').val()
		if(fileUpload==''||fileUpload==null){
			$('#text-error').text('*Chưa có file');
		}
		else {
			$(this).attr('disabled', true)
			var formUploadFile = document.forms['form-upload-file']
			formUploadFile.submit()
		}
	})
	$('#btn-save-file-backup').click(function(){
		var fileUpload = $('#file-upload').val()
		if(fileUpload==''||fileUpload==null){
			$('#text-error').text('*Chưa có file');
		}
		else {
			$(this).attr('disabled', true)
			var formUploadFile = document.forms['form-upload-file']
			formUploadFile.submit()
		}
	})
	$('#btn-save-list-film').click(function(){
		var fileListFilm = $('#file-upload-list-film').val()
		if(fileListFilm==''||fileListFilm==null){
			$('#text-error-list-film').text('*Chưa có file');
		}
		else {
			$(this).attr('disabled', true)
			var forms = $('#div-form-upload-list-film').children();
			var count = forms.length
			for(var i = 0; i < count; i++){
				if($(forms[i]).attr('name')==filmID){
					var a =$('#file-upload-list-film')
					$(forms[i]).append(a)
					$(forms[i]).submit();
					break;
				}
			}
		}
	})
	$('#file-upload').change(function(){
		var fileUpload = $('#file-upload').val()
		if(fileUpload==''||fileUpload==null){
			$('#text-error').text('*Chưa có file');
		}
		else {
			$('#text-error').text('');
		}
	})

	$('#addEmployees').click(function(){
		$('#text-error').text('');
	})

	$('#btn-export-file').click(function(){
		window.location = '/listEmployee/export';
	})
	$('#btn-submit-create-film').click(function(){
		$(this).attr('disabled', true)
		var formCreateFilm = document.forms['form-create-film']
		formCreateFilm.submit()
	})

	$('#btn-submit-create-music').click(function(){
		$(this).attr('disabled', true)
		var formCreateFilm = document.forms['form-create-music']
		formCreateFilm.submit()
	})
	var filmID
	$('.btn-upload-list-film').click(function(){
		$('#text-error-list-film').text('');
		filmID = $(this).attr('data-id');
	})
})
