document.addEventListener('DOMContentLoaded', function(){	
	$('.checkboxClick').change(function(){
		var isChecked = $(this).prop('checked')
		if(isChecked){
			var tr = $(this).closest('tr');
			$(tr).css('background-color','#FF6600');
		}
		else {
			var tr = $(this).closest('tr');
			$(tr).css('background-color','#fff');
		}
	})

	$('#btn-submit-search-QLBQ').click(function(e){
		e.preventDefault();
		var formSearchQLBQ = document.forms['form-search-QLBQ']
		formSearchQLBQ.submit()
	})

	$('.div-hover').hover(function(){
		var elementA = $(this).children()[0];
		$(elementA).css('color','white')
	})

	$('.div-hover').mouseleave(function(){
		var elementA = $(this).children()[0];
		$(elementA).css('color','black')
	})
	$('#a_upload').click(function(e){
		e.preventDefault();
	})
	var filmID;
	var musicID;
    $('.btn-delete-film').click(function(){
    	filmID = $(this).attr('data-id');
    }) 
   	$('.btn-delete-music').click(function(){
    	musicID = $(this).attr('data-id');
    })
	$('#btn-delete-film').click(function(){
		var forms = $('#div-form-delelte').children();
		var count = forms.length
		for(var i = 0; i < count; i++){
			if($(forms[i]).attr('name')==filmID){
				$(forms[i]).submit();
				break;
			}
		}
	})
	$('#btn-delete-music').click(function(){
		var forms = $('#div-form-delelte').children();
		var count = forms.length
		for(var i = 0; i < count; i++){
			if($(forms[i]).attr('name')==musicID){
				$(forms[i]).submit();
				break;
			}
		}
	})

	$('#btn-delete-all-film').click(function(){
		if($('input[name="filmIds[]"]:checked').length>0){
			var forms = document.forms['form-delete-all-film']
			$(forms).submit()
		}
		else {
			$('#modal-p-delete-all-film').text('*Chưa hàng nào được tích')
		}
	})

	$('#btn-delete-all-film_1').click(function(){
		$('#modal-p-delete-all-film').text('')
	})

	$('#div-upload').click(function(){
		$('#text-error').text('');
	})
	$('#btn-upload-film').click(function(){
		var forms = document.forms['form-export-film']
		$(forms).submit()		
	})
	$('#btn-upload-film-backup').click(function(){
		var forms = document.forms['form-export-film-backup']
		$(forms).submit()		
	})


	$('.btn-edit-film').click(function(){
		var forms = $('#div-form-edit').children();
		var count = forms.length
		var filmIDEdit = $(this).attr('filmID')
		for(var i = 0; i < count; i++){
			if($(forms[i]).attr('name')==filmIDEdit){
				$(forms[i]).submit();
				break;
			}
		}
	})

	$('.btn-edit-music').click(function(){
		var forms = $('#div-form-edit').children();
		var count = forms.length
		var musicIDEdit = $(this).attr('musicID')
		for(var i = 0; i < count; i++){
			if($(forms[i]).attr('name')==musicIDEdit){
				$(forms[i]).submit();
				break;
			}
		}
	})


})