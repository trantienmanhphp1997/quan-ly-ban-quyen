$("document").ready(function() {
    var contractFile_id;
    var selected_music_id;
    $('.delete-button').click(function(){
        contractFile_id = $(this).attr('data-id')
    })
    // $(".delete_contract_file").click(function(){
    //     let url = "/contract/file"
    //     let token = $('#_token').val();
    //     $.ajax({
    //         type: "DELETE",
    //         url: url,
    //         data: {
    //             id: contractFile_id,
    //             contract_id: $('#contract_id').val(),
    //             _token: $('#_token').val(),
    //         },
    //         success: function(data) {
    //             $(this).closest("tr").remove();
    //             // var div = `<div class="alert alert-success">
    //             //   <p>Deleted Successfully</p>
    //             // </div>`
    //             // $("#contract_file").html(data);
    //             // $('#contract_file').prepend(div);
    //         },
    //     });
    // })
    // $('#btn-submit-get-error').click(function(e){
    //     e.preventDefault();
    //     var data = $('#error-upload-music').val()
    //     window.livewire.emit('getError', data);
    //     // var formCreateFilm = document.forms['form-get-error-upload']
    //     // formCreateFilm.submit()
    // })
    $('#upload-form').submit(function(e) {
        e.preventDefault();
        let formData = new FormData(this);
        let contract_id = $('#contract_id').val();
        let url = "/contract/file/"+contract_id;
        let token = $('#_token').val();
        $.ajax({
            type: "POST",
            url: url,
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                var div = `<div class="alert alert-success">
                  <p>Created Successfully</p>
                </div>`
                // $("#contract_file").html(data);
                // $('#contract_file').prepend(div);
            },
        });
    })
    window.addEventListener('add-media', event => {
        alert('update');
        // ajax hiển thị dữ liệu
        $("#exampleModal2").hide();
    });
    window.livewire.on('close-contract-file-modal', () => {
        document.getElementById('close-contract-file-modal').click()
    });
    window.livewire.on('close-contract-file-modal', () => {
        $('#close-modal-exampleModalMusicSale').hide()
    });
    
    window.livewire.on('close-contract-file-modal-edit', () => {
        document.getElementById('close-contract-file-modal-edit').click()
    });
    window.livewire.on('close-update-music-modal', () => {
        document.getElementById('close-update-music-modal').click()
    });
    window.livewire.on('close-media-music-modal', () => {
        document.getElementById('close-media-music-modal').click()
        // document.getElementById('money').value= ''
        // document.getElementById('cho_tac_quyen').checked= false
        // document.getElementById('so_doc_quyen').checked= false
        // document.getElementById('so_tac_quyen').checked= false
        // document.getElementById('cho_doc_quyen').checked= false
        // document.getElementById('sale_3rd').checked= false
    });
    window.livewire.on('close-media-music-modal-sale', () => {
        document.getElementById('close-media-music-modal').click()
        document.getElementById('name_khong_dau').value= ''
        document.getElementById('singer_khong_dau').value= ''
        document.getElementById('ma_nhac_cho').value= ''
        document.getElementById('id-date-picker-1 start_time').value= ''
        document.getElementById('id-date-picker-1 end_time').value= ''
        document.getElementById('cho_tac_quyen').checked= false
        document.getElementById('so_doc_quyen').checked= false
        document.getElementById('so_tac_quyen').checked= false
        document.getElementById('cho_doc_quyen').checked= false
    });
    window.livewire.on('close-media-music-modal2', () => {
        document.getElementById('close-media-music-modal2').click()
    });
    window.livewire.on('close-media-film-modal', () => {
        document.getElementById('close-media-music-modal').click()
    });
    window.livewire.on('close-media-music-modal-edit', () => {
        document.getElementById('close-media-music-modal-edit').click()
    });
    window.livewire.on('setEndTime', (endTime) => {
        document.getElementById('id-date-picker-1 end_time1').value =  endTime;
    });
    window.livewire.on('setTimeMusicSale', (startTime, endTime) => {
        document.getElementById('id-date-picker-1 start_time_edit').value =  startTime;
        document.getElementById('id-date-picker-1 end_time_edit').value =  endTime;
    });
    window.livewire.on('setTimeFilmSale', (startTime, endTime) => {
        document.getElementById('id-date-picker-1 start_time_edit').value =  startTime;
        document.getElementById('id-date-picker-1 end_time_edit').value =  endTime;
    });
    window.livewire.on('close-media-film2-modal', () => {
        document.getElementById('close-modal').click()
        document.getElementById('money').value =''
        document.getElementById('moneyVAT').value =''
        document.getElementById('count_live').value =''
        document.getElementById('country').value =''
        document.getElementById('id-date-picker-1 start_time').value =''
        document.getElementById('id-date-picker-1 end_time').value =''
        document.getElementById('note_nuoc').value =''
        document.getElementById('note_film').value =''
    });
    window.livewire.on('close-media-film2-modal-edit', () => {
        document.getElementById('close-modal_edit').click()
        // document.getElementById('money').value =''
        // document.getElementById('count_live').value =''
        // document.getElementById('country').value =''
        // document.getElementById('id-date-picker-1 start_time').value =''
        // document.getElementById('id-date-picker-1 end_time').value =''
    });
    window.livewire.on('close-media-film3-modal', () => {
        document.getElementById('close-media-film-modal').click()
        document.getElementById('id-date-picker-1 start_time2').value =''
        document.getElementById('money_revenue').value =''
        document.getElementById('id-date-picker-1 end_time1').value =''
    });
    window.livewire.on('clear_check_box_buy', () => {
        $('#select_box :selected').remove();
        $("#select_box").select2("val",'abc');
    });
    window.livewire.on('clear_check_box_sale', () => {
        $("#select_box").select2("val",'abc');
    });
    window.livewire.on('setTitleCountry', () => {
        // alert('vào');
        $('span#select2-select_box-container').html('--- Chọn tất cả ---')
        $('span#select2-select_box-container').attr('title','--- Chọn tất cả ---')
    });
    window.livewire.on('close-modal', () => {
        $('#close-media-film-modal').click()
        document.getElementById('money_revenue').value = ''
    });
    window.livewire.on('close-modal1', () => {
        $('#close-media-film-modal').click()
    });
    window.addEventListener('show-toast', event => {
        if(event.detail.type == "success"){
            toastr.success(event.detail.message);
        } else if(event.detail.type  == "error"){
            toastr.error(event.detail.message);
        }
    });
    window.addEventListener('setSelect2', event => {
        $(".select_box").select2({
            placeholder: "Chọn bài hát",
            allowClear: true,
            dropdownAutoWidth: true
        });

    });
    window.addEventListener('setSelect2Film', event => {

        $(".select_box_fim").select2({
            placeholder: "Chọn phim",
            allowClear: true,
            dropdownAutoWidth: true
        });            
        $(".select_role").select2({
            placeholder: 'Chọn quyền...',
            allowClear:true
        });
        $(".select_role2").select2({
            placeholder: 'Chọn hạ tầng...',
            allowClear:true
        });
        $(".select_box").select2({
            placeholder: 'Chọn...',
            allowClear:true
        });
        $(".select_country").select2({
            placeholder: 'Chọn quốc gia...',
            allowClear:true
        });   
    });
    window.addEventListener('setDatepicker', event => {
        $('.date-picker').datepicker({
            autoclose: true,
            todayHighlight: true,
            showOtherMonths: true,
            selectOtherMonths: false
        });
    });
    $(".select_box").select2({
        placeholder: "Chọn bài hát",
        allowClear: true,
        dropdownAutoWidth: true
    });
    $(".select_box_fim").select2({
        placeholder: "Chọn phim",
        allowClear: true,
        dropdownAutoWidth: true
    });
    $(".select_box_partner").select2({
        placeholder: "---Chọn hoặc tạo mới đối tác---",
        tags: true
    });

    $(".select_partner").select2({
        placeholder: "Chọn đối tác cung cấp ...",
        matcher: function(params, data) {
            if ($.trim(params.term) === '') {
                return data;
            }
            if (typeof data.text === 'undefined') {
                return null;
            }
            dataText = nonAccentVietnamese(data.text)
            paramTerm = nonAccentVietnamese(params.term)
            if (dataText.toLowerCase().indexOf(jQuery.trim(paramTerm).toLowerCase()) > -1) {
                var modifiedData = $.extend({}, data, true);
                return modifiedData;
            }
            return null;
        }
    });
    $(".select_contract_number").select2({
        placeholder: "Chọn số hợp đồng ...",
        // tags: true,
        matcher: function(params, data) {
            if ($.trim(params.term) === '') {
                return data;
            }
            if (typeof data.text === 'undefined') {
                return null;
            }
            dataText = nonAccentVietnamese(data.text)
            paramTerm = nonAccentVietnamese(params.term)
            if (dataText.toLowerCase().indexOf(jQuery.trim(paramTerm).toLowerCase()) > -1) {
                var modifiedData = $.extend({}, data, true);
                return modifiedData;
            }
            return null;
        }
    });
    $(".select_country_list_film").select2({
        placeholder: "Chọn quốc gia sản xuất",
        // tags: true,
        matcher: function(params, data) {
            if ($.trim(params.term) === '') {
                return data;
            }
            if (typeof data.text === 'undefined') {
                return null;
            }
            dataText = nonAccentVietnamese(data.text)
            paramTerm = nonAccentVietnamese(params.term)
            if (dataText.toLowerCase().indexOf(jQuery.trim(paramTerm).toLowerCase()) > -1) {
                var modifiedData = $.extend({}, data, true);
                return modifiedData;
            }
            return null;
        }
    });
    function nonAccentVietnamese(str) {
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        // Some system encode vietnamese combining accent as individual utf-8 characters
        str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // Huyền sắc hỏi ngã nặng 
        str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // Â, Ê, Ă, Ơ, Ư
        return str;
    }
    $(".select_country_film").select2({
        placeholder: "Chọn quốc gia...",
    });
    $(".select_country_partner").select2({
        placeholder: "Chọn quốc gia...",
    });

    window.livewire.on('resetFileInput', () => {
        var fileUpload = $('#file_upload')
        $(fileUpload).val('')
        // var parent = $(fileUpload).parent()[0]
        // var childrens = $(parent).children()
        // $(childrens[2]).remove()
        // var fileName = $('#file_name')
        // parent = $(fileName).parent()[0]
        // childrens = $(parent).children()
        // $(childrens[1]).remove()
    });

    $('#btn-save').click(function(e){
        var fileUpload = $('#file_upload')
        if($(fileUpload).val()){
            $('#btn-save2').click()
        }
        else {
            $('#btn-save3').click()
        }
    })
    // $("input.format_number").on({
    //     // input: function(){
    //     //     $(this).val($(this).val().replace(/[^0-9,.]/gi, ''));
    //     // },
    //     keyup: function () {
    //         formatCurrency($(this));
    //     },
    //     // blur: function () {
    //     //     formatCurrency($(this));
    //     // }
    // });
})



function formatNumber(n) {
    return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}
function formatNumber2(n) {
    return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, "")
}

function formatCurrency(input, blur) {
    // appends $ to value, validates decimal side
    // and puts cursor back in right position.

    // get input value
    let input_val = input.val();
    // don't validate empty input
    if (input_val === "") { return; }

    // original length
    let original_len = input_val.length;
    // initial caret position
    let caret_pos = input.prop("selectionStart");
    if(input_val.length>1&&input_val[0]==0&&input_val[1]=='.'){

    }
    else if(input_val.length>1&&input_val[0]=='0'){
        input_val = input_val.substring(1)
    }
    // check for decimal
    if (input_val.indexOf(".") >= 0) {
        // get position of first decimal
        // this prevents multiple decimals from
        // being entered
        let decimal_pos = input_val.indexOf(".");

        // split number by decimal point
        let left_side = input_val.substring(0, decimal_pos);
        let right_side = input_val.substring(decimal_pos);

        // add commas to left side of number
        left_side = formatNumber(left_side);
        // add commas to left side of number
        right_side = formatNumber2(right_side);
        // On blur make sure 3 numbers after decimal
        if (blur === "blur") {
            right_side += "000";
        }
        // alert(right_side)
        // Limit decimal to only 3 digits
        right_side = right_side.substring(0, 3);
        // join number by .
        input_val = left_side + "." + right_side;

    } else {
        // no decimal entered
        // add commas to number
        // remove all non-digits
        input_val = formatNumber(input_val);
        input_val = input_val;

        // final formatting
        if (blur === "blur") {
            input_val += ".000";
        }
    }
    // send updated string to input
    input.val(input_val);
    // put caret back in the right position
    let updated_len = input_val.length;
    caret_pos = updated_len - original_len + caret_pos;
    input[0].setSelectionRange(caret_pos, caret_pos);
}

