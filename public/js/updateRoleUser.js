document.addEventListener('DOMContentLoaded', function(){
    $('.btn-show').click(function(){
        var parent =$(this).parent()[0];
        var childrens  =$(parent).children();
        var count =childrens.length
        var countRole =childrens[count-2].value
        countRole = parseInt(countRole)
        $('#div-submit').empty()
        for(var i = countRole-1; i >= 0; i--){
            var roleName =childrens[count-i-3].value
            var roleNote =childrens[count-i-3].getAttribute("note");
            var html =`
            <tr class="unread checked">
            <td class="hidden-xs">
            <input type="checkbox" class="input-checkbox" name="role_name[]"
            checked="true" value="${roleName}" class="checkbox">
            </td>
            <td class="hidden-xs">&nbsp;
            ${roleNote}
            </td>
            </tr>`
            $('#div-submit').append(html)
        }

        var countRoleDontBelongTo =childrens[count-1].value
        countRoleDontBelongTo = parseInt(countRoleDontBelongTo)
        for(var i = countRoleDontBelongTo-1; i >= 0; i--){
            var roleDontBelongToName =childrens[count-i-3-countRole].value
            var roleDontBelongToNote =childrens[count-i-3-countRole].getAttribute("note");
            var html =`
            <tr class="unread checked">
            <td class="hidden-xs">
            <input type="checkbox" class="input-checkbox" name="role_name[]"
            value="${roleDontBelongToName}" class="checkbox">
            </td>
            <td class="hidden-xs">&nbsp;
            ${roleDontBelongToNote}
            </td>
            </tr>`
            $('#div-submit').append(html)
        }
        var userId = childrens[count-3-countRole-countRoleDontBelongTo].value
        var html =`<input type='hidden' value='${userId}' name='id'></input>`
        $('#div-submit').append(html)
    })
})

