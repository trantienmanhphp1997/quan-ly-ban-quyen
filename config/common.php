<?php
return [
    "groups" => [
        "admin" => [
            "id" => 1,
            "code" => "admin",
            "display_name" => "Administrator",
        ],
    ],
    "departments" => [
        "production" => "production",
        "music" => "production_entertainment_music",
        "film" => "production_entertainment_film",
    ],

    "action" => [
        "updated" => "updated",
        "added" => "added",
        "deleled" => "deleted",
    ],
    "save" => [
        "path" => "store/files",
        "diskType" => "local",
    ],
    "permission" => [
        "action" => [
            "add" => "add",
            "list" => "list",
            "delete" => "delete",
        ],
        "prefix" => "-",
        "module" => [
            "music" => "music", //
            "file" => "file", // thao tác file nếu có quyền music, nhạc contract thì xem được hết tất cả các file
            "film" => "film",
            "contract" => "contract", // vao được danh mục hợp đồng
            "contract-viettel-buy" => "contract-viettel-buy", // viettel bán
            "contract-viettel-sale" => "contract-viettel-sale", // Viettel mua
            "contract-partner" => "contract-partner", // hợp đồng của đối tác
            "contract-category" => "contract-category", // loại hợp đồng nhạc phim , ...
            "contract-file" => "contract-file", // danh sách giấy tờ
            "partner" => "partner", // danh sách đối tác
            "list-employee" => "list-employee", // danh sách nhân viên phụ trách hợp đồng
            "phim-le" => "phim-le", // danh sách phim lẻ
            "countries" => "countries", // danh sách quốc gia
            "contract-media-sale-money" => "contract-media-sale-money", // danh sách sản phẩm bán
            "manage" => "manage", // quản trị tư liệu
            "media-role" => "media-role", // danh sách quyền media
            "users" => "users", // danh sách admin đăng nhập
            "logs" => "logs", // Xem log tác động hệ thống
            "roles" => "roles", // xem dnah sach quyen
            "revenue-film" => "revenue-film", // Doanh thu phim
            "revenue-music" => "revenue-music", // Doanh thu nhạc
            "revenue-type" => "revenue-type", // Loại doanh thu
            "revenue-fee" => "revenue-fee", // Chi phi
        ]
    ],
    "media" => [
        "music" => 1,
        "film" => 2,
    ],
    "send_message" => [
        "time_send" => "00:00",
        "service_id" => "VT_VTNET",
        "label" => "Truy cập",
        "deeplink" => "mocha://survey?ref=http://cmsqlbq.mocha.com.vn/",
    ],
    "expire" => 1,
];
