<?php

namespace App\Listeners;

use App\Events\DemoEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;
use App\Models\User;
use App\Mail\DemoMail;

class SendEmailDemoEvent implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DemoEvent  $event
     * @return void
     */
    public function handle(DemoEvent $event)
    {
        $user = User::find($event->id);
        Mail::to($user->email)->send(new DemoMail());
    }
}
