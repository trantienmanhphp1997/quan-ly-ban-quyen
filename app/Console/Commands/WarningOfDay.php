<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class WarningOfDay extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'word:day';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a warning when end date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $tg=Carbon::now();
        $count=DB::table('contracts')->where('end_time','<=', $tg)->count();
        if($count>0){
            $seconds=30;
            $x=60/$seconds;
            do{
                $this->info($count.' hop dong het han');
                time_sleep_until($tg->addSeconds(2)->timestamp);
            }while(--$x >0);
        }
    }
}
