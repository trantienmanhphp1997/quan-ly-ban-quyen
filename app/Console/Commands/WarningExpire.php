<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use App\Http\Controllers\API\SendMessageApiController;
use Carbon\Carbon;
use DB;

class WarningExpire extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:warningExpire';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Warning media is about to expire';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $time=Carbon::now();
        $tomorrow = $time->addDays(config("common.expire"))->format('Y-m-d');
        $nextWeek = $time->addWeeks(config("common.expire"))->format('Y-m-d');
        $nextMonth = $time->addMonths(config("common.expire"))->format('Y-m-d');
        $user_msisdns =  DB::table('contracts')->groupBy('user_msisdn')->pluck('user_msisdn');
        foreach ($user_msisdns as $key => $user_msisdn) {
            $dayExpireCount = DB::table('contracts')->where('end_time','<=', $tomorrow)->where('user_msisdn','=', $user_msisdn)->count();
            $weekExpireCount = DB::table('contracts')->where('end_time','<=', $nextWeek)->where('user_msisdn','=', $user_msisdn)->count();
            $monthExpireCount = DB::table('contracts')->where('end_time','<=', $nextMonth)->where('user_msisdn','=', $user_msisdn)->count();

            $request = new Request();
            $request->service_id = config("common.send_message.service_id");
            $request["content"] = "Có ".$dayExpireCount." hợp đồng sẽ hết hạn trong vòng 1 ngày tới \n".
            "Có ".$weekExpireCount." hợp đồng sẽ hết hạn trong vòng 1 tuần tới \n".
            "Có ".$monthExpireCount." hợp đồng sẽ hết hạn trong vòng 1 tháng tới";
            $request->label = config("common.send_message.label");
            $request->msisdns = (string)$user_msisdn;
            $request->deeplink = config("common.send_message.deeplink");

            $api = new SendMessageApiController();
            $api->sendMessage($request);
        }
    }
}
