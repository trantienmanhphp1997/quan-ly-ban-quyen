<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use App\Http\Controllers\API\SendMessageApiController;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use DB;
use App\Models\Contract;

class Warning1Month extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:warning1Month';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Warning media is about to expire';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info('Send waring 1 month message to user');
        $time=Carbon::now();
        $nextMonth = $time->addMonths(1)->format('Y-m-d');
        $contracts = DB::table('contracts')->where('warning_status', '=','0')->whereNull('deleted_at');
        foreach($contracts->get() as $contract){
            if($contract->end_time <= $nextMonth && $contract->end_time && $contract->end_time >= Carbon::now()->format('Y-m-d')) {
                if($contract->type == 1 && $contract->category_id == 1) {
                    $request = new Request();
                    $request->service_id = config("common.send_message.service_id");
                    $request["content"] = 'Thông báo! Hợp đồng Mua Nhạc: '.$contract->contract_number. ' sẽ hết hạn vào ngày '.reFormatDate($contract->end_time).'. Vui lòng truy cập vào hệ thống: http://cmsqlbq.mocha.com.vn/ để kiểm tra thông tin chi tiết các hợp đồng sắp hết hạn.
Trân trọng!';
                    $request->label = config("common.send_message.label");
                    $request->msisdns = (string)($contract->user_msisdn);
                    $request->deeplink = config("common.send_message.deeplink");

                    $api = new SendMessageApiController();
                    $api->sendMessage($request);
                }
                elseif($contract->type == 2 && $contract->category_id == 1) {
                    $request = new Request();
                    $request->service_id = config("common.send_message.service_id");
                    $request["content"] = 'Thông báo! Hợp đồng Bán Nhạc: '.$contract->contract_number. ' sẽ hết hạn vào ngày '.reFormatDate($contract->end_time).'. Vui lòng truy cập vào hệ thống: http://cmsqlbq.mocha.com.vn/ để kiểm tra thông tin chi tiết các hợp đồng sắp hết hạn.
Trân trọng!';
                    $request->label = config("common.send_message.label");
                    $request->msisdns = (string)($contract->user_msisdn);
                    $request->deeplink = config("common.send_message.deeplink");

                    $api = new SendMessageApiController();
                    $api->sendMessage($request);
                }
                elseif ($contract->type == 1 && $contract->category_id == 2) {
                    $request = new Request();
                    $request->service_id = config("common.send_message.service_id");
                    $request["content"] = 'Thông báo! Hợp đồng Mua Phim: '.$contract->contract_number. ' sẽ hết hạn vào ngày '.reFormatDate($contract->end_time).'. Vui lòng truy cập vào hệ thống: http://cmsqlbq.mocha.com.vn/ để kiểm tra thông tin chi tiết các hợp đồng sắp hết hạn.
Trân trọng!';
                    $request->label = config("common.send_message.label");
                    $request->msisdns = (string)($contract->user_msisdn);
                    $request->deeplink = config("common.send_message.deeplink");

                    $api = new SendMessageApiController();
                    $api->sendMessage($request);
                }
                elseif($contract->type == 2 && $contract->category_id == 2) {
                    $request = new Request();
                    $request->service_id = config("common.send_message.service_id");
                    $request["content"] = 'Thông báo! Hợp đồng Bán Phim: '.$contract->contract_number. ' sẽ hết hạn vào ngày '.reFormatDate($contract->end_time).'. Vui lòng truy cập vào hệ thống: http://cmsqlbq.mocha.com.vn/ để kiểm tra thông tin chi tiết các hợp đồng sắp hết hạn.
Trân trọng!';
                    $request->label = config("common.send_message.label");
                    $request->msisdns = (string)($contract->user_msisdn);
                    $request->deeplink = config("common.send_message.deeplink");

                    $api = new SendMessageApiController();
                    $api->sendMessage($request);
                }
            }
        }
    }
}
