<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use App\Http\Controllers\API\SendMessageApiController;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use DB;

class Warning3Month extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:warning3Month';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Warning media is about to expire';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info('Send waring 3 month message to user');
        $time=Carbon::now();
        $threeMonth = $time->addMonths(3)->format('Y-m-d');
        $user_msisdns =  DB::table('contracts')->groupBy('user_msisdn')->pluck('user_msisdn');
        foreach ($user_msisdns as $key => $user_msisdn) {
            //hop dong mua nhac
            $count = DB::table('contracts')->where('end_time','=', $threeMonth)->where('user_msisdn','=', $user_msisdn)->where('type', '=', 1)->where('category_id', '=', 1)->where('warning_status', '=','0')->whereNull('deleted_at')->count();
            if($count!=0)
            {
                $request = new Request();
                $request->service_id = config("common.send_message.service_id");
                $request["content"] = 'Thông báo! Bạn có '.$count.' hợp đồng Mua Nhạc sẽ hết hạn sau 3 tháng nữa. Vui lòng truy cập vào hệ thống: http://cmsqlbq.mocha.com.vn/ để kiểm tra thông tin chi tiết các hợp đồng sắp hết hạn.
Trân trọng!';
                $request->label = config("common.send_message.label");
                $request->msisdns = (string)$user_msisdn;
                $request->deeplink = config("common.send_message.deeplink");

                $api = new SendMessageApiController();
                $api->sendMessage($request);
            }

            //hop dong ban nhac
            $count = DB::table('contracts')->where('end_time','=', $threeMonth)->where('user_msisdn','=', $user_msisdn)->where('type', '=', 2)->where('category_id', '=', 1)->where('warning_status', '=','0')->whereNull('deleted_at')->count();
            if($count!=0)
            {
                $request = new Request();
                $request->service_id = config("common.send_message.service_id");
                $request["content"] = 'Thông báo! Bạn có '.$count.' hợp đồng Bán Nhạc sẽ hết hạn sau 3 tháng nữa. Vui lòng truy cập vào hệ thống: http://cmsqlbq.mocha.com.vn/ để kiểm tra thông tin chi tiết các hợp đồng sắp hết hạn.
Trân trọng!';
                $request->label = config("common.send_message.label");
                $request->msisdns = (string)$user_msisdn;
                $request->deeplink = config("common.send_message.deeplink");

                $api = new SendMessageApiController();
                $api->sendMessage($request);
            }

            //hop dong mua phim

            $count = DB::table('contracts')->where('end_time','=', $threeMonth)->where('user_msisdn','=', $user_msisdn)->where('type', '=', 1)->where('category_id', '=', 2)->where('warning_status', '=','0')->whereNull('deleted_at')->count();
            if($count!=0)
            {
                $request = new Request();
                $request->service_id = config("common.send_message.service_id");
                $request["content"] = 'Thông báo! Bạn có '.$count.' hợp đồng Mua Phim sẽ hết hạn sau 3 tháng nữa. Vui lòng truy cập vào hệ thống: http://cmsqlbq.mocha.com.vn/ để kiểm tra thông tin chi tiết các hợp đồng sắp hết hạn.
Trân trọng!';
                $request->label = config("common.send_message.label");
                $request->msisdns = (string)$user_msisdn;
                $request->deeplink = config("common.send_message.deeplink");

                $api = new SendMessageApiController();
                $api->sendMessage($request);
            }

            //hop dong ban phim
            $count = DB::table('contracts')->where('end_time','=', $threeMonth)->where('user_msisdn','=', $user_msisdn)->where('type', '=', 2)->where('category_id', '=', 2)->where('warning_status', '=','0')->whereNull('deleted_at')->count();
            if($count!=0)
            {
                $request = new Request();
                $request->service_id = config("common.send_message.service_id");
                $request["content"] = 'Thông báo! Bạn có '.$count.' hợp đồng Bán Phim sẽ hết hạn sau 3 tháng nữa. Vui lòng truy cập vào hệ thống: http://cmsqlbq.mocha.com.vn/ để kiểm tra thông tin chi tiết các hợp đồng sắp hết hạn.
Trân trọng!';
                $request->label = config("common.send_message.label");
                $request->msisdns = (string)$user_msisdn;
                $request->deeplink = config("common.send_message.deeplink");

                $api = new SendMessageApiController();
                $api->sendMessage($request);
            }
        }
    }
}
