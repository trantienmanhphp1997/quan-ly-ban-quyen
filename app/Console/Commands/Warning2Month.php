<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use App\Http\Controllers\API\SendMessageApiController;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use DB;
use App\Models\Contract;

class Warning2Month extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:warning2Month';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Warning media is about to expire';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info('Send waring 2 month message to user');
        $time=Carbon::now();
        $array = array();
        $array[] = $time->addDays(32)->format('Y-m-d');
        $array[] = $time->addWeeks(1)->format('Y-m-d');
        $array[] = $time->addWeeks(1)->format('Y-m-d');
        $array[] = $time->addWeeks(1)->format('Y-m-d');
        $array[] = $time->addWeeks(1)->format('Y-m-d');
        $user_msisdns =  DB::table('contracts')->whereNull('deleted_at')->groupBy('user_msisdn')->pluck('user_msisdn');
        foreach ($user_msisdns as $key => $user_msisdn) {
            foreach ($array as $day) {
                //hop dong mua nhac
                $count = DB::table('contracts')->where('end_time','=', $day)->where('user_msisdn','=', $user_msisdn)->where('type', '=', 1)->where('category_id', '=', 1)->where('warning_status', '=','0')->whereNull('deleted_at')->count();
                if($count!=0)
                {
                    $request = new Request();
                    $request->service_id = config("common.send_message.service_id");
                    $request["content"] = 'Thông báo! Bạn có '.$count.' hợp đồng Mua Nhạc sẽ hết hạn sau '.Carbon::now()->diffInDays($day).' ngày nữa. Vui lòng truy cập vào hệ thống: http://cmsqlbq.mocha.com.vn/ để kiểm tra thông tin chi tiết các hợp đồng sắp hết hạn.
Trân trọng!';
                    $request->label = config("common.send_message.label");
                    $request->msisdns = (string)$user_msisdn;
                    $request->deeplink = config("common.send_message.deeplink");

                    $api = new SendMessageApiController();
                    $api->sendMessage($request);
                }

                //hop dong ban nhac
                $count = DB::table('contracts')->where('end_time','=', $day)->where('user_msisdn','=', $user_msisdn)->where('type', '=', 2)->where('category_id', '=', 1)->where('warning_status', '=','0')->whereNull('deleted_at')->count();
                if($count!=0)
                {
                    $request = new Request();
                    $request->service_id = config("common.send_message.service_id");
                    $request["content"] = 'Thông báo! Bạn có '.$count.' hợp đồng Bán Nhạc sẽ hết hạn sau '.Carbon::now()->diffInDays($day).' ngày nữa. Vui lòng truy cập vào hệ thống: http://cmsqlbq.mocha.com.vn/ để kiểm tra thông tin chi tiết các hợp đồng sắp hết hạn.
Trân trọng!';
                    $request->label = config("common.send_message.label");
                    $request->msisdns = (string)$user_msisdn;
                    $request->deeplink = config("common.send_message.deeplink");

                    $api = new SendMessageApiController();
                    $api->sendMessage($request);
                }

                //hop dong mua phim

                $count = DB::table('contracts')->where('end_time','=', $day)->where('user_msisdn','=', $user_msisdn)->where('type', '=', 1)->where('category_id', '=', 2)->where('warning_status', '=','0')->whereNull('deleted_at')->count();
                if($count!=0)
                {
                    $request = new Request();
                    $request->service_id = config("common.send_message.service_id");
                    $request["content"] = 'Thông báo! Bạn có '.$count.' hợp đồng Mua Phim sẽ hết hạn sau '.Carbon::now()->diffInDays($day).' ngày nữa. Vui lòng truy cập vào hệ thống: http://cmsqlbq.mocha.com.vn/ để kiểm tra thông tin chi tiết các hợp đồng sắp hết hạn.
Trân trọng!';
                    $request->label = config("common.send_message.label");
                    $request->msisdns = (string)$user_msisdn;
                    $request->deeplink = config("common.send_message.deeplink");

                    $api = new SendMessageApiController();
                    $api->sendMessage($request);
                }

                //hop dong ban phim
                $count = DB::table('contracts')->where('end_time','=', $day)->where('user_msisdn','=', $user_msisdn)->where('type', '=', 2)->where('category_id', '=', 2)->where('warning_status', '=','0')->whereNull('deleted_at')->count();
                if($count!=0)
                {
                    $request = new Request();
                    $request->service_id = config("common.send_message.service_id");
                    $request["content"] = 'Thông báo! Bạn có '.$count.' hợp đồng Bán Phim sẽ hết hạn sau '.Carbon::now()->diffInDays($day).' ngày nữa. Vui lòng truy cập vào hệ thống: http://cmsqlbq.mocha.com.vn/ để kiểm tra thông tin chi tiết các hợp đồng sắp hết hạn.
Trân trọng!';
                    $request->label = config("common.send_message.label");
                    $request->msisdns = (string)$user_msisdn;
                    $request->deeplink = config("common.send_message.deeplink");

                    $api = new SendMessageApiController();
                    $api->sendMessage($request);
                }
            }
        }
    }
}
