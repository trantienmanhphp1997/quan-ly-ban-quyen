<?php


namespace App\Component;


use Illuminate\Support\Arr;

class Recursive
{
    private $data;
    private $htmlSelect = '';

    public function __construct($data)
    {
        $this->data = $data;

    }
    public function recursive($parentId, $id)
    {
        foreach ($this->data as $value) {
            if($id!=$value['id'] ){
                if ($value['id'] == $parentId && $parentId!=0) {
                    $this->htmlSelect .= "<option value='" . $value['id'] . "'selected>" . $value['name'] . "</option>";
                }
                else {
                    $this->htmlSelect .= "<option value='" . $value['id'] . "'>"  . $value['name'] . "</option>";               
                }
            }
        }
        return $this->htmlSelect;

    }
    private $option=[];
    public function recursiveMedia(){
        $firstLevel=Arr::where($this->data, function ($item){
            return empty($item['parent_id']);
        });
        $this->processRoleMediaLevel($firstLevel);
        return $this->option;
    }
    public function processRoleMediaLevel($role_medias, $level = 0) {
        $level++;
        $text= '&nbsp&nbsp&nbsp&nbsp';
        foreach ($role_medias as $key => $role) {
            $children = Arr::where($this->data, function ($value) use ($role) {
                return $role['id'] == $value ['parent_id'];
            });
            $this->option[] = [
                'id' => $role['id'],
                'name' => $role['code'],
                'text' => str_repeat($text, $level - 1)
            ];
            $this->processRoleMediaLevel($children, $level);
        }
    }

}
