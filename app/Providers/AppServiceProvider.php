<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;
use DB;
use App\Models\Contract;
use App\Models\Film;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Auth;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Validator::extend('phone', function($attribute, $value, $parameters, $validator) {

            return ctype_digit($value) && strlen($value) == 10 && ($value[0] == '0') && ($value[1] == '3' || $value[1] == '5' || $value[1] == '7' || $value[1] == '8' || $value[1] == '9');
        });

        Validator::replacer('phone', function($message, $attribute, $rule, $parameters) {
            return str_replace(':attribute',$attribute, 'Số điện thoại không hợp lệ');
        });

        Validator::extend('phonePartner', function($attribute, $value, $parameters, $validator) {
            for($i=0; $i < strlen($value); $i++){
                if( ((int) $value[$i]==0 && $value[$i] !='0') &&($value[$i]!='('&&$value[$i]!=')'&&$value[$i]!='+')){
                    // dd($value[$i]!='('||$value[$i]!=')'||$value[$i]!='+');
                    return false;
                }
            }
            return true;
        });

        Validator::replacer('phonePartner', function($message, $attribute, $rule, $parameters) {
            return str_replace(':attribute',$attribute, 'Số điện thoại chỉ được chứa chữ số và ký tự +()');
        });

        Validator::extend('equal_parent_type', function($attribute, $value, $parameters, $validator) {
            return false;
        });

        Validator::replacer('equal_parent_type', function($message, $attribute, $rule, $parameters) {
            return str_replace(':attribute',$attribute, 'Loại quyền của quyền con phải giống với của quyền cha');
        });

        Validator::extend('my_contract', function($attribute, $value, $parameters, $validator) {
            if(Auth::user()->is_manager) return true;
            if(DB::table('contracts')->where('contract_number','=', $value)->get()->first()) {
                if(DB::table('contracts')->where('contract_number','=', $value)->get()->first()->admin_id!=Auth::user()->id)
                    return false;
                else return true;
            }
            else return true;
        });

        Validator::replacer('my_contract', function($message, $attribute, $rule, $parameters) {
            return str_replace(':attribute',$attribute, 'Số hợp đồng đã tồn tại và thuộc quản lý của một người dùng khác');
        });

        Validator::extend('active_contract', function($attribute, $value, $parameters, $validator) {
            $contract = DB::table('contracts')->where('contract_number','=', $value)->get()->first();
            if($contract) {
                if($contract->deleted_at)
                    return false;
                else return true;
            }
            else return true;
        });

        Validator::replacer('active_contract', function($message, $attribute, $rule, $parameters) {
            return str_replace(':attribute',$attribute, 'Hợp đồng này đã bị xóa, vui lòng khôi phuc lại để thao tác');
        });

        Validator::extend('type_contract', function($attribute, $value, $parameters, $validator) {
            $contract = DB::table('contracts')->where('contract_number','=', $value)->get()->first();
            if($contract){
                if($parameters[0]==4)
                    if(($contract->type==1&&$contract->category_id==1)||$contract->type==4)
                        return true;
                if($parameters[0]==1&&$parameters[1]==1)
                    if($contract->type==4||($contract->type==1&&$contract->category_id==1))
                        return true;
                if($contract->type==$parameters[0])
                {
                    if(isset($parameters[1]) && $contract->category_id!=$parameters[1])
                        return false;
                    else return true;
                }
                else return false;
            }
            return true;
        });

        Validator::replacer('type_contract', function($message, $attribute, $rule, $parameters) {
            $type = '';
            if($parameters[0]==1 && $parameters[1]==1)
                $type = 'mua nhạc';
            elseif($parameters[0]==1 && $parameters[1]==2)
                $type = 'mua phim';
            elseif($parameters[0]==2)
                $type = 'bán';
            elseif($parameters[0]==3)
                $type = 'đối tác';
            else
                $type = 'tác giả';
            return str_replace(':attribute',$attribute, 'Số hợp đồng đã tồn tại và không phải hợp đồng '.$type);
        });

        $filmName = '';
        Validator::extend('time_film_contract', function($attribute, $value, $parameters, $validator){
            $film = Film::find($parameters[0]);
            $contract = Contract::find($film->contract_id);
            if(!$contract||!$contract->start_time||!$contract->end_time||strtotime($value) <= strtotime($contract->end_time) && strtotime($value) >= strtotime($contract->start_time))
                return true;
            else return false;
        });

        Validator::replacer('time_film_contract', function($message, $attribute, $rule, $parameters) {
            $film = Film::find($parameters[0]);
            $contract = Contract::find($film->contract_id);
            return str_replace(':attribute',$attribute, 'Thời gian bắt đầu và kết thúc của phim phải nằm trong thời hạn hợp đồng ('.reFormatDate($contract->start_time).' đến '.reFormatDate($contract->end_time).')');
        });

        Validator::extend('time_film_contract2', function($attribute, $value, $parameters, $validator){
            $contract = Contract::find($parameters[0]);
            if(!$contract||!$contract->start_time||!$contract->end_time||strtotime($value) <= strtotime($contract->end_time) && strtotime($value) >= strtotime($contract->start_time))
                return true;
            else return false;
        });

        Validator::replacer('time_film_contract2', function($message, $attribute, $rule, $parameters) {
            $contract = Contract::find($parameters[0]);
            return str_replace(':attribute',$attribute, 'Thời gian bắt đầu và kết thúc của phim phải nằm trong thời hạn hợp đồng ('.reFormatDate($contract->start_time).' đến '.reFormatDate($contract->end_time).')');
        });

        Validator::extend('film_can_buy', function($attribute, $value, $parameters, $validator) {
            return false;
        });

        Validator::replacer('film_can_buy', function($message, $attribute, $rule, $parameters) {
            return str_replace(':attribute',$attribute, 'Thời hạn của phim '.$parameters[0].' phải nằm trong thời hạn bản quyền của hợp đồng');
        });
        Validator::extend('check_contract', function($attribute, $value, $parameters, $validator) {
            return false;
        });

        Validator::replacer('check_contract', function($message, $attribute, $rule, $parameters) {
            return str_replace(':attribute',$attribute, 'Bộ phim '.$parameters[0].' đã nằm trong hợp đồng khác ('.$parameters[1].')');
        });
        Validator::extend('check_contract_music', function($attribute, $value, $parameters, $validator) {
            return false;
        });

        Validator::replacer('check_contract_music', function($message, $attribute, $rule, $parameters) {
            return str_replace(':attribute',$attribute, 'Bài hát '.$parameters[0].' đã nằm trong hợp đồng khác ('.$parameters[1].')');
        });
        Validator::extend('checkUniqueName', function($attribute, $value, $parameters, $validator) {
            if($parameters[2]){
                $contract = Contract::Join($parameters[0],$parameters[0].'.contract_id','contracts.id')
                    ->where('contract_number',$parameters[2])
                    ->where($parameters[0].'.'.$parameters[1],$value)
                    ->get();
                if(count($contract)) return false;
                return true;
            }
            else {
                // $media = DB::table($parameters[0])->where($parameters[1],$value)
                //     ->where(function($query){
                //         $query->where('contract_id', null);
                //         $query->orWhere('contract_id',0);
                //     })
                //     ->get();
                // if(count($media)) return false;
                return true;
            }
        });

        Validator::replacer('checkUniqueName', function($message, $attribute, $rule, $parameters) {
            if($parameters[0]=='films')
                return str_replace(':attribute',$attribute, 'Đã tồn tại phim '.$parameters[3].' thuộc Số hợp đồng '.$parameters[2]);
            else
                return str_replace(':attribute',$attribute, 'Đã tồn tại bài hát '.$parameters[3].' thuộc Số hợp đồng '.$parameters[2]);
        });


        Validator::extend('checkUniqueFileName', function($attribute, $value, $parameters, $validator) {
            $filmBackups;
            if(count($parameters)>2){
                $filmBackups = DB::table('film_backups')->where('file_name', $parameters[0])->where('driver', $parameters[1])->where('id','!=',$parameters[2])->get(); 
            }
            else{
                $filmBackups = DB::table('film_backups')->where('file_name', $parameters[0])->where('driver', $parameters[1])->get();                
            }
            if(count($filmBackups)) return false;
            return true;
        });

        Validator::replacer('checkUniqueFileName', function($message, $attribute, $rule, $parameters) {
            return str_replace(':attribute',$attribute, 'Tên file đã tồn tại');
        });

    }
}
