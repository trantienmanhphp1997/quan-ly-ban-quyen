<?php

namespace App\Traits;

trait isValue
{
    /**
     * trả về true nếu có giá trị. trả về false nếu :
     * $value = 0,
     * $value = "0",
     * $value = null,
     * $value = undefined,
     * $value = false,
     * $value = NaN,
     * $value = ""
     *
     * @param $value
     * @return bool
     */
    public function isValue($value)
    {
        if ($value === 0) {
            return false;
        } else if ($value === "0") {
            return false;
        } else if ($value === null) {
            return false;
        } else if ($value === "undefined") {
            return false;
        } else if ($value === false) {
            return false;
        } else if ($value === "NaN") {
            return false;
        } else if ($value === "") {
            return false;
        } else if ($value === []) {
            return false;
        }
        return true;
    }
}
