<?php
namespace App\Http\Livewire;

use App\Http\Livewire\Base\BaseLive;
use App\Models\ContractFile;
use App\Models\Contract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Validator;
class ContractFileTable extends BaseLive
{
    use WithFileUploads;
    public $data;
    public $url=null;
    public $searchContract;
    public $deleteId = '';
    public $data2;
    public $file_upload;
    public $file_upload_edit;
    public $date_sign;
    public $date_sign_edit;
    public $file_name;
    public $file_name_edit;
    public $updateMode = false;
    private $contractFiles;
    public $editId = '';
    protected $listeners = ['update'];
    public $kt = false;
    public function deleteId($id)  // hàm này để lấy id
    {
        Log::debug('ContractFileTable deleteId');
        $this->deleteId = $id;
        $this->dispatchBrowserEvent('open-modal-delete');
    }
    protected $rules = [
        'file_name' => 'required',
        'file_upload' => 'required',
    ];

    // public function submit()
    // {
    //     $this->validate();

    //     // Execution doesn't reach here if validation fails.

    //     ContractFile::create([
    //         'name' => $this->file_name,
    //         'link' => $this->link,
    //         'contract_id' => $this->contract_id,
    //     ] );;
    //     $this->dispatchBrowserEvent('close-modal');
    // }

    public function render()
    {
        if($this->updateMode || !$this->contractFiles){
            $this->contractFiles = $this->data->contractFiles()->orderBy('id','desc')->paginate(10);
            $this->updateMode = false;
        }
        // if($this->file_upload) {
        //     $this->resetValidation('file_upload');
        // }
        $contractFile = '';
        if($this->editId){
            $contractFile = ContractFile::find($this->editId);
            if($this->kt){
                $this->file_name_edit = $contractFile->name;
                $this->date_sign_edit = $contractFile->date_sign;
            }
        } 
        // dd($this->file_name_edit);

        Log::debug('ContractFileTable render');
        return view('livewire.contract.file', ['contractFiles' => $this->contractFiles, 'file'=> $contractFile])->with('i', 0);
        // $this->emit('saveData');
    }

    public function delete()   // hàm này để thực hiện xóa
    {
        Log::debug('ContractFileTable delete');
        $contractFile = ContractFile::find($this->deleteId);
        Storage::delete($contractFile->link);
        $contractFile ->delete();
        $this->updateMode = true;
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Xóa thành công"] );
    }
    public function editId(Request $request,$id){
        $this->resetValidation();
        if($this->editId) $this->kt = true;
        else $this->kt=false;
        $this->editId = $id;
    }
    public function update($data){
        // dd($data);
        $data['file_upload_edit'] = $this->file_upload_edit;
        // dd($data);
        Validator::make($data,[
            'file_name_edit' => 'required',
            'file_upload_edit' => $data['file_upload_edit']?'file|max:102400|mimes:xml,doc,docx,xlsx,jpg,png,pdf':'',
        ],[
           'file_name_edit.required' => 'Tên giấy tờ bắt buộc',
           'file_upload_edit.mimes' => "Giấy tờ liên quan phải có định dạng doc, docx, jpg, png, xlsx, pdf",
           'file_upload_edit.file' => "Giấy tờ liên quan phải là file",
        ])->validate();
        $this->resetValidation('file_upload_edit');
        if($data['file_upload_edit']){
            $contractFile = ContractFile::find($this->editId);
            Storage::delete($contractFile->link);
            $contractFileName = time().'_'.$this->file_upload_edit->getClientOriginalName();
            $link = $this->file_upload_edit->storeAs(config("common.save.path"), $contractFileName, config("common.save.diskType"));
            $contractFile->update([
                'name' => $data['file_name_edit'],
                'date_sign' => $data['date_sign_edit'],
                'link' => $link,
            ]);
        }
        else {
            ContractFile::find($this->editId)->update([
                'name' => $data['file_name_edit'],
                'date_sign' => $data['date_sign_edit'],
            ]);           
        }
        $this->resetInput();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Chỉnh sửa thành công"] );
        $this->emit('close-contract-file-modal-edit');
    }
    public function store()
    {
        if($this->date_sign=="") $this->date_sign = null;
        Log::debug('ContractFileTable store'.  $this->file_name . "file_upload: ". $this->file_upload);
        $this->validate([
            'file_name' => 'required',
            'file_upload' => 'required|mimes:xml,doc,docx,xlsx,jpg,png,pdf|max:102400',
        ],[
            'file_upload.required' => "Giấy tờ liên quan bắt buộc",
            'file_upload.mimes' => "Giấy tờ liên quan phải có định dạng doc, docx, jpg, png, xlsx, pdf",
            'file_name.required' => "Tên giấy tờ bắt buộc",
        ],[]);
        $contractFileName = time().'_'.$this->file_upload->getClientOriginalName();
        $this->link = $this->file_upload->storeAs(config("common.save.path"), $contractFileName, config("common.save.diskType"));
        if (get_class($this->data) == "App\Models\Contract") {
            $contract = Contract::find($this->data->id);
            ContractFile::create([
                'name' => $this->file_name,
                'date_sign' => $this->date_sign,
                'link' => $this->link,
                'contract_id' => $this->data->id,
                'categori_id' => $contract->category_id,
            ]);
        }
        elseif (get_class($this->data) == "App\Models\Music") {
            ContractFile::create([
                'name' => $this->file_name,
                'date_sign' => $this->date_sign,
                'link' => $this->link,
                'media_id' => $this->data->id,
                'categori_id' => config("common.media.music"),
            ]);
        }
        else {
            ContractFile::create([
                'name' => $this->file_name,
                'date_sign' => $this->date_sign,
                'link' => $this->link,
                'media_id' => $this->data->id,
                'categori_id' =>  config("common.media.film"),
            ]);
        }
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Thêm giấy tờ thành công"] );
        $this->resetInput();
        $this->updateMode = true;
        $this->emit('close-contract-file-modal');
    }

    public function edit()
    {
        // $validatedDate = $this->validate([
        //     'name' => '',
        //     'contact_id' => '',
        // ]);
        // Log::info('ContractFileTable store');

        // ContractFile::create($validatedDate);

        // session()->flash('message', 'Users Created Successfully.');
        // $this->resetInput();

    }
    public function resetInput(){
        $this->file_name = null;
        $this->date_sign = null;
        $this->file_upload = null;
        $this->editId = null;
        $this->emit('resetFileInput');
    }

    public function openModal()
    {
        $this->emit('show');
    }

    public function checkData(){
        $this->file_upload = null;
        $this->validate([
            'file_name' => 'required',
            'file_upload' => 'required|mimes:xml,doc,docx,xlsx,jpg,png,pdf|max:102400',
        ],[
            'file_upload.required' => "Giấy tờ liên quan bắt buộc",
            'file_upload.mimes' => "Giấy tờ liên quan phải có định dạng doc, docx, jpg, png, xlsx, pdf",
            'file_name.required' => "Tên giấy tờ bắt buộc",
        ],[]);     
    }
    public function forDownload($id){
        $dl = ContractFile::find($id);
        return Storage::download($dl->link);
    }
}
