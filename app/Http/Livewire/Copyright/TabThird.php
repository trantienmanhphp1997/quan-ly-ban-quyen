<?php
namespace App\Http\Livewire\Copyright;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\Film;
use DB;

class TabThird extends Component
{

    public function render(){
        $countryNumberPhimLe =Film::whereNotNull('country')->where('count_tap', 1)->orderBy('country')->groupBy('country')->select(DB::raw('count("country") as num'))->pluck('num')->toArray();
        $countryNumberPhimBo =Film::whereNotNull('country')->where('count_tap', '!=', 1)->orderBy('country')->groupBy('country')->select(DB::raw('count("country") as num'))->pluck('num')->toArray();
        $indexMergePhimBo = $countryNumberPhimBo;
        for($i = 1; $i < count($indexMergePhimBo); $i++){
            $indexMergePhimBo[$i] = $indexMergePhimBo[$i] + $indexMergePhimBo[$i-1]?? 0;

        }
        $indexMergePhimLe = $countryNumberPhimLe;
        for($i = 1; $i < count($indexMergePhimLe); $i++){
            $indexMergePhimLe[$i] = $indexMergePhimLe[$i] + $indexMergePhimLe[$i-1]?? 0;

        }
        $phimLe =Film::whereNotNull('country')->where('count_tap', 1)->orderBy('country')->get();
        $phimBo =Film::whereNotNull('country')->where('count_tap', '!=', 1)->orderBy('country')->get();

        return view('livewire.copyright._tabThird', compact('phimLe', 'phimBo', 'countryNumberPhimLe' ,'countryNumberPhimBo', 'indexMergePhimBo', 'indexMergePhimLe'));
    }
}
