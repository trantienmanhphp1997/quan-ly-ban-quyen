<?php
namespace App\Http\Livewire\Copyright;
use App\Models\Film;
use Livewire\Component;
use Livewire\WithFileUploads;

class TabFirst extends Component
{

    public function render(){
        $data = Film::join('contracts','contracts.id','films.contract_id')
            ->leftJoin('countries','countries.id','films.country')
            ->select('films.*','countries.name as country_name','contracts.contract_number')->get();
        return view('livewire.copyright._tabFirst',compact('data'));
    }
}
