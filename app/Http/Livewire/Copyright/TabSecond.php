<?php
namespace App\Http\Livewire\Copyright;

use Livewire\Component;
use App\Models\Film;
use Livewire\WithFileUploads;

class TabSecond extends Component
{

    public function render(){
        $query = Film::query();
        $data = $query->get();
        return view('livewire.copyright._tabSecond',[
            'data' => $data,
        ]);
    }
}
