<?php
namespace App\Http\Livewire\Copyright;

use Livewire\Component;
use App\Exports\CopyrightExport;
use Excel;
use Livewire\WithFileUploads;

class ListCopyright extends Component
{

    public function render(){
        return view('livewire.copyright.list');
    }

    public function export(){
        $today = date("d_m_Y");
        return Excel::download(new CopyrightExport, 'Theo_dõi_bản_quyền-'.$today.'.xlsx');
    }
}
