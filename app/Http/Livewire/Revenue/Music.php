<?php

namespace App\Http\Livewire\Revenue;

use App\Component\Recursive;
use App\Exports\RevenueMusicExport;
use App\Models\Category;
use App\Models\Contract;
use App\Models\Revenue;
use App\Models\RevenueType;
use App\Models\User;
use Illuminate\Support\Facades\Config;
use Livewire\Component;
use DB;
use Livewire\WithPagination;

class Music extends Component
{
    use WithPagination;

    public $searchNumber, $searchType, $setDate, $reset;

    protected $listeners = [
        'get-music-revenue-chart-data' => 'getRevenueChartData'
    ];

    private $currentUser;

    public function __construct()
    {
        $this->currentUser = auth()->user();
    }

    public function getSearchDateProperty() {
        $tmp = explode('-', $this->setDate);
        if(count($tmp) != 2) return ''; 

        return [
            'start_date' => date("Y-m-d 00:00:00", strtotime($tmp[0])),
            'end_date' => date("Y-m-d 00:00:00", strtotime($tmp[1]))
        ];
    }

    public function updatingSearchNumber() {
        $this->resetPage();
    }

    public function updatingSearchType() {
        $this->resetPage();
    }

    public function updatingSetDate() {
        $this->resetPage();
    }

    public function getRevenueChartData() {
        $query = Contract::where('category_id', '=', 1)->where('type', '=', 2)
                    ->selectRaw('SUM(total_money) AS sum_money')
                    ->selectRaw('DATE_FORMAT(date_sign, "%Y-%m-%d") AS date_modified');
        if($this->currentUser->is_manager == 0) $query->where('admin_id', $this->currentUser->id);
        $query->whereRaw('date_sign BETWEEN CURRENT_DATE() - 7 AND CURRENT_DATE()');
        $result = $query->groupBy('date_modified')->get();

        $this->emit('show-music-revenue-chart-data', $result);

        return $result;
    }

    public function resetSearch(){
        $this->reset = true;
    }

    public function render()
    {
        if($this->reset){
            $this->searchNumber = null;
            $this->setDate = null;
            $this->reset = false;
        }
        $perPage = Config::get('app.per_page') ? Config::get('app.per_page') : 100;
        $query = Contract::where('category_id', '=', 1)->where('type', '=', 2);
        if($this->searchNumber) {
            $query->where('contract_number', 'like', '%'.trim($this->searchNumber).'%');
        }
        if ($this->searchDate) {
            $query->whereBetween('date_sign', [$this->searchDate['start_date'], $this->searchDate['end_date']]);
        }
        $data = $query->orderBy('id','desc')->paginate($perPage);
        $model = config("common.permission.module.revenue-music", "revenue-music");

        $this->getRevenueChartData();

        return view('livewire.revenue.music', [
            'data' => $data,
            'html' => $this->getRevenue(''),
            'model' => $model,
        ]);
    }

    public function getRevenue($parenId)
    {
        $data = RevenueType::all();
        $recusive = new Recursive($data);
        $html = $recusive->recursive($parenId,'');
        return $html;
    }

    public function export(){
        $today = date("d_m_Y");
        $export = new RevenueMusicExport($this->searchNumber, $this->searchType, $this->searchDate);

        $this->getRevenueChartData();
        return \Excel::download($export, 'revenue-music-'.$today.'.xlsx');
    }
}
