<?php

namespace App\Http\Livewire\Revenue;

use App\Component\Recursive;
use App\Exports\MusicExport;
use App\Exports\RevenueFilmExport;
use App\Exports\RevenueFilmDetailExport;
use App\Models\Category;
use App\Models\ContractMediaSaleMoney;
use App\Models\ContractMediaSaleRole;
use App\Models\Revenue;
use App\Models\RevenueType;
use App\Models\User;
use App\Models\Country;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Livewire\Component;
use DB;
use Livewire\WithPagination;

class Film extends Component
{
    use WithPagination;

    public $searchNumber, $searchType,$setDate, $searchName, $searchCountry, $searchCategory, $viewType, $reset;

    private $currentUser;

    public function __construct()
    {
        $this->currentUser = auth()->user();
    }

    protected $listeners=['getRevenueFilmData' => 'getRevenueChartData'];
    public function getSearchDateProperty()
    {
        if($this->setDate!=null)

        {
            $time=explode(" - ",$this->setDate);
            return [
                'start_date' => date("Y-m-d 00:00:00",strtotime($time[0])),
                'end_date' => date("Y-m-d 23:59:59",strtotime($time[1]))
            ];
        }
        else{
            $this->setDate='';
        }
    }

    public function updatingSearchNumber() {
        $this->resetPage();
    }

    public function updatingSearchType() {
        $this->resetPage();
    }

    public function updatingSetDate() {
        $this->resetPage();
    }

    public function getRevenueChartData()
    {
        $query=DB::table('revenue')
        ->where('revenue.category_id', 2)
        ->select(
            DB::raw('SUM(money) AS sum_money'),
            DB::raw("DATE_FORMAT(revenue.date_sign, '%Y-%m-%d') AS date_modified"));

        $query->leftJoin('contracts','contracts.id','=','revenue.contract_id');
        
        if($this->currentUser->is_manager == 0) $query->where('contracts.admin_id', $this->currentUser->id);
        
        if(!empty($this->setDate)){
            $getDate = $this->setDate ? explode(" - ", $this->setDate) : null;
            if ($getDate != '') {
                $startDateCharge = $getDate[0];
                $endDateCharge = $getDate[1];
                if ($startDateCharge != '' && $endDateCharge != '') {
                    $start_time = date("Y-m-d", strtotime($startDateCharge));
                    $end_time = date("Y-m-d", strtotime($endDateCharge));
                    $query->whereBetween('revenue.date_sign', [$start_time, $end_time]);
                }
            }
        }

        $query->whereRaw("revenue.date_sign BETWEEN CURRENT_DATE() - 7 AND CURRENT_DATE()");

        $revenueDay=$query->groupBy('date_modified')->get();

        $this->emit('showRevenueFilmData',$revenueDay);
    }
    public function resetSearch(){
        $this->reset = true;
    }

    public function render()
    {
        if($this->reset){
            $this->searchName = null;
            $this->searchNumber = null;
            $this->searchType = null;
            $this->searchCountry = null;
            $this->searchCategory = null;
            $this->setDate = null;
            $this->count_time = null;
            $this->reset = false;
        }
        $listCountry = Country::orderBy('name')->get()->pluck('name', 'id');
        if($this->viewType==2) {  //Chi tiet
            $perPage = Config::get('app.per_page') ? Config::get('app.per_page') : 100;
            $query = Revenue::where('revenue.category_id',2);

            if($this->currentUser->is_manager == 0) $query->where('contracts.admin_id', $this->currentUser->id);

            $query->leftjoin('contracts','contracts.id','=','revenue.contract_id');
            $query->leftjoin('films','films.id','=','revenue.media_id');
            $query->leftjoin('revenue_type','revenue_type.id','=','revenue.type_id');
            $query->leftjoin('countries','films.country', '=', 'countries.id');
            $query->select(
                'revenue.*',
                'films.license_fee','contracts.contract_number as con_num','films.vn_name as vn_name','films.product_name as product_name', 'films.country as country','films.count_tap as count_tap','films.year_create as year_create', 'countries.name as country_name', 'revenue_type.name as type_name',
            );

            if ($this->searchName) $query->where('films.vn_name','like','%'.trim($this->searchName).'%');
            if ($this->searchCategory == 2) $query->where('films.count_tap','!=', 1);
            if ($this->searchCategory == 1) $query->where('films.count_tap', 1);
            if ($this->searchCountry) $query->where('countries.id', $this->searchCountry);
            if ($this->searchNumber) $query->where('contracts.contract_number','like','%'.trim($this->searchNumber).'%');
            if ($this->searchType) $query->where('revenue.type_id','=',$this->searchType);

            if(!empty($this->setDate)){
                $getDate = $this->setDate ? explode(" - ", $this->setDate) : null;
                if ($getDate != '') {
                    $startDateCharge = $getDate[0];
                    $endDateCharge = $getDate[1];
                    if ($startDateCharge != '' && $endDateCharge != '') {
                     $start_time = date("Y-m-d", strtotime($startDateCharge));
                     $end_time = date("Y-m-d", strtotime($endDateCharge));
                     $query->whereBetween('revenue.date_sign', [$start_time, $end_time]);
                    }
                }
            }

            $query= $query->groupBy('sale_money_id');

            $data = $query->orderBy('id','desc')->paginate($perPage);


            $categories=DB::table('category_films')
                ->leftjoin('film_list_categories', function($join)
                {
                    $join->on('category_films.id', '=', 'film_list_categories.id_category_film');
                })->get();

            $saleRoles= DB::table('contract_media_sale_role')
                ->leftjoin('role_media', function($join)
                {
                    $join->on('contract_media_sale_role.role_id', '=', 'role_media.id');
                })->get();

            $buyRoles= DB::table('contract_media_role')
                ->leftjoin('role_media', function($join)
                {
                    $join->on('contract_media_role.role_id', '=', 'role_media.id');
                })->get();

            $revenue_type=RevenueType::pluck('name','id');


            $revenueDay = DB::table('revenue')
            ->select(
                DB::raw('SUM(money) AS sum_money'),
                DB::raw("DATE_FORMAT(date_sign, '%Y-%m-%d') AS date_modified")
            )
            ->whereRaw("date_sign BETWEEN CURRENT_DATE() - 7 AND CURRENT_DATE() + 1")
            ->groupBy('date_modified')->get();

            $model = config("common.permission.module.revenue-film", "revenue-film");

            $this->getRevenueChartData();

            return view('livewire.revenue.film-detail', [
                'data'=>$data,
                'categories'=>$categories,
                'revenue_type'=>$revenue_type,
                'saleRoles' => $saleRoles,
                'buyRoles' => $buyRoles,
                'html'=>$this->getRevenue(''),
                'revenueDay' => $revenueDay,
                'model' => $model,
                'listCountry' => $listCountry,
            ]);
        }
        else {   // Tong hop
            $perPage = Config::get('app.per_page') ? Config::get('app.per_page') : 100;
            $query = Revenue::where('revenue.category_id',2);

            if($this->currentUser->is_manager == 0) $query->where('contracts.admin_id', $this->currentUser->id);

            $query->leftjoin('contracts','contracts.id','=','revenue.contract_id');
            $query->leftjoin('films','films.id','=','revenue.media_id');
            $query->leftjoin('revenue_type','revenue_type.id','=','revenue.type_id');
            $query->leftjoin('countries','films.country', '=', 'countries.id');
            $query->select(
                'revenue.*',
                'films.license_fee','contracts.contract_number as con_num','films.vn_name as vn_name','films.product_name as product_name', 'films.country as country','films.count_tap as count_tap','films.year_create as year_create', 'countries.name as country_name', 'revenue_type.name as type_name',
            );

            if ($this->searchName) $query->where('films.vn_name','like','%'.trim($this->searchName).'%');
            if ($this->searchCategory == 2) $query->where('films.count_tap','!=', 1);
            if ($this->searchCategory == 1) $query->where('films.count_tap', 1);
            if ($this->searchCountry) $query->where('countries.id', $this->searchCountry);
            // if ($this->searchNumber) $query->where('contracts.contract_number','like','%'.trim($this->searchNumber).'%');
            if ($this->searchType) $query->where('revenue.type_id','=',$this->searchType);
            if(!empty($this->setDate)){
                $getDate = $this->setDate ? explode(" - ", $this->setDate) : null;
                if ($getDate != '') {
                    $startDateCharge = $getDate[0];
                    $endDateCharge = $getDate[1];
                    if ($startDateCharge != '' && $endDateCharge != '') {
                     $start_time = date("Y-m-d", strtotime($startDateCharge));
                     $end_time = date("Y-m-d", strtotime($endDateCharge));
                     $query->whereBetween('films.start_time', [$start_time, $end_time]);
                    }
                }
            }

            $query= $query->groupBy('films.id');

            $data = $query->orderBy('id','desc')->paginate($perPage);


            $categories=DB::table('category_films')
                ->leftjoin('film_list_categories', function($join)
                {
                    $join->on('category_films.id', '=', 'film_list_categories.id_category_film');
                })->get();

            $roles= DB::table('contract_media_sale_role')
                ->leftjoin('role_media', function($join)
                {
                    $join->on('contract_media_sale_role.role_id', '=', 'role_media.id');
                })->get();
            $revenue_type=RevenueType::pluck('name','id');


            $revenueDay = DB::table('revenue')
            ->select(
                DB::raw('SUM(money) AS sum_money'),
                DB::raw("DATE_FORMAT(date_sign, '%Y-%m-%d') AS date_modified")
            )
            ->whereRaw("date_sign BETWEEN CURRENT_DATE() - 7 AND CURRENT_DATE() + 1")
            ->groupBy('date_modified')->get();

            $model = config("common.permission.module.revenue-film", "revenue-film");

            $this->getRevenueChartData();

            return view('livewire.revenue.film', [
                'data'=>$data,
                'categories'=>$categories,
                'revenue_type'=>$revenue_type,
                'roles' => $roles,
                'html'=>$this->getRevenue(''),
                'revenueDay' => $revenueDay,
                'model' => $model,
                'listCountry' => $listCountry,
            ]);
        }
    }

    public function updatedSearchNumber(){
        $this->resetPage();
    }

    public function updatedSearchName(){
        $this->resetPage();
    }

    public function updatedViewType(){
        $this->resetPage();
    }

    public function getRevenue($parenId)
    {
        $data = RevenueType::all();
        $recusive = new Recursive($data);
        $html = $recusive->recursive($parenId,'');
        return $html;
    }
    public function export()
    {
        $today = date("d_m_Y");
        $export= new RevenueFilmExport($this->searchNumber, $this->searchName, $this->setDate, $this->searchCategory, $this->searchCountry);
        $this->getRevenueChartData();
        return \Excel::download($export, 'Thống kê danh thu-Tổng hợp-'.$today.'.xlsx');
    }
    public function exportChiTiet()
    {
        $today = date("d_m_Y");
        $export= new RevenueFilmDetailExport($this->searchNumber, $this->searchName, $this->setDate, $this->searchCategory, $this->searchCountry);
        $this->getRevenueChartData();
        return \Excel::download($export, 'Thống kê danh thu-Chi tiết-'.$today.'.xlsx');
    }
}
