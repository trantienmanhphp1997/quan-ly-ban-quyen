<?php
namespace App\Http\Livewire;

use App\Models\ContractFile;
use App\Models\Music;
use App\Models\ContractMediaRole;
use App\Models\ContractMediaSaleMoney;
use App\Models\Contract;
use Livewire\Component;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Livewire\WithFileUploads;
use App\Http\Livewire\Base\BaseLive;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ContractMusicBuyQtgTable extends BaseLive
{
    public $contract_id;
    public $deleteId = '';
    public $data2;
    public $start_time;
    public $end_time;
    public $money;
    public $musicID;
    public $names = [];
    public $music = [];
    public $updateMode = false;
    protected $listeners = ['nameSelected', 'storeMusicBuyQTG'];
    public $searchTerm;

    public $level = [];
    public $userID;
    public function mount(){
        $this->userID = Auth()->id();
    }
    public function nameSelected($names){
      $this->names = $names;
    }

    public function query()
    {
        $contract_id = $this->contract_id;
        return Music::query()->where('contract_id', $contract_id);
        
    }
    public function deleteId($id)
    {
        // dd($id);
        Log::debug('ContractMediaTable deleteId');
        $this->deleteId = $id;
    }
    protected $rules = [
        // 'file_name' => 'required',
        // 'file_upload' => '',
    ];
    public function submit()
    {
        // dd('vào');
        // $this->validate();

        // // Execution doesn't reach here if validation fails.

        // ContractFile::create([
        //     'name' => $this->file_name,
        //     'link' => $this->file_upload,
        //     'contract_id' => $this->contract_id,
        // ] );
        // $this->dispatchBrowserEvent('close-modal');

    }
    public function render()
    {
        $page = \Request::get('page') ? \Request::get('page') : 1;
        $perPage = Config::get('app_per_page') ? Config::get('app_per_page') : 100;
        $perPage = 10;
        $startLimit = $perPage * ($page - 1);
        $endLimit = $perPage * $page;

        $contract = Contract::find($this->contract_id);
        // if($contract->type==1) $data2 =  Music::Join('contracts','music.contract_id','=', 'contracts.id')->where('contract_id',$this->contract_id)->select('music.*','contract_number')->orderBy('music.id','desc')->offset($startLimit)->limit($perPage)->paginate($perPage);
        $data2 =  Music::where('music.tac_gia',$contract->contract_number)->where('tac_gia','!=',null)->select('music.*','tac_gia as contract_number')->orderBy('music.id','desc')->offset($startLimit)->limit($perPage)->paginate($perPage);
        // dd($data2);

        Log::debug('ContractMediaTable render');
        $arrMusicSession = DB::table('medias_sell_buy')->where('user_id',$this->userID)->where('category_id',1)->where('type',1)->limit(100)->pluck('name', 'media_id as id');
        $arrMusicSession = json_decode(json_encode($arrMusicSession), true);
        $arrMusicNull=Music::where('tac_gia',null)->whereNotIn('id',array_keys($arrMusicSession))->limit(100)->pluck('name', 'id');
        $arrMusicNull = json_decode(json_encode($arrMusicNull), true);
        $arrMusic = $arrMusicSession + $arrMusicNull;
        if($this->searchTerm && strlen($this->searchTerm) >= 2){
            $arrMusic = Music::where('music.name', 'like', '%'.trim($this->searchTerm).'%')->limit(100)->pluck('name', 'id');
        }
        $this->dispatchBrowserEvent('setSelect2');

        return view('livewire.contract.musicBuyQtg', ['data' => $data2, 'arrMusic' => $arrMusic ]);

    }
    public function delete(){
        Log::debug('ContractMediaTable delete');
        $contract = Contract::find($this->contract_id);
        // Music::where('id',$this->deleteId)->update(['contract_id'=>null]);
        Music::where('id',$this->deleteId)->update(['tac_gia'=>null]);
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Xoá mua bài hát thành công" ] );
    }
    public function storeMusicBuyQTG($data)
    {   
        $this->music = $data['listMusic'];
        $filmContract = [];
        $checkContract = true;
        $contract = Contract::find($this->contract_id);
        if($data['listMusic']) {
            // $filmContract = Music::join('contracts','contracts.id','music.contract_id')->whereIn('music.id',$data['listMusic'])->where('contract_id','!=',$this->contract_id)->select('music.name','contracts.contract_number')->get();  
            $filmContract = Music::join('contracts','contracts.contract_number','music.tac_gia')->whereIn('music.id',$data['listMusic'])->where('tac_gia','!=',$contract->contract_number)->select('music.name','contracts.contract_number')->get();    
        }
        if(count($filmContract)) $checkContract = false;   
        $validator = Validator::make($data, [
            'listMusic' => 'required|array'.($checkContract?'':'|check_contract_music:'.$filmContract[0]->name.','.$filmContract[0]->contract_number),
 //           'money' => 'required|numeric|min:0',
        ], [
//            'money.min' =>'Giá tiền phải lớn hơn hoặc bằng 0',
        ], [
            'listMusic' => 'Danh sách bài hát',
//            'money' => 'Giá tiền',
        ])->validate();

        // $startTime = $contract->start_time?reFormatDate($contract->start_time, 'd-m-Y'):'';
        // $endTime = $contract->end_time?reFormatDate($contract->end_time, 'd-m-Y'):'';
        // $validator = Validator::make($data, [
        //     'startTime' => ($data['endTime']&&$startTime)?'required|date|after_or_equal:'.$startTime:( ($data['endTime'])?'required|date':( ($startTime)? 'nullable|date|after_or_equal:'.$startTime: 'nullable|date')) ,
        //     'endTime' => ($data['startTime']&&$endTime)?'required|date|after_or_equal:startTime|before_or_equal:'.$endTime:( ($data['startTime'])?'required|after_or_equal:startTime|date':( ($endTime)? 'nullable|date|before_or_equal:'.$endTime: 'nullable|date'))
        // ], [
        //     'startTime.required' => "Thời gian bắt đầu bắt buộc",
        //     'endTime.required' => "Thời gian kết thúc bắt buộc",
        //     'endTime.after_or_equal' => 'Thời gian kết thúc phải lớn hơn hoặc bằng thời gian bắt đầu',
        //     'endTime.before_or_equal' => 'Thời gian kết thúc phải nhỏ hơn hoặc bằng thời gian kết thúc bản quyền của hợp đồng ('.reFormatDate($endTime, 'd-m-Y').')',
        //     'startTime.after_or_equal' => 'Thời gian bắt đầu phải lớn hơn hoặc bằng thời gian bắt đầu bản quyền của hợp đồng ('.reFormatDate($startTime, 'd-m-Y').')',
        // ])->validate();

        $contract = Contract::find($this->contract_id);
        try {
            Log::debug('ContractMediaMusicTable buy'); 
            $startTime = $contract->start_time;
            $endTime =$contract->end_time;
            // if($contract->type==1) Music::whereIn('id', $data['listMusic'])->update([
            //     'contract_id' => $this->contract_id, 
            //     'start_time' => $startTime,
            //     'end_time'=> $endTime,
            // ]);
            // else {
                // dd('vào');
            $music = Music::whereIn('id', $data['listMusic'])->update([
                'tac_gia' => $contract->contract_number, 
                'start_time' => $startTime,
                'end_time'=> $endTime,
            ]);
            //  }
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Mua bài hát thành công (" . count($data['listMusic']).")"] );

            $this->resetInput();
            $this->emit('close-media-music-modal2');
            $this->emit('clear_check_box_sale');
        }
        catch (\Exception $exception) {
            DB::rollBack();
            Log::error('Message: ' . $exception->getMessage() . ', Line: ' . $exception->getLine());
        }
    }

    public function resetInput(){
        $this->start_time = null;
        $this->end_time = null;
        $this->money = null;
        $this->musicID = null;
        $this->names = [];
        $this->music= null;
    }
     public function openModal()
     {
         $this->emit('show');
     }
    public function checkbox($var){
        $this->$var = !$this->$var;
        // $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => $this->$var ] );
    }
    public function levelClicked(){
        // $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => $this->level ] );
    }

}
