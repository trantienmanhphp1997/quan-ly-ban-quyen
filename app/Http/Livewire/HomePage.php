<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Film;
use DB;

class HomePage extends Component
{

    public $setDate;
    public $minYear;
    public $maxYear;
    public $startYear;
    public $endYear;
    public $startYearList =[];
    public $endYearList =[];
    protected $listeners=[
        'getFilmYearData',
        'getFilmCountryData',
        'getFilmYearCreateData',
        'getFilmListCountryData',
    ];

    public function mount(){

        $this->minYear = min(substr(DB::table('films')->whereNull('deleted_at')->whereNotNull('start_time')->min('start_time'), 0, 4), DB::table('films')->whereNull('deleted_at')->whereNotNull('year_create')->min('year_create'));
        $this->startYear = $this->minYear;
        $this->maxYear = max(substr(DB::table('films')->whereNull('deleted_at')->whereNotNull('end_time')->max('end_time'), 0, 4), DB::table('films')->whereNull('deleted_at')->whereNotNull('year_create')->max('year_create'));
        $this->endYear = $this->maxYear;

    }

    public function render()
    {
        $this->startYearList = [];
        $this->endYearList = [];
        for($i = $this->minYear; $i <= $this->endYear; $i++){
            $this->startYearList[] = $i;
        }
        for($i = $this->startYear; $i <= $this->maxYear; $i++){
            $this->endYearList[] = $i;
        }
        $this->getFilmYearData();
        $this->getFilmCountryData();
        $this->getFilmYearCreateData();
        $this->getFilmListCountryData();

        return view('livewire.home-page');
    }

    public function getFilmYearData(){
        $filmYear = collect();
        for($i = $this->startYear; $i <= $this->endYear; $i++){
            $array = [];
            $array['year'] = $i;
            $array['count']=DB::table('films')->whereNull('deleted_at')->whereNotNull('start_time')
                ->where('start_time', '<=', $i.'/12/31')->where('end_time', '>=', $i.'/1/1')->count('id');
            if($array['count'] !=0 )
                $filmYear->push($array);
        }

        $this->emit('showFilmYearData', $filmYear);
    }

    public function getFilmCountryData(){
        $query = DB::table('films')->whereNull('deleted_at')->where('count_tap', '!=', 1)
        ->join('countries', 'countries.id', '=', 'films.country')
        ->select(
            DB::raw("count(films.id) AS count"),
            DB::raw("countries.name AS country_name"))
        ->where(function($query) {
            $query->where(function($query) {
                $query->where('films.start_time', '>=', $this->startYear.'/12/31' )
                    ->where('films.end_time', '<=', $this->endYear.'/1/1');
            })
            ->orWhere(function($query) {
                $query->where('films.start_time', '<=', $this->startYear.'/12/31' )
                    ->where('films.end_time', '>=', $this->startYear.'/1/1');
            })
            ->orWhere(function($query) {
                $query->where('films.start_time', '<=', $this->endYear.'/12/31' )
                    ->where('films.end_time', '>=', $this->endYear.'/1/1');
            });
        });

        $filmCountry = $query->groupBy('countries.id')->get();
        $this->emit('showFilmCountryData', $filmCountry);
    }

    public function getFilmYearCreateData(){

        $query = DB::table('films')->whereNull('deleted_at')->whereNotNull('year_create')
        ->select(DB::raw("count(id) AS count"), 'year_create');
        $query->whereBetween('films.year_create', [$this->startYear, $this->endYear]);
        $filmYearCreate = $query->groupBy('year_create')->orderBy('year_create')->get();
        $this->emit('showFilmYearCreateData', $filmYearCreate);
    }

    public function getFilmListCountryData(){
        $query = DB::table('films')->whereNull('deleted_at')->where('count_tap', '=', 1)
        ->join('countries', 'countries.id', '=', 'films.country')
        ->select(
            DB::raw("count(films.id) AS count"),
            DB::raw("countries.name AS country_name"))
        ->where(function($query) {
            $query->where(function($query) {
                $query->where('films.start_time', '>=', $this->startYear.'/12/31' )
                    ->where('films.end_time', '<=', $this->endYear.'/1/1');
            })
            ->orWhere(function($query) {
                $query->where('films.start_time', '<=', $this->startYear.'/12/31' )
                    ->where('films.end_time', '>=', $this->startYear.'/1/1');
            })
            ->orWhere(function($query) {
                $query->where('films.start_time', '<=', $this->endYear.'/12/31' )
                    ->where('films.end_time', '>=', $this->endYear.'/1/1');
            });
        });
        $filmListCountry = $query->groupBy('countries.id')->get();
        $this->emit('showFilmListCountryData', $filmListCountry);
    }

    public function resetSearch(){
        $this->startYear = $this->minYear;
        $this->endYear = $this->maxYear;
    }
}
