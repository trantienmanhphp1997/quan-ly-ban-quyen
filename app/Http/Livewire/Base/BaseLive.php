<?php

namespace App\Http\Livewire\Base;

use Livewire\Component;
use Livewire\WithPagination;
abstract class BaseLive extends Component
{
	use WithPagination;
	public $deleteId;
    public $reset = false;
    public $searchTerm;
    public $searchTerm1;
    protected  static function paginationView()
    {
        return 'livewire.common.pagination._pagination';
    }
    public function deleteId($id){
        // Log::debug('FilmTable '.$id);
        $this->deleteId=$id;
    }
    public function levelClicked(){
    	
    }
    public function resetSearch(){
        $this->reset = true;

    }

    public function updatingSearchTerm() {
        $this->resetPage();
    }

    public function updatingSearchTerm1() {
        $this->resetPage();
    }

    public function updatingSearchCategory() {
        $this->resetPage();
    }

    public function updatingSetDate() {
        $this->resetPage();
    }

}
