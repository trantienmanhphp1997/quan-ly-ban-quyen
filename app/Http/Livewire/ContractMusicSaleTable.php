<?php
namespace App\Http\Livewire;

use App\Models\Music;
use App\Models\Contract;
use Illuminate\Support\Facades\Storage;
use App\Models\ContractMediaSaleMoney;
use App\Models\ContractMediaRole;
use Livewire\Component;
use App\Models\Revenue;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use App\Http\Livewire\Base\BaseLive;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Exports\MusicSaleExport;
use App\Exports\MusicSaleDataExport;
use Excel;
class ContractMusicSaleTable extends BaseLive
{
    public $contract_id;
    public $deleteId = '';
    public $editId = '';
    public $data2;
    public $start_time;
    public $end_time;
    public $money;
    public $musicID;
    public $names = [];
    public $updateMode = false;
    public $checkMusicSell=[];
    public $selectAll=false;
    public $disable=true;
    public $music;
    public $userID;
    protected $listeners = ['nameSelected', 'storeMusicSale','deleteAll','getError', 'editMusicSale'];
    // public $cho_doc_quyen = false;
    // public $so_doc_quyen = false;
    // public $cho_tac_quyen = false;
    // public $so_tac_quyen = false;
    // public $sale_3rd = false;
    public $level = [];

    public function mount(){
        $this->userID = Auth()->id();
    }
    public function nameSelected($names){
      $this->names = $names;
    }

    public function query()
    {
        $contract_id = $this->contract_id;
        return Music::query()->where('contract_id', $contract_id);
        
    }
    public function deleteId($id)
    {
        // dd($id);
        Log::debug('ContractMediaTable deleteId');
        $this->deleteId = $id;
    }
    protected $rules = [
        // 'file_name' => 'required',
        // 'file_upload' => '',
    ];
    public function submit()
    {
        // dd('vào');
        // $this->validate();

        // // Execution doesn't reach here if validation fails.

        // ContractFile::create([
        //     'name' => $this->file_name,
        //     'link' => $this->file_upload,
        //     'contract_id' => $this->contract_id,
        // ] );
        // $this->dispatchBrowserEvent('close-modal');

    }
    public function render()
    {
        $perPage = Config::get('app_per_page') ? Config::get('app.per_page') : 10;
        if(Contract::find($this->contract_id)->type==2){ //la hop dong ban
            if($this->reset)
            {
                $this->searchTerm1=null;
                $this->reset=false;
            }

            $data2 =  Music::Join('contract_media_sale_money','music.id','=', 'contract_media_sale_money.media_id')
            ->Join('contracts','contract_media_sale_money.contract_id','=', 'contracts.id')
            ->where('contract_media_sale_money.contract_id',$this->contract_id);
            $data2->leftJoin('contracts as ct','ct.id','music.contract_id')->select('contract_media_sale_money.*','music.name','music.singer','music.end_time as end_time_music','contract_media_sale_money.start_time', 'music.tac_gia'
            ,'contract_media_sale_money.end_time','ct.contract_number');
            Log::debug('ContractMediaTable render');
            if($this->searchTerm && strlen($this->searchTerm) >= 2){
                $arrMusic = Music::where('music.name', 'like', '%'.trim($this->searchTerm).'%')->limit(100)->pluck('name', 'id');
            }
            else {
                $arrMusic = DB::table('medias_sell_buy')->where('user_id',$this->userID)->where('category_id',1)->where('type',2)->limit(100)->pluck('name', 'media_id as id');
            }
            if(trim($this->searchTerm1)){
                $data2->where(function($query){
                    $query->where('music.name', 'like', '%'.trim($this->searchTerm1).'%');
                    $query->orWhere('music.singer','like','%'.trim($this->searchTerm1).'%');
                });
                $this->updatedCheckMusicSell($this->checkMusicSell, 1);
            }
            else {
                if($this->selectAll) {
                    $this->checkMusicSell = $this->musicsQuery->pluck('id')->map(function($item) {
                                    return (string)$item;
                                } )->toArray();                   
                }
            }
            // dd($data2->get());
            $data2=$data2->orderBy('contract_media_sale_money.id','desc')->paginate($perPage);
            // $contractSale = Contract::find($this->contract_id);
            $contractMediaSaleEdit = [];
            if($this->updateMode){
                $contractMediaSaleEdit = Music::Join('contract_media_sale_money','music.id','=', 'contract_media_sale_money.media_id')
                ->where('contract_media_sale_money.id',$this->editId)
                ->select('contract_media_sale_money.*','music.name','music.singer','contract_media_sale_money.start_time', 'music.tac_gia'
                ,'contract_media_sale_money.end_time')
                ->get()->first();
                $this->updateMode=false;
            }
            // dd($contractMediaSale);
            $this->dispatchBrowserEvent('setSelect2');
            $this->dispatchBrowserEvent('setDatepicker');
            return view('livewire.contract.musicSale', [
                'data' => $data2, 
                'arrMusic' => $arrMusic, 
                'contractMediaSaleEdit' =>$contractMediaSaleEdit,
            ]);
        }
        else{  // la hop dong mua
            if($this->reset)
            {
                $this->searchTerm1=null;
                $this->reset=false;
            }
            $contract = Contract::find($this->contract_id);
            $listMusic = Music::where('contract_id', '=', $this->contract_id)
                ->orWhere('tac_gia', $contract->contract_number)->pluck('id');
            // else $listMusic = Music::where('tac_gia', '=', $contract->contract_number)->pluck('id'); 
            // dd('vào');
            $data2 =  Music::Join('contract_media_sale_money','music.id','=', 'contract_media_sale_money.media_id')
            ->Join('contracts','contract_media_sale_money.contract_id','=', 'contracts.id')
            ->whereIn('contract_media_sale_money.media_id', $listMusic)
            ->select('contract_media_sale_money.*','music.name','music.singer','contract_media_sale_money.start_time', 'music.tac_gia'
            ,'contract_media_sale_money.end_time','contract_number', 'contracts.end_time as contract_end_time');
            Log::debug('ContractMediaTable render');

            if(trim($this->searchTerm1)){
                $data2->where(function($query){
                    $query->where('music.name', 'like', '%'.trim($this->searchTerm1).'%');
                    $query->orWhere('music.singer','like','%'.trim($this->searchTerm1).'%');
                 });
            }

            $data2=$data2->orderBy('contract_media_sale_money.id','desc')->paginate($perPage);
            return view('livewire.contract.listMusicSale', ['data' => $data2] );
        }
    }
    public function delete(){
        Log::debug('ContractMediaTable delete');
        
        ContractMediaSaleMoney::where('id', $this->deleteId)->delete();
        Revenue::where('sale_money_id',$this->deleteId)->delete();

        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Xoá bán bài hát thành công" ] );
    }

    public function deleteAll()
    {
        $this->resetPage();
        ContractMediaSaleMoney::Join('music', 'music.id', 'contract_media_sale_money.media_id')
            ->where(function($query){
                $query->where('music.name', 'like', '%'.trim($this->searchTerm1).'%');
                $query->orWhere('music.singer','like','%'.trim($this->searchTerm1).'%');
            })
            ->whereIn('contract_media_sale_money.id',$this->checkMusicSell)
            ->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Xoá bán bài hát thành công (".count($this->checkMusicSell).")" ] );
        $this->checkMusicSell=[];
        $this->selectAll=false;
    }

    public function updatedSelectAll($value,$idMusic)
    {
        if($value)
        {
            $this->checkMusicSell=$this->musicsQuery->pluck('id')->map(function($item) {
                return (string)$item;
            } )->toArray();
        }
        else{
            $this->checkMusicSell=[];
        }
    }


    public function getMusicsProperty()
    {
        $perPage = Config::get('app_per_page') ? Config::get('app.per_page') : 10;
        return $this->musicsQuery->orderBy('contract_media_sale_money.id','desc')->paginate($perPage);
    } 
    public function getMusicsQueryProperty()//lấy ra truy vấn của nhạc = computed property
    {
        // dd('vào');
        $data2 =  Music::Join('contract_media_sale_money','music.id','=', 'contract_media_sale_money.media_id')
        ->Join('contracts','contract_media_sale_money.contract_id','=', 'contracts.id')
        ->where('contract_media_sale_money.contract_id',$this->contract_id)
        ->select('contract_media_sale_money.*','music.name','music.singer','contract_media_sale_money.start_time', 'music.tac_gia'
        ,'contract_media_sale_money.end_time','contract_number');
        if(trim($this->searchTerm1)){
            $data2->where('music.name', 'like', '%'.trim($this->searchTerm1).'%')->orWhere('music.singer','like','%'.trim($this->searchTerm1).'%');
        }
        return $data2;   
    }
    public function isCheckMusicSell($idMusic)
    {
        return in_array($idMusic,$this->checkMusicSell);
    }

    public function updatedCheckMusicSell($value, $status){
        if(!$status){
            $data2 =  Music::Join('contract_media_sale_money','music.id','=', 'contract_media_sale_money.media_id')
            ->Join('contracts','contract_media_sale_money.contract_id','=', 'contracts.id')
            ->where('contract_media_sale_money.contract_id',$this->contract_id)
            ->select('contract_media_sale_money.*','music.name','music.singer','contract_media_sale_money.start_time', 'music.tac_gia'
            ,'contract_media_sale_money.end_time','contract_number');
            if(trim($this->searchTerm1)){
                $data2->where('music.name', 'like', '%'.trim($this->searchTerm1).'%')->orWhere('music.singer','like','%'.trim($this->searchTerm1).'%');
            }
            $this->selectAll=(count($value)==count($data2->get()))?true:false;
        } 
        else{
            // nếu có search
            $queryArray=$this->musicsQuery->pluck('id')->map(function($item) {
                return (string)$item;
            } )->toArray();
            // nếu có selectAll thì lấy hết queryArr không thì lấy phần giao của querry và checkMusicSell hiện tại
            $this->checkMusicSell = $this->selectAll?$queryArray:array_intersect($this->checkMusicSell, $queryArray);
        }
    }
    public function storeMusicSale($data)
    {
        $this->music = $data['listMusic'];
        $startTime = '';
        $endTime = '';
        if($data['listMusic']){
            $musics = Music::whereIn('id', $data['listMusic'])->get();
            foreach($musics as $music){
                if($music->start_time&&$music->start_time!='0000-00-00'){
                    if($startTime){
                        $startTime = ($music->start_time<$startTime)?$music->startTime:$music->start_time;
                    }
                    else{
                        $startTime = $music->start_time;
                    }
                }
            }
            foreach($musics as $music){
                if($music->end_time!='0000-00-00'){
                    if($endTime){
                        $endTime = ($music->end_time>$endTime)?$endTime:$music->end_time;
                    }
                    else{
                        $endTime = $music->end_time;
                    }
                }
            }
        }
        // dd($startTime);
        $validator = Validator::make($data, [
            'listMusic' => 'required|array',

            'choDocQuyen' => 'nullable|boolean',
            'choTacQuyen' => 'nullable|boolean',
            'soDocQuyen' => 'nullable|boolean',
            'soTacQuyen' => 'nullable|boolean',
            'sale3rd' => 'nullable|boolean',
            'startTime' => ($data['endTime']&&$startTime)?'required|date|after_or_equal:'.$startTime:( ($data['endTime'])?'required|date':( ($startTime)? 'nullable|date|after_or_equal:'.$startTime: 'nullable|date')) ,
            'endTime' => ($data['startTime']&&$endTime)?'required|date|after_or_equal:startTime|before_or_equal:'.$endTime:( ($data['startTime'])?'required|after_or_equal:startTime|date':( ($endTime)? 'nullable|date|before_or_equal:'.$endTime: 'nullable|date'))
        ], [
            'startTime.required' => "Thời gian bắt đầu bắt buộc",
            'endTime.required' => "Thời gian kết thúc bắt buộc",
            'endTime.after_or_equal' => 'Thời gian kết thúc phải lớn hơn hoặc bằng thời gian bắt đầu',
            'endTime.before_or_equal' => 'Thời gian kết thúc phải nhỏ hơn hoặc bằng thời gian kết thúc bản quyền của bài hát ('.reFormatDate($endTime, 'd-m-Y').')',
            'startTime.after_or_equal' => 'Thời gian bắt đầu phải lớn hơn hoặc bằng thời gian bắt đầu bản quyền của bài hát ('.reFormatDate($startTime, 'd-m-Y').')',
        ], [
            'listMusic' => 'Danh sách bài hát',
            'endTime' => 'Thời gian kết thúc'
        ])->validate();

        $contract = Contract::find($this->contract_id);      
        $startTime = $contract->start_time?reFormatDate($contract->start_time, 'd-m-Y'):'';
        $endTime = $contract->end_time?reFormatDate($contract->end_time, 'd-m-Y'):'';
        $validator = Validator::make($data, [
            'startTime' => ($data['endTime']&&$startTime)?'required|date|after_or_equal:'.$startTime:( ($data['endTime'])?'required|date':( ($startTime)? 'nullable|date|after_or_equal:'.$startTime: 'nullable|date')) ,
            'endTime' => ($data['startTime']&&$endTime)?'required|date|after_or_equal:startTime|before_or_equal:'.$endTime:( ($data['startTime'])?'required|after_or_equal:startTime|date':( ($endTime)? 'nullable|date|before_or_equal:'.$endTime: 'nullable|date'))
        ], [
            'startTime.required' => "Thời gian bắt đầu bắt buộc",
            'endTime.required' => "Thời gian kết thúc bắt buộc",
            'endTime.after_or_equal' => 'Thời gian kết thúc phải lớn hơn hoặc bằng thời gian bắt đầu',
            'endTime.before_or_equal' => 'Thời gian kết thúc phải nhỏ hơn hoặc bằng thời gian kết thúc bản quyền của hợp đồng ('.reFormatDate($endTime, 'd-m-Y').')',
            'startTime.after_or_equal' => 'Thời gian bắt đầu phải lớn hơn hoặc bằng thời gian bắt đầu bản quyền của hợp đồng ('.reFormatDate($startTime, 'd-m-Y').')',
        ])->validate();

        try{
            Log::debug('ContractMediaMusicTable store');        
            $startTime = '';
            $endTime ='';
            if($data['startTime']) $startTime = reFormatDate($data['startTime'], 'Y-m-d');
            if($data['endTime']) $endTime = reFormatDate($data['endTime'], 'Y-m-d'); 
            foreach ($data['listMusic'] as $musicId) {
                $music = Music::find($musicId);
                $startTime = ($startTime)?$startTime:$music->start_time;
                $endTime = ($endTime)?$endTime:$music->end_time;
                $contract_sale_money = ContractMediaSaleMoney::create([
                    'contract_id' => $this->contract_id,
                    'media_id' => $musicId,
                    'start_time' => $startTime, 
                    'end_time'=> $endTime, 
                    'category_id' => 1, //nhac
                    'cho_tac_quyen' => $data['choTacQuyen'] ? 1 : 0,
                    'cho_doc_quyen' => $data['choDocQuyen']  ? 1 : 0,
                    'so_tac_quyen' => $data['soTacQuyen']  ? 1 : 0,
                    'so_doc_quyen' => $data['soDocQuyen'] ? 1 : 0,
                    'sale_3rd' => $data['sale3rd'] ? 1 : 0,
                    'ma_nhac_cho' => $data['maNhacCho'],
                    'name_khong_dau' => $data['nameKhongDau'],
                    'singer_khong_dau' => $data['singerKhongDau'],
                ]);
            }
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Bán bài hát thành công (" . count($data['listMusic']).")" ] );
            $this->resetInput();
            $this->emit('close-media-music-modal-sale');
            $this->emit('clear_check_box_sale');
            $this->searchTerm = '';
        }
        catch (\Exception $exception) {
            DB::rollBack();
            dd($exception->getMessage());
            Log::error('Message: ' . $exception->getMessage() . ', Line: ' . $exception->getLine());
        }
    }
    public function resetInput(){
        $this->start_time = null;
        $this->end_time = null;
        $this->money = null;
        $this->musicID = null;
        $this->musicID = null;
        $this->names = [];
        $this->music = null;
        $this->updateMode = false;
    }
     public function openModal()
     {
         $this->emit('show');
     }
    public function checkbox($var){
        $this->$var = !$this->$var;
        // $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => $this->$var ] );
    }
    public function levelClicked(){
        // $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => $this->level ] );
    }
    public function example(){
        return Storage::download('public/musicSale.xlsx');
    }
    public function getError($data){
        $today = date("d-m-Y");
        return Excel::download(new MusicSaleExport($data), 'Music_sale_error('.$today.').xlsx');
    }
    public function editId($id){
        $this->editId = $id;
        $contract = ContractMediaSaleMoney::find($id);
        $this->emit('setTimeMusicSale',reFormatDate($contract->start_time), reFormatDate($contract->end_time));
        $this->updateMode = true;
    }
    public function editMusicSale($data){
        $this->updateMode=true;
        $music = ContractMediaSaleMoney::join('music','music.id','contract_media_sale_money.media_id')->where('contract_media_sale_money.id',$this->editId)->select('music.*')->get()->first();
        $startTime = ($music->start_time&&$music->start_time!='00-00-0000')?$music->start_time:'';
        $endTime = ($music->end_time&&$music->end_time!='00-00-0000')?$music->end_time:'';        
        // dd($startTime);
        $validator = Validator::make($data, [
            'startTime' => ($data['endTime']&&$startTime)?'required|date|after_or_equal:'.$startTime:( ($data['endTime'])?'required|date':( ($startTime)? 'nullable|date|after_or_equal:'.$startTime: 'nullable|date')) ,
            'endTime' => ($data['startTime']&&$endTime)?'required|date|after_or_equal:startTime|before_or_equal:'.$endTime:( ($data['startTime'])?'required|after_or_equal:startTime|date':( ($endTime)? 'nullable|date|before_or_equal:'.$endTime: 'nullable|date'))
        ], [
            'startTime.required' => "Thời gian bắt đầu bắt buộc",
            'endTime.required' => "Thời gian kết thúc bắt buộc",
            'endTime.after_or_equal' => 'Thời gian kết thúc phải lớn hơn hoặc bằng thời gian bắt đầu',
            'endTime.before_or_equal' => 'Thời gian kết thúc phải nhỏ hơn hoặc bằng thời gian kết thúc bản quyền của bài hát ('.reFormatDate($endTime, 'd-m-Y').')',
            'startTime.after_or_equal' => 'Thời gian bắt đầu phải lớn hơn hoặc bằng thời gian bắt đầu bản quyền của bài hát ('.reFormatDate($startTime, 'd-m-Y').')',
        ], [])->validate();

        $contract = ContractMediaSaleMoney::join('contracts','contracts.id','contract_media_sale_money.contract_id')->where('contract_media_sale_money.id',$this->editId)->select('contracts.*')->get()->first();   
        $startTime = $contract->start_time?reFormatDate($contract->start_time, 'd-m-Y'):'';
        $endTime = $contract->end_time?reFormatDate($contract->end_time, 'd-m-Y'):'';
        $validator = Validator::make($data, [
            'startTime' => ($data['endTime']&&$startTime)?'required|date|after_or_equal:'.$startTime:( ($data['endTime'])?'required|date':( ($startTime)? 'nullable|date|after_or_equal:'.$startTime: 'nullable|date')) ,
            'endTime' => ($data['startTime']&&$endTime)?'required|date|after_or_equal:startTime|before_or_equal:'.$endTime:( ($data['startTime'])?'required|after_or_equal:startTime|date':( ($endTime)? 'nullable|date|before_or_equal:'.$endTime: 'nullable|date'))
        ], [
            'startTime.required' => "Thời gian bắt đầu bắt buộc",
            'endTime.required' => "Thời gian kết thúc bắt buộc",
            'endTime.after_or_equal' => 'Thời gian kết thúc phải lớn hơn hoặc bằng thời gian bắt đầu',
            'endTime.before_or_equal' => 'Thời gian kết thúc phải nhỏ hơn hoặc bằng thời gian kết thúc bản quyền của hợp đồng ('.reFormatDate($endTime, 'd-m-Y').')',
            'startTime.after_or_equal' => 'Thời gian bắt đầu phải lớn hơn hoặc bằng thời gian bắt đầu bản quyền của hợp đồng ('.reFormatDate($startTime, 'd-m-Y').')',
        ])->validate();

        // lưu trữ
        Log::debug('ContractMediaMusicTable edit');        
        $startTime = '';
        $endTime ='';
        if($data['startTime']) $startTime = reFormatDate($data['startTime'], 'Y-m-d');
        if($data['endTime']) $endTime = reFormatDate($data['endTime'], 'Y-m-d'); 
        $music = Music::find($this->editId);
        $startTime = ($startTime)?$startTime:$music->start_time;
        $endTime = ($endTime)?$endTime:$music->end_time;
        ContractMediaSaleMoney::find($this->editId)->update([
            'start_time' => $startTime, 
            'end_time'=> $endTime, 
            'cho_tac_quyen' => $data['choTacQuyen'] ? 1 : 0,
            'cho_doc_quyen' => $data['choDocQuyen']  ? 1 : 0,
            'so_tac_quyen' => $data['soTacQuyen']  ? 1 : 0,
            'so_doc_quyen' => $data['soDocQuyen'] ? 1 : 0,
            'ma_nhac_cho' => $data['maNhacCho'],
            'name_khong_dau' => $data['nameKhongDau'],
            'singer_khong_dau' => $data['singerKhongDau'],
        ]);
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Chỉnh sửa bán bài hát thành công"] );
        $this->resetInput();
        $this->emit('close-media-music-modal-edit');
    }
    public function export(){
        $today = date("d_m_Y");
        $this->dispatchBrowserEvent('setSelect2');
        return Excel::download(new MusicSaleDataExport($this->contract_id, $this->searchTerm1), 'NhacBan-'.$today.'.xlsx');
        // $this->emit('close-modal-exampleModalMusicSale');
    }
}
