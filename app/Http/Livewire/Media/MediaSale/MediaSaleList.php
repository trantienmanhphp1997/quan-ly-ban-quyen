<?php

namespace App\Http\Livewire\Media\MediaSale;
use App\Models\Contract;
use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Livewire\Base\BaseLive;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use App\Models\ContractMediaSaleMoney;

class MediaSaleList extends BaseLive
{
    public $searchTerm;
    public $searchCategory;
    public $disabledCategory = false;
    public $searchName;
    public $level = [];
    public $count_time;

    public function mount(){
        if(auth()->user()->category_id) {
            $this->disabledCategory = true;
            $this->searchCategory = auth()->user()->category_id;
        }
    }

    public function render(Request $request )
    {
        if($this->reset){
            $this->searchCategory = null;
            $this->searchTerm = null;
            $this->searchName = null;
            $this->reset = false;
        }
        $category = Category::all();
        $page = \Request::get('page') ? \Request::get('page') : 1;
        $perPage = Config::get('app.per_page') ? Config::get('app.per_page') : 2;
        $startLimit = $perPage * ($page - 1);
        $endLimit = $perPage * $page;
        $query = ContractMediaSaleMoney::query();
        $query->leftJoin('contracts', 'contract_media_sale_money.contract_id', '=', 'contracts.id');
        if(auth()->user()->is_manager == 0) $query->where('contracts.admin_id', auth()->user()->id);

        $query->leftJoin('music', function ($join) {
            $join->on('contract_media_sale_money.media_id', '=', 'music.id')
                ->where('contract_media_sale_money.category_id', '=', 1 );
        });
        $query->leftJoin('films', function ($join) {
            $join->on('contract_media_sale_money.media_id', '=', 'films.id')
                 ->where('contract_media_sale_money.category_id', '=', 2);
        });
        $query->leftJoin('categories', function ($join) {
            $join->on('contract_media_sale_money.category_id', '=', 'categories.id');
        });
        $query->select('*', 'contract_media_sale_money.money as data_money', 'contract_media_sale_money.start_time as data_start_time', 'contract_media_sale_money.end_time as data_end_time', 'contract_media_sale_money.contract_id as data_contract_id', 'categories.name as category', 'music.name as music_name', 'films.vn_name as film_name', 'contract_media_sale_money.id as id');
        if($this->searchTerm){
           $query->where('contracts.contract_number','like','%'.trim($this->searchTerm).'%');
        }
        if($this->searchCategory){
            $query->where('contract_media_sale_money.category_id','=',trim($this->searchCategory));
        }
        if($this->searchName){
           $query->where('music.name','like','%'.trim($this->searchName).'%')
            ->orWhere('films.vn_name','like','%'.trim($this->searchName).'%');
        }

        if($this->searchCategory){
            $query->where('contract_media_sale_money.category_id','=',trim($this->searchCategory));
        }
        $data = $query->orderBy('data_contract_id','asc')->offset($startLimit)->limit($perPage)->paginate($perPage);

        $model = config("common.permission.module.contract-media-sale-money", "contract-media-sale-money");
        return view('livewire.media.media-sale.media-sale-list', compact('data', 'category', 'model'))
        ->with('i', ($request->input('page', 1) - 1) * $perPage);
    }

    public function updatedSearchName(){
        $this->resetPage();
    }

}
