<?php

namespace App\Http\Livewire\Media\Manage;
use App\Models\Contract;
use App\Models\Category;
use Carbon\Carbon;
use App\Exports\ManageExampleExport;
use Illuminate\Support\Facades\DB;
use App\Http\Livewire\Base\BaseLive;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use App\Models\FilmBackup;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\Exports\FilmBackupExport;
use Excel;
class Manage extends BaseLive
{

    public $searchTerm;
    public $searchDriver;
    protected $listeners = ['getError'];
    public function render(Request $request)
    {
        if($this->reset){
            $this->searchDriver = null;
            $this->searchTerm = null;
            $this->reset = false;
        }
        $page = \Request::get('page') ? \Request::get('page') : 1;
        $query = FilmBackup::query();

        $perPage = Config::get('app_per_page') ? Config::get('app_per_page') : 10;
        $startLimit = $perPage * ($page - 1);
        $endLimit = $perPage * $page;
        $query->where('driver','like','%'.trim($this->searchDriver).'%');      
        $query->where('vn_name','like','%'.trim($this->searchTerm).'%');
        $data = $query->orderBy('id','desc')->offset($startLimit)->limit($perPage)->paginate($perPage);
        $model = config("common.permission.module.manage", "manage");
        return view('livewire.media.manage.manage', compact('data','model'))->with('i', ($request->input('page', 1) - 1) * $perPage);
    }

    public function updatedSearchTerm(){
        $this->resetPage();
    }

    public function updatedSearchDriver(){
        $this->resetPage();
    }

    public function example(){
        return Storage::download('public/manage.xlsx');
        // return Excel::download(new ManageExampleExport, 'manageExample.xlsx');
    }
    public function getError($data){
        $today = date("d-m-Y");
        return Excel::download(new ManageExampleExport($data), 'Manage_error('.$today.').xlsx');
    }
    public function delete(){
        if(Auth::user()->hasAnyRole('manage-delete|administrator')) {
                //Log::debug('FilmTable '.$this->deleteId);
                FilmBackup::where('id',$this->deleteId)->delete();
                $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Xóa tư liệu phim thành công" ] );
                // session()->flash('live_success', 'Xóa tư liệu phim thành công');
                return;
        }
        abort(403, 'Unauthorized action.');
    }

    public function export(){
        $today = date("d_m_Y");
        return Excel::download(new FilmBackupExport($this->searchDriver, $this->searchTerm), 'filmBackup-'.$today.'.xlsx');
    }
}
