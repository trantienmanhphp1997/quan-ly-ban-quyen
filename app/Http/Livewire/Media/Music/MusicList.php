<?php

namespace App\Http\Livewire\Media\Music;

use Carbon\Carbon;
use Livewire\Component;
use App\Models\Music;
use App\Models\Contract;
use App\Models\ActionLog;
use App\Models\TypeFee;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Log;
use App\Http\Livewire\Base\BaseLive;
use DB;
use App\Exports\MusicExport;
use App\Exports\MusicExampleExport;
use Excel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class MusicList extends BaseLive

{
    use AuthorizesRequests;
    public $searchName;
    public $searchSinger;
    public $searchMusician;
    public $searchStatus;
    public $checkMusic=[];
    public $selectAll=false;
    public $level = [];
    public $count_time;
    public $deleteId='';
    public $typeFee;
    public $threeMonth;
    public $twoMonth;
    public $oneMonth;
    public $oneWeek;
    public $contract_number;
    public $updateContractNumber;
    public $updateQuyenTacGia;
    protected $listeners = ['getError'];

    public $userID;
    public $dataBuySell = [];
    public function mount(){
        $this->contract_number = \Request::get('contract_number');
        $this->userID = Auth()->id();
    }

    public function endTimeContract($value){
        if($value==7)
            $count = Music::whereBetween('end_time',[Carbon::now()->format('Y-m-d'),Carbon::now()->addDay($value)->format('Y-m-d')])->count();
        else
            $count = Music::whereBetween('end_time',[Carbon::now()->addMonth($value-1)->addDays(1)->format('Y-m-d'),Carbon::now()->addMonth($value)->format('Y-m-d')])->count();
        return $count;
    }

    public function updatedSelectAll($value,$idMusic)
    {
        if($value)
        {
            $this->checkMusic=Music::query()->pluck('id')->map(function($item){
                return (string) $item;
            })->toArray();
        }
        else{
            $this->checkMusic=[];
        }
    }

    public function updatedCheckMusic()
    {
        $this->selectAll = false;
    }

    public function isCheckMusic($idMusic)
    {
        return in_array($idMusic,$this->checkMusic);
    }

    public function render()
    {
        // dd(Carbon::now()->format('d/m/Y'));
        if($this->reset){
            $this->searchName = null;
            $this->searchSinger = null;
            $this->searchMusician = null;
            $this->searchStatus = null;
            $this->count_time = null;
            $this->reset = false;
        }
        $count_time = $this->count_time;
        $searchName = trim($this->searchName);
        $searchSinger = trim($this->searchSinger);
        $searchMusician = trim($this->searchMusician);
        //Log::debug('An informational message.' . $searchTerm);

        $perPage = Config::get('app.per_page') ? Config::get('app.per_page') : 10;
        $query = Music::where(function ($query) {
                $query->where('music.end_time', '>=', date('Y-m-d'));
                $query->orWhere('music.end_time', null);
        });
        if(auth()->user()->is_manager == 0 && auth()->user()->status_media == 1){
            $query->where('music.admin_id', auth()->id());
        }


        if($this->searchStatus==1)
            $query = Music::where('music.end_time', '<', date('Y-m-d'));
        elseif($this->searchStatus==2)
            $query = Music::onlyTrashed();
        elseif($this->searchStatus==3)
            $query = Music::withTrashed();
        if($this->contract_number)
            $query->where(function($query){
                 $query->where('contract_number1','=',$this->contract_number);
                 $query->orWhere('contract_number4','=',$this->contract_number);
                 $query->orWhere('tac_gia','=',$this->contract_number);
            });

        if($searchName)
            $query->where('name','LIKE','%'.$searchName.'%');
        if($searchSinger)
            $query->where('singer','LIKE','%'.$searchSinger.'%');
        if($searchMusician)
            $query->where('musician','LIKE','%'.$searchMusician.'%');

        if($count_time==7)
            $query->whereBetween('music.end_time',[Carbon::now()->format('Y-m-d'),Carbon::now()->addDay($count_time)->format('Y-m-d')]);
        if($count_time<7&&$count_time>0)
            $query->whereBetween('music.end_time',[Carbon::now()->addMonth($count_time-1)->addDays(1)->format('Y-m-d'),Carbon::now()->addMonth($count_time)->format('Y-m-d')]);
        $this->threeMonth = $this->endTimeContract(3);
        $this->twoMonth = $this->endTimeContract(2);
        $this->oneMonth = $this->endTimeContract(1);
        $this->oneWeek = $this->endTimeContract(7);
        if(!$this->typeFee){
            $this->typeFee = TypeFee::pluck('name','id');
        }

        $query->leftjoin('contracts', function($join)
        {
            $join->on('music.contract_id', '=', 'contracts.id');
        });
        $query->leftjoin('contracts as ct', function($join)
        {
            $join->on('music.tac_gia', '=', 'ct.contract_number');
        });
        $query->select('music.*', 'contracts.contract_number as contract_number','ct.id as contract_id_tac_gia');
        $data = $query->orderBy('deleted_at')->orderBy('music.id','desc')->paginate($perPage);
        // yêu cầu mới
        $idMusicInList = $data->pluck('id')->toArray();
        $dataBuySellOfUser = DB::table('medias_sell_buy')
            ->where('user_id',$this->userID)
            ->where('category_id',1)
            ->whereIn('media_id',$idMusicInList)
            ->select('media_id','type','id','name')
            ->get();
        $this->dataBuySell = [
            'buy' => [],
            'sell' => [],
            'name' => [],
        ];
        $categoryID=[
            1 => 'buy',
            2 => 'sell',
            3 => 'name'
        ];
        foreach($dataBuySellOfUser as $key => $value){
            $this->dataBuySell[$categoryID[$value->type]][$value->id] = $value->media_id;
            $this->dataBuySell[$categoryID[3]][$value->id] = $value->name;
        }
        return view('livewire.media.music.music-list', ['data' => $data,'model' =>config("common.permission.module.music", "music")]);
    }
    public function query1($count_time){
        $this->count_time = $count_time;
        $this->resetPage();
    }

    public function updatedSearchName(){
        $this->resetPage();
    }

    public function updatedSearchSinger(){
        $this->resetPage();
    }

    public function updatedSearchMusician(){
        $this->resetPage();
    }

    public function updatedSearchStatus(){
        $this->resetPage();
    }

    public function deleteId($id){
        $this->deleteId=$id;
    }
    public function delete(){
        if(Auth::user()->hasAnyRole('music-delete|administrator')) {
            $musics = DB::table('contract_media_sale_money')->where('category_id',1)->where('media_id', $this->deleteId)->get();
            if(count($musics)){
                $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => "Không được phép xóa nhạc đã bán." ] );
                return;
            }
            else {
                Log::debug('FilmTable '.$this->deleteId);
                $music = Music::find($this->deleteId);
                ActionLog::writeActionLog($this->deleteId, $music->getTable(), config("common.actions.deleted", "deleted"), get_class($this), "Xóa", $music->name, null, null);
                // ActionLog::where('record_id', '=', $this->deleteId)->where('model', '=', 'music')->delete();
                $music->update(['contract_id' => null,
                                'ghi_am' =>null,
                                'tac_gia' => null,
                                'contract_number1' => null,
                                'contract_number4' => null,]);
                $music->delete();
                $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Xóa nhạc thành công." ] );
                return;
            }
        }
        abort(403, 'Unauthorized action.');
    }

    public function export()
    {
        $today = date("d_m_Y");
        return Excel::download(new MusicExport($this->searchName, $this->searchSinger, $this->searchMusician, $this->searchStatus, $this->count_time), 'music-'.$today.'.xlsx');
        // $this->dispatchBrowserEvent('close-modal');
    }
    public function example()
    {
        return Storage::download('public/music.xlsx');
        // return Excel::download(new MusicExampleExport, 'musicExample.xlsx');
    }
    public function getError($data){
        $today = date("d-m-Y");
        return Excel::download(new MusicExampleExport($data), 'Music_error('.$today.').xlsx');
    }

    public function saleId($musicID, $name){
        Log::info("Nhac được ban đã lưu vào session thành công" . $musicID);
        $mediaSellBuyID = array_search($musicID,$this->dataBuySell['sell']);
        if($mediaSellBuyID){
            DB::table('medias_sell_buy')->where('id',$mediaSellBuyID)->delete();
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Xóa bài hát khỏi danh sách bán hàng." ] );
        }
        else {
            DB::table('medias_sell_buy')->insert([
                'media_id' => $musicID,
                'user_id' => $this->userID,
                'type' => 2,   // bán
                'category_id' => 1, //nhạc
                'name' => $name, // tên phim
            ]);
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Thêm bài hát vào danh sách bán hàng." ] );
        }
    }

    public function buyId($musicID, $name){
        Log::info("Nhac được mua đã lưu vào session thành công" . $musicID);
        $mediaSellBuyID = array_search($musicID,$this->dataBuySell['buy']);
        if($mediaSellBuyID){
            DB::table('medias_sell_buy')->where('id',$mediaSellBuyID)->delete();
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Xóa bài hát khỏi danh sách mua hàng." ] );
        }
        else {
            DB::table('medias_sell_buy')->insert([
                'media_id' => $musicID,
                'user_id' => $this->userID,
                'type' => 1,   // mua
                'category_id' => 1, //nhạc
                'name' => $name, // tên nhạc
            ]);
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Thêm bài hát vào danh sách mua hàng." ] );
        }
    }
    public function restore($id){
        Music::withTrashed()->where('id', $id)->restore();
        ActionLog::writeActionLog($id, 'music', config("common.actions.restore", "restore"), get_class($this), "Khôi phục", null, null, null);
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Khôi phục bài hát thành công." ] );
    }

    public function update(){
        $this->updateContractNumber = trim($this->updateContractNumber);
        $this->updateQuyenTacGia = trim($this->updateQuyenTacGia);
        $data['updateContractNumber'] = $this->updateContractNumber;
        $data['updateQuyenTacGia'] = $this->updateQuyenTacGia;
        $checkUniqueMusicInContractNumber = 0; // khởi tạo giá trị cho biến check
        foreach($this->checkMusic as $musicID){
            $music = Music::find($musicID);
            $musicUnique = Contract::join('music','music.contract_id','contracts.id')->where('contract_number',$this->updateContractNumber)->where('music.id','!=',$musicID)->where('music.name',$music->name)->get();
            if(count($musicUnique)) {
                $checkUniqueMusicInContractNumber = 1;
                break;
            }
        }
        if($checkUniqueMusicInContractNumber) {
            $this->addError('updateContractNumber', 'Hợp đồng '.$data['updateContractNumber'].' đã tồn tại bài hát '.$music->name);
            return;
        }
        $validator = Validator::make($data, [
            'updateContractNumber' => 'nullable|my_contract|type_contract:1,1|active_contract',
            'updateQuyenTacGia' => 'nullable|my_contract|type_contract:4|active_contract',
        ],[],[
        ])->validate();
        foreach($this->checkMusic as $musicID){
            $old_value =null;
            $new_value = null;
            $music = Music::find($musicID);

            if($this->updateContractNumber && $this->updateContractNumber!=$music->ghi_am){
                $old_value['ghi_am'] = $music->ghi_am;
                $new_value['ghi_am'] = $this->updateContractNumber;
            }
            if($this->updateQuyenTacGia && $this->updateQuyenTacGia!=$music->tac_gia) {
                $old_value['tac_gia'] = $music->tac_gia;
                $new_value['tac_gia'] = $this->updateQuyenTacGia;
            }
            if($old_value)
            ActionLog::writeActionLog($musicID, $music->getTable(), config("common.actions.updated", "updated"), get_class($this), "Sửa", $old_value, $new_value, null);
        }

        if($this->updateContractNumber){
            $count = Contract::where('contract_number', $this->updateContractNumber)->count();
            $contractBuy = Contract::firstOrCreate([
                'contract_number' => $this->updateContractNumber,
            ],
            [
                'type' => 1,
                'category_id' => 1,
            ]);
            if($count==0)
                ActionLog::writeActionLog($contractBuy->id, $contractBuy->getTable(), config("common.actions.created", "created"), null, "Tạo mới", null, null, null);

            Music::whereIn('id', $this->checkMusic)->update(array('contract_id' => $contractBuy->id, 'ghi_am' => $contractBuy->contract_number));
        }
        if($this->updateQuyenTacGia){
            $count = Contract::where('contract_number', $this->updateQuyenTacGia)->count();
            $contract = Contract::firstOrCreate([
                'contract_number' => $this->updateQuyenTacGia,
            ],
            [
                'type' => 4,
                'category_id' => 1,
            ]);
            if($count==0)
                ActionLog::writeActionLog($contract->id, $contract->getTable(), config("common.actions.created", "created"), null, "Tạo mới", null, null, null);
            Music::whereIn('id', $this->checkMusic)->update(array('tac_gia' => $this->updateQuyenTacGia));
        }

        $this->emit('close-update-music-modal');
         ;
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Cập nhật quyền ".count($this->checkMusic)." bài hát thành công" ] );
    }

    public function resetInput(){
        $this->updateContractNumber = null;
        $this->updateQuyenTacGia = null;
        $this->checkMusic = [];
        $this->selectAll = false;
        $this->resetValidation();
    }
}
