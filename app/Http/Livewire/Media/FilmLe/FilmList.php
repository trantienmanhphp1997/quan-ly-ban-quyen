<?php

namespace App\Http\Livewire\Media\FilmLe;

use App\Http\Livewire\Base\BaseLive;

class FilmList extends BaseLive
{
    public function render()
    {
        return view('livewire.media.film-le.film-list');
    }
}
