<?php

namespace App\Http\Livewire\Media\Film;
use App\Models\Film;
use App\Models\FilmList;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Config;
use App\Http\Livewire\Base\BaseLive;
use Excel;
use App\Exports\FilmListExport;

class FilmListChildren extends BaseLive
{
    public $searchName;
    public $searchVNName;
    public $searchActor;
    public $searchParent;
    public $level = [];
    public $deleteId='', $buyId='', $saleId='';
    public $count_time;
    public $filmId;

    public function endTimeContract($value){
        $count=Film::whereBetween('end_time',[Carbon::now(),$value])->count();
        return $count;
    }
    public function render()
    {
        if($this->reset){
            $this->searchName = null;
            $this->searchVNName = null;
            $this->searchActor = null;
            $this->searchParent = null;
            $this->reset = false;
        }
        $searchName = trim($this->searchName);
        $searchVNName = trim($this->searchVNName);
        $searchActor = trim($this->searchActor);
        $searchParent = trim($this->searchParent);

        // $query = FilmList::search($searchTerm, null, true);
        $query = FilmList::query();
        if($this->filmId)
        {
            $query->where('film_id', '=', $this->filmId);
        }
        $query->leftJoin('films', 'film_lists.film_id', '=', 'films.id')->whereNull('films.deleted_at');
        if($this->searchName)
        $query->where('film_lists.name','LIKE','%'.$searchName.'%');
        if($this->searchVNName)
        $query->where('film_lists.vn_name','LIKE','%'.$searchVNName.'%');
        if($this->searchActor)
        $query->where('film_lists.actor_name','LIKE','%'.$searchActor.'%');
        if($this->searchParent)
            $query->where('films.vn_name','LIKE','%'.$searchParent.'%');
        $query->select('film_lists.*',DB::raw('films.vn_name as parent_name'));  
        $data = $query->orderBy('film_lists.id','desc')->paginate('10');
        return view('livewire.media.film.film-le', ['data' => $data,
            'model' =>config("common.permission.module.phim-le", "phim-le")]);
    }
    public function queryFilm(){
        $query = DB::table('film_list');
        $query->leftjoin('contracts', function($join)
        {
            $join->on('films.contract_id', '=', 'contracts.id');
        });
        $query = $query->select('films.*', DB::raw('contracts.contract_number as con_num'));
        return $query;
    }
    public function query1($count_time){
        $this->count_time = $count_time;
    }

    public function updatedSearchName(){
        $this->resetPage();
    }

    public function updatedSearchVNName(){
        $this->resetPage();
    }

    public function updatedSearchActor(){
        $this->resetPage();
    }

    public function updatedSearchParent(){
        $this->resetPage();
    }
    public function deleteId($id){
        Log::debug('FilmTable '.$id);
        $this->deleteId=$id;
    }
    public function delete(){
        Log::debug('FilmTable '.$this->deleteId);
        FilmList::where('id',$this->deleteId)->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Xóa phim lẻ thành công." ] );
        // session()->flash('live_success', 'Xóa thành công.');
    }
    public function buyId($filmID, $name){
        Log::info("Phim được mua đã lưu vào session thành công" . $filmID);
        $this->buyId=$filmID;
        // nếu $filmID khác null
        $arrayFilmID = $filmID;
        $arrayName = $name;
        if($filmID){
            // nếu có session rồi
            if(session()->has('arrayFilmIDBuy')){
                $arrayFilmID = session('arrayFilmIDBuy');
                $arrayName = session('arrayFilmNameBuy');
                $array = explode(' ', $arrayFilmID);
                if(!in_array($filmID, $array) ){
                    $arrayFilmID= $arrayFilmID.' '.$filmID;
                    $arrayName= $arrayName.'__'.$name;
                    session()->put('arrayFilmIDBuy', $arrayFilmID);
                    session()->put('arrayFilmNameBuy', $arrayName);
                }
            }
            else {
                session()->put('arrayFilmIDBuy', $filmID);
                session()->put('arrayFilmNameBuy', $arrayName);
            }
        }
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Thêm phim vào danh sách mua hàng." ] );
    }
    public function saleId($filmID, $name){
        Log::info("Phim được mua đã lưu vào session thành công" . $filmID);
        $this->buyId=$filmID;
        // nếu $filmID khác null
        $arrayFilmID = $filmID;
        $arrayName = $name;
        if($filmID){
            // nếu có session rồi
            if(session()->has('arrayFilmIDSale')){
                $arrayFilmID = session('arrayFilmIDSale');
                $arrayName = session('arrayFilmNameSale');
                $array = explode(' ', $arrayFilmID);
                if(!in_array($filmID, $array) ){
                    $arrayFilmID= $arrayFilmID.' '.$filmID;
                    $arrayName= $arrayName.'__'.$name;
                    session()->put('arrayFilmIDSale', $arrayFilmID);
                    session()->put('arrayFilmNameSale', $arrayName);
                }
            }
            else {
                session()->put('arrayFilmIDSale', $filmID);
                session()->put('arrayFilmNameSale', $arrayName);
            }
        }
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Thêm phim vào danh sách bán hàng." ] );
    }

    public function export(){
        $today = date("d_m_Y");
        return Excel::download(new FilmListExport($this->searchName, $this->searchVNName, $this->searchActor, $this->searchParent), 'film-list'.$today.'.xlsx');
    }
}
