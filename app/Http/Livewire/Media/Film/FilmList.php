<?php

namespace App\Http\Livewire\Media\Film;
use App\Component\Recursive;
use App\Models\Category;
use App\Models\Contract;
use App\Models\Film;
use App\Models\ActionLog;
use App\Models\Country;
use App\Models\RevenueType;
use Carbon\Carbon;
use App\Models\CategoryFilm;
use App\Exports\FilmExport;
use App\Exports\FilmExampleExport;
use App\Exports\FilmListExampleExport;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Livewire\Base\BaseLive;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Excel;

class FilmList extends BaseLive
{
    public $searchTerm;
    public $searchActor;
    public $searchCountry;
    public $searchPartner;
    public $searchCategory;
    public $searchStatus;
    public $searchYear;
    public $level = [];
    public $buyId='', $saleId='';
    public $count_time;
    public $threeMonth;
    public $twoMonth;
    public $oneMonth;
    public $oneWeek;
    public $categories;
    public $category;
    public $cate_film;
    public $country;
    public $listYear;
    public $checkFilms = [];
    protected $listeners = ['getError','getErrorLe'];

    public $userID;
    public $dataBuySell = [];
    public function endTimeContract($value){
        if($value==7)
            $count = Film::whereBetween('end_time',[Carbon::now()->format('Y-m-d'),Carbon::now()->addDay($value)->format('Y-m-d')])->count();
        else
            $count = Film::whereBetween('end_time',[Carbon::now()->addMonth($value-1)->addDays(1)->format('Y-m-d'),Carbon::now()->addMonth($value)->format('Y-m-d')])->count();
        return $count;
    }
    public function mount(){
        $this->userID = Auth()->id();
    }
    public function render()
    {
        if($this->reset){
            $this->searchPartner = null;
            $this->searchCategory = null;
            $this->searchTerm = null;
            $this->searchCountry = null;
            $this->searchStatus = null;
            $this->count_time = null;
            $this->searchYear = null;
            $this->reset = false;
            $this->checkFilms = [];
            $this->emit('setTitleCountry');
        }
        // dd($this->searchCountry);
        // dd($this->searchCountry);
        $this->threeMonth = $this->endTimeContract(3);
        $this->twoMonth = $this->endTimeContract(2);
        $this->oneMonth = $this->endTimeContract(1);
        $this->oneWeek = $this->endTimeContract(7);

        $this->cate_film = CategoryFilm::all();
        $this->country = Country::orderBy('name','asc')->get();

        if(isset($this->categories)){
            $this->categories=DB::table('category_films')
            ->leftjoin('film_list_categories', function($join)
            {
                $join->on('category_films.id', '=', 'film_list_categories.id_category_film');
            })->get();
        }
        if(isset($this->category)){
            $this->category=Category::pluck('name','id');
        }
         
        $perPage = Config::get('app.per_page') ? Config::get('app.per_page') : 10;
        // query film
        $query=$this->queryFilm();
        // search film
        $query = $this->search($query);
        //paginate query
        $data = $query->orderBy('deleted_at')->orderBy('id','desc')->paginate($perPage);
        // yêu cầu mới
        $idFilmsInList = $data->pluck('id')->toArray();
        $dataBuySellOfUser = DB::table('medias_sell_buy')
            ->where('user_id',$this->userID)
            ->where('category_id',2)
            ->whereIn('media_id',$idFilmsInList)
            ->select('media_id','type','id','name')
            ->get();
        $this->dataBuySell = [
            'buy' => [],
            'sell' => [],
            'name' => [],
        ];
        $categoryID=[
            1 => 'buy',
            2 => 'sell',
            3 => 'name'
        ];
        foreach($dataBuySellOfUser as $key => $value){
            $this->dataBuySell[$categoryID[$value->type]][$value->id] = $value->media_id;
            $this->dataBuySell[$categoryID[3]][$value->id] = $value->name;
        }
        return view('livewire.media.film.film-list',[
            'data'=>$data,
            'model' =>config("common.permission.module.film", "film"),
        ]);
    }
    public function search($query){
        if(auth()->user()->is_manager == 0 && auth()->user()->status_media == 1){
            $query->where('films.admin_id', auth()->id());
        }
        $count_time=$this->count_time;
        if($count_time==7)
            $query->whereBetween('films.end_time',[Carbon::now()->format('Y-m-d'),Carbon::now()->addDay($count_time)->format('Y-m-d')]);
        if($count_time<7&&$count_time>0)
            $query->whereBetween('films.end_time',[Carbon::now()->addMonth($count_time-1)->addDays(1)->format('Y-m-d'),Carbon::now()->addMonth($count_time)->format('Y-m-d')]);
        // get list year
        $this->listYear = Film::withTrashed()->where('year_create','!=',null)->select('year_create')->orderBy('year_create','desc')->distinct()->get();
        //search
        
        if($this->searchCountry){
            $query->where('films.country','=',trim($this->searchCountry));
        }
        if($this->searchPartner){
           $query->where('partners.name','like','%'.trim($this->searchPartner).'%');
        }
        if($this->searchYear){
            $query->where('films.year_create','like','%'.trim($this->searchYear).'%');
        }
        if($this->searchCategory){
            $query->join('film_list_categories', 'films.id', '=', 'film_list_categories.id_film_list');
            $query->where('film_list_categories.id_category_film','=',trim($this->searchCategory));
        }
        return $query;
    }
    public function getRevenue($parentId){
        $data=RevenueType::all();
        $recursive= new Recursive($data);
        $html=$recursive->recursive($parentId);
        return $html;
    }
    public function queryFilm(){

        $query = Film::where(function ($query) {
                $query->where('films.end_time', '>=', date('Y-m-d'));
                $query->orWhere('films.end_time', null);
        });
        if($this->searchStatus==1)
            $query = Film::where('films.end_time', '<', date('Y-m-d'));
        elseif($this->searchStatus==2)
            $query = Film::onlyTrashed();
        elseif($this->searchStatus==3)
            $query = Film::withTrashed();
        if($this->searchTerm){
            $query->where('films.vn_name','like','%'.trim($this->searchTerm).'%');
        }
        
        $query->leftjoin('contracts', function($join)
        {
            $join->on('films.contract_id', '=', 'contracts.id');
        });
        $query->leftjoin('partners', function($join)
        {
            $join->on('films.partner_name', '=', 'partners.id');
        });
        $query->leftjoin('countries', function($join)
        {
            $join->on('films.country', '=', 'countries.id');
        });
        $query = $query->select('films.*', 'contracts.contract_number as con_num',
            'countries.name as country_name','contracts.type as type','contracts.date_sign as date_sign', 'partners.name as partnerName');
        return $query;
    }
    public function query1($count_time){
        $this->count_time = $count_time;
        $this->resetPage();
    }
    public function updatedSearchTerm(){
        $this->resetPage();
    }

    public function updatedSearchPartner(){
        $this->resetPage();
    }

    public function updatedSearchCategory(){
        $this->resetPage();
    }

    public function updatedSearchCountry(){
        $this->resetPage();
    }
    public function updatedSearchStatus(){
        $this->resetPage();
    }
    public function updatedSearchYear(){
        $this->resetPage();
    }
    public function deleteAll(){
        if(Auth::user()->hasAnyRole('film-delete|administrator')) {
            // query film
            $query=$this->queryFilm();
            // search film
            $query = $this->search($query);
            // get list id film
            $query = $query->whereIn('films.id',$this->checkFilms)->pluck('id')->toArray();

            $films = DB::table('contract_media_sale_money')->where('category_id',2)->whereIn('media_id', $query)->get();
            if(count($films)){
                $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => "Không được phép xóa phim đã bán." ] );
                return;
            }
            else {
                $filmLe = DB::table('film_lists')->whereIn('film_id',$query)->get();
                if(count($filmLe)){
                    $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => "Không được phép xóa phim đã có phim lẻ." ] );
                    return;                    
                }
                foreach($query as $key=>$deleteId){
                    Log::debug('FilmTable '.$deleteId);
                    $film=Film::find($deleteId);
                    ActionLog::writeActionLog($deleteId, $film->getTable(), config("common.actions.deleted", "deleted"), get_class($this), "Xóa", $film->vn_name, null, null);
                    $film->update(['contract_id' => null]);
                    $film->delete();
                }
                $this->checkFilms = array_diff($this->checkFilms, $query);
                $this->resetPage();
                $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Xóa ".count($query)." phim được chọn thành công." ] );
                return;
            }
        }
        abort(403, 'Unauthorized action.');
    }
    public function delete(){
        if(Auth::user()->hasAnyRole('film-delete|administrator')) {
            $films = DB::table('contract_media_sale_money')->where('category_id',2)->where('media_id', $this->deleteId)->get();
            $filmLe = DB::table('film_lists')->where('film_id',$this->deleteId)->get();
            if(count($films)){
                $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => "Không được phép xóa phim đã bán." ] );
                return;
            }
            else {
                if(count($filmLe)){
                    $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => "Không được phép xóa phim đã có phim lẻ." ] );
                    return;                    
                }
                Log::debug('FilmTable '.$this->deleteId);
                $film=Film::find($this->deleteId);
                ActionLog::writeActionLog($this->deleteId, $film->getTable(), config("common.actions.deleted", "deleted"), get_class($this), "Xóa", $film->vn_name, null, null);
                // ActionLog::where('record_id', '=', $this->deleteId)->where('model', '=', 'films')->delete();
                $film->update(['contract_id' => null]);
                $film->delete();
                $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Xóa phim thành công." ] );

                return;
            }
        }
        abort(403, 'Unauthorized action.');
    }

    public function buyId($filmID, $name){
        Log::info("Phim được mua đã lưu vào session thành công" . $filmID);
        $mediaSellBuyID = array_search($filmID,$this->dataBuySell['buy']);
        if($mediaSellBuyID){
            DB::table('medias_sell_buy')->where('id',$mediaSellBuyID)->delete();
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Xóa phim khỏi danh sách mua hàng." ] );
        }
        else {
            DB::table('medias_sell_buy')->insert([
                'media_id' => $filmID,
                'user_id' => $this->userID,
                'type' => 1,   // mua
                'category_id' => 2, //phim
                'name' => $name, // tên phim
            ]);
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Thêm phim vào danh sách mua hàng." ] );
        }
    }
    public function saleId($filmID, $name){
        Log::info("Phim được mua đã lưu vào session thành công" . $filmID);
        $mediaSellBuyID = array_search($filmID,$this->dataBuySell['sell']);
        if($mediaSellBuyID){
            DB::table('medias_sell_buy')->where('id',$mediaSellBuyID)->delete();
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Xóa phim khỏi danh sách bán hàng." ] );
        }
        else {
            DB::table('medias_sell_buy')->insert([
                'media_id' => $filmID,
                'user_id' => $this->userID,
                'type' => 2,   // bán
                'category_id' => 2, //phim
                'name' => $name, // tên phim
            ]);
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Thêm phim vào danh sách bán hàng." ] );
        }
    }

    public function export(){
        $today = date("d_m_Y");
        return Excel::download(new FilmExport($this->searchTerm, $this->searchCategory, $this->searchPartner, $this->searchCountry, $this->searchStatus, $this->count_time, $this->searchYear), 'films-'.$today.'.xlsx');
    }
    public function example(){
        return Storage::download('public/film.xlsx');
        // return Excel::download(new FilmExampleExport, 'filmExample.xlsx');
    }
    public function exampleLe(){
        return Storage::download('public/phimle.xlsx');
        // return Excel::download(new FilmListExampleExport, 'filmListExample.xlsx');
    }
    public function getError($data){
        $today = date("d-m-Y");
        return Excel::download(new FilmExampleExport($data), 'Film_error('.$today.').xlsx');
    }
    public function getErrorLe($data){
        $today = date("d-m-Y");
        return Excel::download(new FilmListExampleExport($data), 'Film_le_error('.$today.').xlsx');
    }
    public function restore($id){
        Film::withTrashed()->where('id', $id)->restore();
        ActionLog::writeActionLog($id, 'films', config("common.actions.restore", "restore"), get_class($this), "Khôi phục", null, null, null);
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Khôi phục phim thành công." ] );
    }
}
