<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\models\Music;
use Illuminate\Support\Facades\Log;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Config;


class Search extends Component
{
	use WithPagination;
	public $searchTerm;
    public $level = [];
    public function render()
    {
        $searchTerm = $this->searchTerm;
		    Log::debug('An informational message.' . $searchTerm);
        $page = \Request::get('page') ? \Request::get('page') : 1;

        $perPage = Config::get('app_per_page') ? Config::get('app_per_page') : 100;
        $perPage = 10;
        $startLimit = $perPage * ($page - 1);
        $endLimit = $perPage * $page;


        $query = Music::search($searchTerm, null, true);

		$data = $query->orderBy('id','desc')->offset($startLimit)->limit($perPage)->paginate($perPage);

        return view('livewire.search', ['data' => $data]);

    }
  //   public function getRowsQueryProperty()
  //   {
  //       $query = Music::query()
  //           ->when($this->level, fn($query, $levels) => $query->level($levels));

  //       return $query;
  //   }

  //   public function getRowsProperty()
  //   {
  //       return $this->rowsQuery->paginate(10);
  //   }
  //   public function levelClicked()
		// {
		//     // now the property $this->level will contain all the selected values
		// }
        public function paginationView()
    {
        return 'livewire.common.pagination._pagination';
    }
}
