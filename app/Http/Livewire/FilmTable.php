<?php

namespace App\Http\Livewire;
use App\Models\Film;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Illuminate\Support\Facades\Log;

class FilmTable extends Component
{
    public $searchTerm;
    public $searchActor;
    public $level = [];
    public $count_time;
    public $selected_id,$vn_name, $year_create, $actor_name,$country, $category_name,$count_name,
    $count_tap,$count_hour,$partner_name,$end_time,$start_time,$director,$permissin_contract,$note;

    public function endTimeContract($value){
        $count=Film::whereBetween('end_time',[Carbon::now(),$value])->count();
        return $count;
    }
    public function render()
    {
        $searchTerm=$this->searchTerm;
        $page = \Request::get('page') ? \Request::get('page') : 1;
        $perPage = Config::get('app_per_page') ? Config::get('app_per_page') : 100;
        $startLimit = $perPage * ($page - 1);
        $endLimit = $perPage * $page;
        $query=$this->queryFilm();
        $count_time=$this->count_time;
        Log::debug('FilmTable render'.  $count_time);
        if($count_time==7)
            $query->whereBetween('films.end_time',[Carbon::now(),Carbon::now()->addDay($count_time)]);
        if($count_time<7&&$count_time>0)
            $query->whereBetween('films.end_time',[Carbon::now(),Carbon::now()->addMonth($count_time)]);
        //search
        $query->where('films.vn_name','like','%'.$searchTerm.'%');
        $query->where('films.actor_name','like','%'.$this->searchActor.'%');
        $data = $query->orderBy('id','desc')->offset($startLimit)->limit($perPage)->paginate($perPage);
        Log::debug('FilmTable render data'.  count($data));
        $threeMonth=$this->endTimeContract(Carbon::now()->addMonth(3));
        $twoMonth=$this->endTimeContract(Carbon::now()->addMonth(2));
        $oneMonth=$this->endTimeContract(Carbon::now()->addMonth(1));
        $oneWeek=$this->endTimeContract(Carbon::now()->addWeek(1));
        return view('livewire.film-table'
            ,['data'=>$data ,'threeMonth'=>$threeMonth,
            'twoMonth'=>$twoMonth,'oneMonth'=>$oneMonth,'oneWeek'=>$oneWeek]
        );
    }
    public function queryFilm(){
        $query = DB::table('films');
        $query = $query->select('films.*');
        $query->leftjoin('contracts', function($join)
        {
            $join->on('films.contract_id', '=', 'contracts.id');
        });
        return $query;
    }
    public function query1($count_time){
        $this->count_time = $count_time;
   }
}
