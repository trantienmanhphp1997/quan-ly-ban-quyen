<?php

namespace App\Http\Livewire;

use App\Models\Music;
use Kdion4891\LaravelLivewireTables\Column;
use Kdion4891\LaravelLivewireTables\TableComponent;
use Illuminate\Support\Facades\Log;

class MusicTable extends TableComponent
{
    public $checkbox_side = 'left';
    public $checkbox_attribute = 'id';
    public $table_class = 'table-hover table-striped';
    public $thead_class = 'thead-dark';
    public $header_view = 'admin.media.music.livewire.table-header';
    public $footer_view = 'admin.media.music.livewire.table-footer';

    public $per_page = 5;

    public $start_time;
    public $end_time;

    public function query()
    {
        return Music::query();
    }

    public function columns()
    {
        return [
            Column::make('ID')->searchable()->sortable(),
            Column::make('name')->searchable()->sortable(),
            Column::make('singer')->searchable()->sortable(),
            Column::make('Created At')->searchable()->sortable(),
            Column::make()->view('admin.media.music.table-actions'),

        ];
    }
    public function addMedia(){
        //trong trường hợp viettel mua thì insert vào bảng music: contact_id, start_time; end_time;
        //Viettel bán nếu có số tiền thì lưu trong bảng contract_media_sale_money
        // quyền hạn lưu trong bảng contract_media_sale_role

        Log::debug('An informational message.' . json_encode($this->start_time));
        $this->dispatchBrowserEvent('add-media', ['start_time' => $this->start_time]);

    }

}
