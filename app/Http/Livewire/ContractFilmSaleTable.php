<?php
namespace App\Http\Livewire;

use App\Component\Recursive;
use App\Http\Livewire\Base\BaseLive;
use App\Models\Category;
use App\Models\ContractMediaSaleMoney;
use App\Models\ContractMediaSaleRole;
use App\Models\Country;
use App\Models\FilmCountry;
use App\Models\Contract;
use App\Models\Film;
use App\Models\Revenue;
use App\Models\RevenueType;
use App\Models\RoleMedia;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
class ContractFilmSaleTable extends BaseLive
{
    public $contract_id;
    public $deleteId = '';
    public $extensionId = '';
    public $editId = '';
    public $start_time;
    public $end_time;
    public $money;
    public $category_id = '';
    public $country;
    public $filmID;
    public $names;
    public $role1 = [];
    public $role2 = [];
    public $role3 = [];
    public $roleEditSave1;
    public $roleEditSave2;
    public $roleEditSave3;
    public $countryFilmSave;
    public $updateMode = false;
    public $count_live;
    protected $listeners = [
        'nameSelected',
        'roleSelected1',
        'roleSelected2',
        'countrySelected',
        'roleSelected3',
        'storeFilmSale',
        'storeRevenue',
        'editFilmSale',
    ];
    public $level = [];

    public $userID;
    public function mount(){
        $this->userID = Auth()->id();
    }
    public function nameSelected($names, $start_time, $end_time)
    {
        $this->names = $names;
        $this->start_time = $start_time;
        $this->end_time = $end_time;
    }
    public function roleSelected1($role1, $start_time, $end_time)
    {
        $this->role1 = $role1;
        $this->start_time = $start_time;
        $this->end_time = $end_time;
    }
    public function roleSelected2($role2, $start_time, $end_time)
    {
        $this->role2 = $role2;
        $this->start_time = $start_time;
        $this->end_time = $end_time;
    }
//    public function countrySelected($country, $start_time, $end_time){
    //        $this->country=$country;
    //        $this->start_time = $start_time;
    //        $this->end_time = $end_time;
    //    }

    public function query()
    {
        $contract_id = $this->contract_id;
        return Film::query()->where('contract_id', $contract_id);
    }
    public function deleteId($id)
    {
        // dd($id);
        Log::debug('ContractMediaSaleMoney deleteId');
        $this->deleteId = $id;
    }
    public function extensionId($id)
    {
        // dd($id);
        Log::debug('ContractMediaSaleMoney extensionId ' . $id);
        if($this->extensionId==$id){
            $contract = ContractMediaSaleMoney::find($id);
            $this->emit('setEndTime', reFormatDate($contract->end_time));
        };
        $this->extensionId = $id;
    }
    public function editId($id)
    {
        $contract = ContractMediaSaleMoney::find($id);
        $this->roleEditSave1 = null;
        $this->roleEditSave2 = null;
        $this->roleEditSave3 = null;
        $this->countryFilmSave = null;
        $this->emit('setTimeFilmSale',reFormatDate($contract->start_time), reFormatDate($contract->end_time));
        $this->editId = $id;
    }
    protected $rules = [
        // 'file_name' => 'required',
        // 'file_upload' => '',
    ];
    public function submit()
    {
        // dd('vào');
        // $this->validate();

        // // Execution doesn't reach here if validation fails.

        // ContractFile::create([
        //     'name' => $this->file_name,
        //     'link' => $this->file_upload,
        //     'contract_id' => $this->contract_id,
        // ] );
        // $this->dispatchBrowserEvent('close-modal');

    }
    public function getMediaRole($type)
    {
        $data = RoleMedia::all()->where('type', $type)->where('level', 0)->toArray();
        $recursive = new Recursive($data);
        $htmlOption = $recursive->recursiveMedia();
        return $htmlOption;
    }
    public function getRoleLevel()
    {
        $data = RoleMedia::all()->where('level', 1)->toArray();
        $recursive = new Recursive($data);
        $htmlOption = $recursive->recursiveMedia();
        return $htmlOption;
    }
    public function render()
    {
        if($this->reset)
        {
            $this->searchTerm1=null;
            $this->reset=false;
        }
        $perPage = Config::get('app_per_page') ? Config::get('app_per_page') : 10;
        $data_type1 = $this->getMediaRole(1);
        $data_type2 = $this->getMediaRole(2);

        $role_level = RoleMedia::where('level', 1)->pluck('code', 'id');
        $role_level->prepend('---Hạ tầng phát sóng---', '');
        // dd($role_level);
        $categories = Category::pluck('name', 'id');
        $countries = Country::pluck('name', 'id');
        $countries->prepend('---nước phát sóng---', '');

        $type1 = DB::table('contract_media_sale_role')->Join('role_media', 'role_media.id', '=', 'contract_media_sale_role.role_id')
            ->where('role_media.type', 1)
            ->where('contract_id', $this->contract_id)
            ->where('level', 0)
            ->get(); //doc quyen
        $type2 = DB::table('contract_media_sale_role')->Join('role_media', 'role_media.id', '=', 'contract_media_sale_role.role_id')
            ->where('role_media.type', 2)
            ->where('contract_id', $this->contract_id)
            ->where('level', 0)
            ->get();
        $type3 = DB::table('contract_media_sale_role')->Join('role_media', 'role_media.id', '=', 'contract_media_sale_role.role_id')
            ->where('role_media.level', 1)
            ->where('contract_id', $this->contract_id)
            ->get();
        $filmCountry = DB::table('countries')->select('name', 'id');
        $revenueSale = DB::table('contract_media_sale_money')
            ->Join('revenue', 'revenue.sale_money_id', '=', 'contract_media_sale_money.id')
            ->Join('revenue_type', 'revenue_type.id','=','revenue.type_id')
            ->select('revenue.*','contract_media_sale_money.id as sale_money_id', 'revenue_type.name')
            ->get();
        $data_type3 = $this->getMediaRole(2);



        $query = DB::table('contract_media_sale_money')->where('contract_media_sale_money.contract_id', $this->contract_id)->where('contract_media_sale_money.category_id', 2);
        $query->leftjoin('films', function ($join) {
            $join->on('films.id', '=', 'contract_media_sale_money.media_id');
        });
        $query->leftjoin('contracts', function ($join) {
            $join->on('contracts.id', '=', 'contract_media_sale_money.contract_id');
        });
        $query->leftjoin('revenue', function ($join) {
            $join->on('revenue.sale_money_id', '=', 'contract_media_sale_money.id');
        });

        
        $query->select('contract_media_sale_money.id as sale_money_id', 'contract_media_sale_money.money', 'contract_media_sale_money.nuoc', 'revenue.moneyVAT',
            'films.vn_name', 'films.actor_name', 'films.id', 'contract_media_sale_money.start_time', 'contract_media_sale_money.end_time', 'contract_number',
            'contracts.category_id', 'contract_media_sale_money.media_id');
        Log::debug('ContractMediaTable render');
        $revenue = $this->getRevenue('');
        $revenueRoleEdit;
        $revenue_edit =[];
        $roleEdit1 = [];
        $roleEdit2 = [];
        $roleEdit3 = [];
        $filmSaleCountry = [];
        $RevenueRevenuType = [];
        $revenue2='';
        if ($this->editId) {
            $revenue_edit =ContractMediaSaleMoney::Join('films','films.id', 'contract_media_sale_money.media_id')
                ->join('revenue','revenue.sale_money_id', 'contract_media_sale_money.id')
                ->where('contract_media_sale_money.id',$this->editId)
                ->select('contract_media_sale_money.*','films.vn_name','revenue.moneyVAT')
                ->get()->first();
            $roleEdit1 = DB::table('contract_media_sale_role')
                ->Join('role_media', 'role_media.id', '=', 'contract_media_sale_role.role_id')
                ->Join('contract_media_sale_money', 'contract_media_sale_money.id', '=', 'contract_media_sale_role.sale_money_id')
                ->where('contract_media_sale_money.id', $this->editId)
                ->where('role_media.type', 1)
                ->where('level',0)
                ->select('contract_media_sale_role.role_id')
                ->get();
            $roleEdit2 = DB::table('contract_media_sale_role')
                ->Join('role_media', 'role_media.id', '=', 'contract_media_sale_role.role_id')
                ->Join('contract_media_sale_money', 'contract_media_sale_money.id', '=', 'contract_media_sale_role.sale_money_id')
                ->where('contract_media_sale_money.id', $this->editId)
                ->where('role_media.type', 2)
                ->where('level',0)
                ->select('contract_media_sale_role.role_id')
                ->get();
            $roleEdit3 = DB::table('contract_media_sale_role')
                ->Join('role_media', 'role_media.id', '=', 'contract_media_sale_role.role_id')
                ->Join('contract_media_sale_money', 'contract_media_sale_money.id', '=', 'contract_media_sale_role.sale_money_id')
                ->where('contract_media_sale_money.id', $this->editId)
                ->where('level',1)
                ->select('contract_media_sale_role.role_id')
                ->get();
            $RevenueRevenuType = DB::table('revenue')->Join('contract_media_sale_money','contract_media_sale_money.id', 'revenue.sale_money_id')
                ->where('contract_media_sale_money.id',$this->editId)
                ->select('type_id', 'revenue.money', 'revenue.moneyVAT')
                ->get();
            $filmSaleCountry = DB::table('film_country')->Join('films','films.id','film_country.media_id')
                ->Join('contract_media_sale_money','contract_media_sale_money.id','film_country.sale_money_id')
                ->where('contract_media_sale_money.id', $this->editId)
                ->select('film_country.country_id')
                ->get();
            // dd($filmSaleCountry, $this->editId);
            $revenue2 = Revenue::join('revenue_type','revenue_type.id', 'revenue.type_id')->where('sale_money_id', $this->editId)->select('revenue_type.id')->get()->first();
        }
        $RevenuType = DB::table('revenue_type')->where('parent_id','!=',0)->pluck('name', 'id');
        $RevenuType = json_decode(json_encode($RevenuType), true);
        // dd($RevenueRevenuType);
        if ($this->searchTerm && strlen($this->searchTerm) >= 2) {
            $arrFilm = Film::where('films.vn_name', 'like', '%'.trim($this->searchTerm).'%')->limit(100)->pluck('vn_name', 'id');
        }
        else {
            $arrFilm = DB::table('medias_sell_buy')->where('user_id',$this->userID)->where('category_id',2)->where('type',2)->limit(100)->pluck('name as vn_name', 'media_id as id');
        }
        if (trim($this->searchTerm1)) {
            $query->where(function($query){
                $query->where('films.vn_name', 'like', '%'.trim($this->searchTerm1).'%');
                $query->orWhere('films.actor_name','like','%'.trim($this->searchTerm1).'%');
            });
        }
        $data = $query->orderBy('sale_money_id', 'desc')->paginate($perPage);
        $data_revenue = [];
        $revenue1 = '';
        if ($this->extensionId) {
            $data_revenue = $query->where('contract_media_sale_money.id', '=', $this->extensionId)->get();
            $revenue1 = Revenue::join('revenue_type','revenue_type.id', 'revenue.type_id')->where('sale_money_id', $this->extensionId)->get()->first();
        }
        $this->dispatchBrowserEvent('setSelect2Film');
        $this->dispatchBrowserEvent('setDatepicker');
        $input = [
            'data' => $data,
            'type1' => $type1,
            'type2' => $type2,
            'type3' => $type3,
            'roleEdit1' => $roleEdit1,
            'roleEdit2' => $roleEdit2,
            'roleEdit3' => $roleEdit3,
            'RevenueRevenuType' => $RevenueRevenuType,
            'categories' => $categories,
            'arrFilm' => $arrFilm,
            'data_type1' => $data_type1,
            'data_type2' => $data_type2,
            'role_level' => $role_level,
            'revenue' => $revenue,
            'revenue1' => $revenue1,
            'revenue2' => $revenue2,
            'revenueSale' => $revenueSale,
            'data_revenue' => $data_revenue,
            'revenue_edit' => $revenue_edit,
            'RevenuType' => $RevenuType,
            'filmCountry' => $filmCountry->get(),
            'filmSaleCountry' => $filmSaleCountry,
        ];
        return view('livewire.contract.filmSale', $input);
    }
    public function getRevenue($parentId)
    {
        $data = RevenueType::where('parent_id','!=',0)->get();
        $recursive = new Recursive($data);
        $html = $recursive->recursive($parentId,'');
        return $html;
    }
    public function delete()
    {

        // $contract_id=$revenue->contract_id;
        // $media_id=$revenue->media_id;
        Log::debug('ContractMediaTable delete');
        
        ContractMediaSaleMoney::where('id', $this->deleteId)->delete();
        Revenue::where('sale_money_id',$this->deleteId)->delete();

        ContractMediaSaleRole::where('sale_money_id', $this->deleteId)->where('contract_id', $this->contract_id)->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Xóa phim thành công"]);
    }
    public function editFilmSale($data)
    {
        ///
        $this->roleEditSave1 = $data['role1']??[];
        $this->roleEditSave2 = $data['role2']??[];
        $this->roleEditSave3 = $data['role3']??[];
        $this->countryFilmSave = $data['countryFilm']??[];
        $filmID = ContractMediaSaleMoney::find($this->editId);
        $film = Film::find($filmID->media_id);
        $startTime = (!$film->start_time||$film->start_time=='0000-00-00')?'':$film->start_time;
        $endTime = (!$film->end_time||$film->end_time=='0000-00-00')?'':$film->end_time;
        $data['startTimeEdit'] = (!$data['startTimeEdit'])?'':reFormatDate($data['startTimeEdit'],'Y-m-d');
        $data['endTimeEdit'] = (!$data['endTimeEdit'])?'':reFormatDate($data['endTimeEdit'],'Y-m-d');  
        $data['endTimeEdit1'] = $data['endTimeEdit'];  
        $validator = Validator::make($data, [
            // 'money' => 'nullable|numeric|min:0',
            // 'countLiveEdit' => 'nullable|numeric|min:0',
            'role1' => 'nullable|array',
            'role2' => 'nullable|array',
            'role3' => 'nullable|array',
            'startTimeEdit' => ($data['endTimeEdit']&&$startTime)?'required|date|after_or_equal:'.$startTime:( ($data['endTimeEdit'])?'required|date':( ($startTime)? 'nullable|date|after_or_equal:'.$startTime: 'nullable|date')) ,
            'endTimeEdit1' => ($data['startTimeEdit']&&$endTime)?'required|date|after_or_equal:startTimeEdit|before_or_equal:'.$endTime:( ($data['startTimeEdit'])?'required|after_or_equal:startTimeEdit|date':( ($endTime)? 'nullable|date|before_or_equal:'.$endTime: 'nullable|date'))
        ], [
            'startTimeEdit.required' => "Thời gian bắt đầu bắt buộc",
            'endTimeEdit1.required' => "Thời gian kết thúc bắt buộc",
            'endTimeEdit1.after_or_equal' => 'Thời gian kết thúc phải lớn hơn hoặc bằng thời gian bắt đầu',
            'endTimeEdit1.before_or_equal' => 'Thời gian kết thúc phải nhỏ hơn hoặc bằng thời gian kết thúc bản quyền của phim ('.reFormatDate($endTime, 'd-m-Y').')',
            'startTimeEdit.after_or_equal' => 'Thời gian bắt đầu phải lớn hơn hoặc bằng thời gian bắt đầu bản quyền của phim ('.reFormatDate($startTime, 'd-m-Y').')',
            'countLiveEdit.numeric' => "Số lần phát sóng phải là số",
            'countLiveEdit.min' => "Số lần phát sóng phải lớn hơn hoặc bằng 0",
            // 'money.min' => "Giá tiền phải lớn hơn hoặc bằng 0",
            // 'money.numeric' => "Giá tiền phải là số",
        ])->validate();
        // validate hợp đồng
        $contract = Contract::find($this->contract_id);      
        $startTime = $contract->start_time?reFormatDate($contract->start_time, 'd-m-Y'):'';
        $endTime = $contract->end_time?reFormatDate($contract->end_time, 'd-m-Y'):'';
        $validator = Validator::make($data, [
            'startTimeEdit' => ($data['endTimeEdit']&&$startTime)?'required|date|after_or_equal:'.$startTime:( ($data['endTimeEdit'])?'required|date':( ($startTime)? 'nullable|date|after_or_equal:'.$startTime: 'nullable|date')) ,
            'endTimeEdit1' => ($data['startTimeEdit']&&$endTime)?'required|date|after_or_equal:startTimeEdit|before_or_equal:'.$endTime:( ($data['startTimeEdit'])?'required|after_or_equal:startTimeEdit|date':( ($endTime)? 'nullable|date|before_or_equal:'.$endTime: 'nullable|date'))
        ], [
            'startTimeEdit.required' => "Thời gian bắt đầu bắt buộc",
            'endTimeEdit1.required' => "Thời gian kết thúc bắt buộc",
            'endTimeEdit1.after_or_equal' => 'Thời gian kết thúc phải lớn hơn hoặc bằng thời gian bắt đầu',
            'endTimeEdit1.before_or_equal' => 'Thời gian kết thúc phải nhỏ hơn hoặc bằng thời gian kết thúc bản quyền của hợp đồng ('.reFormatDate($endTime, 'd-m-Y').')',
            'startTimeEdit.after_or_equal' => 'Thời gian bắt đầu phải lớn hơn hoặc bằng thời gian bắt đầu bản quyền của hợp đồng ('.reFormatDate($startTime, 'd-m-Y').')',
        ])->validate();

        // $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => " store" ] );
        try {
            Log::debug('ContractMediaMusicTable store' . $data['country']);

            if (empty($data['role1'])) {
                $data['role1'] = [];
            }
            if (empty($data['role2'])) {
                $data['role2'] = [];
            }
            if (empty($data['role3'])) {
                $data['role3'] = [];
            }
            if (empty($data['countryFilm'])) {
                $data['countryFilm'] = [];
            }
            $startTime = '';
            $endTime ='';
            if($data['startTimeEdit']) $startTime = reFormatDate($data['startTimeEdit'], 'Y-m-d');
            if($data['endTimeEdit']) $endTime = reFormatDate($data['endTimeEdit'], 'Y-m-d');            
            ContractMediaSaleMoney::find($this->editId)
                ->update([
                    'start_time' => $startTime,
                    'end_time' => $endTime,
                    'nuoc' => $data['country'],
                    'note' => $data['note'],
                    'note_film' => $data['noteFilm'],
                    'category_id' => 2,
                    'count_live' => $data['countLiveEdit'],
                    'money' => $data['moneyEdit'],
                ]);
            $sale_money = ContractMediaSaleMoney::find($this->editId);
            ContractMediaSaleRole::where('sale_money_id', $this->editId)->delete();
            FilmCountry::where('sale_money_id', $this->editId)->delete();
            foreach($data['countryFilm'] as $value){
                FilmCountry::create([
                    'country_id' => $value,
                    'media_id' => $filmID->media_id,
                    'type' => 1, // bán phim
                    'contract_id' => $this->contract_id,
                    'sale_money_id' =>  $this->editId,
                ]);                
            }
            $arr_role = array_merge($data['role1'], $data['role2'], $data['role3']);
            foreach($arr_role as $roleId) {
                ContractMediaSaleRole::create([
                    'contract_id' => $this->contract_id,
                    'media_id' => $filmID->media_id,
                    'role_id' => $roleId,
                    'start_time' => $startTime,
                    'end_time' => $endTime,
                    'category_id' => 2,
                    'sale_money_id' => $this->editId,
                ]);
                Log::debug('ContractMediaSaleRole: create roleId' . $roleId);
            }
            Revenue::where('sale_money_id', $this->editId)->update([
                    'date_sign' => $sale_money->start_time,
                    'money' =>(int) $data['moneyEdit'],
                    'moneyVAT' =>(int) $data['moneyVATEdit'],
                    'date_sign' => $sale_money->start_time,
                    'type_id' => $data['revenuTypeEdit'],               
            ]);
            // foreach($data['revenuTypeEdit'] as $key => $value){
            //     Revenue::create([
            //         'contract_id' => $this->contract_id,
            //         'media_id' => $filmID->media_id,
            //         'category_id' => $sale_money->category_id,
            //         'date_sign' => $sale_money->start_time,
            //         'money' =>(int) $data['moneyEdit'][$key],
            //         'moneyVAT' =>(int) $data['moneyVATEdit'][$key],
            //         'type_revenue' => 1,
            //         'type_id' => $value, // để tạm là 0 do chưa có giá trị type nào
            //         'sale_money_id' => $sale_money->id,
            //     ]);                    
            // }
            $this->roleEditSave1 = null;
            $this->roleEditSave2 = null;
            $this->roleEditSave3 = null;
            $this->countryFilmSave = null;
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Chỉnh sửa phim thành công" ] );
            $this->emit('close-media-film2-modal-edit');
        } catch (\Exception $exception) {
            DB::rollBack();
            dd($exception->getMessage());
            Log::error('Message exception: ' . $exception->getMessage() . ', Line: ' . $exception->getLine());
        }
    }
    public function storeFilmSale($data)
    {
        $this->country = $data['film'];
        // dd($this->country);
        $startTime = '';
        $endTime = '';
        if($data['film']){
            $film = Film::find($data['film']);
            $startTime = (!$film->start_time||$film->start_time=='0000-00-00')?'':$film->start_time;
            $endTime = (!$film->end_time||$film->end_time=='0000-00-00')?'':$film->end_time;
        } 
        $data['startTime'] = (!$data['startTime']||$data['startTime']=='0000-00-00')?'':$data['startTime'];
        $data['endTime'] = (!$data['endTime']||$data['endTime']=='0000-00-00')?'':$data['endTime'];  
        $data['endTime1'] = $data['endTime'];  
        $validator = Validator::make($data, [
            'film' => 'required',
            'money' => 'nullable|numeric|min:0',
            'moneyVAT' => 'nullable|numeric|min:0',
            // 'countLive' => 'nullable|numeric|min:0',
            'role1' => 'nullable|array',
            'role2' => 'nullable|array',
            'role3' => 'nullable|array',
            'startTime' => ($data['endTime']&&$startTime)?'required|date|after_or_equal:'.$startTime:( ($data['endTime'])?'required|date':( ($startTime)? 'nullable|date|after_or_equal:'.$startTime: 'nullable|date')) ,
            'endTime1' => ($data['startTime']&&$endTime)?'required|date|after_or_equal:startTime|before_or_equal:'.$endTime:( ($data['startTime'])?'required|after_or_equal:startTime|date':( ($endTime)? 'nullable|date|before_or_equal:'.$endTime: 'nullable|date'))
        ], [
            'film.required' => "Tên phim bắt buộc",
            'startTime.required' => "Thời gian bắt đầu bắt buộc",
            'endTime1.required' => "Thời gian kết thúc bắt buộc",
            'endTime1.after_or_equal' => 'Thời gian kết thúc phải lớn hơn hoặc bằng thời gian bắt đầu',
            'endTime1.before_or_equal' => 'Thời gian kết thúc phải nhỏ hơn hoặc bằng thời gian kết thúc bản quyền của phim ('.reFormatDate($endTime, 'd-m-Y').')',
            'startTime.after_or_equal' => 'Thời gian bắt đầu phải lớn hơn hoặc bằng thời gian bắt đầu bản quyền của phim ('.reFormatDate($startTime, 'd-m-Y').')',
            'money.min' => "Giá tiền phải lớn hơn hoặc bằng 0",
            'money.numeric' => "Giá tiền phải là số",
            'moneyVAT.min' => "Giá tiền phải lớn hơn hoặc bằng 0",
            'moneyVAT.numeric' => "Giá tiền phải là số",
        ])->validate();
        // validate hợp đồng
        $contract = Contract::find($this->contract_id);      
        $startTime = $contract->start_time?reFormatDate($contract->start_time, 'd-m-Y'):'';
        $endTime = $contract->end_time?reFormatDate($contract->end_time, 'd-m-Y'):'';
        $validator = Validator::make($data, [
            'startTime' => ($data['endTime']&&$startTime)?'required|date|after_or_equal:'.$startTime:( ($data['endTime'])?'required|date':( ($startTime)? 'nullable|date|after_or_equal:'.$startTime: 'nullable|date')) ,
            'endTime1' => ($data['startTime']&&$endTime)?'required|date|after_or_equal:startTime|before_or_equal:'.$endTime:( ($data['startTime'])?'required|after_or_equal:startTime|date':( ($endTime)? 'nullable|date|before_or_equal:'.$endTime: 'nullable|date'))
        ], [
            'startTime.required' => "Thời gian bắt đầu bắt buộc",
            'endTime1.required' => "Thời gian kết thúc bắt buộc",
            'endTime1.after_or_equal' => 'Thời gian kết thúc phải lớn hơn hoặc bằng thời gian bắt đầu',
            'endTime1.before_or_equal' => 'Thời gian kết thúc phải nhỏ hơn hoặc bằng thời gian kết thúc bản quyền của hợp đồng ('.reFormatDate($endTime, 'd-m-Y').')',
            'startTime.after_or_equal' => 'Thời gian bắt đầu phải lớn hơn hoặc bằng thời gian bắt đầu bản quyền của hợp đồng ('.reFormatDate($startTime, 'd-m-Y').')',
        ])->validate();


        // $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => " store" ] );
        try {
            Log::debug('ContractMediaMusicTable store' . $data['country']);
            // dd($data);
            if (empty($data['role1'])) {
                $data['role1'] = [];
            }
            if (empty($data['role2'])) {
                $data['role2'] = [];
            }
            if (empty($data['role3'])) {
                $data['role3'] = [];
            }
            if (empty($data['countryFilm'])) {
                $data['countryFilm'] = [];
            }
            $startTime = '';
            $endTime ='';
            if($data['startTime']) $startTime = reFormatDate($data['startTime'], 'Y-m-d');
            if($data['endTime']) $endTime = reFormatDate($data['endTime'], 'Y-m-d');   
            $sale_money = ContractMediaSaleMoney::create([
                'contract_id' => $this->contract_id,
                'media_id' => $data['film'],
                'start_time' => $startTime,
                'end_time' => $endTime,
                'money' => $data['money'],
                'nuoc' => $data['country'],
                'note' => $data['note'],
                'note_film' => $data['noteFilm'],
                'category_id' => 2,
                'count_live' => $data['countLive'],
                'status' => 0,
            ]);

            foreach($data['countryFilm'] as $value){
                FilmCountry::create([
                    'country_id' => $value,
                    'media_id' => $data['film'],
                    'type' => 1, // bán phim
                    'contract_id' => $this->contract_id,
                    'sale_money_id' => $sale_money->id,
                ]);                
            }
            $arr_role = array_merge($data['role1'], $data['role2'], $data['role3']);
            foreach ($arr_role as $roleId) {
                ContractMediaSaleRole::create([
                    'contract_id' => $this->contract_id,
                    'media_id' => $data['film'],
                    'role_id' => $roleId,
                    'start_time' => $startTime,
                    'end_time' => $endTime,
                    'category_id' => 2,
                    'sale_money_id' => $sale_money->id,
                ]);
                Log::debug('ContractMediaSaleRole: create roleId' . $roleId);
                // $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => $roleId ] );
            }

            Revenue::create([
                'contract_id' => $this->contract_id,
                'media_id' => $sale_money->media_id,
                'category_id' => $sale_money->category_id,
                'date_sign' => $sale_money->start_time,
                'money' => $sale_money->money, // giá tiền đã có VAT
                'moneyVAT' => $data['moneyVAT'], // giá tiền chưa có VAT
                'type_revenue' => 1,
                'type_id' => $data['revenuType'], // để tạm là 0 do chưa có giá trị type nào
                'sale_money_id' => $sale_money->id,
            ]);
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Bán phim thành công" ] );
            $this->emit('close-media-film2-modal');
            $this->country = '';
            $this->searchTerm = '';
        } catch (\Exception $exception) {
            DB::rollBack();
            dd($exception->getMessage());
            Log::error('Message exception: ' . $exception->getMessage() . ', Line: ' . $exception->getLine());
        }
    }

    public function storeRevenue($media_id, $date_sign, $money_revenue,$type_revenue, $end_time,$start_time)
    {
        $contract = ContractMediaSaleMoney::find($this->extensionId);
        $film = Film::find($contract->media_id);
        $endTime = (!$film->end_time||$film->end_time=='0000-00-00')?'':$film->end_time;
        $data =[
            'startTime' => $start_time,
            'endTime' => $end_time,
            'dataSign' => $date_sign,
            'moneyRevenue' => $money_revenue,
        ];
        // dd($data, $endTime);
        $validator = Validator::make($data, [
            'dataSign' => 'required|date',
            'moneyRevenue' => 'nullable|numeric|min:0',
            'endTime' => ($data['startTime']&&$endTime)?'required|date|after_or_equal:startTime|before_or_equal:'.$endTime:( ($data['startTime'])?'required|after_or_equal:startTime|date':( ($endTime)? 'nullable|date|before_or_equal:'.$endTime: 'nullable|date'))
        ], [
            'dataSign.required' => "Ngày ký bắt buộc",
            'endTime.required' => "Thời gian kết thúc bắt buộc",
            'endTime.after_or_equal' => 'Thời gian kết thúc phải lớn hơn hoặc bằng thời gian bắt đầu',
            'endTime.before_or_equal' => 'Thời gian kết thúc phải nhỏ hơn hoặc bằng thời gian kết thúc bản quyền của phim ('.reFormatDate($endTime, 'd-m-Y').')',
            'moneyRevenue.min' => "Giá tiền phải lớn hơn hoặc bằng 0",
        ])->validate();
        // dd(reFormatDate($date_sign,'Y-m-d'));
        // validate hợp đồng
        $contract = Contract::find($this->contract_id);      
        $startTime = $contract->start_time?reFormatDate($contract->start_time, 'd-m-Y'):'';
        $endTime = $contract->end_time?reFormatDate($contract->end_time, 'd-m-Y'):'';
        $validator = Validator::make($data, [
            'endTime' => ($data['startTime']&&$endTime)?'required|date|after_or_equal:startTime|before_or_equal:'.$endTime:( ($data['startTime'])?'required|after_or_equal:startTime|date':( ($endTime)? 'nullable|date|before_or_equal:'.$endTime: 'nullable|date'))
        ], [
            'startTime.required' => "Thời gian bắt đầu bắt buộc",
            'endTime.required' => "Thời gian kết thúc bắt buộc",
            'endTime.after_or_equal' => 'Thời gian kết thúc phải lớn hơn hoặc bằng thời gian bắt đầu',
            'endTime.before_or_equal' => 'Thời gian kết thúc phải nhỏ hơn hoặc bằng thời gian kết thúc bản quyền của hợp đồng ('.reFormatDate($endTime, 'd-m-Y').')',
            'startTime.after_or_equal' => 'Thời gian bắt đầu phải lớn hơn hoặc bằng thời gian bắt đầu bản quyền của hợp đồng ('.reFormatDate($startTime, 'd-m-Y').')',
        ])->validate();

        Log::debug('ContractMediaRevenue store');
        $revenue = Revenue::where('contract_id', $this->contract_id)
            ->where('media_id', $media_id)
            ->where('sale_money_id', $this->extensionId)
            ->get()
            ->first();
        // nếu đã có revenu thì cập nhật
        if($revenue){
            $revenue->update([
                'moneyVAT' => (int)$money_revenue + $revenue->moneyVAT,
                'date_sign'=> $date_sign? reFormatDate($date_sign,'Y-m-d'):'',
            ]);
        }
        $contract = ContractMediaSaleMoney::find($this->extensionId);
        ContractMediaSaleMoney::find($this->extensionId)->update([
            // 'money' => (int)$money_revenue + $contract->money,
            'end_time' => $end_time? reFormatDate($end_time, 'Y-m-d'): '',
        ]);
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Gia hạn phim thành công"]);
        $this->emit('close-media-film3-modal');

    }
    public function resetInput()
    {
        $this->start_time = null;
        $this->end_time = null;
        $this->musicID = null;
        $this->names = null;
        $this->role1 = [];
        $this->role2 = [];
        $this->money = null;
        $this->country = null;
    }
    public function openModal()
    {
        $this->emit('show');
    }
    public function checkbox($var)
    {
        $this->$var = !$this->$var;
        // $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => $this->$var ] );
    }
    public function levelClicked()
    {
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => $this->level]);
    }
}
