<?php
namespace App\Http\Livewire;

use App\Models\ContractFile;
use App\Models\Music;
use App\Models\Film;
use App\Models\ContractMediaRole;
use App\Models\Contract;
use Livewire\Component;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Livewire\WithFileUploads;
use Way\Generators\Filesystem\FileNotFound;
use App\Http\Livewire\Base\BaseLive;
use Illuminate\Support\Facades\Validator;

class ContractFilmBuyTable extends BaseLive
{

    public $contract_id;
    public $deleteId = '';
    public $data2;
    public $start_time;
    public $end_time;
    public $musicID;
    public $names = [];
    public $updateMode = false;
    public $films;
    protected $listeners = ['nameSelected' , 'storeFilmBuy'];
    public $level = [];
   
    public $userID;


    public function mount(){
        $this->userID = Auth()->id();
    }
    public function nameSelected($names){
      $this->names = $names;
    }

    public function query()
    {
        $contract_id = $this->contract_id;
        return Music::query()->where('contract_id', $contract_id);
    }
    public function deleteId($id)
    {
        // dd($id);
        Log::debug('ContractMediaTable deleteId');
        $this->deleteId = $id;
    }
    
    public function render()
    {
        $page = \Request::get('page') ? \Request::get('page') : 1;
        $perPage = Config::get('app_per_page') ? Config::get('app_per_page') : 100;
        $perPage = 10;
        $startLimit = $perPage * ($page - 1);
        $endLimit = $perPage * $page;

        $data2 =  Film::Join('contracts','films.contract_id','=', 'contracts.id')->where('contract_id',$this->contract_id)->select('films.*','contract_number')->orderBy('films.id','desc')->offset($startLimit)->limit($perPage)->paginate($perPage);
        Log::debug('ContractMediaTable render');
        $arrFilmSession = DB::table('medias_sell_buy')->where('user_id',$this->userID)->where('category_id',2)->where('type',1)->limit(100)->pluck('name as vn_name', 'media_id as id');
        $arrFilmSession = json_decode(json_encode($arrFilmSession), true);
        $arrFilmNull= Film::where('contract_id',null)->orWhere('contract_id',0)->whereNotIn('id',array_keys($arrFilmSession))->limit(100)->pluck('vn_name', 'id');
        $arrFilmNull = json_decode(json_encode($arrFilmNull), true);
        $arrFilm = $arrFilmSession + $arrFilmNull;
        if($this->searchTerm && strlen($this->searchTerm) >= 2){
            $arrFilm = Film::where('films.vn_name', 'like', '%'.trim($this->searchTerm).'%')->limit(100)->pluck('vn_name', 'id');
            // dd(count($arrFilm));
        }
        $this->dispatchBrowserEvent('setSelect2Film');
        return view('livewire.contract.filmBuy', ['data' => $data2, 'arrFilm' => $arrFilm]);

    }

    public function delete(){
        Log::debug('ContractMediaTable delete');
        Film::where('id',$this->deleteId)->update(['contract_id'=>null]);
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Xoá bộ phim thành công" ] );
    }
    public function storeFilmBuy($data)
    {
        $this->films = $data['listFilm'];
        $check = true;
        $filmError = '';
        $contract = Contract::find($this->contract_id);
        if(!empty($data['listFilm']))
            foreach($data['listFilm'] as $filmId){
                $film = Film::find($filmId);
                if(!$film->start_time || !$film->end_time || strtotime($film->start_time) < strtotime($contract->start_time) || strtotime($film->end_time) > strtotime($contract->end_time))
                {
                    $filmError = $film->vn_name;
                    $check = false;
                    break;
                }
            }
        $checkContract = true;
        $filmContract = [];
        if($data['listFilm']) $filmContract = Film::join('contracts','contracts.id','films.contract_id')->whereIn('films.id',$data['listFilm'])->select('films.vn_name','contracts.contract_number')->get();
        if(count($filmContract)) $checkContract = false;
        $validator = Validator::make($data, [
            'listFilm' => 'required|array'.($check?'':('|film_can_buy:'.$filmError)).($checkContract?'':'|check_contract:'.$filmContract[0]->vn_name.','.$filmContract[0]->contract_number),
        ],[],[
            'listFilm' => 'Danh sách phim',
        ])->validate();

        try{
            Log::debug('ContractMediaFilmTable buy');
            Film::whereIn('id', $data['listFilm'])
                ->update([
                    'contract_id' => $this->contract_id, 
                ]);
            $this->resetInput();
            $this->emit('close-media-film-modal');
            $this->emit('clear_check_box_sale');
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Mua  phim thành công"] );
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error('Message: ' . $exception->getMessage() . ', Line: ' . $exception->getLine());
        }

    }
    public function edit()
    {
        // $validatedDate = $this->validate([
        //     'name' => '',
        //     'contact_id' => '',
        // ]);
        // Log::info('ContractFileTable store');

        // ContractFile::create($validatedDate);

        // session()->flash('message', 'Users Created Successfully.');
        // $this->resetInput();

    }
    public function addFilm(){

    }
    public function resetInput(){
        $this->start_time = null;
        $this->end_time = null;
        $this->musicID = null;
        $this->names = [];
        $this->films = null;
    }
    public function openModal(){
        $this->emit('show');
    }
    public function checkbox($var){
        // $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => $var ] );
    }
    public function levelClicked(){
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => $this->level ] );
    }

}
