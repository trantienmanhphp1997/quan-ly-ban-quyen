<?php

namespace App\Http\Livewire\Contract;

use Livewire\Component;

class MusicBuy extends Component
{
    public function render()
    {
        return view('livewire.contract.music-buy');
    }
}
