<?php

namespace App\Http\Livewire\Contract;
use App\Component\Recursive;
use App\Models\Contract;
use App\Models\ContractMediaSaleMoney;
use App\Models\Music;
use App\Models\Film;
use App\Models\ActionLog;
use App\Models\Category;
use App\Models\RevenueType;
use App\Exports\ContractExport;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Livewire\Base\BaseLive;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\SendMessageApiController;
use Illuminate\Http\Request;
use Excel;

class ViettelBuy extends BaseLive
{


	public $searchTerm;
    public $searchCategory;
    public $searchStatus;
    public $searchPhone;
    public $level = [];
    public $count_time;
    //public $searchTime;
    public $setDate;
    public $setDateSign;
    public $contractList;
    public $currentContract;
    //
    public $threeMonth;
    public $twoMonth;
    public $oneMonth;
    public $oneWeek;
    public $checkContracts = [];
    public function __construct()
    {
        $this->currentContract = new Contract();
    }

    protected $listeners = [
        'searchTime',
        'searchDateSign',
        'update-current-contract' => 'updateCurrentContract' 
    ];

    public function mount(){
        if(auth()->user()->category_id) {
            $this->searchCategory = auth()->user()->category_id;
        }
    }

    public function setCurrentContract($contractId) {
        $this->currentContract = Contract::find($contractId);
    }

    public function updateCurrentContract($data) {
        $data['startTime'] = $this->currentContract->start_time;
        $validator = Validator::make($data, [
            'endTime' => 'required|date|after_or_equal:startTime',
            'additionMoney' => 'required|numeric|min:0'
        ], [
            'endTime.after_or_equal' =>'Thời gian kết thúc phải lớn hơn hoặc bằng thời gian bắt đầu',
            'endTime.required' => 'Nhập vào thời gian kết thúc',
            'additionMoney.required' => 'Nhập vào số tiền',
            'additionMoney.min' => 'Số tiền phải lớn hơn hoặc bằng 0',
        ])->validate();

        if(reFormatDate($this->currentContract->end_time)!=reFormatDate($data['endTime'])){
            $old_value['end_time']=reFormatDate($this->currentContract->end_time);
            $changes['end_time']= reFormatDate($data['endTime']);
        }

        if($this->currentContract->category_id == 1) {
            if($data['additionMoney']) {
                $old_value['total_money']=$this->currentContract->total_money;
                $this->currentContract->total_money += $data['additionMoney'];
                $changes['total_money']=$this->currentContract->total_money;
            }
            if($this->currentContract->type==1){
                Music::where('contract_id',$this->currentContract->id)->update([
                    'end_time' =>reFormatDate($data['endTime'], 'Y-m-d'),
                ]);
                $listMusic = Music::where('contract_id',$this->currentContract->id)->pluck('id')->toArray();
                ContractMediaSaleMoney::where('category_id',1)
                    ->whereIn('media_id',$listMusic)
                    ->update([
                        'end_time' =>reFormatDate($data['endTime'], 'Y-m-d'),
                    ]);
            }
        }
        if(isset($old_value)&&isset($changes)&&$old_value!=$changes){
            $log = ActionLog::writeActionLog($this->currentContract->id, $this->currentContract->getTable(), config("common.actions.extension", "extension"), get_class($this), "Gia hạn", $old_value, $changes, null);
        }
        $this->currentContract->end_time = reFormatDate($data['endTime'], 'Y-m-d');
        $this->currentContract->save();

        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => " Gia hạn hợp đồng thành công" ] );
    }

    public function render()
    {
        if($this->reset){
            $this->searchCategory = null;
            $this->searchTerm = null;
            $this->searchStatus = null;
            $this->searchPhone = null;
            $this->count_time = null;
            $this->setDate = null;
            $this->setDateSign = null;
            $this->checkContracts = [];
            $this->reset = false;
        }
        $query = $this->getQuery();
        $query->select('contracts.*', 'partners.name as partner', 'categories.name as category');
        $this->contractList = $query->get();
        $perPage = Config::get('app.per_page') ? Config::get('app.per_page') : 10;
        $data = $query->orderBy('contracts.date_sign','desc')->paginate($perPage);
        $categories=Category::pluck('name','id');
        $categories->prepend('---Tất cả---', '');
        $categories =  collect([
            ''=> '---Tất cả---',
            '2'=> 'Phim',
            '1'=> 'Nhạc',
            // '3'=> 'Quyền tác giả',
        ]);
        // dd($categories);
        $this->threeMonth=$this->endTimeContract(3);
        $this->twoMonth=$this->endTimeContract(2);
        $this->oneMonth=$this->endTimeContract(1);
        $this->oneWeek=$this->endTimeContract(7);
        $this->dispatchBrowserEvent('setDatepicker');
        $model = config("common.permission.module.contract-viettel-buy", "contract-viettel-buy");
        return view('livewire.contract.viettel-buy', compact('data','categories', 'model'));
    }
    public function getQuery(){
        $setDate = $this->setDate;
        $setDateSign = $this->setDateSign;
        // dd($this->setDateSign,$this->setDate);
        $query = Contract::query();
        $query->where(function($query){
                $query->where('contracts.end_time', '>=', date('Y-m-d'));
                $query->orWhereNull('contracts.end_time');
        });
        if($this->searchStatus==1)
            $query = Contract::where('contracts.end_time', '<', date('Y-m-d'));
        elseif($this->searchStatus==2)
            $query = Contract::onlyTrashed();
        elseif($this->searchStatus==3)
            $query = Contract::withTrashed();
        if($this->searchTerm){
            $query->where('contracts.contract_number','like','%'.trim($this->searchTerm).'%');
        }
        if($this->searchPhone){
            $query->where('contracts.user_msisdn','like','%'.trim($this->searchPhone).'%');
        }
        if(!empty($this->setDate)){
            $getDate = $setDate ? explode(" - ", $setDate) : null;
            if ($getDate != '') {
                $startDateCharge = $getDate[0];
                $endDateCharge = $getDate[1];
                if ($startDateCharge != '' && $endDateCharge != '') {
                    $start_time = date("Y-m-d", strtotime($startDateCharge));
                    $end_time = date("Y-m-d", strtotime($endDateCharge));
                    $query->whereBetween('contracts.end_time', [$start_time, $end_time]);
                }
            }
        }
        if(!empty($this->setDateSign)){
            $getDate = $setDateSign ? explode(" - ", $setDateSign) : null;
            if ($getDate != '') {
                $startDateCharge = $getDate[0];
                $endDateCharge = $getDate[1];
                if ($startDateCharge != '' && $endDateCharge != '') {
                    $start_time = date("Y-m-d", strtotime($startDateCharge));
                    $end_time = date("Y-m-d", strtotime($endDateCharge));
                    $query->whereBetween('contracts.date_sign', [$start_time, $end_time]);
                }
            }
        }
        $query = $query->where(function($query){
            $query->where('type', 1);
            $query->orWhere('type', 4);
        });
        $count_time=$this->count_time;

        if($count_time==7)
            $query->whereBetween('contracts.end_time',[Carbon::now()->format('Y-m-d'),Carbon::now()->addDay($count_time)->format('Y-m-d')]);
        elseif($count_time)
            $query->whereBetween('contracts.end_time',[Carbon::now()->addMonth($count_time-1)->addDays(1)->format('Y-m-d'),Carbon::now()->addMonth($count_time)->format('Y-m-d')]);
        $start_date = \Request::get('start_date') ? explode(" - ", \Request::get('start_date')) : null;
        if($start_date != ''){
            $startDateCharge = $start_date[0];

            $endDateCharge = $start_date[1];

            if($startDateCharge != '' && $endDateCharge != ''){
                $start = date("Y-m-d 00:00:00",strtotime($startDateCharge));
                $end = date("Y-m-d 23:59:59",strtotime($endDateCharge));
                $query->whereBetween('date_sign',[$start,$end]);
            }
        }
        $query->leftjoin('partners', function($join){
             $join->on('partners.id','=','contracts.partner_id');
        });
        $query->leftJoin('categories', function($join){
             $join->on('contracts.category_id','=','categories.id');
        });
        if($this->searchCategory){
            if($this->searchCategory==2) {
                $query->where('contracts.category_id','=',2);
            }
            else if($this->searchCategory==1) {
                $query->where('contracts.category_id','=',1)
                ->where(function($query){
                    $query->where('contracts.type',1);
                    $query->orWhere('contracts.type',4);
                });
            }
        }
        return $query;
    }
    public function query1($count_time){
        $this->count_time = $count_time;
        $this->resetPage();
    }

    public function updatedSearchTerm(){
        $this->resetPage();
    }

    public function updatedSearchPhone(){
        $this->resetPage();
    }

    public function updatedSetDate(){
        $this->resetPage();
    }

    public function updatedSetDateSign(){
        $this->resetPage();
    }

    public function updatedSearchCategory(){
        $this->resetPage();
    }
    public function updatedSearchStatus(){
        $this->resetPage();
    }

    public function delete(){
        if(Auth::user()->hasAnyRole('contract-viettel-buy-delete|administrator')) {
            $contract = contract::find($this->deleteId);
            if($contract->type==1){
                $music = Music::where('contract_id', $this->deleteId)->get();
                if(count($music)){
                    $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => "Không được phép xóa hợp đồng có nhạc mua." ] );
                    return;
                }
                $film = Film::where('contract_id', $this->deleteId)->get();
                if(count($film)){
                    $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => "Không được phép xóa hợp đồng có phim mua." ] );
                    return;
                }
            }
            else {
                $music = Music::where('contract_id', $this->deleteId)->get();
                if(count($music)){
                    $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => "Không được phép xóa hợp đồng có nhạc mua." ] );
                    return;
                }                
            }
            ActionLog::writeActionLog($this->deleteId, $contract->getTable(), config("common.actions.deleted", "deleted"), get_class($this), "Xóa", $contract->contract_number, null, null);
            // ActionLog::where('record_id', '=', $this->deleteId)->where('model', '=', 'contracts')->delete();
            $contract->delete();
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Xóa hợp đồng thành công" ] );
            return;
        }
        abort(403, 'Unauthorized action.');
    }
    public function deleteAll(){

        if(Auth::user()->hasAnyRole('contract-viettel-buy-delete|administrator')) {
            // get total contract id
            $query = $this->getQuery()->whereIn('contracts.id',$this->checkContracts)->pluck('contracts.id')->toArray();
            // get contract id type = 1, category 2
            $contract_id_film_type_1 = Contract::whereIn('id',$query)->where('type',1)->where('category_id',2)->pluck('id')->toArray();
            // get contract id type = 1, category 1
            $contract_id_music_type_1 = Contract::whereIn('id',$query)->where('type',1)->where('category_id',1)->pluck('id')->toArray();
            // get contract id type = 4
            $contract_id_type_4 = Contract::whereIn('id',$query)->where('type',4)->pluck('id')->toArray();
            // kiểm tra xem có đủ điều kiện xóa không
            $music = Music::whereIn('contract_id', $contract_id_music_type_1)->get();
            if(count($music)){
                $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => "Không được phép xóa hợp đồng có nhạc mua." ] );
                return;
            }
            $film = Film::whereIn('contract_id', $contract_id_film_type_1)->get();
            if(count($film)){
                $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => "Không được phép xóa hợp đồng có phim mua." ] );
                return;
            }
            $music = Music::whereIn('contract_id', $contract_id_type_4)->get();
            if(count($music)){
                $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => "Không được phép xóa hợp đồng có nhạc mua." ] );
                return;
            }  
            // thực hiện xóa
            foreach($query as $key=>$deleteId){
                $contract = contract::find($deleteId);
                ActionLog::writeActionLog($deleteId, $contract->getTable(), config("common.actions.deleted", "deleted"), get_class($this), "Xóa", $contract->contract_number, null, null);
                $contract->delete();
            }
            $countContractBefore = count($this->checkContracts);
            $this->checkContracts = array_diff($this->checkContracts, $query);
            $countContractAfter = count($this->checkContracts);
            $countContractDelete = $countContractBefore-$countContractAfter;
            $this->resetPage();
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Xóa ".$countContractDelete." hợp đồng được chọn thành công."  ] );
            return;
        }
        abort(403, 'Unauthorized action.');
    }
    public function getRevenue($parentId){
        $data=RevenueType::all();
        $recursive= new Recursive($data);
        $html=$recursive->recursive($parentId);
        return $html;
    }
    public function endTimeContract($value){
        if($value==7)
            $count = Contract::whereBetween('contracts.end_time',[Carbon::now()->format('Y-m-d'),Carbon::now()->addDay($value)->format('Y-m-d')])->whereIn('type', [1,4])->count();
        else
            $count = Contract::whereBetween('contracts.end_time',[Carbon::now()->addMonth($value-1)->addDays(1)->format('Y-m-d'),Carbon::now()->addMonth($value)->format('Y-m-d')])->whereIn('type', [1,4])->count();
        return $count;
    }
    public function searchTime($times){
        $this->setDate=$times;
    }
    public function searchDateSign($times){
        $this->setDateSign=$times;
    }

    public function notify($id, $status){
        $contract = Contract::find($id);
        $contract->warning_status=$status;
        $contract->save();
    }

    public function restore($id){
        Contract::withTrashed()->where('id', $id)->restore();
        ActionLog::writeActionLog($id, 'contracts', config("common.actions.restore", "restore"), get_class($this), "Khôi phục", null, null, null);
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Khôi phục hợp đồng bán thành công." ] );
    }

    public function export(){
        $today = date("d_m_Y");
        return Excel::download(new ContractExport($this->contractList), 'contracts-'.$today.'.xlsx');
    }
}
