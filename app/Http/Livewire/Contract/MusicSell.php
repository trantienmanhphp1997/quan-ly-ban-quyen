<?php

namespace App\Http\Livewire\Contract;

use Livewire\Component;
use WithPagination;

class MusicSell extends Component
{
    public function render()
    {
        return view('livewire.contract.music-sell');
    }
}
