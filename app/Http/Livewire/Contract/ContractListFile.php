<?php

namespace App\Http\Livewire\Contract;
use App\Component\Recursive;
use App\Models\Contract;
use App\Models\Music;
use App\Models\Film;
use App\Models\ContractFile;
use App\Models\Category;
use App\Models\RevenueType;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Livewire\Base\BaseLive;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage as FacadesStorage;
use Illuminate\Support\Facades\Storage;

class ContractListFile extends BaseLive
{
    public $searchContractNumber;
    public $searchCategory;
    public $searchName;
    public function render(){
        if($this->reset){
            $this->searchContractNumber = null;
            $this->searchCategory = null;
            $this->searchName = null;
            $this->reset = false;
        }
        $query = ContractFile::query()
            ->leftjoin('contracts', 'contract_files.contract_id', '=', 'contracts.id')
            ->leftjoin('users', 'contract_files.admin_id', '=', 'users.id')
            ->leftjoin('films', function ($join) {
                $join->on('films.id', '=', 'contract_files.media_id')->where('contract_files.categori_id', '=', 2);
            })
            ->leftjoin('music', function ($join) {
                $join->on('music.id', '=', 'contract_files.media_id')->where('contract_files.categori_id', '=', 1);
            })
            ->whereNull('contracts.deleted_at')
            ->select('contract_files.*','contract_number','users.full_name','contracts.type', 'films.vn_name as film_name', 'music.name as music_name','contracts.start_time as start_time','contracts.end_time as end_time','films.start_time as film_start_time','films.end_time as film_end_time','music.start_time as music_start_time','music.end_time as music_end_time');
        if($this->searchName){
            $query->where('contract_files.name', 'like', '%'.trim($this->searchName).'%');
        }

        if($this->searchContractNumber)
            $query->where(function ($query) {
                $query->where('contracts.contract_number','like','%'.trim($this->searchContractNumber).'%')
                ->orWhere('films.vn_name','like','%'.trim($this->searchContractNumber).'%')
                ->orWhere('music.name','like','%'.trim($this->searchContractNumber).'%');
            });
        if($this->searchName||$this->searchContractNumber ){
            $this->resetPage();
        }


        if($this->searchCategory==1)
            $query->whereNotNull('contract_files.contract_id');
        elseif($this->searchCategory==2)
            $query->where('contract_files.media_id', '!=', 0)->where('contract_files.categori_id', '=', 1);
        elseif($this->searchCategory==3)
            $query->where('contract_files.media_id', '!=', 0)->where('contract_files.categori_id', '=', 2);
        $data = $query->orderBy('id','desc')->paginate(10);
            // dd($data);
        $model = config("common.permission.module.contract-file", "contract-file");
        return view('livewire.contract.contractFile',compact('data', 'model'));
    }
    public function forDownload($id){
        // dd('đã vào');
        $dl = ContractFile::find($id);
        return Storage::download($dl->link);
    }
}
