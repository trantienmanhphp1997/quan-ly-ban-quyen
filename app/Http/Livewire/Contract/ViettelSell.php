<?php

namespace App\Http\Livewire\Contract;
use App\Component\Recursive;
use App\Models\Contract;
use App\Models\Category;
use App\Models\ContractMediaSaleMoney;
use App\Models\Revenue;
use App\Models\RevenueType;
use Carbon\Carbon;
use App\Models\ActionLog;
use Illuminate\Support\Facades\DB;
use App\Http\Livewire\Base\BaseLive;
use Illuminate\Support\Facades\Auth;
// use Dotenv\Validator;
// use GuzzleHttp\Psr7\Request;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Exports\FilmSaleExport;
use App\Exports\MusicSaleDataExport;
use Excel;
class ViettelSell extends BaseLive
{
	public $searchTerm;
    public $searchCategory;
    public $searchStatus;
    public $searchPhone;
    public $level = [];
    public $count_time;
    public $contract_id='';
    public $setDate;
    public $checkContracts = [];
    protected $listeners=['storeRevenue','searchTime', 'getError'];
    
    public function mount(){
        if(auth()->user()->category_id)
            $this->searchCategory = auth()->user()->category_id;
    }

    public function extensionId($id){
        $this->contract_id=$id;
    }

    public function render()
    {
        if($this->reset){
            $this->searchCategory = null;
            $this->searchStatus=null;
            $this->searchTerm = null;
            $this->searchPhone = null;
            $this->count_time = null;
            $this->setDate =null;
            $this->checkContracts = [];
            $this->reset = false;
        }

        $query = $this->getQuery();
        $perPage = Config::get('app.per_page') ? Config::get('app.per_page') : 10;

        $data = $query->orderBy('contracts.date_sign','desc')->paginate($perPage);

        $threeMonth=$this->endTimeContract(3);
        $twoMonth=$this->endTimeContract(2);
        $oneMonth=$this->endTimeContract(1);
        $oneWeek=$this->endTimeContract(7);

        $category = Category::all();

        $data_revenue=[];
        if($this->contract_id){
            $data_revenue=Contract::find($this->contract_id);
        }
        $this->dispatchBrowserEvent('setDatepicker');
        $categories=Category::pluck('name','id');
        $categories->prepend('---Tất cả---', '');
        $categories =  collect([
            ''=> '---Tất cả---',
            '2'=> 'Phim',
            '1'=> 'Nhạc',
            // '3'=> 'Quyền tác giả',
        ]);
        // $revenue=RevenueType::pluck('name','id');
        $model = config("common.permission.module.contract-viettel-sale", "contract-viettel-sale");
        return view('livewire.contract.viettel-sell', compact('data', 'threeMonth','data_revenue', 'twoMonth', 'oneMonth', 'oneWeek', 'category','categories', 'model'));
    }
    public function getQuery (){
        $searchTerm = $this->searchTerm;
        $setDate = $this->setDate;

        $query = Contract::query();
        $query->where(function($query){
                 $query->where('contracts.end_time', '>=', date('Y-m-d'));
                 $query->orWhereNull('contracts.end_time');
             });
        if($this->searchStatus==1)
            $query = Contract::where('contracts.end_time', '<', date('Y-m-d'));
        elseif($this->searchStatus==2)
            $query = Contract::onlyTrashed();
        elseif($this->searchStatus==3)
            $query = Contract::withTrashed();
        $query->where('type',2);
        if($this->searchTerm)
            $query->where('contracts.contract_number','like','%'.trim($this->searchTerm).'%');
        if($this->searchPhone){
            $query->where('contracts.user_msisdn','like','%'.trim($this->searchPhone).'%');
        }
        $count_time=$this->count_time;
        if($count_time==7)
            $query->whereBetween('contracts.end_time',[Carbon::now()->format('Y-m-d'),Carbon::now()->addDay($count_time)->format('Y-m-d')]);
        elseif($count_time)
            $query->whereBetween('contracts.end_time',[Carbon::now()->addMonth($count_time-1)->addDays(1)->format('Y-m-d'),Carbon::now()->addMonth($count_time)->format('Y-m-d')]);
        $start_date = \Request::get('start_date') ? explode(" - ", \Request::get('start_date')) : null;
        if($start_date != ''){
            $startDateCharge = $start_date[0];

            $endDateCharge = $start_date[1];

            if($startDateCharge != '' && $endDateCharge != ''){
                $start = date("Y-m-d 00:00:00",strtotime($startDateCharge));
                $end = date("Y-m-d 23:59:59",strtotime($endDateCharge));
                $query->whereBetween('date_sign',[$start,$end]);
            }
        }
        $query->leftjoin('partners', function($join){
             $join->on('partners.id','=','contracts.partner_id');
        });
        $query->leftJoin('categories', function($join){
             $join->on('contracts.category_id','=','categories.id');
        });
        if($this->searchCategory){
            $query->where('contracts.category_id','=',$this->searchCategory);
        }
        $query->select('contracts.*', DB::raw('partners.name as partner'), DB::raw('categories.name as category'));
        if(!empty($this->setDate)){
            $getDate = $setDate ? explode(" - ", $setDate) : null;
            if ($getDate != '') {
                $startDateCharge = $getDate[0];
                $endDateCharge = $getDate[1];
                if ($startDateCharge != '' && $endDateCharge != '') {
                     $start_time = date("Y-m-d", strtotime($startDateCharge));
                     $end_time = date("Y-m-d", strtotime($endDateCharge));
                     $query->whereBetween('contracts.end_time', [$start_time, $end_time]);
                }
            }
        }
        return $query;
    }
    public function query1($count_time){
        $this->count_time = $count_time;
        $this->resetPage();
    }
    public function updatedSearchTerm(){
        $this->resetPage();
    }

    public function updatedSearchPhone(){
        $this->resetPage();
    }

    public function updatedSetDate(){
        $this->resetPage();
    }

    public function updatedSearchCategory(){
        $this->resetPage();
    }
    public function updatedSearchStatus(){
        $this->resetPage();
    }
    public function delete(){
        if(Auth::user()->hasAnyRole('contract-viettel-sale-delete|administrator')) {
            $media_sale = DB::table('contract_media_sale_money')->where('contract_id', $this->deleteId)->get();
            if(count($media_sale)){
                $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => "Không được phép xóa hợp đồng có sản phẩm đã bán." ] );
                return;
            }
            $contract = Contract::find($this->deleteId);
            ActionLog::writeActionLog($this->deleteId, $contract->getTable(), config("common.actions.deleted", "deleted"), get_class($this), "Xóa", $contract->contract_number, null, null);
            // ActionLog::where('record_id', '=', $this->deleteId)->where('model', '=', 'contracts')->delete();
            $contract->delete();
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Xóa hợp đồng thành công" ] );
            return;
        }
        abort(403, 'Unauthorized action.');
    }
    public function endTimeContract($value){
        if($value==7)
            $count = Contract::whereBetween('contracts.end_time',[Carbon::now()->format('Y-m-d'),Carbon::now()->addDay($value)->format('Y-m-d')])->where('type', 2)->count();
        else
            $count = Contract::whereBetween('contracts.end_time',[Carbon::now()->addMonth($value-1)->addDays(1)->format('Y-m-d'),Carbon::now()->addMonth($value)->format('Y-m-d')])->where('type', 2)->count();
        return $count;
    }
    public function storeRevenue($type_contract,$media_id,$category_id,$date_sign,$money_revenue,$type_revenue,$type_id,$end_time, $start_time){
        $data=[
            'type_contract' => $type_contract,
            'media_id' => $media_id,
            'category_id' => $category_id,
            'date_sign' => $date_sign,
            'money_revenue' => $money_revenue,
            'type_revenue' => $type_revenue,
            'type_id' => $type_id,
            'end_time' => $end_time,
            'start_time' => $start_time,
        ];
        if($category_id==1)
        {
            $validator=Validator::make($data,[
                'date_sign' => 'required|date_format:d-m-Y',
                'end_time' => 'required|date|after_or_equal:start_time',
                'money_revenue' => 'numeric|min:0'
            ],[
                'date_sign.required' => 'Chưa nhập ngày ký',
                'date_sign.date_format' => 'Nhập sai định dạng: Ngày-Tháng-Năm',
                'end_time.after_or_equal' =>'Thời gian kết thúc phải lớn hơn hoặc bằng thời gian bắt đầu',
                'end_time.required' => 'Nhập vào thời gian kết thúc',
                'money_revenue.min' => 'Số tiền phải lớn hơn hoặc bằng 0',
            ])->validate();
        }
        else
        {
            $validator=Validator::make($data,[
                'end_time' => 'required|date|after_or_equal:start_time',
            ],[
                'end_time.after_or_equal' =>'Thời gian kết thúc phải lớn hơn hoặc bằng thời gian bắt đầu',
                'end_time.required' => 'Nhập vào thời gian kết thúc',
            ])->validate();
        }
       
        if($category_id==2){
            $contract = Contract::find($this->contract_id);
            $contract->fill([
                'end_time'=>reFormatDate($end_time, 'Y-m-d'),
            ]);
            $changes = $contract->getDirty();
            $old_value = array();
            if(!empty($changes)){
                foreach ($changes as $key => $value) {
                    $old_value[$key] = $contract->getOriginal($key);
                }
                $old_value['end_time']= reFormatDate($old_value['end_time']);
                $changes['end_time']= reFormatDate($changes['end_time']);
                if($old_value['end_time']==$changes['end_time']) {
                    unset($old_value['end_time']);
                    unset($changes['end_time']);
                }
                if(!empty($changes)) {
                    $log = ActionLog::writeActionLog($contract->id, $contract->getTable(), config("common.actions.extension", "extension"), get_class($this), "Gia hạn", $old_value, $changes, null);
                }
            }
            $contract->save();
        }
        else if($category_id==1){
            Revenue::create([
                'contract_id'=>$this->contract_id,
                'type_contract'=>$type_contract,
                'media_id'=>$media_id,
                'category_id'=>$category_id,
                'date_sign'=>reFormatDate($date_sign, 'Y-m-d'),
                'money'=>$money_revenue,
                'type_revenue'=>$type_revenue,
                'type_id'=>$type_id,
            ]);
            $contract= Contract::find($this->contract_id);
            $contract->fill([
                'total_money'=>$contract->total_money + (int)$money_revenue,
                'end_time'=>$end_time,
                'date_sign'=>reFormatDate($date_sign, 'Y-m-d'),
            ]);

            $changes = $contract->getDirty();
            $old_value = array();

            foreach ($changes as $key => $value) {
                $old_value[$key] = $contract->getOriginal($key);
            }
            $old_value['end_time']= reFormatDate($old_value['end_time']);
            if($old_value['end_time']==$changes['end_time']) {
                unset($old_value['end_time']);
                unset($changes['end_time']);
            }
            if(isset($old_value['date_sign'])){
                $old_value['date_sign'] = reFormatDate($old_value['date_sign']);
                $changes['date_sign'] = reFormatDate($changes['date_sign']);
            }
            if(!empty($changes)) {
                $log = ActionLog::writeActionLog($contract->id, $contract->getTable(), config("common.actions.extension", "extension"), get_class($this), "Gia hạn", $old_value, $changes, null);
            }
            $contract->save();
        }
        $this->emit('close-modal1');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Gia hạn hợp đồng bán thành công" ] );
    }
    public function searchTime($times){
        $this->setDate=$times;
    }   


    public function notify($id, $status){
        $contract = Contract::find($id);
        $contract->warning_status=$status;
        $contract->save();
    }

    public function example(){
        return Storage::download('public/filmSale.xlsx');
    }

    public function restore($id){
        Contract::withTrashed()->where('id', $id)->restore();
        $contract = Contract::find($id);
        ActionLog::writeActionLog($id, 'contracts', config("common.actions.restore", "restore"), get_class($this), "Khôi phục", null, null, null);
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Khôi phục hợp đồng bán thành công." ] );
    }
    public function getError($data){
        $today = date("d-m-Y");
        return Excel::download(new FilmSaleExport($data), 'Film_sale_error('.$today.').xlsx');
    }
    public function deleteAll(){
        if(Auth::user()->hasAnyRole('contract-viettel-sale-delete|administrator')) {
            $query = $this->getQuery()->whereIn('contracts.id',$this->checkContracts)->pluck('id')->toArray();
            $media_sale = DB::table('contract_media_sale_money')->whereIn('contract_id', $query)->get();
            if(count($media_sale)){
                $this->dispatchBrowserEvent('show-toast', ["type" => "error", "message" => "Không được phép xóa hợp đồng có sản phẩm đã bán." ] );
                return;
            }
            foreach($query as $key => $deleteId){
                $contract = Contract::find($deleteId);
                ActionLog::writeActionLog($deleteId, $contract->getTable(), config("common.actions.deleted", "deleted"), get_class($this), "Xóa", $contract->contract_number, null, null);
                $contract->delete();
            }
            $this->checkContracts = array_diff($this->checkContracts, $query);
            $this->resetPage();
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Xóa ".count($query)." hợp đồng được chọn thành công." ] );
            return;
        }
        abort(403, 'Unauthorized action.');        
    }
    // public function export(){
    //     $today = date("d_m_Y");
    //     return Excel::download(new MusicSaleDataExport(), 'films-'.$today.'.xlsx');
    //     $this->emit('close-modal-exampleModalMusicSale');
    // }

}
