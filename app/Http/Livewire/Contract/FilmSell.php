<?php

namespace App\Http\Livewire\Contract;

use Livewire\Component;

class FilmSell extends Component
{
    public function render()
    {
        return view('livewire.contract.film-sell');
    }
}
