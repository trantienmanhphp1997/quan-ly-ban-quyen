<?php
namespace App\Http\Livewire\Contract;

use App\Models\ContractFile;
use App\Models\Music;
use App\Models\Contract;
use Livewire\Component;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

use Livewire\WithFileUploads;

class FilmBuy extends Component
{
    public $contract_id;
    public $deleteId = '';
    public $data2;
    public $start_time;
    public $end_time;
    public $money;
    public $musicID;
    public $names = [];
    public $updateMode = false;
    protected $listeners = ['nameSelected'];

    public $level = [];
    public function nameSelected($names){
      $this->names = $names;
    }

    public function query()
    {
        $contract_id = $this->contract_id;
        return Music::query()->where('contract_id', $contract_id);
    }
    public function deleteId($id)
    {
        // dd($id);
        Log::debug('ContractMediaTable deleteId');
        $this->deleteId = $id;
    }
    protected $rules = [
        // 'file_name' => 'required',
        // 'file_upload' => '',
    ];
    public function submit()
    {
        // dd('vào');
        // $this->validate();

        // // Execution doesn't reach here if validation fails.

        // ContractFile::create([
        //     'name' => $this->file_name,
        //     'link' => $this->file_upload,
        //     'contract_id' => $this->contract_id,
        // ] );
        // $this->dispatchBrowserEvent('close-modal');

    }
    public function render()
    {
        $page = \Request::get('page') ? \Request::get('page') : 1;
        $perPage = Config::get('app_per_page') ? Config::get('app_per_page') : 100;
        $perPage = 10;
        $startLimit = $perPage * ($page - 1);
        $endLimit = $perPage * $page;

        $data2 =  Music::Join('contracts','music.contract_id','=', 'contracts.id')->where('contract_id',$this->contract_id)->select('music.*','contract_number')->orderBy('music.id','desc')->offset($startLimit)->limit($perPage)->paginate($perPage);
        $contract = Contract::find($this->contract_id);
        Log::debug('ContractMediaTable render');
        $arrayMusicID = explode(' ', session('arrayMusicID'));
        $arrayName = explode('__', session('arrayName'));
        // $arraySinger = explode('__', session('arraySinger
        return view('livewire.contract.film-buy', ['data' => $data2, 'arrayName' => $arrayName]);

    }
    public function delete(){
        Log::debug('ContractMediaTable delete');
        Music::where('id',$this->deleteId)->update(['contract_id'=>null]);
    }
   public function store()
    {
        Log::debug('ContractMediaMusicTable store');
        // $this->validate([
        //     'names' => 'required',
        // ]);
        $start_time = $this->start_time;
        $end_time = $this->end_time;
        $money = $this->money;
        $names = $this->names;
        // dd($names);
        $arrayMusicID = explode(' ', session('arrayMusicID'));
        $arrayName = explode('__', session('arrayName'));
        $arrayKey = [];
        foreach ($names as $key => $name) {
            $key = array_search($name, $arrayName);
            if($key>=0){
                $musicID = $arrayMusicID[$key];
                array_push($arrayKey, $key);
                Music::where('id', $musicID)->update(['contract_id' => $this->contract_id, 'start_time' => $start_time, 'end_time'=> $end_time, 'money' => $money]);
            }
        }
        // dd($arrayKey);
        // xóa những bài hát và ID đã có
        foreach ($arrayKey as $key => $value) {
            unset($arrayName[$value]);
            unset($arrayMusicID[$value]);
        }
        // convert về string
        $arrayMusicID = implode(" ", $arrayMusicID);
        $arrayName = implode("__", $arrayName);
        // lưu vào session
        session()->put('arrayMusicID', $arrayMusicID);
        session()->put('arrayName', $arrayName);
        // session()->put('arraySinger', $arraySinger);

        session()->flash('message', 'Đã tạo film thành công');
        $this->resetInput();
        $this->emit('close-media-music-modal');
        $this->emit('clear_check_box', $names);
    }
    public function edit()
    {
        // $validatedDate = $this->validate([
        //     'name' => '',
        //     'contact_id' => '',
        // ]);
        // Log::info('ContractFileTable store');

        // ContractFile::create($validatedDate);

        // session()->flash('message', 'Users Created Successfully.');
        // $this->resetInput();

    }
    private function resetInput(){
        $this->start_time = null;
        $this->end_time = null;
        $this->money = null;
        $this->musicID = null;
        $this->musicID = null;
        $this->names = [];
    }
     public function openModal()
     {
         $this->emit('show');
     }
     public function checkbox($var){
        // $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => $var ] );
     }
     public function levelClicked()
     {
         // now the property $this->level will contain all the selected values
    // dd($this->level);
    // $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => $this->level ] );
     }
}
