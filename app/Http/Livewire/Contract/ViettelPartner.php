<?php

namespace App\Http\Livewire\Contract;

use App\Http\Livewire\Base\BaseLive;
use App\Models\Category;
use App\Models\Contract;
use App\Models\Music;
use App\Models\ActionLog;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ViettelPartner extends BaseLive
{
    public $searchTerm;
    public $searchName;
    public $searchCategory;
    public $searchStatus;
    public $setDate;
    public $deleteId;
    public $level = [];
    public $count_time;
    protected $listeners=[
        'searchTime'
    ];

    public function render()
    {
        if ($this->reset) {
            $this->searchCategory = null;
            $this->searchTerm = null;
            $this->searchName = null;
            $this->searchStatus = null;
            $this->setDate = null;
            $this->count_time = null;
            $this->reset = false;
        }
        $category = Category::all();
        $searchTerm = $this->searchTerm;
        $searchName = $this->searchName;
        $setDate = $this->setDate;
        $page = \Request::get('page') ? \Request::get('page') : 1;
        $media_id = \Request::get('media_id');
        $perPage = Config::get('app.per_page') ? Config::get('app.per_page') : 2;
        // $perPage = 10;
        $startLimit = $perPage * ($page - 1);
        $endLimit = $perPage * $page;

        $query = Contract::query();
        $query->where(function($query){
                 $query->where('contracts.end_time', '>=', date('Y-m-d'));
                 $query->orWhereNull('contracts.end_time');
             });
        if($this->searchStatus==1)
            $query = Contract::where('contracts.end_time', '<', date('Y-m-d'));
        elseif($this->searchStatus==2)
            $query = Contract::onlyTrashed();
        elseif($this->searchStatus==3)
            $query = Contract::withTrashed();

        $query->where(function ($query) {
            $query->where('type', '=', 3);
            // $query->orWhere('type', '=', 4);
        });
        if($this->searchTerm)
        $query->where('contracts.contract_number','like','%'.trim($this->searchTerm).'%');
        
        if ($media_id) {
            $music = Music::find($media_id);
            $query->whereIn('contract_number', [$music->contract_number1,$music->contract_number4]);
            // dd($query->get(), $music->contract_number1, $music->contract_number4);
        }
        $count_time = $this->count_time;
        if($count_time==7)
            $query->whereBetween('contracts.end_time',[Carbon::now()->format('Y-m-d'),Carbon::now()->addDay($count_time)->format('Y-m-d')]);
        elseif($count_time)
            $query->whereBetween('contracts.end_time',[Carbon::now()->addMonth($count_time-1)->addDays(1)->format('Y-m-d'),Carbon::now()->addMonth($count_time)->format('Y-m-d')]);
        $threeMonth=$this->endTimeContract(3);
        $twoMonth=$this->endTimeContract(2);
        $oneMonth=$this->endTimeContract(1);
        $oneWeek=$this->endTimeContract(7);
        $start_date = \Request::get('start_date') ? explode(" - ", \Request::get('start_date')) : null;
        if ($start_date != '') {
            $startDateCharge = $start_date[0];

            $endDateCharge = $start_date[1];

            if ($startDateCharge != '' && $endDateCharge != '') {
                $start = date("Y-m-d 00:00:00", strtotime($startDateCharge));
                $end = date("Y-m-d 23:59:59", strtotime($endDateCharge));
                $query->whereBetween('date_sign', [$start, $end]);
            }
        }
        $query->leftjoin('partners', function ($join) {
            $join->on('partners.id', '=', 'contracts.partner_id');
        });

        $query->leftJoin('categories', function ($join) {
            $join->on('contracts.category_id', '=', 'categories.id');
        });
        if ($this->searchCategory) {
            $query->where('contracts.type', '=', $this->searchCategory);
        }
        $query->select('contracts.*', DB::raw('partners.name as partners_name'), DB::raw('categories.name as category'));
        if(!empty($this->searchTerm)){
            $query->where('contract_number','like','%'.trim($searchTerm).'%');
        }
        if(!empty($this->searchName)){
            $query ->Where('music.name','like','%'.trim($searchName).'%');
            $data = $query->orderBy('contracts.id', 'desc')->offset($startLimit)->limit($perPage)->paginate($perPage);
            $model = config("common.permission.module.contract-partner", "contract-partner");
            return view('livewire.contract.viettel-partner', compact('data', 'threeMonth', 'twoMonth', 'oneMonth', 'oneWeek', 'category', 'model'));
       
       } 
       if(!empty($this->setDate)){
            $getDate = $setDate ? explode(" - ", $setDate) : null;
            if ($getDate != '') {
                $startDateCharge = $getDate[0];
                $endDateCharge = $getDate[1];
                if ($startDateCharge != '' && $endDateCharge != '') {
                     $start_time = date("Y-m-d", strtotime($startDateCharge));
                     $end_time = date("Y-m-d", strtotime($endDateCharge));
                     $query->whereBetween('contracts.end_time', [$start_time, $end_time]);
                }
            }
        }   

        $data = $query->orderBy('contracts.date_sign', 'desc')->paginate($perPage);
        $model = config("common.permission.module.contract-partner", "contract-partner");
        return view('livewire.contract.viettel-partner', compact('data', 'threeMonth', 'twoMonth', 'oneMonth', 'oneWeek', 'category', 'model'));
    }

    public function query1($count_time)
    {
        $this->count_time = $count_time;
        $this->resetPage();
    }
    public function updatedSearchTerm(){
        $this->resetPage();
    }

    public function updatedSetDate(){
        $this->resetPage();
    }
    public function updatedSearchStatus(){
        $this->resetPage();
    }
    public function endTimeContract($value)
    {
        if($value==7)
            $count = Contract::whereBetween('contracts.end_time',[Carbon::now()->format('Y-m-d'),Carbon::now()->addDay($value)->format('Y-m-d')])->where('type', 3)->count();
        else
            $count = Contract::whereBetween('contracts.end_time',[Carbon::now()->addMonth($value-1)->addDays(1)->format('Y-m-d'),Carbon::now()->addMonth($value)->format('Y-m-d')])->where('type', 3)->count();
        return $count;
    }

    public function deleteId($id)
    {
        $this->deleteId=$id;
    }

    public function searchTime($times){
        $this->setDate = $times;
        
    }

    public function delete(){
        if(Auth::user()->hasAnyRole('contract-partner-delete|administrator')) {
                $contractNumber = Contract::find($this->deleteId)->contract_number;
                if($contractNumber) {
                    Music::query()->where('contract_number1', $contractNumber)->update(['contract_number1'=>null]);
                    Music::query()->where('contract_number4', $contractNumber)->update(['contract_number4'=>null]);
                    Music::query()->where('tac_gia', $contractNumber)->update(['tac_gia'=>null]);
                }
                $contract=Contract::find($this->deleteId);
                ActionLog::writeActionLog($this->deleteId, $contract->getTable(), config("common.actions.deleted", "deleted"), get_class($this), "Xóa", $contract->contract_number, null, null);
                // ActionLog::where('record_id', '=', $this->deleteId)->where('model', '=', 'contracts')->delete();

                $contract->delete();
                $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Xóa hợp đồng thành công." ] );
                return;
        }
        abort(403, 'Unauthorized action.');
    }

    public function restore($id){
        // dd('đã vào');
        Contract::withTrashed()->where('id', $id)->restore();
        ActionLog::writeActionLog($id, 'contracts', config("common.actions.restore", "restore"), get_class($this), "Khôi phục", null, null, null);
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Khôi phục hợp đồng đối tác thành công." ] );
    }
}
