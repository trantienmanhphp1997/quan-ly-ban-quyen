<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'start_time' => 'date_format:d-m-Y',
            'end_time' => 'date_format:d-m-Y|after:start_time',
            'contract_number' => 'required|max:255|unique:contracts,contract_number,',
            'partner_id' => 'required',
        ];
    }
}
