<?php

namespace App\Http\Controllers\API;

use App\Services\MochaService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;

class SendMessageApiController extends BaseAPIController
{
    public function sendMessage(Request $request){
        Log::info("Send redirect message", [$request->all()]);
        $client = new \GuzzleHttp\Client();
        $res = $client->request('POST', 'http://cmsdev.quanlysanxuat.tk/api/send-mocha-message',[
            'form_params' => [
                'service_id' => $request->service_id,
                'content' => $request["content"],
                'label' => $request->label,
                'msisdns' => $request->msisdns,
                'deeplink' => $request->deeplink,
            ],
        ]);
        MochaService::sendMessageToUser($request->service_id, $request["content"], $request->msisdns, $request->label, $request->deeplink);
    }
}

