<?php

namespace App\Http\Controllers\Admin\Tool;

use App\Http\Controllers\Controller;
use App\Models\TypeFee;
use Illuminate\Http\Request;

class TypeFeeController extends Controller
{
    public function __construct() {
        $this->checkPermission(config("common.permission.module.revenue-fee", "revenue-fee"));
    }

    public function index(){
        $data=TypeFee::orderBy('id','desc')->get();
        $model = config("common.permission.module.type-fee", "revenue-fee");
        return view('admin.tool.fee.index', compact('data', 'model'));
    }
    public function create(){
        return view('admin.tool.fee.create');
    }
    public function store(Request $request){
        $this->validate($request,[
            'name'=> 'required',
            'name_unique'=>'required|unique:type_fee,name_unique'
        ],[],[
            'name' => 'Tên loại chi phí',
            'name_unique' => 'Mã',
        ]);
        TypeFee::create($request->input());
        return redirect()->route('type_fee.index')->with('success','Thêm phí thành công');
    }
    public function edit($id){
        $data=TypeFee::find($id);
        return view('admin.tool.fee.edit', compact('data'));
    }
    public function update(Request $request,$id){
        $this->validate($request,[
            'name'=> 'required',
            'name_unique'=>'required|unique:type_fee,name_unique,'.$id,
        ],[],[
            'name' => 'Tên loại chi phí',
            'name_unique' => 'Mã',
        ]);
        $fee=TypeFee::find($id);
        $fee->update($request->all());
        return redirect()->route('type_fee.index')->with('success','Cập nhật thông tin phí thành công');
    }
    public function delete($id){
        TypeFee::find($id)->delete();
        return redirect()->route('type_fee.index')->with('success','Xóa phí thành công');
    }
}
