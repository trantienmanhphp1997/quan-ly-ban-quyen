<?php

namespace App\Http\Controllers\Admin\Tool;

use App\Models\Permission;
use App\Models\Role;
use App\Models\ActionLog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class CreateRoleController extends Controller
{
    public function __construct() {
        $this->checkPermission(config("common.permission.module.roles", "roles"));
    }


    public function index(Request $request){
        $perPage = Config::get('app.per_page') ? Config::get('app.per_page') : 10;
        $data = Role::orderBy('id','DESC')->paginate($perPage);
        $model = config("common.permission.module.roles", "roles");
        return view('admin.tool.role.index',compact('data', 'model'));
    }

    public function create(){
        return view('admin.tool.role.create');
    }

    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required|unique:roles,name',
        ]);

        $role = new Role();
        $role->name = $request->input('name');
        $role->note = $request->input('note');
        $role->guard_name = 'web';
        $role->save();

        return redirect()->route('tool.roles')
            ->with('success','Thêm quyền mới thành công');
    }

    public function edit($id)
    {
        $role = Role::find($id);
        return view('admin.tool.role.edit',compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->note = $request->input('note');
        $role->save();

        return redirect()->route('tool.roles')
            ->with('success','Cập nhật quyền thành công');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table("roles")->where('id',$id)->delete();
        return redirect()->route('tool.roles')
            ->with('success','Xóa quyền thành công ');
    }
}
