<?php

namespace App\Http\Controllers\Admin\Tool;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ActionLog;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class ActionLogController extends Controller
{
    protected $perPage = 2;
    protected $editRoutesMap = [];

    public function index(Request $request)
    {

        $this->perPage = Config::get('app.per_page') ? Config::get('app.per_page') : 10;
        $this->editRoutesMap = [
            'music' => 'media.music.edit',
            'films' => 'media.films.edit',
            'contracts' => 'contract.viettel-buy.edit',
        ];

        $search_type = $request->input('search_type', 'music');
        // dd($search_type);
        $name = $request->input('name', '');

        $query = DB::table('action_logs')
            ->select('action_logs.*')->where('model', '=', $search_type)
            ->join('users', 'users.id', '=', 'action_logs.admin_id')
            ->addSelect('users.full_name AS admin_name');
        if(Auth::user()->is_manager != 1)
            $query->where('action_logs.admin_id', Auth::user()->id);
        if($request->start_date){
            $getDate = $request->start_date ? explode(" - ", $request->start_date) : null;
            if ($getDate != '') {
                $startDateCharge = $getDate[0];
                $endDateCharge = $getDate[1];
                if ($startDateCharge != '' && $endDateCharge != '') {
                    $start_time = date("Y-m-d", strtotime($startDateCharge));
                    $end_time = date("Y-m-d", strtotime($endDateCharge. " +1 days"));
                    $query->whereBetween('action_logs.updated_at', [$start_time, $end_time]);
                }
            }
        }

        if ($search_type == 'films') {
            $query->leftjoin('films', 'action_logs.record_id', '=', 'films.id')
                ->addSelect('films.vn_name AS record_name')
                ->addSelect('films.deleted_at as delete_time');
            if ($name != '') {
                $query->where('films.vn_name', 'like', '%'.$name.'%');
            }

        } else if ($search_type == 'music') {
            $query->leftjoin('music', 'action_logs.record_id', '=', 'music.id')
                ->addSelect('music.name AS record_name')
                ->addSelect('music.deleted_at as delete_time');
            if ($name != '') {
                $query->where('music.name', 'like', '%'.$name.'%');
            }

        } else if ($search_type == 'contracts') {
            $query->leftjoin('contracts', 'action_logs.record_id', '=', 'contracts.id')
                ->addSelect('contracts.type as contract_type')
                ->addSelect('contracts.contract_number AS record_name')
                ->addSelect('contracts.deleted_at AS delete_time');
            if ($name != '') {
                $query->where('contracts.contract_number', 'like', '%'.$name.'%');
            }
            // dd($query->get());
        }
        $data = $query->orderBy('action_logs.id', 'desc')->paginate($this->perPage);
        $data->each(function ($item) {
            $item->edit_route = $this->editRoutesMap[$item->model];
            if (isset($item->contract_type)) {
                $item->edit_route = ($item->contract_type == 1||$item->contract_type==4) ? 'contract.viettel-buy.edit' : (($item->contract_type == 2)?'contract.viettel-sell.edit':'contract.viettel-partner.edit');
            }
        });


        return view('admin.tool.actionLog.index')->with([
            'i' => ($request->input('page', 1) - 1) * $this->perPage,
            'data' => $data,
        ]);
    }

    public function download($id){
        return Storage::download(ActionLog::find($id)->file);
    }
}
