<?php

namespace App\Http\Controllers\Admin\Tool;

use App\Models\Role;
use App\Events\DemoEvent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\ActionLog;
use App\Models\Category;

class CreateAdminController extends Controller
{
    public function __construct() {
        $this->checkPermission(config("common.permission.module.users", "users"));
    }

    public function index(Request $request)
    {
        if($request->username) {
            $users = User::search($request->username, null, true)->paginate(10);
            $roles = Role::pluck('id');
        }
        else {
            $users = User::orderBy('id','DESC')->paginate(5);
            $roles = Role::pluck('id');
        }
        foreach ($users as $user) {
            $user->roleDontBelongto = $roles->diff($user->roles()->get()->pluck('id'));
            $user->roleDontBelongto = Role::find($user->roleDontBelongto);
        }
        $model = config("common.permission.module.users", "users");
        $categoryList = Category::all()->pluck('name','id')->toArray();
        return view('admin.tool.admin.index',compact('users', 'model','categoryList'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function create(){
        $roles = Role::pluck('note','id', 'name');
        $categoryList = Category::all()->pluck('name','id')->toArray();
        return view('admin.tool.admin.create',compact('roles','categoryList'));
    }

    public function show($id)
    {
        $user = User::find($id);

        return view('admin.tool.admin.show',compact('user'));
    }

    public function sendMail($id)
    {
        event(new DemoEvent($id));

        return redirect()->route('createAdmin.index')
        ->with('success','Send Mail Successfully');
    }
    public function store(Request $request){

        $this->validate($request, [
            'username' => 'required|unique:users,username,',
            'full_name' => 'required',
            'email' => 'required|email|unique:users,email',
            'phone_number'=>'nullable|unique:users,phone_number|phone',
            'password' => 'required|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*#?&]).+$/',
            'confirm-password' => 'required_with:password|same:password|min:8'
        ],[],[
            'username'=>'Tên đăng nhập',
            'full_name'=>'Họ và tên',
            'email' => 'Email',
            'phone_number'=>'Số điện thoại',
            'password' => 'Mật khẩu',
            'confirm-password' => 'Mật khẩu xác nhận'
        ]);
        $input = $request->all();
        if($request->password){
            $input['password'] = Hash::make($input['password']);
        }
        $user=User::create($input);
        $user->assignRole($request->input('roles'));
        return redirect()->route('createAdmin.index')
            ->with('success','Thêm thành công');
    }
    public function edit($id){

        $roles = Role::pluck('note','id', 'name');
        $user = User::find($id);
        $userRole = $user->roles()->pluck('role_id');
        $role = Role::pluck('id');
        $user->roleDontBelongto = $role->diff($user->roles()->get()->pluck('id'));
        $user->roleDontBelongto = Role::find($user->roleDontBelongto);
        $categoryList = Category::all()->pluck('name','id')->toArray();
        return view('admin.tool.admin.edit',compact('user', 'roles','userRole','roles','categoryList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        // dd($request->all());
        $this->validate($request, [
            'username' => 'required|unique:users,username,'.$id,
            'full_name' => 'required',
            'phone_number'=>'nullable|phone|unique:users,phone_number,'.$id,
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'nullable|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*#?&])(?=.*\d).+$/',
            // 'confirm-password' => 'required_with:password|same:password|min:8'
        ],[],[
            'username'=>'Tên đăng nhập',
            'full_name'=>'Họ và tên',
            'phone_number'=>'Số điện thoại',
            'email' => 'Email',
            'password' => 'Mật khẩu',
            // 'confirm-password' => 'Mật khẩu xác nhận'
        ]);
        // dd('vào');
        $user = User::find($id);
        $input = $request->all();
        if($input['password'])
            $input['password'] = Hash::make($input['password']);
        else
            unset($input['password']);
        if($request->is_manager)
            $request->is_manager=1;
        else $user->is_manager=0;

        $user->update($input);
        $user->save();
        $user->syncRoles($request->input('roles'));
        return redirect()->route('createAdmin.index')
            ->with('success','Cập nhật tài khoản thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Admin::find($id)->delete();

        return redirect()->route('createAdmin.index')
            ->with('success','Xóa tài khoản thành công');
    }

    public function updateRole(Request $request)
    {
        // dd('bào');
        $user = User::find($request->id);
        // dd($request->role_name);
        if(!empty($request->role_name)) {
            $user->syncRoles($request->role_name);
        }
        else DB::table('model_has_roles')->where('model_id',$request->id)->delete();

        return back()
            ->with('success','Cập nhật quyền thành công');
    }
}
