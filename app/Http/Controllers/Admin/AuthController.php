<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        $user = Auth::user();
        return view('auth.profile', compact('user'));
    }

    public function editPassword(){
        return view('auth.passwords.change');
    }

    public function updatePassword(Request $request)
    {
        if(Auth::Check())
        {
            $this->validate($request, [
                'current_password' => 'required',
                'password' => 'required|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*#?&])(?=.*\d).+$/',
                'confirm_password' => 'required|same:password',
                ], [], [
                'current_password' => 'Mật khẩu hiện tại',
                'password' => 'Mật khẩu mới',
                'confirm_password' => 'Mật khẩu xác nhận',
            ]);

            if(Hash::check($request->current_password, Auth::user()->password))
            {
                $user = Auth::user();
                $user->password = Hash::make($request->password);
                $user->save();
                return redirect()->route('home')
                    ->with('success','Đổi mật khẩu thành công');
            }
            else
            {
                return redirect()->route('password.edit')->with('error','Nhập sai mật khẩu hiện tại');
            }
        }
        else
        {
            return redirect()->to('/');
        }
    }

}
