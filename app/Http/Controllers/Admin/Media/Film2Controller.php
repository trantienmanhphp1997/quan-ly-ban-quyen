<?php
 
namespace App\Http\Controllers\Admin\Media;

use App\Component\Recursive;
use App\Exports\FilmExampleExport;
use App\Exports\FilmExport;
use App\Exports\FilmListExampleExport;
use App\Http\Controllers\Controller;
use App\Imports\EmployeeImport;
use App\Imports\EmployeeImport1;
use App\Imports\FilmListUploadImport;
use App\Imports\FilmUploadImport;
use App\Models\ActionLog;
use App\Models\CategoryFilm;
use App\Models\Contract;
use App\Models\ContractMediaRole;
use App\Models\Country;
use App\Models\Film;
use App\Models\Partner;
use App\Models\Revenue;
use App\Models\RevenueType;
use App\Models\RoleMedia;
use App\Models\TypeFee;
use Illuminate\Support\Facades\Storage;
use DateTime;
use DB;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class Film2Controller extends Controller
{
    public function __construct()
    {
        $this->checkPermission(config("common.permission.module.film", "film"));
    }

    public function index()
    {
        return view('admin/media/film2/index');
    }
    public function create()
    {
        $revenueParentType = RevenueType::parent();
        $revenueParentTypeJson = json_encode(RevenueType::parent()->pluck('id')->toArray());
        $cate_film = CategoryFilm::pluck('name', 'id');
        $countries = Country::pluck('name', 'id');
        $data_type1 = $this->getOption(1);
        $data_type2 = $this->getOption(2);
        $data_type3 = $this->getOption(3);
        $revenueType = RevenueType::where('parent_id', '!=', 0)->get();
        $partnerName = Partner::pluck('name','id')->toArray();
        $contractName = Contract::where('category_id',2)->where('type',1)->pluck('contract_number','id')->toArray();
        return view('admin/media/film/create', compact('data_type1', 'data_type2','data_type3', 'cate_film', 'countries', 'revenueType', 'revenueParentType', 'revenueParentTypeJson', 'partnerName', 'contractName'));
    }
    public function contractExtension(Request $request, $id)
    {
        // dd('đã vào');
        $data = Film::find($id);
        $list_type = TypeFee::pluck('name', 'id');
        $admin_id = $data->admin_id;
        return view('admin.media.film2.extension', compact('data', 'list_type'));
        // return view('admin.media.music.contractExtension',compact('music'));
    }
    public function extension(Request $request, $id)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'total_fee' => 'nullable|numeric|min:0',
            'end_time' => empty($request->start_time) ? 'nullable|date' : 'required|date|after_or_equal:start_time', 
        ], [
            'total_fee.numeric' => 'Chi phí phải là 1 số', 
            'total_fee.min' => 'Chi phí phải lớn hơn 0', 
            'end_time.after_or_equal' => 'Thời gian kết thúc phải lớn hơn hoặc bằng thời gian bắt đầu',
            'end_time.required' => 'Nhập vào thời gian kết thúc'
        ]);

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator->errors());
        }

        // Revenue::create($input);
        $film = Film::find($id);
        $film->fill($input);
        $changes = $film->getDirty();
        $old_value = array();
        // dd($changes);
        foreach ($changes as $key => $value) {
            $old_value[$key] = $film->getOriginal($key);
        }

        $film=Film::find($id);
        $film->end_time=$request->end_time;
        $film->total_fee=$request->total_fee;
        if(!empty($changes))
        {
            if(isset($changes['end_time'])){
                $changes['end_time'] = reFormatDate($changes['end_time']);
                $old_value['end_time'] = reFormatDate($old_value['end_time']);
            }
            $log = ActionLog::writeActionLog($id, $film->getTable(), config("common.actions.extension", "extension"), get_class($this), "Gia hạn", $old_value, $changes, null);
        }
        $film->save();
        return redirect()->route('media.films')->with('success','Gia hạn thành công');
    }
    // public function upload(Request $req){
    //     ini_set('memory_limit', '-1');
    //     set_time_limit(0);
    //     if($req->hasFile('file')){
    //         $file = $req->file('file');
    //         $pathFile = $file->getClientOriginalExtension();
    //         if($pathFile=='xlsx'){
    //             $array = Excel:: toArray(new EmployeeImport,$req->file);
    //             // Loại bỏ những cột không có dữ liệu
    //             $arrayIndex = [];
    //             $arrayContract = [];
    //             $array = $array[0];
    //             for($i=0; $i < count($array[0]); $i++){
    //                 if($array[0][$i]!=null){
    //                     array_push($arrayIndex,$i);
    //                 }
    //             }
    //             // retrun nếu không đủ hàng
    //             if(count($arrayIndex)<20){
    //                 return redirect()->back()->with('message_danger','File '.$file->getClientOriginalName().' không đúng, xin kiểm tra lại!');
    //             }
    //             $purchase_contract = $req->purchase_contract;
    //             if($purchase_contract=='buy'){
    //                 $purchase_contract = 1;
    //             }
    //             else if($purchase_contract=='sale'){
    //                 $purchase_contract = 2;
    //             }
    //             for($i = 0; $i < count($array); $i++){
    //                 $film_country = trim($array[$i][$arrayIndex[1]]);
    //                 if($film_country==null) {
    //                     continue;
    //                 }
    //                 $film_name = trim($array[$i][$arrayIndex[2]]);
    //                 if($film_name==null) {
    //                     continue;
    //                 }
    //                 $film_year_created = trim($array[$i][$arrayIndex[3]]);
    //                 if($film_year_created==null) {
    //                     continue;
    //                 }
    //                 $film_category_name = trim($array[$i][$arrayIndex[4]]);
    //                 if($film_category_name==null) {
    //                     continue;
    //                 }
    //                 $film_count_name = trim($array[$i][$arrayIndex[5]]);
    //                 if($film_count_name==null) {
    //                     continue;
    //                 }
    //                 $film_count_tap = trim($array[$i][$arrayIndex[6]]);
    //                 if($film_count_tap==null) {
    //                     continue;
    //                 }
    //                 $film_count_hour = trim($array[$i][$arrayIndex[7]]);
    //                 if($film_count_hour==null) {
    //                     continue;
    //                 }
    //                 $contract_partner_name = trim($array[$i][$arrayIndex[8]]);
    //                 if($contract_partner_name==null) {
    //                     continue;
    //                 }
    //                 $contract_permissin_contract = trim($array[$i][$arrayIndex[9]]);
    //                 if($contract_permissin_contract==null) {
    //                     continue;
    //                 }
    //                 $contract_date_end = trim($array[$i][$arrayIndex[10]]);
    //                 if($contract_date_end==null) {
    //                     continue;
    //                 }
    //                 else if(is_numeric($contract_date_end)){
    //                     $contract_date_end = (int)$contract_date_end;
    //                     $contract_date_end -= 25569;
    //                     $contract_date_end = date("Y-m-d H:i:s", $contract_date_end*3600*24);
    //                 }
    //                 else {
    //                     $time = strtotime($contract_date_end);f
    //                     $contract_date_end = date('Y-m-d H:i:s',$time);
    //                     $new_date = date('d-m-Y',$time);
    //                     if($new_date=='01-01-1970'){
    //                         continue;
    //                     }
    //                 }
    //                 $contract_date_begin = trim($array[$i][$arrayIndex[11]]);
    //                 if($contract_date_begin==null) {
    //                     continue;
    //                 }
    //                 else if(is_numeric($contract_date_begin)){
    //                     $contract_date_begin = (int)$contract_date_begin;
    //                     $contract_date_begin -= 25569;
    //                     $contract_date_begin = date("Y-m-d H:i:s", $contract_date_begin*3600*24);
    //                 }
    //                 else {
    //                     $time = strtotime($contract_date_begin);
    //                     $contract_date_begin = date('Y-m-d H:i:s',$time);
    //                     $new_date = date('d-m-Y',$time);
    //                     if($new_date=='01-01-1970'){
    //                         continue;
    //                     }
    //                 }
    //                 $contract_contract_number = trim($array[$i][$arrayIndex[12]]);
    //                 if($contract_contract_number==null) {
    //                     continue;
    //                 }
    //                 $contract_fee1 = trim($array[$i][$arrayIndex[13]]);
    //                 $contract_fee1 = str_replace(',', '', $contract_fee1);
    //                 $contract_fee1 = str_replace('.', '', $contract_fee1);
    //                 if($contract_fee1==null||!is_numeric($contract_fee1)) {
    //                     continue;
    //                 }
    //                 $contract_fee2 = trim($array[$i][$arrayIndex[14]]);
    //                 $contract_fee2 = str_replace(',', '', $contract_fee2);
    //                 $contract_fee2 = str_replace('.', '', $contract_fee2);
    //                 if($contract_fee2==null||!is_numeric($contract_fee2)) {
    //                     continue;
    //                 }
    //                 $contract_fee3 = trim($array[$i][$arrayIndex[15]]);
    //                 $contract_fee3 = str_replace(',', '', $contract_fee3);
    //                 $contract_fee3 = str_replace('.', '', $contract_fee3);
    //                 if($contract_fee3==null||!is_numeric($contract_fee3)) {
    //                     continue;
    //                 }
    //                 $film_note = trim($array[$i][$arrayIndex[17]]);
    //                 if($film_note==null) {
    //                     continue;
    //                 }
    //                 $film_actor = trim($array[$i][$arrayIndex[18]]);
    //                 if($film_actor==null) {
    //                     continue;
    //                 }
    //                 $film_dirrector = trim($array[$i][$arrayIndex[19]]);
    //                 if($film_dirrector==null) {
    //                     continue;
    //                 }
    //                 // $arrayContract[$contract_contract_number] = $i;
    //                 // array_push($arrayContract[$contract_contract_number], $i);
    //                 $data  = DB::table('contracts')->where('contract_number', $contract_contract_number)->get();
    //                 if(count($data)==1){
    //                     DB::table('films')->insert([
    //                         'vn_name' => $film_name ,
    //                         'actor_name' => $film_actor,
    //                         'start_time' => $contract_date_begin,
    //                         'country' => $film_country ,
    //                         'year_create' => $film_year_created,

    //                         'admin_id'=> Auth::user()->id,
    //                         'category_name' => $film_category_name,
    //                         'count_name' =>$film_count_name,
    //                         'count_tap' => $film_count_tap,
    //                         'count_hour' => $film_count_hour,
    //                         'partner_name' =>$contract_partner_name,
    //                         'permissin_contract' => $contract_permissin_contract,
    //                         'end_time' => $contract_date_end,
    //                         'note' =>$film_note,
    //                         'director' => $film_dirrector,
    //                         'contract_id'=> $data[0]->id,
    //                     ]);
    //                 }
    //                 else {
    //                     // dd($purchase_contract);
    //                     DB::table('contracts')->insert([
    //                         'contract_number' =>$contract_contract_number,
    //                         'fee1' => $contract_fee1,
    //                         'fee2' => $contract_fee2,
    //                         'fee3' => $contract_fee3,
    //                         'type' => $purchase_contract,
    //                         'category_id' => 2,
    //                         'admin_id'=> Auth::user()->id,
    //                     ]);
    //                     $data = DB::table('contracts')->where('contract_number',$contract_contract_number)->get();
    //                     DB::table('films')->insert([
    //                         'vn_name' => $film_name ,
    //                         'actor_name' => $film_actor,
    //                         'start_time' => $contract_date_begin,
    //                         'country' => $film_country ,
    //                         'year_create' => $film_year_created,
    //                         'admin_id'=> Auth::user()->id,
    //                         'category_name' => $film_category_name,
    //                         'count_name' =>$film_count_name,
    //                         'count_tap' => $film_count_tap,
    //                         'count_hour' => $film_count_hour,
    //                         'partner_name' =>$contract_partner_name,
    //                         'permissin_contract' => $contract_permissin_contract,
    //                         'end_time' => $contract_date_end,
    //                         'note' =>$film_note,
    //                         'director' => $film_dirrector,
    //                         'contract_id'=> $data[0]->id,
    //                     ]);
    //                 }
    //             }
    //             return redirect()->route('media.films')->with('message_success', 'Thêm mới từ file '.$file->getClientOriginalName().' thành công!');
    //         }
    //         else {
    //             return redirect()->route('media.films')->with('message_danger','Không đúng định dạng file Excel!');
    //         }
    //     }
    //     else {
    //         return redirect()->route('media.films')->with('message_danger', 'Chưa có file Excel!');
    //     }
    // }
    public function upload2(Request $req)
    {
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        if ($req->hasFile('file')) {
            $file = $req->file('file');
            $pathFile = $file->getClientOriginalExtension();
            if ($pathFile == 'xlsx') {
                $array = Excel::toArray(new EmployeeImport, $req->file);
                $listCodeOldĐQ = DB::table('role_media')->where('type', 1)->pluck('code', 'id');
                $listCodeOldĐQ = json_decode(json_encode($listCodeOldĐQ), true);
                $listCodeOldKĐQ = DB::table('role_media')->where('type', 2)->pluck('code', 'id');
                $listCodeOldKĐQ = json_decode(json_encode($listCodeOldKĐQ), true);
                $newArrayCodeĐQ = [];
                $newArrayCodeKĐQ = [];
                $newArrayCodeĐQID = [];
                $newArrayCodeKĐQID = [];

                // dd($listCodeOldĐQ);
                // Loại bỏ những cột không có dữ liệu
                $arrayIndex = [];
                $arrayContract = [];
                $array = $array[0];
                for ($i = 0; $i < count($array[0]); $i++) {
                    if ($array[0][$i] != null) {
                        array_push($arrayIndex, $i);
                    }
                }
                // retrun nếu không đủ hàng
                if (count($arrayIndex) < 20) {
                    return redirect()->back()->with('message_danger', 'File ' . $file->getClientOriginalName() . ' không đúng, xin kiểm tra lại!');
                }
                $purchase_contract = $req->purchase_contract;
                if ($purchase_contract == 'buy') {
                    $purchase_contract = 1;
                } else if ($purchase_contract == 'sale') {
                    $purchase_contract = 2;
                }
                for ($i = 0; $i < count($array); $i++) {
                    $film_country = trim($array[$i][$arrayIndex[1]]);
                    if ($film_country == null) {
                        continue;
                    }
                    $film_name = trim($array[$i][$arrayIndex[2]]);
                    if ($film_name == null) {
                        continue;
                    }
                    $film_year_created = trim($array[$i][$arrayIndex[3]]);
                    if ($film_year_created == null) {
                        continue;
                    }
                    $film_category_name = trim($array[$i][$arrayIndex[4]]);
                    if ($film_category_name == null) {
                        continue;
                    }
                    $film_count_name = trim($array[$i][$arrayIndex[5]]);
                    if ($film_count_name == null) {
                        continue;
                    }
                    $film_count_tap = trim($array[$i][$arrayIndex[6]]);
                    if ($film_count_tap == null) {
                        continue;
                    }
                    $film_count_hour = trim($array[$i][$arrayIndex[7]]);
                    if ($film_count_hour == null) {
                        continue;
                    }
                    $contract_partner_name = trim($array[$i][$arrayIndex[8]]);
                    if ($contract_partner_name == null) {
                        continue;
                    }
                    // oke
                    $contract_doc_quyen = trim($array[$i][$arrayIndex[9]]);
                    if ($contract_doc_quyen == null) {
                        continue;
                    }
                    $contract_khong_doc_quyen = trim($array[$i][$arrayIndex[10]]);
                    if ($contract_khong_doc_quyen == null) {
                        continue;
                    }
                    $contract_date_end = trim($array[$i][$arrayIndex[11]]);
                    if ($contract_date_end == null) {
                        continue;
                    } else if (is_numeric($contract_date_end)) {
                        $contract_date_end = (int) $contract_date_end;
                        $contract_date_end -= 25569;
                        $contract_date_end = date("Y-m-d H:i:s", $contract_date_end * 3600 * 24);
                    } else {
                        $time = strtotime($contract_date_end);
                        $contract_date_end = date('Y-m-d H:i:s', $time);
                        $new_date = date('d-m-Y', $time);
                        if ($new_date == '01-01-1970') {
                            continue;
                        }
                    }
                    $contract_date_begin = trim($array[$i][$arrayIndex[12]]);
                    if ($contract_date_begin == null) {
                        continue;
                    } else if (is_numeric($contract_date_begin)) {
                        $contract_date_begin = (int) $contract_date_begin;
                        $contract_date_begin -= 25569;
                        $contract_date_begin = date("Y-m-d H:i:s", $contract_date_begin * 3600 * 24);
                    } else {
                        $time = strtotime($contract_date_begin);
                        $contract_date_begin = date('Y-m-d H:i:s', $time);
                        $new_date = date('d-m-Y', $time);
                        if ($new_date == '01-01-1970') {
                            continue;
                        }
                    }
                    $contract_contract_number = trim($array[$i][$arrayIndex[13]]);
                    if ($contract_contract_number == null) {
                        continue;
                    }
                    $contract_fee1 = trim($array[$i][$arrayIndex[14]]);
                    $contract_fee1 = str_replace(',', '', $contract_fee1);
                    $contract_fee1 = str_replace('.', '', $contract_fee1);
                    if ($contract_fee1 == null || !is_numeric($contract_fee1)) {
                        continue;
                    }
                    $contract_fee2 = trim($array[$i][$arrayIndex[15]]);
                    $contract_fee2 = str_replace(',', '', $contract_fee2);
                    $contract_fee2 = str_replace('.', '', $contract_fee2);
                    if ($contract_fee2 == null || !is_numeric($contract_fee2)) {
                        continue;
                    }
                    $contract_fee3 = trim($array[$i][$arrayIndex[16]]);
                    $contract_fee3 = str_replace(',', '', $contract_fee3);
                    $contract_fee3 = str_replace('.', '', $contract_fee3);
                    if ($contract_fee3 == null || !is_numeric($contract_fee3)) {
                        continue;
                    }
                    $film_note = trim($array[$i][$arrayIndex[18]]);
                    if ($film_note == null) {
                        continue;
                    }
                    $film_actor = trim($array[$i][$arrayIndex[19]]);
                    if ($film_actor == null) {
                        continue;
                    }
                    $film_dirrector = trim($array[$i][$arrayIndex[20]]);
                    if ($film_dirrector == null) {
                        continue;
                    }
                    // $arrayContract[$contract_contract_number] = $i;
                    // array_push($arrayContract[$contract_contract_number], $i);
                    $data = DB::table('contracts')->where('contract_number', $contract_contract_number)->get();
                    // loại bỏ khoảng trắng và ',' ở cuối
                    $arrayĐQ = trim($contract_doc_quyen);
                    $arrayĐQ = Str::of($arrayĐQ)->trim(',');
                    // chuyển thành mảng
                    $arrayĐQ = explode(',', $arrayĐQ);

                    // loại bỏ khoảng trắng và ',' ở cuối
                    $arrayKĐQ = trim($contract_khong_doc_quyen);
                    $arrayKĐQ = Str::of($arrayKĐQ)->trim(',');
                    // chuyển thành mảng
                    $arrayKĐQ = explode(',', $arrayKĐQ);

                    // loại bỏ khoảng trắng và ',' ở cuối
                    $arrayTL = trim($film_category_name);
                    $arrayTL = Str::of($arrayTL)->trim(',');
                    // chuyển thành mảng
                    $arrayTL = explode(',', $arrayTL);

                    if (count($data) == 1) {
                        $film = new Film;
                        $film->vn_name = $film_name;
                        $film->actor_name = $film_actor;
                        $film->start_time = $contract_date_begin;
                        $film->country = $this->filmConntry($film_country);
                        $film->year_create = $film_year_created;

                        $film->admin_id = Auth::user()->id;
                        $film->count_name = $film_count_name;
                        $film->count_tap = $film_count_tap;
                        $film->count_hour = $film_count_hour;
                        $film->partner_name = $contract_partner_name;
                        $film->end_time = $contract_date_end;
                        $film->note = $film_note;
                        $film->director = $film_dirrector;
                        $film->contract_id = $data[0]->id;
                        $film->save();
                        $film->categories()->attach($this->categoryFilm($arrayTL));
                        foreach ($arrayĐQ as $value) {
                            $value = trim($value);
                            $role_id;
                            // nếu bản ghi tồn tại trong những thể loại code mới
                            if (array_search($value, $newArrayCodeĐQ)) {
                                $vitri = array_search($value, $newArrayCodeĐQ);
                                $role_id = $newArrayCodeĐQID[$vitri];
                            } else {
                                // nếu bản ghi đã tồn tại trong những thể loại code cũ
                                if (array_search($value, $listCodeOldĐQ)) {
                                    $role_id = array_search($value, $listCodeOldĐQ);
                                } else {
                                    array_push($newArrayCodeĐQ, $value);
                                    // thêm dữ role media mới
                                    $roleMedia = new RoleMedia;
                                    $roleMedia->code = $value;
                                    $roleMedia->categori_id = 2;
                                    $roleMedia->type = 1;
                                    $roleMedia->save();
                                    array_push($newArrayCodeĐQID, $roleMedia->id);
                                    $role_id = $roleMedia->id;
                                };
                            }

                            DB::table('contract_media_role')->insert([
                                'contract_id' => $data[0]->id,
                                'category_id' => 2,
                                'role_id' => $role_id,
                                'media_id' => $film->id,
                            ]);

                        }
                        foreach ($arrayKĐQ as $value) {
                            $value = trim($value);
                            $role_id;
                            // nếu bản ghi tồn tại trong những thể loại code mới
                            if (array_search($value, $newArrayCodeKĐQ)) {
                                $vitri = array_search($value, $newArrayCodeKĐQ);
                                $role_id = $newArrayCodeKĐQID[$vitri];
                            } else {
                                // nếu bản ghi đã tồn tại trong những thể loại code cũ
                                if (array_search($value, $listCodeOldKĐQ)) {
                                    $role_id = array_search($value, $listCodeOldKĐQ);
                                } else {
                                    array_push($newArrayCodeKĐQ, $value);
                                    // thêm dữ role media mới
                                    $roleMedia = new RoleMedia;
                                    $roleMedia->code = $value;
                                    $roleMedia->categori_id = 2;
                                    $roleMedia->type = 2;
                                    $roleMedia->save();
                                    array_push($newArrayCodeKĐQID, $roleMedia->id);
                                    $role_id = $roleMedia->id;
                                };
                            }

                            DB::table('contract_media_role')->insert([
                                'contract_id' => $data[0]->id,
                                'category_id' => 2,
                                'role_id' => $role_id,
                                'media_id' => $film->id,
                            ]);

                        }
                    } else {
                        $contract = DB::table('contracts')->insert([
                            'contract_number' => $contract_contract_number,
                            'fee1' => $contract_fee1,
                            'fee2' => $contract_fee2,
                            'fee3' => $contract_fee3,
                            'type' => $purchase_contract,
                            'category_id' => 2,
                            'admin_id' => Auth::user()->id,
                        ]);
                        $data = DB::table('contracts')->where('contract_number', $contract_contract_number)->get();
                        $film = new Film;
                        $film->vn_name = $film_name;
                        $film->actor_name = $film_actor;
                        $film->start_time = $contract_date_begin;
                        $film->country = $this->filmConntry($film_country);
                        $film->year_create = $film_year_created;
                        $film->admin_id = Auth::user()->id;
                        $film->count_name = $film_count_name;
                        $film->count_tap = $film_count_tap;
                        $film->count_hour = $film_count_hour;
                        $film->partner_name = $contract_partner_name;
                        $film->end_time = $contract_date_end;
                        $film->note = $film_note;
                        $film->director = $film_dirrector;
                        $film->contract_id = $data[0]->id;
                        $film->save();
                        $film->categories()->attach($this->categoryFilm($arrayTL));

                        foreach ($arrayĐQ as $value) {
                            $value = trim($value);
                            $role_id;
                            // nếu bản ghi tồn tại trong những thể loại phim code mới
                            if (array_search($value, $newArrayCodeĐQ)) {
                                $vitri = array_search($value, $newArrayCodeĐQ);
                                $role_id = $newArrayCodeĐQID[$vitri];
                            } else {
                                // nếu bản ghi đã tồn tại trong những thể loại code cũ
                                if (array_search($value, $listCodeOldĐQ)) {
                                    $role_id = array_search($value, $listCodeOldĐQ);
                                } else {
                                    array_push($newArrayCodeĐQ, $value);
                                    // thêm dữ role media mới
                                    $roleMedia = new RoleMedia;
                                    $roleMedia->code = $value;
                                    $roleMedia->categori_id = 2;
                                    $roleMedia->type = 1;
                                    $roleMedia->save();
                                    array_push($newArrayCodeĐQID, $roleMedia->id);
                                    $role_id = $roleMedia->id;
                                };
                            }

                            DB::table('contract_media_role')->insert([
                                'contract_id' => $data[0]->id,
                                'category_id' => 2,
                                'role_id' => $role_id,
                                'media_id' => $film->id,
                            ]);
                        }
                        foreach ($arrayKĐQ as $value) {
                            $value = trim($value);
                            $role_id;
                            // nếu bản ghi tồn tại trong những thể loại code mới
                            if (array_search($value, $newArrayCodeKĐQ)) {
                                $vitri = array_search($value, $newArrayCodeKĐQ);
                                $role_id = $newArrayCodeKĐQID[$vitri];
                            } else {
                                // nếu bản ghi đã tồn tại trong những thể loại code cũ
                                if (array_search($value, $listCodeOldKĐQ)) {
                                    $role_id = array_search($value, $listCodeOldKĐQ);
                                } else {
                                    array_push($newArrayCodeKĐQ, $value);
                                    // thêm dữ role media mới
                                    $roleMedia = new RoleMedia;
                                    $roleMedia->code = $value;
                                    $roleMedia->categori_id = 2;
                                    $roleMedia->type = 2;
                                    $roleMedia->save();
                                    array_push($newArrayCodeKĐQID, $roleMedia->id);
                                    $role_id = $roleMedia->id;
                                };
                            }

                            DB::table('contract_media_role')->insert([
                                'contract_id' => $data[0]->id,
                                'category_id' => 2,
                                'role_id' => $role_id,
                                'media_id' => $film->id,
                            ]);

                        }
                    }
                }
                return redirect()->route('media.films')->with('message_success', 'Thêm mới từ file ' . $file->getClientOriginalName() . ' thành công!');
            } else {
                return redirect()->route('media.films')->with('message_danger', 'Không đúng định dạng file Excel!');
            }
        } else {
            return redirect()->route('media.films')->with('message_danger', 'Chưa có file Excel!');
        }
    }

    public function upload3(Request $req)
    {
        ini_set('memory_limit', '-1');
        set_time_limit(0);

        $validator = Validator::make($req->all(), [
            'file' => 'required|mimes:xlsx,xls',
        ], [
            'file.required' => 'Chưa có file excel',
            'file.mimes' => 'Định dạng không hợp lệ',
        ]);
        if ($validator->passes()) {
            $file = $req->file('file');
            $adminID = Auth::user()->id;
            $isManager = Auth::user()->is_manager;
            $data = Excel::toArray(new EmployeeImport, $req->file);
            $data = $data[0];
            $dem = 0;
            if(!$data){
                return redirect()->route('media.films')->with('error', 'Thêm mới từ file ' . $file->getClientOriginalName() . ' thất bại. File không đúng dịnh dạng mẫu');               
            }
            for($i=count($data[0])-1; $i--; $i>0){
                if(!trim($data[0][$i])){
                    $dem++;
                }
                else break;
            }                    
            if (trim($data[0][0]) == 'STT'&&count($data[0])-$dem >= 31 && count($data[0])-$dem<=34) {
                // dd($data);
                Excel::import(new FilmUploadImport, $req->file);
                sleep(1);
                $error = "";
                if($isManager){
                    $myfilms = DB::table('films')->Leftjoin('contracts', 'films.contract_id', '=', 'contracts.id')->
                    where(function ($query) {
                        $query->where('contract_id', '=', null);
                        $query->orWhere('contracts.category_id', '=', 2);
                    })->select('vn_name', 'contract_number')->get();
                    $myfilms = json_decode(json_encode($myfilms), true);
                    $myfilmsFull = DB::table('films')->Leftjoin('contracts', 'films.contract_id', '=', 'contracts.id')->
                    where(function ($query) {
                        $query->where('contract_id', '=', null);
                        $query->orWhere('contracts.category_id', '=', 2);
                    })->select('vn_name', 'contract_number','films.id','contracts.id as old_contract_id')->get();
                    $myfilmsFull = json_decode(json_encode($myfilmsFull), true);
                }
                else {
                    $myfilms = DB::table('films')->where('films.admin_id',$adminID)->Leftjoin('contracts', 'films.contract_id', '=', 'contracts.id')->
                    where(function ($query) {
                        $query->where('contract_id', '=', null);
                        $query->orWhere('contracts.category_id', '=', 2);
                    })->select('vn_name', 'contract_number')->get();
                    $myfilms = json_decode(json_encode($myfilms), true);
                    $myfilmsFull = DB::table('films')->where('films.admin_id',$adminID)->Leftjoin('contracts', 'films.contract_id', '=', 'contracts.id')->
                        where(function ($query) {
                            $query->where('contract_id', '=', null);
                            $query->orWhere('contracts.category_id', '=', 2);
                        })->select('vn_name', 'contract_number','films.id','contracts.id as old_contract_id')->get();
                    $myfilmsFull = json_decode(json_encode($myfilmsFull), true);
                }
                $otherfilms = DB::table('films')->where('films.admin_id','!=',$adminID)->Leftjoin('contracts', 'films.contract_id', '=', 'contracts.id')->
                    where(function ($query) {
                        $query->where('contract_id', '=', null);
                        $query->orWhere('contracts.category_id', '=', 2);
                    })->select('vn_name', 'contract_number')->get();
                $otherfilms = json_decode(json_encode($otherfilms), true);
                $upload = DB::table('film_uploads')
                    ->where('status', '0')
                    ->where('admin_id', $adminID)
                    ->select('vn_name', 'contract_number')
                    ->get();
                $upload = json_decode(json_encode($upload), true);
                $filmUpload = DB::table('film_uploads')
                    ->where('status', '0')
                    ->where('admin_id', $adminID)
                    ->get();
                DB::table('film_uploads')
                    ->where('status', '0')
                    ->where('admin_id', $adminID)
                    ->delete();
                $ContractOFOthers = DB::table('contracts')->where('category_id',2)->where('type',1)->where('admin_id','!=',$adminID)->pluck('contract_number')->toArray();
                $ContractOFOthers = array_map('mb_strtolower', $ContractOFOthers);
                $ContractOFDifferentType = DB::table('contracts')->where(function ($query) {
                                                $query->where('type', '!=', 1);
                                                $query->orWhere('category_id', '=', 1);
                                            })->pluck('contract_number')->toArray();
                $ContractOFDifferentType = array_map('mb_strtolower', $ContractOFDifferentType);
                $contractHaveTime = DB::table('contracts')->where('category_id',2)->where('type',1)->where('start_time','!=',null)->where('end_time','!=',null)->orderBy('id')->pluck('contract_number')->toArray();
                $contractHaveTime2 = DB::table('contracts')->where('category_id',2)->where('type',1)->where('start_time','!=',null)->where('end_time','!=',null)->orderBy('id')->select('start_time','end_time')->get();
                $contractHaveTime = array_map('mb_strtolower', $contractHaveTime);

                $ContractsOfUser = DB::table('contracts')->pluck('contract_number');
                $ContractsOfUser = json_decode(json_encode($ContractsOfUser), true);
                $ContractsOfUser = array_map('mb_strtolower', $ContractsOfUser);

                $countries = DB::table('countries')->pluck('name','id')->toArray();
                $countries = json_decode(json_encode($countries), true);
                foreach($countries as $key => $value){
                    $countries[$key] = Str::title($countries[$key]);
                }
                               
                $checkActor = 1;
                $checkStartTime = 1;
                $checkEndTime = 1;
                $checkMoney = 1;
                $checkDTTT = 1;
                $checkValidateTime = 1;
                $checkContractNumber = 1;
                $errorStartTime = '';
                $errorEndTime = '';
                $errorValidateTime = '';
                $errorActor = '';
                $errorMoney = '';
                $errorDTTT = '';
                $errorContractNumber = '';
                $errorContractNumberType = '';
                $errorContractNumberTime = '';
                // yêu cầu mới
                $checkProductName = 1;
                $errorProductName = '';
                $checkCountry = 1;
                $errorCountry = '';

                $checkPartnerName = 1;
                $errorPartnerName = '';
                $checkContractNumberEmpty = 1;
                $errorContractNumberEmpty = '';
                $checkYearCreate = 1;
                $errorYearCreate = '';
                $checkBroadcastCountry = 1;
                $errorBroadcastCountry = '';
                $indexErrorBroadcastCountry = [];
                // yêu cầu mới 07/01/2022
                $checkExistContractOfUser = 1;
                $errorExistContractOfUser = '';
                foreach ($filmUpload as $key => $value) {
                    $indexErrorBroadcastCountry[$key] = 0;
                    if($value->vn_name){
                        if(!$value->start_time1){
                            if($checkStartTime){
                                $checkStartTime = 0;
                                $errorStartTime = $errorStartTime.' '.($key+1);
                            }
                            else $errorStartTime = $errorStartTime.', '.($key+1);
                        }
                        if(!$value->end_time1){
                            if($checkEndTime){
                                $checkEndTime = 0;
                                $errorEndTime = $errorEndTime.' '.($key+1);
                            }
                            else $errorEndTime = $errorEndTime.', '.($key+1);
                        }
                        if(!$value->count_tap){
                            if($checkActor){
                                $checkActor = 0;
                                $errorActor = $errorActor.' '.($key+1);
                            }
                            else $errorActor = $errorActor.', '.($key+1);
                        }
                        // if(!$value->product_name){
                        //     if($checkProductName){
                        //         $checkProductName = 0;
                        //         $errorProductName = $errorProductName.' '.($key+1);
                        //     }
                        //     else $errorProductName = $errorProductName.', '.($key+1);
                        // }
                        if(!$value->country){
                            if($checkCountry){
                                $checkCountry = 0;
                                $errorCountry = $errorCountry.' '.($key+1);
                            }
                            else $errorCountry = $errorCountry.', '.($key+1);
                        }                        
                        elseif(!in_array(Str::title($value->country),$countries)){
                            if($checkCountry){
                                $checkCountry = 0;
                                $errorCountry = $errorCountry.' '.($key+1);
                            }
                            else $errorCountry = $errorCountry.', '.($key+1);
                        } 
                        //////// start
                        $arrayQG = trim(Str::title($value->broadcast_country));
                        $arrayQG = Str::of($arrayQG)->trim(',');
                        // chuyển thành mảng
                        $arrayQG = explode(',', $arrayQG);
                        foreach ($arrayQG as $QG) {
                            if(!trim($QG)){
                                continue;
                            }
                            $QG = trim(Str::title($QG));
                            if(!in_array($QG,$countries)){
                                if($checkBroadcastCountry){
                                    $checkBroadcastCountry = 0;
                                    $errorBroadcastCountry = $errorBroadcastCountry.' '.($key+1);
                                }
                                else $errorBroadcastCountry = $errorBroadcastCountry.', '.($key+1);
                                $indexErrorBroadcastCountry[$key] = 1;
                                break;
                            }
            
                        }
                        /////// end
                        if(!$value->partner_name){
                            if($checkPartnerName){
                                $checkPartnerName = 0;
                                $errorPartnerName = $errorPartnerName.' '.($key+1);
                            }
                            else $errorPartnerName = $errorPartnerName.', '.($key+1);
                        }
                        if(!$value->contract_number){
                            if($checkContractNumberEmpty){
                                $checkContractNumberEmpty = 0;
                                $errorContractNumberEmpty = $errorContractNumberEmpty.' '.($key+1);
                            }
                            else $errorContractNumberEmpty = $errorContractNumberEmpty.', '.($key+1);
                        }
                        if(!$value->year_create){
                            if($checkYearCreate){
                                $checkYearCreate = 0;
                                $errorYearCreate = $errorYearCreate.' '.($key+1);
                            }
                            else $errorYearCreate = $errorYearCreate.', '.($key+1);
                        }
                        // dd($value);
                        if($value->start_time1&&$value->end_time1&&strtotime($value->start_time1)>=strtotime($value->end_time1)){
                            if($checkValidateTime){
                                $checkValidateTime = 0;
                                $errorValidateTime = $errorValidateTime.' '.($key+1);
                            }
                            else $errorValidateTime = $errorValidateTime.', '.($key+1);
                        }
                        // if(($value->fee1!=null&&!is_numeric($value->fee1))||($value->fee2!=null&&!is_numeric($value->fee2))||($value->fee3!=null&&!is_numeric($value->fee3))){
                        //     if($checkMoney){
                        //         $checkMoney = 0;
                        //         $errorMoney = $errorMoney.' '.($key+1);
                        //     }
                        //     else $errorMoney = $errorMoney.', '.($key+1);                           
                        // }
                        if(($value->youtube!=null&&!is_numeric($value->youtube))||($value->VOD_nuoc_ngoai!=null&&!is_numeric($value->VOD_nuoc_ngoai))||($value->VOD_noi_bo!=null&&!is_numeric($value->VOD_noi_bo))||($value->VOD_ngoai!=null&&!is_numeric($value->VOD_ngoai))||($value->TH_noi_bo!=null&&!is_numeric($value->TH_noi_bo))||($value->TH_ngoai!=null&&!is_numeric($value->TH_ngoai))){
                            if($checkDTTT){
                                $checkDTTT = 0;
                                $errorDTTT = $errorDTTT.' '.($key+1);
                            }
                            else $errorDTTT = $errorDTTT.', '.($key+1);                           
                        }
                    }
                }

                $index = array();
                $checkFirst = 1;
                foreach($filmUpload as $key => $value){
                    $index[$key] = 1;
                    if(!$value->vn_name||!$value->partner_name||!$value->contract_number||!$value->year_create||(!$value->country||($value->country&&!in_array(Str::title($value->country),$countries)))||!$value->count_tap||!$value->start_time1||!$value->end_time1||($value->youtube!=null&&!is_numeric($value->youtube))||($value->VOD_nuoc_ngoai!=null&&!is_numeric($value->VOD_nuoc_ngoai))||($value->VOD_noi_bo!=null&&!is_numeric($value->VOD_noi_bo))||($value->VOD_ngoai!=null&&!is_numeric($value->VOD_ngoai))||($value->TH_noi_bo!=null&&!is_numeric($value->TH_noi_bo))||($value->TH_ngoai!=null&&!is_numeric($value->TH_ngoai)||$indexErrorBroadcastCountry[$key])){
                        $index[$key] = 0;
                        if($value->vn_name){
                            if($checkFirst){
                                $checkFirst = 0;
                                $error = $error.'Lỗi dữ liệu bản ghi:';
                                $error = $error.' '.($key+1);
                            }
                            else $error = $error.', '.($key+1);
                        }
                        continue;
                    }
                    if(strtotime($value->start_time1)>=strtotime($value->end_time1)){
                        $index[$key] = 0;
                        if($checkFirst){
                            $checkFirst = 0;
                            $error = $error.'Lỗi dữ liệu bản ghi:';
                            $error = $error.' '.($key+1);
                        }
                        else $error = $error.', '.($key+1);
                        continue;                        
                    }
                }
                $checkFirst = 1;
                foreach($filmUpload as $key => $value){
                    $kt=1;
                    if($value->vn_name){
                        if($value->start_time1&&$value->end_time1&&strtotime($value->start_time1)<strtotime($value->end_time1)&&$value->contract_number&&in_array(Str::lower($value->contract_number),$contractHaveTime)){
                            $search = array_search(Str::lower($value->contract_number), $contractHaveTime);
                            if(strtotime($value->start_time1)<strtotime($contractHaveTime2[$search]->start_time)||strtotime($value->end_time1)>strtotime($contractHaveTime2[$search]->end_time)){
                                $kt = 0;
                                if($checkFirst){
                                    $checkFirst = 0;
                                    if($error) $error = $error.". Thời hạn bản quyền của phim không nằm trong thời hạn bản quyền của hợp đồng:";
                                    else $error = $error."Thời hạn bản quyền của phim không nằm trong thời hạn bản quyền của hợp đồng:";
                                    $errorContractNumberTime = $errorContractNumberTime.' '.($key+1);
                                    $error = $error.' '.($key+1);
                                }
                                else{
                                    $errorContractNumberTime = $errorContractNumberTime.', '.($key+1);
                                    $error = $error.', '.($key+1);
                                } 
                            }
                        }
                    }
                    $index[$key] = (!$index[$key])?0:$kt;
                }
                // yêu cầu mới 7/1/2022
                // $checkExistContractOfUser
                $checkFirst = 1;
                foreach($filmUpload as $key => $value){
                    $kt=1;
                    if($value->vn_name){
                        if($value->contract_number&&!in_array(Str::lower($value->contract_number), $ContractsOfUser)){
                            $kt = 0;
                            if($checkFirst){
                                $checkFirst = 0;
                                if($error) $error = $error.". Số hợp đồng không tồn tại:";
                                else $error = $error."Số hợp đồng không tồn tại:";
                                $errorExistContractOfUser = $errorExistContractOfUser.' '.($key+1);
                                $error = $error.' '.($key+1);
                            }
                            else{
                                $errorExistContractOfUser = $errorExistContractOfUser.', '.($key+1);
                                $error = $error.', '.($key+1);
                            } 
                        }
                    }
                    $index[$key] = (!$index[$key])?0:$kt;
                }
                if(!$isManager){
                    $checkFirst = 1;
                    foreach($filmUpload as $key => $value){
                        $kt=1;
                        if($value->vn_name){
                            if($value->contract_number&&in_array(Str::lower($value->contract_number), $ContractOFOthers)){
                                $kt = 0;
                                if($checkFirst){
                                    $checkFirst = 0;
                                    if($error) $error = $error.". Số hợp đồng đã tồn tại và thuộc quản lý của một người dùng khác:";
                                    else $error = $error."Số hợp đồng đã tồn tại và thuộc quản lý của một người dùng khác:";
                                    $errorContractNumber = $errorContractNumber.' '.($key+1);
                                    $error = $error.' '.($key+1);
                                }
                                else{
                                    $errorContractNumber = $errorContractNumber.', '.($key+1);
                                    $error = $error.', '.($key+1);
                                } 
                            }
                        }
                        $index[$key] = (!$index[$key])?0:$kt;
                    }
                }

                $checkFirst = 1;
                foreach($filmUpload as $key => $value){
                    $kt=1;
                    if($value->vn_name){
                        if($value->contract_number&&in_array(Str::lower($value->contract_number), $ContractOFDifferentType)){
                            $kt = 0;
                            if($checkFirst){
                                $checkFirst = 0;
                                if($error) $error = $error.". Số hợp đồng đã tồn tại và không phải hợp đồng mua phim:";
                                else $error = $error."Số hợp đồng đã tồn tại và không phải hợp đồng mua phim:";
                                $errorContractNumberType = $errorContractNumberType.' '.($key+1);
                                $error = $error.' '.($key+1);
                            }
                            else{
                                $errorContractNumberType = $errorContractNumberType.', '.($key+1);
                                $error = $error.', '.($key+1);
                            } 
                        }
                    }
                    $index[$key] = (!$index[$key])?0:$kt;
                }

                $count = count($upload);
                $duplicate = [];
                // check trùng bên trong bảng upload
                $checkFirst = 1;
                $errorUpload = '';
                for ($i = 0; $i < $count; $i++) {
                    if($upload[$i]['vn_name']){
                        $kt = 1;
                        $check = 1;
                        for ($j = 0; $j < $count; $j++) {
                            if($upload[$j]['vn_name']){
                                if (array_map('mb_strtolower', $upload[$i]) == array_map('mb_strtolower', $upload[$j]) && $i != $j) {
                                    $kt = 0;
                                    if(!in_array($i,$duplicate)&&$check){
                                        array_push($duplicate,$i);                                        
                                        $check = 0;
                                        if($checkFirst) {
                                            $errorUpload = $errorUpload.' ('.($i+1);
                                            if($error) $error = $error.". Trùng bản ghi trong file:";
                                            else $error = $error."Trùng bản ghi trong file:";
                                            $checkFirst = 0;
                                            $error = $error.' ('.($i+1);
                                        }
                                        else{
                                            $errorUpload = $errorUpload.'; ('.($i+1);
                                            $error = $error.'; ('.($i+1);
                                        } 
                                    }

                                    if(!in_array($j,$duplicate)){
                                        array_push($duplicate,$j);
                                        $errorUpload = $errorUpload.', '.($j+1);
                                        $error = $error.', '.($j+1);
                                    }
                                    $index[$j] = (!$index[$j])?0:$kt;
                                }
                            }
                        }
                        if($check==0) {
                            $errorUpload = $errorUpload.')';
                            $error = $error.')';
                        }
                        $index[$i] = (!$index[$i])?0:$kt;
                    }
                }
                // check trùng với bảng film
                $errorDB = '';
                $checkFirst = 1;
                if(!$isManager){
                    foreach ($upload as $key => $value) {
                        if ($value['vn_name']) {
                            $kt = 1;
                            foreach ($otherfilms as $keyFilm => $film) {
                                if (array_map('mb_strtolower', $value) == array_map('mb_strtolower', $film)) {
                                    if($checkFirst){
                                        if($error) $error = $error.". Trùng bản ghi trong DB:";
                                        else $error = $error."Trùng bản ghi trong DB:";
                                        $errorDB = $errorDB.' '.($key+1);
                                        $error = $error.' '.($key+1);
                                        $checkFirst = 0;
                                    }
                                    else {
                                        $errorDB = $errorDB.', '.($key+1);
                                        $error = $error.', '.($key+1);
                                    }
                                    $kt = 0;
                                    break;
                                }
                            }
                            $index[$key] = (!$index[$key])?0:$kt;
                        }
                    }
                }
                $oldFilms = [];
                $checkFirst = 1;
                foreach ($upload as $key => $value) {
                    if ($value['vn_name']&&$index[$key]) {
                        foreach ($myfilms as $keyFilm => $film) {
                            if (array_map('mb_strtolower', $value) == array_map('mb_strtolower', $film)) {
                                $oldFilms[] =['index'=>$key,'old_film_id'=>$myfilmsFull[$keyFilm]['id'],'old_contract_id'=>$myfilmsFull[$keyFilm]['old_contract_id']];
                            }
                        }
                    }
                }
                if($error){
                    $error1 = [];
                    array_push($error1, [
                        'errorUpload' => $errorUpload
                    ]);
                    array_push($error1, [
                        'errorDB' => $errorDB
                    ]);
                    array_push($error1, [
                        'errorStartTime' => $errorStartTime
                    ]);
                    array_push($error1, [
                        'errorEndTime' => $errorEndTime
                    ]);
                    array_push($error1, [
                        'errorValidateTime' => $errorValidateTime
                    ]);
                    array_push($error1, [
                        'errorCountry' => $errorCountry
                    ]);
                    array_push($error1, [
                        'errorPartnerName' => $errorPartnerName
                    ]);
                    array_push($error1, [
                        'errorContractNumberEmpty' => $errorContractNumberEmpty
                    ]);
                    array_push($error1, [
                        'errorExistContractOfUser' => $errorExistContractOfUser
                    ]);
                    array_push($error1, [
                        'errorYearCreate' => $errorYearCreate
                    ]);
                    array_push($error1, [
                        'errorActor' => $errorActor
                    ]);
                    array_push($error1, [
                        'errorMoney' => $errorMoney
                    ]);
                    array_push($error1, [
                        'errorDTTT' => $errorDTTT
                    ]);
                    array_push($error1, [
                        'errorContractNumber' => $errorContractNumber
                    ]);
                    array_push($error1, [
                        'errorContractNumberType' => $errorContractNumberType
                    ]);
                    array_push($error1, [
                        'errorContractNumberTime' => $errorContractNumberTime
                    ]);
                    array_push($error1, [
                        'errorBroadcastCountry' => $errorBroadcastCountry
                    ]);
                    return redirect()->route('media.films')->with('errorVersion2', 'Thêm mới từ file ' . $file->getClientOriginalName() . ' thất bại. '.$error)->with('outputError', json_encode($error1));
                }
                //add id error to index
                foreach($oldFilms as $key => $value){
                    $index[$value['index']] = 0;
                    $id = $value['old_film_id'];
                    $contract_id = $value['old_contract_id'];
                    $oldFilms[$key] = $filmUpload[$key];
                    $oldFilms[$key]->old_film_id = $id;
                    $oldFilms[$key]->id = null;
                    $oldFilms[$key]->old_contract_id = $contract_id;
                }
                foreach ($index as $key => $value) {
                    if (!$index[$key]) {
                        // dd('đã vào');
                        unset($filmUpload[$key]);
                    }
                }
                if(!count($filmUpload)&&!count($oldFilms)){
                    return redirect()->route('media.films')->with('error', 'Thêm mới từ file ' . $file->getClientOriginalName() . ' thất bại. Thông tin trên file Import rỗng');
                }
                // contracts
                // if($isManager) $contracts = DB::table('contracts')->where('type',1)->where('category_id',2)->select('contract_number')->get();
                // else $contracts = DB::table('contracts')->where('type',1)->where('category_id',2)->where('admin_id',$adminID)->select('contract_number')->get();
                // $contracts = json_decode(json_encode($contracts), true);
                // for ($i=0; $i < count($contracts); $i++) { 
                //     $contracts[$i]['contract_number'] = Str::lower($contracts[$i]['contract_number']);
                // }
                // partner
                $partners = Partner::select('name')->get();
                $partners = json_decode(json_encode($partners), true);
                for ($i=0; $i < count($partners); $i++) { 
                    $partners[$i]['name'] = Str::lower($partners[$i]['name']);
                }

                $newFilmUpload = [];
                $newContracts = [];
                $newContracts1 = [];
                $newPartners = [];
                $newPartners1 = [];
                // contract
                // foreach ($filmUpload as $key => $film) {
                //     $newFilm = [
                //         'contract_number' => Str::lower($film->contract_number),
                //     ];
                //     // nếu bản ghi tồn tại trong những hợp đồng mới
                //     if (in_array($newFilm, $newContracts1)) {
                //         continue;
                //     } else {
                //         // nếu bản ghi đã tồn tại trong những hợp đồng cũ
                //         if (in_array($newFilm, $contracts)) {
                //             continue;
                //         }
                //         // contract_number phải khác null
                //         else {
                //             array_push($newContracts1, $newFilm);
                //             array_push($newContracts, [
                //                 'contract_number' => $film->contract_number,
                //             ]);
                //         };
                //     }
                // }
                // partner
                foreach ($filmUpload as $key => $film) {
                    $newFilm = [
                        'name' => Str::lower($film->partner_name),
                    ];
                    // nếu bản ghi tồn tại trong những hợp đồng mới
                    if (in_array($newFilm, $newPartners1)) {
                        continue;
                    } else {
                        // nếu bản ghi đã tồn tại trong những hợp đồng cũ
                        if (in_array($newFilm, $partners)) {
                            continue;
                        }
                        // contract_number phải khác null
                        else {
                            array_push($newPartners1, $newFilm);
                            array_push($newPartners, [
                                'name' => $film->partner_name,
                            ]);
                        };
                    }
                }
                //////
                foreach ($oldFilms as $key => $film) {
                    $newFilm = [
                        'name' => Str::lower($film->partner_name),
                    ];
                    // nếu bản ghi tồn tại trong những hợp đồng mới
                    if (in_array($newFilm, $newPartners1)) {
                        continue;
                    } else {
                        // nếu bản ghi đã tồn tại trong những hợp đồng cũ
                        if (in_array($newFilm, $partners)) {
                            continue;
                        }
                        // contract_number phải khác null
                        else {
                            array_push($newPartners1, $newFilm);
                            array_push($newPartners, [
                                'name' => $film->partner_name,
                            ]);
                        };
                    }
                }
                // for ($i = 0; $i < count($newContracts); $i++) {
                //     $newContracts[$i]['admin_id'] = $adminID;
                //     $newContracts[$i]['type'] = 1;
                //     $newContracts[$i]['category_id'] = 2;
                // }
                // // dd($newContracts);
                // // thêm hợp đồng vào bảng contracts
                // foreach (array_chunk($newContracts, 1000) as $t) {
                //     DB::table('contracts')->insert($t);
                // }
                // thêm đối tác vào bảng đối tác
                foreach (array_chunk($newPartners, 1000) as $t) {
                    DB::table('partners')->insert($t);
                }
                ////// country
                    // $countries = DB::table('countries')->select('name')->get();
                    // $countries = json_decode(json_encode($countries), true);
                    // for ($i=0; $i < count($countries); $i++) { 
                    //     $countries[$i]['name'] = Str::title($countries[$i]['name']);
                    // }                
                    // $newCountries = [];
                    // foreach ($filmUpload as $key => $film) {
                    //     if(!trim($film->country)){
                    //         continue;
                    //     }
                    //     $country = [
                    //         'name' => Str::title(trim($film->country)),
                    //     ];
                    //     // nếu bản ghi tồn tại trong những country mới
                    //     if (in_array($country, $newCountries)) {
                    //         continue;
                    //     } else {
                    //         // nếu bản ghi đã tồn tại trong những country cũ
                    //         if (in_array($country, $countries)) {
                    //             continue;
                    //         }
                    //         // contract_number phải khác null
                    //         else {
                    //             array_push($newCountries, $country);
                    //         };
                    //     }
                    // }
                    // for ($i = 0; $i < count($newCountries); $i++) {
                    //     $newCountries[$i]['admin_id'] = $adminID;
                    // }
                    // // thêm country mới vào bảng country
                    // foreach (array_chunk($newCountries, 1000) as $t) {
                    //     DB::table('countries')->insert($t);
                    // }
                sleep(1);
                //film
                //Lấy danh sách hợp đồng
                if($isManager) $ContractsAfterInsert = DB::table('contracts')->where('type',1)->where('category_id',2)->pluck('contract_number', 'id');
                else $ContractsAfterInsert = DB::table('contracts')->where('type',1)->where('category_id',2)->where('admin_id',$adminID)->pluck('contract_number', 'id');
                $ContractsAfterInsert = json_decode(json_encode($ContractsAfterInsert), true);
                $ContractsAfterInsert = array_map('mb_strtolower', $ContractsAfterInsert);
                // lấy danh sách đối tác
                $partners = Partner::pluck('name','id');
                $partners = json_decode(json_encode($partners), true);
                $partners = array_map('mb_strtolower', $partners);


                $arrayFilm = [];
                foreach ($filmUpload as $key => $film2) {
                    $contract_id = array_search(Str::lower($film2->contract_number), $ContractsAfterInsert);

                    $partner_name = array_search(Str::lower($film2->partner_name), $partners);

                    $country_id = array_search(Str::title($film2->country), $countries);
                    if (!$contract_id||$film2->contract_number==null) {
                        $contract_id=null;
                    }
                    $film = [];
                    $film['country'] = $country_id;
                    $film['vn_name'] = $film2->vn_name;
                    $film['product_name'] = $film2->product_name;
                    $film['year_create'] = $film2->year_create;
                    $film['count_name'] = $film2->count_name;
                    $film['count_tap'] = $film2->count_tap;
                    $film['count_hour'] = $film2->count_hour;
                    $film['partner_name'] = $partner_name;
                    $film['addition_note'] = $film2->addition_note;
                    $film['nuoc'] = $film2->nuoc;
                    $film['count_live'] = $film2->count_live;
                    $film['license_fee'] = $film2->fee1;
                    $film['partner_fee'] = $film2->fee2;
                    $film['other_fee'] = $film2->fee3;
                    $film['total_fee'] = (float)$film['license_fee'] + (float)$film['partner_fee'] + (float)$film['other_fee'];
                    $film['start_time'] = $film2->start_time1;
                    $film['end_time'] = $film2->end_time1;
                    $film['note'] = $film2->note;
                    $film['actor_name'] = $film2->actor_name;
                    $film['director'] = $film2->director;
                    $film['contract_id'] = $contract_id;
                    $film['admin_id'] = $adminID;
                    $film['warning_status'] = 5; //status new;
                    array_push($arrayFilm, $film);
                }
                //////
                foreach ($oldFilms as $key => $film2) {
                    $partner_name = array_search(Str::lower($film2->partner_name), $partners);
                    $country_id = array_search(Str::title($film2->country), $countries);
                    $film = [];
                    $film['country'] = $country_id;
                    $film['vn_name'] = $film2->vn_name;
                    $film['product_name'] = $film2->product_name;
                    $film['year_create'] = $film2->year_create;
                    $film['count_name'] = $film2->count_name;
                    $film['count_tap'] = $film2->count_tap;
                    $film['count_hour'] = $film2->count_hour;
                    $film['partner_name'] = $partner_name;
                    $film['addition_note'] = $film2->addition_note;
                    $film['nuoc'] = $film2->nuoc;
                    $film['count_live'] = $film2->count_live;
                    $film['license_fee'] = $film2->fee1;
                    $film['partner_fee'] = $film2->fee2;
                    $film['other_fee'] = $film2->fee3;
                    $film['total_fee'] = (float)$film['license_fee'] + (float)$film['partner_fee'] + (float)$film['other_fee'];
                    $film['start_time'] = $film2->start_time1;
                    $film['end_time'] = $film2->end_time1;
                    $film['note'] = $film2->note;
                    $film['actor_name'] = $film2->actor_name;
                    $film['director'] = $film2->director;
                    DB::table('films')->where('id',$film2->old_film_id)->update($film);
                }
                // dd('vào');
                // dd($arrayFilm, $filmUpload);
                // DB::table('music')->insert($arrayMusic);
                foreach (array_chunk($arrayFilm, 1000) as $t) {
                    DB::table('films')->insertOrIgnore($t);
                }
                // role_media
                $listCodeOldĐQ = DB::table('role_media')->where('type', 1)->where('categori_id', 2)->select('code')->get();
                $listCodeOldĐQ = json_decode(json_encode($listCodeOldĐQ), true);
                for ($i=0; $i < count($listCodeOldĐQ); $i++) { 
                    $listCodeOldĐQ[$i]['code'] = Str::upper($listCodeOldĐQ[$i]['code']);
                }  
                $listCodeOldKĐQ = DB::table('role_media')->where('type', 2)->where('categori_id', 2)->select('code')->get();
                $listCodeOldKĐQ = json_decode(json_encode($listCodeOldKĐQ), true);
                for ($i=0; $i < count($listCodeOldKĐQ); $i++) { 
                    $listCodeOldKĐQ[$i]['code'] = Str::upper($listCodeOldKĐQ[$i]['code']);
                }                
                $listCategory = DB::table('category_films')->select('name')->get();
                $listCategory = json_decode(json_encode($listCategory), true);
                for ($i=0; $i < count($listCategory); $i++) { 
                    $listCategory[$i]['name'] = Str::upper($listCategory[$i]['name']);
                }   
                // $listQG =  Country::select('name')->get();
                // $listQG = json_decode(json_encode($listQG), true);
                // for ($i=0; $i < count($listQG); $i++) { 
                //     $listQG[$i]['name'] = Str::title($listQG[$i]['name']);
                // }      
                $newArrayCodeĐQ = [];
                $newArrayCodeKĐQ = [];
                $newArrayCodeĐQID = [];
                $newArrayCodeKĐQID = [];
                $newArrayTL = [];
                // $newArrayQG = [];
                foreach ($filmUpload->merge($oldFilms) as $key => $film) {
                    // lấy category
                    // loại bỏ khoảng trắng và ',' ở cuối
                    $arrayTL = trim(Str::upper($film->category));
                    $arrayTL = Str::of($arrayTL)->trim(',');
                    // chuyển thành mảng
                    $arrayTL = explode(',', $arrayTL);
                    foreach ($arrayTL as $TL) {
                        if(!trim($TL)){
                            continue;
                        }
                        $newTL = [
                            'name' => trim($TL),
                        ];
                        // nếu bản ghi tồn tại trong những role mới
                        if (in_array($newTL, $newArrayTL)) {
                            continue;
                        } else {
                            // nếu bản ghi đã tồn tại trong những role cũ
                            if (in_array($newTL, $listCategory)) {
                                continue;
                            } else {
                                array_push($newArrayTL, $newTL);
                            };
                        }
                    }

                    // xử lý độc quyền
                    // loại bỏ khoảng trắng và ',' ở cuối
                    $arrayĐQ = trim(Str::upper($film->DQ));
                    $arrayĐQ = Str::of($arrayĐQ)->trim(',');
                    // chuyển thành mảng
                    $arrayĐQ = explode(',', $arrayĐQ);
                    foreach ($arrayĐQ as $ĐQ) {
                        if(!trim($ĐQ)){
                            continue;
                        }
                        $newĐQ = [
                            'code' => trim($ĐQ),
                        ];
                        // nếu bản ghi tồn tại trong những role mới
                        if (in_array($newĐQ, $newArrayCodeĐQ)) {
                            continue;
                        } else {
                            // nếu bản ghi đã tồn tại trong những role cũ
                            if (in_array($newĐQ, $listCodeOldĐQ)) {
                                continue;
                            } else {
                                array_push($newArrayCodeĐQ, $newĐQ);
                            };
                        }
                    }
                    // xử lý không độc quyền
                    $arrayKĐQ = trim(Str::upper($film->KDQ));
                    $arrayKĐQ = Str::of($arrayKĐQ)->trim(',');
                    // chuyển thành mảng
                    $arrayKĐQ = explode(',', $arrayKĐQ);
                    foreach ($arrayKĐQ as $KĐQ) {
                        if(!trim($KĐQ)){
                            continue;
                        }
                        $newKĐQ = [
                            'code' => trim($KĐQ),
                        ];
                        // nếu bản ghi tồn tại trong những role mới
                        if (in_array($newKĐQ, $newArrayCodeKĐQ)) {
                            continue;
                        } else {
                            // nếu bản ghi đã tồn tại trong những role cũ
                            if (in_array($newKĐQ, $listCodeOldKĐQ)) {
                                continue;
                            } else {
                                array_push($newArrayCodeKĐQ, $newKĐQ);
                            };
                        }
                    }
                    // // xử lý quốc gia được phép phát sóng
                    // $arrayQG = trim(Str::title($film->broadcast_country));
                    // $arrayQG = Str::of($arrayQG)->trim(',');
                    // // chuyển thành mảng
                    // $arrayQG = explode(',', $arrayQG);
                    // foreach ($arrayQG as $QG) {
                    //     if(!trim($QG)){
                    //         continue;
                    //     }
                    //     $newQG = [
                    //         'name' => trim($QG),
                    //     ];
                    //     // nếu bản ghi tồn tại trong những role mới
                    //     if (in_array($newQG, $newArrayQG)) {
                    //         continue;
                    //     } else {
                    //         // nếu bản ghi đã tồn tại trong những role cũ
                    //         if (in_array($newQG, $listQG)) {
                    //             continue;
                    //         } else {
                    //             array_push($newArrayQG, $newQG);
                    //         };
                    //     }
                    // }
                }
                // for ($i = 0; $i < count($newArrayQG); $i++) {
                //     $newArrayQG[$i]['admin_id'] = $adminID;
                // }
                // foreach (array_chunk($newArrayQG, 1000) as $t) {
                //     DB::table('countries')->insertOrIgnore($t);
                // }
                for ($i = 0; $i < count($newArrayCodeĐQ); $i++) {
                    $newArrayCodeĐQ[$i]['categori_id'] = 2;
                    $newArrayCodeĐQ[$i]['type'] = 1;
                    $newArrayCodeĐQ[$i]['admin_id'] = $adminID;
                    $newArrayCodeĐQ[$i]['level'] = 0;
                }
                // dd($newArrayCodeKĐQ);
                foreach (array_chunk($newArrayCodeĐQ, 1000) as $t) {
                    DB::table('role_media')->insertOrIgnore($t);
                }

                for ($i = 0; $i < count($newArrayCodeKĐQ); $i++) {
                    $newArrayCodeKĐQ[$i]['categori_id'] = 2;
                    $newArrayCodeKĐQ[$i]['type'] = 2;
                    $newArrayCodeKĐQ[$i]['admin_id'] = $adminID;
                    $newArrayCodeKĐQ[$i]['level'] = 0;
                }
                foreach (array_chunk($newArrayCodeKĐQ, 1000) as $t) {
                    DB::table('role_media')->insertOrIgnore($t);
                }
                // category_film
                foreach (array_chunk($newArrayTL, 1000) as $t) {
                    DB::table('category_films')->insertOrIgnore($t);
                }
                // xử lý hạ tầng phát sóng, phải chờ lưu xong độc quyền và không độc quyền của phim
                $listHT = DB::table('role_media')->where('categori_id', 2)->where('level',1)->select('code')->get();
                $listHT = json_decode(json_encode($listHT), true);
                for ($i=0; $i < count($listHT); $i++) { 
                    $listHT[$i]['code'] = Str::upper($listHT[$i]['code']);
                }   
                $newArrayHT = [];
                foreach ($filmUpload->merge($oldFilms) as $key => $film) {
                    // xử lý hạ tầng phát sóng
                    $arrayHT = trim(Str::upper($film->Broadcast_infrastructure));
                    $arrayHT = Str::of($arrayHT)->trim(',');
                    // chuyển thành mảng
                    $arrayHT = explode(',', $arrayHT);
                    foreach ($arrayHT as $HT) {
                        if(!trim($HT)){
                            continue;
                        }
                        $newHT = [
                            'code' => trim($HT),
                        ];
                        // nếu bản ghi tồn tại trong những role mới
                        if (in_array($newHT, $newArrayHT)) {
                            continue;
                        } else {
                            // nếu bản ghi đã tồn tại trong những role cũ
                            if (in_array($newHT, $listHT)) {
                                continue;
                            } else {
                                array_push($newArrayHT, $newHT);
                            };
                        }
                    }
                }
                for ($i = 0; $i < count($newArrayHT); $i++) {
                    $newArrayHT[$i]['categori_id'] = 2;
                    $newArrayHT[$i]['level'] = 1;
                    $newArrayHT[$i]['admin_id'] = $adminID;
                }
                foreach (array_chunk($newArrayHT, 1000) as $t) {
                    DB::table('role_media')->insertOrIgnore($t);
                }
                ////// delete bảng trung gian
                // get array old_film_ids
                $old_film_ids = [];
                foreach($oldFilms as $key=>$value){
                    $old_film_ids[] = $value->old_film_id;
                }
                // xóa bảng trung gian theo old_film_ids
                DB::table('film_revenue_type')->whereIn('media_id',$old_film_ids)->delete();
                DB::table('contract_media_role')->whereIn('media_id',$old_film_ids)->delete();
                DB::table('film_country')->whereIn('media_id',$old_film_ids)->delete();
                DB::table('film_list_categories')->whereIn('id_film_list',$old_film_ids)->delete();
                //////
                sleep(1);
                // contract_role_media
                //////
                $filmAfterInsert = DB::table('films')->where('admin_id', $adminID)->where('warning_status', '5')->pluck('vn_name', 'id');
                $filmAfterInsert = json_decode(json_encode($filmAfterInsert), true);

                $filmIDAfterInsert = DB::table('films')->where('admin_id', $adminID)->where('warning_status', '5')->pluck('contract_id', 'id');
                DB::table('films')
                    ->where('admin_id', $adminID)
                    ->where('warning_status', 5)
                    ->update(['warning_status' => 4]);
                $filmIDAfterInsert = json_decode(json_encode($filmIDAfterInsert), true);

                $roleMediaĐQAfterInsert = DB::table('role_media')->where('categori_id', 2)->where('type', 1)->pluck('code', 'id');
                $roleMediaĐQAfterInsert = json_decode(json_encode($roleMediaĐQAfterInsert), true);
                foreach($roleMediaĐQAfterInsert as $key => $value){
                    $roleMediaĐQAfterInsert[$key] = Str::upper($roleMediaĐQAfterInsert[$key]);    
                } 
                $roleMediaKĐQAfterInsert = DB::table('role_media')->where('categori_id', 2)->where('type', 2)->pluck('code', 'id');
                $roleMediaKĐQAfterInsert = json_decode(json_encode($roleMediaKĐQAfterInsert), true);
                foreach($roleMediaKĐQAfterInsert as $key => $value){
                    $roleMediaKĐQAfterInsert[$key] = Str::upper($roleMediaKĐQAfterInsert[$key]);    
                }
                $htAfterInsert = DB::table('role_media')->where('categori_id', 2)->where('level', 1)->pluck('code', 'id');
                $htAfterInsert = json_decode(json_encode($htAfterInsert), true);
                foreach($htAfterInsert as $key => $value){
                    $htAfterInsert[$key] = Str::upper($htAfterInsert[$key]);    
                }
                // $countries = DB::table('countries')->pluck('name', 'id');
                // $countries = json_decode(json_encode($countries), true);   
                // foreach($countries as $key => $value){
                //     $countries[$key] = Str::title($countries[$key]);    
                // }                  

                $categoryAfterInsert = DB::table('category_films')->pluck('name', 'id');
                $categoryAfterInsert = json_decode(json_encode($categoryAfterInsert), true);
                foreach($categoryAfterInsert as $key => $value){
                    $categoryAfterInsert[$key] = Str::upper($categoryAfterInsert[$key]);    
                }        
                $contractMediaRole = [];
                $categoryFilm = [];
                $countryFilm = [];
                $DT = [];
                foreach ($filmUpload->merge($oldFilms) as $key => $film) {
                    if($film->id){
                        $filmID = array_search($film->vn_name, $filmAfterInsert);
                        $contractID = $filmIDAfterInsert[$filmID];
                    }
                    else {
                        $filmID = $film->old_film_id;
                        $contractID = $film->old_contract_id;
                    }
                    if ($filmID) {
                        // xử lý category
                        $arrayCategoryFilm = trim(Str::upper($film->category));
                        $arrayCategoryFilm = Str::of($arrayCategoryFilm)->trim(',');
                        // chuyển thành mảng
                        $arrayCategoryFilm = explode(',', $arrayCategoryFilm);
                        foreach ($arrayCategoryFilm as $value) {
                            $categoryID = array_search(trim($value), $categoryAfterInsert);
                            array_push($categoryFilm, [
                                'id_film_list' => $filmID,
                                'id_category_film' => $categoryID,
                            ]);
                        }
                        // xử lý độc quyền
                        // loại bỏ khoảng trắng và ',' ở cuối
                        $arrayĐQ = trim(Str::upper($film->DQ));
                        $arrayĐQ = Str::of($arrayĐQ)->trim(',');
                        // chuyển thành mảng
                        $arrayĐQ = explode(',', $arrayĐQ);
                        foreach ($arrayĐQ as $ĐQ) {
                            if(!trim($ĐQ)){
                                continue;
                            }
                            $roleMediaID = array_search(trim($ĐQ), $roleMediaĐQAfterInsert);
                            array_push($contractMediaRole, [
                                'role_id' => $roleMediaID,
                                'media_id' => $filmID,
                                'category_id' => 2,
                                'admin_id' => $adminID,
                                'contract_id' => $contractID,
                            ]);
                        }
                        // xử lý không độc quyền
                        // loại bỏ khoảng trắng và ',' ở cuối
                        $arrayKĐQ = trim(Str::upper($film->KDQ));
                        $arrayKĐQ = Str::of($arrayKĐQ)->trim(',');
                        // chuyển thành mảng
                        $arrayKĐQ = explode(',', $arrayKĐQ);
                        foreach ($arrayKĐQ as $KĐQ) {
                            if(!trim($KĐQ)){
                                continue;
                            }
                            $roleMediaID = array_search(trim($KĐQ), $roleMediaKĐQAfterInsert);
                            // dd(trim($KĐQ), $roleMediaKĐQAfterInsert);
                            array_push($contractMediaRole, [
                                'role_id' => $roleMediaID,
                                'media_id' => $filmID,
                                'category_id' => 2,
                                'admin_id' => $adminID,
                                'contract_id' => $contractID,
                            ]);
                        }
                        // xử lý hạ tầng phát sóng
                        // loại bỏ khoảng trắng và ',' ở cuối
                        $arrayHT = trim(Str::upper($film->Broadcast_infrastructure));
                        $arrayHT = Str::of($arrayHT)->trim(',');
                        // chuyển thành mảng
                        $arrayHT = explode(',', $arrayHT);
                        foreach ($arrayHT as $HT) {
                            if(!trim($HT)){
                                continue;
                            }
                            $roleMediaID = array_search(trim($HT), $htAfterInsert);
                            // dd(trim($KĐQ), $roleMediaKĐQAfterInsert);
                            array_push($contractMediaRole, [
                                'role_id' => $roleMediaID,
                                'media_id' => $filmID,
                                'category_id' => 2,
                                'admin_id' => $adminID,
                                'contract_id' => $contractID,
                            ]);
                        }
                        // xử lý quốc gia
                        // loại bỏ khoảng trắng và ',' ở cuối
                        $arrayQG = trim(Str::title($film->broadcast_country));
                        $arrayQG = Str::of($arrayQG)->trim(',');
                        // chuyển thành mảng
                        $arrayQG = explode(',', $arrayQG);
                        foreach ($arrayQG as $QG) {
                            if(!trim($QG)){
                                continue;
                            }
                            $conutryID = array_search(trim($QG), $countries);
                            // dd($countries, $QG);
                            array_push($countryFilm, [
                                'country_id' => $conutryID,
                                'media_id' => $filmID,
                                'type' => 2,
                            ]);
                        }

                        // xử lý lưu doanh thu tối thiểu
                        $money = (int) $film->youtube;
                        $money = $money ? $money : 0;
                        array_push($DT,[
                            'media_id' => $filmID,
                            'revenue_type_id' => 3,
                            'money' =>$money,
                        ]);

                        $money = (int) $film->VOD_nuoc_ngoai;
                        $money = $money ? $money : 0;
                        array_push($DT,[
                            'media_id' => $filmID,
                            'revenue_type_id' => 4,
                            'money' =>$money,
                        ]);

                        $money = (int) $film->VOD_noi_bo;
                        $money = $money ? $money : 0;
                        array_push($DT,[
                            'media_id' => $filmID,
                            'revenue_type_id' => 5,
                            'money' =>$money,
                        ]);

                        $money = (int) $film->VOD_ngoai;
                        $money = $money ? $money : 0;
                        array_push($DT,[
                            'media_id' => $filmID,
                            'revenue_type_id' => 6,
                            'money' =>$money,
                        ]);

                        $money = (int) $film->TH_noi_bo;
                        $money = $money ? $money : 0;
                        array_push($DT,[
                            'media_id' => $filmID,
                            'revenue_type_id' => 7,
                            'money' =>$money,
                        ]);

                        $money = (int) $film->TH_ngoai;
                        array_push($DT,[
                            'media_id' => $filmID,
                            'revenue_type_id' => 8,
                            'money' =>$money,
                        ]);
                    }
                }
                // dd($contractMediaRole);
                foreach (array_chunk($DT, 1000) as $t) {
                    DB::table('film_revenue_type')->insertOrIgnore($t);
                }
                foreach (array_chunk($contractMediaRole, 1000) as $t) {
                    DB::table('contract_media_role')->insertOrIgnore($t);
                }
                foreach (array_chunk($countryFilm, 1000) as $t) {
                    DB::table('film_country')->insertOrIgnore($t);
                }   
                // film_list_categories
                foreach (array_chunk($categoryFilm, 1000) as $t) {
                    DB::table('film_list_categories')->insertOrIgnore($t);
                }
                $fileName = time().'_'.$file->getClientOriginalName();
                $link = $file->storeAs(config("common.save.path"), $fileName, config("common.save.diskType"));

                ActionLog::writeActionLog(null, 'films', config("common.actions.created", "created"), null, "Tải lên", null,null, $link);
                return redirect()->route('media.films')->with('success', 'Thêm mới từ file ' . $file->getClientOriginalName() . ' thành công ' . count($arrayFilm)." bộ phim. Chỉnh sửa ".count($oldFilms).' bộ phim');
            }
            else {
                return redirect()->route('media.films')->with('error', 'Thêm mới từ file ' . $file->getClientOriginalName() . ' thất bại. File không đúng dịnh dạng mẫu');
            }

        }

        return redirect()->route('media.films')->with('error', $validator->errors()->first());

        // if ($req->hasFile('file'))
    }

    public function export()
    {
        $today = date("d_m_Y");
        return Excel::download(new FilmExport, 'films-' . $today . '.xlsx');
    }
    public function exportExampleFile()
    {
        return Storage::download('store/files/film.xlsx');
        // return Excel::download(new FilmExampleExport, 'filmsExample.xlsx');
    }
    public function exportExampleFileFilmList()
    {
        return Excel::download(new FilmListExampleExport, 'filmListExample.xlsx');
    }
    public function delete($id)
    {
        DB::table('films')->where('id', $id)->delete();
        return redirect()->back()->with('message_success', 'Xóa thành công');
    }

    public function deleteAll(Request $req)
    {
        DB::table('films')->whereIn('id', $req->filmIds)->delete();
        return redirect()->back()->with('message_success', 'Xóa thành công');
    }
    public function getOption($type)
    {
        $data = '';
        if($type==3) $data = RoleMedia::all()->where('level', 1)->toArray();
        else $data = RoleMedia::all()->where('type', $type)->where('level','!=',1)->toArray();
        $recursive = new Recursive($data);
        $htmlOption = $recursive->recursiveMedia();
        return $htmlOption;
    }
    public function edit($id)
    {
        $revenueParentType = RevenueType::parent();
        $revenueParentTypeJson = json_encode(RevenueType::parent()->pluck('id')->toArray());
        $cate_film = CategoryFilm::pluck('name', 'id');
        $countries = Country::pluck('name', 'id');
        $data = Film::find($id);
        $cate_filmId = $data->categories()->pluck('id_category_film');
        $countryId = $data->filmCountries()->where('sale_money_id',null)->pluck('country_id');
        $contract_num = Contract::find($data->contract_id);
        $data_type1 = $this->getOption(1);
        $data_type2 = $this->getOption(2);
        $data_type3 = $this->getOption(3);
        $revenueType = RevenueType::where('parent_id', '!=', 0)->get();
        // dd($data_type2, $data_type1, $data->roles->where('type',2));

        $partnerName = Partner::pluck('name','id')->toArray();
        $contractName = Contract::where('category_id',2)->where('type',1)->pluck('contract_number','id')->toArray();
        return view('admin.media.film.edit', compact('countries', 'countryId', 'data', 'data_type2', 'data_type1','data_type3', 'cate_film', 'contract_num', 'cate_filmId', 'revenueType', 'revenueParentType', 'revenueParentTypeJson', 'partnerName', 'contractName'));
    }
    public function show($id)
    {
        $revenueParentType = RevenueType::parent();
        $revenueParentTypeJson = json_encode(RevenueType::parent()->pluck('id')->toArray());
        $cate_film = CategoryFilm::pluck('name', 'id');
        $countries = Country::pluck('name', 'id');
        $data = Film::find($id);
        $cate_filmId = $data->categories()->pluck('id_category_film');
        $countryId = $data->filmCountries()->where('sale_money_id',null)->pluck('country_id');
        $contract_num = Contract::find($data->contract_id);
        $data_type1 = $this->getOption(1);
        $data_type2 = $this->getOption(2);
        $data_type3 = $this->getOption(3);
        $revenueType = RevenueType::where('parent_id', '!=', 0)->get();
        // dd($data_type2, $data_type1, $data->roles->where('type',2));

        $partnerName = Partner::pluck('name','id')->toArray();
        $contractName = Contract::where('category_id',2)->where('type',1)->pluck('contract_number','id')->toArray();
        return view('admin.media.film.show', compact('countries', 'countryId', 'data', 'data_type2', 'data_type1','data_type3', 'cate_film', 'contract_num', 'cate_filmId', 'revenueType', 'revenueParentType', 'revenueParentTypeJson', 'partnerName', 'contractName'));
    }
    public function filmConntry($film_country)
    {
        $country = Country::firstOrCreate(['name' => $film_country]);
        $country_id = $country->id;
        return $country_id;
    }
    public function categoryFilm($categories_film)
    {
        foreach ($categories_film as $value) {
            $cateIntance = CategoryFilm::firstOrCreate(['name' => $value]);
            $cate_film[] = $cateIntance->id;
        }
        return $cate_film;
    }
    public function store(Request $request)
    {
        $count_tap = str_replace(',','',$request->count_tap);
        $license_fee = str_replace(',','',$request->license_fee);
        $partner_fee = str_replace(',','',$request->partner_fee);
        $other_fee = str_replace(',','',$request->other_fee);
        // $check=true;
        // $cont = Contract::where('contract_number', $request->contract_number)->first();
        // if($cont&&$cont->type!=1) $check=false;
        $id = $request->contract_id;
        if($request->contract_id) {
            $contract = Contract::find($request->contract_id);
            $request->contract_number = $contract->contract_number;
        }

        $this->validate($request, [
            'vn_name' => 'required|max:1000|checkUniqueName:films,vn_name,'.$request->contract_number.','.$request->vn_name,
            // 'product_name' => 'required|max:200',
            'start_time' => ($id)?('required|time_film_contract2:'.$id):'required',
            'end_time' => ($id)?('required|after:start_time|time_film_contract2:'.$id):'required|after:start_time',
            'actor_name' => 'max:200',
            'country' => 'required|max:50',
            'year_create' => 'digits:4',
            'count_name' => 'max:1000',
            'count_tap' => 'required|max:24',
            'count_hour' => 'max:1000',
            'partner_name' => 'required|max:1000',
            'count_live' => 'max:5',
            'director' => 'max:200',
            'permissin_contract' => 'max:1000',
            'partner_fee' => 'max:24',
            'license_fee' => 'max:24',
            'other_fee' => 'max:24',
            // 'total_fee' => 'max:20',
            // 'actor_name' => 'required',
            'contract_id' => 'required',
        ], [
            'vn_name.checkUniqueName' => 'Tên phim đã tồn tại',
            'vn_name.required' => 'Tên phim bắt buộc',
            'count_tap.required' => 'Số tập phim bắt buộc',
            'count_tap.max' => 'Số tập phim không dài quá 24 ký tự',
        ], [
            'vn_name' => 'Tên phim',
            'start_time' => 'Ngày bắt đầu ',
            'end_time' => 'Ngày kết thúc',
            'actor_name' => 'Diễn viên',
            'product_name' => 'Tên phim tiếng Anh',
            'country' => 'Quốc gia sản xuất',
            'year_create' => 'Năm sản xuất',
            'count_name' => 'Số đầu phim',
            'count_tap' => 'Số tập phim ',
            'count_hour' => 'Thời lượng/tập',
            'partner_name' => 'Đối tác cung cấp',
            'count_live' => 'Số lần phát sóng',
            'director' => 'Đạo diễn',
            'permissin_contract' => 'Quyền hạn sử dụng',
            'partner_fee' => 'Chi phí đối tác',
            'license_fee' => 'Chi phí bản quyền',
            'other_fee' => 'Chi phí khác ',
            'total_fee' => 'Tổng chi phí',
            'contract_id' => 'Số hợp đồng',
        ]);
        $country_id = null;
        if (!empty($request->country)) {
            $country = Country::firstOrCreate(['name' => Str::title($request->country)]);
            $country_id = $country->id;
        }
        $film = Film::create([
            "vn_name" => $request->vn_name,
            "product_name" => $request->product_name,
            "country" => $country_id,
            "year_create" => $request->year_create,
            "count_name" => $request->count_name,
            "count_tap" => $count_tap,
            "count_live" => $request->count_live,
            "count_hour" => $request->count_hour,
            "partner_name" => $request->partner_name,
            "end_time" => $request->end_time,
            "partner_fee" => $partner_fee,
            "license_fee" => $license_fee,
            "other_fee" => $other_fee,
            "total_fee" => $request->total_fee,
            "start_time" => $request->start_time,
            "actor_name" => $request->actor_name,
            "director" => $request->director,
            "permissin_contract" => $request->permissin_contract,
            "contract_id" => $request->contract_id,
            "note" => $request->note,
            'addition_note' => $request->addition_note,
            'nuoc' => $request->nuoc,
        ]);

        $revenueType = RevenueType::where('parent_id', '!=', 0)->get();
        foreach($revenueType as $type){
            $typeName = 'id'.$type->id;
            $film->revenueType()->attach($type->id, [
                'money' => ($request->$typeName)?(int)$request->$typeName:0,
            ]);
        }

        if (isset($request->role1)) {
            foreach ($request->role1 as $value) {
                $roleIntance1 = ContractMediaRole::firstOrCreate([
                    'contract_id' => $id,
                    'role_id' => $value,
                    'media_id' => $film->id,
                    'end_time' => reFormatDate($request->end_time,'Y-m-d'),
                    'start_time' => reFormatDate($request->start_time,'Y-m-d'),
                ]);
                $role_media[] = $roleIntance1->id;
            }
        }
        if (isset($request->role2)) {
            foreach ($request->role2 as $value) {
                $roleIntance2 = ContractMediaRole::firstOrCreate([
                    'contract_id' => $id,
                    'role_id' => $value,
                    'media_id' => $film->id,
                    'end_time' => reFormatDate($request->end_time,'Y-m-d'),
                    'start_time' => reFormatDate($request->start_time,'Y-m-d'),
                ]);
                $role_media[] = $roleIntance2->id;
            }
        }
        if (isset($request->role3)) {
            foreach ($request->role3 as $value) {
                $roleIntance3 = ContractMediaRole::firstOrCreate([
                    'contract_id' => $id,
                    'role_id' => $value,
                    'media_id' => $film->id,
                    'end_time' => reFormatDate($request->end_time,'Y-m-d'),
                    'start_time' => reFormatDate($request->start_time,'Y-m-d'),
                ]);
                $role_media[] = $roleIntance3->id;
            }
        }
        // dd($role_media);
        if (isset($role_media)) {
            $film->roles()->attach($role_media);
        }
        if (isset($request->cate_film)) {
            foreach ($request->cate_film as $value) {
                if ($value) {
                    $cateIntance = CategoryFilm::firstOrCreate(['name' => Str::upper($value)]);
                    $cate_film[] = $cateIntance->id;
                }

            }
            $film->categories()->attach($cate_film);
        }
        if (isset($request->country_films)) {
            foreach ($request->country_films as $value) {
                $countryIntance = Country::firstOrCreate(['name' => Str::title($value)]);
                $country_films[] = $countryIntance->id;
            }
            $film->filmCountries()->attach($country_films, ['type' => 2, 'contract_id' => $id]);
        }
        $log = ActionLog::writeActionLog($film->id, $film->getTable(), config("common.actions.created", "created"), get_class($this), "Tạo mới", null, null, null);
        return redirect()->route('media.films')
            ->with('success', 'Tạo phim mới thành công');
    }
    // float function handleInputNumber($number){
    //     $number = trim($number);
    //     $number = str_replace(",","",$number);
    //     return $number;
    // }
    public function update(Request $request, $id)
    {
        $count_tap = str_replace(',','',$request->count_tap);
        $license_fee = str_replace(',','',$request->license_fee);
        $partner_fee = str_replace(',','',$request->partner_fee);
        $other_fee = str_replace(',','',$request->other_fee);
        $film = Film::find($id);
        //cap nhat doanh thu toi thieu
        $revenueType = RevenueType::where('parent_id', '!=', 0)->get();
        $film->revenueType()->detach();
        foreach($revenueType as $type){
            $typeName = 'id'.$type->id;
            $film->revenueType()->attach($type->id, [
                'money' => ($request->$typeName)?(int)$request->$typeName:0,
            ]);
        }

        $check = '';
        $contract = DB::table('contracts')->where('contract_number', $request->contract_number)->where('type',1)->get();
        $contract_id = count($contract)?$contract[0]->id:'';
        $request->vn_name = trim($request->vn_name);
        if (!empty($request->contract_number)) {
            $check = Rule::unique('films')->where(
                function ($query) use ($film, $contract_id) {
                    $query->where('contract_id', $contract_id)->where('id', '<>', $film->id);
                });
        }
        else {
            $check = Rule::unique('films')->where(
                function ($query) use ($film, $contract_id) {
                    $query->where('contract_id', null)->where('id', '<>', $film->id);
                }); 
        }
        $this->validate($request, [
            'country' => 'required|max:50',
            // 'product_name' => 'required|max:200',
            'year_create' => 'digits:4',
            'count_name' => 'max:1000',
            'count_tap' => 'required|max:24',
            'count_hour' => 'max:1000',
            'partner_name' => 'required|max:1000',
            'count_live' => 'max:5',
            'director' => 'max:200',
            'permissin_contract' => 'max:1000',
            'partner_fee' => 'max:24',
            'license_fee' => 'max:24',
            'other_fee' => 'max:24',
            'total_fee' => 'max:24',
            'vn_name' => ['required', $check,
            ],
            'start_time' => 'required|time_film_contract:'.$id,
            'end_time' => 'required|after:start_time|time_film_contract:'.$id,
            'actor_name' => 'max:200',
        ], [
            'count_tap.required' => 'Số tập phim bắt buộc',
            'count_tap.max' => 'Số tập phim không dài quá 24 ký tự',
        ], [
            'vn_name' => 'Tên phim',
            'start_time' => 'Ngày bắt đầu ',
            'end_time' => 'Ngày kết thúc',
            'actor_name' => 'Diễn viên',
            'product_name' => 'Tên phim tiếng Anh',
            'country' => 'Quốc gia sản xuất',
            'year_create' => 'Năm sản xuất',
            'count_name' => 'Số đầu phim',
            'count_tap' => 'Số tập phim ',
            'count_hour' => 'Thời lượng/tập',
            'partner_name' => 'Đối tác cung cấp',
            'count_live' => 'Số lần phát sóng',
            'director' => 'Đạo diễn',
            'permissin_contract' => 'Quyền hạn sử dụng',
            'partner_fee' => 'Chi phí đối tác',
            'license_fee' => 'Chi phí bản quyền',
            'other_fee' => 'Chi phí khác ',
            'total_fee' => 'Tổng chi phí',
        ]);
        $country_id = null;
        if (!empty($request->country)) {
            $country = Country::firstOrCreate(['name' => Str::title($request->country)]);
            $country_id = $country->id;
        }
        //
        $input = $request->all();
        $film = Film::find($id);
        $film->fill($input);
        $changes = $film->getDirty();
        $old_value = array();
        if(reFormatDate($film->getOriginal('start_time'))==$changes['start_time']) unset($changes['start_time']);
        if(reFormatDate($film->getOriginal('end_time'))==$changes['end_time']) unset($changes['end_time']);


        if(isset($changes['total_fee'])){
            unset($changes['total_fee']);
        }
        foreach ($changes as $key => $value) {
            $old_value[$key] = $film->getOriginal($key);
        }
        if(isset($old_value['end_time'])&&$old_value['end_time']){
            $old_value['end_time'] = reFormatDate($old_value['end_time']);
        }
        if(isset($old_value['start_time'])&&$old_value['start_time']){
            $old_value['start_time'] = reFormatDate($old_value['start_time']);
        }
        if(isset($changes['country'])) {
            if ($old_value['country']) {
                if(Country::find($old_value['country']) && Country::find($old_value['country'])->name==$changes['country']) {
                    unset($changes['country']);
                    unset($old_value['country']);
                }
                else  $old_value['country'] = Country::find($old_value['country'])->name??'';
            }
            else $old_value['country'] ='';
        }
        if(isset($changes['partner_name'])){
            $changes['partner_name'] = Partner::find($changes['partner_name'])->name;
            if(isset($old_value['partner_name']))
                $old_value['partner_name'] = Partner::find($old_value['partner_name'])->name??'';
        }
        //dd($old_value);
        //
        // dd($request->all());
        $film->update([
            "vn_name" => $request->vn_name,
            "product_name" => $request->product_name,
            "country" => $country_id,
            "year_create" => $request->year_create,
            "count_live" => $request->count_live,
            "count_name" => $request->count_name,
            "count_tap" => $count_tap,
            "count_hour" => $request->count_hour,
            "partner_fee" => $partner_fee,
            "license_fee" => $license_fee,
            "other_fee" => $other_fee,
            "total_fee" => $request->total_fee,
            "partner_name" => $request->partner_name,
            "end_time" => $request->end_time,
            "start_time" => $request->start_time,
            "actor_name" => $request->actor_name,
            "director" => $request->director,
            "permissin_contract" => $request->permissin_contract,
            "note" => $request->note,
            "addition_note" => $request->addition_note,
        ]);

        if (isset($request->role1)) {
            foreach ($request->role1 as $value) {
                $roleIntance1 = RoleMedia::firstOrCreate(['id' => $value]);
                $role_media[] = $roleIntance1->id;
            }
        }
        if (isset($request->role2)) {
            foreach ($request->role2 as $value) {
                $roleIntance2 = RoleMedia::firstOrCreate(['id' => $value]);
                $role_media[] = $roleIntance2->id;
            }

        }
        if (isset($request->role3)) {
            foreach ($request->role3 as $value) {
                $roleIntance3 = RoleMedia::firstOrCreate(['id' => $value]);
                $role_media[] = $roleIntance3->id;
            }
        }
        if (isset($role_media)) {
            $film->roles()->sync($role_media);
        }

        if (isset($request->cate_film)) {
            $cate_film = [];
            foreach ($request->cate_film as $value) {
                if ($value) {
                    $cateIntance = CategoryFilm::firstOrCreate(['name' => Str::upper($value)]);
                    $cate_film[] = $cateIntance->id;
                }
            }
            $film->categories()->sync($cate_film);
        }
        if (isset($request->country_films)) {
            foreach ($request->country_films as $value) {
                $countryIntance = Country::firstOrCreate(['name' => Str::title($value)]);
                $country_films[] = $countryIntance->id;
            }
            $film->filmCountries()->sync($country_films);
        }
        //
        // $film->save();
        if(!empty($changes))
        $log = ActionLog::writeActionLog($id, $film->getTable(), config("common.actions.updated", "updated"), get_class($this), "Sửa", $old_value, $changes, null);
        //
        return redirect()->route('media.films.edit',$id)
            ->with('success', 'Cập nhập thông tin phim thành công');
    }

    public function uploadListFilm(Request $req, $id)
    {

        ini_set('memory_limit', '-1');
        set_time_limit(0);

        $validator = Validator::make($req->all(), [
            'file' => 'required|mimes:xlsx,xls',
        ], [
            'file.required' => 'Chưa có file excel',
            'file.mimes' => 'Định dạng không hợp lệ',
        ]);

        if($validator->passes()) {
            $file = $req->file('file');
        
            $adminID = Auth::user()->id;
            $data = Excel::toArray(new EmployeeImport, $req->file);
            $data = $data[0];
            $dem = 0;
            if(!$data){
                return redirect()->route('media.films')->with('error', 'Thêm mới từ file ' . $file->getClientOriginalName() . ' thất bại. File không đúng dịnh dạng mẫu');               
            }
            for($i=count($data[0])-1; $i--; $i>0){
                if(!trim($data[0][$i])){
                    $dem++;
                }
                else break;
            }
            if (trim($data[0][0]) == 'STT'&&count($data[0])-$dem >= 10 && count($data[0])-$dem<=13) {
                $filmByID = Film::find($id);
                Excel::import(new FilmListUploadImport, $req->file);
                sleep(1);
                // lấy danh sách film lẻ vừa upload lên
                $filmListUpload = DB::table('film_list_uploads')
                    ->where('admin_id', $adminID)->where('status', 0)
                    ->get();    
                $filmLists = DB::table('film_lists')
                    ->where('film_id',$id)
                    ->select('name','year_create')
                    ->get();     
                $filmLists = json_decode(json_encode($filmLists), true);    
                $upload = DB::table('film_list_uploads')
                    ->where('admin_id', $adminID)
                    ->where('status', 0)
                    ->select('name','year_create')
                    ->get();
                $upload = json_decode(json_encode($upload), true);
                DB::table('film_list_uploads')->where('admin_id', $adminID)->where('status', 0)->delete();

                $checkVnName = 1;
                $checkDirector = 1;
                $checkRevenue = 1;
                $errorVnName = '';
                $errorDirector = '';
                $errorRevenue = '';

                foreach ($filmListUpload as $key => $value) {
                    if($value->name){
                        if(!$value->vn_name){
                            if($checkVnName){
                                $checkVnName = 0;
                                $errorVnName = $errorVnName.' '.($key+1);
                            }
                            else $errorVnName = $errorVnName.', '.($key+1);
                        }    
                        if(!$value->director){
                            if($checkDirector){
                                $checkDirector = 0;
                                $errorDirector = $errorDirector.' '.($key+1);
                            }
                            else $errorDirector = $errorDirector.', '.($key+1);
                        }
                        // if($value->box_office_revenue!=null&&!is_numeric($value->box_office_revenue)){
                        //     if($checkRevenue){
                        //         $checkRevenue = 0;
                        //         $errorRevenue = $errorRevenue.' '.($key+1);
                        //     }
                        //     else $errorRevenue = $errorRevenue.', '.($key+1);
                        // }
                    }
                }

                $error = '';
                $index = array();
                $checkFirst = 1;
                foreach($filmListUpload as $key => $value){
                    $index[$key] = 1;
                    // $value->box_office_revenue = str_replace(',', '', $value->box_office_revenue);
                    // $value->box_office_revenue = str_replace('.', '', $value->box_office_revenue);
                    // if(!$value->name||!$value->vn_name||!$value->director||($value->box_office_revenue!=null&&!is_numeric($value->box_office_revenue))){
                    if(!$value->name||!$value->vn_name||!$value->director){
                        $index[$key] = 0;
                        if($value->name){
                            if($checkFirst){
                                $checkFirst = 0;
                                $error = $error.'Lỗi dữ liệu bản ghi:';
                                $error = $error.' '.($key+1);
                            }
                            else $error = $error.', '.($key+1);
                        }
                        continue;
                    }
                }
                $count = count($upload);
                $duplicate = [];
                // check trùng bên trong bảng upload
                $checkFirst = 1;
                $errorUpload = '';            
                for ($i = 0; $i < $count; $i++) {
                    if($upload[$i]['name']){
                        $kt = 1;
                        $check = 1;
                        for ($j = 0; $j < $count; $j++) {
                            if($upload[$j]['name']){
                                if (array_map('mb_strtolower', $upload[$i]) == array_map('mb_strtolower', $upload[$j]) && $i != $j) {
                                    $kt = 0;
                                    if(!in_array($i,$duplicate)&&$check){
                                        array_push($duplicate,$i);                                        
                                        $check = 0;
                                        if($checkFirst) {
                                            if($error) $error = $error.". Trùng bản ghi trong file:";
                                            else $error = $error."Trùng bản ghi trong file:";
                                            $checkFirst = 0;
                                            $error = $error.' ('.($i+1);
                                            $errorUpload = $errorUpload.' ('.($i+1);
                                        }
                                        else {
                                            $errorUpload = $errorUpload.'; ('.($i+1);
                                            $error = $error.'; ('.($i+1);
                                        }
                                    }

                                    if(!in_array($j,$duplicate)){
                                        array_push($duplicate,$j);
                                        $errorUpload = $errorUpload.', '.($j+1);
                                        $error = $error.', '.($j+1);
                                    }
                                    $index[$j] = (!$index[$j])?0:$kt;
                                }
                            }
                        }
                        if($check==0){
                            $errorUpload = $errorUpload.')';
                            $error = $error.')';
                        }
                        $index[$i] = (!$index[$i])?0:$kt;
                    }
                }
                // check trùng trong DB
                $checkFirst = 1;
                $errorDB = '';
                foreach ($upload as $key => $value) {
                    if ($value['name']) {
                        $kt = 1;
                        foreach ($filmLists as $keyFilm => $filmList) {
                            if (array_map('mb_strtolower', $value) == array_map('mb_strtolower', $filmList)) {
                                if($checkFirst){
                                    if($error) $error = $error.". Trùng bản ghi trong DB:";
                                    else $error = $error."Trùng bản ghi trong DB:";
                                    $error = $error.' '.($key+1);
                                    $errorDB = $errorDB.' '.($key+1);
                                    $checkFirst = 0;
                                }
                                else {
                                    $error = $error.', '.($key+1);
                                    $errorDB = $errorDB.', '.($key+1);
                                }
                                $kt = 0;
                                break;
                            }
                        }
                        $index[$key] = (!$index[$key])?0:$kt;
                    }
                }
                if($error){
                    $error1 = [];
                    array_push($error1, [
                        'errorUpload' => $errorUpload
                    ]);
                    array_push($error1, [
                        'errorDB' => $errorDB
                    ]);
                    array_push($error1, [
                        'errorVnName' => $errorVnName
                    ]);
                    array_push($error1, [
                        'errorDirector' => $errorDirector
                    ]);
                    // array_push($error1, [
                    //     'errorRevenue' => $errorRevenue
                    // ]);
                    return redirect()->route('media.films')->with('errorVersion2', 'Thêm mới từ file ' . $file->getClientOriginalName() . ' thất bại. '.$error)->with('outputError', json_encode($error1))->with('phimLe','PhimLe');
                }
                foreach ($index as $key => $value) {
                    if (!$index[$key]) {
                        // dd('đã vào');
                        unset($filmListUpload[$key]);
                    }
                }
                if(!count($filmListUpload)){
                    return redirect()->route('media.films')->with('error', 'Thêm mới từ file ' . $file->getClientOriginalName() . ' thất bại. Thông tin trên file Import rỗng');
                }

                // dd($filmListUpload);
                $arrayFilmList = [];
                foreach ($filmListUpload as $key => $film) {
                    // loại bỏ khoảng trắng và ',' ở cuối
                    $categoryFilm = trim($film->category_film);
                    $categoryFilm = Str::of($categoryFilm)->trim(',');
                    $filmList = [];
                    $filmList['contract_id'] = $filmByID->contract_id;
                    $filmList['admin_id'] = $film->admin_id;
                    $filmList['film_id'] = $id;
                    $filmList['category_film'] = $categoryFilm;
                    $filmList['name'] = $film->name;
                    $filmList['vn_name'] = $film->vn_name;
                    $filmList['year_create'] = $film->year_create;
                    $filmList['box_office_revenue'] = (int)$film->box_office_revenue;
                    $filmList['permission'] = $film->permission;
                    // $filmList['category_film']  = $film->category_film;
                    $filmList['actor_name'] = $film->actor_name;
                    $filmList['director'] = $film->director;
                    $filmList['count_minute'] = $film->count_minute;
                    $filmList['note'] = $film->note;
                    array_push($arrayFilmList, $filmList);
                }
                // insert vào film_lists
                // DB::table('film_lists')->insert($arrayFilmList);
                foreach (array_chunk($arrayFilmList, 1000) as $t) {
                    DB::table('film_lists')->insert($t);
                }

                return redirect()->route('media.films')->with('success', 'Thêm mới từ file ' . $file->getClientOriginalName() . ' thành công! '.count($filmListUpload).' bộ phim lẻ.');
            } else {
                return redirect()->route('media.films')->with('error', 'Thêm mới từ file ' . $file->getClientOriginalName() . ' thất bại. File Không đúng định dạng mẫu');
            }
        }
            
        return redirect()->route('media.films')->with('error', $validator->errors()->first());
    
    }
}
