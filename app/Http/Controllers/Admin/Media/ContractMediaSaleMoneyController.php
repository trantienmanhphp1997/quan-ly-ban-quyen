<?php

namespace App\Http\Controllers\Admin\Media;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ContractMediaSaleMoney;
use DB;
use Illuminate\Support\Facades\Config;

class ContractMediaSaleMoneyController extends Controller
{
    public function __construct() {
        $this->checkPermission(config("common.permission.module.contract-media-sale-money", "contract-media-sale-money"));
    }

    public function index()
    {
        return view('admin/media/contractMediaSaleMoney/index');
    }
}
