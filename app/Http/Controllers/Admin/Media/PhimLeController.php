<?php

namespace App\Http\Controllers\Admin\Media;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Reponse;
use App\Models\Film;
use App\Models\FilmList;
use App\Models\CategoryFilm;
use App\Imports\EmployeeImport;
use App\Exports\FilmExport;
use App\Exports\FilmExampleExport;

use App\Imports\FilmListUploadImport;
use App\Models\ActionLog;
use Illuminate\Support\Facades\Auth;
use Excel;
use DB;
use Illuminate\Support\Str;
class PhimLeController extends Controller
{

    public function __construct() {
        $this->checkPermission(config("common.permission.module.phim-le", "phim-le"));
    }
    public function index(Request $request){    
        return view('admin/media/phim-le/index', ['id'=>$request->id]);
    }

    public function create(){

        return view('admin/media/phim-le/create');

    }
   public function store(request $request){
       $this->validate($request,[
        'name'=>'required',
        'vn_name'=>'required',
        'year_create'=>'required',
        'box_office_revenue'=>'max:20',
        'permission'=>'max:50',
        'category_film'=>'max:100',
        'actor_name'=>'required',
        'director'=>'max:50',
        'count_minute'=>'max:30',
        'director'=>'required',
        'note'=>'max:100',
      ],[],[
        'name'=>'Tên gốc',
        'vn_name'=>'Tên phim',
        'year_create'=>'Năm sản xuất',
        'box_office_revenue'=>'Doanh thu phòng vé',
        'permission'=>'Quyền',
        'count_minute'=>'Thời lượng phim',
        'category_film'=>'Thể loại phim',
        'actor_name'=>'Diễn viên',
        'director'=>'Đạo diễn',
        'note'=>'Ghi chú',

       ]);
       $filmLe = FilmList::create([
        "name"=> $request->name,
        "vn_name"=>$request->vn_name,
        "year_create"=>$request->year_create,
        "box_office_revenue"=>$request->box_office_revenue,
        "category_film"=>$request->category_film,
        "permission"=>$request->permission,
        "actor_name"=>$request->actor_name,
        "director"=>$request->director,
        "count_minute"=>$request->count_minute,
        "note"=>$request->note,
        "admin_id"=>Auth::user()->id,

       ]);
       return redirect()->route('media.phim-le')->with('success','Tạo mới phim lẻ thành công');
   }

   public function edit($id){
    //    $getData =FilmList::table('film_lists')->select('id','name','vn_name','year_create','box_office_revenue',
    //    'category_film','permission','actor_name','director','count_minute','note')->where('id',$id)->get();
    //    return view('admin.media.phim-le.edit');
       $data = FilmList::LeftJoin('films','films.id','film_lists.film_id')->where('film_lists.id',$id)->select('film_lists.*','films.vn_name as film_name')->get()->first();
       return view('admin.media.phim-le.edit',compact('data'));
   }
   public function update(request $request,$id){
      $this->validate($request,[
        'name'=>'required',
        'vn_name' =>'required',
        'actor_name' => 'required'
      ],[
        'name.required'=>'Chưa nhập tên gốc',
        'vn_name.required'=> 'Chưa nhập tên tiếng việt',
        'actor_name.required'=>'Chưa nhập tên diễn viên'
      ]);

        $input=$request->all(); //lấy tất cả bản ghi trong 1 table
        $filmLe=FilmList::find($id)->update($input);
        // $filmLe->fill($input);
        // $change=$filmLe->getDirty();
        // $oldFilm=array();
        // foreach($change as $key=>$value){
        //     $oldFilm[$key] = $filmLe->value($key);
        // }
        // $filmLe->save();

       // $log = ActionLog::writeActionLog($id, $filmLe->getTable(), config("common.actions.updated", "updated"),  get_class($this), "update ", $oldFilm, $change);
        return redirect()->route('media.phim-le')->with('success','Cập nhập phim lẻ thành công');
   }

}
