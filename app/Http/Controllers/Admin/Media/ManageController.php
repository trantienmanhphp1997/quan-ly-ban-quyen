<?php

namespace App\Http\Controllers\Admin\Media;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Reponse;
use App\Models\Category;
use App\Models\FilmBackup;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Auth;
use App\Imports\ManageUploadImport;
use App\Exports\ManageExampleExport;
use App\Exports\FilmBackupExport;
use App\Models\CategoryFilm;
use App\Imports\EmployeeImport;
use Illuminate\Support\Str;
use DB;
use Excel;
use Closure;
use Illuminate\Support\Facades\Validator;

class ManageController extends Controller{


    public function __construct() {
        $this->checkPermission(config("common.permission.module.manage", "manage"));
    }


    public $searchDriver;
    public $keyword;

    public function index(Request $request){
        return view('admin.media.manage.index');
    }

   public function upload(Request $req){
        ini_set('memory_limit', '-1');
        set_time_limit(0);

        $validator = Validator::make($req->all(), [
            'file' => 'required|mimes:xlsx,xls',
        ], [
            'file.required' => 'Chưa có file excel',
            'file.mimes' => 'Định dạng không hợp lệ',
        ]);

        if($validator->passes()) {
            $file = $req->file('file');
            $adminID = Auth::user()->id;
            $data = Excel::toArray(new EmployeeImport, $req->file);
            $data = $data[0];
            $dem = 0;
            if(!$data){
                return redirect()->route('media.manage')->with('error', 'Thêm mới từ file ' . $file->getClientOriginalName() . ' thất bại. File không đúng dịnh dạng mẫu');               
            }
            for($i=count($data[0])-1; $i--; $i>0){
                if(!trim($data[0][$i])){
                    $dem++;
                }
                else break;
            }
            if(trim($data[0][0]) == 'STT'&&count($data[0])-$dem >= 15 && count($data[0])-$dem<=18){
                Excel::import(new ManageUploadImport, $req->file);
                sleep(1);
                $filmBackups = DB::table('film_backups')->where('admin_id',$adminID)->where('status',1)->get();
                // dd($filmBackups);
                $error = "";
                $films = DB::table('film_backups')->where('status','0')->select('driver', 'file_name')->get();
                $films = json_decode(json_encode($films), true);
                $upload = DB::table('film_backups')
                    ->where('status', '1')
                    ->where('admin_id', $adminID)
                    ->select('driver', 'file_name')
                    ->get();
                $upload = json_decode(json_encode($upload), true);
                // Chuẩn hóa date
                    foreach ($filmBackups as $key => $value) {
                        if($value->end_time=='0000-00-00') $filmBackups[$key]->end_time = null;
                    } 
                //
                $checkEndTime = 1;
                $checkDriver = 1;
                $checkBackupDriver = 1;
                $checkVnName = 1;
                $checkCountry = 1;
                $checkTimeNumber = 1;
                $checkVolumn = 1;
                $errorEndTime = '';
                $errorDriver = '';
                $errorBackupDriver = '';
                $errorVnName = '';
                $errorCountry = '';
                $errorTimeNumber = '';
                $errorVolumn = '';
                foreach ($filmBackups as $key => $value) {
                    if($value->file_name){
                        if(!$value->end_time){
                            if($checkEndTime){
                                $checkEndTime = 0;
                                $errorEndTime = $errorEndTime.' '.($key+1);
                            }
                            else $errorEndTime = $errorEndTime.', '.($key+1);
                        }    
                        if(!$value->driver){
                            if($checkDriver){
                                $checkDriver = 0;
                                $errorDriver = $errorDriver.' '.($key+1);
                            }
                            else $errorDriver = $errorDriver.', '.($key+1);
                        }
                        if(!$value->backup_driver){
                            if($checkBackupDriver){
                                $checkBackupDriver = 0;
                                $errorBackupDriver = $errorBackupDriver.' '.($key+1);
                            }
                            else $errorBackupDriver = $errorBackupDriver.', '.($key+1);
                        }

                        if(!$value->vn_name){
                            if($checkVnName){
                                $checkVnName = 0;
                                $errorVnName = $errorVnName.' '.($key+1);
                            }
                            else $errorVnName = $errorVnName.', '.($key+1);
                        }
                        if(!$value->country){
                            if($checkCountry){
                                $checkCountry = 0;
                                $errorCountry = $errorCountry.' '.($key+1);
                            }
                            else $errorCountry = $errorCountry.', '.($key+1);
                        }
                        if(!$value->time_number){
                            if($checkTimeNumber){
                                $checkTimeNumber = 0;
                                $errorTimeNumber = $errorTimeNumber.' '.($key+1);
                            }
                            else $errorTimeNumber = $errorTimeNumber.', '.($key+1);
                        }
                        if(!$value->volumn){
                            if($checkVolumn){
                                $checkVolumn = 0;
                                $errorVolumn = $errorVolumn.' '.($key+1);
                            }
                            else $errorVolumn = $errorVolumn.', '.($key+1);
                        }
                    }
                }

                $index = array();
                $checkFirst = 1;
                foreach($filmBackups as $key => $value){
                    $index[$key] = 1;
                    if(!$value->driver||!$value->backup_driver||!$value->vn_name||!$value->file_name||!$value->country||!$value->end_time||!$value->volumn||!$value->time_number){
                        $index[$key] = 0;
                        if($value->file_name){
                            if($checkFirst){
                                $checkFirst = 0;
                                $error = $error.'Lỗi dữ liệu bản ghi:';
                                $error = $error.' '.($key+1);
                            }
                            else $error = $error.', '.($key+1);
                        }
                        continue;
                    }
                }
                $count = count($upload);
                $duplicate = [];
                // check trùng bên trong bảng upload
                $checkFirst = 1;
                $errorUpload = '';
                for ($i = 0; $i < $count; $i++) {
                    if($upload[$i]['file_name']){
                        $kt = 1;
                        $check = 1;
                        for ($j = 0; $j < $count; $j++) {
                            if($upload[$j]['file_name']){
                                if (array_map('mb_strtolower', $upload[$i]) == array_map('mb_strtolower', $upload[$j]) && $i != $j) {
                                    $kt = 0;
                                    if(!in_array($i,$duplicate)&&$check){
                                        array_push($duplicate,$i);                                        
                                        $check = 0;
                                        if($checkFirst) {
                                            if($error) $error = $error.". Trùng bản ghi trong file:";
                                            else $error = $error."Trùng bản ghi trong file:";
                                            $checkFirst = 0;
                                            $error = $error.' ('.($i+1);
                                            $errorUpload = $errorUpload.' ('.($i+1);
                                        }
                                        else {
                                            $errorUpload = $errorUpload.'; ('.($i+1);
                                            $error = $error.'; ('.($i+1);
                                        }
                                    }

                                    if(!in_array($j,$duplicate)){
                                        array_push($duplicate,$j);
                                        $errorUpload = $errorUpload.', '.($j+1);
                                        $error = $error.', '.($j+1);
                                    }
                                    $index[$j] = (!$index[$j])?0:$kt;
                                }
                            }
                        }
                        if($check==0) {
                            $errorUpload = $errorUpload.')';
                            $error = $error.')';
                        }
                        $index[$i] = (!$index[$i])?0:$kt;
                    }
                }
                // check trùng với bảng film
                $checkFirst = 1;
                $errorDB = '';
                foreach ($upload as $key => $value) {
                    if ($value['file_name']) {
                        $kt = 1;
                        foreach ($films as $keyFilm => $film) {
                            if (array_map('mb_strtolower', $value) == array_map('mb_strtolower', $film)) {
                                if($checkFirst){
                                    if($error) $error = $error.". Trùng bản ghi trong DB:";
                                    else $error = $error."Trùng bản ghi trong DB:";
                                    $errorDB = $errorDB.' '.($key+1);
                                    $error = $error.' '.($key+1);
                                    $checkFirst = 0;
                                }
                                else {
                                    $errorDB = $errorDB.', '.($key+1);
                                    $error = $error.', '.($key+1);
                                }
                                $kt = 0;
                                break;
                            }
                        }
                        $index[$key] = (!$index[$key])?0:$kt;
                    }
                }

                // yêu cầu mới
                $countries = DB::table('countries')->pluck('name')->toArray();
                $countries = json_decode(json_encode($countries), true);
                // Chuẩn hóa lại tên quốc gia đã lưu trong DB
                $countries = array_map('mb_strtolower', $countries);
                $checkFirst = 1;
                $errorExistCountry = '';
                foreach ($filmBackups as $key => $value) {
                    if($value->file_name){
                        $index[$key] = 1;
                        if(!in_array(Str::lower($value->country),$countries)){
                            $index[$key] = 0;
                            if($value->file_name){
                                if($checkFirst){
                                    $checkFirst = 0;
                                    if($error) $error = $error.". Quốc gia không tồn tại trong hệ thống:";
                                    else $error = $error."Quốc gia không tồn tại trong hệ thống:";
                                    $errorExistCountry = $errorExistCountry.' '.($key+1);
                                }
                                else $errorExistCountry = $errorExistCountry.', '.($key+1);
                            }
                            continue;
                        }
                    }
                }
                if($error){
                    DB::table('film_backups')
                        ->where('status', '1')
                        ->where('admin_id', $adminID)
                        ->delete();
                    $error1 = [];
                    array_push($error1, [
                        'errorUpload' => $errorUpload
                    ]);
                    array_push($error1, [
                        'errorDB' => $errorDB
                    ]);
                    array_push($error1, [
                        'errorEndTime' => $errorEndTime
                    ]);
                    array_push($error1, [
                        'errorDriver' => $errorDriver
                    ]);
                    array_push($error1, [
                        'errorBackupDriver' => $errorBackupDriver
                    ]);
                    array_push($error1, [
                        'errorVnName' => $errorVnName
                    ]);
                    array_push($error1, [
                        'errorCountry' => $errorCountry
                    ]);
                    array_push($error1, [
                        'errorTimeNumber' => $errorTimeNumber
                    ]);
                    array_push($error1, [
                        'errorVolumn' => $errorVolumn
                    ]);
                    array_push($error1, [
                        'errorExistCountry' => $errorExistCountry
                    ]);
                    return redirect()->route('media.manage')->with('errorVersion2', 'Thêm mới từ file ' . $file->getClientOriginalName() . ' thất bại. '.$error)->with('outputError', json_encode($error1));
                }
                DB::table('film_backups')
                    ->where('status', '1')
                    ->where('admin_id', $adminID)
                    ->where('file_name','=','')
                    ->delete();
                DB::table('film_backups')
                    ->where('status', '1')
                    ->where('admin_id', $adminID)
                    ->update(["status"=>0]);
                foreach ($filmBackups as $key => $value) {
                    if (!$index[$key]) {
                        // dd('đã vào');
                        unset($filmBackups[$key]);
                    }
                }
                if(!count($filmBackups)){
                    return redirect()->route('media.manage')->with('error', 'Thêm mới từ file ' . $file->getClientOriginalName() . ' thất bại. Thông tin trên file Import rỗng');
                }

                return redirect()->route('media.manage')->with('success', 'Thêm mới từ file '.$file->getClientOriginalName().' thành công '. count($filmBackups). ' bản ghi');
            } else {
                return redirect()->route('media.manage')->with('error', 'Thêm mới từ file '.$file->getClientOriginalName().' thất bại. File không đúng định dạng mẫu');
            }
        }

        return redirect()->route('media.manage')->with('error', $validator->errors()->first());
    }

    public function export(){
        $today = date("d_m_Y");
        return Excel::download(new FilmBackupExport, 'filmBackup-'.$today.'.xlsx');
    }

    public function exportExampleFile(Request $request){
        return Excel::download(new ManageExampleExport, 'manageExample.xlsx');
    }
    public function create(){

        //$cate_manage=$this->cateManage();
        return view('admin/media/manage/create');
    }
    // public function cateManage(){
    //     $cate_manage=[];
    //     $category = CategoryFilm::all();
    //     foreach($category as $value){
    //         $cateIntance = CategoryFilm::firstOrCreate(['name'=>$value]); // tìm kiếm tên thể loại, nếu chưa có sẽ tạo mới
    //         $cate_manage[]= $cateIntance->id; //lấy ra bản ghi và gán id cho nó
    //     }
    // return $cate_manage;


    // }


    public function store(Request $request){

        $this->validate($request,[
            'driver'=>'required',
            'backup_driver'=>'required',
            'file_name'=>'required|checkUniqueFileName:'.$request->file_name.','.$request->driver,
            'film_name'=>'max:100',
            'vn_name'=>'required',
            'country'=>'required',
            'partner'=>'max:100',
            'series_film'=>'max:100',
            'count_film'=>'max:100',
            'file_type'=>'max:10',
            'time_number'=>'required',
            'volumn'=>'required',
            'file_format'=>'max:100',
            'end_time'=>'required',
            'file_delivery_person'=>'max:100',
         ],[],[
            'volumn'=>'Dung lượng',
            'driver'=>'Mã ổ',
            'vn_name'=>'Tên phim Tiếng Việt',
            'series_film'=>'Thể loại phim',
            'country'=>'Quốc gia',
            'time_number'=>'Thời lượng phim',
            'end_time'=>'Thời hạn bản quyền',
            'backup_driver'=>'Ổ backup',
            'file_name'=>'Tên file',
            'file_type'=>'Loại file',

        ]);
        $tuLieu = FilmBackup::create([
            'driver'=>$request->driver,
            'backup_driver'=>$request->backup_driver,
            'file_name'=>$request->file_name,
            'film_name'=>$request->film_name,
            'vn_name'=>$request->vn_name,
            'country'=>$request->country,
            'partner'=>$request->partner,
            'series_film'=>$request->series_film,
            'count_film'=>$request->count_film,
            'file_type'=>$request->file_type,
            'time_number'=>$request->time_number,
            'volumn'=>$request->volumn,
            'file_format'=>$request->file_format,
            'end_time'=>$request->end_time,
            'file_delivery_person'=>$request->file_delivery_person
        ]);

        return redirect()->route('media.manage')->with('success','Tạo tư liệu phim thành công');


    }
    public function edit($id){

        $data = FilmBackup::find($id);
        return view('admin.media.manage.edit',compact('data'));
    }
    public function update(Request $request,$id){

        $this->validate($request,[
            'driver'=>'required',
            'backup_driver'=>'required',
            'file_name'=>'required|checkUniqueFileName:'.$request->file_name.','.$request->driver.','.$id,
            'film_name'=>'max:100',
            'vn_name'=>'required',
            'country'=>'required',
            'partner'=>'max:100',
            'OptionCate'=>'max:100',
            'count_film'=>'max:100',
            'file_type'=>'max:10',
            'time_number'=>'required',
            'volumn'=>'required',
            'file_format'=>'max:100',
            'end_time'=>'required',
            'file_delivery_person'=>'max:100',
         ],[],[
            'volumn'=>'Dung lượng',
            'driver'=>'Mã ổ',
            'vn_name'=>'Tên phim Tiếng Việt',
            'series_film'=>'Thể loại phim',
            'country'=>'Quốc gia',
            'time_number'=>'Thời lượng phim',
            'end_time'=>'Thời hạn bản quyền',
            'backup_driver'=>'Ổ backup',
            'file_name'=>'Tên file',
            'file_type'=>'Loại file',

        ]);
        $input = $request->all();
        $manage=FilmBackup::find($id)->update($input);
        return redirect()->route('media.manage')->with('success','Cập nhật tự liệu phim thành công');
    }
    public function delete($id){
        FilmBackup::where('id',$id)->delete();
        return redirect()->route('media.manage')->with('success','Xóa tư liệu phim thành công');

    }
}

