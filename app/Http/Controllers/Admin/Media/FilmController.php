<?php

namespace App\Http\Controllers\Admin\Media;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Reponse;
use App\Models\Film;
use App\Models\FilmList;
use App\Imports\EmployeeImport;
use App\Exports\FilmExport;
use App\Exports\FilmExampleExport;

use App\Imports\FilmListUploadImport;
use App\Models\ActionLog;
use Illuminate\Support\Facades\Auth;
use Excel;
use DB;
use Illuminate\Support\Str;
class FilmController extends Controller
{
    public function __construct() {
        $this->checkPermission(config("common.permission.module.film", "film"));
    }
    public function endTimeContract($value){
        $count=Film::whereBetween('end_time',[Carbon::now(),$value])->count();
        return $count;
    }
    public function index(Request $request){
        $page = \Request::get('page') ? \Request::get('page') : 1;
        $query = DB::table('films');
        $perPage = Config::get('app_per_page') ? Config::get('app_per_page') : 100;
        $startLimit = $perPage * ($page - 1);
        $endLimit = $perPage * $page;

        // if($request->keyword){
        //     $query = Film::search($request->keyword, null, true);
        // }
        // dd($query);
        $query = $query->select('films.*');
        $query->leftjoin('contracts', function($join)
        {
            $join->on('films.contract_id', '=', 'contracts.id');
        });
        $threeMonth=$this->endTimeContract(Carbon::now()->addMonth(3));
        $twoMonth=$this->endTimeContract(Carbon::now()->addMonth(2));
        $oneMonth=$this->endTimeContract(Carbon::now()->addMonth(1));
        $oneWeek=$this->endTimeContract(Carbon::now()->addWeek(1));
        if($request->deadline==3)
            $query->whereBetween('films.end_time',[Carbon::now(),Carbon::now()->addMonth(3)]);
        elseif ($request->deadline==2)
            $query->whereBetween('films.end_time',[Carbon::now(),Carbon::now()->addMonth(2)]);
        elseif($request->deadline==1)
            $query->whereBetween('films.end_time',[Carbon::now(),Carbon::now()->addMonth(1)]);
        elseif($request->deadline==4)
            $query->whereBetween('films.end_time',[Carbon::now(),Carbon::now()->addWeek(1)]);
        $data = $query->orderBy('films.id','desc')->offset($startLimit)->limit($perPage)->paginate($perPage);
        return view('admin/media/film/index', compact('data','threeMonth','twoMonth','oneMonth','oneWeek'))->with('i', ($request->input('page', 1) - 1) * $perPage);
    }

    public function create(){
    	return view('admin/media/film/create');
    }

    public function upload(Request $req){
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        if($req->hasFile('file')){
            $file = $req->file('file');
            $pathFile = $file->getClientOriginalExtension();
            if($pathFile=='xlsx'){
                $array = Excel:: toArray(new EmployeeImport,$req->file);
                // Loại bỏ những cột không có dữ liệu
                $arrayIndex = [];
                $arrayContract = [];
                $array = $array[0];
                for($i=0; $i < count($array[0]); $i++){
                    if($array[0][$i]!=null){
                        array_push($arrayIndex,$i);
                    }
                }
                // retrun nếu không đủ hàng
                if(count($arrayIndex)<20){
                    return redirect()->back()->with('message_danger','File '.$file->getClientOriginalName().' không đúng, xin kiểm tra lại!');
                }
                $purchase_contract = $req->purchase_contract;
                if($purchase_contract=='buy'){
                    $purchase_contract = 1;
                }
                else if($purchase_contract=='sale'){
                    $purchase_contract = 2;
                }
                for($i = 0; $i < count($array); $i++){
                    $film_country = trim($array[$i][$arrayIndex[1]]);
                    if($film_country==null) {
                        continue;
                    }
                    $film_name = trim($array[$i][$arrayIndex[2]]);
                    if($film_name==null) {
                        continue;
                    }
                    $film_year_created = trim($array[$i][$arrayIndex[3]]);
                    if($film_year_created==null) {
                        continue;
                    }
                    $film_category_name = trim($array[$i][$arrayIndex[4]]);
                    if($film_category_name==null) {
                        continue;
                    }
                    $film_count_name = trim($array[$i][$arrayIndex[5]]);
                    if($film_count_name==null) {
                        continue;
                    }
                    $film_count_tap = trim($array[$i][$arrayIndex[6]]);
                    if($film_count_tap==null) {
                        continue;
                    }
                    $film_count_hour = trim($array[$i][$arrayIndex[7]]);
                    if($film_count_hour==null) {
                        continue;
                    }
                    $contract_partner_name = trim($array[$i][$arrayIndex[8]]);
                    if($contract_partner_name==null) {
                        continue;
                    }
                    $contract_permissin_contract = trim($array[$i][$arrayIndex[9]]);
                    if($contract_permissin_contract==null) {
                        continue;
                    }
                    $contract_date_end = trim($array[$i][$arrayIndex[10]]);
                    if($contract_date_end==null) {
                        continue;
                    }
                    else if(is_numeric($contract_date_end)){
                       $contract_date_end = (int)$contract_date_end;
                       $contract_date_end -= 25569;
                       $contract_date_end = date("Y-m-d H:i:s", $contract_date_end*3600*24);
                    }
                    else {
                        $time = strtotime($contract_date_end);
                        $contract_date_end = date('Y-m-d H:i:s',$time);
                        $new_date = date('d-m-Y',$time);
                        if($new_date=='01-01-1970'){
                            continue;
                        }
                    }
                    $contract_date_begin = trim($array[$i][$arrayIndex[11]]);
                    if($contract_date_begin==null) {
                        continue;
                    }
                    else if(is_numeric($contract_date_begin)){
                       $contract_date_begin = (int)$contract_date_begin;
                       $contract_date_begin -= 25569;
                       $contract_date_begin = date("Y-m-d H:i:s", $contract_date_begin*3600*24);
                    }
                    else {
                        $time = strtotime($contract_date_begin);
                        $contract_date_begin = date('Y-m-d H:i:s',$time);
                        $new_date = date('d-m-Y',$time);
                        if($new_date=='01-01-1970'){
                            continue;
                        }
                    }
                    $contract_contract_number = trim($array[$i][$arrayIndex[12]]);
                    if($contract_contract_number==null) {
                        continue;
                    }
                    $contract_fee1 = trim($array[$i][$arrayIndex[13]]);
                    $contract_fee1 = str_replace(',', '', $contract_fee1);
                    $contract_fee1 = str_replace('.', '', $contract_fee1);
                    if($contract_fee1==null||!is_numeric($contract_fee1)) {
                        continue;
                    }
                    $contract_fee2 = trim($array[$i][$arrayIndex[14]]);
                    $contract_fee2 = str_replace(',', '', $contract_fee2);
                    $contract_fee2 = str_replace('.', '', $contract_fee2);
                    if($contract_fee2==null||!is_numeric($contract_fee2)) {
                        continue;
                    }
                    $contract_fee3 = trim($array[$i][$arrayIndex[15]]);
                    $contract_fee3 = str_replace(',', '', $contract_fee3);
                    $contract_fee3 = str_replace('.', '', $contract_fee3);
                    if($contract_fee3==null||!is_numeric($contract_fee3)) {
                        continue;
                    }
                    $film_note = trim($array[$i][$arrayIndex[17]]);
                    if($film_note==null) {
                        continue;
                    }
                    $film_actor = trim($array[$i][$arrayIndex[18]]);
                    if($film_actor==null) {
                        continue;
                    }
                    $film_dirrector = trim($array[$i][$arrayIndex[19]]);
                    if($film_dirrector==null) {
                        continue;
                    }
                    // $arrayContract[$contract_contract_number] = $i;
                    // array_push($arrayContract[$contract_contract_number], $i);
                    $data  = DB::table('contracts')->where('contract_number', $contract_contract_number)->get();
                    if(count($data)==1){
                        DB::table('films')->insert([
                            'vn_name' => $film_name ,
                            'actor_name' => $film_actor,
                            'start_time' => $contract_date_begin,
                            'country' => $film_country ,
                            'year_create' => $film_year_created,

                            'admin_id'=> Auth::user()->id,
                            'category_name' => $film_category_name,
                            'count_name' =>$film_count_name,
                            'count_tap' => $film_count_tap,
                            'count_hour' => $film_count_hour,
                            'partner_name' =>$contract_partner_name,
                            'permissin_contract' => $contract_permissin_contract,
                            'end_time' => $contract_date_end,
                            'note' =>$film_note,
                            'director' => $film_dirrector,
                            'contract_id'=> $data[0]->id,
                        ]);
                    }
                    else {
                        // dd($purchase_contract);
                        DB::table('contracts')->insert([
                            'contract_number' =>$contract_contract_number,
                            'fee1' => $contract_fee1,
                            'fee2' => $contract_fee2,
                            'fee3' => $contract_fee3,
                            'type' => $purchase_contract,
                            'category_id' => 2,
                            'admin_id'=> Auth::user()->id,
                        ]);
                        $data = DB::table('contracts')->where('contract_number',$contract_contract_number)->get();
                        DB::table('films')->insert([
                            'vn_name' => $film_name ,
                            'actor_name' => $film_actor,
                            'start_time' => $contract_date_begin,
                            'country' => $film_country ,
                            'year_create' => $film_year_created,

                            'admin_id'=> Auth::user()->id,
                            'category_name' => $film_category_name,
                            'count_name' =>$film_count_name,
                            'count_tap' => $film_count_tap,
                            'count_hour' => $film_count_hour,
                            'partner_name' =>$contract_partner_name,
                            'permissin_contract' => $contract_permissin_contract,
                            'end_time' => $contract_date_end,
                            'note' =>$film_note,
                            'director' => $film_dirrector,
                            'contract_id'=> $data[0]->id,
                        ]);
                    }
                }
                return redirect()->route('media.films')->with('message_success', 'Thêm mới từ file '.$file->getClientOriginalName().' thành công!');
            }
            else {
                return redirect()->route('media.films')->with('message_danger','Không đúng định dạng file Excel!');
            }
        }
        else {
            return redirect()->route('media.films')->with('message_danger', 'Chưa có file Excel!');
        }
    }

    public function export(){
        $today = date("d_m_Y");
        return Excel::download(new FilmExport, 'films-'.$today.'.xlsx');
    }
    public function exportExampleFile(){
        return Excel::download(new FilmExampleExport, 'filmsExample.xlsx');
    }

    public function delete($id){
        DB::table('films')->where('id',$id)->delete();
        return redirect()->back()->with('message_success','Deleted Successfully');
    }

    public function deleteAll(Request $req){
        DB::table('films')->whereIn('id', $req->filmIds)->delete();
        return redirect()->back()->with('message_success','Deleted Successfully');;
    }
    public function edit($id){
        $data = Film::find($id);
        return view('admin.media.film.edit',compact('data'));
    }
    public function store(Request $request)
    {
        Film::create($request->all());

        return redirect()->route('media.films')
            ->with('message_success','Created Successfully');
    }

    public function update(Request $request, $id){

        $this->validate($request, [
            'vn_name' => 'required',
        ]);

        $input = $request->all();
        $film = Film::find($id);
        $film->fill($input);
        $changes = $film->getDirty();
        $old_value = array();
        foreach ($changes as $key => $value) {
            $old_value[$key] = $film->value($key);
        }

        $film->save();
        $log = ActionLog::writeActionLog($id, $film->getTable(), config("common.actions.updated", "updated"),  get_class($this), "update ", $old_value, $changes, null);

        return redirect()->route('media.films')
            ->with('message_success','updated Successfully');
    }

    public function uploadListFilm(Request $req, $id){
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        if($req->hasFile('file')){
            $file = $req->file('file');
            $pathFile = $file->getClientOriginalExtension();
            if($pathFile=='xlsx'){
                $filmByID = Film::find($id);
                $adminID = Auth::user()->id;
                Excel::import(new FilmListUploadImport, $req->file);
                // lấy danh sách film lẻ vừa upload lên
                $filmListUpload = DB::table('film_list_uploads')->where('admin_id',$adminID)->where('status',0)->where('name','!=',null)->get();
                // lấy danh sách tên loại phim
                $categoryFilm = DB::table('category_films')->select('name')->get();
                $categoryFilm = json_decode(json_encode($categoryFilm), true);
                DB::table('film_list_uploads')->where('admin_id',$adminID)->where('status',0)->update(['status' => '1']);
                // mảng lưu những thẻ loại phim mới
                $newCategory = [];
                $arrayFilmList = [];
                foreach ($filmListUpload as $key => $film) {
                    // loại bỏ khoảng trắng và ',' ở cuối
                    $arrayCategoryFilm = trim($film->category_film);
                    $arrayCategoryFilm = Str::of($arrayCategoryFilm)->trim(',');
                    // lưu lại để sử dụng cho lần foreach sau
                    $filmListUpload[$key]->category_film = $arrayCategoryFilm;
                    // chuyển thành mảng
                    $arrayCategoryFilm = explode(',',$arrayCategoryFilm);
                    foreach ($arrayCategoryFilm as $value) {
                        $value  = trim($value);
                        $newFilm = [
                            'name' => $value,
                        ];
                        // nếu bản ghi tồn tại trong những thể loại phim mới mới
                        if(in_array($newFilm, $newCategory)){
                            continue;
                        }
                        else {
                            // nếu bản ghi đã tồn tại trong những thể loại phim cũ
                            if(in_array($newFilm, $categoryFilm)){
                                continue;
                            }
                            else {
                                array_push($newCategory, $newFilm);
                            };
                        }
                    }
                    $filmList = [];
                    $filmList['contract_id'] = $filmByID->contract_id;
                    $filmList['admin_id']  = $film->admin_id;
                    $filmList['film_id'] = $id;

                    $filmList['name']  = $film->name;
                    $filmList['vn_name']  = $film->vn_name;
                    $filmList['year_create']  = $film->year_create;
                    $filmList['box_office_revenue']  = $film->box_office_revenue;
                    $filmList['permission']  = $film->permission;
                    // $filmList['category_film']  = $film->category_film;
                    $filmList['actor_name']  = $film->actor_name;
                    $filmList['director']  = $film->director;
                    $filmList['count_minute']  = $film->count_minute;
                    $filmList['note']  = $film->note;
                    array_push($arrayFilmList, $filmList);
                }
                // insert những thể loại phim mới
                // CategoryFilm::insert($newCategory);
                foreach (array_chunk($newCategory,1000) as $t){
                    DB::table('category_films')->insert($t);
                }
                // tìm max ID
                $maxID = DB::table('film_lists')->max('id');
                if($maxID==null){
                    $maxID = 0;
                }
                // insert vào film_lists
                // DB::table('film_lists')->insert($arrayFilmList);
                foreach (array_chunk($arrayFilmList,1000) as $t){
                    DB::table('film_lists')->insert($t);
                }

                $categoryFilmAfterInsert = DB::table('category_films')->pluck('name','id');
                $categoryFilmAfterInsert = json_decode(json_encode($categoryFilmAfterInsert), true);
                $arrayFilmListCategory = [];

                foreach($filmListUpload as $key => $film2){
                    $arrayCategoryFilm = explode(',',$film2->category_film);
                    foreach ($arrayCategoryFilm as $value) {
                        $value = trim($value);
                        $keyValue = array_search($value , $categoryFilmAfterInsert);
                        if($keyValue){
                            $film = [];
                            $film['id_film_list'] = $maxID+1;
                            $film['id_category_film'] = $keyValue;
                            array_push($arrayFilmListCategory,$film);
                        }
                    }
                    $maxID++;
                }
                // DB::table('film_list_categories')->insert($arrayFilmListCategory);
                foreach (array_chunk($arrayFilmListCategory,1000) as $t){
                    DB::table('film_list_categories')->insert($t);
                }
                 return redirect()->route('media.films')->with('message_success', 'Thêm mới từ file '.$file->getClientOriginalName().' thành công!');
            }
            else {
                return redirect()->route('media.films')->with('message_danger','Không đúng định dạng file Excel!');
            }
        }
        else {
            return redirect()->route('media.films')->with('message_danger', 'Chưa có file Excel!');
        }
    }
}
