<?php
namespace App\Http\Controllers\Admin\Media;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Reponse;
use App\Models\Category;
use App\Models\FilmBackup;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Auth;
use App\Imports\ManageUploadImport;
use App\Exports\ManageExampleExport;
use App\Exports\FilmBackupExport;
use App\Models\CategoryFilm;
use App\Models\Country;
use DB;
use Excel;
use PhpParser\Node\Expr\FuncCall;
use App\Exports\CountryExport;

class CountriesController extends Controller{

    public function __construct() {
        $this->checkPermission(config("common.permission.module.countries", "countries"));
    }
    public $searchCode ;
    public $searchName;

    public function index(Request $request){
        $searchName = $request->searchName?trim($request->searchName):'';
        $searchCode = $request->searchCode?trim($request->searchCode):'';
        $query=Country::query();
        if($searchName) $query->where('name','like', '%'.$searchName.'%');
        if($searchCode) $query->where('code','like', '%'.$searchCode.'%');
        $data = $query->orderBy('id','desc')->paginate(10);
        $model= config("common.permission.module.countries", "countries");
        return view('admin/media/countries/index',compact('data','model'));
    }
    public function create(){
        return view('admin/media/countries/create');
    }
    public function store(Request $request){
        $this->validate($request,[
            'name'=>'required',
            'code'=>'required|unique:countries,code,',
            ],[],[
            'name'=>'Tên quốc gia',
            'code'=>'Mã quốc gia'
        ]);
        $country= Country::create([
            'name'=>$request->name,
            'code'=>$request->code
        ]);
        return redirect()->route('media.countries')->with('success','Thêm quốc gia mới thành công');
    }

    public function edit($id){
        $data = Country::find($id);
        return view('admin/media/countries/edit',compact('data'));
    }
    public function update(Request $request,$id){
        $this->validate($request,[
            'name'=>'required',
            'code'=>'required|unique:countries,code,'.$id,
            ],[],[
            'name'=>'Tên quốc gia',
            'code'=>'Mã quốc gia'

        ]);
        $input =$request->all();
        $country = Country::find($id)->update($input);
        return redirect()->route('media.countries')->with('success','Sửa quốc gia thành công');
    }
    public function delete($id){
        Country::where('id',$id)->delete();
        return redirect()->route('media.countries')->with('success','Xóa quốc gia thành công');
    }
}
