<?php

namespace App\Http\Controllers\Admin\Media;

use App\exports\MusicExampleExport;
use App\Exports\MusicExport;
use App\Http\Controllers\Controller;
use App\Imports\EmployeeImport;
use App\Imports\MusicUploadImport;
use App\Models\ActionLog;
use App\Models\Music;
use App\Models\TypeFee;
use App\Models\Contract;
use App\Models\ContractMediaSaleMoney;
use DB;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
class MusicController extends Controller
{
    public function __construct()
    {
        $this->checkPermission(config("common.permission.module.music", "music"));
    }
    public function index(Request $request)
    {
        return view('admin.media.music2.index');
    }
    public function create()
    {
        $list_type = TypeFee::pluck('name', 'id');
        return view('admin/media/music/create', compact('list_type'));
    }

    public function store(Request $request)
    {
        $request->ghi_am = trim($request->ghi_am);
        $request->name = trim($request->name);
        $request->contract_number1 = trim($request->contract_number1);
        $request->contract_number4 = trim($request->contract_number4);
        $this->validate($request, [
            'name' => 'required|max:1000|checkUniqueName:music,name,'.$request->ghi_am.','.$request->name,
            'singer' => 'max:1000',
            'musician' => 'max:1000',
            'album' => 'max:1000',
            'ghi_am' => 'nullable|my_contract|max:1000|type_contract:1,1|active_contract',
            'tac_gia' => 'nullable|my_contract|max:1000|type_contract:4|active_contract',
            'contract_cps' => 'max:1000',
            'contract_number1' => 'nullable|my_contract|max:1000|type_contract:3|active_contract',
            'contract_number2' => 'max:1000',
            'contract_number3' => 'max:1000',
            'contract_number4' => 'nullable|my_contract|max:1000|type_contract:3|active_contract',
            'contract_number5' => 'max:1000',
            'contract_number6' => 'max:1000',
            'music_id' => 'max:1000',
            'start_time' => 'required',
            'money' => 'max:1000',
            'note' => 'max:1000',
            'end_time' => 'required|after:start_time',
            'type_fee_id' => 'required|max:10',
        ], [], [
            'name' => 'Tên bài hát',
            'type_fee_id' => 'Loại hợp đồng',
            'singer' => 'Ca sĩ',
            'album' => 'Album',
            'musician' => 'Nhạc sĩ',
            'ghi_am' => 'Quyền liên quan',
            'tac_gia' => 'Quyền tác giả',
            'contract_cps' => 'Số phụ lục hợp đồng',
            'contract_number1' => 'Số hợp đồng quyền liên quan',
            'contract_number2' => 'Số phụ lục hợp đồng quyền liên quan',
            'contract_number3' => 'Số thứ tự trong PL quyền liên quan',
            'contract_number4' => 'Số HĐ quyền tác giả',
            'contract_number5' => 'Số phụ lục HĐ quyền tác giả',
            'contract_number6' => 'Số thứ tự trong PL quyền tác giả',
            'start_time' => 'Thời gian phát hành',
            'end_time' => 'Thời hạn bản quyền',
            'music_id' => 'Music ID',
            'money' => 'Giá tiền',
            'note' => 'Ghi chú',
        ]);
        $music = Music::create($request->all());
        $id = 0;

        if (isset($request->ghi_am)&&$request->ghi_am) {
            $count = count(Contract::where('contract_number', $request->ghi_am)->pluck('id'));
            if ($count == 0) {
                $contract = Contract::create([
                    'contract_number' => $request->ghi_am,
                    'type' => 1,
                    'category_id' => 1,
                ]);
                ActionLog::writeActionLog($contract->id, $contract->getTable(), config("common.actions.created", "created"), null, "Tạo mới", null, null, null);
                $id = $contract->id;
            } else {
                $contract = Contract::where('contract_number', $request->ghi_am)->pluck('id');
                foreach ($contract as $value) {
                    $id = $value;
                }
            }
        }

        $music->contract_id=$id;
        $music->save();
        
        if (isset($request->tac_gia)&&$request->tac_gia) {
            $count = count(Contract::where('contract_number', $request->tac_gia)->pluck('id'));
            $contract = Contract::firstOrCreate([
                'contract_number' => $request->tac_gia,
            ],
            [
                'type' => 4,
                'category_id' => 1,
            ]);
            if($count==0)
                ActionLog::writeActionLog($contract->id, $contract->getTable(), config("common.actions.created", "created"), null, "Tạo mới", null, null, null);

        }

        if (isset($request->contract_number1)&&$request->contract_number1) {
            $count = Contract::where('contract_number', $request->contract_number1)->count();
            $contract = Contract::firstOrCreate([
                'contract_number' => $request->contract_number1,
            ],
            [
                'type' => 3,
                'category_id' => 1,
            ]);
            if($count==0)
                ActionLog::writeActionLog($contract->id, $contract->getTable(), config("common.actions.created", "created"), null, "Tạo mới", null, null, null);
        }

        if (isset($request->contract_number4)&&$request->contract_number4) {
            if(isset($request->contract_number1)&&$request->contract_number1==$request->contract_number4) {}
            else {
                $count = Contract::where('contract_number', $request->contract_number4)->count();
                $contract = Contract::firstOrCreate([
                    'contract_number' => $request->contract_number4,
                ],
                [
                    'type' => 3,
                    'category_id' => 1,
                ]);
                if($count==0)
                ActionLog::writeActionLog($contract->id, $contract->getTable(), config("common.actions.created", "created"), null, "Tạo mới", null, null, null);
            }
        }
        $log = ActionLog::writeActionLog($music->id, $music->getTable(), config("common.actions.created", "created"), get_class($this), "Tạo mới", null, null, null);
 
        return redirect()->route('media.music.index')
            ->with('success', 'Tạo nhạc thành công');
    }

    public function edit($id)
    {
        $data = Music::find($id);
        // $data->start_time = reFormatDate($data->start_time);
        // $data->end_time = reFormatDate($data->end_time);
        // $data->ghi_am=(Contract::find($data->contract_id))?Contract::find($data->contract_id)->contract_number:'';
        // $data->save();
//        var_dump($rolePermissions);die;
        $list_type = TypeFee::pluck('name', 'id');
        $admin_id = $data->admin_id;
        return view('admin.media.music.edit', compact('data', 'list_type'));
    }
    public function show($id)
    {
        $data = Music::find($id);
        $list_type = TypeFee::pluck('name', 'id');
        $admin_id = $data->admin_id;
        return view('admin.media.music.show', compact('data', 'list_type'));
    }

    public function update(Request $request, $id)
    {
        $check = '';
        $request->name = trim($request->name);
        $request->contract_number1 = trim($request->contract_number1);
        $request->contract_number4 = trim($request->contract_number4);
        $music = Music::find($id);
        $contract_id = (Contract::find($music->contract_id))?Contract::find($music->contract_id)->id:'';
        if (!empty($request->ghi_am)) {
            $check = Rule::unique('music')->where(
                function ($query) use ($music, $contract_id) {
                    $query->where('contract_id', $contract_id)->where('id', '<>', $music->id);
                });
        }
        $oldContractNumber1 = $music->contract_number1;
        $oldContractNumber4 = $music->contract_number4;
        $newContractNumber1 = $request->contract_number1;
        $newContractNumber4 = $request->contract_number4;
        $check1 = '';
        if($oldContractNumber1!=$newContractNumber1&&empty($oldContractNumber1)){
            if($newContractNumber1==$newContractNumber4&&$newContractNumber4==$oldContractNumber4){
                if(count(Contract::where('contract_number',$newContractNumber1)->get())){
                   // $check1 = 1; // trùng với contract number 4 và đã có 
                   $check1 = '';
                }
                // else $check1 = 2; // trùng với contractNumber 4 và chưa có contract number này
                else $check1 = 'nullable|max:1000|unique:contracts,contract_number,'. $id;
            }
            // else $check1 = 3; // contract mới hoàn toàn
            else $check1 = 'nullable|max:1000|unique:contracts,contract_number,'. $id;
        }
        $check2 = '';
        if($oldContractNumber4!=$newContractNumber4&&empty($oldContractNumber4)){
            if($newContractNumber1==$newContractNumber4&&$newContractNumber1==$oldContractNumber1){
                if(count(Contract::where('contract_number',$newContractNumber1)->get())){
                   // $check1 = 1; // trùng với contract number 4 và đã có 
                   $check2 = '';
                }
                // else $check1 = 2; // trùng với contractNumber 4 và chưa có contract number này
                else $check2 = 'nullable|max:1000|unique:contracts,contract_number,'. $id;
            }
            // else $check1 = 3; // contract mới hoàn toàn
            else $check2 = 'nullable|max:1000|unique:contracts,contract_number,'. $id;
        }

        $this->validate($request, [
            'name' => ['required', $check,
            ],
            'singer' => 'max:1000',
            'musician' => 'max:1000',
            'album' => 'max:1000',
            'ghi_am' => 'max:1000',
            'tac_gia' => 'max:1000',
            'contract_cps' => 'max:1000',
            'contract_number1' => 'nullable|my_contract|max:1000|type_contract:3|active_contract',
            'contract_number2' => 'max:1000',
            'contract_number3' => 'max:1000',
            'contract_number4' => 'nullable|my_contract|max:1000|type_contract:3|active_contract',
            'contract_number5' => 'max:1000',
            'contract_number6' => 'max:1000',
            'music_id' => 'max:1000',
            'start_time' => 'required',
            'money' => 'max:1000',
            'note' => 'max:1000',
            'end_time' => 'required|after:start_time',
            'type_fee_id' => 'required|max:10',
        ], [], [
            'name' => 'Tên bài hát',
            'type_fee_id' => 'Loại hợp đồng',
            'singer' => 'Ca sĩ',
            'album' => 'Album',
            'musician' => 'Nhạc sĩ',
            'ghi_am' => 'Quyền ghi âm',
            'tac_gia' => 'Quyền tác giả',
            'contract_cps' => 'Số phụ lục hợp đồng',
            'contract_number1' => 'Số hợp đồng quyền liên quan',
            'contract_number2' => 'Số phụ lục hợp đồng quyền liên quan',
            'contract_number3' => 'Số thứ tự trong PL quyền liên quan',
            'contract_number4' => 'Số HĐ quyền tác giả',
            'contract_number5' => 'Số phụ lục HĐ quyền tác giả',
            'contract_number6' => 'Số thứ tự trong PL quyền tác giả',
            'start_time' => 'Thời gian phát hành',
            'end_time' => 'Thời hạn bản quyền',
            'music_id' => 'Music ID',
            'money' => 'Giá tiền',
            'note' => 'Ghi chú',
        ]);
        $input = $request->all();
        $music = Music::find($id);
        $music->fill($input);
        $changes = $music->getDirty();

        $old_value = array();
        if(reFormatDate($music->getOriginal('start_time'))==$changes['start_time']) unset($changes['start_time']);
        if(reFormatDate($music->getOriginal('end_time'))==$changes['end_time']) unset($changes['end_time']);
        foreach ($changes as $key => $value) {
            $old_value[$key] = $music->getOriginal($key);
        }

        if(isset($changes['contract_number1']))
        {
            $count = Contract::where('contract_number', $changes['contract_number1'])->count();
            $contract = Contract::firstOrCreate([
                'contract_number' => $changes['contract_number1'],
            ],
            [
                'type' => 3,
                'category_id' => 1,
            ]);
            if($count==0)
                ActionLog::writeActionLog($contract->id, $contract->getTable(), config("common.actions.created", "created"), null, "Tạo mới", null, null, null);
        }
        if (isset($changes['contract_number4'])) {
            if(isset($changes['contract_number1'])&&$changes['contract_number1']==$changes['contract_number4']) {}
            else {
                $count = Contract::where('contract_number', $changes['contract_number4'])->count();
                $contract = Contract::firstOrCreate([
                    'contract_number' => $changes['contract_number4'],
                ],
                [
                    'type' => 3,
                    'category_id' => 1,

                ]);
                if($count==0)
                    ActionLog::writeActionLog($contract->id, $contract->getTable(), config("common.actions.created", "created"), null, "Tạo mới", null, null, null);
            }
        }

        if(isset($old_value['end_time'])&&$old_value['end_time']){
            $old_value['end_time'] = reFormatDate($old_value['end_time']);
        }
        if(isset($old_value['start_time'])&&$old_value['start_time']){
            $old_value['start_time'] = reFormatDate($old_value['start_time']);
        }
        // dd($changes);
        if(isset($changes['type_fee_id'])) {
            $changes['type_fee_id'] = TypeFee::find($changes['type_fee_id'])->name;
            $old_value['type_fee_id'] = TypeFee::find($old_value['type_fee_id'])->name;
        }

        //dd($old_value['name']);
        //dd($music->fill($input)->getOriginal()['name']);
        if ($request->cho_docquyen) {
            $music->cho_docquyen = 1;
        } else {
            $music->cho_docquyen = 0;
        }

        if ($request->cho_tacquyen) {
            $music->cho_tacquyen = 1;
        } else {
            $music->cho_tacquyen = 0;
        }

        if ($request->so_docquyen) {
            $music->so_docquyen = 1;
        } else {
            $music->so_docquyen = 0;
        }

        if ($request->so_tacquyen) {
            $music->so_tacquyen = 1;
        } else {
            $music->so_tacquyen = 0;
        }

        if ($request->sale_permission) {
            $music->sale_permission = 1;
        } else {
            $music->sale_permission = 0;
        }

        if ($request->type_MV) {
            $music->type_MV = 1;
        } else {
            $music->type_MV = 0;
        }

        if ($request->type_audio) {
            $music->type_audio = 1;
        } else {
            $music->type_audio = 0;
        }

        $music->save();
        if(isset($changes['start_time'])||isset($changes['end_time'])){
            ContractMediaSaleMoney::where('category_id',1)->where('media_id',$music->id)->update([
                'start_time' => $music->start_time,
                'end_time' => $music->end_time,
            ]);
        }
        if(!empty($changes)) {
            $log = ActionLog::writeActionLog($id, $music->getTable(), config("common.actions.updated", "updated"), get_class($this), "Sửa", $old_value, $changes, null);
        }
        return redirect()->route('media.music.index')
            ->with('success', 'Cập nhật thông tin nhạc thành công');
    }

    public function destroy($id)
    {
        Music::find($id)->delete();
        return redirect()->route('media.music.index')
            ->with('success', 'Deleted Successfully');
    }

    public function upload(Request $req)
    {
        ini_set('memory_limit', '-1');
        set_time_limit(0);

        $validator = Validator::make($req->all(), [
            'file' => 'required|mimes:xlsx,xls',
        ], [
            'file.required' => 'Chưa có file excel',
            'file.mimes' => 'Định dạng không hợp lệ',
        ]);

        if ($validator->passes()) {
            $file = $req->file('file');
            $adminID = Auth::user()->id;
            $isManager = Auth::user()->is_manager;
            $data = Excel::toArray(new EmployeeImport, $req->file);
            $data = $data[0];
            $dem = 0;
            if(!$data){
                return redirect()->route('media.music.index')->with('error', 'Thêm mới từ file ' . $file->getClientOriginalName() . ' thất bại. File không đúng dịnh dạng mẫu');               
            }
            for($i=count($data[0])-1; $i--; $i>0){
                if(!trim($data[0][$i])){
                    $dem++;
                }
                else break;
            }
            if (trim($data[0][0]) == 'STT'&&count($data[0])-$dem >= 26 && count($data[0])-$dem<=29) {
                Excel::import(new MusicUploadImport, $req->file);
                sleep(1);
                $error = "";
                if($isManager) {
                    $contracts = DB::table('contracts')->where(function($query){
                        $query->where('type',1)->where('category_id',1);
                        $query->orwhere('type',4);
                    })->select('contract_number')->get(); 
                    $contractsDoiTac = DB::table('contracts')->where('type',3)->select('contract_number')->get();
                    // $contractsTacGia = DB::table('contracts')->where(function($query){
                    //     $query->where('type',1)->where('category_id',1);
                    //     $query->orwhere('type',4);
                    // })->select('contract_number')->get(); 
                }
                else {
                    $contracts = DB::table('contracts')->where(function($query){
                        $query->where('type',1)->where('category_id',1);
                        $query->orwhere('type',4);
                    })->where('admin_id',$adminID)->select('contract_number')->get(); 
                    $contractsDoiTac = DB::table('contracts')->where('type',3)->where('admin_id',$adminID)->select('contract_number')->get();
                    // $contractsTacGia = DB::table('contracts')->where(function($query){
                    //     $query->where('type',1)->where('category_id',1);
                    //     $query->orwhere('type',4);
                    // })->where('admin_id',$adminID)->select('contract_number')->get(); 
                }
                $contracts = json_decode(json_encode($contracts), true);
                $contractsDoiTac = json_decode(json_encode($contractsDoiTac), true);
                // $contractsTacGia = json_decode(json_encode($contractsTacGia), true);

                // $ContractOFOthers = DB::table('contracts')->where('type',1)->where('category_id',1)->where('admin_id','!=',$adminID)->pluck('contract_number')->toArray(); 
                // $ContractOFOthers = array_map('mb_strtolower', $ContractOFOthers);       
                $ContractOFOthers1 = DB::table('contracts')->where('type',3)->where('admin_id','!=',$adminID)->pluck('contract_number')->toArray();  
                $ContractOFOthers1 = array_map('mb_strtolower', $ContractOFOthers1);             
                $ContractOFOthers = DB::table('contracts')->where(function($query){
                    $query->where('type',1)->where('category_id',1);
                    $query->orwhere('type',4);
                })->where('admin_id','!=',$adminID)->pluck('contract_number')->toArray(); 
                $ContractOFOthers = array_map('mb_strtolower', $ContractOFOthers);   
                   
                // $ContractOFDifferentType = DB::table('contracts')->where(function ($query) {
                //                                 $query->where('type', '!=', 1);
                //                                 $query->orWhere('category_id', '=', 2);
                //                             })->pluck('contract_number')->toArray();  
                // $ContractOFDifferentType = array_map('mb_strtolower', $ContractOFDifferentType);
                $ContractOFDifferentType1 = DB::table('contracts')->where('type','!=',3)->pluck('contract_number')->toArray();    
                $ContractOFDifferentType1 = array_map('mb_strtolower', $ContractOFDifferentType1);
                $ContractOFDifferentType = DB::table('contracts')->where(function ($query) {
                                                $query->where('type', '!=', 1);
                                                $query->orWhere('category_id', '=', 2);
                                            })
                                            ->where('type','!=',4)->pluck('contract_number')->toArray();  
                $ContractOFDifferentType = array_map('mb_strtolower', $ContractOFDifferentType);

                $contractDB = [];  
                $contractTacGia = [];   
                // 
                $musics = DB::table('music')->leftJoin('contracts', 'music.contract_id', '=', 'contracts.id')->where(function ($query) {
                        $query->where('contract_id', '=', null);
                        $query->orWhere('contracts.category_id', '=', 1);
                    })
                    ->select('music.name as ten_bai_hat', 'contracts.contract_number as quyen_ghi_am')->get();
                $musics = json_decode(json_encode($musics), true);
                $upload = DB::table('music_upload')
                    ->where('status', '0')
                    ->where('admin_id', $adminID)
                    ->select('ten_bai_hat', 'quyen_ghi_am')
                    ->get();
                $upload = json_decode(json_encode($upload), true);
                $countNumberMusic = count($upload);
                $musicUpload = DB::table('music_upload')
                    ->where('status', '0')
                    ->where('admin_id', $adminID)
                    ->get();
                DB::table('music_upload')
                    ->where('status', '0')
                    ->where('admin_id', $adminID)
                    ->delete();
                foreach($musicUpload as $key=>$value){
                    if($value->ten_bai_hat&&$value->quyen_ghi_am&&!in_array($value->quyen_ghi_am,$contractDB )){
                        $contractDB[$key] =  Str::lower($value->quyen_ghi_am); 
                    }
                    if($value->ten_bai_hat&&$value->quyen_tac_gia&&!in_array($value->quyen_tac_gia,$contractTacGia )){
                        $contractTacGia[$key] = Str::lower($value->quyen_tac_gia);
                    }
                }
                // dd($contractDB, $contractTacGia);
                // check lỗi dữ liệu để gửi về
                $checkType = 1;
                $checkTypeFee = 1;
                $checkStartTime = 1;
                $checkEndTime = 1;
                $checkMoney = 1;
                $checkValidateTime = 1;
                $errorStartTime = '';
                $errorEndTime = '';
                $errorValidateTime = '';
                $errorType = '';
                $errorTypeFee = '';
                $errorMoney = '';
                $errorContractNumber = '';
                $errorContractNumber1 = '';
                $errorContractNumber2 = '';
                $errorContractNumber3 = '';
                $typeFee = DB::table('type_fee')->pluck('name','id')->toArray();
                foreach($typeFee as $key => $value){
                    $typeFee[$key] = Str::title($value);
                }
                foreach ($musicUpload as $key => $value) {
                    $value->gia_tien = str_replace(',', '', $value->gia_tien);
                    $value->gia_tien = str_replace('.', '', $value->gia_tien);
                    if($value->ten_bai_hat){
                        if(!$value->time_release){
                            if($checkStartTime){
                                $checkStartTime = 0;
                                $errorStartTime = $errorStartTime.' '.($key+1);
                            }
                            else $errorStartTime = $errorStartTime.', '.($key+1);
                        }
                        if(!$value->time_end){
                            if($checkEndTime){
                                $checkEndTime = 0;
                                $errorEndTime = $errorEndTime.' '.($key+1);
                            }
                            else $errorEndTime = $errorEndTime.', '.($key+1);
                        }
                        if(!$value->loai_hd){
                            if($checkType){
                                $checkType = 0;
                                $errorType = $errorType.' '.($key+1);
                            }
                            else $errorType = $errorType.', '.($key+1);
                        }
                        // dd($value->loai_hd,array_search($value->loai_hd,$typeFee), $typeFee );
                        if($value->loai_hd&&!array_search(Str::title($value->loai_hd),$typeFee)){
                            if($checkTypeFee){
                                $checkTypeFee = 0;
                                $errorTypeFee = $errorTypeFee.' '.($key+1);
                            }
                            else $errorTypeFee = $errorTypeFee.', '.($key+1);
                        }
                        if($value->gia_tien!=null&&!is_numeric($value->gia_tien)){
                            if($checkMoney){
                                $checkMoney = 0;
                                $errorMoney = $errorMoney.' '.($key+1);
                            }
                            else $errorMoney = $errorMoney.', '.($key+1);
                        }
                        if($value->time_release&&$value->time_end&&strtotime($value->time_release)>=strtotime($value->time_end)){
                            if($checkValidateTime){
                                $checkValidateTime = 0;
                                $errorValidateTime = $errorValidateTime.' '.($key+1);
                            }
                            else $errorValidateTime = $errorValidateTime.', '.($key+1);
                        }
                    }
                }

                $index = array();
                $checkFirst = 1;
                foreach($musicUpload as $key => $value){
                    $index[$key] = 1;
                    $value->gia_tien = str_replace(',', '', $value->gia_tien);
                    $value->gia_tien = str_replace('.', '', $value->gia_tien);
                    if(!$value->ten_bai_hat||!$value->loai_hd||!$value->time_release||!$value->time_end||($value->gia_tien!=null&&!is_numeric($value->gia_tien))||!array_search(Str::title($value->loai_hd),$typeFee)){
                        $index[$key] = 0;
                        if($value->ten_bai_hat){
                            if($checkFirst){
                                $checkFirst = 0;
                                $error = $error.'Lỗi dữ liệu bản ghi:';
                                $error = $error.' '.($key+1);
                            }
                            else $error = $error.', '.($key+1);
                            // dd($value->ten_bai_hat,$value->loai_hd,$value->time_release,$value->time_end,is_numeric($value->gia_tien), $value->gia_tien);
                        }
                        continue;
                    }
                    if(strtotime($value->time_release)>=strtotime($value->time_end)){
                        $index[$key] = 0;
                        if($checkFirst){
                            $checkFirst = 0;
                            $error = $error.'Lỗi dữ liệu bản ghi:';
                            $error = $error.' '.($key+1);
                        }
                        else $error = $error.', '.($key+1);
                        continue;                        
                    }
                }
                if(!$isManager){
                    $checkFirst = 1;
                    foreach($musicUpload as $key => $value){
                        $kt=1;
                        if($value->ten_bai_hat){
                            if($value->quyen_ghi_am&&in_array(Str::lower($value->quyen_ghi_am), $ContractOFOthers)){
                                $kt = 0;
                                if($checkFirst){
                                    $checkFirst = 0;
                                    if($error) $error = $error.". Số hợp đồng đã tồn tại và thuộc quản lý của một người dùng khác:";
                                    else $error = $error."Số hợp đồng đã tồn tại và thuộc quản lý của một người dùng khác:";
                                    $errorContractNumber = $errorContractNumber.' '.($key+1);
                                    $error = $error.' '.($key+1);
                                }
                                else{
                                    $errorContractNumber = $errorContractNumber.', '.($key+1);
                                    $error = $error.', '.($key+1);
                                } 
                            }
                        }
                        $index[$key] = (!$index[$key])?0:$kt;
                    }

                    $checkFirst = 1;
                    foreach($musicUpload as $key => $value){
                        $kt=1;
                        if($value->ten_bai_hat){
                            if($value->so_hd_lq&&in_array(Str::lower($value->so_hd_lq), $ContractOFOthers1)){
                                $kt = 0;
                                if($checkFirst){
                                    $checkFirst = 0;
                                    if($error) $error = $error.". Số HĐ quyền liên quan đã tồn tại và thuộc quản lý của một người dùng khác:";
                                    else $error = $error."Số HĐ quyền liên quan đã tồn tại và thuộc quản lý của một người dùng khác:";
                                    $errorContractNumber1 = $errorContractNumber1.' '.($key+1);
                                    $error = $error.' '.($key+1);
                                }
                                else{
                                    $errorContractNumber1 = $errorContractNumber1.', '.($key+1);
                                    $error = $error.', '.($key+1);
                                } 
                            }
                        }
                        $index[$key] = (!$index[$key])?0:$kt;
                    }

                    $checkFirst = 1;
                    foreach($musicUpload as $key => $value){
                        $kt=1;
                        if($value->ten_bai_hat){
                            if($value->hd_tac_gia&&in_array(Str::lower($value->hd_tac_gia), $ContractOFOthers1)){
                                $kt = 0;
                                if($checkFirst){
                                    $checkFirst = 0;
                                    if($error) $error = $error.". Số HĐ quyền tác giả đã tồn tại và thuộc quản lý của một người dùng khác:";
                                    else $error = $error."Số HĐ quyền tác giả đã tồn tại và thuộc quản lý của một người dùng khác:";
                                    $errorContractNumber2 = $errorContractNumber2.' '.($key+1);
                                    $error = $error.' '.($key+1);
                                }
                                else{
                                    $errorContractNumber2 = $errorContractNumber2.', '.($key+1);
                                    $error = $error.', '.($key+1);
                                } 
                            }
                        }
                        $index[$key] = (!$index[$key])?0:$kt;
                    }

                    $checkFirst = 1;
                    foreach($musicUpload as $key => $value){
                        $kt=1;
                        if($value->ten_bai_hat){
                            if($value->quyen_tac_gia&&in_array(Str::lower($value->quyen_tac_gia), $ContractOFOthers)){
                                $kt = 0;
                                if($checkFirst){
                                    $checkFirst = 0;
                                    if($error) $error = $error.". Quyền tác giả đã tồn tại và thuộc quản lý của một người dùng khác:";
                                    else $error = $error."Quyền tác giả đã tồn tại và thuộc quản lý của một người dùng khác:";
                                    $errorContractNumber3 = $errorContractNumber3.' '.($key+1);
                                    $error = $error.' '.($key+1);
                                }
                                else{
                                    $errorContractNumber3 = $errorContractNumber3.', '.($key+1);
                                    $error = $error.', '.($key+1);
                                } 
                            }
                        }
                        $index[$key] = (!$index[$key])?0:$kt;
                    }
                }
                $checkFirst = 1;
                $errorContractNumberType = '';
                foreach($musicUpload as $key => $value){
                    $kt=1;
                    if($value->ten_bai_hat){
                        if($value->quyen_ghi_am&&in_array(Str::lower($value->quyen_ghi_am), $ContractOFDifferentType)){
                            $kt = 0;
                            if($checkFirst){
                                $checkFirst = 0;
                                if($error) $error = $error.". Số hợp đồng đã tồn tại và không phải hợp đồng mua nhạc:";
                                else $error = $error."Số hợp đồng đã tồn tại và không phải hợp đồng mua nhạc:";
                                $errorContractNumberType = $errorContractNumberType.' '.($key+1);
                                $error = $error.' '.($key+1);
                            }
                            else{
                                $errorContractNumberType = $errorContractNumberType.', '.($key+1);
                                $error = $error.', '.($key+1);
                            } 
                        }
                    }
                    $index[$key] = (!$index[$key])?0:$kt;
                }
                $checkFirst = 1;
                $errorContractNumberType1= '';
                foreach($musicUpload as $key => $value){
                    $kt=1;
                    if($value->ten_bai_hat){
                        if($value->so_hd_lq&&in_array(Str::lower($value->so_hd_lq), $ContractOFDifferentType1)){
                            $kt = 0;
                            if($checkFirst){
                                $checkFirst = 0;
                                if($error) $error = $error.".Số hợp đồng đã tồn tại và không phải hợp đồng đối tác:";
                                else $error = $error."Số hợp đồng đã tồn tại và không phải hợp đồng đối tác:";
                                $errorContractNumberType1 = $errorContractNumberType1.' '.($key+1);
                                $error = $error.' '.($key+1);
                            }
                            else{
                                $errorContractNumberType1 = $errorContractNumberType1.', '.($key+1);
                                $error = $error.', '.($key+1);
                            } 
                        }
                    }
                    $index[$key] = (!$index[$key])?0:$kt;
                }
                $checkFirst = 1;
                $errorContractNumberType2 = '';
                foreach($musicUpload as $key => $value){
                    $kt=1;
                    if($value->ten_bai_hat){
                        if($value->hd_tac_gia&&in_array(Str::lower($value->hd_tac_gia), $ContractOFDifferentType1)){
                            $kt = 0;
                            if($checkFirst){
                                $checkFirst = 0;
                                if($error) $error = $error.". Số hợp đồng đã tồn tại và không phải hợp đồng đối tác:";
                                else $error = $error."Số hợp đồng đã tồn tại và không phải hợp đồng đối tác:";
                                $errorContractNumberType2 = $errorContractNumberType2.' '.($key+1);
                                $error = $error.' '.($key+1);
                            }
                            else{
                                $errorContractNumberType2 = $errorContractNumberType2.', '.($key+1);
                                $error = $error.', '.($key+1);
                            } 
                        }
                    }
                    $index[$key] = (!$index[$key])?0:$kt;
                }
                $checkFirst = 1;
                $errorContractNumberType3 = '';
                foreach($musicUpload as $key => $value){
                    $kt=1;
                    if($value->ten_bai_hat){
                        if($value->quyen_tac_gia&&in_array(Str::lower($value->quyen_tac_gia), $ContractOFDifferentType)){
                            $kt = 0;
                            if($checkFirst){
                                $checkFirst = 0;
                                if($error) $error = $error.". Số hợp đồng đã tồn tại và không phải hợp đồng tác giả:";
                                else $error = $error."Số hợp đồng đã tồn tại và không phải hợp đồng tác giả:";
                                $errorContractNumberType3 = $errorContractNumberType3.' '.($key+1);
                                $error = $error.' '.($key+1);
                            }
                            else{
                                $errorContractNumberType3 = $errorContractNumberType3.', '.($key+1);
                                $error = $error.', '.($key+1);
                            } 
                        }
                    }
                    $index[$key] = (!$index[$key])?0:$kt;
                }
                // check trùng tên trong file excel
                $checkFirst = 1;
                $errorUniqueContract1 = '';
                foreach($musicUpload as $key => $value){
                    $kt=1;
                    if($value->ten_bai_hat){
                        if($value->so_hd_lq&&in_array(Str::lower($value->so_hd_lq), $contractDB)){
                            $vitri = array_search(Str::lower($value->so_hd_lq), $contractDB);
                            $kt = 0;
                            if($checkFirst){
                                $checkFirst = 0;
                                if($error) $error = $error.". Quyền liên quan và Số HĐ quyền liên quan trùng nhau trong file excel:";
                                else $error = $error."Quyền liên quan và Số HĐ quyền liên quan trùng nhau trong file excel:";
                                $errorUniqueContract1 = $errorUniqueContract1.'('.($vitri+1).','.($key+1).')';
                                $error = $error.'('.($vitri+1).','.($key+1).')';
                            }
                            else{
                                $errorUniqueContract1 = $errorUniqueContract1.'; ('.($vitri+1).','.($key+1).')';
                                $error = $error.'; ('.($vitri+1).','.($key+1).')';
                            } 
                        }
                    }
                    $index[$key] = (!$index[$key])?0:$kt;
                }
                $checkFirst = 1;
                $errorUniqueContract2 = '';
                foreach($musicUpload as $key => $value){
                    $kt=1;
                    if($value->ten_bai_hat){
                        if($value->hd_tac_gia&&in_array(Str::lower($value->hd_tac_gia), $contractDB)){
                            $vitri = array_search(Str::lower($value->hd_tac_gia), $contractDB);
                            $kt = 0;
                            if($checkFirst){
                                $checkFirst = 0;
                                if($error) $error = $error.". Quyền liên quan và Số HĐ quyền tác giả trùng nhau trong file excel:";
                                else $error = $error."Quyền liên quan và Số HĐ quyền liên quan trùng nhau trong file excel:";
                                $errorUniqueContract2 = $errorUniqueContract2.'('.($vitri+1).','.($key+1).')';
                                $error = $error.'('.($vitri+1).','.($key+1).')';
                            }
                            else{
                                $errorUniqueContract2 = $errorUniqueContract2.'; ('.($vitri+1).','.($key+1).')';
                                $error = $error.'; ('.($vitri+1).','.($key+1).')';
                            } 
                        }
                    }
                    $index[$key] = (!$index[$key])?0:$kt;
                }


                $checkFirst = 1;
                $errorUniqueContract3 = '';
                foreach($musicUpload as $key => $value){
                    $kt=1;
                    if($value->ten_bai_hat){
                        if($value->so_hd_lq&&in_array(Str::lower($value->so_hd_lq), $contractTacGia)){
                            $vitri = array_search(Str::lower($value->so_hd_lq), $contractTacGia);
                            $kt = 0;
                            if($checkFirst){
                                $checkFirst = 0;
                                if($error) $error = $error.". Quyền tác giả và Số HĐ quyền liên quan trùng nhau trong file excel:";
                                else $error = $error."Quyền tác giả và Số HĐ quyền liên quan trùng nhau trong file excel:";
                                $errorUniqueContract3 = $errorUniqueContract3.'('.($vitri+1).','.($key+1).')';
                                $error = $error.'('.($vitri+1).','.($key+1).')';
                            }
                            else{
                                $errorUniqueContract3 = $errorUniqueContract3.'; ('.($vitri+1).','.($key+1).')';
                                $error = $error.'; ('.($vitri+1).','.($key+1).')';
                            } 
                        }
                    }
                    $index[$key] = (!$index[$key])?0:$kt;
                }
                $checkFirst = 1;
                $errorUniqueContract4 = '';
                foreach($musicUpload as $key => $value){
                    $kt=1;
                    if($value->ten_bai_hat){
                        if($value->hd_tac_gia&&in_array(Str::lower($value->hd_tac_gia), $contractTacGia)){
                            $vitri = array_search(Str::lower($value->hd_tac_gia), $contractTacGia);
                            $kt = 0;
                            if($checkFirst){
                                $checkFirst = 0;
                                if($error) $error = $error.". Quyền tác giả và Số HĐ quyền tác giả trùng nhau trong file excel:";
                                else $error = $error."Quyền tác giả và Số HĐ quyền liên quan trùng nhau trong file excel:";
                                $errorUniqueContract4 = $errorUniqueContract4.'('.($vitri+1).','.($key+1).')';
                                $error = $error.'('.($vitri+1).','.($key+1).')';
                            }
                            else{
                                $errorUniqueContract4 = $errorUniqueContract4.'; ('.($vitri+1).','.($key+1).')';
                                $error = $error.'; ('.($vitri+1).','.($key+1).')';
                            } 
                        }
                    }
                    $index[$key] = (!$index[$key])?0:$kt;
                }




                $count = count($upload);
                $duplicate = [];
                // check trùng bên trong bảng upload
                $errorUpload = '';
                $checkFirst = 1;
                for ($i = 0; $i < $count; $i++) {
                    if($upload[$i]['ten_bai_hat']){
                        $kt = 1;
                        $check = 1;
                        for ($j = 0; $j < $count; $j++) {
                            if($upload[$j]['ten_bai_hat']){
                                if (array_map('mb_strtolower', $upload[$i]) == array_map('mb_strtolower', $upload[$j]) && $i != $j) {
                                    $kt = 0;
                                    if(!in_array($i,$duplicate)&&$check){
                                        array_push($duplicate,$i);                                        
                                        $check = 0;
                                        if($checkFirst) {
                                            $errorUpload = $errorUpload.' ('.($i+1);
                                            if($error) $error = $error.". Trùng bản ghi trong file:";
                                            else $error = $error."Trùng bản ghi trong file:";
                                            $checkFirst = 0;
                                            $error = $error.' ('.($i+1);
                                        }
                                        else {
                                            $errorUpload = $errorUpload.'; ('.($i+1);
                                            $error = $error.'; ('.($i+1);
                                        }
                                    }

                                    if(!in_array($j,$duplicate)){
                                        array_push($duplicate,$j);
                                        $errorUpload = $errorUpload.', '.($j+1);
                                        $error = $error.', '.($j+1);
                                    }
                                    $index[$j] = (!$index[$j])?0:$kt;
                                }
                            }
                        }
                        if($check==0) {
                            $errorUpload = $errorUpload.')';
                            $error = $error.')';
                        }
                        $index[$i] = (!$index[$i])?0:$kt;
                    }
                }
                // check trùng với bảng film
                $checkFirst = 1;
                $errorDB = '';
                foreach ($upload as $key => $value) {
                    // if($key==2) dd($value);
                    if($value['ten_bai_hat']){
                        $kt = 1;
                        foreach ($musics as $keyFilm => $music) {
                            if (array_map('mb_strtolower', $value) == array_map('mb_strtolower', $music)) {
                                if($checkFirst){
                                    $errorDB = $errorDB.' '.($key+1);
                                    if($error) $error = $error.". Trùng bản ghi trong DB:";
                                    else $error = $error."Trùng bản ghi trong DB:";
                                    $error = $error.' '.($key+1);
                                    $checkFirst = 0;
                                }
                                else {
                                    $errorDB = $errorDB.', '.($key+1);
                                    $error = $error.', '.($key+1);
                                }
                                $kt = 0;
                                break;
                            }
                        }
                        $index[$key] = (!$index[$key])?0:$kt;
                    }
                }
                if($error){
                    $error1 = [];
                    array_push($error1, [
                        'errorUpload' => $errorUpload
                    ]);
                    array_push($error1, [
                        'errorDB' => $errorDB
                    ]);
                    array_push($error1, [
                        'errorStartTime' => $errorStartTime
                    ]);
                    array_push($error1, [
                        'errorEndTime' => $errorEndTime
                    ]);
                    array_push($error1, [
                        'errorValidateTime' => $errorValidateTime
                    ]);
                    array_push($error1, [
                        'errorType' => $errorType
                    ]);
                    array_push($error1, [
                        'errorTypeFee' => $errorTypeFee
                    ]);
                    array_push($error1, [
                        'errorMoney' => $errorMoney
                    ]);
                    array_push($error1, [
                        'errorContractNumber' => $errorContractNumber
                    ]);
                    array_push($error1, [
                        'errorContractNumber1' => $errorContractNumber1
                    ]);
                    array_push($error1, [
                        'errorContractNumber2' => $errorContractNumber2
                    ]);
                    array_push($error1, [
                        'errorContractNumber3' => $errorContractNumber3
                    ]);
                    array_push($error1, [
                        'errorContractNumberType' => $errorContractNumberType
                    ]);
                    array_push($error1, [
                        'errorContractNumberType1' => $errorContractNumberType1
                    ]);
                    array_push($error1, [
                        'errorContractNumberType2' => $errorContractNumberType2
                    ]);
                    array_push($error1, [
                        'errorContractNumberType3' => $errorContractNumberType3
                    ]);
                    array_push($error1, [
                        'errorUniqueContract1' => $errorUniqueContract1
                    ]);
                    array_push($error1, [
                        'errorUniqueContract2' => $errorUniqueContract2
                    ]);
                    array_push($error1, [
                        'errorUniqueContract3' => $errorUniqueContract3
                    ]);
                    array_push($error1, [
                        'errorUniqueContract4' => $errorUniqueContract4
                    ]);
                    return redirect()->route('media.music.index')->with('errorVersion2', 'Thêm mới từ file ' . $file->getClientOriginalName() . ' thất bại. '.$error)->with('outputError', json_encode($error1));
                }
                foreach ($index as $key => $value) {
                    if (!$index[$key]) {
                        // dd('đã vào');
                        unset($musicUpload[$key]);
                    }
                }
                if(!count($musicUpload)){
                    return redirect()->route('media.music.index')->with('error', 'Thêm mới từ file ' . $file->getClientOriginalName() . ' thất bại. Thông tin trên file Import rỗng');
                }
                // dd($contracts);
                for($i = 0; $i < count($contracts); $i++){
                    $contracts[$i]['contract_number'] = Str::lower($contracts[$i]['contract_number']);
                }
                for($i = 0; $i < count($contractsDoiTac); $i++){
                    $contractsDoiTac[$i]['contract_number'] = Str::lower($contractsDoiTac[$i]['contract_number']);
                }
                // for($i = 0; $i < count($contractsTacGia); $i++){
                //     $contractsTacGia[$i]['contract_number'] = Str::lower($contractsTacGia[$i]['contract_number']);
                // }
                $newContracts = [];
                $newContracts1 = [];
                $newContractsTacGia = [];
                $newContractsTacGia1 = [];
                $newContractsDoiTac = [];
                $newContractsDoiTac1 = [];
                foreach ($musicUpload as $key => $music) {
                    if($music->quyen_ghi_am){
                        $newMusic = [
                            'contract_number' => Str::lower($music->quyen_ghi_am),
                        ];
                        if (!in_array($newMusic, $newContracts1)&&!in_array($newMusic, $contracts)) {
                            array_push($newContracts1, $newMusic);
                            array_push($newContracts, [
                                'contract_number' => $music->quyen_ghi_am,
                            ]);
                        }
                    }
                    if($music->hd_tac_gia){
                        $newMusic = [
                            'contract_number' => Str::lower($music->hd_tac_gia),
                        ];
                        if (!in_array($newMusic, $newContractsDoiTac1)&&!in_array($newMusic, $contractsDoiTac)) {
                            array_push($newContractsDoiTac1, $newMusic);
                            array_push($newContractsDoiTac, [
                                'contract_number' => $music->hd_tac_gia,
                                'admin_id' => $adminID,
                                'type' => 3,
                                'category_id' => 1,
                            ]);
                        }                        
                    }                  
                    if($music->so_hd_lq){
                        $newMusic = [
                            'contract_number' => Str::lower($music->so_hd_lq),
                        ];
                        if (!in_array($newMusic, $newContractsDoiTac1)&&!in_array($newMusic, $contractsDoiTac)) {
                            array_push($newContractsDoiTac1, $newMusic);
                            array_push($newContractsDoiTac, [
                                'contract_number' => $music->so_hd_lq,
                                'admin_id' => $adminID,
                                'type' => 3,
                                'category_id' => 1,
                            ]);
                        }                        
                    }
                }
                foreach ($musicUpload as $key => $music) {
                    if($music->quyen_tac_gia){
                        $newMusic = [
                            'contract_number' => Str::lower($music->quyen_tac_gia),
                        ];
                        if (!in_array($newMusic, $newContractsTacGia1)&&!in_array($newMusic, $contracts)&&!in_array($newMusic, $newContracts1)) {
                            array_push($newContractsTacGia1, $newMusic);
                            array_push($newContractsTacGia, [
                                'contract_number' => $music->quyen_tac_gia,
                                'admin_id' => $adminID,
                                'type' => 4,
                                'category_id' => 1,
                            ]);
                        }
                    }
                }
                // Thêm admin ID, thêm type contract
                $purchase_contract = 1;

                for ($i = 0; $i < count($newContracts); $i++) {
                    $newContracts[$i]['admin_id'] = $adminID;
                    $newContracts[$i]['type'] = $purchase_contract;
                    $newContracts[$i]['category_id'] = 1;
                }
                foreach (array_chunk($newContracts, 1000) as $t) {
                    DB::table('contracts')->insert($t);
                }
                foreach (array_chunk($newContractsTacGia, 1000) as $t) {
                    DB::table('contracts')->insert($t);
                }
                foreach (array_chunk($newContractsDoiTac, 1000) as $t) {
                    DB::table('contracts')->insert($t);
                }
                $count_partner = count($newContractsDoiTac);
                // ddd
                sleep(1);
                if($isManager){
                    // $ContractsAfterInsert = DB::table('contracts')->where('type',1)->where('category_id',1)->pluck('contract_number', 'id')->toArray();
                    $contractsDoiTac = DB::table('contracts')->where('type',3)->pluck('contract_number','id')->toArray();   
                    $ContractsAfterInsert = DB::table('contracts')->where(function($query){
                        $query->where('type',1)->where('category_id',1);
                        $query->orwhere('type',4);
                    })->pluck('contract_number','id')->toArray();        
                }
                else {
                    // $ContractsAfterInsert = DB::table('contracts')->where('type',1)->where('admin_id',$adminID)->where('category_id',1)->pluck('contract_number', 'id')->toArray();
                    $contractsDoiTac = DB::table('contracts')->where('type',3)->where('admin_id',$adminID)->pluck('contract_number','id')->toArray();   
                    $ContractsAfterInsert = DB::table('contracts')->where(function($query){
                        $query->where('type',1)->where('category_id',1);
                        $query->orwhere('type',4);
                    })->where('admin_id',$adminID)->pluck('contract_number','id')->toArray();
                }
                $ContractsAfterInsert1 = array_map('mb_strtolower',$ContractsAfterInsert);
                $contractsDoiTac1 = array_map('mb_strtolower',$contractsDoiTac);
                // $contractsTacGia1 = array_map('mb_strtolower',$contractsTacGia);
                $arrayMusic = [];
                // dd($contractsDoiTac, $contractsDoiTac1);
                foreach ($musicUpload as $key => $music2) {
                    $music = [];
                    $contract_id = array_search(Str::lower($music2->quyen_ghi_am), $ContractsAfterInsert1);
                    if (!$contract_id||$music2->quyen_ghi_am==null) {
                        $contract_id = null;
                    }
                    // hợp đồng đối tác và hợp đồng tác giả 
                    $doitac1 = '';
                    if($music2->hd_tac_gia) $doitac1 = array_search(Str::lower($music2->hd_tac_gia), $contractsDoiTac1);
                    if($doitac1) $doitac1 = $contractsDoiTac[$doitac1];
                    // dd($doitac1);
                    $music['contract_number4'] = $doitac1;
                    $doitac1 = '';
                    if($music2->so_hd_lq) $doitac1 = array_search(Str::lower($music2->so_hd_lq), $contractsDoiTac1);
                    if($doitac1) $doitac1 = $contractsDoiTac[$doitac1];
                    $music['contract_number1'] = $doitac1;
                    $doitac1 = '';
                    if($music2->quyen_tac_gia) $doitac1 = array_search(Str::lower($music2->quyen_tac_gia), $ContractsAfterInsert1);
                    if($doitac1) $doitac1 = $ContractsAfterInsert[$doitac1];
                    $music['tac_gia'] = $doitac1;

                    $music['name'] = $music2->ten_bai_hat;
                    $music['singer'] = $music2->ca_si;
                    $music['musician'] = $music2->nhac_si;
                    $music['album'] = $music2->album;
                    if ($music2->doc_quyen) {
                        $music['cho_docquyen'] = 1;
                    } else {
                        $music['cho_docquyen'] = 0;
                    }
                    if ($music2->tac_quyen) {
                        $music['cho_tacquyen'] = 1;
                    } else {
                        $music['cho_tacquyen'] = 0;
                    }
                    if ($music2->doc_quyen_so) {
                        $music['so_docquyen'] = 1;
                    } else {
                        $music['so_docquyen'] = 0;
                    }
                    if ($music2->tac_quyen_so) {
                        $music['so_tacquyen'] = 1;
                    } else {
                        $music['so_tacquyen'] = 0;
                    }
                    if ($music2->ban_doi_tac_3) {
                        $music['sale_permission'] = 1;
                    } else {
                        $music['sale_permission'] = 0;
                    }
                    $music['type_fee_id'] = array_search(Str::title($music2->loai_hd), $typeFee);
                    $music['ghi_am'] = $music2->quyen_ghi_am;
                    // $music['tac_gia'] = $music2->quyen_tac_gia;
                    $music['contract_cps'] = $music2->phu_luc_hd;
                    $music['money'] = (int) $music2->gia_tien;
                    // $music['contract_number1'] = $music2->so_hd_lq;
                    $music['contract_number2'] = $music2->pl_hd;
                    $music['contract_number3'] = $music2->stt_phu_luc;
                    // $music['contract_number4'] = $music2->hd_tac_gia;
                    $music['contract_number5'] = $music2->pl_quyen_tac_gia;
                    $music['contract_number6'] = $music2->stt_pl_quyen_tac_gia;

                    $music['start_time'] = $music2->time_release;
                    $music['end_time'] = $music2->time_end;
                    if ($music2->mv) {
                        $music['type_MV'] = 1;
                    } else {
                        $music['type_MV'] = 0;
                    }
                    if ($music2->audio) {
                        $music['type_audio'] = 1;
                    } else {
                        $music['type_audio'] = 0;
                    }
                    $music['note'] = $music2->note;
                    $music['contract_id'] = $contract_id;
                    $music['music_id'] = $music2->id_ms;
                    $music['admin_id'] = $adminID;
                    $music['warning_status'] = 3; //status new;
                    array_push($arrayMusic, $music);
                }
                // DB::table('music')->insert($arrayMusic);
                foreach (array_chunk($arrayMusic, 1000) as $t) {
                    DB::table('music')->insertOrIgnore($t);
                }
                // $count_partner = $this->insertContractPartner();

                $fileName = time().'_'.$file->getClientOriginalName();
                $link = $file->storeAs(config("common.save.path"), $fileName, config("common.save.diskType"));
                ActionLog::writeActionLog(null, 'music', config("common.actions.created", "created"), null, "Tải lên", null,null, $link);
                return redirect()->route('media.music.index')->with('success', 'Thêm mới từ file ' . $file->getClientOriginalName() . ' thành công ' . count($arrayMusic) . " bài hát." . $count_partner . " hợp đồng đối tác");
            } else {
                return redirect()->route('media.music.index')->with('error', 'Thêm mới từ file ' . $file->getClientOriginalName() . ' thất bại. File không đúng dịnh dạng mẫu');
            }

        }
        return redirect()->route('media.music.index')->with('error', $validator->errors()->first());

    }
    public function insertContractPartner()
    {
        $arrayMusic = DB::table('music')->where('admin_id', Auth::user()->id)->where('warning_status', 3)->select('contract_number1', 'tac_gia', 'contract_number4', 'id')->get(); //danh sach nhạc vừa insert
        $array_contract = DB::table('contracts')->where('type','=',3)->pluck('contract_number');
        $array_contract = json_decode(json_encode($array_contract), true);
        $array_contract = array_map('mb_strtolower', $array_contract);
        $array_contract_tac_gia = DB::table('contracts')->where('type','!=',2)->where('type','!=',3)->pluck('contract_number')->toArray();
        $array_contract_tac_gia = array_map('mb_strtolower', $array_contract_tac_gia);
        $will_insert_contracts = []; // mảng sẽ insert
        $will_insert_contracts1 = []; // mảng sẽ insert
        $will_insert_contracts_tac_gia = []; // mảng sẽ insert
        $will_insert_contracts_tac_gia1 = []; // mảng sẽ insert
        $adminID = Auth::user()->id;
        foreach ($arrayMusic as $music) {
            if ($music->contract_number1 && !in_array(Str::lower($music->contract_number1), $array_contract)&& !in_array(['contract_number'=>Str::lower($music->contract_number1)], $will_insert_contracts1) ) {
                array_push($will_insert_contracts1, ['contract_number' => Str::lower($music->contract_number1)]);
                array_push($will_insert_contracts, ['contract_number' => $music->contract_number1]);
            }

            if ($music->contract_number4 && !in_array(Str::lower($music->contract_number4), $array_contract)&& !in_array(['contract_number'=>Str::lower($music->contract_number4)], $will_insert_contracts1)) {
                array_push($will_insert_contracts1, ['contract_number' => Str::lower($music->contract_number4)]);
                array_push($will_insert_contracts, ['contract_number' => $music->contract_number4]);
            }

            if ($music->tac_gia && !in_array(Str::lower($music->tac_gia), $array_contract_tac_gia)&& !in_array(['contract_number'=>Str::lower($music->tac_gia)], $will_insert_contracts_tac_gia1)) {
                array_push($will_insert_contracts_tac_gia1, ['contract_number' => Str::lower($music->tac_gia)]);
                array_push($will_insert_contracts_tac_gia, ['contract_number' => $music->tac_gia]);
            }

        }
        foreach($will_insert_contracts as $key => $value){
            $will_insert_contracts[$key]['type'] = 3;
            $will_insert_contracts[$key]['category_id'] = 1;
            $will_insert_contracts[$key]['admin_id'] = $adminID;
        }
        foreach($will_insert_contracts_tac_gia as $key => $value){
            $will_insert_contracts_tac_gia[$key]['type'] = 4;
            $will_insert_contracts_tac_gia[$key]['category_id'] = 1;
            $will_insert_contracts_tac_gia[$key]['admin_id'] = $adminID;
        }
        foreach (array_chunk($will_insert_contracts_tac_gia, 1000) as $t) {
            DB::table('contracts')->insertOrIgnore($t);
        }
        foreach (array_chunk($will_insert_contracts, 1000) as $t) {
            DB::table('contracts')->insertOrIgnore($t);
        }
        DB::table('music')->where('admin_id', Auth::user()->id)
            ->where('warning_status', '3')
            ->update([
                'warning_status' => '0',
            ]);

        return count($will_insert_contracts) + count($will_insert_contracts_tac_gia);
    }
    public function export()
    {
        $today = date("d_m_Y");
        return Excel::download(new MusicExport, 'music-' . $today . '.xlsx');
    }
    public function example()
    {
        return Excel::download(new MusicExampleExport, 'musicExample.xlsx');
    }

    public function contractExtension(Request $request, $id)
    {

        $data = Music::find($id);
        $list_type = TypeFee::pluck('name', 'id');
        $admin_id = $data->admin_id;
        return view('admin.media.music.contractExtension', compact('data', 'list_type'));
        // return view('admin.media.music.contractExtension',compact('music'));
    }

    public function updateContract(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'end_time' => empty($request->start_time) ? 'nullable|date' : 'required|date|after_or_equal:start_time', 
        ], [
            'end_time.after_or_equal' => 'Thời gian kết thúc phải lớn hơn hoặc bằng thời gian bắt đầu',
            'end_time.required' => 'Nhập vào thời gian kết thúc' 
        ]);
        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator->errors());
        }

        $music = Music::find($id);
        $music->fill($request->all());
        $changes = $music->getDirty();
        $old_value = array();
        foreach ($changes as $key => $value) {
            $old_value[$key] = $music->getOriginal($key);
        }
        if(!empty($changes))
        {
            if($changes['end_time']){
                $changes['end_time'] = reFormatDate($changes['end_time']);
                $old_value['end_time'] = reFormatDate($old_value['end_time']);
            }
            $log = ActionLog::writeActionLog($id, $music->getTable(), config("common.actions.extension", "extension"), get_class($this), "Gia hạn", $old_value, $changes, null);
        }
        if ($request->link) {
            $link = $request->file('link');
            $linkName = time() . '-music.' . $link->extension();
            $link->move('upload/image', $linkName);

            $contractFile = DB::table('contract_files')->where('id', $request->contractFileID)->get();
            if (count($contractFile)) {
                //nếu đã tồn tại update lại link của contractFile
            } else {
                DB::table('contract_files')->insert([
                    'link' => $linkName,
                    'categori_id' => 1,
                    // media_id
                ]);
            }
        }
        $music->save();
        if(isset($changes['end_time'])){
            ContractMediaSaleMoney::where('category_id',1)->where('media_id',$music->id)->update([
                'end_time' => $music->end_time,
            ]);
        }
        return redirect()->route('media.music.index')->with('success', 'Gia hạn thành công');
    }

    public function buy(Request $request)
    {
        if ($request->musicID) {
            // session()->forget('arrayMusicID');
            // session()->put('arrayName');
            // session()->put('arraySingle');
            // return ;
            if ($request->session()->has('arrayMusicIDBuy')) {
                $arrayMusicID = session('arrayMusicIDBuy');
                $arrayName = session('arrayMusicNameBuy');
                $musicID = $request->musicID;
                $name = $request->name;
                $array = explode(' ', $arrayMusicID);
                if (!in_array($musicID, $array)) {
                    $arrayMusicID = $arrayMusicID . ' ' . $musicID;
                    $arrayName = $arrayName . '__' . $name;
                    session()->put('arrayMusicIDBuy', $arrayMusicID);
                    session()->put('arrayMusicNameBuy', $arrayName);
                }

            } else {
                $arrayMusicID = $request->musicID;
                $arrayName = $request->name;
                session()->put('arrayMusicIDBuy', $arrayMusicID);
                session()->put('arrayMusicNameBuy', $arrayName);
            }
            $data = 'Thêm nhạc để mua thành công';
            return $arrayMusicID;
        }
    }

    public function sale(Request $request)
    {
        if ($request->musicID) {
            // session()->forget('arrayMusicID');
            // session()->put('arrayName');
            // session()->put('arraySingle');
            // return ;
            if ($request->session()->has('arrayMusicIDSale')) {
                $arrayMusicID = session('arrayMusicIDSale');
                $arrayName = session('arrayMusicNameSale');
                $musicID = $request->musicID;
                $name = $request->name;
                $array = explode(' ', $arrayMusicID);
                if (!in_array($musicID, $array)) {
                    $arrayMusicID = $arrayMusicID . ' ' . $musicID;
                    $arrayName = $arrayName . '__' . $name;
                    session()->put('arrayMusicIDSale', $arrayMusicID);
                    session()->put('arrayMusicNameSale', $arrayName);
                }

            } else {
                $arrayMusicID = $request->musicID;
                $arrayName = $request->name;
                session()->put('arrayMusicIDSale', $arrayMusicID);
                session()->put('arrayMusicNameSale', $arrayName);
            }
            $data = 'Thêm nhạc để bán thành công';
            return $arrayMusicID;
        }
    }
}
