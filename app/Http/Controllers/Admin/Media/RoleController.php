<?php

namespace App\Http\Controllers\Admin\Media;

use App\Component\Recursive;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Country;
use App\Models\RoleMedia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
class RoleController extends Controller
{
    public function __construct() {
        $this->checkPermission(config("common.permission.module.media-role", "media-role"));
    }

    public function index(Request $request){
        $categories=Category::pluck('name','id');
        $categories[0]='Chọn tất cả';
        $page = \Request::get('page') ? \Request::get('page') : 1;
        $query = DB::table('role_media as a');
        $perPage = Config::get('app_per_page') ? Config::get('app_per_page') : 10;
        $startLimit = $perPage * ($page - 1);
        $query->leftjoin('role_media as b',function ($join){
            $join->on('a.parent_id','=','b.id');
        });
        $query->leftjoin('categories',function ($join){
            $join->on('categories.id','=','a.categori_id');
        });
        $query->leftjoin('users',function ($join){
            $join->on('users.id','=','a.admin_id');
        });
        if(!empty(\request()->keyword))
            $query->where(function ($query) {
                $query->Where('a.name','like','%'.\request()->keyword.'%');
                $query->orWhere('a.code','like','%'.\request()->keyword.'%');
                
            });
        if(!empty(\request()->type))
            $query->where('a.type',\request()->type);
        // if(!empty(\request()->categori_id))
        //     $query->where('a.categori_id',\request()->categori_id);
        if(!empty(\request()->level))
            $query->where('a.level',\request()->level);
        $query= $query->select('a.*',DB::raw('categories.name as cate_name'),
            DB::raw('users.username as user_name'),DB::raw('b.code as parent_name'));
        $data = $query->orderBy('a.id','desc')->offset($startLimit)->limit($perPage)->paginate($perPage);
        
        $model = config("common.permission.module.media-role", "media-role");
        return view('admin/media/role/index', compact('data','categories', 'model'))->with('i', ($request->input('page', 1) - 1) * $perPage);
    }
    public function create(){
        $data_role=$this->getRoleMedia();
        $categories=Category::pluck('name','id');
        $countries=Country::pluck('name','id');
        $countries->prepend('---Quốc gia hiệu lực---', '');
        return view('admin.media.role.create',compact('categories','data_role', 'countries'));
    }
    public function getRoleMedia()
    {
        $data = RoleMedia::all()->toArray();
        $recursive = new Recursive($data);
        $htmlOption = $recursive->recursiveMedia();
        return $htmlOption;
    }
    public function store(Request $request){
        $equalParentType = true;
        if(RoleMedia::find($request->parent_id) && $request->type && RoleMedia::find($request->parent_id)->type!=$request->type) $equalParentType = false;
        $this->validate($request,[
            'name'=>'required|unique:role_media',
            'code'=>[
                'required',
                Rule::unique('role_media')->where(function ($query) {
                    return $query->where('categori_id', \request()->input('categori_id'))
                        ->where('type', \request()->input('type'));
                })
            ],
            'categori_id'=>[
                // Rule::unique('role_media')->where(function ($query) {
                //     return $query->where('code', \request()->input('code'))
                //         ->where('type', \request()->input('type'));
                // })
            ],
            'type'=>[
                $equalParentType?'':'equal_parent_type',
                // Rule::unique('role_media')->where(function ($query) {
                //     return $query->where('code', \request()->input('code'))
                //         ->where('categori_id', \request()->input('categori_id'));
                // })
            ],
        ],[],[
            'name'=>'Tên quyền',
            'code'=>'Mã quyền',
            'categori_id'=>'Quyền cha',

        ]);

        $role = RoleMedia::create($request->all());
        $role->level = $role->level??0;
        $role->save();
        $country_id=null;
        if(!empty($request->country)){
            $country=Country::firstOrCreate(['name'=>Str::title($request->country)]);
            $country_id=$country->id;
            $role->country_id = $country_id;
            $role->save();
        }

        return redirect()->route('media.role.index')->with('success','Tạo mới quyền thành công');
    }
    public function edit($id){
        $role=RoleMedia::find($id);
        $roles = RoleMedia::where('parent_id', $id)->pluck('id');
        $data_role=$this->getRoleMedia();
        $list_role= RoleMedia::where('id','!=', $id)->whereNotIn('id', $roles)->where('categori_id',$role->categori_id)->pluck('code', 'id');
        $list_role->prepend('---Quyền cha---', 0);
        $categories=Category::pluck('name','id');
        $countries=Country::pluck('name','id');
        $countries->prepend('---Quốc gia hiệu lực---', '');
        return view('admin.media.role.edit',compact('categories','role','data_role', 'list_role', 'countries'));
    }
    public function update($id, Request $request){
        $equalParentType = true;
        if(RoleMedia::find($request->parent_id) && $request->type && RoleMedia::find($request->parent_id)->type!=$request->type) $equalParentType = false;
        $this->validate($request,[
            'name'=>'required|unique:role_media,name,'.$id,
            'code'=>[
                'required',
                Rule::unique('role_media')->where(function ($query) {
                    return $query
                        //->where('categori_id', \request()->input('categori_id'))
                        ->where('type', \request()->input('type'));
                })->ignore($id)
            ],
            'type'=>$equalParentType?'':'equal_parent_type',
        ],[],[
            'name'=>'Tên quyền',
            'code'=>'Mã quyền',
            'type'=>'loại quyền'
        ]);
        $role=RoleMedia::find($id);
        $role->update($request->all());
        $role->level = $request->level?1:0;
        $role->save();
        return redirect()->route('media.role.index')->with('success','Sửa quyền thành công');
    }
    public function delete($id){
        RoleMedia::where('id',$id)->delete();
        RoleMedia::where('parent_id',$id)->delete();
        return redirect()->route('media.role.index')->with('success','Xóa quyền thành công');
    }

}
