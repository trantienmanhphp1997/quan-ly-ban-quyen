<?php

namespace App\Http\Controllers\Admin\Contractor;

use App\Http\Controllers\Controller;
use App\Models\Category;
use DB;

class ContractFileController extends Controller
{
    public function index(){
        $categories=Category::pluck('name','id');
        $query= DB::table('contract_files');
        $query->leftjoin('categories', function($join)
        {
            $join->on('contract_files.categori_id', '=', 'categories.id');
        });
        $query->leftjoin('users', function($join){
            $join->on('contract_files.admin_id','=','users.id');
        });
        $query->leftjoin('contracts', function($join){
            $join->on('contract_files.contract_id','=','contracts.id');
        });
        $query->select('contract_files.*', DB::raw('categories.name as cate_name'),
            DB::raw('users.username as username'), DB::raw('contracts.contract_number as contract_num'));
        if(!empty(\request()->contractId)){
            $query->where('contracts.contract_number','like','%'.\request()->contractId.'%')
                ->orWhere('contract_files.name','like','%'.\request()->contractId.'%');
        }
        else {
            if(\request()->category!='all'&&\request()->category!=null){
                $query->orWhere('categori_id',\request()->category);
            }
        }
        $data=$query->orderBy('id','desc')->paginate(10);
        return view('admin.contractFile.index', compact('data','categories'));
    }
}
