<?php

namespace App\Http\Controllers\Admin\Contract;

use App\Http\Controllers\Controller;
use App\Models\contract_file;
use Illuminate\Http\Request;

class ApprovalPapersController extends Controller
{
    public function index(Request $request){
        $contractFile = contract_file::orderBy('id','DESC')->paginate(5);
        return view('admin.contractFile.index',compact('contractFile'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }
    public function create()
    {
        return view('admin.contractFile.create');
    }

    public function store(Request $request)
    {
        $contractFile = contract_file::orderBy('id','DESC')->paginate(5);
        $request->validate([
            'name' => 'required',
            'date_sign' => 'required',
            'note' => 'required'
        ]);

        contract_file::create($request->all());

        return view('admin.contractFile.index',compact('contractFile'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

//    public function show(contract_file $contract_file)
//    {
//        return view('admin.contractPurchaseBuy.show',compact('contract_file'));
//    }


}
