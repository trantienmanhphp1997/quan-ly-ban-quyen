<?php

namespace App\Http\Controllers\Admin\Contract;

use App\Models\Permission;
use App\Models\ContractFile;
use App\Models\contract;
use App\Models\ActionLog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage as FacadesStorage;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
class ContractFileController extends Controller
{
    public function __construct() {
        $this->checkPermission(config("common.permission.module.contract-file", "contract-file"));
    }

    public function index(Request $request){
        return view('admin/contractFile/index');
    }
    public function destroy(Request $request)
    {
        DB::table("contract_files")->where('id', $request->id)->delete();
        $contract = contract::find($request->contract_id);

        return view('admin.contract.childrent_contractPurchaseBuy.contractFile.index', [
            'contract' => $contract,
        ])->with('message','Deleted Successfully');
    }
    public function create(){
        return view('admin/contractFile/create');
    }
    public function store(Request $request){
        $this->validate($request,[
            'name'=>'required',
            'date_sign'=>'required',
            'contract_id'=>'max:100',
            'note'=>'max:100',
            'link'=>'required',
            'media_id'=>'max:100',
            'OptionCate'=>'max:100'],[],[

             'name'=>'Tên hợp đồng',
             'link'=>'Đường link',
             'date_sign'=>'Ngày ký'

        ]);
        $date_sign = strtotime($request->date_sign);
        $date_sign = date('Y-m-d', $date_sign);
        // dd($request->date_sign);
        $contract_file = ContractFile::create([
            'name'=>$request->name,
            'date_sign'=>$date_sign,
            'contract_id'=>$request->contract_id,
            'note'=>$request->note,
            'link'=>$request->link,
            'media_id'=>$request->media_id,
            'categori_id'=>$request->OptionCate
        ]);
        return redirect()->route('contract.file')->with('success','update Success');

    }
    public function edit($id){
        $data = DB::table('contract_files')
            ->leftjoin('contracts', 'contract_files.contract_id', '=', 'contracts.id')
            ->leftjoin('users', 'contract_files.admin_id', '=', 'users.id')
            ->where('contract_files.id',$id)
            ->select('contract_files.*','contract_number','users.full_name')
            ->get();
        $data = $data[0];
        return view('admin.contractFile.edit',compact('data'));
    }
    public function update(Request $request,$id){
        $this->validate($request,[
            'name'=>'required',
            'date_sign'=>'required',
            'contract_id'=>'max:100',
            'note'=>'max:100',
            'link'=>'required',
            'media_id'=>'max:100',
            'OptionCate'=>'max:100'],[],[

            'name'=>'Tên hợp đồng',
            'link'=>'Đường link',
            'date_sign'=>'Ngày ký'
        ]);
        $input = $request->all();
        $date=date_create($input['date_sign']);
        $input['date_sign'] = date_format($date,"Y-m-d");
        $contract_file = ContractFile::find($id)->update($input);
        return redirect()->route('contract.file')->with('success','update Successfully');

    }
    public function delete($id){
        ContractFile::where('id',$id)->delete();
        return redirect()->route('contract.file')->with('success','delete Successfully');

    }
    public function forDownload($id){
        $dl = ContractFile::find($id);
        return Storage::download($dl->link);
    }

}
