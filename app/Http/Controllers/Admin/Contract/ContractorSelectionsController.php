<?php

namespace App\Http\Controllers\Admin\Contract;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\contractor_selections;

class ContractorSelectionsController extends Controller
{
    public function index(Request $request){
        $contractor = contractor_selections::orderBy('id','DESC')->paginate(5);
        dd("sss");
        return view('admin.contractorSelections.index',compact('contractor'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        return view('admin.contractorSelections.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'contract_number' => 'required',
            'date_sign' => 'required',
            'plan' => 'required',
            'result' => 'required'
        ]);

        contractor_selections::create($request->all());

        return redirect()->route('admin.contractorSelections.index')
            ->with('success','contract purchase created successfully.');
    }

    public function show(contractor_selections $contractor_selections)
    {
        return view('admin.contractPurchaseBuy.show',compact('contractor_selections'));
    }

    public function edit(contractor_selections $contractor_selections)
    {
        return view('admin.contractPurchaseBuy.edit',compact('contractor_selections'));
    }

    public function update(Request $request, contractor_selections $contractor_selections)
    {
        $request->validate([
            'contract_number' => 'required',
            'date_sign' => 'required',
            'plan' => 'required',
            'result' => 'required'
        ]);

        $contractor_selections->update($request->all());

        return redirect()->route('admin.contractPurchaseBuy.index')
            ->with('success','Updated successfully');
    }

    public function destroy(contractor_selections $contractor_selections)
    {
        $contractor_selections->delete();

        return redirect()->route('admin.contractPurchaseBuy.index')
            ->with('success', 'contract deleted successfully');
    }
}
