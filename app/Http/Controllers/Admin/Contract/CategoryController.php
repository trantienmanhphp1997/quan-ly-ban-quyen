<?php

namespace App\Http\Controllers\Admin\Contract;

use App\Models\Permission;
use App\Models\Category;
use App\Models\Contract;
use App\Models\ActionLog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    public function __construct() {
        $this->checkPermission(config("common.permission.module.contract-category", "contract-category"));
    }

    public function index(Request $request){
        $perPage = Config::get('app_per_page') ? Config::get('app_per_page') : 100;
        $data = Category::orderBy('id','DESC')->paginate($perPage);
        $model =config("common.permission.module.contract-category", "contract-category");
        return view('admin.contract.category.index',compact('data', 'model'))
            ->with('i', ($request->input('page', 1) - 1) * $perPage);
    }

    public function create(){
        return view('admin.contract.category.create');
    }

    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required|unique:categories,name',
        ],[
            'name.required' =>'Tên loại hợp đồng bắt buộc',
            'name.unique' => "Tên loại hợp đồng đã tồn tại",
        ],[]);

        Category::create($request->all());

        return redirect()->route('contract.category')
            ->with('success','Thêm loại hợp đồng thành công');
    }

    public function edit($id)
    {
        $category = Category::find($id);

        return view('admin.contract.category.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|unique:categories,name,'.$id,
        ],[
            'name.required' =>'Tên loại hợp đồng bắt buộc',
            'name.unique' => "Tên loại hợp đồng đã tồn tại",
        ],[]);

        $category = Category::find($id);
        $category->name = $request->name;
        $category->note = $request->note;
        $category->save();

        return redirect()->route('contract.category')
            ->with('success','Chỉnh sửa thành công');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $category= Category::find($id);
        $contract = Contract::where('category_id', $category->id)->get();
            if(count($contract)){
                return redirect()->route('contract.category')
            ->with('error','Không được phép xóa thể loại hợp đồng đã tạo hợp đồng.');
            }
        $category->delete();

        return redirect()->route('contract.category')
            ->with('success','Xóa loại hợp đồng thành công');
    }
}
