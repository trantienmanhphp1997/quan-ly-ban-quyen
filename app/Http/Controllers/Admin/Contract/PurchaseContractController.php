<?php

namespace App\Http\Controllers\Admin\Contract;

use App\Http\Controllers\Controller;
use App\Http\Controllers\API\SendMessageApiController;
use App\Models\Contract;
use App\Models\Music;
use App\Models\Revenue;
use App\Models\RevenueType;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Partner;
use App;
use App\Models\ActionLog;
use DB;
class PurchaseContractController extends Controller
{
    public function __construct() {
        $this->checkPermission(config("common.permission.module.contract-viettel-buy", "contract-viettel-buy"));
    }

    public function index(Request $request){
        return view('admin.contract.viettel-buy.index');
    }
    public function create()
    {
        $list_categoy = Category::pluck('name', 'id');
        $list_categoy->prepend('---Tất cả---', '');
        $list_partner = Partner::pluck('name', 'id');
        $list_partner->prepend('---Tất cả---', '');
        return view('admin.contract.contractPurchaseBuy.create',compact('list_categoy', 'list_partner'));
    }
    public function store(Request $request)
    {
        // dd($request->all());
        if($request->category_id_old) {
            if($request->category_id){
                $request->category_id = $request->category_id_old;
            }
            else {
                $request->merge(['category_id' => $request->category_id_old]);
            }
        }
        $this->validate($request, [
            'user_email' => 'nullable|email',
            'user_msisdn' => 'nullable|phone',
            'title' => 'max:255',
            'content' => 'max:1000',
            'total_money' => 'max:20',
            'contract_number' => 'required|max:255|unique:contracts,contract_number,',
            'date_sign' =>'required|date_format:d-m-Y',
            'category_id' => 'required',
            'partner_id' => 'required',
            'start_time' => 'date_format:d-m-Y',
            'end_time' => 'date_format:d-m-Y|after:start_time',
            'report' => 'required',
        ],[],[
            'user_email' => 'Email',
            'title' => 'Tiêu đề',
            'date_sign' =>'Ngày ký',
            'content' => 'Nội dung',
            'total_money' => 'Tổng giá trị hợp đồng',
            'contract_number' => 'Số hợp đồng',
            'category_id' => 'Loại hợp đồng',
            'partner_id' => 'Đối tác',
            'start_time' => 'Ngày bắt đầu bản quyền',
            'end_time' => 'Ngày hết hạn',
            'report' => 'Theo tờ trình số',
//            'total_money' => 'required',
//            'payment_schedule' => 'required',
//            'payment_amount' => 'required',
//            'handover_schedule' => 'required',
//            'content_handover' => 'required',
//            'content' => 'required',
//            'fullname' => 'required',
            // 'user_email' => 'required',
            // 'user_name' => 'required',
            // 'user_msisdn' => 'required',
        ]);
        // $this->validator($request->all())->validate();
        try{
            $param = $request->all();
            $data = Contract::create($param);

            $data->type = 1;
            if(!Partner::find($request->partner_id)){
                $partner = Partner::create(['name' => $request->partner_id ] );
                $data->partner_id = $partner->id;
            }

            $data->save();
            $log = ActionLog::writeActionLog($data->id, $data->getTable(), config("common.actions.created", "created"), get_class($this), "Tạo mới", null, null, null);
            // $list_categoy = Category::pluck('name', 'id');
            // $list_categoy->prepend('---Tất cả---', '');
            // $list_partner = Partner::pluck('name', 'id');
        // return view('admin.contract.contractPurchaseBuy.edit',compact('data', 'list_categoy' , 'list_partner'))->with('success','Added successfully');
            return redirect()->route('contract.viettel-buy.edit', $data->id)
            ->with('success','Thêm mới hợp đồng thành công');
            // return redirect()->route('contract.viettel-buy')
            // ->with('message','Added successfully');
        }catch (Exception $e) {
            session()->flash("error", __("error"));
            return redirect()->back()->withInput();
        }
    }

    public function show(contract $contract)
    {
        return view('admin.contract.contractPurchaseBuy.show',compact('contract'));
    }
    public function copy($id){
        $data = Contract::find($id);
        if(!isset($data)) abort(403, 'Unauthorized action.');
        $list_categoy = Category::pluck('name', 'id');
        $list_categoy->prepend('---Tất cả---', '');
        $list_partner = Partner::pluck('name', 'id');
        $list_partner->prepend('---Tất cả---', '');
        $contract_payment = DB::table('contract_payment')->join('contracts', 'contract_payment.contract_id', '=', 'contracts.id')->where('contract_payment.contract_id',$id)->select('contract_payment.*')->orderBy('contract_payment.date_pay','asc')->get();
        return view('admin.contract.contractPurchaseBuy.copy',compact('data', 'list_categoy' , 'list_partner','contract_payment'));
    }
    public function edit($id)
    {
        $data = Contract::find($id);
        if(!isset($data)) abort(403, 'Unauthorized action.');
        // nhạc
        // $data=[];
        // if($contract->category_id==1){
        //     $data = DB::table('music')->where('contract_id',$contract->id)->get();
        // }
        // else {

        // }
        $list_categoy = Category::pluck('name', 'id');
        $list_categoy->prepend('---Tất cả---', '');
        $list_partner = Partner::pluck('name', 'id');
        $list_partner->prepend('---Tất cả---', '');
        $contract_payment = DB::table('contract_payment')->join('contracts', 'contract_payment.contract_id', '=', 'contracts.id')->where('contract_payment.contract_id',$id)->select('contract_payment.*')->orderBy('contract_payment.date_pay','asc')->get();
        return view('admin.contract.contractPurchaseBuy.edit',compact('data', 'list_categoy' , 'list_partner','contract_payment'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'user_email' => 'nullable|email',
            'user_msisdn' => 'nullable|phone',
            'title' => 'max:255',
            'content' => 'max:1000',
            'total_money' => 'max:20',
            'contract_number' => 'required|max:255|unique:contracts,contract_number,' . $id,
            'date_sign' =>'required|date_format:d-m-Y',
            'partner_id' => 'required',
            'start_time' => 'date_format:d-m-Y',
            'end_time' => 'date_format:d-m-Y|after:start_time',
            'report' => 'required',
        ],[],[
            'user_email' => 'Email',
            'title' => 'Tiêu đề',
            'date_sign' =>'Ngày ký',
            'content' => 'Nội dung',
            'total_money' => 'Tổng giá trị hợp đồng',
            'contract_number' => 'Số hợp đồng',
            'category_id' => 'Loại hợp đồng',
            'partner_id' => 'Đối tác',
            'start_time' => 'Ngày bắt đầu bản quyền',
            'end_time' => 'Ngày hết hạn',
            'report' => 'Theo tờ trình số',
        ]);


        $contract = Contract::find($id);
        $contract->fill($request->all());
        $new_value = $contract->getDirty();

        if(reFormatDate($contract->getOriginal('start_time'))==$new_value['start_time']) unset($new_value['start_time']);
        if(reFormatDate($contract->getOriginal('end_time'))==$new_value['end_time']) unset($new_value['end_time']);
        if(reFormatDate($contract->getOriginal('date_sign'))==$new_value['date_sign']) unset($new_value['date_sign']);
        $old_value = array_intersect_key($contract->getOriginal(), $new_value);
        if(isset($old_value['date_sign'])){
            $old_value['date_sign'] = reFormatDate($old_value['date_sign']);
        }
        if(isset($old_value['start_time'])){
            $old_value['start_time'] = reFormatDate($old_value['start_time']);
        }
        if(isset($old_value['end_time'])){
            $old_value['end_time'] = reFormatDate($old_value['end_time']);
        }
        if(isset($new_value['partner_id'])){
            $old_value['partner_id'] = Partner::find($old_value['partner_id'])->name??'';
            if(Partner::find($new_value['partner_id']))
                $new_value['partner_id'] =Partner::find($new_value['partner_id'])->name;
        }
        if(isset($old_value['contract_number']))
        Music::withTrashed()->where('tac_gia', $old_value['contract_number'])->update(['tac_gia' => $new_value['contract_number']]);

        $partner_id = $request->partner_id;

        //call API send message
        // if($request->user_msisdn) {
        //     $requestSendMessage = new Request();
        //     $requestSendMessage->service_id = config("common.send_message.service_id");
        //     $requestSendMessage["content"] = "test";
        //     $requestSendMessage->label = config("common.send_message.label");
        //     $requestSendMessage->msisdns = $request->user_msisdn;
        //     $requestSendMessage->deeplink = config("common.send_message.deeplink");

        //     $api = new SendMessageApiController();
        //     $api->sendMessage($requestSendMessage);
        // }
        
        if(!Partner::find($request->partner_id)){
            $partner = Partner::create(['name' => $request->partner_id ] );
            $contract->partner_id = $partner->id;
        }
        try {
            $contract->save();
            if(!empty($new_value)) {
                ActionLog::writeActionLog($id, $contract->getTable(), config("common.actions.updated", "updated"),  get_class($this), "Sửa", $old_value, $new_value, null);
            }
            // dd($request->div_active);
            return redirect()->route('contract.viettel-buy.edit',$id)
                ->with('success','Cập nhật hợp đồng thành công')->with('tab',$request->div_active);

        } catch (Exception $e) {
            session()->flash("error", __("Fail"));
            return redirect()->back()->withInput();
        }

    }
    // public function destroy($id)
    // {
    //     $contract = contract::find($id);
    //     ActionLog::writeActionLog(null, $contract->getTable(), config("common.actions.deleted", "deleted"), get_class($this), "Xóa", $contract->contract_number, null);
    //     ActionLog::where('record_id', '=', $id)->where('model', '=', 'contracts')->delete();
    //     $contract->delete();

    //     return redirect()->route('contract.viettel-buy')
    //         ->with('success', 'Xóa hợp đồng thành công');
    // }

    public function storePaymentProcess(Request $request, $id){
        $date_pay = strtotime($request->date_pay);
        $date_pay = date('Y-m-d H:i:s',$date_pay);
        DB::table('contract_payment')->insert([
            'contract_id' => $id,
            'date_pay' => $date_pay,
            'money' => $request->money,
            'note' => $request->note
        ]);
        $contract = DB::table('contract_payment')->where('contract_id', $id)->where('date_pay', $date_pay)->where('money', $request->money)->where('note', $request->note)->get();
        $count = count($contract);
        $id = $contract[$count-1]->id;
        return $id;
    }

    public function deletePaymentProcess($id){
        DB::table('contract_payment')->where('id', $id)->delete();
        return 'Xóa tiến độ thanh toán thành công';
    }
}
