<?php

namespace App\Http\Controllers\Admin\Contract;

use App\Http\Controllers\Controller;
use App\Http\Controllers\API\SendMessageApiController;
use App\Models\ActionLog;
use App\Models\Contract;
use App\Models\Music;
use App\Models\ContractMediaSaleMoney;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Country;
use App\Models\Partner;
use App\Models\Revenue;
use DB;
use Excel;
use App\Imports\EmployeeImport;
use App\Imports\FilmSaleUploadImport;
use App\Imports\MusicSaleUploadImport;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;


class PurchaseContractSaleController extends Controller
{
    public function __construct() {
        $this->checkPermission(config("common.permission.module.contract-viettel-sale", "contract-viettel-sale"));
    }

    public function index(Request $request){
        return view('admin.contract.viettel-sell.index');
    }

    public function create()
    {
        $list_categoy = Category::pluck('name', 'id');
        $list_categoy->prepend('---Tất cả---', '');
        $list_partner = Partner::pluck('name', 'id');
        $list_partner->prepend('---Tất cả---', '');
        return view('admin.contract.viettel-sell.create',compact('list_categoy', 'list_partner'));
    }

    public function store(Request $request)
    {
        if($request->category_id_old) {
            if($request->category_id){
                $request->category_id = $request->category_id_old;
            }
            else {
                $request->merge(['category_id' => $request->category_id_old]);
            }
        }
        $this->validate($request, [
            'user_email' => 'nullable|email',
            'user_msisdn' => 'nullable|phone',
            'title' => 'max:255',
            'content' => 'max:1000',
            'total_money' => 'max:20',
            'category_id' => 'required',
            'contract_number' => 'required|max:255|unique:contracts,contract_number,',
            'date_sign' =>'required|date_format:d-m-Y',
            'partner_id' => 'required',
            'start_time' => 'date_format:d-m-Y',
            'end_time' => 'date_format:d-m-Y|after:start_time',
            'report' => 'required',
        ],[],[
            'user_email' => 'Email',
            'title' => 'Tiêu đề',
            'date_sign' =>'Ngày ký',
            'content' => 'Nội dung',
            'total_money' => 'Tổng giá trị hợp đồng',
            'contract_number' => 'Số hợp đồng',
            'category_id' => 'Loại hợp đồng',
            'partner_id' => 'Đối tác',
            'start_time' => 'Ngày bắt đầu bản quyền',
            'end_time' => 'Ngày hết hạn',
            'report' => 'Theo tờ trình số',
        ]);

        try {    
            $param = $request->all();
            $data = Contract::create($param);

            $data->type = 2;
            if(!Partner::find($request->partner_id)){
                $partner = Partner::create(['name' => $request->partner_id ] );
                $data->partner_id = $partner->id;
            }

            $data->save();
            $log = ActionLog::writeActionLog($data->id, $data->getTable(), config("common.actions.created", "created"), get_class($this), "Tạo mới", null, null, null);

            // $list_categoy = Category::pluck('name', 'id');
            // $list_categoy->prepend('---Tất cả---', '');
            // $list_partner = Partner::pluck('name', 'id');
        // return view('admin.contract.contractPurchaseBuy.edit',compact('data', 'list_categoy' , 'list_partner'))->with('success','Added successfully');
            return redirect()->route('contract.viettel-sell.edit', $data->id)
            ->with('success','Thêm mới hợp đồng thành công');
            // return redirect()->route('contract.viettel-buy')
            // ->with('message','Added successfully');
        }catch (Exception $e) {
            session()->flash("error", __("error"));
            return redirect()->back()->withInput();
        }
    }

    public function show(Contract $contract)
    {
        return view('admin.contract.contractPurchaseSale.show',compact('contract'));
    }
    public function copy($id)
    {
        $data = Contract::find($id);
        if(!isset($data)) abort(403, 'Unauthorized action.');
        $list_categoy = Category::pluck('name', 'id');
        $list_categoy->prepend('---Tất cả---', '');
        $list_partner = Partner::pluck('name', 'id');
        $list_partner->prepend('---Tất cả---', '');
        $contract_payment = DB::table('contract_payment')->join('contracts', 'contract_payment.contract_id', '=', 'contracts.id')->where('contract_payment.contract_id',$id)->select('contract_payment.*')->orderBy('contract_payment.date_pay','asc')->get();
        return view('admin.contract.viettel-sell.copy',compact('data', 'list_categoy' , 'list_partner','contract_payment'));
    }
    public function edit($id)
    {
        $data = Contract::find($id);
        // dd($data);
        if(!isset($data)) abort(403, 'Unauthorized action.');
        $list_categoy = Category::pluck('name', 'id');
        $list_categoy->prepend('---Tất cả---', '');
        $list_partner = Partner::pluck('name', 'id');
        $list_partner->prepend('---Tất cả---', '');
        $contract_payment = DB::table('contract_payment')->join('contracts', 'contract_payment.contract_id', '=', 'contracts.id')->where('contract_payment.contract_id',$id)->select('contract_payment.*')->orderBy('contract_payment.date_pay','asc')->get();
        return view('admin.contract.viettel-sell.edit',compact('data', 'list_categoy' , 'list_partner','contract_payment'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'user_email' => 'nullable|email',
            'user_msisdn' => 'nullable|phone',
            'title' => 'max:255',
            'content' => 'max:1000',
            'total_money' => 'max:20',
            'contract_number' => 'required|max:255|unique:contracts,contract_number,' . $id,
            'date_sign' =>'required|date_format:d-m-Y',
            'partner_id' => 'required',
            'start_time' => 'date_format:d-m-Y',
            'end_time' => 'date_format:d-m-Y|after:start_time',
            'report' => 'required',
        ],[],[
            'user_email' => 'Email',
            'title' => 'Tiêu đề',
            'date_sign' =>'Ngày ký',
            'content' => 'Nội dung',
            'total_money' => 'Tổng giá trị hợp đồng',
            'contract_number' => 'Số hợp đồng',
            'category_id' => 'Loại hợp đồng',
            'partner_id' => 'Đối tác',
            'start_time' => 'Ngày bắt đầu bản quyền',
            'end_time' => 'Ngày hết hạn',
            'report' => 'Theo tờ trình số',
        ]);



        $contract = Contract::find($id);
        $contract->fill($request->all());
        $new_value = $contract->getDirty();

        if(reFormatDate($contract->getOriginal('start_time'))==$new_value['start_time']) unset($new_value['start_time']);
        if(reFormatDate($contract->getOriginal('end_time'))==$new_value['end_time']) unset($new_value['end_time']);
        if(reFormatDate($contract->getOriginal('date_sign'))==$new_value['date_sign']) unset($new_value['date_sign']);
        $old_value = array_intersect_key($contract->getOriginal(), $new_value);
        if(isset($old_value['date_sign'])){
            $old_value['date_sign'] = reFormatDate($old_value['date_sign']);
        }
        if(isset($old_value['start_time'])){
            $old_value['start_time'] = reFormatDate($old_value['start_time']);
        }
        if(isset($old_value['end_time'])){
            $old_value['end_time'] = reFormatDate($old_value['end_time']);
        }

        if(isset($new_value['partner_id'])){
            $old_value['partner_id'] = Partner::find($old_value['partner_id'])->name??'';
            if(Partner::find($new_value['partner_id']))
                $new_value['partner_id'] =Partner::find($new_value['partner_id'])->name;
        }
        if(isset($old_value['contract_number']))
        Music::withTrashed()->where('tac_gia', $old_value['contract_number'])->update(['tac_gia' => $new_value['contract_number']]);

        $partner_id = $request->partner_id;

        $revenue = Revenue::where('contract_id', '=', $contract->id)->first();
        if($revenue != null) {
            $revenue->update([
                'date_sign' => reFormatDate($contract->date_sign, 'Y-m-d'),
                'money' => $contract->total_money
            ]);
        }
        
        //call API send message
        // if($request->user_msisdn) {
        //     $requestSendMessage = new Request();
        //     $requestSendMessage->service_id = config("common.send_message.service_id");
        //     $requestSendMessage["content"] = "test";
        //     $requestSendMessage->label = config("common.send_message.label");
        //     $requestSendMessage->msisdns = $request->user_msisdn;
        //     $requestSendMessage->deeplink = config("common.send_message.deeplink");

        //     $api = new SendMessageApiController();
        //     $api->sendMessage($requestSendMessage);
        // }
        
        if(!Partner::find($request->partner_id)){
            $partner = Partner::create(['name' => $request->partner_id ] );
            $contract->partner_id = $partner->id;
        }
        try {
            $contract->save();
            if(!empty($new_value)) {
                ActionLog::writeActionLog($id, $contract->getTable(), config("common.actions.updated", "updated"),  get_class($this), "Sửa", $old_value, $new_value, null);
            }
                return redirect()->route('contract.viettel-sell.edit',$id)
                ->with('success','Cập nhật hợp đồng thành công')->with('tab',$request->div_active);

        } catch (Exception $e) {
            session()->flash("error", __("Fail"));
            return redirect()->back()->withInput();
        }

    }


    // public function destroy($id)
    // {
    //     $contract = Contract::find($id);
    //     ActionLog::writeActionLog(null, $contract->getTable(), config("common.actions.deleted", "deleted"), get_class($this), "Xóa", $contract->contract_number, null);
    //     ActionLog::where('record_id', '=', $id)->where('model', '=', 'contracts')->delete();
    //     $contract->delete();
    //     DB::table('contract_media_sale_money')->where('contract_id',$id)->delete();

    //     DB::table('revenue')->where('contract_id', $id)->delete();
    //     return redirect()->route('contract.viettel-sell')
    //         ->with('success', 'Xóa hợp đồng thành công');
    // }
    // public function delete($id){
    //     Contract::where('id',$id)->delete();
    //     return redirect()->route('contract.viettel-sell')->with('success','delete Successfully');
    // }
    public function storePaymentProcess(Request $request, $id){
        $date_pay = strtotime($request->date_pay);
        $date_pay = date('Y-m-d H:i:s',$date_pay);
        DB::table('contract_payment')->insert([
            'contract_id' => $id,
            'date_pay' => $date_pay,
            'money' => $request->money,
            'note' => $request->note
        ]);
        $contract = DB::table('contract_payment')->where('contract_id', $id)->where('date_pay', $date_pay)->where('money', $request->money)->where('note', $request->note)->get();
        $count = count($contract);
        $id = $contract[$count-1]->id;
        return $id;
    }

    public function deletePaymentProcess($id){
        DB::table('contract_payment')->where('id', $id)->delete();
        return 'Xóa tiến độ thanh toán thành công';
    }
    public function save($id, Request $req){
        ini_set('memory_limit', '-1');
        set_time_limit(0);

        $validator = Validator::make($req->all(), [
            'file' => 'required|mimes:xlsx,xls',
        ], [
            'file.required' => 'Chưa có file excel',
            'file.mimes' => 'Định dạng không hợp lệ',
        ]);

        if ($validator->passes()) {
            $file = $req->file('file');
            $adminID = Auth::user()->id;
            $data = Excel::toArray(new EmployeeImport, $req->file);
            $data = $data[0];
            $dem = 0;
            if(!$data){
                return redirect()->route('contract.viettel-sell.edit', $id)->with('error', 'Thêm mới từ file ' . $file->getClientOriginalName() . ' thất bại. File không đúng dịnh dạng mẫu')->with('tab','menu4');               
            }
            for($i=count($data[0])-1; $i--; $i>0){
                if(!$data[0][$i]){
                    $dem++;
                }
                else break;
            }
            if (trim($data[0][0]) == 'STT'&&count($data[0])-$dem >= 19 && count($data[0])-$dem<=22) {
                Excel::import(new MusicSaleUploadImport, $req->file);
                sleep(1);
                // 
                $musicUpload = DB::table('contract_music_sale_uploads')
                    ->where('status', '0')
                    ->where('admin_id', $adminID)
                    ->get();
                DB::table('contract_music_sale_uploads')
                    ->where('status', '0')
                    ->where('admin_id', $adminID)
                    ->delete();
                // Chuẩn hóa date
                foreach ($musicUpload as $key => $value) {
                    if($value->start_time=='0000-00-00') $musicUpload[$key]->start_time = null;
                    if($value->end_time=='0000-00-00') $musicUpload[$key]->end_time = null;
                } 
                // 
                $checkStartTime = 1;
                $checkEndTime = 1;
                $checkValidateTime = 1;
                $checkGhiAm = 1;
                $errorStartTime = '';
                $errorEndTime = '';
                $errorValidateTime = '';
                $errorGhiAm = '';
                $errorContractNumberType = '';
                $errorContractNumberType3 = '';
                // $ContractOFDifferentType = DB::table('contracts')->where(function ($query) {
                //                                 $query->where('type', '!=', 1);
                //                                 $query->orWhere('category_id', '=', 2);
                //                             })->pluck('contract_number')->toArray(); 
                $ContractOFDifferentType = DB::table('contracts')->where(function ($query) {
                                                $query->where('type', '!=', 1);
                                                $query->orWhere('category_id', '=', 2);
                                            })
                                            ->where('type','!=',4)->pluck('contract_number')->toArray(); 
                foreach ($musicUpload as $key => $value) {
                    if($value->name){
                        // dd($value,(!$value->start_time&&$value->end_time)||($value->start_time&&!$value->end_time));
                        if((!$value->start_time&&$value->end_time)||($value->start_time&&!$value->end_time)){
                            if($checkStartTime){
                                $checkStartTime = 0;
                                $errorStartTime = $errorStartTime.' '.($key+1);
                            }
                            else $errorStartTime = $errorStartTime.', '.($key+1);
                        }  
                        if(!$value->ghi_am){
                            if($checkGhiAm){
                                $checkGhiAm = 0;
                                $errorGhiAm = $errorGhiAm.' '.($key+1);
                            }
                            else $errorGhiAm = $errorGhiAm.', '.($key+1);
                        }    
                        if($value->start_time&&$value->end_time&&strtotime($value->start_time)>=strtotime($value->end_time)){
                            if($checkValidateTime){
                                $checkValidateTime = 0;
                                $errorValidateTime = $errorValidateTime.' '.($key+1);
                            }
                            else $errorValidateTime = $errorValidateTime.', '.($key+1);
                        } 
                    }
                }
                $error = '';
                $index = array();
                $checkFirst = 1;
                foreach($musicUpload as $key => $value){
                    $index[$key] = 1;
                    if(!$value->name||!$value->ghi_am){
                        $index[$key] = 0;
                        if($value->name){
                            if($checkFirst){
                                $checkFirst = 0;
                                $error = $error.'Lỗi dữ liệu bản ghi:';
                                $error = $error.' '.($key+1);
                            }
                            else $error = $error.', '.($key+1);
                        }
                        continue;
                    }
                }


                $checkFirst = 1;
                foreach($musicUpload as $key => $value){
                    $kt=1;
                    if($value->name){
                        if((!$value->start_time&&$value->end_time)||($value->start_time&&!$value->end_time)){
                            $kt = 0;
                            if($checkFirst){
                                $checkFirst = 0;
                                if($error) $error = $error.". Thiếu thời gian phát hành hoặc Thiếu thời hạn bản quyền:";
                                else $error = $error."Thiếu thời gian phát hành hoặc Thiếu thời hạn bản quyền:";
                                $error = $error.' '.($key+1);
                            }
                            else{
                                $error = $error.', '.($key+1);
                            } 
                        }
                    }
                    $index[$key] = (!$index[$key])?0:$kt;
                }
                ///// start
                $checkFirst = 1;
                foreach($musicUpload as $key => $value){
                    $kt=1;
                    if($value->name){
                        if($value->start_time&&$value->end_time&&strtotime($value->start_time)>=strtotime($value->end_time)){
                            $kt = 0;
                            if($checkFirst){
                                $checkFirst = 0;
                                if($error) $error = $error.". Thời gian phát hành lớn hơn hoặc bằng thời hạn bản quyền:";
                                else $error = $error."Thời gian phát hành lớn hơn hoặc bằng thời hạn bản quyền:";
                                $error = $error.' '.($key+1);
                            }
                            else{
                                $error = $error.', '.($key+1);
                            } 
                        }
                    }
                    $index[$key] = (!$index[$key])?0:$kt;
                }
                ////// end
                $checkFirst = 1;
                $errorContractNumberType = '';
                foreach($musicUpload as $key => $value){
                    $kt=1;
                    if($value->name){
                        if($value->ghi_am&&in_array($value->ghi_am, $ContractOFDifferentType)){
                            $kt = 0;
                            if($checkFirst){
                                $checkFirst = 0;
                                if($error) $error = $error.". Số hợp đồng đã tồn tại và không phải hợp đồng mua nhạc:";
                                else $error = $error."Số hợp đồng đã tồn tại và không phải hợp đồng mua nhạc:";
                                $errorContractNumberType = $errorContractNumberType.' '.($key+1);
                                $error = $error.' '.($key+1);
                            }
                            else{
                                $errorContractNumberType = $errorContractNumberType.', '.($key+1);
                                $error = $error.', '.($key+1);
                            } 
                        }
                    }
                    $index[$key] = (!$index[$key])?0:$kt;
                }

                
                $checkFirst = 1;
                $errorContractNumberType3 = '';
                foreach($musicUpload as $key => $value){
                    $kt=1;
                    if($value->name){
                        if($value->tac_gia&&in_array($value->tac_gia, $ContractOFDifferentType)){
                            $kt = 0;
                            if($checkFirst){
                                $checkFirst = 0;
                                if($error) $error = $error.". Số hợp đồng đã tồn tại và không phải hợp đồng tác giả:";
                                else $error = $error."Số hợp đồng đã tồn tại và không phải hợp đồng tác giả:";
                                $errorContractNumberType3 = $errorContractNumberType3.' '.($key+1);
                                $error = $error.' '.($key+1);
                            }
                            else{
                                $errorContractNumberType3 = $errorContractNumberType3.', '.($key+1);
                                $error = $error.', '.($key+1);
                            } 
                        }
                    }
                    $index[$key] = (!$index[$key])?0:$kt;
                }
                if($error){
                    $error1 = [];
                    array_push($error1, [
                        'errorStartTime' => $errorStartTime
                    ]);
                    // array_push($error1, [
                    //     'errorEndTime' => $errorEndTime
                    // ]);
                    array_push($error1, [
                        'errorValidateTime' => $errorValidateTime
                    ]);
                    array_push($error1, [
                        'errorGhiAm' => $errorGhiAm
                    ]);
                    array_push($error1, [
                        'errorContractNumberType' => $errorContractNumberType
                    ]);
                    array_push($error1, [
                        'errorContractNumberType3' => $errorContractNumberType3
                    ]);
                    return redirect()->route('contract.viettel-sell.edit', $id)->with('errorVersion2', 'Thêm mới từ file ' . $file->getClientOriginalName() . ' thất bại. '.$error)->with('tab','menu4')->with('outputError', json_encode($error1));
                }               
                foreach ($index as $key => $value) {
                    if (!$index[$key]) {
                        unset($musicUpload[$key]);
                    }
                }
                if(!count($musicUpload)){
                    return redirect()->route('contract.viettel-sell.edit', $id)->with('error', 'Thêm mới từ file ' . $file->getClientOriginalName() . ' thất bại. Thông tin trên file Import rỗng')->with('tab','menu4');
                }
                $countMusic = 0;
                $countContract = 0;
                $count_partner = 0;
                foreach ($musicUpload as $key => $music) {
                    $contract = Contract::where('contract_number', $music->ghi_am)->get()->first();
                    // dd($contract);
                    if($contract){
                        $musicOld = Music::join('contracts', 'contracts.id', 'music.contract_id')
                            ->where('contract_number', $music->ghi_am)
                            ->where('name', $music->name)
                            ->select('music.id','music.start_time','music.end_time')
                            ->first();
                        if($musicOld){
                            ContractMediaSaleMoney::create([
                                'contract_id' => $id,
                                'media_id' => $musicOld->id,
                                'start_time' => $music->start_time?$music->start_time:$musicOld->start_time, 
                                'end_time'=> $music->end_time?$music->end_time:$musicOld->end_time, 
                                'category_id' => 1, //nhac
                                'cho_tac_quyen' => $music->cho_tacquyen ? 1 : 0,
                                'cho_doc_quyen' => $music->cho_docquyen  ? 1 : 0,
                                'so_tac_quyen' => $music->so_docquyen  ? 1 : 0,
                                'so_doc_quyen' => $music->so_tacquyen ? 1 : 0,
                                'ma_nhac_cho' => $music->ma_nhac_cho,                                 
                                'name_khong_dau' => $music->name_khong_dau,                                 
                                'singer_khong_dau' => $music->singer_khong_dau,                                 
                            ]);
                        }
                        else {
                            $countMusic++;
                            $musicNew = Music::create([
                                'name' => $music->name,
                                'singer' =>$music->singer,
                                'musician' => $music->musician,
                                'cho_docquyen'=>$music->cho_docquyen,
                                'cho_tacquyen' => $music->cho_tacquyen,
                                'so_docquyen' => $music->so_docquyen,
                                'so_tacquyen'=>$music->so_tacquyen,
                                'ghi_am' => $music->ghi_am,
                                'tac_gia' => $music->tac_gia,
                                'start_time' => $music->start_time,
                                'end_time' => $music->end_time,
                                'music_id'=> $music->music_id,
                                'contract_number2' => $music->contract_number2,
                                'contract_number3' => $music->contract_number3,
                                'contract_number5' => $music->contract_number5,
                                'contract_number6' => $music->contract_number6,
                                'contract_id' => $contract->id,
                                'warning_status' => 3,
                            ]);
                            ContractMediaSaleMoney::create([
                                'contract_id' => $id,
                                'media_id' => $musicNew->id,
                                'start_time' => $music->start_time, 
                                'end_time'=> $music->end_time, 
                                'category_id' => 1, //nhac
                                'cho_tac_quyen' => $music->cho_tacquyen ? 1 : 0,
                                'cho_doc_quyen' => $music->cho_docquyen  ? 1 : 0,
                                'so_tac_quyen' => $music->so_docquyen  ? 1 : 0,
                                'so_doc_quyen' => $music->so_tacquyen ? 1 : 0,
                                'ma_nhac_cho' => $music->ma_nhac_cho,    
                                'name_khong_dau' => $music->name_khong_dau,                                 
                                'singer_khong_dau' => $music->singer_khong_dau,                               
                            ]);

                        }
                    }
                    else {
                        $countMusic++;
                        $countContract++;
                        $contract = Contract::create([
                            'contract_number' => $music->ghi_am,
                            'start_time' => $music->start_time, 
                            'end_time'=> $music->end_time, 
                            'type' => 1,  // hợp đồng mua
                            'category_id' => 1 // hợp đồng nhạc
                        ]);
                        $musicNew = Music::create([
                            'name' => $music->name,
                            'singer' =>$music->singer,
                            'musician' => $music->musician,
                            'cho_docquyen'=>$music->cho_docquyen,
                            'cho_tacquyen' => $music->cho_tacquyen,
                            'so_docquyen' => $music->so_docquyen,
                            'so_tacquyen'=>$music->so_tacquyen,
                            'ghi_am' => $music->ghi_am,
                            'tac_gia' => $music->tac_gia,
                            'start_time' => $music->start_time,
                            'end_time' => $music->end_time,
                            'music_id'=> $music->music_id,
                            'contract_number2' => $music->contract_number2,
                            'contract_number3' => $music->contract_number3,
                            'contract_number5' => $music->contract_number5,
                            'contract_number6' => $music->contract_number6,
                            'contract_id' => $contract->id,
                            'warning_status' => 3,
                        ]);
                        ContractMediaSaleMoney::create([
                            'contract_id' => $id,
                            'media_id' => $musicNew->id,
                            'start_time' => $music->start_time, 
                            'end_time'=> $music->end_time, 
                            'category_id' => 1, //nhac
                            'cho_tac_quyen' => $music->cho_tacquyen,
                            'cho_doc_quyen' => $music->cho_docquyen,
                            'so_tac_quyen' => $music->so_docquyen,
                            'so_doc_quyen' => $music->so_tacquyen,
                            'ma_nhac_cho' => $music->ma_nhac_cho,   
                            'name_khong_dau' => $music->name_khong_dau,                                 
                            'singer_khong_dau' => $music->singer_khong_dau,                                
                        ]);
                    }
                }
                // dd('dừng');
                foreach($musicUpload as $key =>$music){
                    if($music->tac_gia){
                        $contractTacGiaOld = Contract::where('contract_number',$music->tac_gia)->get();
                        if(!count($contractTacGiaOld)){
                            Contract::create([
                                'contract_number' => $music->tac_gia,
                                'type' => 4,  // hợp đồng mua
                                'category_id' => 1 // hợp đồng nhạc                                    
                            ]);
                            $count_partner++;
                        }
                    }
                    
                }
                // $count_partner = $this->insertContractPartner();
                $fileName = time().'_'.$file->getClientOriginalName();
                $link = $file->storeAs(config("common.save.path"), $fileName, config("common.save.diskType"));
                ActionLog::writeActionLog($id, 'contracts', config("common.actions.created", "created"), null, "Tải lên", null,null, $link);
                return redirect()->route('contract.viettel-sell.edit', $id)->with('success', 'Thêm mới từ file ' . $file->getClientOriginalName() . ' thành công ' . count($musicUpload) . " bài hát bán. Tạo mới ".($countContract+$count_partner).' hợp đồng mua, '.$countMusic.' bài hát')->with('tab','menu4');
            } else {
                return redirect()->route('contract.viettel-sell.edit', $id)->with('error', 'Thêm mới từ file ' . $file->getClientOriginalName() . ' thất bại. File không đúng dịnh dạng mẫu')->with('tab','menu4');
            }

        }
        return redirect()->route('contract.viettel-sell.edit', $id)->with('error', $validator->errors()->first())->with('tab','menu4');
    }
    public function insertContractPartner()
    {
        $arrayMusic = DB::table('music')->where('admin_id', Auth::user()->id)->where('warning_status', 3)->select('tac_gia', 'id')->get(); //danh sach nhạc vừa insert
        $array_contract_tac_gia = DB::table('contracts')->where("type",'!=', 2)->where('type','!=',3)->pluck('contract_number')->toArray();
        $will_insert_contracts_tac_gia = []; // mảng sẽ insert
        $adminID = Auth::user()->id;
        foreach ($arrayMusic as $music) {
            if ($music->tac_gia && !in_array($music->tac_gia, $array_contract_tac_gia)&& !in_array(['contract_number'=>$music->tac_gia], $will_insert_contracts_tac_gia)) {
                array_push($will_insert_contracts_tac_gia, ['contract_number' => $music->tac_gia]);
            }
        }
        foreach($will_insert_contracts_tac_gia as $key => $value){
            $will_insert_contracts_tac_gia[$key]['type'] = 4;
            $will_insert_contracts_tac_gia[$key]['category_id'] = 1;
            $will_insert_contracts_tac_gia[$key]['admin_id'] = $adminID;
        }
        foreach (array_chunk($will_insert_contracts_tac_gia, 1000) as $t) {
            DB::table('contracts')->insertOrIgnore($t);
        }
        DB::table('music')->where('admin_id', Auth::user()->id)
            ->where('warning_status', '3')
            ->update([
                'warning_status' => '0',
            ]);

        return count($will_insert_contracts_tac_gia);
    }
    public function saveFilmSale(Request $req){
        ini_set('memory_limit', '-1');
        set_time_limit(0);

        $validator = Validator::make($req->all(), [
            'file' => 'required|mimes:xlsx,xls',
        ], [
            'file.required' => 'Chưa có file excel',
            'file.mimes' => 'Định dạng không hợp lệ',
        ]);

        if ($validator->passes()) {
            $file = $req->file('file');
            $adminID = Auth::user()->id;
            $data = Excel::toArray(new EmployeeImport, $req->file);
            $data = $data[0];
            $dem = 0;
            if(!$data){
                return redirect()->route('contract.viettel-sell')->with('error', 'Thêm mới từ file ' . $file->getClientOriginalName() . ' thất bại. File không đúng dịnh dạng mẫu');               
            }
            for($i=count($data[0])-1; $i--; $i>0){
                if(!$data[0][$i]){
                    $dem++;
                }
                else break;
            }
            if (trim($data[0][0]) == 'STT'){

                Excel::import(new FilmSaleUploadImport, $req->file);
                sleep(1);
                $error = "";
                $filmUpload = DB::table('contract_film_sale_uploads')
                    ->where('status', '0')
                    ->where('admin_id', $adminID)
                    ->get();
                DB::table('contract_film_sale_uploads')
                    ->where('status', '0')
                    ->where('admin_id', $adminID)
                    ->delete();
                $countries = DB::table('countries')->pluck('name', 'id');
                $countries = json_decode(json_encode($countries), true); 
                foreach($countries as $key => $value){
                    $countries[$key] = Str::title($countries[$key]);    
                }
                // Chuẩn hóa date
                    foreach ($filmUpload as $key => $value) {
                        if($value->start_time=='0000-00-00') $filmUpload[$key]->start_time = null;
                        if($value->end_time=='0000-00-00') $filmUpload[$key]->end_time = null;
                    } 
                    $ContractOFDifferentType = DB::table('contracts')->where(function ($query) {
                                $query->where('type', '!=', 2);
                                $query->orWhere('category_id', '=', 1);
                            })->pluck('contract_number')->toArray();
                    $ContractOFDifferentType = array_map('mb_strtolower', $ContractOFDifferentType);
                //
                $checkStartTime = 1;
                $checkEndTime = 1;
                $checkValidateTime = 1;
                $checkContractNumber = 1;
                $checkRevenueType = 1;
                $errorStartTime = '';
                $errorEndTime = '';
                $errorValidateTime = '';
                $errorContractNumber = '';
                $errorContractNumberType = '';
                $errorRevenueType = '';
                $checkCountry = 1;
                $errorCountry = '';
                $indexErrorCountry = [];
                foreach ($filmUpload as $key => $value) {
                    $indexErrorCountry[$key] = 0;
                    if($value->vn_name){
                        if(!$value->start_time){
                            if($checkStartTime){
                                $checkStartTime = 0;
                                $errorStartTime = $errorStartTime.' '.($key+1);
                            }
                            else $errorStartTime = $errorStartTime.', '.($key+1);
                        }
                        if(!$value->end_time){
                            if($checkEndTime){
                                $checkEndTime = 0;
                                $errorEndTime = $errorEndTime.' '.($key+1);
                            }
                            else $errorEndTime = $errorEndTime.', '.($key+1);
                        }
                        if(!$value->contract_number){
                            if($checkContractNumber){
                                $checkContractNumber = 0;
                                $errorContractNumber = $errorContractNumber.' '.($key+1);
                            }
                            else $errorContractNumber = $errorContractNumber.', '.($key+1);
                        }
                        //////// start
                        $arrayQG = trim(Str::title($value->country));
                        $arrayQG = Str::of($arrayQG)->trim(',');
                        // chuyển thành mảng
                        $arrayQG = explode(',', $arrayQG);
                        foreach ($arrayQG as $QG) {
                            if(!trim($QG)){
                                continue;
                            }
                            $QG = trim(Str::title($QG));
                            if(!in_array($QG,$countries)){
                                if($checkCountry){
                                    $checkCountry = 0;
                                    $errorCountry = $errorCountry.' '.($key+1);
                                }
                                else $errorCountry = $errorCountry.', '.($key+1);
                                $indexErrorCountry[$key] = 1;
                                break;
                            }
            
                        }
                        /////// end
                        if(!$value->revenu_type){
                            if($checkRevenueType){
                                $checkRevenueType = 0;
                                $errorRevenueType = $errorRevenueType.' '.($key+1);
                            }
                            else $errorRevenueType = $errorRevenueType.', '.($key+1);
                        }
                        if($value->start_time&&$value->end_time&&strtotime($value->start_time)>=strtotime($value->end_time)){
                            if($checkValidateTime){
                                $checkValidateTime = 0;
                                $errorValidateTime = $errorValidateTime.' '.($key+1);
                            }
                            else $errorValidateTime = $errorValidateTime.', '.($key+1);
                        }
                    }
                }

                $index = array();
                $checkFirst = 1;
                foreach($filmUpload as $key => $value){
                    $index[$key] = 1;
                    if(!$value->vn_name||!$value->start_time||!$value->end_time||!$value->contract_number||!$value->revenu_type||$indexErrorCountry[$key]){
                        $index[$key] = 0;
                        if($value->vn_name){
                            if($checkFirst){
                                $checkFirst = 0;
                                $error = $error.'Lỗi dữ liệu bản ghi:';
                                $error = $error.' '.($key+1);
                            }
                            else $error = $error.', '.($key+1);
                        }
                        continue;
                    }
                    if(strtotime($value->start_time)>=strtotime($value->end_time)){
                        $index[$key] = 0;
                        if($checkFirst){
                            $checkFirst = 0;
                            $error = $error.'Lỗi dữ liệu bản ghi:';
                            $error = $error.' '.($key+1);
                        }
                        else $error = $error.', '.($key+1);
                        continue;                        
                    }
                }
                $checkFirst = 1;
                foreach($filmUpload as $key => $value){
                    $kt=1;
                    if($value->vn_name){
                        if($value->contract_number&&in_array(Str::lower($value->contract_number), $ContractOFDifferentType)){
                            $kt = 0;
                            if($checkFirst){
                                $checkFirst = 0;
                                if($error) $error = $error.". Số hợp đồng đã tồn tại và không phải hợp đồng bán phim:";
                                else $error = $error."Số hợp đồng đã tồn tại và không phải hợp đồng bán phim:";
                                $errorContractNumberType = $errorContractNumberType.' '.($key+1);
                                $error = $error.' '.($key+1);
                            }
                            else{
                                $errorContractNumberType = $errorContractNumberType.', '.($key+1);
                                $error = $error.', '.($key+1);
                            } 
                        }
                    }
                    $index[$key] = (!$index[$key])?0:$kt;
                }                
                // check loại doanh thu phù hợp
                $errorValidateRevenueType = '';
                $arrRevenuType = DB::table('revenue_type')->where('parent_id','!=',0)->pluck('name','id')->toArray();
                $arrRevenuType = array_map('mb_strtolower', $arrRevenuType);
                $checkFirst = 1;
                foreach ($filmUpload as $key => $value) {
                    if ($value->vn_name) {
                        $kt = 1;
                        if ($value->revenu_type&&!in_array(Str::lower($value->revenu_type), $arrRevenuType)) {
                            if($checkFirst){
                                if($error) $error = $error.". Không hỗ trợ loại doanh thu:";
                                else $error = $error."Không hỗ trợ loại doanh thu:";
                                $error = $error.' '.($key+1);
                                $errorValidateRevenueType = $errorValidateRevenueType.' '.($key+1);
                                $checkFirst = 0;
                            }
                            else {
                                $errorValidateRevenueType = $errorValidateRevenueType.', '.($key+1);
                                $error = $error.', '.($key+1);
                            }
                            $kt = 0;
                        }
                        $index[$key] = (!$index[$key])?0:$kt;
                    }
                }
                // check loại phim bán có phù hợp không
                $films = DB::table('films')->orderby('id','desc')->pluck('vn_name','id')->toArray();
                $films = array_map('mb_strtolower', $films);
                $filmsStartTime = DB::table('films')->orderby('id','desc')->pluck('start_time','id')->toArray();
                $filmsEndTime = DB::table('films')->orderby('id','desc')->pluck('end_time','id')->toArray();
                $checkFirst = 1;
                $errorValidateTimeFilm = '';
                foreach ($filmUpload as $key => $value) {
                    $kt = 1;
                    if ($value->vn_name) {
                            $filmsID = array_keys($films, Str::lower($value->vn_name));
                            if(!$this->checkValidateDate($filmsID, $value, $filmsStartTime, $filmsEndTime )){
                                if($checkFirst){
                                    if($error) $error = $error.". Thời hạn bản quyền phim không thỏa mãn:";
                                    else $error = $error."Thời hạn bản quyền phim không thỏa mãn:";
                                    $errorValidateTimeFilm = $errorValidateTimeFilm.' '.($key+1);
                                    $error = $error.' '.($key+1);
                                    $checkFirst = 0;
                                }
                                else {
                                    $errorValidateTimeFilm = $errorValidateTimeFilm.', '.($key+1);
                                    $error = $error.', '.($key+1);
                                }
                                $kt = 0;
                            }
                    }
                    $index[$key] = (!$index[$key])?0:$kt;
                }

                if($error){
                    $error1 = [];
                    array_push($error1, [
                        'errorStartTime' => $errorStartTime
                    ]);
                    array_push($error1, [
                        'errorEndTime' => $errorEndTime
                    ]);
                    array_push($error1, [
                        'errorValidateTime' => $errorValidateTime
                    ]);
                    array_push($error1, [
                        'errorValidateTimeFilm' => $errorValidateTimeFilm
                    ]);
                    array_push($error1, [
                        'errorContractNumber' => $errorContractNumber
                    ]);
                    array_push($error1, [
                        'errorContractNumberType' => $errorContractNumberType
                    ]);
                    array_push($error1, [
                        'errorRevenueType' => $errorRevenueType
                    ]);
                    array_push($error1, [
                        'errorValidateRevenueType' => $errorValidateRevenueType
                    ]);
                    array_push($error1, [
                        'errorCountry' => $errorCountry
                    ]);
                    return redirect()->route('contract.viettel-sell')->with('errorVersion2', 'Thêm mới từ file ' . $file->getClientOriginalName() . ' thất bại. '.$error)->with('outputError', json_encode($error1));
                }               
                foreach ($index as $key => $value) {
                    if (!$index[$key]) {
                        unset($filmUpload[$key]);
                    }
                }
                if(!count($filmUpload)){
                    return redirect()->route('contract.viettel-sell')->with('error', 'Thêm mới từ file ' . $file->getClientOriginalName() . ' thất bại. Thông tin trên file Import rỗng');
                }
                // lấy ra tất cả partner
                $partners = DB::table('partners')->select('name')->get();
                $partners = json_decode(json_encode($partners), true);
                $newPartners = [];
                foreach ($filmUpload as $key => $film) {
                    if($film->partner_name){
                        $newPartner = [
                            'name' => $film->partner_name,
                        ];
                        if (in_array($newPartner, $newPartners)) {
                            continue;
                        } else {
                            if (in_array($newPartner, $partners)) {
                                continue;
                            }
                            else {
                                if($newPartner) array_push($newPartners, $newPartner);
                            };
                        }
                    }
                }
                foreach($newPartners as $key => $value){
                    $newPartners[$key]['admin_id'] = $adminID;
                }
                foreach (array_chunk($newPartners, 1000) as $t) {
                    DB::table('partners')->insert($t);
                }
                sleep(1);
                // lấy ra danh sách partner sau khi insert vào DB
                $partners = DB::table('partners')->pluck('name', 'id');
                $partners = json_decode(json_encode($partners), true);
                // lấy ra tất cả hợp đồng của phim
                $contracts = DB::table('contracts')->where('type',2)->where('contracts.category_id','2')->select('contract_number')->get();
                $contracts = json_decode(json_encode($contracts), true);
                for ($i=0; $i < count($contracts) ; $i++) { 
                    $contracts[$i]['contract_number'] = Str::lower($contracts[$i]['contract_number']);
                }
                $newContracts = [];
                $newContractsSave = [];
                $countNumberContract = 0;
                foreach ($filmUpload as $key => $film) {
                    $newFilm = [
                        'contract_number' => Str::lower($film->contract_number),
                    ];
                    // nếu bản ghi tồn tại trong những hợp đồng mới
                    if (in_array($newFilm, $newContracts)) {
                        continue;
                    } else {
                        // nếu bản ghi đã tồn tại trong những hợp đồng cũ
                        if (in_array($newFilm, $contracts)) {
                            continue;
                        }
                        // contract_number phải khác null
                        else {
                            $countNumberContract++;
                            array_push($newContracts, $newFilm);
                            $partner_id = array_search($film->partner_name, $partners);
                            array_push($newContractsSave, [
                                'contract_number' => $film->contract_number,
                                'user_name' => $film->employee_name,
                                'user_msisdn' => $film->telephone_number,
                                'user_email' => $film->email,
                                'partner_id' => $partner_id?$partner_id:null,
                                'category_id' => 2, //phim
                                'type' => 2, // hợp đồng bán
                                'admin_id' => $adminID,
                                'date_sign' => $film->date_sign,
                            ]);
                        };
                    }
                }
                // lưu contract mới
                foreach (array_chunk($newContractsSave, 1000) as $t) {
                    DB::table('contracts')->insert($t);
                }
                // lấy tất cả phim trong DB
                $films = DB::table('films')->select('vn_name')->get();
                $films = json_decode(json_encode($films), true);
                for ($i=0; $i < count($films) ; $i++) { 
                    $films[$i]['vn_name'] = Str::lower($films[$i]['vn_name']);
                }
                $newFilms = [];
                $newFilmsSave = [];
                $countNumberFilm = 0;
                foreach ($filmUpload as $key => $film) {
                    $newFilm = [
                        'vn_name' => Str::lower($film->vn_name),
                    ];
                    if (in_array($newFilm, $newFilms)) {
                        continue;
                    } else {
                        if (in_array($newFilm, $films)) {
                            continue;
                        }
                        else {
                            $countNumberFilm++;
                            array_push($newFilms, $newFilm);
                            array_push($newFilmsSave, [
                                'product_name' => $film->name,
                                'vn_name' => $film->vn_name,
                                'admin_id' => $adminID,
                                'warning_status' => 4,
                            ]);
                        };
                    }
                }
                // lưu phim mới
                foreach (array_chunk($newFilmsSave, 1000) as $t) {
                    DB::table('films')->insert($t);
                }
                // lấy ra KĐQ, ĐQ, quốc gia được phép phát sóng
                $listCodeOldĐQ = DB::table('role_media')->where('type', 1)->where('categori_id', 2)->select('code')->get();
                $listCodeOldĐQ = json_decode(json_encode($listCodeOldĐQ), true);
                for ($i=0; $i < count($listCodeOldĐQ); $i++) { 
                    $listCodeOldĐQ[$i]['code'] = Str::upper($listCodeOldĐQ[$i]['code']);
                }  
                $listCodeOldKĐQ = DB::table('role_media')->where('type', 2)->where('categori_id', 2)->select('code')->get();
                $listCodeOldKĐQ = json_decode(json_encode($listCodeOldKĐQ), true);
                for ($i=0; $i < count($listCodeOldKĐQ); $i++) { 
                    $listCodeOldKĐQ[$i]['code'] = Str::upper($listCodeOldKĐQ[$i]['code']);
                }  
                // $listQG =  Country::select('name')->get();
                // $listQG = json_decode(json_encode($listQG), true);
                // for ($i=0; $i < count($listQG) ; $i++) { 
                //     $listQG[$i]['name'] = Str::title($listQG[$i]['name']);
                // }
                $newArrayCodeĐQ = [];
                $newArrayCodeKĐQ = [];
                $newArrayCodeĐQID = [];
                $newArrayCodeKĐQID = [];
                // $newArrayQG = [];
                foreach ($filmUpload as $key => $film) {
                    // xử lý độc quyền
                    // loại bỏ khoảng trắng và ',' ở cuối
                    $arrayĐQ = trim(Str::upper($film->ĐQ));
                    $arrayĐQ = Str::of($arrayĐQ)->trim(',');
                    // chuyển thành mảng
                    $arrayĐQ = explode(',', $arrayĐQ);
                    foreach ($arrayĐQ as $ĐQ) {
                        if(!trim($ĐQ)){
                            continue;
                        }
                        $newĐQ = [
                            'code' => trim($ĐQ),
                        ];
                        // nếu bản ghi tồn tại trong những role mới
                        if (in_array($newĐQ, $newArrayCodeĐQ)) {
                            continue;
                        } else {
                            // nếu bản ghi đã tồn tại trong những role cũ
                            if (in_array($newĐQ, $listCodeOldĐQ)) {
                                continue;
                            } else {
                                array_push($newArrayCodeĐQ, $newĐQ);
                            };
                        }
                    }
                    // xử lý không độc quyền
                    $arrayKĐQ = trim(Str::upper($film->KĐQ));
                    $arrayKĐQ = Str::of($arrayKĐQ)->trim(',');
                    // chuyển thành mảng
                    $arrayKĐQ = explode(',', $arrayKĐQ);
                    foreach ($arrayKĐQ as $KĐQ) {
                        if(!trim($KĐQ)){
                            continue;
                        }
                        $newKĐQ = [
                            'code' => trim($KĐQ),
                        ];
                        // nếu bản ghi tồn tại trong những role mới
                        if (in_array($newKĐQ, $newArrayCodeKĐQ)) {
                            continue;
                        } else {
                            // nếu bản ghi đã tồn tại trong những role cũ
                            if (in_array($newKĐQ, $listCodeOldKĐQ)) {
                                continue;
                            } else {
                                array_push($newArrayCodeKĐQ, $newKĐQ);
                            };
                        }
                    }
                    // xử lý quốc gia được phép phát sóng
                        // $arrayQG = trim(Str::title($film->country));
                        // $arrayQG = Str::of($arrayQG)->trim(',');
                        // // chuyển thành mảng
                        // $arrayQG = explode(',', $arrayQG);
                        // foreach ($arrayQG as $QG) {
                        //     if(!trim($QG)){
                        //         continue;
                        //     }
                        //     $newQG = [
                        //         'name' => trim($QG),
                        //     ];
                        //     // nếu bản ghi tồn tại trong những role mới
                        //     if (in_array($newQG, $newArrayQG)) {
                        //         continue;
                        //     } else {
                        //         // nếu bản ghi đã tồn tại trong những role cũ
                        //         if (in_array($newQG, $listQG)) {
                        //             continue;
                        //         } else {
                        //             array_push($newArrayQG, $newQG);
                        //         };
                        //     }
                        // }
                }
                // Lưu quốc gia, độc quyền, không độc quyền mới
                    // for ($i = 0; $i < count($newArrayQG); $i++) {
                    //     $newArrayQG[$i]['admin_id'] = $adminID;
                    // }
                    // foreach (array_chunk($newArrayQG, 1000) as $t) {
                    //     DB::table('countries')->insertOrIgnore($t);
                    // }
                for ($i = 0; $i < count($newArrayCodeĐQ); $i++) {
                    $newArrayCodeĐQ[$i]['categori_id'] = 2;
                    $newArrayCodeĐQ[$i]['type'] = 1;
                    $newArrayCodeĐQ[$i]['admin_id'] = $adminID;
                    $newArrayCodeĐQ[$i]['level'] = 0;
                }
                foreach (array_chunk($newArrayCodeĐQ, 1000) as $t) {
                    DB::table('role_media')->insertOrIgnore($t);
                }

                for ($i = 0; $i < count($newArrayCodeKĐQ); $i++) {
                    $newArrayCodeKĐQ[$i]['categori_id'] = 2;
                    $newArrayCodeKĐQ[$i]['type'] = 2;
                    $newArrayCodeKĐQ[$i]['admin_id'] = $adminID;
                    $newArrayCodeKĐQ[$i]['level'] = 0;
                }
                foreach (array_chunk($newArrayCodeKĐQ, 1000) as $t) {
                    DB::table('role_media')->insertOrIgnore($t);
                }
                // xử lý hạ tầng phát sóng, phải chờ lưu xong độc quyền và không độc quyền của phim
                $listHT = DB::table('role_media')->where('categori_id', 2)->where('level',1)->select('code')->get();
                $listHT = json_decode(json_encode($listHT), true);
                for ($i=0; $i < count($listHT); $i++) { 
                    $listHT[$i]['code'] = Str::upper($listHT[$i]['code']);
                }  
                $newArrayHT = [];
                foreach ($filmUpload as $key => $film) {
                    // xử lý hạ tầng phát sóng
                    $arrayHT = trim(Str::upper($film->HT));
                    $arrayHT = Str::of($arrayHT)->trim(',');
                    // chuyển thành mảng
                    $arrayHT = explode(',', $arrayHT);
                    foreach ($arrayHT as $HT) {
                        if(!trim($HT)){
                            continue;
                        }
                        $newHT = [
                            'code' => trim($HT),
                        ];
                        // nếu bản ghi tồn tại trong những role mới
                        if (in_array($newHT, $newArrayHT)) {
                            continue;
                        } else {
                            // nếu bản ghi đã tồn tại trong những role cũ
                            if (in_array($newHT, $listHT)) {
                                continue;
                            } else {
                                array_push($newArrayHT, $newHT);
                            };
                        }
                    }
                }
                for ($i = 0; $i < count($newArrayHT); $i++) {
                    $newArrayHT[$i]['categori_id'] = 2;
                    $newArrayHT[$i]['level'] = 1;
                    $newArrayHT[$i]['admin_id'] = $adminID;
                }
                foreach (array_chunk($newArrayHT, 1000) as $t) {
                    DB::table('role_media')->insertOrIgnore($t);
                }
                sleep(1);
                // Lưu phim bán vào hợp đồng bán
                $contracts = DB::table('contracts')->where('type',2)->where('contracts.category_id','2')->pluck('contract_number','id')->toArray();
                $contracts = array_map('mb_strtolower', $contracts);
                $films = DB::table('films')->orderby('id','desc')->pluck('vn_name','id')->toArray();
                $films = array_map('mb_strtolower', $films);
                $filmsStartTime = DB::table('films')->orderby('id','desc')->pluck('start_time','id')->toArray();
                $filmsEndTime = DB::table('films')->orderby('id','desc')->pluck('end_time','id')->toArray();
                // dd($contracts, $films);
                $newContractMediaSaleMoney = [];
                $newRevenue = [];
                // $filmsCount = array_count_values($films);
                foreach($filmUpload as $key => $film){
                    // lấy ra mảng những ID phù hợp
                    $filmsID =   array_keys($films, Str::lower($film->vn_name));
                    $typeID =   array_search(Str::lower($film->revenu_type), $arrRevenuType);
                    $contractID =   array_search(Str::lower($film->contract_number), $contracts);
                    if($contractID){
                        // dd($filmsEndTime, $filmsStartTime,strtotime($film->end_time)<=strtotime($filmsEndTime[6]), $film->end_time, $filmsEndTime[6] );
                        foreach($filmsID as $filmID){
                            if(!$filmsStartTime[$filmID]||!$filmsEndTime[$filmID]||(strtotime($film->end_time)<=strtotime($filmsEndTime[$filmID]) && strtotime($film->start_time)>=strtotime($filmsStartTime[$filmID])) ){
                                array_push($newContractMediaSaleMoney, [
                                    'contract_id' => $contractID,
                                    'media_id' => $filmID,
                                    'money' => $film->money,
                                    'category_id' =>2,
                                    'start_time' =>$film->start_time,
                                    'end_time' =>$film->end_time,
                                    'nuoc' =>$film->country_ps,
                                    'count_live' =>$film->count,
                                    'note' =>$film->country_note_ps,
                                    'note_film' =>$film->note,
                                    'status' => 1, // đã import và chưa xử lý
                                    'admin_id' =>$adminID,
                                ]);
                                array_push($newRevenue, [
                                    'contract_id' => $contractID,
                                    'media_id' => $filmID,
                                    'money' => $film->money,
                                    'moneyVAT' =>$film->moneyVAT,
                                    'category_id' =>2,
                                    'date_sign' =>$film->start_time,
                                    'type_contract'=>0,
                                    'type_id'=>$typeID,
                                    'type_revenue'=>0,
                                    'admin_id' =>$adminID,
                                ]);
                            }
                        }
                    }
                }
                // Lưu contract_media_sale_money
                foreach (array_chunk($newContractMediaSaleMoney, 1000) as $t) {
                    DB::table('contract_media_sale_money')->insertOrIgnore($t);
                }
                sleep(1);
                // lấy contract_media_sale_money mới import
                $contractMediaSaleMoney = DB::table('contract_media_sale_money')
                    ->where('admin_id', $adminID)
                    ->where('status',1)
                    ->where('category_id',2)
                    ->orderby('id', 'asc')
                    ->select('id')
                    ->get();
                DB::table('contract_media_sale_money')
                    ->where('status',1)
                    ->where('admin_id', $adminID)
                    ->where('category_id',2)
                    ->update(['status'=>0]);
                // 
                // $filmIDAfterInsert = json_decode(json_encode($filmIDAfterInsert), true);

                $roleMediaĐQAfterInsert = DB::table('role_media')->where('categori_id', 2)->where('type', 1)->pluck('code', 'id');
                $roleMediaĐQAfterInsert = json_decode(json_encode($roleMediaĐQAfterInsert), true);
                foreach($roleMediaĐQAfterInsert as $key => $value){
                    $roleMediaĐQAfterInsert[$key] = Str::upper($roleMediaĐQAfterInsert[$key]);    
                }
                $roleMediaKĐQAfterInsert = DB::table('role_media')->where('categori_id', 2)->where('type', 2)->pluck('code', 'id');
                $roleMediaKĐQAfterInsert = json_decode(json_encode($roleMediaKĐQAfterInsert), true);
                foreach($roleMediaKĐQAfterInsert as $key => $value){
                    $roleMediaKĐQAfterInsert[$key] = Str::upper($roleMediaKĐQAfterInsert[$key]);    
                }
                $htAfterInsert = DB::table('role_media')->where('categori_id', 2)->where('level', 1)->pluck('code', 'id');
                $htAfterInsert = json_decode(json_encode($htAfterInsert), true);
                foreach($htAfterInsert as $key => $value){
                    $htAfterInsert[$key] = Str::upper($htAfterInsert[$key]);    
                }
                $contractMediaRole = [];
                $countryFilm = [];
                $DT = [];
                $key1=0;
                foreach($filmUpload as $key => $film){
                    $filmsID =  array_keys($films, Str::lower($film->vn_name));

                    foreach($filmsID as  $filmID){
                        if(!$filmsStartTime[$filmID]||!$filmsEndTime[$filmID]||(strtotime($film->end_time)<=strtotime($filmsEndTime[$filmID]) && strtotime($film->start_time)>=strtotime($filmsStartTime[$filmID])) ){
                            $saleMoneyID = $contractMediaSaleMoney[$key1]->id;
                            $contractID = $newRevenue[$key1]['contract_id'];
                            $newRevenue[$key1]['sale_money_id'] = $saleMoneyID;
                            // xử lý độc quyền
                            // loại bỏ khoảng trắng và ',' ở cuối
                            $arrayĐQ = trim(Str::upper($film->ĐQ));
                            $arrayĐQ = Str::of($arrayĐQ)->trim(',');
                            // chuyển thành mảng
                            $arrayĐQ = explode(',', $arrayĐQ);
                            foreach ($arrayĐQ as $ĐQ) {
                                if(!trim($ĐQ)){
                                    continue;
                                }
                                $roleMediaID = array_search(trim($ĐQ), $roleMediaĐQAfterInsert);
                                array_push($contractMediaRole, [
                                    'role_id' => $roleMediaID,
                                    'media_id' => $filmID,
                                    'category_id' => 2,
                                    'admin_id' => $adminID,
                                    'contract_id' => $contractID,
                                    'sale_money_id' => $saleMoneyID,
                                ]);
                            }
                            // xử lý không độc quyền
                            // loại bỏ khoảng trắng và ',' ở cuối
                            $arrayKĐQ = trim(Str::upper($film->KĐQ));
                            $arrayKĐQ = Str::of($arrayKĐQ)->trim(',');
                            // chuyển thành mảng
                            $arrayKĐQ = explode(',', $arrayKĐQ);
                            foreach ($arrayKĐQ as $KĐQ) {
                                if(!trim($KĐQ)){
                                    continue;
                                }
                                $roleMediaID = array_search(trim($KĐQ), $roleMediaKĐQAfterInsert);
                                // dd(trim($KĐQ), $roleMediaKĐQAfterInsert);
                                array_push($contractMediaRole, [
                                    'role_id' => $roleMediaID,
                                    'media_id' => $filmID,
                                    'category_id' => 2,
                                    'admin_id' => $adminID,
                                    'contract_id' => $contractID,
                                    'sale_money_id' => $saleMoneyID,
                                ]);
                            }
                            // xử lý hạ tầng phát sóng
                            // loại bỏ khoảng trắng và ',' ở cuối
                            $arrayHT = trim(Str::upper($film->HT));
                            $arrayHT = Str::of($arrayHT)->trim(',');
                            // chuyển thành mảng
                            $arrayHT = explode(',', $arrayHT);
                            foreach ($arrayHT as $HT) {
                                if(!trim($HT)){
                                    continue;
                                }
                                $roleMediaID = array_search(trim($HT), $htAfterInsert);
                                array_push($contractMediaRole, [
                                    'role_id' => $roleMediaID,
                                    'media_id' => $filmID,
                                    'category_id' => 2,
                                    'admin_id' => $adminID,
                                    'contract_id' => $contractID,
                                    'sale_money_id' => $saleMoneyID,
                                ]);
                            }
                            // xử lý quốc gia
                            // loại bỏ khoảng trắng và ',' ở cuối
                            $arrayQG = trim(Str::title($film->country));
                            $arrayQG = Str::of($arrayQG)->trim(',');
                            // chuyển thành mảng
                            $arrayQG = explode(',', $arrayQG);
                            foreach ($arrayQG as $QG) {
                                if(!trim($QG)){
                                    continue;
                                }
                                $conutryID = array_search(trim(Str::title($QG)), $countries);
                                array_push($countryFilm, [
                                    'country_id' => $conutryID,
                                    'media_id' => $filmID,
                                    'type' => 1,
                                    'contract_id' => $contractID,
                                    'sale_money_id' => $saleMoneyID,
                                ]);
                            }
                            $key1++;
                        }
                    }
                }
                // Lưu revenu
                foreach (array_chunk($newRevenue, 1000) as $t) {
                    DB::table('revenue')->insertOrIgnore($t);
                }
                foreach (array_chunk($contractMediaRole, 1000) as $t) {
                    DB::table('contract_media_sale_role')->insertOrIgnore($t);
                }
                foreach (array_chunk($countryFilm, 1000) as $t) {
                    DB::table('film_country')->insertOrIgnore($t);
                } 
                $fileName = time().'_'.$file->getClientOriginalName();
                $link = $file->storeAs(config("common.save.path"), $fileName, config("common.save.diskType"));
                ActionLog::writeActionLog(null, 'contracts', config("common.actions.created", "created"), null, "Tải lên", null,null, $link);
                return redirect()->route('contract.viettel-sell')->with('success', 'Thêm mới từ file ' . $file->getClientOriginalName() . ' thành công ('.count($filmUpload).') bản ghi. Tạo mới ('.$key1.') phim bán, ('.$countNumberContract.') hợp đồng bán, ('.$countNumberFilm.') bộ phim');
            } else {
                return redirect()->route('contract.viettel-sell')->with('error', 'Thêm mới từ file ' . $file->getClientOriginalName() . ' thất bại. File không đúng dịnh dạng mẫu');
            }

        }
        return redirect()->route('contract.viettel-sell')->with('error', $validator->errors()->first());
    }
    public function checkValidateDate($filmsID, $value, $filmsStartTime, $filmsEndTime ){
        if(count($filmsID)){
            foreach($filmsID as $filmID){
                if($filmsStartTime[$filmID]&&$filmsEndTime[$filmID]&&strtotime($filmsStartTime[$filmID])<=strtotime($value->start_time)&&strtotime($filmsEndTime[$filmID])>=strtotime($value->end_time)) return true;
            }
        }
        else {
            return true;
        }
        return false;
    }
}
