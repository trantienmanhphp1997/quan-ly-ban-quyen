<?php

namespace App\Http\Controllers\Admin\Contract;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Contract;
use App\Models\Music;
use App\Models\ActionLog;
use App\Models\Partner;
use Illuminate\Http\Request;
use DB;
use League\Flysystem\Exception;

class PurchaseContractPartnerController extends Controller
{

    public function __construct() {
        $this->checkPermission(config("common.permission.module.contract-partner", "contract-partner"));
    }

    public function index(){
        return view('admin.contract.viettel-partner.index');
    }
    public function edit($id){
        $data=Contract::find($id);
        $list_categoy = Category::pluck('name', 'id');
        $list_categoy->prepend('---Tất cả---', '');
        $list_partner = Partner::pluck('name', 'id');
        $list_partner->prepend('---Tất cả---', '');
        $contract_payment = DB::table('contract_payment')->join('contracts', 'contract_payment.contract_id', '=', 'contracts.id')->where('contract_payment.contract_id',$id)->select('contract_payment.*')->orderBy('contract_payment.date_pay','asc')->get();

        return view('admin.contract.viettel-partner.edit', compact(
            'data','list_categoy','list_partner', 'contract_payment'));
    }
    public function update(Request $request,$id){
        $this->validate($request, [
            'contract_number' => 'required|max:255|unique:contracts,contract_number,' . $id,
            'date_sign' => 'required',
            'start_time' => 'required',
            'end_time' => 'required|after:start_time',
            'partner_id' => 'required',
            'report' => 'required',
        ],[],[
            'contract_number' => 'Số hợp đồng',
            'date_sign' => 'Ngày kí',
            'start_time' => 'Ngày bắt đầu',
            'end_time' => 'Ngày kết thúc',
            'partner_id' => 'Đối tác',
            'report' => 'Theo tờ trình số',
        ]);
        $contract = Contract::find($id);
        $contract->fill($request->all());
        $new_value = $contract->getDirty();
        if(reFormatDate($contract->getOriginal('start_time'))==$new_value['start_time']) unset($new_value['start_time']);
        if(reFormatDate($contract->getOriginal('end_time'))==$new_value['end_time']) unset($new_value['end_time']);
        if(reFormatDate($contract->getOriginal('date_sign'))==$new_value['date_sign']) unset($new_value['date_sign']);
        $old_value = array_intersect_key($contract->getOriginal(), $new_value);
        if(isset($old_value['date_sign'])){
            $old_value['date_sign'] = reFormatDate($old_value['date_sign']);
        }
        if(isset($old_value['start_time'])){
            $old_value['start_time'] = reFormatDate($old_value['start_time']);
        }
        if(isset($old_value['end_time'])){
            $old_value['end_time'] = reFormatDate($old_value['end_time']);
        }
        if(isset($new_value['partner_id'])){
            $old_value['partner_id'] = Partner::find($old_value['partner_id'])->name??'';
            if(Partner::find($new_value['partner_id']))
                $new_value['partner_id'] =Partner::find($new_value['partner_id'])->name;
        }
        $partner_id = $request->partner_id;
        if(!Partner::find($request->partner_id)){
            $partner = Partner::create(['name' => $request->partner_id ] );
            $contract->partner_id = $partner->id;
        }        
        if(isset($old_value['contract_number'])){
            Music::withTrashed()->where('contract_number1', $old_value['contract_number'])->update(['contract_number1' => $new_value['contract_number']]);
            Music::withTrashed()->where('contract_number4', $old_value['contract_number'])->update(['contract_number4' => $new_value['contract_number']]);
            Music::withTrashed()->where('tac_gia', $old_value['contract_number'])->update(['tac_gia' => $new_value['contract_number']]);
        }
        $contract->partner_id = $partner_id;
        try {
            $contract->save();
            if(!empty($new_value)) {
                ActionLog::writeActionLog($id, $contract->getTable(), config("common.actions.updated", "updated"),  get_class($this), "Sửa", $old_value, $new_value, null);
            }
            return redirect()->route('contract.viettel-partner.edit',$id)->with('tab', $request->div_active)
                ->with('success','Cập nhật hợp đồng thành công');
        } catch (Exception $e) {
            session()->flash("error", __("Fail"));
            return redirect()->back()->withInput();
        }
    }

}
