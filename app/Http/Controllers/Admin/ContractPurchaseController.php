<?php

namespace App\Http\Controllers\Admin;
use App\Admin;

use App\Models\ContractPurchase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
class ContractPurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.contractPurchase.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ContractPurchase  $contractPurchase
     * @return \Illuminate\Http\Response
     */
    public function show(ContractPurchase $contractPurchase)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ContractPurchase  $contractPurchase
     * @return \Illuminate\Http\Response
     */
    public function edit(ContractPurchase $contractPurchase)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ContractPurchase  $contractPurchase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContractPurchase $contractPurchase)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ContractPurchase  $contractPurchase
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContractPurchase $contractPurchase)
    {
        //
    }
}
