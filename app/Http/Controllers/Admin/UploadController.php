<?php

namespace App\Http\Controllers\Admin;
use App\Admin;

use App\Models\employees;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

use App\imports\EmployeeImport;
use App\exports\EmployeeExport;
use Excel;
class UploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $fillable = ['firstname','lastname','email','salary','phone'];

    public function listEmployee(){
        $employees = DB::table('employees')->get();
        return view('admin.upload.listEmployee',['employees'=>$employees,]);
    }
    public function upload()
    {
        return view('admin.upload.index');
    }

    public function store(Request $req)
    {

        if($req->hasFile('file')){
            $file = $req->file('file');
            $pathFile = $file->getClientOriginalExtension();
            if($pathFile=='xlsx'){
                Excel:: import(new EmployeeImport,$req->file);
                return redirect()->route('listEmployee');             
            }
            else {
                return redirect()->route('listEmployee');  
            }
        }
        else {
            return redirect()->route('listEmployee');  
        }
    }

    public function export(){
        return Excel::download(new EmployeeExport, 'employeeList.xlsx');
    }
}
