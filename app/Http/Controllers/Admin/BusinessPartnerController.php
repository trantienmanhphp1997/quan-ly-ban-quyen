<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Models\Contract;
// use App\Models\employees;
class BusinessPartnerController extends Controller
{
    public function __construct() {
        $this->checkPermission(config("common.permission.module.list-employee", "list-employee"));
    }
    public $searchName;
    public $searchPhone;
    public function listEmployee(request $request){
        $searchName = $request->searchName;
        $searchPhone = $request->searchPhone;
        $searchContractNumber = $request->searchContractNumber;
        $query = Contract::query();
        // $employees = employees::paginate();
    	//  $contracts = DB::table('contracts')->get();
        // $name=DB::table('contracts')->pluck('user_name')->get();
        // $phone=DB::table('contracts')->pluck('user_msisdn')->get();
        if(!empty($request->searchName)){
            $query->Where('user_name','like','%'.$searchName.'%');
        }
        if(!empty($request->searchPhone)){
            $query->where('user_msisdn','like','%'.$searchPhone.'%');
        }
        if(!empty($request->searchContractNumber)){
            $query->where('contract_number','like','%'.$searchContractNumber.'%');
        }
        $query->where(function ($query) {
            $query->where('user_name','!=',null)
                  ->orWhere('user_msisdn','!=',null);
        });
        
        $data = $query->orderby('id')->paginate(10);
        $model = config("common.permission.module.list-employee", "list-employee");
        return view('admin/businessPartner/listEmployee',compact('model','data'));

    }

}
