<?php

namespace App\Http\Controllers\Admin\Revenue;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ContractMediaSaleMoney;
use App\Models\Revenue;

class RevenueMusicController extends Controller
{
    public function __construct() {
        $this->checkPermission(config("common.permission.module.revenue-music", "revenue-music"));
    }
    public function index(){
        return view('admin.revenue.sale.music.index');
    }
    public function delete($id)
    {
        $revenue=Revenue::find($id);
        ContractMediaSaleMoney::where('id',$revenue->sale_money_id)->delete();
        $revenue->delete();
        return redirect()->route('revenue.music')->with('success', 'Xóa doanh thu nhạc thành công');
    }
}
