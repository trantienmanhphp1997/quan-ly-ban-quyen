<?php

namespace App\Http\Controllers\Admin\Revenue;

use App\Component\Recursive;
use App\Http\Controllers\Controller;
use App\Models\RevenueType;
use App\Models\Revenue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class RevenueTypeController extends Controller
{
    public function __construct() {
        $this->checkPermission(config("common.permission.module.revenue-type", "revenue-type"));
    }

    public function index(Request $request){
        $page = \Request::get('page') ? \Request::get('page') : 1;
        $perPage = Config::get('app.per_page') ? Config::get('app.per_page') : 10;
        $query= DB::table('revenue_type as a')->orderBy('created_at', 'desc');
        $query->leftJoin('revenue_type as b', function ($join){
            $join->on('a.parent_id','=','b.id');
        });
        $query->leftJoin('users', function ($join){
            $join->on('users.id','=','a.admin_id');
        });
        if(!empty($request->keyword))
            $query->where('a.name','like','%'.$request->keyword.'%');
        $query->select('a.*',DB::raw('users.username as user_name'),DB::raw('b.name as parent_name'));
        $data=$query->orderBy('a.id','asc')->paginate($perPage);
        $model = config("common.permission.module.revenue-type", "revenue-type");
        return view('admin.revenue.type.index', compact('data', 'model'));
    }
    public function create(){
        $htmlOption=$this->getParent('create','',0);
        return view('admin.revenue.type.create', compact('htmlOption'));
    }
    public function getParent($status,$parentId, $id){
        if($status=='create'){
            $data = RevenueType::where('parent_id', '=', 0)->pluck('name','id')->toArray();
        }
        else if($status=='edit') {
            $count = RevenueType::where('parent_id','=', $id)->count();
            if($count != 0) return $htmlOption=null;
            else $data = RevenueType::where('parent_id', '=', 0)->where('id', '!=', $id)->pluck('name','id')->toArray();

            // $data = RevenueType::where('parent_id','!=',$id)->get(); 
            // $data = RevenueType::pluck('parent_id','id');
            // $data = json_decode(json_encode($data), true);
            // $parent = [];
            // $parent1 = [];
            // $parent2 = [];
            // array_push($parent1, $id);
            // foreach($data as $key => $value){
            //     $parent[$key] = 0;
            // }
            // $kt = 0;
            // while(count($parent1)>0){
            //     $parent[$parent1[$kt]] = 1;
            //     foreach($data as $key => $value){
            //         if(!$parent[$key]&& $value==$parent1[$kt]){
            //             // print($key.' '.array_search($id, $parent1));
            //             if(!array_search($id, $parent1)){
            //                 array_push($parent1, $key);
            //                 array_push($parent2, $key);
            //             }
            //         }
            //     }
            //     unset($parent1[$kt]);
            //     $kt++;
            // }
            // $data = RevenueType::whereNotIn('id', $parent2)->get();
        }
        return $data;
        // $recursive = new Recursive($data);
        // $htmlOption = $recursive->recursive($parentId, $id);
        // return $htmlOption;
    }
    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required|unique:revenue_type',
        ], [], [
            'name' => 'Tên loại doanh thu',
        ]);
        RevenueType::create($request->all());
        return redirect()->route('revenue_type.index')->with('success', 'Thêm loại doanh thu thành công');
    }
    public function edit($id){
        $data=RevenueType::find($id);
        $htmlOption=$this->getParent('edit',$data->parent_id, $data->id);
        return view('admin.revenue.type.edit', compact('data', 'htmlOption'));
    }
    public function update($id, Request $request){
        $this->validate($request, [
            'name' => 'required|unique:revenue_type,name,'.$id,
        ], [], [
            'name' => 'Tên loại doanh thu',
        ]);
        RevenueType::find($id)->update($request->all());
        return redirect()->route('revenue_type.index')->with('success', 'Cập nhật loại doanh thu thành công');
    }
    public function delete($id){
        if(Revenue::where('type_id', $id)->get()->isNotEmpty()){

            return redirect()->route('revenue_type.index')->with('error', 'Không được xóa loại doanh thu đang bán');
        }
        if(RevenueType::where('parent_id', $id)->get()->isNotEmpty()){
            return redirect()->route('revenue_type.index')->with('error', 'Không được xóa loại doanh thu đang có doanh thu con');
        }
        RevenueType::find($id)->delete();
        return redirect()->route('revenue_type.index')->with('success', 'Xóa loại doanh thu thành công');
    }
}
