<?php

namespace App\Http\Controllers\Admin\Revenue;

use App\Http\Controllers\Controller;
use App\Models\ContractMediaSaleMoney;
use Illuminate\Http\Request;
use App\Models\Revenue;
use DB;

class RevenueFilmController extends Controller
{
    public function __construct()
    {
        $this->checkPermission(config("common.permission.module.revenue-film", "revenue-film"));
    }

    public function index()
    {
        $revenueDay = DB::table('revenue')
            ->select(
                DB::raw('SUM(money) AS sum_money'),
                DB::raw("DATE_FORMAT(created_at, '%Y-%m-%d') AS date_modified")
            )
            ->whereRaw("created_at BETWEEN CURRENT_DATE() - 7 AND CURRENT_DATE() + 1")
            ->groupBy('date_modified')->get();

        return view('admin.revenue.sale.film.index', compact('revenueDay'))->with('revenueDay', $revenueDay);
    }

    public function delete($id)
    {
        $revenue=Revenue::find($id);
        ContractMediaSaleMoney::where('id',$revenue->sale_money_id)->delete();
        $revenue->delete();
        return redirect()->route('revenue.film')->with('success', 'Xóa doanh thu phim thành công');
    }
}
