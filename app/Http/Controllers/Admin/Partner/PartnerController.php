<?php

namespace App\Http\Controllers\Admin\Partner;

use App\Http\Controllers\Controller;
use App\Models\Partner;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DB;

class PartnerController extends Controller
{
    public function __construct() {
        $this->checkPermission(config("common.permission.module.partner", "partner"));
        
    }
    public $searchName;
    public $searchPhone;
    public $searchEmail;
    public function index(Request $request){
        $searchName = $request->searchName;
        $searchPhone = $request->searchPhone;
        $searchEmail = $request->searchEmail;
        
        $query = DB::table('partners')->leftJoin('countries','countries.id','partners.country_id')->select('partners.*','countries.name as country_name');
        if(!empty($request->searchName)){
            $query->where(function ($query) use ($searchName) {
                $query->where('partners.name','like','%'.$searchName.'%');
                $query->orwhere('partners.common_name','like','%'.$searchName.'%');
            });
        }
        if(!empty($request->searchPhone)){
            $query->where('partners.phone','like','%'.$searchPhone.'%');
        }
        if(!empty($request->searchEmail)){
            $query->where('partners.email','like','%'.$searchEmail.'%');
        }
        $data = $query->orderby('id','desc')->paginate(10);
        $model = config("common.permission.module.partner", "partner");
        return view('admin.partner.index', compact('data','model'));


    }
    public function create(){
        $countries = Country::pluck('name', 'id');
        return view('admin.partner.create',compact('countries'));
    }
    public function store(Request $request){
        $this->validate($request,[
            'name'=>'required|unique:partners,name',
            'email'=>'required|email',
            // 'address'=>'',
            'phone'=>'phonePartner',
            'note'=>'max:1000'
            ,],[
                'name.required' => 'Tên trong hợp đồng bắt buộc',
                'name.unique' => 'Tên trong hợp đồng đã tồn tại',
            ],[
            'email'=>'Email',
            'address'=>'Địa chỉ',
            'phone' => 'Số điện thoại',
            'note'=>'Ghi chú',
        ]);
        $country_id = null;
        if($request->country){
            $country = Country::where('name',$request->country)->first();
            if($country){
                $country_id = $country->id;
            }
            else {
                $country = Country::create([
                    'name' => Str::title($request->country),
                ]);
                $country_id = $country->id;
            }
        };
        $partner=[
            'name'=>$request->name,
            'common_name'=>$request->common_name,
            'email'=>$request->email,
            'address'=>$request->address,
            'country_id'=>$country_id,
            'phone'=>$request->phone,
            'note'=>$request->note,
            'admin_id'=>auth()->id()
        ];
        Partner::create($partner);
        return redirect()->route('partner.index')->with('success', 'Thêm đối tác thành công');
    }
    public function edit($id){
        $countries = Country::pluck('name', 'id');
        $partner=Partner::find($id);
        return view('admin.partner.edit', compact('partner','countries'));
    }
    public function update(Request $request, $id){
        $this->validate($request,[
            'name'=>'required|unique:partners,name,'.$id,
            'email'=>'required|email',
            // 'address'=>'required',
            'phone'=>'phonePartner',
            'note'=>'max:1000'
            ,],[
                'name.required' => 'Tên trong hợp đồng bắt buộc',
                'name.unique' => 'Tên trong hợp đồng đã tồn tại',
            ],[
            'email'=>'Email',
            'address'=>'Địa chỉ',
            'note'=>'Ghi chú',
            'phone' => 'Số điện thoại',
        ]);
        $partner=Partner::find($id);
        $country_id = null;
        if($request->country){
            $country = Country::where('name',$request->country)->first();
            if($country){
                $country_id = $country->id;
            }
            else {
                $country = Country::create([
                    'name' => Str::title($request->country),
                ]);
                $country_id = $country->id;
            }
        };
        $partner->update([
            'name'=>$request->name,
            'common_name'=>$request->common_name,
            'email'=>$partner->email?$partner->email:$request->email,
            'country_id'=>$country_id,
            'address'=>$request->address,
            'phone'=>$request->phone,
            'note'=>$request->note,
        ]);
        return redirect()->route('partner.index')->with('success', 'Sửa thông tin đối  tác thành công');
    }
    public function delete($id){
        Partner::find($id)->delete();
        return redirect()->route('partner.index')->with('success','Xóa thông tin đối tác thành công');
    }
}
