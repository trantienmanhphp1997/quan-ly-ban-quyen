<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	protected function checkPermission($name)
		{ 
            $this->middleware('preventBackHistory');
            $role_list = $name . config("common.permission.prefix", "-") . config("common.permission.action.list", "list");;
            $role_create_edit = $name .  config("common.permission.prefix", "-") . config("common.permission.action.create-edit", "create-edit");
            $role_delete = $name .  config("common.permission.prefix", "-") . config("common.permission.action.delete", "delete");

            self::middleware('role:administrator|' . $role_list .'|'. $role_create_edit . '|' . $role_delete, ['only' => ['index','show']]);
            self::middleware('role:administrator|' . $role_create_edit . '|' . $role_delete, ['only' => ['create', 'store','edit','update']]);
            self::middleware('role:administrator|' . $role_delete, ['only' => ['destroy']]);
		}


}
