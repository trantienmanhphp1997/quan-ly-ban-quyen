<?php
/**
 * Return nav-here if current path begins with this path.
 *
 * @param string $path
 * @return string
 */
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

function setActive($path)
{
    return \Request::is($path . '*') ? ' class=active' :  '';
}
function getURL()
{
    // dd(\Request::url());
    if(\Request::is('contract/viettel-buy' . '*')){
        return 'buy';
    }
    else if(\Request::is('contract/viettel-sell' . '*')) {
        return 'sell';
    }
    else if(\Request::is('contract/viettel-partner' . '*')) {
        return 'partner';
    }

    else {
        return 'media';
    }
}
function setOpen($path)
{
    return \Request::is($path . '*') ? ' class=open' :  '';
}
function countChiPhiTap($data){
    $license_fee =$data->license_fee??0 ;
    $partner_fee =$data->partner_fee??0 ;
    $other_fee =$data->other_fee??0 ;
    $license_fee = (float) str_replace(',','',$license_fee);
    $partner_fee = (float)str_replace(',','',$partner_fee);
    $other_fee = (float)str_replace(',','',$other_fee);
    return $license_fee+$partner_fee+$other_fee;
}
function countChiPhi($data){
    $license_fee =$data->license_fee??0 ;
    $partner_fee =$data->partner_fee??0 ;
    $other_fee =$data->other_fee??0 ;
    $count_tap = $data->count_tap??0 ;
    $license_fee = (float)str_replace(',','',$license_fee);
    $partner_fee =(float) str_replace(',','',$partner_fee);
    $other_fee = (float)str_replace(',','',$other_fee);
    $count_tap = (float)str_replace(',','',$count_tap);
    return ($license_fee+$partner_fee+$other_fee)*$count_tap;
}


function getAdminName($id)
{
    $result = \App\Admin::find($id);
    return $result['name'];
}

function reFormatDate($datetime, $format='d-m-Y'){
    return (isset($datetime) && ($datetime != '0000-00-00 00:00:00') && ($datetime != '0000-00-00'))? date($format, strtotime($datetime)) : '';
}

function numberFormat($money = 0, $dec_point = '.' , $thousands_sep = ','){
    $money = str_replace(',','',$money);
    if(!$money) $money = 0;
    $arr = explode('.', sprintf("%.3f", $money));
    $decimal = (count($arr) > 1 && $arr[1] != '000') ? 3 : 0;
    return number_format($money, $decimal, $dec_point, $thousands_sep);
}

function decodeEmoji($content){
    return \App\Emoji::Decode($content);
}



function getToday(){
    $today = date("m/d/Y");

    return $today . ' - ' . $today;
}

function get7Day(){
    $today = date('m/d/Y');

    $sevenDay = date('m/d/Y', strtotime("-7 days"));

    return $sevenDay . ' - ' . $today;
}

function getTodayPicker(){
    $today = date("m/d/Y");

    return $today . ' - ' . $today;
}

function checkRole($role,$role2){
    if($role2) {
        if(Auth::user()->hasRole($role)||Auth::user()->hasRole('administrator')||Auth::user()->hasRole($role2)) return true;
        else return false;
    }
    else {
        if(Auth::user()->hasRole($role)||Auth::user()->hasRole('administrator')) return true;
        else return false;
    }
}

function getMailReply($id)
{
    $result = \App\Message::where(['parentId' => $id, 'status' => 1])->get();
    return $result;
}

function getMasterMailReply($id)
{
    $result = \App\Message::where(['parentId' => $id, 'status' => 1, 'senderUserId' => 1000000])->get();
    return $result;
}
function stringLimit($str, $limit = 30){
    return Str::limit($str, $limit);
}

function charCalculation ($char, $number) {
    $charList = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M','N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM','AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ'];
    foreach ($charList as $key => $value){
        if($value == $char){
            $number += $key;
            break;
        }
    }
    return $charList[$number];
}

function myMedia ($admin_id) {
    if(Auth::user()->id == $admin_id || Auth::user()->is_manager == 1 )
        return true;
    return false;
}

function boldTextSearchV2($text, $searchTerm){
    if(!strlen($searchTerm)) return $text;
    $newText = strtolower(removeStringUtf8($text));
    $newSearchTerm = strtolower(removeStringUtf8($searchTerm));
    $lengText = strlen($newText);
    $lengSearchTerm = strlen($newSearchTerm);
    $index = 0;
    for($i = 0; $i <= $lengText - $lengSearchTerm; $i++){
        if($newSearchTerm==substr($newText,$i,$lengSearchTerm)){
            // dd($newSearchTerm, $i, $newText,mb_substr($text,0,$i+$index),mb_substr($text,$i+$index,$lengSearchTerm));
            $text = mb_substr($text,0,$i+$index).'<b>'.mb_substr($text,$i+$index,$lengSearchTerm).'</b>'.mb_substr($text,$i + $index + $lengSearchTerm ,$lengText-$i-$lengSearchTerm);
            $index+=7;
        }
    }
    return $text;

}

function removeStringUtf8($str)
{
    $hasSign = array(
        'à', 'á', 'ạ', 'ả', 'ã', 'â', 'ầ', 'ấ', 'ậ', 'ẩ', 'ẫ', 'ă', 'ằ', 'ắ', 'ặ', 'ẳ', 'ẵ', '&agrave;', '&aacute;', '&acirc;', '&atilde;',
        'è', 'é', 'ẹ', 'ẻ', 'ẽ', 'ê', 'ề', 'ế', 'ệ', 'ể', 'ễ', '&egrave;', '&eacute;', '&ecirc;',
        'ì', 'í', 'ị', 'ỉ', 'ĩ', '&igrave;', '&iacute;', '&icirc;',
        'ò', 'ó', 'ọ', 'ỏ', 'õ', 'ô', 'ồ', 'ố', 'ộ', 'ổ', 'ỗ', 'ơ', 'ờ', 'ớ', 'ợ', 'ở', 'ỡ', '&ograve;', '&oacute;', '&ocirc;', '&otilde;',
        'ù', 'ú', 'ụ', 'ủ', 'ũ', 'ư', 'ừ', 'ứ', 'ự', 'ử', 'ữ', '&ugrave;', '&uacute;',
        'ỳ', 'ý', 'ỵ', 'ỷ', 'ỹ', '&yacute;',
        'đ', '&eth;',
        'À', 'Á', 'Ạ', 'Ả', 'Ã', 'Â', 'Ầ', 'Ấ', 'Ậ', 'Ẩ', 'Ẫ', 'Ă', 'Ằ', 'Ắ', 'Ặ', 'Ẳ', 'Ẵ', '&Agrave;', '&Aacute;', '&Acirc;', '&Atilde;',
        'È', 'É', 'Ẹ', 'Ẻ', 'Ẽ', 'Ê', 'Ề', 'Ế', 'Ệ', 'Ể', 'Ễ', '&Egrave;', '&Eacute;', '&Ecirc;',
        'Ì', 'Í', 'Ị', 'Ỉ', 'Ĩ', '&Igrave;', '&Iacute;', '&Icirc;',
        'Ò', 'Ó', 'Ọ', 'Ỏ', 'Õ', 'Ô', 'Ồ', 'Ố', 'Ộ', 'Ổ', 'Ỗ', 'Ơ', 'Ờ', 'Ớ', 'Ợ', 'Ở', 'Ỡ', '&Ograve;', '&Oacute;', '&Ocirc;', '&Otilde;',
        'Ù', 'Ú', 'Ụ', 'Ủ', 'Ũ', 'Ư', 'Ừ', 'Ứ', 'Ự', 'Ử', 'Ữ', '&Ugrave;', '&Uacute;',
        'Ỳ', 'Ý', 'Ỵ', 'Ỷ', 'Ỹ', '&Yacute;',
        'Đ', '&ETH;',
    );
    $noSign = array(
        'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a',
        'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e',
        'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i',
        'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o',
        'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u',
        'y', 'y', 'y', 'y', 'y', 'y',
        'd', 'd',
        'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A',
        'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E',
        'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I',
        'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O',
        'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U',
        'Y', 'Y', 'Y', 'Y', 'Y', 'Y',
        'D', 'D'
    );

    $str = str_replace($hasSign, $noSign, $str);
    return $str;
}



function ToVNColumnName(string $table, string $colName) {
    // Chỗ này sau này phải viết ra chỗ khác :D
    $mapData = [
        'music' => [
            'name' => 'Tên',
            'ghi_am' => 'Quyền ghi âm',
            'tac_gia' => 'Quyền tác giả',
            'cho_docquyen' => 'Nhạc chờ độc quyền',
            'cho_tacquyen' => 'Nhạc số tác quyền',
            'so_docquyen' => 'Nhạc số độc quyền',
            'so_tacquyen' => 'Nhạc số tác quyền',
            'sale_permission' => 'Bán đối tác thứ 3',
            'start_time' => 'Thời gian bắt đầu',
            'end_time' => 'Thời gian kết thúc',
            'singer' => 'Ca sĩ',
            'musician' => 'Nhạc sĩ',
            'contract_id' => 'Hợp đồng',
            'contract_cps' => 'Số Phụ lục HĐ',
            'contract_number1' => 'Số HĐ Quyền liên quan',
            'contract_number2' => 'Số Phụ lục HĐ Quyền liên quan',
            'contract_number3' => 'Số thứ tự trong PL Quyền liên quan',
            'contract_number4' => 'Số HĐ Quyền tác giả',
            'contract_number5' => 'Số Phụ lục HĐ Quyền tác giả',
            'contract_number6' => 'Số thứ tự trong PL Quyền Tác giả',
            'money' => 'Giá tiền',
            'note' => 'Ghi chú',
            'album' => 'Album',
            'type_fee_id' => 'Loại hợp đồng',
        ],
        'films' => [
            'vn_name' => 'Tên tiếng Việt',
            'product_name' => 'Tên tiếng Anh',
            'actor_name' => 'Diễn viên chính',
            'country' => 'Quốc gia sản xuất',
            'nuoc' => 'Nước phát sóng',
            'year_create' => 'Năm sản xuất',
            'start_time' => 'Thời gian bắt đầu',
            'end_time' => 'Thời gian kết thúc',
            'contract_id' => 'Hợp đồng',
            'count_name' => 'Số đầu phim',
            'count_hour' => 'Số giờ phim',
            'count_tap' => 'Số tập phim',
            'partner_name' => 'Đối tác cung cấp',
            'partner_fee' => 'Chi phí đối tác',
            'other_fee' => 'Chi phí hậu kì',
            'total_fee' => 'Tổng chi phí',
            'director' => 'Đạo điễn',
            'note' => 'Ghi chú',
            'license_fee' => 'Chi phí bản quyền',
            'count_live' => 'Số lần phát sóng',
            'permissin_contract' => 'Quyền hạn sử dụng',

        ],

        'contracts' => [
            'contract_number' => 'Số hợp đồng',
            'title' => 'Tiêu đề',
            'date_sign' => 'Ngày kí',
            'total_money' => 'Tổng chi phí',
            'content' => 'Nội dung',
            'start_time' => 'Thời gian bắt đầu',
            'end_time' => 'Thời gian kết thúc',
            'note' => 'Tình trạng nội dung',
            'partner_id' => 'Đối tác',
            'user_name' => 'Người phụ trách',
            'user_msisdn' => 'Số điện thoại',
            'user_email' => 'Email',
            'report' => 'Theo tờ trình số',
        ]
    ];

    return (!isset($mapData[$table][$colName])) ? $colName : $mapData[$table][$colName];
}
