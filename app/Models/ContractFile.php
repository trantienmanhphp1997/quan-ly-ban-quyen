<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

class ContractFile extends BaseModel
{
    use HasFactory;
    protected $table = "contract_files";
    protected $fillable = [
        'name',
        'date_sign',
        'note',
        'contract_id',
        'link',
        'media_id',
        'categori_id',
    ];
    
    protected static function booted(){
        if(Auth::user()->is_manager != 1){
            static::addGlobalScope('checkManager', function (Builder $builder) {
                $builder->where('contract_files.admin_id', Auth::user()->id);
            });
        }
    }
}
