<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
use App\Models\RevenueType;
use App\Traits\FullTextSearch;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use OwenIt\Auditing\Contracts\Auditable; 
class Film extends BaseModel implements Auditable
{
    use HasFactory, FullTextSearch, SoftDeletes;
    use \OwenIt\Auditing\Auditable;
    protected $table = 'films';
    public $timestamps = 'fasle';
    protected $searchable = [
            'vn_name',
            'actor_name',
            'product_name',
    ];
    protected $fillable = [
    'vn_name','product_name', 'rating','partner_fee','license_fee', 'other_fee','total_fee','count_live','date_tv','date_vod', 'actor_name', 'date_license_start', 'date_license_end', 'country', 'year_create', 'admin_id', 'category_id', 'count_name', 'count_tap', 'count_hour', 'partner_name', 'permissin_contract', 'time_end', 'note', 'director','contract_id','end_time', 'start_time','link', 'addition_note', 'nuoc'];

    // protected static function booted(){
    //     if(Auth::user()->is_manager != 1){
    //         static::addGlobalScope('checkManager', function (Builder $builder) {
    //             $builder->where('films.admin_id', Auth::user()->id);
    //         });
    //     }
    // }

    public function contracts()
    {
        return $this->hasMany(Contract::class, 'media_id');
    }
    public function contract()
    {
        return $this->belongsTo(Contract::class, 'contract_id');
    }

    public function contractFiles()
    {
        return $this->hasMany(ContractFile::class, 'media_id')->where('categori_id', config("common.media.film"));
    }
    public function categories(){
        return $this->belongsToMany(CategoryFilm::class, 'film_list_categories', 'id_film_list','id_category_film' );
    }
    public function roles(){
        return $this->belongsToMany(RoleMedia::class, 'contract_media_role', 'media_id', 'role_id');
    }
    public function roleSales(){
        return $this->belongsToMany(RoleMedia::class, 'contract_media_sale_role','media_id', 'role_id' );
    }
    public function filmCountries(){
        return $this->belongsToMany(Country::class,'film_country','media_id','country_id');
    }

    public function countryProduction(){
        return $this->belongsTo(Country::class, 'country');
    }

    public function saleRevenueByType($typeId){
        return isset(DB::table('revenue')->where('media_id', '=', $this->id)->where('type_id','=',$typeId)->get()->first()->moneyVAT)?DB::table('revenue')->where('media_id', '=', $this->id)->where('type_id','=',$typeId)->sum('moneyVAT'):'0';
    }

    public function sumRevenueByParent($typeId){

        $childrenTypeId = RevenueType::where('parent_id', $typeId)->pluck('id');
        return isset(DB::table('revenue')->where('media_id', '=', $this->id)->whereIn('type_id',$childrenTypeId)->get()->first()->money)?DB::table('revenue')->where('media_id', '=', $this->id)->whereIn('type_id', $childrenTypeId)->sum('money'):'0';
    }

    public function contractRole(){
        return $this->hasMany(ContractMediaRole::class,'media_id');
    }

    public function revenueType(){
        return $this->belongsToMany(RevenueType::class, 'film_revenue_type', 'media_id', 'revenue_type_id')->withPivot('money');
    }

    public function revenueByType($typeId){
        return isset(DB::table('film_revenue_type')->where('media_id', '=', $this->id)->where('revenue_type_id','=',$typeId)->get()->first()->money)?DB::table('film_revenue_type')->where('media_id', '=', $this->id)->where('revenue_type_id','=',$typeId)->get()->first()->money:'0';
    }

    public function minRevenue(){
        return DB::table('film_revenue_type')->where('media_id', '=', $this->id)->sum('money');
    }

    public function saleRevenue(){
        return DB::table('revenue')->where('media_id', '=', $this->id)->sum('moneyVAT');
    }
}
