<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\BaseModel;

class RoleMedia extends BaseModel
{
    use HasFactory;
    protected $fillable = ['id','name','code','parent_id','categori_id','type','admin_id', 'level', 'country_id','nuoc'];
    public static function getRoleMedia(){
        $data = DB::table('role_media')->select('id','name','code','parent_id','category_id','type','admin_id')->get();
        return $data;
    }
}
