<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Film;

class FilmList extends Model
{
    protected $table = 'film_lists';
    protected $fillable=[
        'name',
        'vn_name',
        'year_create',
        'box_office_revenue',
        'category_film',
        'permission',
        'actor_name',
        'director',
        'count_minute',
        'note',
        'admin_id',
    ];
    use HasFactory, SearchableTrait;
    protected $searchable = [
        'columns' => [
            'film_lists.name'  => 10,
            'film_lists.vn_name'  => 10,
            'film_lists.actor_name' => 10
        ]
    ];

    protected static function booted(){
        if(Auth::user()->is_manager != 1){
            static::addGlobalScope('checkManager', function (Builder $builder) {
                $builder->where('film_lists.admin_id', Auth::user()->id);
            });
        }
    }

    public function film()
    {
        return $this->belongsTo(Film::class, 'film_id');
    }
}
