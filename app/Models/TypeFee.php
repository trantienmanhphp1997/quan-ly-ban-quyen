<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeFee extends Model
{
    use HasFactory;
    protected $table = 'type_fee';
    public $timestamps = false;
    protected $fillable = [
        'name',
        'name_unique',
    ];
}
