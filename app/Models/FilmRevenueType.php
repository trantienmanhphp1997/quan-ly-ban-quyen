<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FilmRevenueType extends BaseModel
{
    use HasFactory;
    protected $table = 'film_revenue_type';

 	protected $fillable = [
        'id',
        'media_id',
        'revenue_type_id',
        'money',
	];
}
