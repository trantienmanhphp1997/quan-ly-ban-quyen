<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
use App\Models\Contract;
// use Nicolaslopezj\Searchable\SearchableTrait;
use App\Traits\FullTextSearch;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use App\Models\TypeFee;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable; 
class Music extends BaseModel implements Auditable
{
    use HasFactory, FullTextSearch, SoftDeletes;
    use \OwenIt\Auditing\Auditable;
    protected $table = 'music';

    protected $fillable = [
        'name', 'singer', 'musician', 'album', 'contract_number1', 'contract_number2', 'contract_number3', 'contract_number4', 'contract_number5', 'contract_number6', 'contract_cps', 'date_live', 'date_end', 'money', 'note', 'music_id', 'cho_docquyen' ,'cho_tacquyen', 'so_docquyen', 'so_tacquyen', 'sale_permission', 'ghi_am', 'tac_gia', 'end_time', 'start_time', 'type_fee_id', 'contract_id','warning_status'
    ];

    protected $searchable = [
            'music.name',
            'music.singer',
            'music.musician',
    ];

    // protected static function booted(){
    //     if(Auth::user()->is_manager != 1){
    //         static::addGlobalScope('checkManager', function (Builder $builder) {
    //             $builder->where('music.admin_id', Auth::user()->id);
    //         });
    //     }
    // }

    public function contractFiles()
    {
        return $this->hasMany(ContractFile::class, 'media_id')->where('categori_id', config("common.media.music"));
    }

    public function typeFee()
    {
        return $this->belongsTo(TypeFee::class, 'type_fee_id');
    }

    public function contract()
    {
        return $this->belongsTo(Contract::class, 'contract_id');
    }

}
