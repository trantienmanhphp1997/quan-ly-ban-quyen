<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

class ContractMediaSaleMoney extends BaseModel
{
    use HasFactory;
    protected $table='contract_media_sale_money';
    protected $fillable=['contract_id','media_id','money','moneyVAT','category_id','start_time'
    ,'end_time','admin_id', 'cho_tac_quyen', 'cho_doc_quyen', 'so_tac_quyen', 'so_doc_quyen', 'sale_3rd', 'nuoc','link', 'admin_id', 'note','count_live', 'ma_nhac_cho','note_film','status', 
    'name_khong_dau','singer_khong_dau',
	];

    // protected static function booted(){
    //     if(Auth::user()->is_manager != 1){
    //         static::addGlobalScope('checkManager', function (Builder $builder) {
    //             $builder->where('contract_media_sale_money.admin_id', Auth::user()->id);
    //         });
    //     }
    // }

    public function roles(){
        return $this->belongsToMany(RoleMedia::class,'contract_media_sale_role','sale_money_id','role_id');
    }

    public function roleSales()
    {
        return $this->hasMany(ContractMediaSaleRole::class, 'sale_money_id');
    }

    public function revenue()
    {
        return $this->hasOne(Revenue::class, 'sale_money_id');
    }
}
