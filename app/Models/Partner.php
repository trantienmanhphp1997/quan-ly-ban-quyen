<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\BaseModel;
class Partner extends BaseModel
{
    use HasFactory;
    protected $table = 'partners';
    protected $fillable = ['name','email','address','phone','note','admin_id','common_name','country_id'];
    public static function getPartner(){
        $data = DB::table('partners')->select('id','name','email','address','phone','note','admin_id','common_name','country_id')->get();
        return $data;
    }
}
