<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MusicUpload extends BaseModel
{
    use HasFactory;
    protected $table = 'music_upload';

	 protected $fillable = [
	    'STT', 'ten_bai_hat', 'ca_si', 'nhac_si', 'album', 
            'doc_quyen', 
            'tac_quyen',
            'doc_quyen_so', 
            'tac_quyen_so',  
            'ban_doi_tac_3',   
            'loai_hd',
            'quyen_ghi_am',  
            'quyen_tac_gia',
            'phu_luc_hd',
            'gia_tien',
            'so_hd_lq',
            'pl_hd',
            'stt_phu_luc',
            'hd_tac_gia',
            'pl_quyen_tac_gia',
            'stt_pl_quyen_tac_gia',
            'time_release',
            'time_end',
            'mv',
            'audio',
            'note',
            'id_ms',
            'status',
            'admin_id',
	];
}
