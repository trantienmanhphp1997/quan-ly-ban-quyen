<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\BaseModel;
use Illuminate\Support\Facades\DB;

class Revenue extends BaseModel
{
    use HasFactory;
    protected $table = 'revenue';
    protected $fillable = ['id', 'contract_id', 'type_contract', 'media_id', 'category_id', 'admin_id','type_id',
        'date_sign','money','moneyVAT', 'type_revenue','sale_money_id'];
    public static function getRevenueType()
    {
        $data = DB::table('revenue')->select('id', 'contract_id', 'type_contract', 'media_id', 'category_id', 'admin_id','type_id',
            'date_sign','money', 'type_revenue')->get();
        return $data;
    }

    public function youtube()
    {
        return isset(Revenue::where('sale_money_id', '=', $this->sale_money_id)->where('type_id', '=', 3)->get()->first()->money)?Revenue::where('sale_money_id', '=', $this->sale_money_id)->where('type_id', '=', 3)->get()->first()->money:0;
    }

    public function vod_nuoc_ngoai()
    {
        return isset(Revenue::where('sale_money_id', '=', $this->sale_money_id)->where('type_id', '=', 4)->get()->first()->money)?Revenue::where('sale_money_id', '=', $this->sale_money_id)->where('type_id', '=', 4)->get()->first()->money:0;
    }
    public function vod_noi_bo()
    {
        return isset(Revenue::where('sale_money_id', '=', $this->sale_money_id)->where('type_id', '=', 5)->get()->first()->money)?Revenue::where('sale_money_id', '=', $this->sale_money_id)->where('type_id', '=', 5)->get()->first()->money:0;
    }

    public function vod_ngoai()
    {
        return isset(Revenue::where('sale_money_id', '=', $this->sale_money_id)->where('type_id', '=', 6)->get()->first()->money)?Revenue::where('sale_money_id', '=', $this->sale_money_id)->where('type_id', '=', 6)->get()->first()->money:0;
    }

    public function th_noi_bo()
    {
        return isset(Revenue::where('sale_money_id', '=', $this->sale_money_id)->where('type_id', '=', 7)->get()->first()->money)?Revenue::where('sale_money_id', '=', $this->sale_money_id)->where('type_id', '=', 7)->get()->first()->money:0;
    }

    public function th_ngoai()
    {
        return isset(Revenue::where('sale_money_id', '=', $this->sale_money_id)->where('type_id', '=', 8)->get()->first()->money)?Revenue::where('sale_money_id', '=', $this->sale_money_id)->where('type_id', '=', 8)->get()->first()->money:0;
    }

    public function saleRevenue(){
        return DB::table('revenue')->where('sale_money_id','=', $this->sale_money_id)->sum('money');
    }

    public function film()
    {
        return $this->belongsTo(Film::class, 'media_id');
    }

    public function contract()
    {
        return $this->belongsTo(Contract::class, 'contract_id');
    }

    public function media_sale()
    {
        return $this->belongsTo(ContractMediaSaleMoney::class, 'sale_money_id');
    }

    public function revenueType()
    {
        return $this->belongsTo(RevenueType::class, 'type_id');
    }
}
