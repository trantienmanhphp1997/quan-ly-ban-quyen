<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;

class ContractMusicSaleUpload extends BaseModel
{
    use HasFactory;
    protected $table = "contract_music_sale_uploads";
    protected $fillable=[
        'STT',
        'ma_nhac_cho',
        'name_khong_dau',
        'singer_khong_dau',
        'name',
        'singer',
        'musician',
        'cho_docquyen',
        'cho_tacquyen',
        'so_docquyen',
        'so_tacquyen',
        'ghi_am',
        'tac_gia',
        'contract_cps',
        'contract_number1',
        'contract_number2',
        'contract_number3',
        'contract_number4',
        'contract_number5',
        'contract_number6',
        'start_time',
        'end_time',
        'music_id',
        'status',
        'admin_id',
    ];
}
