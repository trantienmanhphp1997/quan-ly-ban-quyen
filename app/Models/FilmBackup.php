<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

class FilmBackup extends BaseModel
{
    use HasFactory;
	protected $table = 'film_backups';
    protected $fillable = [
    'driver', 'backup_driver', 'file_name', 'film_name', 'vn_name', 'country', 'partner', 'series_film', 'count_film','file_type','time_number', 'volumn', 'file_format', 'end_time', 'file_delivery_person', 'admin_id','status'];

    protected static function booted(){
        if(Auth::user()->is_manager != 1){
            static::addGlobalScope('checkManager', function (Builder $builder) {
                $builder->where('film_backups.admin_id', Auth::user()->id);
            });
        }
    }
}
