<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FilmListUpload extends BaseModel
{
    use HasFactory;
    protected $table = 'film_list_uploads';

 	protected $fillable = [
 		'id',
 		'STT',
 		'name',
 		'vn_name',
 		'year_create',
 		'box_office_revenue',
 		'permission',
 		'category_film',
 		'actor_name',
 		'director',
 		'count_minute',
 		'note',
 		'status',
	];
}
