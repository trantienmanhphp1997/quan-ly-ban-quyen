<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class contractor_selections extends Model
{
    use HasFactory;

    protected $table = "contractor_selections";
    protected $fillable = [
        'contract_number',
        'date_sign',
        'plan',
        'result'
    ];
}
