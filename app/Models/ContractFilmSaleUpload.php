<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
class ContractFilmSaleUpload extends BaseModel
{
    use HasFactory;
    protected $table = "contract_film_sale_uploads";
    protected $fillable=[
        'STT',
        'name',
        'vn_name',
        'contract_number',
        'partner_name',
        'employee_name',
        'telephone_number',
        'email',
        'country',
        'count',
        'ĐQ',
        'KĐQ',
        'HT',
        'country_ps',
        'country_note_ps',
        'revenu_type',
        'money',
        'moneyVAT',
        'start_time',
        'end_time',
        'date_sign',
        'note',
        'status',
        'admin_id',
    ];
}
