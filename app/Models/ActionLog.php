<?php

namespace App\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActionLog extends BaseModel
{
    use HasFactory;

    public $fillable = ['admin_id'];

    public static function writeActionLog($record_id, $table, $action, $module, $action_log, $old_value, $new_value, $file)
    {
        $log = new ActionLog();
        $log->record_id = $record_id;
        $log->model = $table;
        $log->action_log = $action_log;
        $log->module = $module;
        $log->old_value = ($old_value)?json_encode($old_value, JSON_UNESCAPED_UNICODE):'';
        $log->new_value = ($old_value)?json_encode($new_value, JSON_UNESCAPED_UNICODE):'';
        $log->file = $file;
        $log->created_at = date("Y-m-d H:i:s", time()+7*3600);
        $log->save();
    }

    public function admin()
    {
        return $this->belongsTo(User::class, 'admin_id');
    }

    public function films()
    {

    }

    public function musics()
    {

    }
}
