<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
use Illuminate\Support\Facades\DB;
class RevenueType extends BaseModel
{
    use HasFactory;
    protected $table = 'revenue_type';
    protected $fillable = ['id', 'name', 'parent_id', 'note', 'admin_id'];

    public static function getRevenueType()
    {
        $data = DB::table('revenue_type')->select('id', 'name', 'parent_id', 'note', 'admin_id')->get();
        return $data;
    }

    public static function parent()
    {
        $listParentId = RevenueType::where('parent_id', '!=', 0)->groupBy('parent_id')->select('parent_id');
        $listParent = RevenueType::whereIn('id', $listParentId)->get();
        return $listParent;
    }

    public function getParent(){
        return $this->belongsTo(RevenueType::class, 'parent_id');
    }
}
