<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
use App\Models\RoleMedia;
class ContractMediaRole extends BaseModel
{
    use HasFactory;
    protected $table = "contract_media_role";
    protected $fillable=['contract_id','role_id', 'media_id','category_id','admin_id','start_time','end_time','cho_tac_quyen'
    ,'cho_doc_quyen','so_tac_quyen','so_doc_quyen','sale_3rd'];

    public function roleMedia(){
        return $this->belongsTo(RoleMedia::class, 'role_id');
    }
}
