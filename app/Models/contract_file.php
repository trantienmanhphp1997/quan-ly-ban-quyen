<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\BaseModel;
use Nicolaslopezj\Searchable\SearchableTrait;

class contract_file extends BaseModel
{
    use HasFactory, SearchableTrait;
    protected $table = "contract_files";
    protected $fillable = [
        'name',
        'date_sign',
        'note',
        'contract_id',
        'link',
        'categori_id',
        'media_id',
    ];
    public function categories(){
        return $this->hasOne(Category::class, 'categori_id');
    }
    protected $searchable = [
        'columns' => [
            'contract_files.name'  => 10,
            'contract_files.contract_id'  => 10,
            'contract_files.categoti_id' => 10,
        ]
    ];
}
