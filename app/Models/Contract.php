<?php

namespace App\Models;

use App\Models\ContractFile;
use App\Models\Category;
use App\Models\Partner;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable; 
class Contract extends BaseModel implements Auditable
{
    use HasFactory, SearchableTrait, SoftDeletes;
    use \OwenIt\Auditing\Auditable;
    protected $guarded = ['*'];
    protected $table = "contracts";
    protected $fillable = [
        'contract_number',
        'date_sign',
        'total_money',
        'content',
        'user_name',
        'user_msisdn',
        'user_email',
        'admin_id',
        'status',
        'type',
        'admin_update_id',
        'category_id',
        'partner_id',
        'end_time',
        'start_time',
        'note',
        'title',
        'media_id',
        'report',
    ];

    protected $searchable = [
        'columns' => [
            'contracts.contract_number' => 10,
            'contracts.user_name' => 10,
            'contracts.title' => 10,
        ],
    ];

    public function contractFiles()
    {
        return $this->hasMany(ContractFile::class);
    }

    protected static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            if ($model->date_sign) {
                $model->date_sign = date('Y-m-d', strtotime($model->date_sign));
            }
        });
        self::updating(function ($model) {
            if ($model->date_sign) {
                $model->date_sign = date('Y-m-d', strtotime($model->date_sign));
            }
        });
    }

    protected static function booted(){
        if(Auth::user()->is_manager != 1){
            static::addGlobalScope('checkManager', function (Builder $builder) {
                $builder->where('contracts.admin_id', Auth::user()->id);
            });
        }
    }

    public function films()
    {
        return $this->hasMany(Film::class, 'contract_id');
    }

    public function musics()
    {
        return $this->hasMany(Music::class, 'contract_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function partner()
    {
        return $this->belongsTo(Partner::class, 'partner_id');
    }

}
