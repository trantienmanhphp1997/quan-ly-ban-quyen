<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FilmListCategory extends Model
{
    use HasFactory;
    protected $table='film_list_categories';
    protected $fillable=['id_film_list','id_category_film'];
}
