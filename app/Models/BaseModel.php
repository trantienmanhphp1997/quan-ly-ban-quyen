<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

//diepth2
// apply for table with admin_id; start_time; end_time
abstract class BaseModel extends Model
{
	protected static function boot()
		{
		parent::boot();
		self::creating(function($model) {
		 	$model->admin_id = auth()->id();
            if($model->start_time){
                $model->start_time = date('Y-m-d',strtotime($model->start_time));
            }
            if($model->end_time){
                $model->end_time = date('Y-m-d',strtotime($model->end_time));
            }
		});
        self::updating(function($model) {
            if($model->start_time){
                $model->start_time = date('Y-m-d',strtotime($model->start_time));
            }
            if($model->end_time){
                $model->end_time = date('Y-m-d',strtotime($model->end_time));
            }
        });
	}
    // protected static function booted()
    //     {
    //         if(Auth::user()->is_manager != 1){
    //             static::addGlobalScope('checkManager', function (Builder $builder) {
    //             $builder->where('admin_id', Auth::user()->id);
    //         }); 
    //     }
    // }
}