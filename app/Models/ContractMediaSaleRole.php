<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;

class ContractMediaSaleRole extends BaseModel
{
    use HasFactory;
    protected $table = "contract_media_sale_role";
    protected $fillable=['contract_id','role_id', 'media_id','category_id','admin_id','start_time','end_time','category_id', 'sale_money_id'];
}
