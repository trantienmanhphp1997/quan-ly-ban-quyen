<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use DB;

class employees extends Model
{
    use HasFactory;
    protected $fillable = ['firstname','lastname','email','salary','phone'];

    public static function getEmployees(){
    	$data = DB::table('employees')->select('id','firstname','lastname','email','salary','phone')->get();
    	return $data;
    }

}
