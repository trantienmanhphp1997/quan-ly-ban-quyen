<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Nicolaslopezj\Searchable\SearchableTrait;
// Chay lenh: composer require nicolaslopezj/searchable

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasRoles, SearchableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'users';
    protected $searchable = [
        'columns' => [
            'users.username'  => 10,
            'users.full_name'  => 10,
        ]
    ];

    protected $fillable = [
        'username',
        'full_name',
        'email',
        'password',
        'is_manager',
        'phone_number',
        'remember_token',
        'category_id',
        'status_media',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function roles(){
        return $this->belongsToMany(Role::class, 'model_has_roles', 'model_id', 'role_id');
    }

}
