<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FilmCountry extends Model
{
    use HasFactory;
    protected $table='film_country';
    protected $fillable=['country_id','media_id','type', 'contract_id','sale_money_id'];
}
