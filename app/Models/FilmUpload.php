<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FilmUpload extends BaseModel
{
    use HasFactory;
    protected $table = 'film_uploads';
	protected $fillable = [
		"id",
		"STT",
		"country",
		"vn_name",
		"product_name",
		"year_create",
		"category",
		"count_name",
		"count_tap",
		"count_hour",
		"partner_name",
		"DQ",
		"KDQ",
		"start_time1",
		"end_time1",
		"contract_number",
		"fee1",
		"fee2",
		"fee3",
		"note",
		"actor_name",
		"director",
		"contract_number_root",
		"status",
		"admin_id",
		"broadcast_country",
		"nuoc",
		"count_live",
		"Broadcast_infrastructure",
		"addition_note",
		"youtube",
		"VOD_nuoc_ngoai",
		"VOD_noi_bo",
		"VOD_ngoai",
		"TH_noi_bo",
		"TH_ngoai",
		'product_name',
	];
}
