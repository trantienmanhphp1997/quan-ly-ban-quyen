<?php

namespace App\Exports;

use App\Models\Music;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
class MusicSaleExport implements FromArray, WithHeadings, WithEvents,WithCustomStartCell
{
    protected $data;
    protected $dataNew;
    function __construct($data) {
        $this->data = $data;
    }
    public function array():array{	
    	ini_set('memory_limit', '-1');
    	set_time_limit(0);
        $this->data = json_decode($this->data, true);
        $this->dataNew = array_filter($this->data, function($v, $k) {
            foreach($v as $key=>$value){
                return $value;
            }
        }, ARRAY_FILTER_USE_BOTH);
        $data1 = [];
        array_push($data1, $this->dataNew);
        return $data1;
    }
	public function headings():array{
		return [
		];
	}
     public function map($data): array {
        return [
            $data,
        ];
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->getDelegate()->setCellValue('A1', 'Danh sách lỗi chi tiết');
                $event->sheet->getDelegate()->setCellValue('B1', 'Các bản ghi bị lỗi');
                $arrayTextError = [
                    'Thiếu thời gian phát hành hoặc Thiếu thời hạn bản quyền',
                    'Thời gian phát hành lớn hơn hoặc bằng thời hạn bản quyền',
                    'Thiếu quyền liên quan',
                    'Số hợp đồng đã tồn tại và không phải hợp đồng mua nhạc',
                    'Số hợp đồng đã tồn tại và không phải hợp đồng tác giả',
                ];
                $dem = 0;
                $data = $this->data;
                foreach($data as $key => $value){
                    if(in_array($value,$this->dataNew)){
                        $event->sheet->getDelegate()->setCellValue('A'.($dem+2),$arrayTextError[$key]);
                        $dem++;
                    }
                }
                $default_font_style = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12]
                ];
                $default_font_style_title = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12, 'bold' =>  true],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'startColor' => [
                            'argb' => 'B7DEE8',
                        ],
                        'endColor' => [
                            'argb' => 'B7DEE8',
                        ],
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '00000000'],
                        ],
                    ],
                ];

                $active_sheet = $event->sheet->getDelegate();
                $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
                $active_sheet->getParent()->getDefaultStyle()->getAlignment()->applyFromArray(
                    array('horizontal' => 'left')
                );
                $arrayAlphabet = [
                    'C', 'D', 'E'
                ];  
                foreach ($arrayAlphabet as $alphabet) {
                    $event->sheet->getColumnDimension($alphabet)->setWidth(30);
                }
                $event->sheet->getColumnDimension('A')->setWidth(100); 
                $event->sheet->getColumnDimension('B')->setWidth(50); 
                $active_sheet->getStyle('A1:B1')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('A1:B1')->applyFromArray($default_font_style_title);


                $style = array(
                    'alignment' => array(
                        'horizontal' => 'left',
                    ),
                    'font' => ['name' => 'Times New Roman', 'size' => 12, 'bold' =>  true],
                );
                $event->sheet->getStyle("A2:A10")->applyFromArray($style);
                $event->sheet->getStyle("C1:C1")->applyFromArray($style);
            },
        ];
    }
    public function startCell(): string
    {
        return 'B2';
    }
}
