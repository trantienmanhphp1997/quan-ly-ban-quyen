<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Models\Film;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use DB;

class CopyrightSheetThird implements FromView, WithEvents{
    use RegistersEventListeners;
    public function view(): View
    {
        $countryNumberPhimLe =Film::whereNotNull('country')->where('count_tap', 1)->orderBy('country')->groupBy('country')->select(DB::raw('count("country") as num'))->pluck('num')->toArray();
        $countryNumberPhimBo =Film::whereNotNull('country')->where('count_tap', '!=', 1)->orderBy('country')->groupBy('country')->select(DB::raw('count("country") as num'))->pluck('num')->toArray();
        $indexMergePhimBo = $countryNumberPhimBo;
        for($i = 1; $i < count($indexMergePhimBo); $i++){
            $indexMergePhimBo[$i] = $indexMergePhimBo[$i] + $indexMergePhimBo[$i-1]?? 0;

        }
        $indexMergePhimLe = $countryNumberPhimLe;
        for($i = 1; $i < count($indexMergePhimLe); $i++){
            $indexMergePhimLe[$i] = $indexMergePhimLe[$i] + $indexMergePhimLe[$i-1]?? 0;

        }
        $phimLe =Film::whereNotNull('country')->where('count_tap', 1)->orderBy('country')->get();
        $phimBo =Film::whereNotNull('country')->where('count_tap', '!=', 1)->orderBy('country')->get();

        return view('livewire.copyright._tabThird', compact('phimLe', 'phimBo', 'countryNumberPhimLe' ,'countryNumberPhimBo', 'indexMergePhimBo', 'indexMergePhimLe'));
    }

    public function registerEvents(): array
    {
        return [

            AfterSheet::class => function(AfterSheet $event) {
                $default_font_style2 = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '00000000'],
                        ],
                    ],
                ];
                $active_sheet = $event->sheet->getDelegate();
                $active_sheet->getStyle('B2:B1000')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                );
                $active_sheet->getStyle('A8:A1000')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                );
                $active_sheet->getStyle('A3:Z5')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                );
                $event->sheet->getStyle('A3:Z5')->getAlignment()->setWrapText(true);
                $active_sheet->getStyle('A3:Z107')->applyFromArray($default_font_style2);
            },
        ];
    }
}
