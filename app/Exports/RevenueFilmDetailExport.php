<?php

namespace App\Exports;

use App\Models\Film;
use App\Models\Revenue;
use Maatwebsite\Excel\Concerns\FromCollection;
use DB;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;

class RevenueFilmDetailExport implements FromCollection, WithHeadings, WithMapping, WithEvents, WithCustomStartCell
{
    /**
     * @return \Illuminate\Support\Collection
     */

    protected $searchNumber;
    protected $searchName;
    protected $searchDate;
    protected $searchCategory;
    protected $searchCountry;
    
    public function __construct($searchNumber, $searchName, $searchDate, $searchCategory, $searchCountry){
        $this->searchNumber= trim($searchNumber);
        $this->searchName= trim($searchName);
        $this->searchCountry= trim($searchCountry);
        $this->searchCategory= $searchCategory;
        $this->setDate = $searchDate;
    }

    public function collection()
    {
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        $query = Revenue::where('revenue.category_id',2);
        if(auth()->user()->is_manager == 0) $query->where('revenue.admin_id', auth()->user()->id);
        $query->leftjoin('contracts','contracts.id','=','revenue.contract_id');
        $query->leftjoin('films','films.id','=','revenue.media_id');
        $query->leftjoin('countries','films.country', '=', 'countries.id');
        $query->select('revenue.*', 'films.license_fee','contracts.contract_number as con_num','films.vn_name as vn_name');
        if ($this->searchName) $query->where('films.vn_name','like','%'.trim($this->searchName).'%');
        if ($this->searchNumber) $query->whereRaw("contracts.contract_number LIKE '%{$this->searchNumber}%'");
        if ($this->searchName) $query->where('films.vn_name','like','%'.trim($this->searchName).'%');
        if ($this->searchCategory == 2) $query->where('films.count_tap','!=', 1);
        if ($this->searchCategory == 1) $query->where('films.count_tap', 1);
        if ($this->searchCountry) $query->where('countries.id', $this->searchCountry);
        if(!empty($this->setDate)){
            $getDate = $this->setDate ? explode(" - ", $this->setDate) : null;
            if ($getDate != '') {
                $startDateCharge = $getDate[0];
                $endDateCharge = $getDate[1];
                if ($startDateCharge != '' && $endDateCharge != '') {
                    $start_time = date("Y-m-d", strtotime($startDateCharge));
                    $end_time = date("Y-m-d", strtotime($endDateCharge));
                    $query->whereBetween('revenue.date_sign', [$start_time, $end_time]);
                }
            }
        }

        $query= $query->groupBy('sale_money_id');
        $revenue=$query->orderBy('revenue.id','desc')->get();
        $key=1;

        foreach ($revenue as $val ) {
            $haTangMua='';
            $haTangBan='';
            $val->stt=$key++;
            $val->total_money = DB::table('revenue')->where('sale_money_id','=', $val->sale_money_id)->sum('money');
            if($val->film) {
                foreach($val->film->roles as $role) {
                    if($role->level==1) $haTangMua.=$role->code.', ';
                    $val->haTangMua= trim($haTangMua,', ');
                }
            }
            foreach($val->media_sale->roles as $role) {
                if($role->level==1) $haTangBan.=$role->code.', ';
                $val->haTangBan= trim($haTangBan,', ');
            }
        }
        return $revenue;
    }

    public function headings(): array
    {
        return [
            'STT',
            'Tên phim tiếng Việt - Tên phim tiếng Anh',
            'Số hợp đồng',
            'Đối tác',
            'Tổng chi phí mua',
            'Doanh thu bán',
            'Loại doanh thu',
            'Hạ tầng phát sóng mua',
            'Hạ tầng phát sóng bán',
            'Thời gian bắt đầu',
            'Thời gian kết thúc',
            'Ngày ký HĐ',
        ];
    }
    
    public function map($revenue):array{
        return [
            $revenue->stt,
            $revenue->film->product_name?$revenue->vn_name.' - '.$revenue->film->product_name:$revenue->vn_name,
            $revenue->con_num,
            $revenue->contract->partner->name??'',
            countChiPhiTap($revenue->film),
            $revenue->moneyVAT??'0',
            $revenue->revenueType->name,
            $revenue->haTangMua,
            $revenue->haTangBan,
            reFormatDate($revenue->media_sale->start_time),
            reFormatDate($revenue->media_sale->end_time),
            reFormatDate($revenue->contract->date_sign),
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event
                    ->sheet
                    ->getDelegate()
                    ->setMergeCells([
                        'A2:F2',
                        'A3:F3',
                        'A6:F6',
                    ]);
                    ;
                    // ->freezePane('A3');
                $event->sheet->getRowDimension(9)->setRowHeight(30);
                $event->sheet->getDelegate()->setCellValue('A2', 'CÔNG TY TRUYỀN THÔNG VIETTEL');
                $event->sheet->getDelegate()->setCellValue('A3', 'Quản lý kho nội dung');
                $event->sheet->getDelegate()->setCellValue('A6', 'THỐNG KÊ DOANH THU PHIM (Chi tiết)');

                $event->sheet->getDelegate()->setCellValue('A9', 'STT');
                $event->sheet->getDelegate()->setCellValue('B9', 'Tên phim tiếng Việt - Tên phim tiếng Anh');
                $event->sheet->getDelegate()->setCellValue('C9', 'Số hợp đồng');
                $event->sheet->getDelegate()->setCellValue('D9', 'Đối tác');
                $event->sheet->getDelegate()->setCellValue('E9', 'Tổng chi phí mua/tập');
                $event->sheet->getDelegate()->setCellValue('F9', 'Doanh thu bán');
                $event->sheet->getDelegate()->setCellValue('G9', 'Loại doanh thu');
                $event->sheet->getDelegate()->setCellValue('H9', 'Hạ tầng phát sóng mua');
                $event->sheet->getDelegate()->setCellValue('I9', 'Hạ tầng phát sóng bán');
                $event->sheet->getDelegate()->setCellValue('J9', 'Thời gian bắt đầu');
                $event->sheet->getDelegate()->setCellValue('K9', 'Thời gian kết thúc');
                $event->sheet->getDelegate()->setCellValue('L9', 'Ngày kí hợp đồng');


                $default_font_style = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12]
                ];
                $default_font_style_title = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12, 'bold' =>  true],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'startColor' => [
                            'argb' => 'B7DEE8',
                        ],
                        'endColor' => [
                            'argb' => 'B7DEE8',
                        ],
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '00000000'],
                        ],
                    ],
                ];
                $default_font_style_title2 = [
                    'font' => ['name' => 'Times New Roman', 'size' => 14, 'bold' =>  true],
                ];
                $event->sheet->getStyle('A9:AZ9')->getAlignment()->setWrapText(true);

                $active_sheet = $event->sheet->getDelegate();
                $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
                $active_sheet->getParent()->getDefaultStyle()->getAlignment()->applyFromArray(
                    array('horizontal' => 'left')
                );
                $arrayAlphabet = [
                    'B',
                ];
                foreach ($arrayAlphabet as $alphabet) {
                    $event->sheet->getColumnDimension($alphabet)->setWidth(40);
                }
                $arrayAlphabet = [
                    'C', 'D', 'E', 'F', 'G', 'H',
                    'I', 'J', 'K', 'L'
                ];
                foreach ($arrayAlphabet as $alphabet) {
                    $event->sheet->getColumnDimension($alphabet)->setWidth(20);
                }

                $event->sheet->getColumnDimension('A')->setWidth(5);
                $arrayAlphabet = [
                    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                    'I', 'J', 'K', 'L'
                ];
                foreach ($arrayAlphabet as $alphabet) {
                    $newCell =  $alphabet.'9:'.$alphabet.'9';
                    $active_sheet->getStyle($newCell)->getAlignment()->applyFromArray(
                        array('horizontal' => 'center', 'vertical'=>'center')
                    );
                    $active_sheet->getStyle($newCell)->applyFromArray($default_font_style_title);
                }
                $active_sheet->getStyle('A2:F2')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                );
                $active_sheet->getStyle('A2:F2')->applyFromArray($default_font_style_title2);
                $active_sheet->getStyle('A3:F3')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                );
                $active_sheet->getStyle('A3:F3')->applyFromArray($default_font_style_title2);
                $active_sheet->getStyle('A6:F6')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                );
                $active_sheet->getStyle('A6:F6')->applyFromArray($default_font_style_title2);
            },
        ];
    }
    public function startCell(): string
    {
        return 'A9';
    }
}
