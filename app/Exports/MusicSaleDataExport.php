<?php

namespace App\Exports;

use App\Models\Music;
use App\Models\ContractMediaSaleMoney;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Excel;
use Carbon\Carbon;
use Illuminate\Support\Collection;
class MusicSaleDataExport implements FromCollection,WithMapping,WithEvents, WithCustomStartCell
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use RegistersEventListeners;
    public $stt = 0;

    protected $searchTerm1;
    protected $contract_id;
    protected $countRow = 0;
    function __construct($contract_id, $searchTerm1) {
        $this->searchTerm1 = trim($searchTerm1);
        $this->contract_id = trim($contract_id);
    }

    public function collection()
    {
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        // return collect([]);
        $query = ContractMediaSaleMoney::where('contract_media_sale_money.category_id',1)
            ->where('contract_media_sale_money.contract_id',$this->contract_id)
            ->leftJoin('music','music.id','contract_media_sale_money.media_id')
            ->leftJoin('contracts','contracts.id','contract_media_sale_money.contract_id')
            ->leftJoin('contracts as ct','ct.id','music.contract_id')
            ->select('contract_media_sale_money.*','music.name as music_name', 'music.singer','music.musician',
            'contracts.contract_number', 'music.contract_number2','music.tac_gia', 'music.music_id',
            'music.contract_number5','music.contract_number3','music.contract_number6','ct.contract_number as ct_contract_number');
        if($this->searchTerm1){
            $query->where(function($query){
                $query->where('music.name', 'like', '%'.$this->searchTerm1.'%');
                $query->orWhere('music.singer','like','%'.$this->searchTerm1.'%');
            });
        }
        $music= $query->get();
        // dd($music);
        $this->countRow = count($music);
        return $music;
    }
    public function map($music): array {
        return [
            ++$this->stt,
            $music->contract_number,
            $music->ma_nhac_cho,
            $music->name_khong_dau,
            $music->singer_khong_dau,
            $music->music_name,
            $music->singer,
            $music->musician,
            $music->cho_doc_quyen?'x':'',
            $music->cho_tac_quyen?'x':'',
            $music->so_doc_quyen?'x':'',
            $music->so_tac_quyen?'x':'',
            $music->ct_contract_number,
            $music->contract_number2,
            $music->contract_number3,
            $music->tac_gia,
            $music->contract_number5,
            $music->contract_number6,

            $music->start_time? reFormatDate($music->start_time,'d/m/Y'):'',
            $music->end_time? reFormatDate($music->end_time,'d/m/Y'):'',
            $music->music_id,
        ];
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event
                    ->sheet
                    ->getDelegate()
                    ->setMergeCells([
                        'A8:A10',
                        'B8:B10',
                        'C8:C10',
                        'D8:D10',
                        'E8:E10',
                        'F8:F10',
                        'G8:G10',

                        'I8:J8',
                        'K8:L8',
                        'L9:L10',
                        'I9:I10',
                        'J9:J10',
                        'K9:K10',

                        'H8:H10',
                        'M8:M10',
                        'N8:N10',
                        'O8:O10',
                        'P8:P10',
                        'Q8:Q10',
                        'R8:R10',
                        'S8:S10',
                        'T8:T10',
                        'U8:U10',

                        'A2:F2',
                        'A3:F3',
                        'A5:F5',
                    ]);
                    // ->freezePane('A3');
                $event->sheet->getDelegate()->setCellValue('A2', 'CÔNG TY TRUYỀN THÔNG VIETTEL');
                $event->sheet->getDelegate()->setCellValue('A3', 'Quản lý kho nội dung');
                $event->sheet->getDelegate()->setCellValue('A5', 'DANH SÁCH BÀI HÁT');

                $event->sheet->getDelegate()->setCellValue('A8', 'STT');
                $event->sheet->getDelegate()->setCellValue('B8', 'Mã Hợp đồng bán');
                $event->sheet->getDelegate()->setCellValue('C8', 'MÃ NHẠC CHỜ');
                $event->sheet->getDelegate()->setCellValue('D8', 'TÊN BÀI HÁT KHÔNG DẤU');
                $event->sheet->getDelegate()->setCellValue('E8', 'CA SĨ THỂ HIỆN KHÔNG DẤU');
                $event->sheet->getDelegate()->setCellValue('F8', 'TÊN BÀI HÁT (có dấu)');
                $event->sheet->getDelegate()->setCellValue('G8', 'CA SĨ THỂ HIỆN');
                $event->sheet->getDelegate()->setCellValue('H8', 'NHẠC SĨ');
                $event->sheet->getDelegate()->setCellValue('I8', 'NHẠC CHỜ');
                $event->sheet->getDelegate()->setCellValue('K8', 'NHẠC SỐ');
                $event->sheet->getDelegate()->setCellValue('M8', 'QUYỀN LIÊN QUAN');
                $event->sheet->getDelegate()->setCellValue('N8', 'Số Phụ lục HĐ Quyền liên quan (Số HĐ của Viettel và CPs)');
                $event->sheet->getDelegate()->setCellValue('O8', 'Số thứ tự trong PL quyền liên quan');
                $event->sheet->getDelegate()->setCellValue('P8', 'QUYỀN TÁC GIẢ');
                $event->sheet->getDelegate()->setCellValue('Q8', 'Số Phụ lục HĐ Quyền tác giả (Số HĐ của Viettel và CPs)');
                $event->sheet->getDelegate()->setCellValue('R8', 'Số thứ tự trong PL Quyền tác giả');
                $event->sheet->getDelegate()->setCellValue('S8', 'Thời gian phát hành');
                $event->sheet->getDelegate()->setCellValue('T8', 'Thời hạn bản quyền');
                $event->sheet->getDelegate()->setCellValue('U8', 'ID Keeng');
                $event->sheet->getDelegate()->setCellValue('I9', 'Độc quyền');
                $event->sheet->getDelegate()->setCellValue('J9', 'Tác quyền');
                $event->sheet->getDelegate()->setCellValue('K9', 'Độc quyền');
                $event->sheet->getDelegate()->setCellValue('L9', 'Tác quyền');
            
                

                $default_font_style = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12]
                ];
                $default_font_style2 = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '00000000'],
                        ],
                    ],
                ];
                $default_font_style_title = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12, 'bold' =>  true],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'startColor' => [
                            'argb' => 'B7DEE8',
                        ],
                        'endColor' => [
                            'argb' => 'B7DEE8',
                        ],
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '00000000'],
                        ],
                    ],
                ];
                $default_font_style_title2 = [
                    'font' => ['name' => 'Times New Roman', 'size' => 14, 'bold' =>  true],
                ];
                $event->sheet->getStyle('A8:AZ8')->getAlignment()->setWrapText(true);
                $event->sheet->getStyle('A9:AZ9')->getAlignment()->setWrapText(true);
                $event->sheet->getStyle('A10:AZ10')->getAlignment()->setWrapText(true);

                $active_sheet = $event->sheet->getDelegate();
                $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
                $active_sheet->getParent()->getDefaultStyle()->getAlignment()->applyFromArray(
                    array('horizontal' => 'left')
                );
                $arrayAlphabet = [
                    'A', 'B', 'C', 'D', 'E', 'F', 'G','H',
                    'M', 'N', 'O', 'P', 'Q', 'R','S','T','U'
                ];
                foreach ($arrayAlphabet as $alphabet) {
                    $event->sheet->getColumnDimension($alphabet)->setWidth(30);
                }
                $arrayAlphabet = [
                    'L', 'I', 'J', 'K'
                ];
                foreach ($arrayAlphabet as $alphabet) {
                    $event->sheet->getColumnDimension($alphabet)->setWidth(15);
                }
                $event->sheet->getColumnDimension('A')->setWidth(5);
                $arrayAlphabet = [
                    'A', 'B', 'C', 'D', 'E', 'F',  'G', 'H',
                    'M', 'N', 'O', 'P', 'Q', 'R','S','T','U'
                ];    
                foreach ($arrayAlphabet as $alphabet) {
                    $newCell =  $alphabet.'8:'.$alphabet.'10';
                    $active_sheet->getStyle($newCell)->getAlignment()->applyFromArray(
                        array('horizontal' => 'center', 'vertical'=>'center')
                    ); 
                    $active_sheet->getStyle($newCell)->applyFromArray($default_font_style_title);
                }                           
                $active_sheet->getStyle('A2:F2')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('A2:F2')->applyFromArray($default_font_style_title2);
                $active_sheet->getStyle('A3:F3')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('A3:F3')->applyFromArray($default_font_style_title2);
                $active_sheet->getStyle('A5:F5')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('A5:F5')->applyFromArray($default_font_style_title2);


                $active_sheet->getStyle('I8:J8')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('I8:J8')->applyFromArray($default_font_style_title);
                $active_sheet->getStyle('I9:I10')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('I9:I10')->applyFromArray($default_font_style_title);
                $active_sheet->getStyle('J9:J10')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('J9:J10')->applyFromArray($default_font_style_title);

                $active_sheet->getStyle('K8:L8')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('K8:L8')->applyFromArray($default_font_style_title);
                $active_sheet->getStyle('K9:K10')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('K9:K10')->applyFromArray($default_font_style_title);
                $active_sheet->getStyle('L9:L10')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('L9:L10')->applyFromArray($default_font_style_title);

                if($this->countRow) $active_sheet->getStyle('A11:U'.($this->countRow+10))->applyFromArray($default_font_style2);
            },
        ];
    }
    public function startCell(): string
    {
        return 'A11';
    }
}
