<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Models\User;

class CopyrightSheetSecond implements FromView {

    public function view(): View
    {
        return view('admin.user', [
            'users'=> User::all()]);
    }
}
