<?php

namespace App\Exports;

use App\Models\Music;
use App\Models\Contract;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Excel;
use Carbon\Carbon;
class MusicExport implements FromCollection,WithHeadings,WithMapping,WithEvents, WithCustomStartCell
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use RegistersEventListeners;
    public $stt = 0;

    protected $searchName;
    protected $searchSinger;
    protected $searchMusician;
    protected $searchStatus;
    protected $count_time;
    protected $countRow = 0;
    function __construct($searchName, $searchSinger, $searchMusician, $searchStatus, $count_time) {
        $this->searchName = trim($searchName);
        $this->searchSinger = trim($searchSinger);
        $this->searchMusician = trim($searchMusician);
        $this->searchStatus = $searchStatus;
        $this->count_time = $count_time;
    }

    public function collection()
    {
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        $query = Music::where(function ($query) {
                $query->where('music.end_time', '>=', date('Y-m-d'));
                $query->orWhere('music.end_time', null);
        });
        if(auth()->user()->is_manager == 0 && auth()->user()->status_media == 1){
            $query->where('music.admin_id', auth()->id());
        }
        $count_time = $this->count_time;
         if($count_time==7)
            $query->whereBetween('music.end_time',[Carbon::now()->format('Y-m-d'),Carbon::now()->addDay($count_time)->format('Y-m-d')]);
        if($count_time<7&&$count_time>0)
            $query->whereBetween('music.end_time',[Carbon::now()->addMonth($count_time-1)->addDays(1)->format('Y-m-d'),Carbon::now()->addMonth($count_time)->format('Y-m-d')]);
        if($this->searchStatus==1)
            $query = Music::where('music.end_time', '<', date('Y-m-d'));
        elseif($this->searchStatus==2)
            $query = Music::onlyTrashed();
        elseif($this->searchStatus==3)
            $query = Music::withTrashed();
        $music = $query;    
        if($this->searchName)
            $music->where('music.name','LIKE','%'.$this->searchName.'%');
        if($this->searchSinger)
            $music->where('music.singer','LIKE','%'.$this->searchSinger.'%');
        if($this->searchMusician)
            $music->where('music.musician','LIKE','%'.$this->searchMusician.'%');
        $music= $music->leftJoin('contracts','contracts.id','music.contract_id')
            ->leftJoin('type_fee','type_fee.id','music.type_fee_id')
            ->select('music.*','contracts.contract_number','type_fee.name as type_fee_name')->get();
        $this->countRow = count($music);
        return $music;
    }

    public function headings(): array {
        return [
            'STT',
            'Tên bài hát',
            'Ca sĩ thể hiện',
            'Nhạc sĩ',
            'Album',
            'Nhạc chờ độc quyền',
            'Nhạc chờ tác quyền',
            'Nhạc số độc quyền',
            'Nhạc số tác quyền',
            'Bán đối tác thứ 3',
            'Loại hợp đồng',
            'QUYỀN LIÊN QUAN',
            'Quyền tác giả',
            'Số Phụ lục HĐ (Số HĐ của Viettel và CPs)',
            'Giá tiền (Đối với HĐ Mua bản quyền và sản xuất)',
            'Số HĐ Quyền liên quan (biểu diễn, ghi âm)',
            'Số Phụ lục HĐ Quyền liên quan (biểu diễn, ghi âm)',
            'Số thứ tự trong PL Quyền liên quan',
            'Số HĐ Quyền tác giả',
            'Số Phụ lục HĐ Quyền tác giả',
            'Số thứ tự trong PL Quyền Tác giả',                   
            'Thời gian phát hành',
            'Thời hạn bản quyền',
            'Audio',
            'MV',
            'Ghi chú',
            'ID Keeng',
        ];
    }
    public function map($music): array {
        return [
            ++$this->stt,
            $music->name,
            $music->singer,
            $music->musician,
            $music->album,
            $music->cho_docquyen?'x':'',
            $music->cho_tacquyen?'x':'',
            $music->so_docquyen?'x':'',
            $music->so_tacquyen?'x':'',
            $music->sale_permission?'x':'',
            $music->contract_number,
            $music->tac_gia,
            $music->contract_cps,
            $music->contract_number1,
            $music->contract_number2,
            $music->contract_number3,
            $music->contract_number4,
            $music->contract_number5,
            $music->contract_number6,
            $music->start_time? reFormatDate($music->start_time,'d/m/Y'):'',
            $music->end_time? reFormatDate($music->end_time,'d/m/Y'):'',
            $music->type_audio?'x':'',
            $music->type_MV?'x':'',
            $music->note,
            $music->type_fee_name,
            $music->money,
            $music->music_id,
        ];
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event
                    ->sheet
                    ->getDelegate()
                    ->setMergeCells([
                        'A8:A10',
                        'B8:B10',
                        'C8:C10',
                        'D8:D10',
                        'E8:E10',
                        'F8:G8',
                        'H8:I8',
                        'F9:F10',
                        'G9:G10',
                        'H9:H10',
                        'I9:I10',
                        'J8:J10',
                        'K8:K10',
                        'L8:L10',
                        'M8:M10',
                        'N8:N10',
                        'O8:O10',
                        'P8:P10',
                        'Q8:Q10',
                        'R8:R10',
                        'S8:S10',
                        'T8:T10',
                        'U8:U10',
                        'X8:X10',
                        'Y8:Y10',
                        'Z8:Z10',
                        'AA8:AA10',
                        'V8:W8',
                        'V9:V10',
                        'W9:W10',
                        'A2:F2',
                        'A3:F3',
                        'A5:F5',
                    ]);
                    // ->freezePane('A3');
                $event->sheet->getDelegate()->setCellValue('A2', 'CÔNG TY TRUYỀN THÔNG VIETTEL');
                $event->sheet->getDelegate()->setCellValue('A3', 'Quản lý kho nội dung');
                $event->sheet->getDelegate()->setCellValue('A5', 'DANH SÁCH BÀI HÁT');

                $event->sheet->getDelegate()->setCellValue('A8', 'STT');
                $event->sheet->getDelegate()->setCellValue('B8', 'TÊN BÀI HÁT (có dấu)');
                $event->sheet->getDelegate()->setCellValue('C8', 'CA SĨ THỂ HIỆN');
                $event->sheet->getDelegate()->setCellValue('D8', 'NHẠC SĨ');
                $event->sheet->getDelegate()->setCellValue('E8', 'ALBUM');
                $event->sheet->getDelegate()->setCellValue('F8', 'NHẠC CHỜ');
                $event->sheet->getDelegate()->setCellValue('H8', 'NHẠC SỐ');
                $event->sheet->getDelegate()->setCellValue('J8', 'Bán đối tác thứ 3');
                $event->sheet->getDelegate()->setCellValue('K8', 'QUYỀN LIÊN QUAN');
                $event->sheet->getDelegate()->setCellValue('L8', 'QUYỀN TÁC GIẢ');
                $event->sheet->getDelegate()->setCellValue('M8', 'Số Phụ lục HĐ (Số HĐ của Viettel và CPs)');
                $event->sheet->getDelegate()->setCellValue('N8', 'Số HĐ Quyền liên quan (biểu diễn, ghi âm)');
                $event->sheet->getDelegate()->setCellValue('O8', 'Số Phụ lục HĐ Quyền liên quan (biểu diễn, ghi âm)');
                $event->sheet->getDelegate()->setCellValue('P8', 'Số thứ tự trong PL Quyền liên quan');
                $event->sheet->getDelegate()->setCellValue('Q8', 'Số HĐ Quyền tác giả');
                $event->sheet->getDelegate()->setCellValue('R8', 'Số Phụ lục HĐ Quyền tác giả');
                $event->sheet->getDelegate()->setCellValue('S8', 'Số thứ tự trong PL Quyền Tác giả');
                $event->sheet->getDelegate()->setCellValue('T8', 'Thời gian phát hành');
                $event->sheet->getDelegate()->setCellValue('U8', 'Thời hạn bản quyền');
                $event->sheet->getDelegate()->setCellValue('V8', 'Loại');
                $event->sheet->getDelegate()->setCellValue('X8', 'Ghi chú');

                $event->sheet->getDelegate()->setCellValue('F9', 'Độc quyền');
                $event->sheet->getDelegate()->setCellValue('G9', 'Tác quyền');
                $event->sheet->getDelegate()->setCellValue('H9', 'Độc quyền');
                $event->sheet->getDelegate()->setCellValue('I9', 'Tác quyền');
                $event->sheet->getDelegate()->setCellValue('V9', 'Audio');
                $event->sheet->getDelegate()->setCellValue('W9', 'MV');
                $event->sheet->getDelegate()->setCellValue('Y8', 'Loại HĐ');
                $event->sheet->getDelegate()->setCellValue('Z8', 'Giá tiền (Đối với HĐ Mua bản quyền và sản xuất)');
                $event->sheet->getDelegate()->setCellValue('AA8', 'ID Keeng');

                $default_font_style = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12]
                ];
                $default_font_style2 = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '00000000'],
                        ],
                    ],
                ];
                $default_font_style_title = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12, 'bold' =>  true],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'startColor' => [
                            'argb' => 'B7DEE8',
                        ],
                        'endColor' => [
                            'argb' => 'B7DEE8',
                        ],
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '00000000'],
                        ],
                    ],
                    'wrapText' => true,
                ];
                $default_font_style_title2 = [
                    'font' => ['name' => 'Times New Roman', 'size' => 14, 'bold' =>  true],
                ];
                $event->sheet->getStyle('A8:AZ8')->getAlignment()->setWrapText(true);
                $event->sheet->getStyle('A9:AZ9')->getAlignment()->setWrapText(true);
                $event->sheet->getStyle('A10:AZ10')->getAlignment()->setWrapText(true);
                $active_sheet = $event->sheet->getDelegate();
                $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
                $active_sheet->getParent()->getDefaultStyle()->getAlignment()->applyFromArray(
                    array('horizontal' => 'left')
                );
                $arrayAlphabet = [
                    'A', 'B', 'C', 'D', 'E',
                    'K', 'L', 'Q', 'R', 'X','Y','S','Z',
                ];
                foreach ($arrayAlphabet as $alphabet) {
                    $event->sheet->getColumnDimension($alphabet)->setWidth(30);
                }
                $arrayAlphabet = [
                    'T','U', 'M', 'N', 'O', 'P','J',
                ];
                foreach ($arrayAlphabet as $alphabet) {
                    $event->sheet->getColumnDimension($alphabet)->setWidth(20);
                }
                $arrayAlphabet = [
                    'F', 'G', 'H', 'I','V', 'W', 'AA',
                ];
                foreach ($arrayAlphabet as $alphabet) {
                    $event->sheet->getColumnDimension($alphabet)->setWidth(15);
                }
                $event->sheet->getColumnDimension('A')->setWidth(5);
                $arrayAlphabet = [
                    'A', 'B', 'C', 'D', 'E', 'J', 'K', 'L', 'M', 'N', 
                    'O', 'P', 'Q', 'R', 'S', 'T', 'U','X','Y','Z','AA'
                ];    
                foreach ($arrayAlphabet as $alphabet) {
                    $newCell =  $alphabet.'8:'.$alphabet.'10';
                    $active_sheet->getStyle($newCell)->getAlignment()->applyFromArray(
                        array('horizontal' => 'center', 'vertical'=>'center')
                    ); 
                    $active_sheet->getStyle($newCell)->applyFromArray($default_font_style_title);
                }                           
                $active_sheet->getStyle('A2:F2')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('A2:F2')->applyFromArray($default_font_style_title2);
                $active_sheet->getStyle('A3:F3')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('A3:F3')->applyFromArray($default_font_style_title2);
                $active_sheet->getStyle('A5:F5')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('A5:F5')->applyFromArray($default_font_style_title2);


                $active_sheet->getStyle('F8:I8')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('F8:I8')->applyFromArray($default_font_style_title);
                $active_sheet->getStyle('F9:I10')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('F9:I10')->applyFromArray($default_font_style_title);
                $active_sheet->getStyle('V8:W8')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('V8:W8')->applyFromArray($default_font_style_title);
                $active_sheet->getStyle('V9:W10')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('V9:W10')->applyFromArray($default_font_style_title);

                if($this->countRow) $active_sheet->getStyle('A11:AA'.($this->countRow+10))->applyFromArray($default_font_style2);
            },
        ];
    }
    public function startCell(): string
    {
        return 'A10';
    }
}
