<?php

namespace App\Exports;

use App\Models\Music;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
class FilmSaleExport implements FromArray, WithHeadings, WithEvents,WithCustomStartCell
{
    protected $data;
    function __construct($data) {
        $this->data = $data;
    }
    public function array():array{  
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        $this->data = json_decode($this->data, true);
        $data1 = [];
        array_push($data1, $this->data);
        return $data1;
    }
    public function headings():array{
        return [
        ];
    }
     public function map($data): array {
        return [
            $data,
        ];
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                // $event
                //     ->sheet
                //     ->getDelegate()
                //     ->setMergeCells([
                //         'A1:A5',
                //     ]);
                    // ->freezePane('A3');
                $event->sheet->getDelegate()->setCellValue('A1', 'Danh sách lỗi chi tiết');
                $event->sheet->getDelegate()->setCellValue('B1', 'Các bản ghi bị lỗi');
                $event->sheet->getDelegate()->setCellValue('A2', 'Thiếu thời gian bắt đầu bản quyền hoặc định dạng không hợp lệ');
                $event->sheet->getDelegate()->setCellValue('A3', 'Thiếu thời hạn bản quyền hoặc định dạng không hợp lệ');
                $event->sheet->getDelegate()->setCellValue('A4', 'Thời gian bắt đầu bản quyền lớn hơn hoặc bằng thời hạn bản quyền');
                $event->sheet->getDelegate()->setCellValue('A5', 'Thời hạn bản quyền của phim không phù hợp');
                $event->sheet->getDelegate()->setCellValue('A6', 'Thiếu số hợp đồng');
                $event->sheet->getDelegate()->setCellValue('A7', 'Số hợp đồng đã tồn tại và không phải hợp đồng bán phim');
                $event->sheet->getDelegate()->setCellValue('A8', 'Thiếu loại doanh thu');
                $event->sheet->getDelegate()->setCellValue('A9', 'Sai tên loại doanh thu');
                $event->sheet->getDelegate()->setCellValue('A10', 'Quốc gia được phép phát sóng chưa có trong hệ thống');
                $default_font_style = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12]
                ];
                $default_font_style_title = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12, 'bold' =>  true],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'startColor' => [
                            'argb' => 'B7DEE8',
                        ],
                        'endColor' => [
                            'argb' => 'B7DEE8',
                        ],
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '00000000'],
                        ],
                    ],
                ];

                $active_sheet = $event->sheet->getDelegate();
                $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
                $active_sheet->getParent()->getDefaultStyle()->getAlignment()->applyFromArray(
                    array('horizontal' => 'left')
                );
                $arrayAlphabet = [
                    'C', 'D', 'E'
                ];  
                foreach ($arrayAlphabet as $alphabet) {
                    $event->sheet->getColumnDimension($alphabet)->setWidth(30);
                }
                $event->sheet->getColumnDimension('A')->setWidth(100); 
                $event->sheet->getColumnDimension('B')->setWidth(50); 
                $active_sheet->getStyle('A1:B1')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('A1:B1')->applyFromArray($default_font_style_title);


                $style = array(
                    'alignment' => array(
                        'horizontal' => 'left',
                    ),
                    'font' => ['name' => 'Times New Roman', 'size' => 12, 'bold' =>  true],
                );
                $event->sheet->getStyle("A2:A10")->applyFromArray($style);
                $event->sheet->getStyle("C1:C1")->applyFromArray($style);
            },
        ];
    }
    public function startCell(): string
    {
        return 'B2';
    }
}
