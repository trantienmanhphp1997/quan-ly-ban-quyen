<?php

namespace App\Exports;

use App\Models\Film;
use App\Models\Country;
use App\Models\Contract;
use App\Models\FilmListCategory;
use App\Models\RevenueType;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Carbon\Carbon;

class FilmExport implements FromCollection, WithHeadings, WithMapping,WithEvents, WithCustomStartCell
{
	use RegistersEventListeners;
	public $stt = 0;

	protected $searchTerm;
	protected $searchCategory;
	protected $searchPartner;
	protected $searchCountry;
    protected $searchStatus;
    protected $searchYear;
    protected $count_time;
    protected $currentPosition;
    protected $mergeCells;
    protected $countRow = 0;

	function __construct($searchTerm='', $searchCategory='', $searchPartner='', $searchCountry='', $searchStatus='', $count_time='', $searchYear='') {
        $this->searchTerm = trim($searchTerm);
        $this->searchCategory = trim($searchCategory);
        $this->searchPartner = trim($searchPartner);
        $this->searchCountry = trim($searchCountry);
        $this->searchStatus = trim($searchStatus);
        $this->searchYear = trim($searchYear);
        $this->count_time = $count_time;
    }

    public function collection()
    {
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        $query = Film::where(function ($query) {
                $query->where('films.end_time', '>=', date('Y-m-d'));
                $query->orWhere('films.end_time', null);
        });
        if(auth()->user()->is_manager == 0 && auth()->user()->status_media == 1){
            $query->where('films.admin_id', auth()->id());
        }
        $count_time=$this->count_time;
        if($count_time==7)
            $query->whereBetween('films.end_time',[Carbon::now()->format('Y-m-d'),Carbon::now()->addDay($count_time)->format('Y-m-d')]);
        if($count_time<7&&$count_time>0)
            $query->whereBetween('films.end_time',[Carbon::now()->addMonth($count_time-1)->addDays(1)->format('Y-m-d'),Carbon::now()->addMonth($count_time)->format('Y-m-d')]);

        if($this->searchStatus==1)
            $query = Film::where('films.end_time', '<', date('Y-m-d'));
        elseif($this->searchStatus==2)
            $query = Film::onlyTrashed();
        elseif($this->searchStatus==3)
            $query = Film::withTrashed();
        $films = $query;
        if($this->searchTerm)
            $films->where('films.vn_name','like','%'.$this->searchTerm.'%');
        if($this->searchCategory){	
            $films->join('film_list_categories', 'films.id', '=', 'film_list_categories.id_film_list');
            $films->where('film_list_categories.id_category_film','=',$this->searchCategory);
        }
        if($this->searchCountry){
            $films->where('films.country','=',$this->searchCountry);
        }
        $films->leftJoin('partners', 'partners.id', '=', 'films.partner_name');
        if($this->searchPartner){
            $films->where('partners.name','like','%'.$this->searchPartner.'%');
        }
        if($this->searchYear){
            $query->where('films.year_create','like','%'.trim($this->searchYear).'%');
        }
        $films = $films->orderBy('films.deleted_at')->orderBy('films.id','desc')->select('films.*', 'partners.name as partnerName')->get();
        foreach ($films as $film) {
            $doc_quyen='';
            $khong_doc_quyen='';
            $roles =  $film->contractRole;
            foreach ($roles as $role) {
            	if($role->roleMedia){
	                if($role->roleMedia->type == 1)
	                    $doc_quyen=$doc_quyen." ".$role->roleMedia->code.",";
	                else if($role->roleMedia->type == 2)
	                    $khong_doc_quyen=$khong_doc_quyen." ".$role->roleMedia->code.",";
	            	}
            }
            // lấy quốc gia
            $country = Country::find($film->country);
            if($country){
            	$country = $country->name;
            }
            else {
            	$country = '';
            }
            $film->country = $country;
            // lấy số mã hợp đồng
            $contractNumber = Contract::find($film->contract_id);
            if($contractNumber){
            	$contractNumber = $contractNumber->contract_number;
            }
            else {
            	$contractNumber = '';
            }
            $film->contract_id = $contractNumber;
            // lấy thể loại phim
            $categories = FilmListCategory::
            	Join('films','film_list_categories.id_film_list','=','films.id')
            	->Join('category_films', 'film_list_categories.id_category_film','=','category_films.id')->where('films.id',$film->id)->pluck('category_films.name');
            $categories = json_decode(json_encode($categories), true);
            if($categories){
            	$categories = implode(", ",$categories);
            }
            else {
            	$categories = '';
            }
			$film->category_name = $categories;

            $broadcast_country = DB::table('film_country')
                ->Join('films','film_country.media_id','=','films.id')
                ->Join('countries', 'film_country.country_id','=','countries.id')
                ->where('films.id',$film->id)
                ->pluck('countries.name');
            $broadcast_country = json_decode(json_encode($broadcast_country), true);
            if($broadcast_country){
                $broadcast_country = implode(", ",$broadcast_country);
            }
            else {
                $broadcast_country = '';
            }
            $film->broadcast_country = $broadcast_country;  
            // Hạ tầng phát sóng
            $haTang = DB::table('contract_media_role')
                ->Join('films','contract_media_role.media_id','=','films.id')
                ->Join('role_media', 'contract_media_role.role_id','=','role_media.id')
                ->where('films.id',$film->id)
                ->where('level',1)
                ->pluck('role_media.code');
            $haTang = json_decode(json_encode($haTang), true);
            if($haTang){
                $haTang = implode(", ",$haTang);
            }
            else {
                $haTang = '';
            }
            $film->haTang = $haTang;                        
			// convert datetime
			$film->start_time = $film->start_time?date('m/d/Y', strtotime($film->start_time)):'';
			$film->end_time = $film->start_time?date('m/d/Y', strtotime($film->end_time)):'';


            $film->doc_quyen=$doc_quyen;
            $film->khong_doc_quyen=$khong_doc_quyen;
            // Lấy doanh thu tối thiểu
        }
        $this->countRow = count($films);
        return $films;
    }
	public function headings():array{
		return [
			'STT',
			'Quốc gia',
			'Tên nội dung (Tên phim)',
			'Năm SX',
			'Thể loại',
			'Số đầu phim (tên phim)',
			'Số tập phim',
			'Thời lượng/tập',
			'Đối tác cung cấp',
            'Độc quyền',
            'Không độc quyền',
			'Thời hạn bản quyền',
			'Thời gian bắt đầu BQ',
			'Số/Mã Hợp đồng',
			'Chi phí bản quyền (VNĐ) (đơn giá/tập)',
			'chi phí hậu kỳ (VNĐ) (đơn giá/tập)',
			'chi phí kiểm duyệt (VNĐ) (đơn giá/tập)',
            'Tổng chi phí/tập(VNĐ)',
			'Tổng chi phí (VNĐ)',
			'Mô tả phim (nội dung phim)',
			'Diễn viên',
			'Đạo diễn',
		];
	}


    public function map($film): array {
        $map = [
            ++$this->stt,
            $film->country,
            $film->product_name?$film->vn_name.' - '.$film->product_name:$film->vn_name,
            $film->year_create,
            $film->category_name,
            $film->count_name,
            $film->count_tap,
            $film->count_hour,
            $film->partnerName,
            $film->doc_quyen,
            $film->khong_doc_quyen,
            $film->end_time,
            $film->start_time,
            $film->contract_id,
            $film->license_fee?numberFormat($film->license_fee):'0',
            $film->partner_fee?numberFormat($film->partner_fee):'0',
            $film->other_fee?numberFormat($film->other_fee):'0',
            countChiPhiTap($film)?numberFormat(countChiPhiTap($film)):'0',
            countChiPhiTap($film)*$film->count_tap?numberFormat(countChiPhiTap($film)*$film->count_tap):'0',
            $film->note,
            $film->actor_name,
            $film->director,
            $film->broadcast_country,
            $film->nuoc,
            $film->count_live,
            $film->haTang,
        ];
        $sum=0;
        $parentTypes = RevenueType::parent();
        foreach ($parentTypes as $parentType) {
            $childrenTypes = RevenueType::where('parent_id', '=', $parentType->id)->get();
            $sumRevenueByParent = 0;
            foreach ($childrenTypes as $childrenType) {
                $map[] = numberFormat($film->revenueByType($childrenType->id));
                $sumRevenueByParent += $film->revenueByType($childrenType->id);
            }
            $map[] = $sumRevenueByParent?numberFormat($sumRevenueByParent):'0';
            $sum += $sumRevenueByParent;
        }
        $map[] = $sum?numberFormat($sum):'0';
        $map[] = $sum?numberFormat($sum*$film->count_tap):'0';
        $map[] = $film->addition_note;

        return $map;
    }
    public function registerEvents(): array
    {

        $this->mergeCells = [
                        'A8:A10',
                        'B8:B10',
                        'C8:C10',
                        'D8:D10',
                        'E8:E10',
                        'F8:F10',
                        'G8:G10',
                        'H8:H10',
                        'I8:I10',
                        'J8:J10',
                        'K8:K10',
                        'L8:L10',
                        'M8:M10',
                        'N8:N10',
                        'T8:T10',
                        'U8:U10',
                        'V8:V10',
                        'W8:W10',
                        'X8:X10',
                        'Y8:Y10',
                        'Z8:Z10',
                        'A2:F2',
                        'A3:F3',
                        'A5:F5',
                        'O8:S8',
                        'O9:O10',
                        'P9:P10',
                        'Q9:Q10',
                        'R9:R10',
                        'S9:S10',
                    ];

        $this->currentPosition='AA';
        $countParentType = RevenueType::parent()->count();
        $this->countRevenueParentType = $countParentType;
        $countChildrenType = RevenueType::where('parent_id','!=', 0)->count();
        $countRevenueType = $countParentType + $countChildrenType + 1;
        $this->mergeCells[] ='AA8:'.charCalculation('AA', $countRevenueType).'8';
        foreach(RevenueType::parent() as $parentType){
            $count = RevenueType::where('parent_id','=', $parentType->id)->count();
            $this->mergeCells[] = $this->currentPosition.'9:'.charCalculation($this->currentPosition, $count).'9';
            $this->currentPosition = charCalculation($this->currentPosition, $count+1);
        }
        $this->mergeCells[] = $this->currentPosition.'9:'.$this->currentPosition.'10';
        $this->currentPosition = charCalculation($this->currentPosition, 1);
        $this->mergeCells[] = $this->currentPosition.'9:'.$this->currentPosition.'10';
        $this->currentPosition = charCalculation($this->currentPosition, 1);
        $this->mergeCells[] = $this->currentPosition.'8:'.$this->currentPosition.'10';



        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event
                    ->sheet
                    ->getDelegate()
                    ->setMergeCells($this->mergeCells);

                    // ->freezePane('A3');
                $event->sheet->getDelegate()->setCellValue('A2', 'CÔNG TY TRUYỀN THÔNG VIETTEL');
                $event->sheet->getDelegate()->setCellValue('A3', 'Quản lý kho nội dung');
                $event->sheet->getDelegate()->setCellValue('A5', 'DANH SÁCH PHIM BỘ');

                $event->sheet->getDelegate()->setCellValue('A8', 'STT');
                $event->sheet->getDelegate()->setCellValue('B8', 'Quốc gia');
                $event->sheet->getDelegate()->setCellValue('C8', 'Tên phim tiếng Việt - tiếng Anh');
                $event->sheet->getDelegate()->setCellValue('D8', 'Năm SX');
                $event->sheet->getDelegate()->setCellValue('E8', 'Thể loại');
                $event->sheet->getDelegate()->setCellValue('F8', 'Số đầu phim');
                $event->sheet->getDelegate()->setCellValue('G8', 'Số tập phim');
                $event->sheet->getDelegate()->setCellValue('H8', 'Thời lượng/tập');
                $event->sheet->getDelegate()->setCellValue('I8', 'Đối tác cung cấp');
                $event->sheet->getDelegate()->setCellValue('J8', 'Độc quyền');
                $event->sheet->getDelegate()->setCellValue('K8', 'Không độc quyền');
                $event->sheet->getDelegate()->setCellValue('L8', 'Thời hạn bản quyền');
                $event->sheet->getDelegate()->setCellValue('M8', 'Thời gian bắt đầu bản quyền');
                $event->sheet->getDelegate()->setCellValue('N8', 'Số/Mã Hợp đồng');
                $event->sheet->getDelegate()->setCellValue('O8', 'Chi phí');
                $event->sheet->getDelegate()->setCellValue('T8', 'Mô tả phim (nội dung phim)');
                $event->sheet->getDelegate()->setCellValue('U8', 'Diễn viên');
                $event->sheet->getDelegate()->setCellValue('V8', 'Đạo diễn');
                $event->sheet->getDelegate()->setCellValue('W8', 'Quốc gia được phép phát sóng');
                $event->sheet->getDelegate()->setCellValue('X8', 'Nước phát sóng');
                $event->sheet->getDelegate()->setCellValue('Y8', 'Số lần phát sóng');
                $event->sheet->getDelegate()->setCellValue('Z8', 'Hạ tầng phát sóng');
               // $event->sheet->getDelegate()->setCellValue('AF8', 'Ghi chú');

                $event->sheet->getDelegate()->setCellValue('AA8', 'Doanh thu phân bổ theo tờ trình');

                $currentPosition = 'AA';

                foreach(RevenueType::parent() as $parentType) {
                    $event->sheet->getDelegate()->setCellValue($currentPosition.'9', 'DT tối thiểu theo tờ trình (USD/tập) '.$parentType->name);
                    $count = RevenueType::where('parent_id','=', $parentType->id)->count();
                    $currentPosition = charCalculation($currentPosition, $count+1);
                }
                $event->sheet->getDelegate()->setCellValue($currentPosition.'9', 'DTTT theo tờ trình (đơn giá/tập)');
                $currentPosition = charCalculation($currentPosition, 1);
                $event->sheet->getDelegate()->setCellValue($currentPosition.'9', 'Tổng DTTT theo tờ trình');
                $currentPosition = charCalculation($currentPosition, 1);
                $event->sheet->getDelegate()->setCellValue($currentPosition.'8', 'Ghi chú');

                $currentPosition = 'AA';
                foreach(RevenueType::parent() as $parentType) {
                    foreach(RevenueType::where('parent_id', '=', $parentType->id)->get() as $childrenType) {
                        $event->sheet->getDelegate()->setCellValue($currentPosition++.'10', $childrenType->name);
                    }
                    $event->sheet->getDelegate()->setCellValue($currentPosition++.'10', 'Tổng '.$parentType->name);
                }
                $event->sheet->getDelegate()->setCellValue($currentPosition.'8', 'Ghi chú');

                $event->sheet->getDelegate()->setCellValue('O9', 'Chi phí bản quyền (đơn giá/tập)');
                $event->sheet->getDelegate()->setCellValue('P9', 'Chi phí hậu kỳ (đơn giá/tập)');
                $event->sheet->getDelegate()->setCellValue('Q9', 'Chi phí kiểm duyệt (đơn giá/tập)');
                $event->sheet->getDelegate()->setCellValue('R9', 'Chi phí/tập');
                $event->sheet->getDelegate()->setCellValue('S9', 'Tổng chi phí');


                $default_font_style = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12],
                ];
                $default_font_style2 = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '00000000'],
                        ],
                    ],
                ];
                $default_font_style_title = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12, 'bold' =>  true],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'startColor' => [
                            'argb' => 'B7DEE8',
                        ],
                        'endColor' => [
                            'argb' => 'B7DEE8',
                        ],
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '00000000'],
                        ],
                    ],
                    'wrapText' => true,
                ];
                $default_font_style_title2 = [
                    'font' => ['name' => 'Times New Roman', 'size' => 14, 'bold' =>  true],
                ];
                $event->sheet->getStyle('A8:AZ8')->getAlignment()->setWrapText(true);
                $event->sheet->getStyle('A9:AZ9')->getAlignment()->setWrapText(true);
                $event->sheet->getStyle('A10:AZ10')->getAlignment()->setWrapText(true);

                $active_sheet = $event->sheet->getDelegate();
                $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
                $active_sheet->getParent()->getDefaultStyle()->getAlignment()->applyFromArray(
                    array('horizontal' => 'left')
                );
                $arrayAlphabet = [
                    'A', 'B', 'C', 'E', 'I', 'J', 'K', 'L', 'M','N', 'O', 'P', 'Q', 'T', 'U', 'V', 'W', 'Z',
                ];
                foreach ($arrayAlphabet as $alphabet) {
                    $event->sheet->getColumnDimension($alphabet)->setWidth(30);
                }
                $arrayAlphabet = [
                    'M','N',
                ];
                foreach ($arrayAlphabet as $alphabet) {
                    $event->sheet->getColumnDimension($alphabet)->setWidth(20);
                }
                $currentPosition = 'AA';
                for($i=1; $i<=RevenueType::where('parent_id', '!=', 0)->count()+RevenueType::parent()->count()+3; $i++) {
                    $arrayAlphabet[] = $currentPosition++;
                }

                foreach ($arrayAlphabet as $alphabet) {
                    $event->sheet->getColumnDimension($alphabet)->setWidth(30);
                }
                $arrayAlphabet = [
                    'D', 'G', 'H', 'R', 'F', 'S', 'X', 'Y', 'O', 'P', 'Q',
                ];
                foreach ($arrayAlphabet as $alphabet) {
                    $event->sheet->getColumnDimension($alphabet)->setWidth(20);
                }
                $event->sheet->getColumnDimension('A')->setWidth(5);
                $arrayAlphabet = [
                    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M','N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
                ];
                $currentPosition = 'AA';
                for($i=1; $i<=RevenueType::where('parent_id', '!=', 0)->count()+RevenueType::parent()->count()+3; $i++) {
                    $arrayAlphabet[] = $currentPosition++;
                }
                $check =false;
                foreach ($arrayAlphabet as $alphabet) {
                    if($alphabet=='AA') $check = true;
                    if($check) $event->sheet->getColumnDimension($alphabet)->setWidth(25);
                    $newCell =  $alphabet.'8:'.$alphabet.'10';
                    $active_sheet->getStyle($newCell)->getAlignment()->applyFromArray(
                        array('horizontal' => 'center', 'vertical'=>'center')
                    ); 
                    $active_sheet->getStyle($newCell)->applyFromArray($default_font_style_title);
                }                           
                $active_sheet->getStyle('A2:F2')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('A2:F2')->applyFromArray($default_font_style_title2);
                $active_sheet->getStyle('A3:F3')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('A3:F3')->applyFromArray($default_font_style_title2);
                $active_sheet->getStyle('A5:F5')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('A5:F5')->applyFromArray($default_font_style_title2);


                $active_sheet->getStyle('P8:S8')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('P9:S10')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 

                $active_sheet->getStyle('AA8:AF8')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 

                $active_sheet->getStyle('AA9:AD9')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 

                $active_sheet->getStyle('AE9:AF9')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 

                $active_sheet->getStyle('AA10:AF10')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 

                // $event->sheet->getDelegate()->getStyle('A1:W100')->getAlignment()->setWrapText(true);

                // dd($count,$currentPosition);

                $currentPosition = charCalculation($currentPosition, -1);
                if($this->countRow) $active_sheet->getStyle('A11:'.$currentPosition.($this->countRow+10))->applyFromArray($default_font_style2);

            },
        ];
    }
    public function startCell(): string
    {
        return 'A10';
    }
}
