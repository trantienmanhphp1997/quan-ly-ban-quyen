<?php

namespace App\Exports;

use App\Models\FilmBackup;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
class FilmBackupExport implements FromCollection,WithHeadings,WithMapping,WithEvents,WithCustomStartCell
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use RegistersEventListeners;

    protected $searchDriver;
    protected $searchTerm;
    public $stt = 0;
    protected $countRow = 0;
    public function __construct($searchDriver, $searchTerm){
        $this->searchDriver = $searchDriver;
        $this->searchTerm = $searchTerm;
    }

    public function collection()
    {
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        $query = FilmBackup::query();
        $query->where('driver','like','%'.trim($this->searchDriver).'%');
        $query->where('vn_name','like','%'.trim($this->searchTerm).'%');
        $query = $query->get();
        $this->countRow = count($query);
        return $query;
    }

    public function headings(): array {
        return [
            'STT',
            'Mã ổ',
            'Ổ backup',
            'Tên file',
            'Tên phim tiếng Anh',
            'Tên phim tiếng Việt',
            'Quốc gia',
            'Đối tác',
            'Bộ hay lẻ',
            'Số tập',
            'Loại file',
            'Thời lượng(phút)',
            'Dung lượng(Gb)',
            'Đuôi file hình',
            'Thời hạn bản quyền',
            'Người giao file',
        ];
    }
    public function map($FilmBackup): array {
        return [
            ++$this->stt,
            $FilmBackup->driver,
            $FilmBackup->backup_driver,
            $FilmBackup->file_name,
            $FilmBackup->film_name,
            $FilmBackup->vn_name,
            $FilmBackup->country,
            $FilmBackup->partner,
            ($FilmBackup->series_film==1)?'Bộ':(($FilmBackup->series_film==2)?'Lẻ':''),
            $FilmBackup->count_film,
            $FilmBackup->file_type,
            $FilmBackup->time_number,
            $FilmBackup->volumn,
            $FilmBackup->file_format,
            $FilmBackup->end_time? reFormatDate($FilmBackup->end_time,'m/d/Y'):'',
            $FilmBackup->file_delivery_person,
        ];
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event
                    ->sheet
                    ->getDelegate()
                    ->setMergeCells([
                        'A2:F2',
                        'A3:F3',
                        'A5:F5',
                    ]);
                $event->sheet->getDelegate()->setCellValue('A2', 'CÔNG TY TRUYỀN THÔNG VIETTEL');
                $event->sheet->getDelegate()->setCellValue('A3', 'Quản lý kho nội dung');
                $event->sheet->getDelegate()->setCellValue('A5', 'DANH SÁCH TƯ LIỆU');
                $default_font_style = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12]
                ];

                $default_font_style2 = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '00000000'],
                        ],
                    ],
                ];
                $default_font_style_title = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12, 'bold' =>  true],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'startColor' => [
                            'argb' => 'B7DEE8',
                        ],
                        'endColor' => [
                            'argb' => 'B7DEE8',
                        ],
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '00000000'],
                        ],
                    ],
                ];
                $default_font_style_title2 = [
                    'font' => ['name' => 'Times New Roman', 'size' => 14, 'bold' =>  true],
                ];
                $event->sheet->getStyle('A8:AZ8')->getAlignment()->setWrapText(true);

                $active_sheet = $event->sheet->getDelegate();
                $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
                $active_sheet->getParent()->getDefaultStyle()->getAlignment()->applyFromArray(
                    array('horizontal' => 'left')
                );
                $cellRange = 'A:U';
                $arrayAlphabet = [
                    'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                    'K', 'L', 'M', 'N', 'O', 'P'
                ];
                foreach ($arrayAlphabet as $alphabet) {
                    $event->sheet->getColumnDimension($alphabet)->setWidth(30);
                }
                $event->sheet->getColumnDimension('A')->setWidth(5);
                // title
                $cellRange = 'A8:P8';
                $active_sheet->getStyle($cellRange)->applyFromArray($default_font_style_title);
                $active_sheet->getStyle($cellRange)->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                );

                $active_sheet->getStyle('A2:F2')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('A2:F2')->applyFromArray($default_font_style_title2);
                $active_sheet->getStyle('A3:F3')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('A3:F3')->applyFromArray($default_font_style_title2);
                $active_sheet->getStyle('A5:F5')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('A5:F5')->applyFromArray($default_font_style_title2);
                if($this->countRow) $active_sheet->getStyle('A9:P'.($this->countRow+8))->applyFromArray($default_font_style2);
            },
        ];
    }
    public function startCell(): string
    {
        return 'A8';
    }
}
