<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Exports\CopyrightSheetFirst;
use App\Exports\CopyrightSheetSecond;
use App\Exports\CopyrightSheetThird;

class CopyrightExport implements WithMultipleSheets{
    public function sheets(): array{
        $sheets = [];
        // $sheets[] = new CopyrightSheetFirst(); // sheet 1
        // $sheets[] = new CopyrightSheetSecond(); // sheet 2
        $sheets[] = new CopyrightSheetThird(); // sheet 3
        return $sheets;
    }

}
