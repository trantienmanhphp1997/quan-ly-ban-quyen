<?php

namespace App\Exports;

use App\Models\FilmList;
use Maatwebsite\Excel\Concerns\FromCollection; 
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use DB;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
class FilmListExport implements FromCollection, WithHeadings, WithMapping,WithEvents,WithCustomStartCell,WithColumnFormatting
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use RegistersEventListeners;
    public $stt = 0;
    protected $searchName;
    protected $searchVNName;
    protected $searchActor;
    protected $searchParent;
    protected $countRow = 0;
    function __construct($searchName, $searchVNName, $searchActor, $searchParent) {
        $this->searchName = trim($searchName);
        $this->searchVNName = trim($searchVNName); 
        $this->searchActor = trim($searchActor); 
        $this->searchParent = trim($searchParent); 
    }

    public function collection()
    {      
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        $filmList = FilmList::query();
        $filmList->leftJoin('films', 'film_lists.film_id', '=', 'films.id')->whereNull('films.deleted_at');
        if($this->searchName)
            $filmList->where('film_lists.name','LIKE','%'.$this->searchName.'%');
        if($this->searchVNName)
            $filmList->where('film_lists.vn_name','LIKE','%'.$this->searchVNName.'%');
        if($this->searchActor)
            $filmList->where('film_lists.actor_name','LIKE','%'.$this->searchActor.'%');
        if($this->searchParent)
            $filmList->where('films.vn_name','LIKE','%'.$this->searchParent.'%');
        $filmList->select('film_lists.*', DB::raw('film_lists.vn_name as vn_name'), DB::raw('film_lists.name as name'), DB::raw('film_lists.actor_name as actor_name'),DB::raw('films.vn_name as parent_name'));
        $filmList = $filmList->get();
        $this->countRow = count($filmList);       
        return $filmList;
    }

    public function headings():array{
        return [
            'STT',
            'Tên gốc',
            'Tên tiếng Việt',
            'Năm sản xuất',
            'Doanh thu phòng vé',
            'Quyền',
            'Thể loại',
            'Diễn viên',
            'Đạo diễn',
            'Thời lượng',
            'Ghi chú',
            'Phim cha',
        ];
    }

    public function map($filmList): array {
        return [
            ++$this->stt,
            $filmList->name,
            $filmList->vn_name,
            $filmList->year_create,
            $filmList->box_office_revenue,
            $filmList->permission,
            $filmList->category_film,
            $filmList->actor_name,
            $filmList->director,
            $filmList->count_minute,
            $filmList->note,
            $filmList->parent_name,
        ];
    }
    public function registerEvents(): array
    {
       return [
            AfterSheet::class => function(AfterSheet $event) {
                $event
                    ->sheet
                    ->getDelegate()
                    ->setMergeCells([
                        'A2:F2',
                        'A3:F3',
                        'A5:F5',
                    ]);
                $event->sheet->getDelegate()->setCellValue('A2', 'CÔNG TY TRUYỀN THÔNG VIETTEL');
                $event->sheet->getDelegate()->setCellValue('A3', 'Quản lý kho nội dung');
                $event->sheet->getDelegate()->setCellValue('A5', 'DANH SÁCH PHIM LẺ');
                $default_font_style = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12]
                ];
                $default_font_style2 = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '00000000'],
                        ],
                    ],
                ];
                $default_font_style_title = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12, 'bold' =>  true],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'startColor' => [
                            'argb' => 'B7DEE8',
                        ],
                        'endColor' => [
                            'argb' => 'B7DEE8',
                        ],
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '00000000'],
                        ],
                    ],
                ];
                $default_font_style_title2 = [
                    'font' => ['name' => 'Times New Roman', 'size' => 14, 'bold' =>  true],
                ];
                $event->sheet->getStyle('A8:AZ8')->getAlignment()->setWrapText(true);
                $event->sheet->getStyle('A9:AZ9')->getAlignment()->setWrapText(true);
                $event->sheet->getStyle('A10:AZ10')->getAlignment()->setWrapText(true);
                
                $active_sheet = $event->sheet->getDelegate();
                $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
                $active_sheet->getParent()->getDefaultStyle()->getAlignment()->applyFromArray(
                    array('horizontal' => 'left')
                );
                $arrayAlphabet = [
                    'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                    'K', 'L'
                ];
                foreach ($arrayAlphabet as $alphabet) {
                    $event->sheet->getColumnDimension($alphabet)->setWidth(30);
                }
                $event->sheet->getColumnDimension('A')->setWidth(5);
                // title
                $cellRange = 'A8:L8';
                $active_sheet->getStyle($cellRange)->applyFromArray($default_font_style_title);
                $active_sheet->getStyle($cellRange)->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                );

                $active_sheet->getStyle('A2:F2')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('A2:F2')->applyFromArray($default_font_style_title2);
                $active_sheet->getStyle('A3:F3')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('A3:F3')->applyFromArray($default_font_style_title2);
                $active_sheet->getStyle('A5:F5')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('A5:F5')->applyFromArray($default_font_style_title2);
                if($this->countRow) $active_sheet->getStyle('A9:L'.($this->countRow+8))->applyFromArray($default_font_style2);
            },
        ];
    }
    public function startCell(): string
    {
        return 'A8';
    }

    public function columnFormats(): array
    {
        return [
            'E' => NumberFormat::FORMAT_CURRENCY_USD,
            // 'E' => NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE,
        ];
    }

}