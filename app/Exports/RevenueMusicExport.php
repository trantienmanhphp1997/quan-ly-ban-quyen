<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;
use App\Models\Contract;

class RevenueMusicExport implements FromCollection, WithHeadings, WithMapping, WithEvents, ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */

    protected $searchNumber;
    protected $searchType;
    protected $searchDate;
    protected $countRow = 0;
    public function __construct($searchNumber, $searchType, $searchDate)
    {
        $this->searchNumber = trim($searchNumber);
        $this->searchType = trim($searchType);
        $this->searchDate = $searchDate;
    }

    public function collection()
    {
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        $query = Contract::where('category_id', '=', 1)->where('type', '=', 2);
        if($this->searchNumber) {
            $query->where('contract_number', 'like', '%'.trim($this->searchNumber).'%');
        }
        if ($this->searchDate) {
            $query->whereBetween('date_sign', [$this->searchDate['start_date'], $this->searchDate['end_date']]);
        }
        $query = $query->get();
        $stt = 1;
        foreach($query as $val){
            $val->stt= $stt++;
        }
        $this->countRow = count($query);
        return $query;
    }

    public function headings(): array
    {
        return [
            'STT',
            'Số hợp đồng',
            'Giá tiền',
            'Ngày ký',
            'Thời gian bắt đầu bản quyền',
            'Thời gian kết thúc bản quyền'
        ];
    }
    public function map($contract): array
    {
        return [
            $contract->stt,
            $contract->contract_number,
            $contract->total_money?numberFormat($contract->total_money):'0',
            $contract->date_sign? reFormatDate($contract->date_sign,'d/m/Y'):'',
            $contract->start_time? reFormatDate($contract->start_time,'d/m/Y'):'', // thời gian bắt đầu
            $contract->end_time? reFormatDate($contract->end_time,'d/m/Y'):'', // thời gian kết thúc
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $default_font_style = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12],
                ];
                $default_font_style2 = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '00000000'],
                        ],
                    ],
                ];
                $default_font_style_title = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12, 'bold' =>  true],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'startColor' => [
                            'argb' => 'B7DEE8',
                        ],
                        'endColor' => [
                            'argb' => 'B7DEE8',
                        ],
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '00000000'],
                        ],
                    ],
                ];
                $active_sheet = $event->sheet->getDelegate();
                $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
                $active_sheet->getParent()->getDefaultStyle()->getAlignment()->applyFromArray(
                    array('horizontal' => 'left')
                );
                $cellRange = 'A:C';
                $arrayAlphabet = [
                    'A', 'B', 'C','D','E','F',
                ];
                foreach ($arrayAlphabet as $alphabet) {
                    $event->sheet->getColumnDimension($alphabet)->setAutoSize(true);
                }
                // title
                $cellRange = 'A1:F1';
                $active_sheet->getStyle($cellRange)->applyFromArray($default_font_style_title);
                $active_sheet->getStyle($cellRange)->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical' => 'center')
                );
                if($this->countRow) $active_sheet->getStyle('A2:F'.($this->countRow+1))->applyFromArray($default_font_style2);

            },
        ];
    }
}
