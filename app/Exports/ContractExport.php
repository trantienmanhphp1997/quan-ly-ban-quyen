<?php

namespace App\Exports;

use App\Models\Contract;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Excel;
use Carbon\Carbon;
class ContractExport implements FromCollection,WithHeadings,WithMapping,WithEvents, WithCustomStartCell
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use RegistersEventListeners;
    public $stt = 0;
    public $contract = 0;
    protected $countRow = 0;
    function __construct($contractList) {
        $this->contract = $contractList;
    }

    public function collection()
    {
        ini_set('memory_limit', '-1');
        set_time_limit(0);

        $this->countRow = count($this->contract);
        return $this->contract;
    }

    public function headings(): array {
        return [
            'STT',
            'Tiêu đề',
            'Nội dung',
            'Số hợp đồng',
            'Loại hợp đồng',
            'Theo tờ trình số',
            'Ngày ký',
            'Ngày bắt đầu bản quyền',
            'Ngày hết hạn',
            'Tổng giá trị hợp đồng',
            'Tình trạng nội dung',
            'Đối tác',
            'Họ và tên',
            'Số điện thoại',

        ];
    }
    public function map($contract): array {
        return [
            ++$this->stt,
            $contract->title,
            $contract->content,
            $contract->contract_number,
            $contract->category->name,
            $contract->report,
            reFormatDate($contract->date_sign,'d/m/Y'),
            reFormatDate($contract->start_time,'d/m/Y'),
            reFormatDate($contract->end_time,'d/m/Y'),
            $contract->total_money,
            $contract->note,
            $contract->partner->name??'',
            $contract->user_name,
            $contract->user_msisdn,
        ];
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event
                    ->sheet
                    ->getDelegate()
                    ->setMergeCells([
                        'A8:A9',
                        'B8:B9',
                        'C8:C9',
                        'D8:D9',
                        'E8:E9',
                        'G8:G9',
                        'I8:I9',
                        'F8:F9',
                        'G8:G9',
                        'H8:H9',
                        'I8:I9',
                        'J8:J9',
                        'K8:K9',
                        'L8:L9',
                        'M8:N8',
                        'A2:F2',
                        'A3:F3',
                        'A5:F5',
                    ]);
                    // ->freezePane('A3');
                $event->sheet->getDelegate()->setCellValue('A2', 'CÔNG TY TRUYỀN THÔNG VIETTEL');
                $event->sheet->getDelegate()->setCellValue('A3', 'Quản lý kho nội dung');
                $event->sheet->getDelegate()->setCellValue('A5', 'DANH SÁCH HỢP ĐỒNG MUA');

                $event->sheet->getDelegate()->setCellValue('A8', 'STT');
                $event->sheet->getDelegate()->setCellValue('B8', 'Tiêu đề');
                $event->sheet->getDelegate()->setCellValue('C8', 'Nội dung');
                $event->sheet->getDelegate()->setCellValue('D8', 'Số hợp đồng');
                $event->sheet->getDelegate()->setCellValue('E8', 'Loại hợp đồng');
                $event->sheet->getDelegate()->setCellValue('F8', 'Theo tờ trình số');
                $event->sheet->getDelegate()->setCellValue('G8', 'Ngày ký');
                $event->sheet->getDelegate()->setCellValue('H8', 'Ngày bắt đầu bản quyền');
                $event->sheet->getDelegate()->setCellValue('I8', 'Ngày hết hạn');
                $event->sheet->getDelegate()->setCellValue('J8', 'Tổng giá trị hợp đồng');
                $event->sheet->getDelegate()->setCellValue('K8', 'Tình trạng nội dung');
                $event->sheet->getDelegate()->setCellValue('L8', 'Đối tác');
                $event->sheet->getDelegate()->setCellValue('M8', 'Người phụ trách');

                $default_font_style = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12]
                ];
                $default_font_style2 = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '00000000'],
                        ],
                    ],
                ];
                $default_font_style_title = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12, 'bold' =>  true],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'startColor' => [
                            'argb' => 'B7DEE8',
                        ],
                        'endColor' => [
                            'argb' => 'B7DEE8',
                        ],
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '00000000'],
                        ],
                    ],
                    'wrapText' => true,
                ];
                $default_font_style_title2 = [
                    'font' => ['name' => 'Times New Roman', 'size' => 14, 'bold' =>  true],
                ];
                $event->sheet->getStyle('A8:N9')->getAlignment()->setWrapText(true);
                $active_sheet = $event->sheet->getDelegate();
                $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
                $active_sheet->getParent()->getDefaultStyle()->getAlignment()->applyFromArray(
                    array('horizontal' => 'left')
                );
                $arrayAlphabet = [
                    'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
                ];
                foreach ($arrayAlphabet as $alphabet) {
                    $event->sheet->getColumnDimension($alphabet)->setWidth(30);
                }

                $event->sheet->getColumnDimension('A')->setWidth(5);
                $arrayAlphabet = [
                    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',

                ];    
                foreach ($arrayAlphabet as $alphabet) {
                    $newCell =  $alphabet.'8:'.$alphabet.'9';
                    $active_sheet->getStyle($newCell)->getAlignment()->applyFromArray(
                        array('horizontal' => 'center', 'vertical'=>'center')
                    ); 
                    $active_sheet->getStyle($newCell)->applyFromArray($default_font_style_title);
                }                           
                $active_sheet->getStyle('A2:F2')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('A2:F2')->applyFromArray($default_font_style_title2);
                $active_sheet->getStyle('A3:F3')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('A3:F3')->applyFromArray($default_font_style_title2);
                $active_sheet->getStyle('A5:F5')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                ); 
                $active_sheet->getStyle('A5:F5')->applyFromArray($default_font_style_title2);


                if($this->countRow) $active_sheet->getStyle('A10:N'.($this->countRow+10))->applyFromArray($default_font_style2);
            },
        ];
    }
    public function startCell(): string
    {
        return 'A9';
    }
}
