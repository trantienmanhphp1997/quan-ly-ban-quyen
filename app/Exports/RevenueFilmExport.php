<?php

namespace App\Exports;

use App\Models\Film;
use App\Models\Revenue;
use App\Models\RevenueType;
use Maatwebsite\Excel\Concerns\FromCollection;
use DB;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;

class RevenueFilmExport implements FromCollection, WithHeadings, WithMapping
, WithEvents, WithCustomStartCell
{
    /**
     * @return \Illuminate\Support\Collection
     */

    protected $searchNumber;
    protected $searchName;
    protected $searchDate;
    protected $searchCategory;
    protected $searchCountry;
    protected $mergeCells;
    protected $currentPosition;
    protected $countRevenueType;
    protected $countRevenueParentType;
    protected $countRow = 0;
    protected $setDate;

    public function __construct($searchNumber, $searchName, $setDate, $searchCategory, $searchCountry){
        $this->searchNumber= trim($searchNumber);
        $this->searchName= trim($searchName);
        $this->searchCountry= trim($searchCountry);
        $this->searchCategory= $searchCategory;
        $this->setDate = $setDate;
    }

    public function collection()
    {
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        $query = Revenue::where('revenue.category_id',2);
        if(auth()->user()->is_manager == 0) $query->where('revenue.admin_id', auth()->user()->id);

        $query->leftjoin('contracts','contracts.id','=','revenue.contract_id');
        $query->leftjoin('films','films.id','=','revenue.media_id');
        $query->leftjoin('revenue_type','revenue_type.id','=','revenue.type_id');
        $query->leftjoin('countries','films.country', '=', 'countries.id');
        $query->select(
            'revenue.*', 
            'films.license_fee',
            DB::raw('contracts.contract_number as con_num'),DB::raw('films.vn_name as vn_name'),
            DB::raw('films.product_name as product_name'),
            DB::raw('films.country as country'),
            DB::raw('films.count_tap as count_tap'),
            DB::raw('films.year_create as year_create'),
            DB::raw('countries.name as country_name'),
            //DB::raw('revenue_type.name as type_name')
        );
        if ($this->searchName) $query->where('films.vn_name','like','%'.trim($this->searchName).'%');
        // if ($this->searchNumber) $query->whereRaw("contracts.contract_number LIKE '%{$this->searchNumber}%'");
        if ($this->searchName) $query->where('films.vn_name','like','%'.trim($this->searchName).'%');
        if ($this->searchCategory == 2) $query->where('films.count_tap','!=', 1);
        if ($this->searchCategory == 1) $query->where('films.count_tap', 1);
        if ($this->searchCountry) $query->where('countries.id', $this->searchCountry);
        //if ($this->searchType) $query->where('revenue.type_id','=',$this->searchType);
        if(!empty($this->setDate)){
            $getDate = $this->setDate ? explode(" - ", $this->setDate) : null;
            if ($getDate != '') {
                $startDateCharge = $getDate[0];
                $endDateCharge = $getDate[1];
                if ($startDateCharge != '' && $endDateCharge != '') {
                    $start_time = date("Y-m-d", strtotime($startDateCharge));
                    $end_time = date("Y-m-d", strtotime($endDateCharge));
                    $query->whereBetween('films.start_time', [$start_time, $end_time]);
                }
            }
        }

        $query= $query->groupBy('films.id');

        $revenue=$query->orderBy('revenue.id','desc')->get();

        $str_role='';
        $str_cate='';

        $categories=DB::table('film_list_categories')
            ->leftjoin('category_films', function($join)
            {
                $join->on('category_films.id', '=', 'film_list_categories.id_category_film');
            })->get();
        $roles= \DB::table('contract_media_sale_role')
            ->leftjoin('role_media', function($join)
            {
                $join->on('contract_media_sale_role.role_id', '=', 'role_media.id');
            })->get();
        $key=1;
        foreach ($revenue as $val) {
            $role_name_buy='';
            $role_name_sale='';
            foreach($val->film->roles as $role) {
                if($role->level!=1)
                    $role_name_buy.=$role->code.', ';
            }
            $val->role_name_buy= trim($role_name_buy,', ');
            foreach($val->film->roleSales->unique() as $role) {
                if($role->level!=1)
                    $role_name_sale.=$role->code.', ';
            }
            $val->role_name_sale= trim($role_name_sale,', ');
            foreach($categories->where('id_film_list',$val->media_id) as  $item){
                if($item->name!='')$str_cate.=$item->name.', ';
            }
            $val->date_sign = reFormatDate($val->date_sign);
            $val->cate_name=trim($str_cate,', ');
            $val->stt=$key++;
            $str_role='';
            $str_cate='';
        }
        $this->countRow = count($revenue);
        return $revenue;
    }

    public function headings(): array
    {

        $headings = [
            'STT',
            'Tên phim tiếng Việt- Tên phim tiếng Anh',
            'Số hợp đồng',
            'Quốc gia',
            'Thể loại',
            'Số tập',
            'Năm sản xuất',
            'Tổng chi phí mua',
        ];

        $parentTypes = RevenueType::parent();
        foreach ($parentTypes as $parentType) {
            $childrenTypes = RevenueType::where('parent_id', '=', $parentType->id)->get();
            foreach ($childrenTypes as $childrenType) {
                $headings[] = $childrenType->name;
            }
            $headings[] = 'Tổng '.$parentType->name;
        }
        $headings[] = 'Tổng';


        foreach ($parentTypes as $parentType) {
            $childrenTypes = RevenueType::where('parent_id', '=', $parentType->id)->get();
            foreach ($childrenTypes as $childrenType) {
                $headings[] = $childrenType->name;
            }
            $headings[] = 'Tổng '.$parentType->name;
        }
        $headings[] = 'Tổng';

        foreach ($parentTypes as $parentType) {
            $headings[] = $parentType->name;
        }
        $headings[] = 'Tổng';

        $lastHeadings = [
            'Danh sách quyền mua',
            'Danh sách quyền đã bán',
            'Thời gian bắt đầu',
            'Thời gian kết thúc',
            'Nội dung phim',
            'Diễn viên',
            'Đạo diễn',
        ];
        $headings = array_merge($headings, $lastHeadings);
        return $headings;
    }
    
    public function map($revenue):array{
        $map = [
            $revenue->stt,
            $revenue->film->product_name?$revenue->vn_name.' - '.$revenue->film->product_name:$revenue->vn_name,
            $revenue->film->contract?$revenue->film->contract->contract_number:'',
            $revenue->country_name,
            $revenue->cate_name,
            $revenue->count_tap,
            $revenue->year_create,
            numberFormat(countChiPhiTap($revenue->film)),
        ];
        $parentTypes = RevenueType::parent();

        $sum = 0;
        $minRevenueArray =[];
        foreach ($parentTypes as $parentType) {
            $childrenTypes = RevenueType::where('parent_id', '=', $parentType->id)->get();
            $sumRevenueByParent = 0;
            foreach ($childrenTypes as $childrenType) {
                $map[] = numberFormat($revenue->film->revenueByType($childrenType->id));
                $sumRevenueByParent += $revenue->film->revenueByType($childrenType->id);
            }
            $map[] = $sumRevenueByParent?numberFormat($sumRevenueByParent):'0';
            $sum += $sumRevenueByParent;
            $minRevenueArray[] = $sumRevenueByParent;
        }
        $map[] = $sum?numberFormat($sum):'0';
        $minRevenueArray[] =$sum;

        $sum = 0;
        $saleRevenueArray =[];
        foreach ($parentTypes as $parentType) {
            $childrenTypes = RevenueType::where('parent_id', '=', $parentType->id)->get();
            $sumRevenueByParent = 0;
            foreach ($childrenTypes as $childrenType) {
                $map[] = numberFormat($revenue->film->saleRevenueByType($childrenType->id));
                $sumRevenueByParent += $revenue->film->saleRevenueByType($childrenType->id);
            }
            $map[] = $sumRevenueByParent?numberFormat($sumRevenueByParent):'0';
            $sum += $sumRevenueByParent;
            $saleRevenueArray[] = $sumRevenueByParent;
        }
        $map[] = $sum?numberFormat($sum):'0';
        $saleRevenueArray[] =$sum;

        for ($i=0; $i<count($minRevenueArray); $i++ ) {
            $map[] = numberFormat($minRevenueArray[$i]-$saleRevenueArray[$i]);
        }

        $lastMap = [
            $revenue->role_name_buy, // danh sách quyền mua
            $revenue->role_name_sale, // danh sách quyền bán
            $revenue->film->start_time? reFormatDate($revenue->film->start_time,'m/d/Y'):'', // thời gian bắt đầu
            $revenue->film->end_time? reFormatDate($revenue->film->end_time,'m/d/Y'):'', // thời gian kết thúc
            $revenue->film->note,
            $revenue->film->actor_name,
            $revenue->film->director,
        ];
        $map = array_merge($map, $lastMap);
        return $map;
    }

    public function registerEvents(): array
    {

        $this->mergeCells = [
                        'A9:A11',
                        'B9:B11',
                        'C9:C11',
                        'D9:D11',
                        'E9:E11',
                        'F9:F11',
                        'G9:G11',
                        'H9:H11',
                        'A2:F2',
                        'A3:F3',
                        'A6:F6',
                    ];
        $this->currentPosition = 'I';
        $countParentType = RevenueType::parent()->count();
        $this->countRevenueParentType = $countParentType;
        $countChildrenType = RevenueType::where('parent_id','!=', 0)->count();
        $countRevenueType = $countParentType + $countChildrenType + 1;
        $this->countRevenueType= $countRevenueType;
        $doanhThuDaBan = charCalculation('Q', $countRevenueType);
        $this->mergeCells[] = 'I9:'.charCalculation('I', $countRevenueType-1).'9';
        $this->currentPosition = charCalculation('I', $countRevenueType);
        $this->mergeCells[] = $this->currentPosition.'9:'.charCalculation($this->currentPosition, $countRevenueType-1).'9';
        $this->currentPosition = charCalculation($this->currentPosition, $countRevenueType);
        $this->mergeCells[] = $this->currentPosition.'9:'.charCalculation($this->currentPosition,$countParentType).'10';


        $this->currentPosition = 'I';
        foreach(RevenueType::parent() as $parentType){
            $countChildrenOfParent = RevenueType::where('parent_id','=', $parentType->id)->count();
            $this->mergeCells[] = $this->currentPosition.'10:'.charCalculation($this->currentPosition, $countChildrenOfParent).'10';
            $this->currentPosition = charCalculation($this->currentPosition, $countChildrenOfParent+1);
        }
        $this->mergeCells[] = $this->currentPosition.'10:'.$this->currentPosition.'11';
        $this->currentPosition = charCalculation($this->currentPosition, 1);


        foreach(RevenueType::parent() as $parentType){
            $countChildrenOfParent = RevenueType::where('parent_id','=', $parentType->id)->count();
            $this->mergeCells[] = $this->currentPosition.'10:'.charCalculation($this->currentPosition, $countChildrenOfParent).'10';
            $this->currentPosition = charCalculation($this->currentPosition, $countChildrenOfParent+1);
        }
        $this->mergeCells[] = $this->currentPosition.'10:'.$this->currentPosition.'11';
        $this->currentPosition = charCalculation($this->currentPosition, $countParentType+2);

        for ($i=0; $i<=6; $i++){
            $this->mergeCells[] = $this->currentPosition.'9:'.$this->currentPosition.'11';
            $this->currentPosition++;
        }
        $this->currentPosition = 'I';

        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event
                    ->sheet
                    ->getDelegate()
                    ->setMergeCells($this->mergeCells);
                    // ->freezePane('A3');
                $event->sheet->getRowDimension(10)->setRowHeight(30);
                $event->sheet->getRowDimension(11)->setRowHeight(30);
                $event->sheet->getDelegate()->setCellValue('A2', 'CÔNG TY TRUYỀN THÔNG VIETTEL');
                $event->sheet->getDelegate()->setCellValue('A3', 'Quản lý kho nội dung');
                $event->sheet->getDelegate()->setCellValue('A6', 'THỐNG KÊ DOANH THU PHIM (Tổng hợp)');

                $event->sheet->getDelegate()->setCellValue('A9', 'STT');
                $event->sheet->getDelegate()->setCellValue('B9', 'Tên phim tiếng Việt- Tên phim tiếng Anh');
                $event->sheet->getDelegate()->setCellValue('C9', 'Số hợp đồng');
                $event->sheet->getDelegate()->setCellValue('D9', 'Quốc gia');
                $event->sheet->getDelegate()->setCellValue('E9', 'Thể loại');
                $event->sheet->getDelegate()->setCellValue('F9', 'Số tập');
                $event->sheet->getDelegate()->setCellValue('G9', 'Năm sản xuất');
                $event->sheet->getDelegate()->setCellValue('H9', 'Tổng chi phí mua/tập');
                $event->sheet->getDelegate()->setCellValue('I9', 'DT tối thiểu theo tờ trình');
                $event->sheet->getDelegate()->setCellValue(charCalculation('I', $this->countRevenueType).'9', 'Doanh thu đã bán');
                foreach(RevenueType::parent() as $parentType){
                    $event->sheet->getDelegate()->setCellValue( $this->currentPosition.'10', 'DT tối thiểu theo tờ trình (USD/tập) '.$parentType->name);
                    $countChildrenOfParent = RevenueType::where('parent_id','=', $parentType->id)->count();
                    $this->currentPosition = charCalculation($this->currentPosition, $countChildrenOfParent+1);
                }
                $event->sheet->getDelegate()->setCellValue( $this->currentPosition++.'10', 'Tổng');

                foreach(RevenueType::parent() as $parentType){
                    $event->sheet->getDelegate()->setCellValue( $this->currentPosition.'10', 'Doanh thu đã bán (USD/tập) '.$parentType->name);
                    $countChildrenOfParent = RevenueType::where('parent_id','=', $parentType->id)->count();
                    $this->currentPosition = charCalculation($this->currentPosition, $countChildrenOfParent+1);
                }
                $event->sheet->getDelegate()->setCellValue( $this->currentPosition++.'10', 'Tổng');
                $event->sheet->getDelegate()->setCellValue( $this->currentPosition++.'9', 'Doanh thu còn lại cần phải bán (USD/tập)');
                $this->currentPosition = charCalculation($this->currentPosition, $this->countRevenueParentType);

                $event->sheet->getDelegate()->setCellValue( $this->currentPosition++.'9', 'Danh sách quyền mua');
                $event->sheet->getDelegate()->setCellValue( $this->currentPosition++.'9', 'Danh sách quyền đã bán');
                $event->sheet->getDelegate()->setCellValue( $this->currentPosition++.'9', 'Thời gian bắt đầu');
                $event->sheet->getDelegate()->setCellValue( $this->currentPosition++.'9', 'Thời gian kết thúc');
                $event->sheet->getDelegate()->setCellValue( $this->currentPosition++.'9', 'Nội dung phim');
                $event->sheet->getDelegate()->setCellValue( $this->currentPosition++.'9', 'Diễn viên');
                $event->sheet->getDelegate()->setCellValue( $this->currentPosition++.'9', 'Đạo diễn');

                $default_font_style = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12]
                ];
                $default_font_style2 = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '00000000'],
                        ],
                    ],
                ];
                $default_font_style_title = [
                    'font' => ['name' => 'Times New Roman', 'size' => 12, 'bold' =>  true],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'startColor' => [
                            'argb' => 'B7DEE8',
                        ],
                        'endColor' => [
                            'argb' => 'B7DEE8',
                        ],
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '00000000'],
                        ],
                    ],
                ];
                $default_font_style_title2 = [
                    'font' => ['name' => 'Times New Roman', 'size' => 14, 'bold' =>  true],
                ];

                $event->sheet->getStyle('A11:AZ11')->getAlignment()->setWrapText(true);
                $event->sheet->getStyle('A9:AZ9')->getAlignment()->setWrapText(true);
                $event->sheet->getStyle('A10:AZ10')->getAlignment()->setWrapText(true);

                $active_sheet = $event->sheet->getDelegate();
                $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
                $active_sheet->getParent()->getDefaultStyle()->getAlignment()->applyFromArray(
                    array('horizontal' => 'left')
                );
                $arrayAlphabet = [
                    'A', 'C', 'D', 'E', 'H',
                ];
                for($i=$this->countRevenueType*2+$this->countRevenueParentType+2; $i<=$this->countRevenueType*2+$this->countRevenueParentType+8; $i++){
                    $arrayAlphabet[]= charCalculation('H', $i);
                }
                foreach ($arrayAlphabet as $alphabet) {
                    $event->sheet->getColumnDimension($alphabet)->setWidth(30);
                }
                $arrayAlphabet = [
                    'B',
                ];
                foreach ($arrayAlphabet as $alphabet) {
                    $event->sheet->getColumnDimension($alphabet)->setWidth(40);
                }

                $arrayAlphabet = [
                    'F', 'G'
                ];
                foreach ($arrayAlphabet as $alphabet) {
                    $event->sheet->getColumnDimension($alphabet)->setWidth(15);
                }

                $arrayAlphabet = [ ];
                for($i=1; $i<=$this->countRevenueType*2+$this->countRevenueParentType+1; $i++){
                    $arrayAlphabet[]= charCalculation('H', $i);
                }
                foreach ($arrayAlphabet as $alphabet) {
                    $event->sheet->getColumnDimension($alphabet)->setWidth(12);
                }
                $event->sheet->getColumnDimension('A')->setWidth(5);
                $arrayAlphabet = [
                    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                ];

                for($i=1; $i<=$this->countRevenueType*2+$this->countRevenueParentType+8; $i++){
                    $arrayAlphabet[]= charCalculation('H', $i);
                }
                foreach ($arrayAlphabet as $alphabet) {
                    $newCell =  $alphabet.'9:'.$alphabet.'11';
                    $active_sheet->getStyle($newCell)->getAlignment()->applyFromArray(
                        array('horizontal' => 'center', 'vertical'=>'center')
                    );
                    $active_sheet->getStyle($newCell)->applyFromArray($default_font_style_title);
                }
                $active_sheet->getStyle('A2:F2')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                );
                $active_sheet->getStyle('A2:F2')->applyFromArray($default_font_style_title2);

                $active_sheet->getStyle('A3:F3')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                );
                $active_sheet->getStyle('A3:F3')->applyFromArray($default_font_style_title2);

                $active_sheet->getStyle('A6:F6')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                );
                $active_sheet->getStyle('A6:F6')->applyFromArray($default_font_style_title2);

                $active_sheet->getStyle('I9:Q9')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                );

                $active_sheet->getStyle('I11:P11')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                );

                $active_sheet->getStyle('I10:M10')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                );

                $active_sheet->getStyle('N10:P10')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                );

                $active_sheet->getStyle('Q10:Q11')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                );

                $active_sheet->getStyle('R9:Z9')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                );

                $active_sheet->getStyle('R11:Y11')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                );

                $active_sheet->getStyle('R10:V10')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                );

                $active_sheet->getStyle('W10:Y10')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                );

                $active_sheet->getStyle('Z10:Z11')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                );

                $active_sheet->getStyle('AA9:AC10')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                );

                $active_sheet->getStyle('AA11:AC11')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center', 'vertical'=>'center')
                );
                $currentPosition = charCalculation('H', $this->countRevenueType*2+$this->countRevenueParentType+8);
                if($this->countRow) $active_sheet->getStyle('A12:'.$currentPosition.($this->countRow+11))->applyFromArray($default_font_style2);
            },
        ];
    }
    public function startCell(): string
    {
        return 'A11';
    }
}
