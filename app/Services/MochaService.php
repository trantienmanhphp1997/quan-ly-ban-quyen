<?php


namespace App\Services;


use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class MochaService
{
    /**
     * send message to list of users by phone numbers
     * @param $service_id: provided by mocha
     * @param $message_content: content wants to send
     * @param $phone_numbers: string of users' phone number, delimited by ","
     * @param $label: button's name
     * @param $link: deeplink to redirect
     */
    public static function sendMessageToUser($service_id, $message_content, $users, $label, $link){
        Log::info('sendMessageToUser', [$users]);
        try{
            $phone_numbers = [];
            foreach ($users as $user) {
                if (!empty($user->recieve_notice_phone_number))
                    $phone_numbers[] = data_get($user, 'recieve_notice_phone_number', '');
                else
                    $phone_numbers[] = data_get($user, 'phone_number', '');
            }

            $phone_unique = array_unique($phone_numbers);
            $chunkedphones = array_chunk($phone_unique, 20);
            foreach ($chunkedphones as $chunkedphone){
                self::sendMessageToPhoneNumbers($service_id, $message_content, $chunkedphone, $label, $link);
            }
        }catch (\Exception $e){
            Log::error($e);
            Log::error("cannot call api mocha");
            return null;
        }
        return true;
    }

    /**
     * send message to list of users by phone numbers
     * @param $service_id: provided by mocha
     * @param $message_content: content wants to send
     * @param $phone_numbers: string of users' phone number, delimited by ","
     * @param $label: button's name
     * @param $link: deeplink to redirect
     */
    public static function sendMessageToPhoneNumbers($service_id, $message_content, $phone_numbers, $label, $link){
        Log::info("Send message", [
            'service_id' => $service_id,
            'content' => $message_content,
            'label' => $label,
            'msisdns' =>  $phone_numbers,
            'deeplink' => $link,
        ]);
        try{
            return Http::asForm()->post(config("mocha.send_message_url"), [
                'service_id' => $service_id,
                'content' => $message_content,
                'label' => $label,
                'msisdns' =>  $phone_numbers,
                'deeplink' => $link,
            ]);
        }catch (\Exception $e){
            Log::error($e);
            Log::error("cannot call api mocha");
            return null;
        }

    }

    /** determind user info by uuid
     * @param $uuid
     * @return \Illuminate\Http\Client\Response
     * @output
     *  "code": 200 // thanh cong
        "msisdn": "0972791532", user's phone number
        "avatar": "link avatar của user",
        "gender": 1, =1 nam,=0 nữ
     */
    public function getMochaUserByUuid($uuid){
        Log::info("Get user info by uuid");
        Log::info("Uuid: ".$uuid);
        Log::info("Link: ". config("mocha.get_student_info_by_uuid"));
        // @todo withOptions(['verify' => false])
        return Http::withOptions(['verify' => false])->asForm()->post(config("mocha.get_student_info_by_uuid"), [
            "uuid" => $uuid,
            "username" => "hocthi",
            "pwd" => "hocthi2020"
        ]);
    }

    /**
     * send message to list of users by phone numbers
     * @param $service_id: provided by mocha
     * @param $message_content: content wants to send
     * @param $phone_numbers: list of users' phone number
     * @param $label: button's name
     * @param $link: deeplink to redirect
     */
    public static function sendMessage($service_id, $message_content, $phone_numbers, $label, $link){
        Log::debug('Send message to mocha user', [$phone_numbers, $message_content, $link]);
//        Log::info(json_encode([
//            'service_id' => $service_id,
//            'content' => $message_content,
//            'label' => $label,
//            'msisdns' => implode(',', $phone_numbers),
//            'deeplink' => $link,
//        ]));
//
//        return Http::asForm()->post(config("mocha.send_message_url"), [
//            'service_id' => $service_id,
//            'content' => $message_content,
//            'label' => $label,
//            'msisdns' => implode(',', $phone_numbers),
//            'deeplink' => $link,
//        ]);
    }
}
