<?php

namespace App\Imports;

use App\Models\FilmUpload;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\WithStartRow;
use DateTime;

class FilmUploadImport implements ToModel, WithValidation, SkipsOnError, WithStartRow
{
    use Importable, SkipsErrors, SkipsFailures;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        // $row[7] = floatval(str_replace(',','.',trim($row[7])));
        if($row[7]=='0.0') $row[7] = 0;
        $date = DateTime::createFromFormat('d/m/Y', $row[12]);
        if($date && $date->format('d/m/Y') === $row[12]){
            $row[12] = $date->format('Y-m-d');
        }   
        else {
            $row[12]= is_numeric($row[12])?\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject(trim($row[12]))->format('Y-m-d'):null;
        }  
        $date = DateTime::createFromFormat('d/m/Y', $row[13]);
        if($date && $date->format('d/m/Y') === $row[13]){
            $row[13] = $date->format('Y-m-d');
        }   
        else {
            $row[13]= is_numeric($row[13])?\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject(trim($row[13]))->format('Y-m-d'):null;
        }  
        return new FilmUpload([
            'STT'     => @$row[0],
            'country'    => @$row[1]?trim($row[1]):'',
            'vn_name' =>   @$row[2]?trim($row[2]):'',
            'product_name' =>   @$row[3]?trim($row[3]):'',
            'year_create'     =>  @$row[4]?trim($row[4]):'',
            'category'     => @$row[5],
            'count_name'     =>  @$row[6]?trim($row[6]):'',
            'count_tap'     => @$row[7]?$row[7]:'',
            'count_hour'     => @trim($row[8])?trim($row[8]):'',
            'partner_name'     => @$row[9]?trim($row[9]):'',
            'DQ'     => @$row[10],
            'KDQ'     => @$row[11],
            'end_time1'     => @$row[12],
            'start_time1'     => @$row[13],
            'contract_number'     => @$row[14]?trim($row[14]):'',
            'fee1'     => @$row[15]?trim($row[15]):'',
            'fee2'     => @$row[16]?trim($row[16]):'',
            'fee3'     => @$row[17]?trim($row[17]):'',
            'note'     => @$row[19],
            'actor_name'=> @$row[20],          
            'director'=> @$row[21],                   
            'broadcast_country'=> @$row[22],                   
            'nuoc'=> @$row[23],                   
            'count_live'=> @$row[24],                   
            'Broadcast_infrastructure'=> @$row[25],                   
            'youtube'=> @$row[26],                   
            'VOD_nuoc_ngoai'=> @$row[27],                   
            'VOD_noi_bo'=> @$row[28],                   
            'VOD_ngoai'=> @$row[29],                   
            'TH_noi_bo'=> @$row[30],                   
            'TH_ngoai'=> @$row[31],                   
            'addition_note'=> @$row[32],                   
        ]);
    }
    public function rules(): array
    {
         return [
             // 'ten_bai_hat' => 'required',
        //      // Above is alias for as it always validates in batches
         ];
    }
    public function startRow(): int
    {
        return 11;
    }
}
