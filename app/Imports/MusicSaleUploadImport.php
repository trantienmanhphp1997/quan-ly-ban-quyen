<?php

namespace App\Imports;

use App\Models\ContractMusicSaleUpload;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Illuminate\Support\Str;
use DateTime;
use DB;
class MusicSaleUploadImport implements ToModel, WithValidation, SkipsOnError, WithStartRow
{
    use Importable, SkipsErrors, SkipsFailures;
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {   
        $date = DateTime::createFromFormat('d/m/Y', $row[17]);
        if($date && $date->format('d/m/Y') === $row[17]){
            $row[17] = $date->format('Y-m-d');
        }   
        else {
            $row[17]= is_numeric($row[17])?\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject(trim($row[17]))->format('Y-m-d'):null;        
        }    
        $date = DateTime::createFromFormat('d/m/Y', $row[18]);
        if($date && $date->format('d/m/Y') === $row[18]){
            $row[18] = $date->format('Y-m-d');
        }   
        else {
            $row[18]= is_numeric($row[18])?\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject(trim($row[18]))->format('Y-m-d'):null;
        }  
        return new ContractMusicSaleUpload([
            'STT' => @$row[0],
            'ma_nhac_cho' =>  @$row[1]?trim($row[1]):'',
            'name_khong_dau' => @$row[2]?trim($row[2]):'',
            'singer_khong_dau' => @$row[3]?trim($row[3]):'',
            'name' => @$row[4]?trim($row[4]):'',
            'singer' => @$row[5]?trim($row[5]):'',
            'musician' => @$row[6]?trim($row[6]):'',
            'cho_docquyen' => @$row[7]? 1 : 0,
            'cho_tacquyen' => @$row[8]?1:0,
            'so_docquyen' => @$row[9]?1:0,
            'so_tacquyen' => @$row[10]?1:0,
            'ghi_am' =>  @$row[11]?trim($row[11]):'',
            'contract_number2' => @$row[12]?trim($row[12]):'',
            'contract_number3' => @$row[13]?trim($row[13]):'',
            'tac_gia' => @$row[14]?trim($row[14]):'',
            'contract_number5' => @$row[15]?trim($row[15]):'',
            'contract_number6' => @$row[16]?trim($row[16]):'',
            'start_time'     => @$row[17],
            'end_time' => @$row[18],
            'music_id' => @$row[19],
        ]);
    }
    public function rules(): array
    {
        return [
            // 'ten_bai_hat' => 'required',
            // 'status' => 'required'
        ];
    }
    public function startRow(): int
    {
        return 11;
    }
}
