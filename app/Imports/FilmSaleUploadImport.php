<?php

namespace App\Imports;

use App\Models\ContractFilmSaleUpload;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Illuminate\Support\Str;
use DateTime;
use DB;
class FilmSaleUploadImport implements ToModel, WithValidation, SkipsOnError, WithStartRow,WithCalculatedFormulas
{
    use Importable, SkipsErrors, SkipsFailures;
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {   
        $date = DateTime::createFromFormat('d/m/Y', $row[5]);
        if($date && $date->format('d/m/Y') === $row[5]){
            $row[5] = $date->format('Y-m-d');
        }   
        else {
            $row[5]= is_numeric($row[5])?\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject(trim($row[5]))->format('Y-m-d'):null;
        } 
        $date = DateTime::createFromFormat('d/m/Y', $row[16]);
        if($date && $date->format('d/m/Y') === $row[16]){
            $row[16] = $date->format('Y-m-d');
        }   
        else {
            $row[16]= is_numeric($row[16])?\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject(trim($row[16]))->format('Y-m-d'):null;
        } 
        $date = DateTime::createFromFormat('m/d/Y', $row[17]);
        if($date && $date->format('m/d/Y') === $row[17]){
            $row[17] = $date->format('Y-m-d');
        }   
        else {
            $row[17]= is_numeric($row[17])?\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject(trim($row[17]))->format('Y-m-d'):null;        
        }    
        return new ContractFilmSaleUpload([
            'STT' => @$row[0],
            'name' =>  @$row[1]?trim($row[1]):'',
            'vn_name' => @$row[2]?trim($row[2]):'',
            'contract_number' => @$row[3]?trim($row[3]):'',
            'partner_name' => @$row[4]?trim($row[4]):'',
            'date_sign' => @$row[5]?trim($row[5]):'',
            'employee_name' => @$row[6]?trim($row[6]):'',
            'telephone_number' => @$row[7]?trim($row[7]):'',
            'email' => @$row[8]?trim($row[8]):'',
            'country' =>@$row[9]?trim($row[9]):'',
            'count' => @$row[10]?trim($row[10]):'',
            'ĐQ' =>  @$row[11]?trim($row[11]):'',
            'KĐQ' => @$row[12]?trim($row[12]):'',
            'HT' => @$row[13]?trim($row[13]):'',
            'country_ps' => @$row[14]?trim($row[14]):'',
            'country_note_ps' => @$row[15]?trim($row[15]):'',
            'start_time' => @$row[16]?trim($row[16]):'',
            'end_time' => @$row[17]?trim($row[17]):'',
            'revenu_type' => @$row[18]?trim($row[18]):'',
            'money' => @$row[19]?trim($row[19]):'',
            'moneyVAT'     => @$row[20]?(int)trim($row[20]):'',
            'note' => @$row[21]?trim($row[21]):'',
        ]);
    }
    public function rules(): array
    {
        return [
            // 'ten_bai_hat' => 'required',
            // 'status' => 'required'
        ];
    }
    public function startRow(): int
    {
        return 11;
    }
}
