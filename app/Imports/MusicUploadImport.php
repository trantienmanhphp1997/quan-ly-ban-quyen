<?php

namespace App\Imports;

use App\Models\MusicUpload;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Illuminate\Support\Str;
use DateTime;
class MusicUploadImport implements ToModel, WithValidation, SkipsOnError, WithStartRow
{
    use Importable, SkipsErrors, SkipsFailures;
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {   
        $date = DateTime::createFromFormat('d/m/Y', $row[19]);
        if($date && $date->format('d/m/Y') === $row[19]){
            $row[19] = $date->format('Y-m-d');
        }   
        else {
            $row[19]= is_numeric($row[19])?\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject(trim($row[19]))->format('Y-m-d'):null;        
        }    
        $date = DateTime::createFromFormat('d/m/Y', $row[20]);
        if($date && $date->format('d/m/Y') === $row[20]){
            $row[20] = $date->format('Y-m-d');
        }   
        else {
            $row[20]= is_numeric($row[20])?\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject(trim($row[20]))->format('Y-m-d'):null;
        }    
        return new MusicUpload([
            'STT' => @$row[0],
            'ten_bai_hat' =>  @$row[1]?trim($row[1]):'',
            'ca_si' => @$row[2],
            'nhac_si' => @$row[3],
            'album' => @$row[4],
            'doc_quyen' => @$row[5],
            'tac_quyen' => @$row[6],
            'doc_quyen_so' => @$row[7],
            'tac_quyen_so' => @$row[8],
            'ban_doi_tac_3' => @$row[9],
            'quyen_ghi_am' =>  @$row[10]?trim($row[10]):'',
            'quyen_tac_gia' => @$row[11]?trim($row[11]):'',
            'phu_luc_hd' =>  @$row[12]?trim($row[12]):'',
            'so_hd_lq' => @$row[13]?trim($row[13]):'',
            'pl_hd' => @$row[14]?trim($row[14]):'',
            'stt_phu_luc' => @$row[15]?trim($row[15]):'',
            'hd_tac_gia' => @$row[16]?trim($row[16]):'',
            'pl_quyen_tac_gia' => @$row[17]?trim($row[17]):'',
            'stt_pl_quyen_tac_gia' => @$row[18]?trim($row[18]):'',
            'time_release'     => @$row[19],
            'time_end' => @$row[20],
            'audio' => @$row[21],
            'mv' => @$row[22],
            'note' => @$row[23],
            'loai_hd' => @$row[24]?trim($row[24]):'',
            'gia_tien' => @$row[25],
            'id_ms' => @$row[26],

        ]);
    }
    public function rules(): array
    {
        return [
            // 'ten_bai_hat' => 'required',
            // 'status' => 'required'
        ];
    }
    public function startRow(): int
    {
        return 11;
    }
}
