<?php

namespace App\Imports;

use App\Models\FilmBackup;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Str;
use DateTime;

class ManageUploadImport implements ToModel, WithValidation, SkipsOnError, WithStartRow
{
    use Importable, SkipsErrors, SkipsFailures;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row){
        // dd($row[14]);
        $date = DateTime::createFromFormat('d/m/Y', $row[14]);
        if($date && $date->format('d/m/Y') === $row[14]){
            $row[14] = $date->format('Y-m-d');
        }   
        else {
            $row[14]= is_numeric($row[14])?\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject(trim($row[14]))->format('Y-m-d'):null;
        }  
        return new FilmBackup([
            'driver'     => @$row[1]?trim($row[1]):'',
            'backup_driver'    =>  @$row[2]?trim($row[2]):'',
            'file_name' =>  @$row[3]?trim($row[3]):'',
            'film_name'     =>  @$row[4]?trim($row[4]):'',
            'vn_name'     =>@$row[5]?trim($row[5]):'',
            'country'     =>@$row[6]?trim(Str::title($row[6])):'',
            'partner'     => @$row[7],
            'series_film'     => @(strpos(trim(Str::title($row[8])), 'Bộ'))? 1 : (strpos(trim(Str::title($row[8])), 'Lẻ')? 2 : 0),
            'count_film'     => @$row[9]?trim($row[9]):'',
            'file_type'     => @$row[10],
            'time_number'     => @$row[11]?trim($row[11]):'',
            'volumn'     => @$row[12]?trim($row[12]):'',
            'file_format'     => @$row[13]?trim($row[13]):'',
            'end_time'     => @$row[14],
            'file_delivery_person'     => @$row[15],
            'status' => 1,
        ]);
    }
    public function rules(): array{
         return [
             // 'ten_bai_hat' => 'required',
        //      // Above is alias for as it always validates in batches
         ];
    }
    public function startRow(): int{
        return 9;
    }
}
