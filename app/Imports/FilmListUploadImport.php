<?php

namespace App\Imports;

use App\Models\FilmListUpload;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\WithStartRow;


class FilmListUploadImport implements ToModel, WithValidation, SkipsOnError, WithStartRow
{
    use Importable, SkipsErrors, SkipsFailures;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new FilmListUpload([
            'STT'     => @$row[0],
            'name'    => @$row[1]?trim($row[1]):'', 
            'vn_name' =>  @$row[2]?trim($row[2]):'',
            'year_create'     => @$row[3],
            'box_office_revenue'     => @$row[4],
            'permission'     => @$row[5],
            'category_film'     => @$row[6],
            'actor_name'     => @$row[7],
            'director'     => @$row[8],
            'count_minute' => @$row[9],
            'note' => @$row[10],
        ]);
    }
    public function rules(): array
    {
         return [
             // 'ten_bai_hat' => 'required',
        //      // Above is alias for as it always validates in batches
         ];
    }
    public function startRow(): int
    {
        return 9;
    }
}
